<?php
include("init.php");

$menu = new menuEditor();
//$menu->recurseArray($posted_data,$order);

if ($_GET['id'] AND $_GET['action'] == 'modify') {
	
	$menus = $menu->getMenu($_GET['id'],array('returnItems'=>1));
	$menus['title'] ="";
	$smarty->assign("menu",$menus);
	
	$s = new siteModules();
	$layout = $s->pageBoxes("content",e_FILE,'back',array('getBoxes'=>'full','boxFields'=>'id,title,name,settings'));
	$smarty->assign("layout",$layout);
	
	$m = array();
	foreach ($loaded_modules as $key => $value ) {
		if ( $value['is_item'] == 1 ) {
			$m[]=$key;
		}
 	}
 	$smarty->assign("modules",$m);
	
	$tpl="admin2/menu/menusEditor.tpl";
} else {  //END MODIFY MENU
	$tpl="admin2/menu/menuEditor.tpl";
}

$smarty->assign("menus",$menu->getMenu(0,array('num_items'=>1,'debug'=>0)));
$smarty->assign("new_languages",get_all_countries());//assigned template variable codes
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file",$tpl);
$smarty->display(CURRENT_SKIN."/home.tpl");

?>