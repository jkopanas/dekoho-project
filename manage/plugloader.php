<?php
include("init.php");//load from manage!!!!

//---INITIATIONS-----
global $smarty, $plugins;

//---SECURITY SWITCH-----
if (!is_array($plugins[$_REQUEST['fetch']]) ) {
	exit("No plugin were loaded - plugin Does not exist");
}
//---/SECURITY SWITCH-----

$current_plugin = $plugins[$_REQUEST['fetch']]; // The requested Plugin
//---/INITIATIONS-----

// In case the Plugin has a boxView installed. This is a usefull method to be used
// as boxViews are designed to be fetched with AJAX.
if(isset($_REQUEST['boxview']) && (strlen($_REQUEST['boxview'])>0))
{
	$s = new siteModules();
	$smarty->assign("submenu", $current_plugin['name']);
	$boxView = $s->getBoxView($_REQUEST['boxview']);
	
	$smarty->assign("viewname",$boxView['boxview']);
	if(isset($boxView['settings']['jsIncludes']))
	{
		$smarty->assign("jsIncludes",$boxView['settings']['jsIncludes']);
	}
	if(isset($boxView['settings']['boxViewTemplate']))
	{
		$smarty->assign("boxViewTemplate",$boxView['settings']['boxViewTemplate']);
	}
	$fetcfingPage = "../".$boxView['file'];
	
	
	// if a request is passed to pluloader.php then it shold be a synchronous call
	// with a fetch of a specified boxView.
	// To Support Asynchronous Boxviews requests shuld be send to /ajax/loader?view=..&boxID=..
// 	$smarty->assign("PLUGIN_NAME", 			$current_plugin['name']);
// 	$smarty->assign("PLUGIN_CORE", 			"/plugins/".$current_plugin['path']);
// 	$smarty->assign("PLUGIN_AJAX_CORE", 	"/ajax/plugins/".$current_plugin['path']);
// 	$smarty->assign("menu",					$current_plugin['name']);
// 	$smarty->assign("page_title",			SITE_NAME." Administration | Plugin | ".$current_plugin['name']);
// 	$smarty->assign("fetcfingPage",			$fetcfingPage);
// 	$smarty->assign("include_file",			$fetcfingPage);
// 	exit();
}
else
{
	$smarty->assign("viewname",$current_plugin['name']);
	$fetcfingPage = "../plugins/".$current_plugin['tpl'];
}
HookParent::getInstance()->doTriggerHookGlobally("beforePlugLoad".$current_plugin['name']);
$smarty->assign("PLUGIN_NAME", 			$current_plugin['name']);
$smarty->assign("PLUGIN_CORE", 			"/plugins/".$current_plugin['path']);
$smarty->assign("PLUGIN_AJAX_CORE", 	"/ajax/plugins/".$current_plugin['path']);
$smarty->assign("menu",					$current_plugin['name']);
$smarty->assign("page_title",			SITE_NAME." Administration | Plugin | ".$current_plugin['name']);
$smarty->assign("fetcfingPage",			$fetcfingPage);
$smarty->assign("include_file",			$fetcfingPage);

$smarty->display(CURRENT_SKIN."/home.tpl");
exit();

/*******************************************************************
* O => YOU ARE HERE
*
* ViewContrl -> View Controler; controler.js
* View -> the View of the box; tpl render
* ModContrl -> the modular_box controler; loader.php
* Module -> the modular_box; The php file of the box
*
* A View request is been answered, as the view asks for its data.
* Data are been processed by the modular box and returned to view
* witch should render the fetched json.
*
* 		ViewContrl			View		ModContrl			Module
*---------------------------------------------------------------------------
*-REQVIEW-->_I_				 |				 |				 |
* 			| |	Call View	 |				 |				 |
* 			| |---------------------------->_I_				 |
* 							 | Smarty Displ |O|Fetch view	 |
* 							_I_<------------|O|				 |
* 		Ajax Display		| |				 |				 |
* <-------------------------| |				 |				 |
* 							 |				 |				 |
* 							_I_				 |				 |
*  							| |Call loader	 |				 |
*  							| |------------>_I_				 |
*  							 |				| |Ajax-Load	 |
* 							 |				| |------------>_I_
* 							 |					Return Data	| | Fetch Data
* 							_I_<----------------------------| |
* 							| |Render Data
* 			Display Data	| |
* <-------------------------| |
*
* A Request is been handled, as the command should be proccesed
* and return its state.
*
* -REQ-CMD---Module+Comonent+RequestCMD---->___
* 											| |Ajax-Load
* 											| |------------>___
* 												Return Data	| | Process CMD
* 							_I_<----------------------------| | & Fetch Data
* 							| |Render Data
* 			Display Data	| |
* <-------------------------| |
*
*******************************************************************/
?>