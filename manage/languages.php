<?php
include("init.php");
####################### PAGINATION DATA ##################################################
if ($_GET["page"]!=""):		$page	= $_GET["page"];	else:	$page	= 1;endif;
define ('CURRENT_PAGE',$page);
$smarty->assign("PAGE",$page);//assigned template variable PAGE
###################### END OF PAGINATION DATA ###########################################
$language = ($_GET['language']) ? $_GET['language'] : $_POST['language'];
$countryCode = ($_GET['code']) ? $_GET['code'] : $_POST['code'];
$Languages = new Languages();


$smarty->assign("AllCountryCodes",$Languages->getAllCountries(BACK_LANG));
$smarty->assign("AvailableLanguages",$Languages->AvailableLanguages(array()));
$smarty->assign("countryCode",$countryCode);

if ($countryCode) {
	$smarty->assign("selectedCountry",$Languages->get_country($countryCode));
}

$smarty->assign("new_languages",get_all_countries());//assigned template variable codes
$smarty->assign("page_title",SITE_NAME." Administration");

$smarty->assign("include_file","admin2/languages.tpl");
$smarty->display(CURRENT_SKIN."/home.tpl");

?>