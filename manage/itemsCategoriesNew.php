<?php
include("init.php");//load from manage!!!!

if (array_key_exists($_GET['module'],$loaded_modules)) {
	
	$current_module = $loaded_modules[$_GET['module']];
	$smarty->assign("current_module",$current_module);
	$c = new Items(array('module'=>$loaded_modules[$_GET['module']]));
	$categories = $c->recurseCategories(0,0,array('num_subs'=>1));
	$themes = get_themes();
	$smarty->assign("catItems",json_encode($categories));
	$smarty->assign("themes",$themes);
	$smarty->assign("themesEnc",json_encode($themes));
	$p = new SettingsManager();
	$pageSettings = $p->getSettings($module,basename('itemsCategories.php'),array('debug'=>0));
	$smarty->assign("pageSettings",$pageSettings);
	$smarty->assign("pageSettingsEnc",json_encode($pageSettings));
//	$obj['allCategories'] = $c->AllItemsTreeCategories();
	$obj['allCategories'] = $categories;
	$sql->db_Select($_GET['module'],'id,title,permalink','ORDER BY id DESC LIMIT 50',$mode="no_where");
	if ($sql->db_Rows()) {
		$obj['allPages'] = execute_multi($sql);
	}
	$obj['featured'] = $c->FeaturedContent(0,array('get_results'=>1,'active'=>0,'debug'=>0,'thumb'=>0,'orderby'=>'orderby','cat_details'=>1));
	$smarty->assign("arr",json_encode($obj));
}

$smarty->assign("include_file","common/categories/home.tpl");
$smarty->display(CURRENT_SKIN."/home.tpl");
?>