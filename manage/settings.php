<?php
include("init.php");
include(ABSPATH."/includes/xajax/xajax.inc.php");
$xajax = new xajax();
define ('XAJAX_DEFAULT_CHAR_ENCODING', 'ISO-8859-7' );
$xajax->registerFunction("processForm");
$language = ($_GET['language']) ? $_GET['language'] : $_POST['language'];
$form_mode = ($_GET['mode']) ? $_GET['mode'] : $_POST['mode'];
$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];
$cat = ($cat == "") ? "general" :$cat;



function processForm($aFormValues)
{
	global $smarty,$lang;

if ($aFormValues['mode'] == "modify") 
{
		include("../includes/ConvertCharset.class.php");
	$NewEncoding = new ConvertCharset;
$sql = new db();
$t = new textparse();
		foreach ($aFormValues as $key => $val)
		{
		if (strstr($key,"-")) 
		{
			list($field,$name)=split("-",$key);
			$query = "value = '".$t->formtpa($val)."' WHERE name = '$field'";
			$sql->db_Update("config",$query);
		}//END OF IF
		}//END OF WHILE  
$smarty->assign("mode_save",1);//assigned template variable mode_save
$smarty->assign("message",$lang['settings_saved']);//assigned template variable mode_save
$smarty->assign("page_title",SITE_NAME." Administration");
}

    $objResponse = new xajaxResponse("iso-8859-7");
    $dialog = $smarty->fetch('admin/message_dialog.tpl');
    $objResponse->addPrepend("SomeElementId","innerHTML",$dialog);
    $objResponse->addAssign("submitButton","value",$lang['save']);
    $objResponse->addAssign("submitButton","disabled",false);
    return $objResponse;
}

$xajax->processRequests();
$smarty->assign("ajax_requests",$xajax->printJavascript(URL));
$smarty->assign("menu","settings");
$smarty->assign("submenu",$cat);
$smarty->assign("cat",$cat."_settings");//assigned template variable cat
$smarty->assign("imanager",1);//assigned template variable imanager
$smarty->assign("group_settings",group_prefs());//assigned template variable group_settings
$smarty->assign("settings",load_prefs(1,$cat));//assigned template variable translations
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","admin/settings.tpl");
$smarty->display("admin/home.tpl");

?>