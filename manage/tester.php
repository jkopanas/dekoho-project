<?php
include("init.php");
$a = MediaFiles();
foreach ($a as $v) {
	$files[$v['title']] = $v;
}
$current_module = $loaded_modules['content'];
$id = 58;

$i = new ItemImages(array('module'=>$current_module));
$images = $i->GetItemDetailedImages($id,null);
$Docs = new ItemMedia(array('module' => $current_module,'itemid' => $id,'debug'=>0,'orderby' => 'orderby'));
$smarty->assign("image_types",get_image_types($current_module['id']));
$smarty->assign("images",$images);
$smarty->assign("data",json_encode(array('sizes'=>get_image_types(1),'image'=>array('images'=>$images,'documents'=>$Docs->ItemDocuments(),'videos'=>$Docs->ItemVideos()))));

$smarty->assign("fileTypes",json_encode($files));
$smarty->assign("include_file","admin2/tester.tpl");
$smarty->display(CURRENT_SKIN."/home.tpl");
?>