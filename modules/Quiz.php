<?php

class Quiz extends Items {
	
	public $settings;
	public $module;
	public $itemid;
	
    function Quiz($settings) {
		$this->itemid = $settings['itemid'];
		$this->module = $settings['module'];
		$this->settings = $settings;
		
		
    }//END FUNCTION
    
    function GetQuiz($id,$settings=0)
    {
    	global $sql;
    	$fields[] = ($settings['fields']) ? $settings['fields'] : "*";
    	$tables[] = "quiz";
		if ($settings['num_questions']) {
			$fields[] = "(SELECT count(id) FROM quiz_questions WHERE quiz_id = $id) as num_questions";
		}
    	$q[] = "id = $id";
    	if ($settings['active']) {
    		$q[] = "active = ".$settings['active'];
    	}
    	if ($settings['GetItemId']) {
    		$tables[] = " INNER JOIN quiz_items ON (quiz.id=quiz_items.quiz_id)";
    		$fields[] = "quiz_items.itemid";
    	}
    	$query = implode(" AND ",$q);
    	$fields_q = implode(" , ",$fields);
    	$tables_q = implode("  ",$tables);
    	if ($settings['debug']) {
    		echo "SELECT $fields_q FROM $tables_q WHERE $query<br>";
    	}
    	$sql->db_Select($tables_q,$fields_q,"$query");
    	if ($sql->db_Rows()) {
    		$res = execute_single($sql);
    		$res['settings'] = form_settings_array($res['settings']);
	    	if ($settings['getQuestions']) {
	    		$res['questions'] = $this->GetQuestions($id,$settings);
	    	}//END GET QUESTIONS
	    	if ($settings['getResults']) {
				$res['results']	= $this->GetResults($id,$settings);
	    	}
    	}//END ROWS
		return $res;
    }//END FUNCTION
    
    function GetQuizes($settings=0)
    {
    	global $sql;
    	$tables = "quiz";
    	$fields = ($settings['fields']) ? $settings['fields'] : "$tables.*";
    	
		if ($settings['num_questions']) {
			$fields .= ",(SELECT count(id) FROM quiz_questions WHERE quiz_id = quiz.id) as num_questions";
		}
    	$q[] = "quiz.module = '".$this->module['name']."'";
    	if ($settings['itemid']) {
    		$tables .= " INNER JOIN quiz_items ON (quiz.id = quiz_items.quiz_id)";
    		$q[] = "itemid = ".$settings['itemid'];
    	}
    	if ($settings['active']) {
    		$q[] = "active = ".$settings['active'];
    	}
    	$query = implode(" AND ",$q);
    	
    	if ($settings['sort']) {
    		$query .= " ORDER BY ".$settings['sort'];
    	}
    	if ($settings['way']) {
    		$query .= " ".$settings['way'];
    	}
    	
    	if ($settings['debug']) {
    		echo "SELECT $fields FROM $tables WHERE $query<br>";
    	}
    	$sql->db_Select($tables,$fields,"$query");
    	if ($sql->db_Rows()) {
    		$res = execute_multi($sql);
    		
	    	if ($settings['getQuestions']) {
	    		for ($i=0;count($res)>$i;$i++)
	    		{
	    			$res[$i]['questions'] = $this->GetQuestions($res[$i]['id'],$settings);
	    		}
	    		
	    	}//END GET QUESTIONS
    	}//END ROWS
		return $res;
    }//END FUNCTION
	
    function GetResults($quiz_id,$settings=0)
    {
    	global $sql;
    	$tables = "quiz_results";
    	$fields = ($settings['fields']) ? $settings['fields'] : "$tables.*";
    	
    	$q[] = "quiz_id = $quiz_id";
    	$query = implode(" AND ",$q);
    	if ($settings['sort']) {
    		$query .= " ORDER BY ".$settings['sort'];
    	}
    	if ($settings['way']) {
    		$query .= " ".$settings['way'];
    	}
    	if ($settings['debug']) {
    		echo "SELECT $fields FROM $tables WHERE $query<br>";
    	}
    	$sql->db_Select($tables,$fields,$query);
    	
    	if ($sql->db_Rows()) {
    		$res = execute_multi($sql);
    		
    		for ($i=0;count($res) > $i;$i++)
    		{
    			$res[$i]['settings'] = form_settings_array($res[$i]['settings']);
    		}
    		
    	}//END ROWS
    	return $res;
    }//END FUNCTION
    
    function GetQuestion($question_id,$settings=0)
    {
    	global $sql;
    	$fields = ($settings['fields']) ? $settings['fields'] : "*";
		if ($settings['num_answers']) {
			$fields .= ",(SELECT count(id) FROM quiz_answers WHERE question_id = $question_id) as num_answers";
		}
    	$q[] = "id = $question_id";
    	
    	$query = implode(" AND ",$q);
    	if ($settings['debug']) {
    		echo "SELECT $fields FROM quiz_questions WHERE $query<br>";
    	}
    	$sql->db_Select("quiz_questions",$fields,"$query");
    	if ($sql->db_Rows()) {
    		$res = execute_single($sql);
    		$res['settings'] = form_settings_array($res['settings']);
	    	if ($settings['getAnswers']) {
	    		$res['questions'] = $this->GetQuestions($question_id,$settings);
	    	}//END GET QUESTIONS
    	}//END ROWS
		return $res;
    	
    }//END FUNCTION
    
    function GetQuestions($quiz_id,$settings=0)
    {
    	global $sql;
    	$fields = ($settings['fields']) ? $settings['fields'] : "*";

    	$q[] = "quiz_id = $quiz_id";
    	
    	$query = implode(" AND ",$q);
    	if ($settings['sort']) {
    		$query .= " ORDER BY ".$settings['sort'];
    	}
    	if ($settings['way']) {
    		$query .= " ".$settings['way'];
    	}

    	if ($settings['debug']) {
    		echo "SELECT $fields FROM quiz_questions WHERE $query<br>";
    	}
    	$sql->db_Select(" quiz_questions",$fields,"$query");
    	if ($sql->db_Rows()) {
    		$res = execute_multi($sql);
	    	for ($i=0;count($res) > $i;$i++)
    		{
    			$res[$i]['settings'] = form_settings_array($res[$i]['settings']);
	    	if ($settings['getAnswers']) {

    				$res[$i]['answers'] = $this->GetAnswers($quiz_id,$res[$i]['id']);
	    	}//END GET ANSWERS
	    	}
    	}//END ROWS
		return $res;
    }//END FUNCTION
    
    function GetAnswers($quiz_id,$question_id,$settings=0)
    {
    	global $sql;
    	$fields = ($settings['fields']) ? $settings['fields'] : "*";
		$orderby = ($settings['orderby']) ? $settings['orderby'] : "orderby";
		$way = ($settings['way']) ? $settings['way'] : "ASC";
		
    	$q[] = "quiz_id = $quiz_id";
    	$q[] = "question_id = $question_id";
    	
    	$query = implode(" AND ",$q);
    	if ($settings['debug']) {
    		echo "SELECT $fields FROM quiz_answers WHERE $query<br>";
    	}
    	$sql->db_Select("quiz_answers",$fields,"$query");
    	if ($sql->db_Rows()) {
    		$res = execute_multi($sql);
    		

    	}//END ROWS
		return $res;
    }//END FUNCTION
    
    function CheckAnswers($quiz,$UserAnswers,$settings=0)
    {
    	global $sql;
    	$allQuestions = $quiz['questions'];
    	if ($allQuestions AND $UserAnswers) {
    		
    	foreach ($allQuestions as $v)
    	{
    		$QuestionsById[$v['id']] = $v;
    		$total_points[] = $v['points'];
    		foreach ($v['answers'] as $k=>$val)
    		{
    			$allAnswers[$val['id']] = $val;
    			if ($val['correct']) {
    				$correctValues[$v['id']] = $val['id'];
    			}
    		}
    	}//END LOOP RESULTS
    	$res['correct'] = array_intersect($UserAnswers,$correctValues);
    	$res['wrong'] = array_diff($UserAnswers,$correctValues);
    	foreach ($res['correct'] as $k=>$v)
    	{
    		$points[] = $QuestionsById[$k]['points'];
    	}
    	
    	$res['points'] =(is_array($points)) ? array_sum($points) : 0;
    	$res['total_points'] = array_sum($total_points);
    	
    	if ($settings['ShowResults']) {
    		$percent = (count($res['correct'])/count($allQuestions))*100;
    		$sql->db_Select("quiz_results","*","points_from <= ".round($percent)." AND points_to >= ".round($percent)." AND quiz_id = ".$quiz['id']);
    		if ($sql->db_Rows()) {
    			$res['result'] = execute_single($sql);
    		}
    	}
    	return $res;
    	}//END IF 
    }//END FUNCTION
    
    function CheckUserScores($uid,$settings=0)
    {
    	global $sql;
    	$fields = ($settings['fields']) ? $settings['fields'] : "*";
    	$tables = "quiz_scores";
    	
		$q[] = "uid = $uid";    	
    	if ($settings['quiz_id']) {
    		$q[] = "quiz_id = ".$settings['quiz_id'];
    	}
    	
    	$orderby = ($settings['orderby']) ? $settings['orderby'] : "date_added";
		$way = ($settings['way']) ? $settings['way'] : "DESC";
		
    	$query = implode(" AND ",$q);
    	if ($settings['debug']) {
    		echo "SELECT $fields FROM $tables WHERE $query<br>";
    	}
    	$sql->db_Select($tables,$fields,$query);
    	if ($sql->db_Rows()) {
    		if ($settings['quiz_id']) {
    			$res = execute_single($sql);
    			$res['settings'] = form_settings_array($res['settings']);
    			if ($res['answers_detailed']) {
    				$res['answers_detailed'] = json_decode($res['answers_detailed']);
    			}
    		}//END SINGLE
    		else {
    			$res = execute_multi($sql);
    			for ($i=0;count($res); $res++)
    			{
	    			$res[$i]['settings'] = form_settings_array($res[$i]['settings']);
	    			if ($res[$i]['answers_detailed']) {
	    				$res[$i]['answers_detailed'] = json_decode($res[$i]['answers_detailed']);
	    			}
	    			
    			}
    		}//END MANY
    		
    		return $res;
    	}//END ROWS
    	
    }//END FUNCTION
    
}//END CLASS

?>