<?php
include("../../../manage/init.php");//load from manage!!!!

if ($gallery_module = module_is_active("gallery",1,1,1)) 
{
	$module_path = URL."/".$gallery_module['folder']."/admin";
	$smarty->assign("latest_gallery",get_latest_gallery($gallery_module['settings']['latest_gallery_admin'],DEFAULT_LANG));
	$smarty->assign("MODULE_FOLDER",$module_path);
	
	$t = new textparse();

	
	
########################## UPDATE gallery ##################################################################################
if ($_POST['action'] == "add") 
{
	$required_fields = array('gallery_title');

	//STEP 1: grab POST variables into a formated array
 $posted_data = array();
 foreach ($_POST as $key => $value) 
 {   
	 $posted_data[$key] = $value;
 }//END OF FOREACH
 //check required fields
 for ($i=0;count($posted_data) > $i;$i++)
 {
 	for ($j=0;count($required_fields) > $j;$j++)
 	{
 		if ($posted_data[$required_fields[$j]] == "") 
 		{  
 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
 	 		$error = 1;
 		}//END OF IF
 	}//END OF FOR

 }//END OF FOR

 ##### IF NO ERRORS PROCEED #####
 if ($error != 1) 
 {
//Database insertions
//Add the basic gallery details
$categoryid = $_POST['categoryid'];
$extra_categories = $_POST['categoryids'];
$sku = $_POST['sku'];
$orderby = $_POST['orderby'];
$active = $_POST['active'];
$orderby = $_POST['orderby'];
$description = $t->formtpa($_POST['short_desc']);
$long_description = $t->formtpa($_POST['long_desc']);
$meta_desc = $t->formtpa($_POST['meta_desc']);
$meta_keywords = $t->formtpa($_POST['meta_keywords']);
$date_added = time();
$image = ($_POST['image']) ? $_POST['image'] : $gallery_module['settings']['default_thumb'];
$title = $t->formtpa($_POST['gallery_title']);
//gallery_r($_POST);//END OF gallery_r
$sql->db_Insert("gallery","'','$sku','$image','$date_added','$active','".ID."','$orderby'");
$id = mysql_insert_id();
$sql->db_Insert("gallery_lng","'".DEFAULT_LANG."','$id','$title','$description','$long_description',
'$meta_desc','$meta_keywords'");
//TRANSLATIONS

if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
$countries = get_countries(2,$trans="yes");
for ($i=0;count($countries) > $i;$i++)
{
$sql->db_Insert("gallery_lng","'".$countries[$i]['code']."','$id','$title','$description','$long_description',
'$meta_desc','$meta_keywords'");
}//END OF FOR
}//END OF IF


//SECONDARY UPDATES
//gallery CATEGORIES
	$sql->db_Insert("gallery_page_categories","'$categoryid','$id','Y'");

//See if there are any extra categories to insert
if (!empty($extra_categories)) 
{  
 for ($i=0;count($extra_categories) > $i;$i++)
 {
 	$sql->db_Insert("gallery_page_categories","'".$extra_categories[$i]."','$id','N'");
 }//END OF FOR
}//END OF IF

//See if there are any extra categories to insert
//first drop the old ones
$sql->db_Delete("gallery_page_categories","galleryid = '$id' AND main = 'N'");
if (!empty($extra_categories)) 
{  
 for ($i=0;count($extra_categories) > $i;$i++)
 {
 	$sql->db_Insert("gallery_page_categories","'".$extra_categories[$i]."','$id','N'");
 }//END OF FOR
}//END OF IF

//add extra fields
add_gallery_extra_field_values($_POST,$id,DEFAULT_LANG);
if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
for ($i=0;count($countries) > $i;$i++)
{
add_gallery_extra_field_values($_POST,$id,$countries[$i]['code']);
}//END OF FOR
}//END OF IF

//ADD TAGS
$tags = construct_tags_array($_POST['tags'],",");
//insert new tags
for ($i=0;count($tags) > $i;$i++)
{
	$tag = $tags[$i];
	$sql->db_Insert("gallery_tags","'','$id','$tag'");
}
header("Location: $module_path/gallery_modify.php?id=$id");
exit();
 }//END OF NO ERRORS
 $smarty->assign("error_list",$error_list);//assigned template variable error_list<BR>
}

	
################################################## LOAD gallery FEATURES ############################################################
	$gallery = get_gallery($id,DEFAULT_LANG);
	

	$smarty->assign("allcategories",get_all_gallery_categories(DEFAULT_LANG));//assigned template variable allcategories
	$smarty->assign("extra_fields",get_extra_fields_gallery("Y",DEFAULT_LANG));


	############# CHECK OUT FOR MANUFACTURERS MODULE ###############################

	if (module_is_active($manufacturers_module = "manufacturers",1,1)) 
	{
		$smarty->assign("manufacturers_list",get_manufacturers(DEFAULT_LANG));
		$smarty->assign("manufacturers_module",$manufacturers_module);
	}
	############# END OF  MANUFACTURERS MODULE #####################################
	
	############# CHECK OUT FOR ESHOP MODULE #######################################
	
	if (module_is_active($eshop_module = "eshop",1,1)) 
	{
		$smarty->assign("eshop",1);
		$smarty->assign("eshop_module",$eshop_module);
	}
	
	//Locations for real estate
	if ($locations_module = module_is_active("locations",1,1)) 
	{
		$gallery['locid'] = locations_gallery_location($id);
		$smarty->assign("locations",1);
		$smarty->assign("locations_module",$locations_module);
		$smarty->assign("all_locations",get_all_locations(DEFAULT_LANG));
	}
	
	if ($_GET['cat']) 
	{
		$tmp_cat['catid']=$_GET['cat'];
		$smarty->assign("gallery",$tmp_cat);
		
	}
	editor("description","description1",0,0,0,$editor_settings);
	editor("long","long1",0,0,0,$editor_settings);
	$smarty->assign("gallery_module",$gallery_module);
	$smarty->assign("action","add");
################################################ END OF LOAD gallery #########################################
}//END OF LOAD MODULE



$smarty->assign("USE_AJAX",$gallery_module['folder']."/admin/ajax_functions.tpl");
$smarty->assign("menu",$gallery_module['name']);
$smarty->assign("submenu","add");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/gallery/admin/gallery_modify.tpl");
$smarty->display("admin/home.tpl");

?>