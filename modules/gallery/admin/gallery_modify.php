<?php
include("../../../manage/init.php");//load from manage!!!!

if ($gallery_module = module_is_active("gallery",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$gallery_module['folder']."/admin");
	
	$t = new textparse();
	$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
	
	
########################## UPDATE gallery ##################################################################################
if ($_POST['action'] == "update") 
{
	$required_fields = array('gallery_title');

	//STEP 1: grab POST variables into a formated array
 $posted_data = array();
 foreach ($_POST as $key => $value) 
 {   
	 $posted_data[$key] = $value;
 }//END OF FOREACH
 //check required fields
 for ($i=0;count($posted_data) > $i;$i++)
 {
 	for ($j=0;count($required_fields) > $j;$j++)
 	{
 		if ($posted_data[$required_fields[$j]] == "") 
 		{  
 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
 	 		$error = 1;
 		}//END OF IF
 	}//END OF FOR

 }//END OF FOR

 ##### IF NO ERRORS PROCEED #####
 if ($error != 1) 
 {
//Database insertions
//Add the basic gallery details
$categoryid = $_POST['categoryid'];
$extra_categories = $_POST['categoryids'];
$sku = $_POST['gallery_sku'];
$active = $_POST['active'];
$orderby = $_POST['orderby'];
$description = $t->formtpa($_POST['short_desc']);
$long_description = $t->formtpa($_POST['long_desc']);
$meta_desc = $t->formtpa($_POST['meta_desc']);
$meta_keywords = $t->formtpa($_POST['meta_keywords']);
$title = $t->formtpa($_POST['gallery_title']);
$orderby = $_POST['orderby'];

//gallery_r($_POST);//END OF gallery_r
$query = "sku = '$sku', active = '$active', orderby = $orderby WHERE id = $id";
$sql->db_Update("gallery",$query);
//echo $query."<br><br>";
$query = "title = '$title',description = '$description',full_description = '$long_description', meta_desc = '$meta_desc', 
meta_keywords = '$meta_keywords' WHERE galleryid = $id AND code = '".DEFAULT_LANG."'";
//echo $query."<br><br>";
$sql->db_Update("gallery_lng",$query);

//SECONDARY UPDATES

//gallery CATEGORIES
$sql->db_Select("gallery_page_categories","catid","galleryid = $id AND catid = $categoryid AND main = 'Y'");

if ($sql->db_Rows() > 0) //UPDATE
{
	$sql->db_Update("gallery_page_categories","catid = $categoryid WHERE galleryid = $id AND main = 'Y'");
}
else 
{
	$sql->db_Insert("gallery_page_categories","'$categoryid','$galleryid','Y'");
}


//Add main category
$sql->db_Update("gallery_page_categories","catid = '$categoryid' WHERE main = 'Y' AND galleryid = '$id'");
//See if there are any extra categories to insert



if ($extra_categories) 
{  
	//first drop the old ones
$sql->db_Delete("gallery_page_categories","galleryid = '$id' AND main = 'N'");
 for ($i=0;count($extra_categories) > $i;$i++)
 {
 	if ($extra_categories[$i] != "")
 	{
 		$sql->db_Insert("gallery_page_categories","'".$extra_categories[$i]."','$id','N'");
 	}
 	
 }//END OF FOR
}//END OF IF

//add extra fields
upd_gallery_extra_field_values($_POST,$id,DEFAULT_LANG);
if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
for ($i=0;count($countries) > $i;$i++)
{
upd_gallery_extra_field_values($_POST,$id,$countries[$i]['code']);
}//END OF FOR
}//END OF IF

//UPDATE TAGS
$tags = construct_tags_array($_POST['tags'],",");
//Clear old tags
$sql->db_Delete("gallery_tags","galleryid = $id");
//insert new tags
for ($i=0;count($tags) > $i;$i++)
{
	$tag = $tags[$i];
	$sql->db_Insert("gallery_tags","'','$id','$tag'");
}

############# CHECK OUT FOR ESHOP MODULE #######################################
	if ($eshop_module = module_is_active("eshop",1,0)) 
	{
		$price = $_POST['price'];
		$list_price = $_POST['list_price'];
		
$sql->db_Select("eshop_product_details","id","galleryid = $id");
if ($sql->db_Rows() > 0) 
{
	$sql->db_Update('eshop_product_details',"price = '$price',list_price = '$list_price' WHERE galleryid = $id");
}
else 
{
		$sql->db_Insert("eshop_product_details","'','$id','$price','$list_price','$quantity','$membership','$distribution','$weight',
		'$avail','$sales_stats','$views_stats','$del_stats','$shipping_freight','$free_shipping','$discount_avail','$min_amount',
		'$low_avail_limit','$free_tax','image'");
}
		
	}
	
//Locations STUFF
	if (module_is_active($locations_module = "locations",1,0)) 
	{
		$locid = $_POST['locid'];
		$gallery_loc = locations_gallery_location($id);
		if ($gallery_loc) 
		{
			$sql->db_Update("gallery_locations","id = '$locid' WHERE galleryid ='$id'");
		}
		else 
		{
			$sql->db_Insert("gallery_locations","'$locid','$id'");
		}
		
	}//END OF MODULE

$smarty->assign("updated",1);
 }//END OF NO ERRORS
 $smarty->assign("error_list",$error_list);//assigned template variable error_list<BR>
}

	
################################################## LOAD gallery ############################################################
	$gallery = get_gallery($id,DEFAULT_LANG,0,1);

	
	if (!empty($gallery)) 
	{  
	$sql->db_Select("gallery_page_categories","catid","galleryid = $id AND main = 'N'");
	$all_gallery_categories = execute_multi($sql);
	$smarty->assign("categoryids",$all_gallery_categories);//assigned template variable categories
	$smarty->assign("allcategories",get_all_gallery_categories(DEFAULT_LANG));//assigned template variable allcategories
	$smarty->assign("more_gallery",get_latest_gallery($gallery_module['settings']['latest_gallery'],DEFAULT_LANG,"no",$gallery['catid']));
	$smarty->assign("extra_fields",get_extra_fields_gallery("Y",DEFAULT_LANG,$id,$gallery['catid']));
	
	$smarty->assign("id",$id);
	$smarty->assign("tags",get_gallery_tags($id,","));
	$smarty->assign("nav",gallery_cat_nav($gallery['catid'],DEFAULT_LANG));

	if ($manufacturers_module = module_is_active("manufacturers",1,1)) 
	{
		$smarty->assign("manufacturers_list",get_manufacturers(DEFAULT_LANG));
		$smarty->assign("manufacturers_module",$manufacturers_module);
		$gallery['mid'] = get_gallery_manufacturer($id);
	}
	############# END OF  MANUFACTURERS MODULE #####################################
	
	############# CHECK OUT FOR ESHOP MODULE #######################################
	
	if ($eshop_module = module_is_active("eshop",1,1)) 
	{
		$gallery['details'] = eshop_product_details($id,'image');
		$smarty->assign("eshop",1);
		$smarty->assign("eshop_module",$eshop_module);
	}
	
	//Locations for real estate
	if ($locations_module = module_is_active("locations",1,1)) 
	{
		$gallery['locations'] = locations_gallery_location($id);
		$smarty->assign("locations",1);
		$smarty->assign("locations_module",$locations_module);
		$smarty->assign("all_locations",get_all_locations(DEFAULT_LANG));
	}

	editor("description","description1",0,0,0,$editor_settings);
	editor("long","long1",0,0,0,$editor_settings);
	$smarty->assign("gallery",$gallery);
	$smarty->assign("gallery_module",$gallery_module);
	$smarty->assign("action","update");
	}//END OF gallery found
	else 
	{  
		header("Location: search.php"); 
		exit();
	}//END OF ELSE

################################################ END OF LOAD gallery #########################################
}
//gallery_r($gallery);

$smarty->assign("USE_AJAX",$gallery_module['folder']."/admin/ajax_functions.tpl");
$smarty->assign("menu",$gallery_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","modify");//USED ON ADDITIONAL MENUS
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/gallery/admin/gallery_modify.tpl");
$smarty->display("admin/home.tpl");

?>