<?php
include("../../../manage/init.php");//load from manage!!!!

if ($gallery_module = module_is_active("gallery",1,1)) 
{
	$module_path = URL."/".$gallery_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",$module_path);
$t = new textparse();
$page = $_POST['page'];
################################################ MASS ACTIVATE/DEACTIVATE ###################################
if ($_POST['mode'] == "deactivate") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			if ($val == 1)//eliminates the check all/none box 
			{
				$sql->db_Update("gallery","active = 0 WHERE id = $key");				
			}
		}//END OF WHILE

	if (preg_match("search",$page)) {
			header("Location:$page?search=1");
		}
		else {
				header("Location:$page");
			}	
	
}

if ($_POST['mode'] == "activate") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			if ($val == 1)//eliminates the check all/none box 
			{
				$sql->db_Update("gallery","active = 1 WHERE id = $key");				
			}
		}//END OF WHILE

		if (preg_match("search",$page)) {
			header("Location:$page?search=1");
		}
		else {
				header("Location:$page");
			}	
}
################################################ END MASS ACTIVATE/DEACTIVATE ###############################

################################################ MASS DELETE ###############################
if ($_POST['mode'] == "delete") 
{
	
		foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			if ($val == 1)//eliminates the check all/none box 
			{
	$gallery_details = get_gallery($id,DEFAULT_LANG);
	$categoryid = $gallery_details['catid'];  
//delete the gallery
$sql->db_Delete("gallery","id = $id"); 
//delete translations
$sql->db_Delete("gallery_lng","galleryid = $id"); 
//delete gallery links
$sql->db_Delete("gallery_links","gallery_source = $id"); 
$sql->db_Delete("gallery_links","gallery_dest = $id"); 
//delete from any category
$sql->db_Delete("gallery_page_categories","galleryid = $id"); 
//delete from any category
$sql->db_Delete("gallery_locations","galleryid = $id"); 
//delete from images
$sql->db_Delete("gallery_images","itemid = $id"); 
$sql->db_Delete("gallery_images_aditional","itemid = $id"); 
//delete from featured gallery
$sql->db_Delete("featured_gallery","galleryid = $id"); 
//delete from extra fields
$sql->db_Delete("gallery_extra_field_values","galleryid = $id"); 
//delete from classes
$sql->db_Delete("gallery_class_gallery","galleryid = $id"); 
//delete from bookmars
$sql->db_Delete("bookmars","galleryid = $id");
//Delete from feeds
$sql->db_Delete("feeds","galleryid = $id");
//Subtract from gallery count
$sql->db_Update("gallery_categories","gallery_count = gallery_count-1 WHERE categoryid = '$categoryid'");
//delete recipe features
//ALL DONE			
$gallery_dir = $_SERVER['DOCUMENT_ROOT'].$gallery_module['settings']['images_path']."/category_$categoryid/gallery_$id/";
//echo $gallery_dir."<Br>";
recursive_remove_directory($gallery_dir);
			}
		}//END OF WHILE

		if (preg_match("search",$page)) {
			header("Location:$page?search=1");
			exit();
		}
		else {
				header("Location:".$module_path."/$page");
				exit();
			}	
}

################################################ END MASS DELETE ###############################

############################################### MASS FIRST ACTIVATION ##############################3
if ($_POST['mode'] == "first_activate") 
{
		foreach ($_POST as $key => $val)
		{
					if (strstr($key,"-")) 
					{
						
						list($field,$code)=split("-",$key);
						if ($field == "gallery_title") 
						{
							$val = $t->formtpa($val);
							$sql->db_Update("gallery_lng","title = '$val' WHERE galleryid = $code");
							
						}
						elseif ($field == "categoryid")
						{
							$sql->db_Update("gallery_page_categories","catid = $val WHERE galleryid = $code AND main = 'Y'");
						}
						else 
						{
							$sql->db_Update("gallery","$field = '$val' WHERE id = $code");
							
						}
						
					}
		}
	
	foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			
			if ($val == 1)//eliminates the check all/none box 
			{
					$sql->db_Update("gallery","active = 0 WHERE id = $code");
			}
		}
		//clear up
		header("Location:".$module_path."/$page");
		exit();
}

############################################### MASS EDIT ##############################3
if ($_POST['mode'] == "mass_edit") 
{
	print_r($_POST);
		foreach ($_POST as $key => $val)
		{
					if (strstr($key,"-")) 
					{
						
						list($field,$code)=split("-",$key);
						if ($field == "gallery_title") 
						{
							$val = $t->formtpa($val);
							$sql->db_Update("gallery_lng","title = '$val' WHERE galleryid = $code");
							
						}
						elseif ($field == "categoryid")
						{
								//gallery CATEGORIES
								$sql->db_Select("gallery_page_categories","catid","galleryid = $code AND main = 'Y'");

								if ($sql->db_Rows() > 0) //UPDATE
								{
									$sql->db_Update("gallery_page_categories","catid = $val WHERE galleryid = $code AND main = 'Y'");
								}
								else 
								{
									$sql->db_Insert("gallery_page_categories","'$val','$code','Y'");
								}
						}
						else 
						{
							$sql->db_Update("gallery","$field = '$val' WHERE id = $code");
							
						}
						
					}
					if (strstr($key,"tags")) 
					{
						
					}
					
					if (strstr($key,"eshop_")) 
					{
						list($vh,$ch)=split("eshop_",$key);
						{
							
							list($ph,$oh)=split("-",$ch);
//							echo "$ph - $oh --- $val<br>";
							$sql->db_Select("eshop_product_details","id","productid = $oh AND type = 'image'");
							if ($sql->db_Rows() > 0) 
							{
							$sql->db_Update('eshop_product_details',"$ph = '$val' WHERE productid = $oh AND type='image'");
							}
							else 
							{
									$sql->db_Insert("eshop_product_details","'','$oh','$price','$list_price','$quantity','$membership','$distribution','$weight','$avail','$sales_stats','$views_stats','$del_stats','$shipping_freight','$free_shipping','$discount_avail','$min_amount','$low_avail_limit','$free_tax','image'");
									$sql->db_Update('eshop_product_details',"$ph = '$val' WHERE productid = $oh AND type = 'image'");
							}
					}
		}//ESHOP
		
					if (strstr($key,"locations_")) 
					{
						
						list($vh,$ch)=split("locations_",$key);
						{
							
						list($ph,$oh)=split("-",$ch);
						$sql->db_Select("gallery_locations","galleryid","galleryid = $oh");
//						echo "SELECT galleryid FROM gallery_locations WHERE galleryid = $oh<br>";
						if ($sql->db_Rows()) 
						{
							$sql->db_Update("gallery_locations","id = '$val' WHERE galleryid ='$oh'");
						}
						else 
						{
							$sql->db_Insert("gallery_locations","'$val','$oh'");
						}
						}
					}//LOCATIONS
		}
		header("Location:".$module_path."/$page");
		exit();
}

############################################### MASS UPDATE ALL ##############################3
if ($_POST['mode'] == "mass_update_all") 
{

//print_r($_POST['ids']);
//print_r($_POST);
#################################### LOOP ALL FIELDS TO BE CHANGED #####################
foreach ($_POST as $k => $v) 
		{
			if (strstr($k,"mass_"))
			{
				$k = str_replace("mass_","",$k);
//				list($field,$id)=split("mass_",$k);
//				echo "$k : $v<br>";
				if ($k == 'title' AND !empty($v)) 
				{
					$update['title'] = $v;
				}
				if ($k == "eshop_price" AND !empty($v)) 
				{
					$update['eshop_price'] = $v;
				}
				if ($k == "tags" AND !empty($v)) 
				{
					$update['tags'] = $v;
				}	
				if ($k == "categoryid" AND !empty($v)) 
				{
					$update['categoryid'] = $v;
				}
				if ($k == "active" AND (!empty($v) OR $v==0)) 
				{
					$update['active'] = $v;
				}			
			}//FOUND MASS
		}//FOREACH
		#################### NOW LOOP THE RESULTS ########################
//	print_r($update);
		foreach ($_POST['ids'] as $key => $val)
		{

			if ($val == 1)//eliminates the check all/none box 
			{
						if ($update['title']) 
						{
							$val = $update['title'];
							$val = $t->formtpa($val);
							$sql->db_Update("gallery_lng","title = '$val' WHERE galleryid = $key");
							
						}
						if ($update['categoryid'])
						{
							$val = $update['categoryid'];
								//gallery CATEGORIES
								$sql->db_Select("gallery_page_categories","catid","galleryid = $key AND main = 'Y'");

								if ($sql->db_Rows() > 0) //UPDATE
								{
									$sql->db_Update("gallery_page_categories","catid = $val WHERE galleryid = $key AND main = 'Y'");
								}
								else 
								{
									$sql->db_Insert("gallery_page_categories","'$val','$key','Y'");
								}
						}


					if (!empty($update['active']) OR $update['active'] == 0) 
					{
						$val = $update['active'];
//						echo "active = '$val' WHERE id = $key<br>";
						$sql->db_Update("gallery","active = '$val' WHERE id = $key");
					}
					if ($update['tags']) 
					{
						//UPDATE TAGS
						$tags = construct_tags_array($_POST['mass_tags'],",");
						//Clear old tags
						$sql->db_Delete("gallery_tags","galleryid = $key");
						//insert new tags
						for ($i=0;count($tags) > $i;$i++)
						{
							$tag = $tags[$i];
							$sql->db_Insert("gallery_tags","'','$key','$tag'");
						}
					}//END TAGS
					
					if ($update['eshop_price']) 
					{
						

						$val = $update['eshop_price'];
			
							
							
							$sql->db_Select("eshop_product_details","id","productid = $key AND type = 'image'");
							if ($sql->db_Rows() > 0) 
							{
//								echo "productid = $key AND type = 'image'<br>";
							$sql->db_Update('eshop_product_details',"price = '$val' WHERE productid = $key AND type='image'");
							}
							else 
							{
									$sql->db_Insert("eshop_product_details","'','$key','$val','$list_price','$quantity','$membership','$distribution','$weight','$avail','$sales_stats','$views_stats','$del_stats','$shipping_freight','$free_shipping','$discount_avail','$min_amount','$low_avail_limit','$free_tax','image'");
							}
					}//END UPDATE
					if (strstr($key,"locations_")) 
					{
						
						list($vh,$ch)=split("locations_",$key);
						{
							
						list($ph,$oh)=split("-",$ch);
						$sql->db_Select("gallery_locations","galleryid","galleryid = $oh");
//						echo "SELECT galleryid FROM gallery_locations WHERE galleryid = $oh<br>";
						if ($sql->db_Rows()) 
						{
							$sql->db_Update("gallery_locations","id = '$val' WHERE galleryid ='$oh'");
						}
						else 
						{
							$sql->db_Insert("gallery_locations","'$val','$oh'");
						}
						}
					}//LOCATIONS
		}//END OF VAL==1
		}//END OF FOREACH
		header("Location:".$module_path."/$page");
		exit();
}

############################################### MASS DELETE IMAGES ##############################3
if ($_POST['mode'] == "delete_images") 
{
		foreach ($_POST['ids'] as $key => $val)
		{
			$id = $key;
			
			if ($val == 1)//eliminates the check all/none box 
			{
						
						list($field,$code)=split("-",$key);
						$sql->db_Select("gallery_images","image,thumb,galleryid","id = $key");
 						$a = execute_single($sql);
 						$sql->db_Select("gallery","image","id = ".$a['galleryid']);
 						$gallery = execute_single($sql);
// 						gallery_r($gallery);
 						if ($a['thumb'] == $gallery['image']) 
 						{
 							$sql->db_Update("gallery","image = '".$gallery_module['settings']['default_thumb']."' WHERE id = ".$a['galleryid']);
 						}
 						@unlink($_SERVER['DOCUMENT_ROOT'].$a['image']); 
						@unlink($_SERVER['DOCUMENT_ROOT'].$a['thumb']);
 						$sql->db_Delete("gallery_images","id = $key");
//echo $key."<br>";
					}
		}
	
		header("Location:".$module_path."/$page");
		exit();
}
############################################### MASS ACTIVATE IMAGES ##############################3
if ($_POST['mode'] == "activate_all_images") 
{
	$id = $_POST['id'];
	$sql->db_Update("gallery_images","available = 1 WHERE galleryid = $id AND type = ".$_POST['type']);
	header("Location:".$module_path."/$page");
	exit();
}

}//END OF LOAD MODULE
?>