<?php

/**
 * A class that features the updating porcedure of the core of MCMS.
 * Its is used to Update the core files of the MCMS.
 * @author
 *
 */
class CoreUpdater
{
	private $sqlDriver;
	private $zipArchive;
	
	/**
	 * A class that features the updating porcedure of the core of MCMS.
	 * Its is used to Update the core files of the MCMS.
	 */
	function __construct()
	{
		$this->init();
	}

	/**
	 * A class that features the updating porcedure of the core of MCMS.
	 * Its is used to Update the core files of the MCMS.
	 */
	function init()
	{
		global $sql;

		$this->sqlDriver = &$sql;
		$this->zipArchive = new ZipArchive();
	}
	
	/**
	 * Fetches a remote file to the given filepath.
	 * return the written bytes
	 * $settings['callUrl']
	 * $settings['filepath']
	 * @param aray $settings
	 * @return int
	 */
	public function fetchRemoteFile($settings)
	{
		$bCount 	= 0;
		$remoteFP 	= fopen($settings['callUrl'], "rb");
		$localFP 	= fopen($settings['filepath'], "ab");
		if($remoteFP && $localFP)
		{
			while (!feof($remoteFP))
			{
				$bCount += fwrite($localFP, fread($remoteFP, 512));
			}
		}
		fclose($remoteFP);
		fclose($localFP);
		return $bCount;
	}
	
	/**
	 * Generate a temporary File Directory
	 * @param array $settings
	 * @return string
	 */
	public function makeTempDir($settings=array())
	{
		$localPath = ABSPATH."".$settings['path']."";
		@mkdir($localPath,0775,true);
		return $localPath;
	}
	
	/**
	 * Unzip the updating archive to the temprary dir.
	 * returns the count of the unziped files
	 * @param array $settings
	 * @return number
	 */
	public function unZipFiles($settings)
	{
		$fCount = 0;
		if(($res=$this->zipArchive->open($settings['dirPTR'].$settings['archiveNameSTR'])) === true)
		{
			for($i = 0; $i < $this->zipArchive->numFiles; $i++)
			{
				if($this->zipArchive->extractTo(
					$settings['dirPTR'].'tmp/',
					$this->zipArchive->getNameIndex($i)
				))
				{
					$fCount++;
				}
			}
			$this->zipArchive->close();
		}
		return $fCount;
	}
	
	/**
	 * Copy the Source File to the Destination.
	 * False is returned in failure, so Transaction may rollback
	 * @param array $settings
	 * @return boolean
	 */
	public function copyFiles($settings=array())
	{
		if(is_array($settings['filesArr']) 
			&& is_dir($settings['srcFilePath'])
			&& is_dir($settings['destFilePath']))
		{
			return $this->copyFilesTraversive($settings['filesArr'], $settings['srcFilePath'], $settings['destFilePath']);
		}
		return false;
	}
	
	/**
	 * Copy files traversive from source to destination
	 * $srcArr ==  Relative paths of files, of temp SRC & Dest directories
	 * @param array $srcArr
	 * @param string $srcPath
	 * @param string $destPath
	 * @return boolean
	 */
	public function copyFilesTraversive($srcArr,$srcPath,$destPath)
	{
		$result = true; // Boolean trick in order to result true all processes should succed
		foreach ($srcArr as $k => $file)
		{
			// Create Parrent FileDir of file
			if(!is_dir(dirname($destPath.$file)))
			{
				$result = $result && mkdir(dirname($destPath.$file),0755,true);
			}
			// Traverse to copy Directory's files
			if(is_dir($srcPath.$file))
			{
				if(!is_dir($destPath.$file))
				{
					$result = $result && mkdir($destPath.$file,0755,true);
				}
				
				$dirPTR = opendir($srcPath.$file);
				$tmpFileArr = array();
				$tmpCurr = "";
				while (($tmpCurr = readdir($dirPTR)) !== false) 
				{
					if($tmpCurr != "." && $tmpCurr != "..")
					{
						$tmpFileArr[] = $file."/".$tmpCurr;
					}
				}
				closedir($dirPTR);
				
				$result = $result && $this->copyFilesTraversive($tmpFileArr, $srcPath, $destPath);
			}
			elseif(is_file($srcPath.$file))
			{
				$result = $result && ($srcFP = fopen($srcPath.$file, "r"));
				$result = $result && ($destFP = fopen($destPath.$file, "w"));
				
				while (!feof($srcFP))
				{
					fwrite($destFP, fread($srcFP, 512));
				}
				
				$result = $result && fclose($srcFP);
				$result = $result && fclose($destFP);
			}
		}
		return $result;
	}
	
	/**
	* Deletes the Source File
	* False is returned in failure, so Transaction may rollback
	* $settings['filepath']
	* @param array $settings
	* @return boolean
	*/
	public function deleteFiles($settings=array())
	{
		if(is_dir($settings['filepath']))
		{
			$this->deleteFilesTraversive($settings['filepath']);
			@rmdir($settings['filepath']);
			return !is_dir($settings['filepath']);
		}
		return false;
	}
	
	/**
	 * Atempts to Delete A Whole Directory with contents traversive
	 * @param string $filepath
	 * @return boolean
	 */
	private function deleteFilesTraversive($filepath)
	{
		try
		{
			$dir = new RecursiveDirectoryIterator($filepath, RecursiveDirectoryIterator::KEY_AS_PATHNAME);// | RecursiveDirectoryIterator::SKIP_DOTS);
			foreach(new RecursiveIteratorIterator($dir,RecursiveIteratorIterator::CHILD_FIRST) as $file=>$c)
			{
				if(preg_match('/(^\.)|(\/\.$)|(\/\.\.$)/', $file))
				{
					continue;
				}
				if(is_dir($file))
				{
					if($this->deleteFilesTraversive($file));
					{
						rmdir($file); // deletes the folder itself
					}
					continue;
				}
				unlink($file);
			}
		}
		catch (Exception $e)
		{
			return false;
		}
		return true;
	}
	
	/**
	* Execute SQL
	* False is returned in failure, so Transaction may rollback
	* @param array $settings
	* @return boolean
	*/
	public function executeSQL($settings=array())
	{
		if(!isset($settings['sql']))
		{
			$settings['sql'] = file_get_contents($settings['path']);
		}
		return $this->sqlDriver->q($settings['sql']);
	}
	
	
	/**
	 * Generates the SQL Dump. by the Given Tables
	 * @param unknown_type $settings
	 * @return string
	 */
	public function generateSQLDump($settings=array())
	{
		$sqlDump = '';
		if(!is_array($settings['tables']))
		{
			return($sqlDump);
		}
		
		foreach ($settings['tables'] as $table)
		{
			$sqlDump .= "\n";
			if($table['droptable'])
			{
				$sqlDump .= "\nDROP TABLE " . $table['name'] ." ; ";
				$sqlDump .= "\r\n";
			}
			
			if($table['createtable'])
			{
				$this->sqlDriver->db_Select_gen('SHOW CREATE TABLE '.$table['name']);
				$r = execute_single($this->sqlDriver);
				$sqlDump .= "\n" . $r['Create Table'] ." ; ";
				$sqlDump .= "\r\n";
			}
			$sqlDump .= "\n";
			
			$rowArr = array();
			$this->sqlDriver->db_Select($table['name']);
			
			if(($this->sqlDriver->db_Rows()>0))
			{
				$rowArr = execute_multi($this->sqlDriver);
				foreach ($rowArr as $row)
				{
					$sqlDump .= "\n";
					$sqlDump .= " INSERT INTO ".$table['name']." VALUES( ";
					$tmpArr = array();
					foreach ($row as $k => $v)
					{
						if(strlen($v)==0 || is_null($v))
						{
							$tmpArr[] = "\"\"";
							continue;
						}
						if(is_numeric($v))
						{
							$tmpArr[] = "'".$v."'";
							continue;
						}
						$tmpArr[] = "\"".addslashes($v)."\"";
					}
					$sqlDump .= implode(" , ", $tmpArr);
					$sqlDump .= " ); \r\n";
				}
			}
			$sqlDump .= "\r\n\r\n";
		}
		
		return($sqlDump);
	}
	
	/**
	* Builds the Stages of the Updating procedure
	* return the no of states
	* @param array Pointer $statesMap
	* @return number
	*/
	public function setUpdatingStates(&$statesMap)
	{
		$i = -1;
		$statesMap = array();
		//-------------------
		$key = 'fetchFiles'; // fetch archive
		$statesMap[++$i] = $key;
		$statesMap[$key] = $i;
		//-------------------
		$key = 'unzipFiles'; // Unzip files to tmp
		$statesMap[++$i] = $key;
		$statesMap[$key] = $i;
		//-------------------
		$key = 'copyFiles'; // Copy Files from tmp
		$statesMap[++$i] = $key;
		$statesMap[$key] = $i;
		//-------------------
		$key = 'updateSql'; // Execute SQL
		$statesMap[++$i] = $key;
		$statesMap[$key] = $i;
		//-------------------
		$key = 'cleanFiles'; // Clear Cache & Temp files, finilize
		$statesMap[++$i] = $key;
		$statesMap[$key] = $i;
		//-------------------
	
		return $i;
	}
}

?>