<?php

/**
 * A class that features the tracing ability of Updater Module.
 * Its is used to build the updating pakage.
 * It Parses the JSON token, as it describes the vital files of the tracing update.
 * @author
 *
 */
class TraceWorkerUpdater
{
	private $sqlDriver;
	private $zipArchive;
	/**
	 * A class that features the tracing ability of Updater Module.
	 * Its is used to build the updating pakage.
	 * It Parses the JSON token, as it describes the vital files of the tracing update.
	 *
	 */
	function __construct()
	{
		$this->init();
	}

	/**
	 * A class that features the tracing ability of Updater Module.
	 * Its is used to build the updating pakage.
	 * It Parses the JSON token, as it describes the vital files of the tracing update.
	 *
	 */
	function init()
	{
		global $sql;

		$this->sqlDriver = &$sql;
		$this->zipArchive = new ZipArchive();
	}

	/**
	 * Parse the Array of files and generates a zip cfor the given array
	 * @param array $settings
	 * @return boolean
	 */
	public function generateArchive(&$settings=array())
	{
		if($this->zipArchive->open(
			$settings['dirPTR'].$settings['archiveNameSTR'],
			( file_exists($settings['dirPTR'].$settings['archiveNameSTR'])? ZipArchive::OVERWRITE : ZipArchive::CREATE ) 
		) === true)
		{
			// First parse for Files & dirs the given filesArr, then recurse
			foreach($settings['filesArr'] as $fPtr)
			{
				if(is_dir($fPtr))
				{
					$this->addDirToArchiveTraversive($this->zipArchive, $fPtr);
					continue;
				}
				$tmpFileName = $fPtr;
				$count=1;
				$tmpFileName = str_replace($settings['dirPTR'],'',$tmpFileName);
				$tmpFileName = str_replace(ABSPATH,'',$tmpFileName);
				$tmpFileName = str_replace("\"",'',$tmpFileName);
				$tmpFileName = preg_replace("/\//",'',$tmpFileName,1);
				$tmpFileName = preg_replace("/\\\/",'',$tmpFileName,1);
				$this->zipArchive->addFile($fPtr,$tmpFileName);
			}
			$this->zipArchive->close();
			return file_exists($settings['dirPTR'].$settings['archiveNameSTR']);
		}
		return false;
	}
	
	/**
	 * Tree iteration Pattern
	 * @param ZipArchive $zip
	 * @param array $fileArr
	 */
	private function addDirToArchiveTraversive(&$zip,$filePTR)
	{
		try 
		{
			$dir = new RecursiveDirectoryIterator($filePTR, RecursiveDirectoryIterator::KEY_AS_PATHNAME);// | RecursiveDirectoryIterator::SKIP_DOTS);
			foreach(new RecursiveIteratorIterator($dir) as $file=>$c)
			{
				if(preg_match('/(^\.)|(\/\.$)|(\/\.\.$)/', $file)) 
				{
					continue;
				}
				
				if(is_dir($file))
				{
					$this->addDirToArchiveTraversive(&$zip,$file);
				}
				
				$tmpFileName = $file;
				$count=1;
				$tmpFileName = str_replace(ABSPATH,'',$tmpFileName);
				$tmpFileName = str_replace("\"",'',$tmpFileName);
				$tmpFileName = preg_replace("/\//",'',$tmpFileName,1);
				$tmpFileName = preg_replace("/\\\/",'',$tmpFileName,1);
				$zip->addFile($file,$tmpFileName);
			}
		}
		catch (Exception $e)
		{
			
		}
		return $zip;
	}
	
	/**
	 * Generates the SQL Dump. by the Given Tables
	 * @param unknown_type $settings
	 * @return string
	 */
	public function generateSQLDump($settings=array())
	{
		$sqlDump = '';
		if(!is_array($settings['tables']))
		{
			return($sqlDump);
		}
		
		foreach ($settings['tables'] as $table)
		{
			$sqlDump .= "\n";
			if($table['droptable'])
			{
				$sqlDump .= "\nDROP TABLE " . $table['name'] ." ; ";
				$sqlDump .= "\r\n";
			}
			
			if($table['createtable'])
			{
				$r = execute_single($this->sqlDriver->db_Select_gen('SHOW CREATE TABLE '.$table['name']));
				$sqlDump .= "\n" . $r[1] ." ; ";
				$sqlDump .= "\r\n";
			}
			$sqlDump .= "\n";
			
			$rowArr = array();
			$rowArr = execute_multi($this->sqlDriver->db_Select($table['name']));
			
			foreach ($rowArr as $row)
			{
				$sqlDump .= "\n";
				$sqlDump .= " INSERT INTO ".$table['name']." VALUES( ";
				$tmpArr = array();
				foreach ($row as $k => $v)
				{
					if(strlen($v)==0 || is_null($v))
					{
						$tmpArr[] = "\"\"";
						continue;
					}
					if(is_numeric($v))
					{
						$tmpArr[] = "'".$v."'";
						continue;
					}
					$tmpArr[] = "\"".addslashes($v)."\"";
				}
				$sqlDump .= implode(" , ", $tmpArr);
				$sqlDump .= " ); \r\n";
			}
			$sqlDump .= "\r\n\r\n";
		}
		
		return($sqlDump);
	}
	
	/**
	 * Builds the Stages of the Tracing procedure
	 * return the no of states
	 * @param array Pointer $statesMap
	 * @return number
	 */
	public function setTraceWorkerStates(&$statesMap)
	{
		$i = -1;
		$statesMap = array();
		//-------------------
		$key = 'tracestart'; // New Versio Token is built
		$statesMap[++$i] = $key;
		$statesMap[$key] = $i;
		//-------------------
		$key = 'buildmanifest'; // Build Manifest & dependencies
		$statesMap[++$i] = $key;
		$statesMap[$key] = $i;
		//-------------------
		$key = 'builarchive'; // Zip Files
		$statesMap[++$i] = $key;
		$statesMap[$key] = $i;
		//-------------------
		$key = 'buildatabase'; // Dump DB
		$statesMap[++$i] = $key;
		$statesMap[$key] = $i;
		//-------------------
		$key = 'finilize'; // Clear Cache & Temp files, finilize
		$statesMap[++$i] = $key;
		$statesMap[$key] = $i;
		//-------------------
		
		return $i;
	}

}


?>