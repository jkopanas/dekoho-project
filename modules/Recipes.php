<?php
class Recipes extends Items {
	
	function GetRecipeDetails($id,$settings=0)
	{
		global $sql;
		$res['ingredients'] = $this->GetRecipeIngredients(array_merge($settings,array('id'=>$this->itemid,'debug'=>1)));
		return $res;
	}//END FUNCTION
	
	function GetRecipeIngredients($settings)
	{
		global $sql;
		$orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : " ORDER BY orderby";
		$way = ($settings['way']) ? $settings['way'] : " ASC";
		$main = ($settings['main']) ? " AND main = ".$settings['main'] : "";
		$limit = ($settings['limit']) ? " LIMIT ".$settings['limit'] : "";
		$ingredientIDs = ($settings['ingids']) ? " AND recipes_ingredients_items.ingid IN (".implode(',',$settings['ingids']).") " : "";
		$ingredientIDs = ($settings['ids']) ? " AND recipes_ingredients_items.id IN (".implode(',',$settings['ids']).") " : "";
		if ($settings['id']) {
		$sql->db_Select("recipes_ingredients INNER JOIN recipes_ingredients_items ON (recipes_ingredients.id=recipes_ingredients_items.ingid)","*","itemid = ".$settings['id'].$ingredientIDs.$main.$orderby.$way.$limit);
//		echo "SELECT * FROM recipes_ingredients INNER JOIN recipes_ingredients_items ON (recipes_ingredients.id=recipes_ingredients_items.ingid) WHERE itemid = ".$settings['id'].$ingredientIDs.$main.$orderby.$way.$limit;
		if ($settings['full-recipe']) {
			$item = execute_multi($sql);
			for ($i=0;count($item) > $i;$i++)
			{
				if ($item[$i]['type'] == 'ingredient') {
					$item[$i]['unit_text'] = $this->ConversionUnits(array('id'=>$item[$i]['unit_id']));
				}			
			}
			return $item;
		}
		else {
		return execute_multi($sql);
		}
		}//END ITEMS INGREDIENTS 
		elseif ($settings['return'] == 'list')
		{
			if (is_array($settings['ingredient_id'])) {
				$sql->db_Select("recipes_ingredients","base","id IN (".implode(",",$settings['ingredient_id']).") GROUP BY base");
			}
			else {
				$sql->db_Select("recipes_ingredients","base","id = ".$settings['ingredient_id']);
	//			echo "SELECT base FROM recipes_ingredients WHERE id = ".$settings['ingredient_id'];
			}
			
			return execute_multi($sql);
			
		}
		elseif ($settings['ingredient_id'])
		{
			$sql->db_Select("recipes_ingredients INNER JOIN recipes_ingredients_items ON (recipes_ingredients.id=recipes_ingredients_items.ingid)","itemid","recipes_ingredients.id = ".$settings['ingredient_id']);
			$b = execute_multi($sql);
			return $b;
		}
		elseif ($settings['ingredient_name'])
		{
			$sql->db_Select("recipes_ingredients INNER JOIN recipes_ingredients_items ON (recipes_ingredients.id=recipes_ingredients_items.ingid)","itemid","base = '".$a['base']."'");
			$b = execute_multi($sql);
			return $b;
		}

		else {//GET ALL INGREDIENTS
			$mode = ($settings['base'] OR $settings['type']) ? "default" : "no_where";
			$type_q = ($settings['type']) ? "type = '".$settings['type']."' " : "";
	//		echo "SELECT * FROM recipes_ingredients WHERE $type_q GROUP BY base ".$main.$orderby.$way.$limit;
			$sql->db_Select("recipes_ingredients INNER JOIN recipes_ingredients_items ON (recipes_ingredients.id=recipes_ingredients_items.ingid)","*","$type_q GROUP BY base ".$main.$orderby.$way.$limit,$mode);
			if ($sql->db_Rows()) {
				return execute_multi($sql);
			}
		}
	}//END FUNCTION

function ConversionUnits($settings)
{
	global $sql;
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$orderby = ($settings['orderby']) ? $settings['orderby'] : "name";
	$way = ($settings['way']) ? $settings['way'] : "ASC";
	
	if ($settings['id']) {
		$sql->db_Select("recipes_conversion_table",$fields,"id = ".$settings['id']);
		return execute_single($sql);
	}
	elseif ($settings['base']) {
		$sql->db_Select("recipes_conversion_table",$fields,"base = 1 ORDER BY $orderby $way");
		return execute_multi($sql);	
	}
	else {
		$sql->db_Select("recipes_conversion_table",$fields,"ORDER BY $orderby $way",$mode="no_where");
		return execute_multi($sql);	
	}

}//END FUNCTION

function AddConversionTable($data,$settings)
{
	global $sql;
	$t = new textparse();
	$name = $t->formtpa($data['name']);
	$description = $t->formtpa($data['description']);
	$value = $t->formtpa($data['value']);
	$catid = $t->formtpa($data['catid']);
	$base = $t->formtpa($data['base']);
	
	$sql->db_Insert("recipes_conversion_table","'','$name','$description','$value','$catid','$base'");
	
	if ($settings['return']) {
		return $sql->last_insert_id;
	}
}//END FUNCTION

function SaveRecipeIngredients($itemid,$data,$settings=0)
{
	$sql = new db();
	$t = new textparse();
	$order = form_settings_array($data['ingredient_order']);
		foreach ($data as $k => $v) {
		if (strstr($k,'-') AND !strstr($k,'efields-')) {
			list($field,$ingid)=split("-",$k);
			if (is_numeric($ingid)) {
				$tmp[$ingid][$field] = $v;
				if ($field == 'base' AND $v) {
					$tmpIngredients[$ingid] = $v;//Collect to check if already in data base
				}
				elseif ($field == 'type' AND $v == 'header')
				{
					$headers[] = $ingid;
				}
			}

		}
		
	}//END SETUP THE INGREDIENT LIST
	ksort($tmp);
	foreach ($order as $k=>$v)//SORT OUT THE INGREDIETNS
	{
		list($uselessPrefix,$sortOrder)=split("-",$v);
		$type = ($tmp[$sortOrder]['type'] == 'header') ? "header" : "ingredient";
		$tmp[$sortOrder]['orderby'] = $sortOrder;
	}

	/* DROP OLD INGREDIENTS */
$sql->db_Delete("recipes_ingredients_items","itemid = $itemid");
	

$i=0;
foreach ($tmp as $k=>$v)
{
	if ($v['type'] == 'ingredient') {//INGREDIENT
		$sql->db_Select("recipes_ingredients","id","base = '".$v['base']."' AND type = '".$v['type']."'");
		if ($sql->db_Rows()) {//FOUND GET THE ID
			$r = execute_single($sql);
			$ingredientID = $r['id'];
		}//END FOUND
		else {//NOT FOUND ADD IT
			$sql->db_Insert("recipes_ingredients","'','".$v['base']."','".greeklish($v['ingredient'])."','".$v['type']."'");
			$ingredientID = $sql->last_insert_id;
		}//END INGREIDENT NOT FOUND
	}//END INGREDIENT
	elseif ($v['type'] == 'header'){//HEADER
		$sql->db_Select("recipes_ingredients","id","permalink = '".greeklish($v['ingredient'])."' AND type = '".$v['type']."'");
		if ($sql->db_Rows()) {//FOUND GET THE ID
			$r = execute_single($sql);
			$ingredientID = $r['id'];
		}//END FOUND
		else {//NOT FOUND ADD IT
			$sql->db_Insert("recipes_ingredients","'','".$v['base']."','".greeklish($v['ingredient'])."','".$v['type']."'");
			$ingredientID = $sql->last_insert_id;
		}//END INGREIDENT NOT FOUND
	}//END HEADER
	$InsertQuery[] = "('','$ingredientID','$itemid','".$t->formtpa($v['ingredient'])."','".$v['unit']."','".$v['quantity']."','".$v['main']."','".$v['orderby']."')";

}//END LOOP POSTED DATA
if ($InsertQuery) {
	mysql_query("INSERT INTO recipes_ingredients_items VALUES ".implode(",",$InsertQuery));
}

}//END FUNCTION

function InsertNewIngredients($InsertQuery)//THIS FUNCTION IS ONLY USED BECAUSE INSERT DOES NOT WORK !!!!
{
	global $sql;
	$query = "INSERT INTO recipes_ingredients_items VALUES".implode(",\n",$InsertQuery);
	mysql_query($query);
	echo $query;
}
}//END CLASS

?>