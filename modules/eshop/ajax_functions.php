<?php
global $smarty;

$xajax->registerFunction("ajax_payment_method");
$xajax->registerFunction("ajax_shipping_method");
$xajax->registerFunction("ajax_submit_checkout");

function ajax_submit_checkout($Form,$res_div)
{
	global $sql,$smarty,$lang;
	$objResponse = new xajaxResponse();

	$processor = eshop_payment_processors($Form['processor']);
	$smarty->assign("processor", $processor);
	
	$required_fields = $processor['required_fields'];
	
//check required fields
 for ($i=0;count($Form) > $i;$i++)
 {
 	for ($j=0;count($required_fields) > $j;$j++)
 	{
 		if ($Form[$required_fields[$j]] == "") 
 		{  
 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
 	 		$objResponse->addScript('$(\'label[for="'.$required_fields[$j].'"]\').show();');
 	 		$error = 1;
 		}//END OF IF
 	}//END OF FOR

 }//END OF FOR
 if (!$error) 
 {
 	
 }

  $smarty->assign("error_list",$error_list);//assigned template variable error_list<BR>
//	$message = $smarty->fetch("common/error_list.tpl");	

	$objResponse->addScript('$("#'.$res_div.'").focus();');
	
//$objResponse->addEvent('CartForm','onsubmit','submit_checkout()');
//	$objResponse->addAssign($res_div,"innerHTML",$message);
//	$objResponse->addScript("setTimeout(\"document.getElementById('inr_display').innerHTML=''\",3500);");
	return $objResponse;
}

function ajax_payment_method($id,$res_div)
{
	global $sql,$smarty;
	$objResponse = new xajaxResponse();
	$item = eshop_payment_methods($id,0,1);
	if ($item['surcharge']) 
	{
		$new_total = calculate_cart_price(0,$item['surcharge'],$item['surcharge_type'],"add",$_SESSION['cart']['price']['total']);
	}
	else 
	{
		$new_total = $_SESSION['cart']['price']['total'];
	}
	if ($item['shipping_methods']) 
	{
		$smarty->assign("shipping",eshop_shipping_methods('',1,explode(':::',$item['shipping_methods'])));
		$smarty->assign("selection",1);
	}
	else 
	{
		$smarty->assign("shipping",eshop_shipping(0,1,1));
	}
	unset($_SESSION['cart']['price']);

	$_SESSION['cart']['price'] = $new_total;

	$smarty->assign("processor", eshop_payment_processors($item['settings']['payment_processor']));
	$smarty->assign("payment_method",$item['id']);
	$smarty->assign("total_price",$new_total);
	$message = $smarty->fetch("modules/eshop/shipping_options.tpl");	
	
//$objResponse->addEvent('CartForm','onsubmit','submit_checkout()');
	$objResponse->addAssign('total_price',"innerHTML",$new_total['formated_total']);
	$objResponse->addAssign($res_div,"innerHTML",$message);
//	$objResponse->addScript("setTimeout(\"document.getElementById('inr_display').innerHTML=''\",3500);");
	return $objResponse;
}


function ajax_shipping_method($id,$payment_method,$res_div)
{
	global $sql,$smarty;
	$objResponse = new xajaxResponse();
	$a[] = $id;
	$item = eshop_payment_methods($payment_method,0,1);
	if ($item['surcharge']) 
	{
		$new_total = calculate_cart_price(0,$item['surcharge'],$item['surcharge_type'],"add",$_SESSION['cart']['price']['total']);
	}
	else 
	{
		$new_total = $_SESSION['cart']['price']['total'];
	}
	$item = eshop_shipping_methods(0,0,$a);



		$cart = get_user_cart(ID,1,FRONT_LANG);
		$new_total = calculate_cart_price(0,$item[0]['base_cost'],'',"add",$new_total['total']);

	
	$smarty->assign("total_price",$new_total);
	
	
	$objResponse->addAssign($res_div,"innerHTML",$new_total['formated_total']);
//	$objResponse->addScript("setTimeout(\"document.getElementById('inr_display').innerHTML=''\",3500);");
	return $objResponse;
}
//$xajax->processRequests();
//$smarty->assign("ajax_requests",$xajax->getJavascript(URL)); 
?>