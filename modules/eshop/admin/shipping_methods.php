<?php
include("../../../manage/init.php");//load from manage!!!!

$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
$code = ($code) ? $code: 1;

if ($content_module = module_is_active("eshop",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$content_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$content_module['settings']);

if ($_POST['action'] == "add") 
{
		$t = new textparse();
		$shipping = $t->formtpa($_POST['shipping']);
		$description = $t->formtpa($_POST['description']);
		$orderby = $_POST['orderby'];
		$active = $_POST['active'];
		$date_added = time();
		foreach ($_POST as $key => $val)
		{
			$val = $t->formtpa($val);
			if (strstr($key,"settings-")) 
			{
				list($dump,$field)=split("-",$key);
				$ss[$field] = $val;
	
			}
		}
		if (is_array($ss) AND !empty($ss)) {
			$settings = form_settings_string($ss,"###",":::");
		}
		$sql->db_Insert("eshop_shipping","'','$shipping','$shipping_time','$destination','$code','$id','$orderby','$active','$weight_min',
		'$weight_limit','$date_added','$description','$settings','$base_cost'");
		
		header("Location: shipping_methods.php");
		exit();
}
if ($_POST['action'] == "update") 
{
foreach ($_POST as $key => $val)
{
if (strstr($key,"settings-")) 
{
				list($dump,$field)=split("-",$key);
				$ss[$field] = $val;
}
$sql->db_Update("eshop_shipping","$key = '$val' WHERE shippingid = $id");
}
		if (is_array($ss) AND !empty($ss)) {
			$q = form_settings_string($ss,"###",":::");
			$query = "settings ='$q' WHERE shippingid = $id";
			$sql->db_Update("eshop_shipping",$query);
		}
}

if ($_POST['action'] == "modify") 
{
	
	foreach ($_POST as $key => $val)
	{
			if (strstr($key,"-")) 
		{
			list($field,$newid)=split("-",$key);
			$query = "$field = '$val' WHERE shippingid = $newid AND code = '$code'";
//echo $query."<Br>";
			$sql->db_Update("eshop_shipping",$query);
		}//END OF IF
	}
	if ($_POST['new_shipping']) 
	{
		$t = new textparse();
		$shipping = $t->formtpa($_POST['new_shipping']);
		$shipping_time = $t->formtpa($_POST['new_shipping_time']);
		$destination = $_POST['new_destination'];
		$orderby = $_POST['new_orderby'];
		$active = $_POST['new_active'];
		$weight_min = $_POST['new_weight_min'];
		$weight_limit = $_POST['new_weight_limit'];
		$base_cost = $_POST['new_base_cost'];
		$date_added = time();
		$sql->db_Insert("eshop_shipping","'','$shipping','$shipping_time','$destination','$code','$id','$orderby','$active','$weight_min',
		'$weight_limit','$date_added','$description','$settings','$base_cost'");
		
		header("Location: shipping_methods.php?id=$id#methods");
		exit();
		
	}
}

if ($_POST['action'] == "delete")
{

			foreach ($_POST['ids'] as $key => $val)
		{
		
			if ($val == 1)//eliminates the check all/none box 
			{
				$sql->db_Delete("eshop_shipping","shippingid = $key");
			}
		}
		header("Location: shipping_methods.php?id=$id#methods");
		exit();
}
if ($_GET['action'] == "delete")
{
	    $tmp = eshop_shipping_methods($id);
		$sql->db_Delete("eshop_shipping","shippingid = $id");
		for ($i=0;count($tmp) > $i;$i++)
		{
			$sql->db_Delete("eshop_shipping","shippingid = ".$tmp[$i]['shippingid']);
		}
		header("Location: shipping_methods.php");
		exit();
}
if ($_GET['action'] == "edit")
{
	$smarty->assign("shipping", eshop_shipping($id));
	$smarty->assign("action","update");
}
	
	$shipping_methods = eshop_shipping();
	$smarty->assign("shipping_methods",$shipping_methods);

if ($code == 1) 
{
	$smarty->assign("code",$shipping_methods[0]['code']);
}
else 
{
	$smarty->assign("code",$code);
}
}

if ($id) {
	$smarty->assign("methods",eshop_shipping_methods($id));
}
		

$smarty->assign("id",$id);
$smarty->assign("menu",$content_module['name']);
$smarty->assign("submenu","shipping");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/eshop/admin/shipping_methods.tpl");
$smarty->display("admin/home.tpl");
$smarty->clear_all_assign();
?>