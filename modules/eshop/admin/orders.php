<?php
include("../../../manage/init.php");//load from manage!!!!

if (array_key_exists("eshop",$loaded_modules)) {
	$current_module = $loaded_modules['eshop'];
	$smarty->assign("MODULE_FOLDER",URL."/".$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$filter_fields = array('sort_field','sort_direction','status','results_per_page','payment_method','custom_field','custom_value','archive');//Setup the available filters
	$status_codes = eshop_order_status_codes();
	for ($i=0;count($status_codes) > $i;$i++)
	{
		$codes[$status_codes[$i]['id']] = $status_codes[$i]['title'];
	}
	$smarty->assign("status_codes",$codes);
	if ($_POST['action'] == 'update') {
		$sql->db_Update("eshop_orders","status = ".$_POST['status']." WHERE id = '".$_POST['id']."'");
		header("Location: orders.php?id=".$_POST['id']);
		exit();
	}//END UPDATE
	if ($_GET['action']=='print' AND $_GET['id']) {
		$orderid = $_GET['id'];
		$filters = array('custom_field'=>'id','custom_value'=>$orderid);
		$settings = array('filters'=>$filters,'limit'=>1,'return' => 'single','debug'=>0);
		if (array_key_exists("eshop",$loaded_modules)) {
			$settings['get_products'] = 1;
			$settings['lang'] = DEFAULT_LANG;
		}
		$order = get_orders($settings);
		$total_price = array_sum($order['prices']);
		$smarty->assign("total_price_no_vat",$total_price);
		$smarty->assign("vat_price",($loaded_modules['eshop']['settings']['vat']*($total_price/100)));
		$smarty->assign("order",$order);
		$content = $smarty->fetch("modules/eshop/admin/print_order.tpl");
		###### PDF #####
		require_once($_SERVER['DOCUMENT_ROOT'].'/includes/tcpdf/config/lang/eng.php');
		require_once($_SERVER['DOCUMENT_ROOT'].'/includes/tcpdf/tcpdf.php');
		
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 
		$pdf->SetHeaderData('logo.gif', PDF_HEADER_LOGO_WIDTH, SITE_NAME, '#'.$order['id']);

		
		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
		
		//set some language-dependent strings
		$pdf->setLanguageArray($l); 
		
		// ---------------------------------------------------------
		
		// set font
		$pdf->SetFont('freeserif', '', 10);
		
		// add a page
		$pdf->AddPage();
		
		// get esternal file content
		$pdf->writeHTML($content, true, 0, true, 0);
		
		// reset pointer to the last page
		$pdf->lastPage();
		
		// ---------------------------------------------------------
		// write some JavaScript code


// force print dialog
$js .= 'print(true);';

// set javascript
$pdf->IncludeJS($js);
		//Close and output PDF document
		$pdf->Output($order['id'].'.pdf', 'D');
		exit();
	}
	if ($_POST['action'] == 'archive') {
		foreach ($_POST['ids'] as $v)
		{
			$tmp[] = "'$v'";
		}
		$sql->db_Update("eshop_orders","archive = 1 WHERE id IN (".implode(",",$tmp).")");
		header("Location: orders.php");
		exit();	
	}
	if ($_POST['action'] == 'delete') {
		foreach ($_POST['ids'] as $v)
		{
			$tmp[] = "'$v'";
		}
		
		$sql->db_Delete("lottery_tickets","orderid IN (".implode(",",$tmp).")");
		$sql->db_Delete("eshop_orders","id IN (".implode(",",$tmp).")");
		header("Location: orders.php");
		exit();	
	}
	if ($_GET['action'] == 'archive') {
		$sql->db_Update("eshop_orders","archive = 1 WHERE id = '".$_GET['id']."'");
		header("Location: orders.php?id=".$_GET['id']);
		exit();	
	}
	if ($_GET['id']) {
		$orderid = $_GET['id'];
		$filters = array('custom_field'=>'id','custom_value'=>$orderid);
		$settings = array('filters'=>$filters,'limit'=>1,'return' => 'single','debug'=>0);
		if (array_key_exists("eshop",$loaded_modules)) {
			$settings['get_products'] = 1;
			$settings['lang'] = DEFAULT_LANG;
		}
		$order = get_orders($settings);

		$total_price_no_vat = $order['amount'];
		
		if ($order['details']['ExtraCharge'])
		{
			$extra_charge_vat = vat_price($order_details['ExtraCharge'],$loaded_modules['eshop']['settings']['vat'],'minus');
			$total_price_vat = vat_price(($total_price_no_vat - $order['details']['extra_charge_no_vat']),$loaded_modules['eshop']['settings']['vat']) + $order['details']['ExtraCharge'];
			$original_price_no_vat = $total_price_no_vat - $order['details']['extra_charge_no_vat'];
			$origial_price_vat = vat_price($original_price_no_vat,$loaded_modules['eshop']['settings']['vat']);	
			$vat_price = ($loaded_modules['eshop']['settings']['vat']*($original_price_no_vat/100)) + $order['details']['extra_charge_vat'];
		}//THIS IS FOR INSTALLMENTS
		else {
			$total_price_vat = vat_price(($total_price_no_vat - $order['details']['extra_charge_no_vat']),$loaded_modules['eshop']['settings']['vat']);
			$vat_price = ($loaded_modules['eshop']['settings']['vat']*($total_price_no_vat/100));
		}//END NORMAL
		
		$smarty->assign("total_price_no_vat",$total_price_no_vat);
		$smarty->assign("total_price_vat",$total_price_vat);
		$smarty->assign("vat_price",$vat_price);
		$smarty->assign("order",$order);
	}//END ORDER FOUND
	else {//NO ORDER ID
		$limit = ($_REQUEST['results_per_page'] AND $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 30;
		$settings = array('return' => 'paginated','limit' => $limit,'page' => $page,'debug'=>0);

		if ($_REQUEST['mode'] == 'filters') 
		{
			foreach ($_REQUEST as $k => $v)//Check to see which filters where posted to setup the url
			{
				if (in_array($k,$filter_fields) AND $v != '%') //Only use the ones changed
				{
					$query_url .= "$k=$v&";
					$filters_array[$k] = $v;
				}
			}
			if ($_REQUEST['submit']) {//Submit was hit, reset filters
				$page = 1;//Reset paging
			}
		if (is_array($filters_array)) {
			$settings['filters'] = $filters_array;
		}	
			$smarty->assign("query",$query_url."mode=filters");//we append the mode to use the GET method as well
			$smarty->assign("filters",$_REQUEST);
		}//END FILTERS	
	
		$smarty->assign("orders",get_orders($settings));
		$smarty->assign("payment_methods",eshop_payment_methods());
	}//END OF ALL ORDERS
	
}
else {
	exit();
}
$smarty->assign("menu",$current_module['name']);
$smarty->assign("submenu","orders");
$smarty->assign("page_title",SITE_NAME." Administration | ".$current_module['menu_title_admin']);
$smarty->assign("include_file",$current_module['folder']."/admin/orders.tpl");
$smarty->display("admin/home.tpl");
$smarty->clear_all_assign();
?>