<?php
/************************************************ /
function get_user_cart($uid,$mode="no",$lang="no",$status=0,$orderid=0)
{
	global $sql;
	$cart = array();
	//	clean_up_cart($uid);
	if ($orderid) {
		$order = " AND order_id = $orderid";
	}

	if ($mode == "no")
	{
		$prints = 0;
		$sql->db_Select("eshop_cart","SUM(quantity) as total_products, SUM(price) as total_price","uid = $uid AND status = $status $order GROUP BY uid");
		if ($sql->db_Rows() >0)
		{
			$cart = execute_single($sql,0);
			$cart['total_price_clean'] =$cart['total_price'];
			$cart['total_price'] = number_format($cart['total_price'], 2, ',', ' ');

		}
	}
	else
	{
		$sql->db_Select("eshop_cart","*","uid = $uid AND status = $status $order");
		//		echo "uid = $uid AND status = $status $order GROUP BY productid";
		if ($sql->db_Rows() >0)
		{
			$cart = execute_multi($sql,0);
			$total_price = 0;
			for ($i=0;count($cart) > $i;$i++)
			{
				$cart[$i]['product'] = get_gallery($cart[$i]['productid'],$lang,1,1,0,0,1);
				//Get the quantities
				$sql->db_Select("eshop_cart","*","uid = $uid AND status = $status AND productid = ".$cart[$i]['productid']." $order");
				//			echo "SELECT * FROM eshop_cart WHERE uid = $uid AND status = $status AND productid = ".$cart[$i]['productid']." $order";
				$res = execute_multi($sql,0);
					
				$total_price = $total_price + $cart[$i]['price'];
				//Product Price Info
				$count = $sql->db_Rows();
				if ($cart[$i]['options'])
				{
					$options = array();
					$options = form_settings_array($cart[$i]['options'],"###",":::");
					$cart[$i]['options'] = '';

					$j=0;
					foreach ($options as $k => $v)
					{
						unset($o);
						$cart[$i]['options'][$j] = get_gallery_class_options($k,$lang,$v,$cart[$i]['productid']);
						$o = calculate_cart_price(0,$cart[$i]['options'][$j]['price_modifier'],$cart[$i]['options'][$j]['modifier_type'],"add",$total_price);
						$total_price = $o['total'];
						$j++;
					}
				}
				//APPLY DISCOUNTS
				$h = eshop_discounts($cart[$i]['productid']);
				if ($h)
				{
					//	print_r($h);

					$tmp_discount = calculate_cart_price(0,$h['discount'],$h['discount_type'],"sub",$total_price);
					$total_price = $tmp_discount['total'];
					$cart[$i]['discounts'][] = $h;
				}
				$cart[$i]['total_price'] = $total_price;
				$cart[$i]['formated_total'] = number_format($total_price, 2, ',', ' ');
			}
		}
	}
	if ($cart)
	{

		$tmp = array();
		$tmp['items'] = $cart;
		$tmp['price'] = calculate_cart_price($cart);
		if ($tmp['price'])
		{
			//Get global discounts
			$discounts = eshop_discounts();

			if ($discounts)
			{
				$tmp_price = $tmp['price']['total'];
				for ($i=0;count($discounts) > $i;$i++)
				{
					unset($tmp_discount);
					$tmp_discount = calculate_cart_price(0,$discounts[$i]['discount'],$discounts[$i]['discount_type'],"sub",$total_price);

					$total_price = $tmp_discount['total'];
					$tmp['discounts'][] = $discounts[$i];
				}
				$tmp['price']['total'] = $total_price;
			}
		}//END IF CART ITEMS
		$tmp['price']['total'] =   $total_price;
		$tmp['price']['formated_total'] =   number_format($total_price, 2, ',', ' ');
		$tmp['total_items'] = count($cart);
		$_SESSION['cart'] = $tmp;
	}
	return $tmp;
}
/************************************************/
/************************************************ /
function eshop_discounts($productid=0)
{
	global $sql;
	//Check if a product discount is available.
	if ($productid)
	{
		$sql->db_Select("eshop_discounts_products","*","productid = $productid");
		if ($sql->db_Rows()) //FOUND A VALID DISCOUNT
		{
			$tmp = execute_single($sql);
			$sql->db_Select("eshop_discounts","*","discountid = ".$tmp['discountid']);
			return execute_single($sql);
		}
		else
		{
			$sql->db_Select("eshop_discounts","*","type = 0");
			return execute_multi($sql);
		}
	}
	else
	{
		$sql->db_Select("eshop_discounts","*","type = 0");
		return execute_multi($sql);
	}

}
/************************************************/
/************************************************ /
function empty_cart($uid,$status=0)
{
	global $sql;

	$sql->db_Delete("eshop_cart","uid = $uid AND status = $status");
}
/************************************************/
/************************************************ /
function calculate_cart_price($cart=0,$number=0,$type=0,$mode=0,$total=0)
{
	//first get total cart price
	$total = ($total) ? $total : 0;
	if ($cart)
	{
		for ($i=0;count($cart) > $i;$i++)
		{
			$total = $cart[$i]['total_price'] + $total;
		}
	}

	$price['items_total'] = $total;

	if ($mode)
	{
		if ($mode == "add")
		{
			if ($type == "%")
			{
				$total = $total + ($number * $total)/100;
			}
			else
			{
					
				$total = $total + $number;
			}
		}
		if ($mode == "sub")
		{
			if ($type == "%")
			{
				$total = $total - ($number * $total)/100;
			}
			else
			{
					
				$total = $total - $number;
			}
		}


	}

	$price['total'] = $total;
	$price['formated_total'] =   number_format($price['total'], 2, ',', '.');;
	return $price;
}
/************************************************/
/************************************************ /
function clean_up_cart($uid)
{
	$sql = new db();
	$sql2 = new db();
	$sql->db_Select("eshop_cart","productid","uid = $uid AND status = 0");
	$tmp = execute_multi($sql,0);
	for ($i=0;count($tmp) > $i;$i++)
	{
		$sql->db_Select("user_images","id","uid = $uid AND id = ".$tmp[$i]['productid']);
		if ($sql->db_Rows() == 0)
		{
			$sql2->db_Delete("user_cart","uid = $uid AND status = 0 AND productid = ".$tmp[$i]['productid']);
		}
	}
}
/************************************************/

/************************************************ /
function get_orders($settings)
{
	global $sql,$loaded_modules;
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	$tables = "eshop_orders";
	$filters = $settings['filters'];
	$sort_direction = ($filters['sort_direction'])? $filters['sort_direction'] :'DESC';
	$sort_field = ($filters['sort_field']) ? $filters['sort_field'] : 'id';
	if (isset($filters['archive'])) {
		$q_ar[] = " archive = ".$filters['archive'];
	}
	if (isset($filters['missing']))
	{
		$q_ar[] = $filters['missing']." IS NULL ";
	}
	if (isset($filters['custom_field']))
	{
		if ($filters['custom_field'] == 'uname') {
			$sql->db_Select("users","id","uname = '".$filters['custom_value']."'");
			$tmp = execute_single($sql);
			$filters['custom_value'] = $tmp['id'];
			$filters['custom_field'] = 'uid';
		}
		$q_ar[] = $filters['custom_field']." LIKE '%".$filters['custom_value']."%'";
	}
	if (isset($filters['status'])) {
		$q_ar[] = " status = ".$filters['status'];
	}
	if (isset($filters['uid'])) {
		$q_ar[] = " uid = ".$filters['uid'];
	}
	if (isset($filters['payment_method']))
	{
		$q_ar[] = " payment_method = ".$filters['payment_method'];
	}
	if ($sort_field == 'uname')
	{
		$tables .= " INNER JOIN users ON (users.id=eshop_orders.uid) ";
		$fields = "eshop_orders.*";
	}
	if (count($q_ar) > 1)
	{
		$q = implode(" AND ",$q_ar);
	}
	else {
		$q = $q_ar[0];
	}
	$q .= " ORDER BY $sort_field $sort_direction ";

	if (count($q_ar) == 0)
	{
		$query_mode='no_where';
	}
	else {
		$query_mode = 'default';
	}

	if ($settings['return'] == 'multi' OR $settings['return'] == 'single') //Return all results, NO PAGINATION
	{
		if ($settings['limit'])
		{
			$q .= "LIMIT ".$settings['limit'];
		}
		$sql->db_Select("eshop_orders",$fields,$q,$query_mode);
		if ($sql->db_Rows())
		{
			$orders = execute_multi($sql);
		}
	}//END ALL RESULTS
	elseif ($settings['return'] == 'paginated') // RETURN PAGINATED RESULTS
	{
		$sql->db_Select($tables,$fields,$q,$query_mode);//GET THE TOTAL COUNT
		if ($settings['debug'])
		{
			echo "<br>SELECT $fields FROM $tables WHERE $q<br>";
		}
		if ($sql->db_Rows()) //FOUND RESULTS NOW GET THE ACTUAL DATA
		{
			$total = $sql->db_Rows();
			$current_page = ($settings['page']) ? $settings['page'] : 1;
			$results_per_page =  $settings['limit'];
			if (isset($settings['start']))
			{
				$start = $settings['start'];
			}
			else {
				$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
			}
			$q .= " LIMIT $start,".$results_per_page;
			$sql->db_Select($tables,$fields,$q,$query_mode);
			$search_results = execute_multi($sql,1);
			if ($settings['debug'])
			{
				echo "<br>SELECT $fields FROM eshop_orders WHERE $q<br>";
			}
			paginate_results($current_page,$results_per_page,$total);
	
			$orders = $search_results;

		}//END FOUND RESULTS

	}//END PAGINATION

	if (count($orders) > 0)
	{

		for ($i=0;count($orders) > $i;$i++)
		{
			$orders[$i]['payment_method'] = eshop_payment_methods($orders[$i]['payment_method']);
			$orders[$i]['details'] = form_settings_array($orders[$i]['details'],"###",":::");
			$orders[$i]['user'] = get_users($orders[$i]['uid'],0);

			if (array_key_exists('lottery',$loaded_modules))
			{
				$details = array();
				$details = $orders[$i]['details'];
				$tmp = explode("---",$details['tickets']);
				$a = array();
				foreach ($tmp as $v)
				{
					$a = explode("|",$v);
					$items[$orders[$i]['id']]['groupid'] = $a[0];
					$items[$orders[$i]['id']]['status'] = $orders[$i]['status'];
					$items[$orders[$i]['id']]['t'][$a[1]]['tickets'] = explode(",",$a[2]);
					if ($settings['get_tickets']) {
						$items[$orders[$i]['id']]['t'][$a[1]]['item'] =  get_products($a[1],$settings['lang'],1);
						$orders[$i]['items'] = $items;
					}
					$orders[$i]['num_tickets'] += count($items[$orders[$i]['id']]['t'][$a[1]]['tickets']);
				}
			}//END LOTTERY
			if (array_key_exists('products',$loaded_modules))
			{
				if ($settings['get_products']) {
					for ($i=0;count($orders) > $i;$i++)
					{
						//echo "SELECT eshop_cart.*,products_lng.title FROM eshop_cart INNER JOIN products_lng ON (products_lng.productid=eshop_cart.productid) WHERE order_id = ".$orders[$i]['id']."<br>";
						$sql->db_Select("eshop_cart INNER JOIN products_lng ON (products_lng.productid=eshop_cart.productid)","eshop_cart.*,products_lng.title,(eshop_cart.quantity * eshop_cart.price) as total","order_id = ".$orders[$i]['id']);
						if ($sql->db_Rows()) {
							$orders[$i]['products'] = execute_multi($sql);
						}
						foreach ($orders[$i]['products'] as $v)
						{
							$orders[$i]['prices'][] = $v['total'];
						}
					}
				}
			}
		}
	}
	if ($settings['return'] == 'single') {
		return $orders[0];
	}
	else {
		return $orders;
	}
}
/************************************************/
/************************************************ /
function eshop_product_details($id,$type='product')
{
	global $sql;

	$sql->db_Select("eshop_product_details","*","productid = $id AND type = '$type'");
	return execute_single($sql);
}
/************************************************/
/************************************************ /
function eshop_shipping($id=0,$include=0,$active=0)
{
	global $sql;
	if ($active) {
		$active_q = " AND active = $active";
	}
	if ($id)
	{
		$sql->db_Select("eshop_shipping","*","shippingid = $id $active_q");

		$s = execute_single($sql,1);
		if ($include)
		{
			$s['methods'] = eshop_shipping_methods($s['shippingid']);
		}
		if ($s['settings']) {
			$s['settings'] = form_settings_array($s['settings'],"###",":::");
		}

		return $s;
	}
	else
	{
		$sql->db_Select("eshop_shipping","*","parent = 0 $active_q ORDER BY orderby");
		$all_methods = execute_multi($sql,1);
		if ($include)
		{

			for ($i=0;count($all_methods) > $i;$i++)
			{
				$all_methods[$i]['methods'] = eshop_shipping_methods($all_methods[$i]['shippingid'],$active);
			}
		}

		for ($i=0;count($all_methods) > $i;$i++)
		{
			$tmp = form_settings_array($all_methods[$i]['settings'],"###",":::");
			unset($all_methods[$i]['settings']);
			$all_methods[$i]['settings'] = $tmp;
		}
		return $all_methods;
	}

}
/************************************************/
/************************************************ /
function eshop_shipping_methods($id=0,$active=0,$filter=0)
{
	global $sql;
	if ($active) {
		$active_q = " AND active = $active";
	}
	if ($filter)
	{
		$filter_q = "shippingid IN(".implode(",",$filter).") ";
	}
	if ($id) {
		$id_q = "parent = '$id'";
	}
	//	echo "$id_q $active_q $filter_q ORDER BY orderby<br>";
	$sql->db_Select("eshop_shipping","*","$id_q $filter_q $active_q ORDER BY orderby");
	if ($sql->db_Rows() > 0)
	{

		return execute_multi($sql,1);

	}
}
/************************************************/
/************************************************ /
function eshop_payment_methods($id=0,$active=0,$filters=0)
{
	global $sql;


	//	echo "parent = '$id' ORDER BY orderby";
	if ($id)
	{
		if ($active) {
			$active_q = " AND active = $active";
		}
		$sql->db_Select("eshop_payment_methods","*","id = $id $active_q ORDER BY orderby");
		if ($sql->db_Rows() > 0)
		{

			$s = execute_single($sql,1);
			$s['settings'] = form_settings_array($s['settings'],"###",":::");
			if ($filters)
			{
				$c = explode(":::",$s['shipping_methods']);
				$s['shipping'] = eshop_shipping_methods(0,0,$c);
			}
			return $s;

		}
	}
	else
	{
		if ($active) {
			$active_q = "active = $active";
		}
		$mode = ($active) ? "default" : "no_where";

		$sql->db_Select("eshop_payment_methods","*","$active_q ORDER BY orderby",$mode);

		if ($sql->db_Rows() > 0)
		{

			$s = execute_multi($sql,1);
			for ($i=0;count($s) > $i;$i++)
			{
				$s[$i]['settings'] = form_settings_array($s[$i]['settings'],"###",":::");
				if ($filters['shipping_methods']) {
					//GET SHIIPPING METHODS FOR THIS PAYMENT METHOD
					$shipping_methods = str_replace(":::",",",$s[$i]['shipping_methods']);
					$sql->db_Select("eshop_shipping","parent,shippingid,shipping,base_cost,settings","shippingid IN ($shipping_methods) ORDER BY orderby");
					if ($sql->db_Rows()) {
						$tmp = array();
						$tmp = execute_multi($sql);
						foreach ($tmp as $v)
						{
							$s[$i]['shipping'][$v['parent']]['methods'][] = $v;
						}
						foreach ($s[$i]['shipping'] as $k=>$v) {
							$sql->db_Select("eshop_shipping","*","shippingid = $k");
							$t = execute_single($sql);
							$t['settings'] = form_settings_array($t['settings'],'###',':::');
							$s[$i]['shipping'][$k]['parent_array'] = $t;
								
						}
					}//END ROWS
				}//END SHIPPING METHODS
				if ($filters['processor']) {
					//PAYMENT PROCESSOR
					$s[$i]['processor'] = eshop_payment_processors($s[$i]['settings']['payment_processor']);
				}
			}//END LOOP
			return $s;

		}
	}

}
/************************************************/

/************************************************ /
function eshop_payment_processors($id=0)
{
	global $sql;

	if ($id)
	{
		$sql->db_Select("eshop_payment_processors","*","id = $id");
		$s = execute_single($sql,1);
		$s['settings'] = form_settings_array($s['settings'],"###",":::");
		$s['required_fields'] =explode(":::",$s['required_fields']);
		return $s;
	}
	else
	{
		$sql->db_Select("eshop_payment_processors","*");
		$s = execute_multi($sql,1);
		for ($i=0;count($s) > $i;$i++)
		{
			$s[$i]['settings'] = form_settings_array($s[$i]['settings'],"###",":::");
			$s[$i]['required_fields'] = explode(":::",$s[$i]['required_fields']);
		}
		return $s;
	}
}
/************************************************/

/************************************************ /
function add_to_cart($uid,$product,$options,$lang)
{
	global $sql;



	//	$price = str_replace(".",",",$price);
	$status = 0;//Active cart object. When order complete set it to 1
	$order_id = 0;//Active cart object. When order complete set it to the order number
	$quantity = ($options['quantity']) ? $options['quantity'] : 1;
	//Analyze options
	$cart_options = analyze_cart_options($options,$lang);


	########################### CALCULATE PRICE #################################################
	$price = ($product['eshop']['price'] * $quantity);//Calculate for the base quantity
	//ADITIONAL
	for ($i=0;count($tmp_opt) > $i;$i++)
	{
		if ($tmp_opt[$i]['modifier_type'] == '%')
		{
			$price = $price + ($tmp_opt[$i]['price_modifier'] * $price)/100;
		}
		elseif ($tmp_opt[$i]['modifier_type'] == '$')
		{
			$price = $price + $tmp_opt[$i]['price_modifier'];
		}
	}

	######################### CHECK CART FOR THIS PRODUCT ########################################
	// First check if the options aggree with the cart
	$sql->db_Select("eshop_cart","productid,quantity,price","status = 0 AND uid = $uid AND productid = ".$product['id']." AND options = '$cart_options'");
	if ($sql->db_Rows())
	{
		$res = execute_single($sql);
		$quantity = $res['quantity'] + 1;
		$price = $price + $res['price'];

		//Found update quantities
		$sql->db_Update("eshop_cart","quantity = '$quantity', price = '$price' WHERE status = 0 AND uid = $uid AND productid = ".$product['id']." AND options = '$cart_options'");

	}
	else //NOT FOUND INSERT IT
	{
		$time = time();
		$sql->db_Insert("eshop_cart","'$uid','".$product['id']."','$quantity','$price','$status','$order_id','$cart_options','$cart_details','$time'");
	}


	return 1;
}
/************************************************/
/************************************************ /
function remove_from_cart($product,$uid,$time,$lang)
{
	global $sql;

	//	echo "status = 0 AND uid = $uid AND productid = ".$product['id']." AND time = '$time'";
	if ($sql->db_Delete("eshop_cart","status = 0 AND uid = $uid AND productid = ".$product['id']." AND date_added = '$time'"))
	{
		return 1;
	}

}
/************************************************/


function eshop_classes($id=0,$module_id=0,$options=0,$return_mode="id",$exclusions=0)
{
	global $sql;

	if ($id)
	{
		$sql->db_Select("eshop_classes","*","id = $id");
		if ($sql->db_Rows())
		{
			$res = execute_single($sql);
			if ($options)
			{
				$res['options'] = eshop_classes_options($res['id']);
			}
			return $res;
		}

	}
	else
	{
		//See if there are any classes to exclude from this product
		if (is_array($exclusions) AND $module_id)
		{
			$sql->db_Select("eshop_classes_exclusions","class_id","module_id = $module_id AND itemid = ".$exclusions['itemid']);
			//			echo "SELECT class_id FROM eshop_classes_exclusions WHERE module_id = $module_id AND itemid = ".$exclusions['itemid'];
			if ($sql->db_Rows())
			{
				$excluded = execute_multi($sql);
				foreach ($excluded as $v)
				{
					$tmp[] = $v['class_id'];
				}
				if ($exclusions['mode'] == 'remove') //APPLIES WHEN WE DON'T WANT TO RETURN THE EXCLUDED CLASSES
				{
					$excluded_list = " AND id NOT IN (".implode(",",$tmp).") ";
				}
				elseif ($exclusions['mode'] == 'add') //RETURNS THE EXCLUDED CLASSES ONLY
				{
					$excluded_list = " AND id IN (".implode(",",$tmp).") ";
				}
			}
			else {
				if ($exclusions['mode'] == 'add')
				{
					return false;
				}
			}
		}//END EXCLUSIONS
		if ($module_id)
		{
			$sql->db_Select("eshop_classes","*","module_id = $module_id $excluded_list");
		}
		else
		{
			$sql->db_Select("eshop_classes","*");
		}

		if ($sql->db_Rows())
		{
			$res = execute_multi($sql,1);
			if ($return_mode != "id")
			{
				$tmp = array();
				foreach ($res as $v)
				{
					$tmp[$v['title']] = $v;
					if ($options)
					{
						$tmp[$v['title']]['options'] = eshop_classes_options($v['id']);
					}
				}
				unset($res);
				$res = $tmp;

			}
			else
			{
				if ($options)
				{
						
					for ($i=0;count($res) > $i;$i++)
					{
						$res[$i]['options'] = eshop_classes_options($res[$i]['id']);
					}
				}
			}

			return $res;
		}//END FOUND
	}//END MANY

}

function eshop_classes_options($classid)
{
	global $sql;

	$sql->db_Select("eshop_classes_options","*","classid = $classid ORDER BY orderby");
	if ($sql->db_Rows())
	{
		return execute_multi($sql,1);
	}
}

function eshop_classes_get_option($optionid,$table,$settings=0)
{
	global $sql;

	$sql->db_Select($table,"*","optionid = $optionid");
	if ($sql->db_Rows())
	{
		$a =execute_single($sql,1);
		if ($settings['get_product']) {
			$a['product'] = get_products($a['itemid'],$settings['lang'],$settings['active']);
		}
		return $a;
	}
}

function eshop_copy_class_to_local($settings)
{
	global $sql;
	$table = $settings['module']."_classes";
	$table_lng = $settings['module']."_classes_lng";
	$options_table = $settings['module']."_classes_options";
	$options_table_lng = $settings['module']."_classes_options_lng";
	$id = $settings['itemid'];

	//First get the class a hand
	$class = eshop_classes($settings['class_id'],$settings['module_id'],1);
	$title = $class['title']."_copy";
	$classtext = $class['classtext']." - copy";
	$module_id = $settings['module_id'];
	$type = $class['type'];
	$option_type = $class['option_type'];
	$availability = $class['active'];
	$global_id = $class['id'];
	//	print_r($class);
	//Create the class entry
	$sql->db_Insert("$table","'',$global_id,'$id','$orderby','1','$title','$classtext','$module_id','$type','$option_type'");
	$cid = mysql_insert_id();
	//NOW COPY THE CLASS OPTIONS
	foreach ($class['options'] as $k => $v)
	{
		$pos = $i;
		$title = $v['option_name'];
		$price_modifier = $v['price_modifier'];
		$modifier_type = $v['modifier_type'];
		$orderby = $v['orderby'];

		$sql->db_Insert("$options_table","'',".$cid.",$id,'".$orderby."','".$v['option_value']."','".$v['price_modifier']."','".$v['modifier_type']."','".$v['option_name']."'");


	}
}
/************************************************ /
function vat_price($price_without_vat,$vat="default",$mode='plus')
{
	global $loaded_modules;
	$vat = ($vat == 'default') ? $loaded_modules['eshop']['settings']['vat'] : $vat;
	if (is_array($price_without_vat)) {
		$price_without_vat = array_sum($price_without_vat);

	}
	if ($mode == 'plus') {
		$price_with_vat = $price_without_vat + ($vat*($price_without_vat/100)); // work out the amount of vat
	}
	else {
		$price_with_vat = $price_without_vat - ($vat*($price_without_vat/100)); // work out the amount of vat
	}
	return $price_with_vat;
}
/************************************************/
/************************************************ /
function eshop_order_status_codes($settings=0)
{
	global $sql;
	$sql->db_Select("eshop_order_status_codes","*");
	return execute_multi($sql);

}
/************************************************/
/************************************************ /
function eshop_monthly_payments($settings)
{
	global $sql;
	$fields = ($settings['fields']) ? $settings['fields'] : "*";
	if ($settings['id']) {
		$q[] = 'id = '.$settings['id'];
	}
	if ($settings['show']) {
		$q[] = 'eshop_cc_rates.show = '.$settings['show'];
	}

	if (is_array($q)) {
		$query = implode(" AND ",$q);
		$mode = 'default';
	}
	else {
		$mode = 'no_where';
	}
	$sql->db_Select("eshop_cc_rates",$fields,$query,$mode);

	if ($settings['debug']) {
		echo "SELECT $fields FROM eshop_cc_rates WHERE $query";
	}
	$rates = execute_multi($sql);

	if ($settings['exclussions']) {
		$sql->db_Select("eshop_cc_rates_exclussions","*","itemid = ".$settings['itemid']);
		if ($sql->db_Rows()) {
			$exclussions = execute_multi($sql);
			for ($i=0;count($rates) > $i;$i++)
			{
				foreach ($exclussions as $v)
				{
					if ($v['payment_id'] == $rates[$i]['id']) {
						$rates[$i]['monthly_charge'] = $v['monthly_charge'];
					}
				}
			}
		}
	}
	return $rates;
}
/************************************************/
?>