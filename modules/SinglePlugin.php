<?php


Class SinglePlugin  {

	
	public static $instance;
	
	private $current_module;
	
	public $AvailPlugin = array();
	
	private function __construct() {
	
	}
	
	
    public static function start()
    {
    	
    	$className = __CLASS__;
        if (!isset(self::$instance)) {
        	
            self::$instance = new $className();
            self::$instance->RegisterPlugin();
        }

        return self::$instance;
        
    }
    
    
    public function getinstance() {
    	return self::$instance;
    }
    
	
 	public function __clone()
    {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup()
    {
        trigger_error('Unserializing is not allowed.', E_USER_ERROR);
    }

    
    public function LoadBoxPlugin(array $arrays,$settings = array()) {
       global $sql;   
       $tmp=array(); 
       $sql->db_Select("plugins","*","active=1");
       $row = execute_multi($sql);
    	foreach ($row as $key => $value ) {		
    		$ar=json_decode($value['settings'],true);
    		foreach ($ar['boxID'] as $k => $v ) {
    		$ars =array_flip($arrays);
    			if ( array_key_exists($k,$ars ) )  {
    				if ($settings['overwrite']) {
    					$arrays[$ars[$k]]=$v;
    				} else {
    					$tmp[]=$v;
    				}
    			} 
    		}
    	} 
    	
    	return array_merge($arrays,$tmp);
    	
    }
    
    public function RegisterPlugin() {
    	global $sql;
       $sql->db_Select("plugins","*","active=1");
       $row = execute_multi($sql);
   	   
       $this->AvailPlugin = $row;
       HookParent::getInstance()->doBindHookGlobally("PreRender", "SinglePlugin::LoadPlugin");
    	
    }
    
	
    
    public static function LoadPlugin ( $args) {
       global $sql;
          
   	   foreach ( self::getinstance()->AvailPlugin as $key => $value ) {
    		$ar=json_decode($value['settings'],true);
    		if ( in_array($args['module']['name'], $ar['current_module']) )  {
    			if (isset($args['ajax'])) {
    					include(ABSPATH."/ajax/plugins/".$value['name']."/".$value['name'].".php");
    			} else {
    					include(ABSPATH."/plugins/".$value['name']."/index.php");
    			}
    		}
    	}  	   	
    
    }
 
    
}

?>