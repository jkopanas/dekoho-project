<?php

/**
 * messages
 * 
 * @package smsportal
 * @author george
 * @copyright 2011
 * @version $Id$
 * @access public
 */

class messages
{
	public $users;
	
    function messages()
    {
        $this->tables['smsDLR'] = array('table'=>'smsDLR','key'=>'id');
        $this->users = new user();
    }
    
    function sendMessage($smsData)
    {
        //These values must be replaced by the correct!!!
        $uid=1;
        $instanceID=1;
        $batchID = time().$instanceID.$uid;       
               
        /*
        data structure
        $smsData[$i]['message']
        $smsData[$i]['originator']
        $smsData[$i]['number']
        $smsData[$i]['date']
        $smsData[$i]['utf8']
        
        */
        
        /**
        * formats each line as the java app expects.
        * Also includes blank items for the java array to be created correctly.
        * csv order : id,instanceID,batchD,originator,number,message,date,uid,msgid,dlr,utf8
        */
        
        if ($fp = @fopen("$batchID.csv", 'w')) 
        {
           foreach ($smsData as $line) 
           {
               if($line['utf8']!=1) $line['utf8']=0;
               $linedata = array("" , $instanceID , $batchID , $line['originator'] , $line['number'],$line['message'],$line['date'],$uid,"","",$line['utf8']);
               
               fputcsv($fp, $linedata ,",");
           }
           fclose($fp);
        }
        
        
    }
    
    function searchMessages($settings)
    {
    	//print_r($settings);
        global $sql;
        //$users->tables["users"]["table"]
        $fields = (!$settings['fields']) ? "*" : $settings['fields'];
        $orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
    	$way = ($settings['way']) ? "  ".$settings['way'] : "";
    	$limit = ($settings['results_per_page']) ? "LIMIT ".$settings['results_per_page'] : "";
    	$settings['return'] = ($settings['return']) ? $settings['return'] : "multi";
    	if (is_array($settings['searchFields'])) {
    		foreach ($settings['searchFields'] as $k => $v) {
    			$q[] = "$k = '$v'";
    		}
    	}//END SETUP SEARCH FIELDS
    	if(!$settings['searchFields']['archived']==1)
    	{
    		$settings['searchFields']['archived']=0;
    	}
    	if (is_array($settings['searchFieldsLike'])) {
    		foreach ($settings['searchFieldsLike'] as $k => $v) {
    			$q[] = "$k LIKE '%$v%'";
    		}
    	}//END SETUP SEARCH FIELDS WITH LIKE
    	
    	if (is_array($settings['greaterThan'])) {
    		foreach ($settings['greaterThan'] as $k => $v) {
    			$q[] = $k." > ".$v;
    		}
    	}//END SETUP SEARCH FIELDS WITH LIKE
    	
    	if (is_array($settings['lessThan'])) {
    		foreach ($settings['lessThan'] as $k => $v) {
    			$q[] = $k." < ".$v;
    		}
    	}
    	
        if (is_array($settings['missingFields'])) {
    		foreach ($settings['missingFields'] as $k => $v) {
    			$q[] = "$k != '$v'";
    		}
    	}//END SETUP SEARCH FIELDS
    	 
        if($q)
        {
            $query=implode(" AND ",$q);
        }

        
        if (!$query) 
		{
			$query_mode='no_where';
		}
		else {
			$query_mode = 'default';
		}

		if ($settings['return'] == 'multi') //Return all results, NO PAGINATION
		{
			$sql->db_Select($this->tables['smsDLR']['table'],$fields,"$query $orderby $way $limit",$query_mode);
			if ($settings['debug']) {
				echo "SELECT ".$fields." FROM ".$this->tables['smsDLR']['table']." WHERE $query $orderby $way $limit<br>";
			}
			if ($sql->db_Rows()) 
			{
				$res = execute_multi($sql);
				
			}
		}//END ALL RESULTS
		elseif ($settings['return'] == 'paginated') // RETURN PAGINATED RESULTS
		{
			$sql->db_Select($this->tables['smsDLR']['table'],'id',"$query $orderby $way",$query_mode);//GET THE TOTAL COUNT
			
				if ($settings['debug']) 
				{
					echo "SELECT id FROM ".$this->tables['smsDLR']['table']." WHERE $query $orderby $way<br>";
				}
			if ($sql->db_Rows()) //FOUND RESULTS NOW GET THE ACTUAL DATA
			{
				$total = $sql->db_Rows();
				$current_page = ($settings['page']) ? $settings['page'] : 1;
				$results_per_page =  $settings['results_per_page'];
				if (isset($settings['start'])) 
				{
					$start = $settings['start'];
				}
				else {
					$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
				}
				$limit = "LIMIT $start,".$results_per_page;
				$sql->db_Select($this->tables['smsDLR']['table'],$fields,"$query $orderby $way $limit",$query_mode);
				if ($settings['debug']) {
					echo "<br>SELECT $fields FROM ".$this->tables['smsDLR']['table']." WHERE $query $orderby $way $limit";
				}
				$res = execute_multi($sql,1);
				paginate_results($current_page,$results_per_page,$total);
				
			}//END FOUND RESULTS
		}//END PAGINATION        
    	elseif ($settings['return'] == 'single') // RETURN PAGINATED RESULTS
    	{
			$sql->db_Select($this->tables['smsDLR']['table'],$fields,"$query $orderby $way $limit",$query_mode);
			if ($settings['debug']) {
				echo "SELECT ".$fields." FROM ".$this->tables['smsDLR']['table']." WHERE $query $orderby $way $limit<br>";
			}
			if ($sql->db_Rows()) 
			{
				$res = execute_single($sql);	
			}
			
			
			if ($settings['getUserDetails']) {
						$res['userDetails'] = $this->users->userDetails($res['uid'],array('fields'=>'id,uname'));
			}//END USER DETAILS
    				
    	}
    	elseif ($settings['return'] == 'count') // RETURN COUNT RESULTS
		{
			$sql->db_Select($this->tables['smsDLR']['table'],"count(".$this->tables['smsDLR']['table'].".id) as total","$query $orderby $way $limit",$query_mode);
			if ($settings['debug']) {
				echo "SELECT count(".$this->tables['smsDLR']['table'].".id) as total FROM ".$this->tables['smsDLR']['table']." WHERE $query $orderby $way $limit<br>";
			}
			$res = execute_single($sql);
			return $res['total'];
		}
        	
    	if ($settings['return']!='single') // RETURN PAGINATED RESULTS
    	{
			if ($settings['getUserDetails']) {
				for ($i=0;count($res) > $i;$i++)
				{
					$res[$i]['userDetails'] = $this->users->userDetails($res[$i]['uid'],array('fields'=>'id,uname'));
				}
			}//END USER DETAILS
    	}
			
        return $res;
    }
    
   
    function calculateCost($itemid)
    {
        return $cost;
    }
    
    function proccessDLRcsv($filename)
    {
        $home = $_SERVER['DOCUMENT_ROOT'];
        $sfp = gzopen($home."/sharedocs/".$filename.".gz", "rb");
        $fp = fopen($home."/sharedocs/test/".$filename, "w");
        
        while ($string = gzread($sfp, 4096)) {
            fwrite($fp, $string, strlen($string));
        }
        gzclose($sfp);
        fclose($fp);
        
    }
   
    function importDLR($filename)
    {
        global $sql;
        $home = $_SERVER['DOCUMENT_ROOT'];
           
        $query = "LOAD DATA LOCAL INFILE '$home/sharedocs/$filename'
        INTO TABLE smsDLR
        FIELDS TERMINATED BY ',' 
        LINES TERMINATED BY '\n' 
        (msgid,originator,destination,messageText,dateAdded,dateSend,uid,batchID,dlrStatus,error)";
        
        $sql->q($query);
        
    }
    
    function exportDLR($filename)
    {
       
    }
     
   
    function requestDLR($id)
    {
		   
    }
    
    function getPossibleStatus()
    {
    	global $sql;
    	$sql->db_Select($this->tables['smsDLR']['table'],"dlrStatus","GROUP BY dlrStatus",$mode="no_where");
    	$res = execute_multi($sql,1);
    	
    	return $res;
    }
    
    function getSingle($id)
    {
    	$settings = array("searchFields"=>array("id"=>$id),"debug"=>0,'return'=>'single',"getUserDetails"=>1);
    	return $this->searchMessages($settings);
    }
    
    
	    
	function setupSearchData($data)
	{
		$posted_data = $data['data'];
		$secondaryData = $data['secondaryData'];
		
		$secondaryData['searchFieldsLike'] = $data["likeData"];

		if ($secondaryData['custom_field'] AND $secondaryData['custom_value']) {
			$searchFilters['custom_field'] = $secondaryData['custom_field'];
			$searchFilters['custom_value'] = $secondaryData['custom_value'];
		}
		if ($secondaryData['missing']) {
			$searchFilters['missing'] = $secondaryData['missing'];
		}
		
		if ($secondaryData['dateFrom']) {
			$tmp = explode("/",$secondaryData['dateFrom']);
			$secondaryData['greaterThan']['dateAdded'] = mktime(23,59,0,$tmp[1],$tmp[0],$tmp[2]);
		}
		
		if ($secondaryData['dateTo']) {
			$tmp = explode("/",$secondaryData['dateTo']);
			$secondaryData['lessThan']['dateAdded'] = mktime(23,59,0,$tmp[1],$tmp[0],$tmp[2]);
		}
		
		$secondaryData['searchFields'] = $posted_data;
		$secondaryData['page'] = ($_GET['page']) ? $_GET['page'] : 1;
		$secondaryData['searchFields'] = $posted_data;
		
		$secondaryData['filters'] = $searchFilters;
		$secondaryData['debug'] = ($secondaryData['debug']) ? $secondaryData['debug'] : 0;
		$secondaryData['return'] = ($secondaryData['return']) ? $secondaryData['return'] : 'paginated';
		$secondaryData['getTotalAmount'] = ($secondaryData['getTotalAmount']) ? $secondaryData['getTotalAmount'] : 0;
		$secondaryData['getUserDetails'] = ($secondaryData['getUserDetails']) ? $secondaryData['getUserDetails'] : 0;
		$secondaryData['fields'] = ($secondaryData['fields']) ? $secondaryData['fields'] : '*';
		$secondaryData['page'] = ($_GET['page']) ? $_GET['page'] : 1;	
		
		return $secondaryData;
	}//END FUNCTION
}

?>