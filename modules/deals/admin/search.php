<?php
include("../../../manage/init.php");//load from manage!!!!

$deal = new deals();
$settings=array('fields'=>'*','return'=>'paginated','results_per_page'=>20);
$deals = $deal->searchDeals($settings);

$categories = $deal->dealCategories();
foreach($categories as $category)
{
    $temp[$category['categoryid']]=$category;
}
$categories=$temp;

$shops = $deal->dealShops();
foreach($shops as $shop)
{
    $temp[$shop['id']]=$shop;
}
$shops=$temp;


$smarty->assign('categories',$categories);
$smarty->assign('shops',$shops);
$smarty->assign('deals',$deals);

$smarty->assign("include_file","modules/deals/admin/search.tpl");
$smarty->display("admin/home.tpl");

?>