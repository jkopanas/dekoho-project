<?php
include("../../../manage/init.php");//load from manage!!!!

if ($news_module = module_is_active("news",1,1)) 
{
	$smarty->assign("latest_news",get_latest_news($news_module['settings']['latest_news_admin'],DEFAULT_LANG));
	$smarty->assign("MODULE_FOLDER",URL."/".$news_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$news_module['settings']);
	$smarty->assign("allcategories",get_all_news_categories(DEFAULT_LANG));//assigned template variable allcategories
//print_r(get_latest_news($news_module['settings']['latest_news_admin'],DEFAULT_LANG));
}


$smarty->assign("menu",$news_module['name']);
$smarty->assign("submenu","main");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/news/admin/main.tpl");
$smarty->display("admin/home.tpl");

?>