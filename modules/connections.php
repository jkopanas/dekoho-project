<?php
class connections
{
    
    function getConnections($args)
    {
        global $sql;
        
        $sql->db_Select("smsConnections","*",$args,"notdefault");
        
        return execute_multi($sql);
    }
    
    function getConnectionDetails($id)
    {
        global $sql;
        $sql->db_Select("smsConnections","*", "id=$id");
        return execute_single($sql);
    }
    
    function newConnection($settings)
    {
        global $sql;        
        
        foreach($settings as $k=>$v)
        {
            $args.="$k,";
            $vals.="'$v',";
        }
        $args.="date_added";
        $vals.=time();  
        
        $sql->db_Insert("smsConnections ($args)",$vals);
        
        //echo "INSERT INTO smsConnections ($args) VALUES ($vals)";
    }
    
    function updateConnection($connectionID,$settings)
    {
        global $sql;
        
        foreach($settings as $k=>$v)
        {
            $args.="$k='$v',";
        }
        $args = trim($args,",");
        
        $sql->db_Update("smsConnections",$args." WHERE id=$connectionID");
        
    }
    
    function deleteConnection($connectionID)
    {
        global $sql;
        $sql->db_Delete("smsConnections","id=$connectionID");
    }
    
    function getConnectionDependencies($id)
    {
        global $sql;
        $routes = new routes;
        $settings=array("searchFields"=>array("connectionID"=>$id),"debug"=>0);
        $res['routes']= $routes->searchRoutes($settings);
        
        
    }
}

?>