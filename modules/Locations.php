<?php
class Locations
{
	var $itemid;
	var $module;
	var $settings;
	
	function Locations($settings)
	{
		$this->itemid = $settings['itemid'];
		$this->module = $settings['module'];
		$this->settings = $settings;
		
	}//END FUNCTION
	
	function GetZipCode($settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		if ($settings['zip']) {
			$q[] = "zip = ".$settings['zip'];
		}
		if ($settings['id']) {
			$q[] = "id = ".$settings['id'];
		}
		if ($settings['like']) {
			$q[] = "zip LIKE '".$settings['like']."$q%'";
		}
		if (is_array($q)) {
			$query = implode(' AND ',$q);
			$mode = 'default';
		}
		else {
			$mode = 'no_where';
		}
		
		if ($settings['sort']) {
			$query .= " ORDER BY ".$settings['sort'];
		}
		if ($settings['way']) {
			$query .= " ".$settings['way'];
		}
		if ($settings['limit']) {
			$query .= " LIMIT ".$settings['limit'];
		}
		if ($settings['debug']) {
			echo "SELECT $fields FROM locations WHERE $query";
		}
		$sql->db_Select("locations",$fields,$query,$mode);
		if ($sql->db_Rows()) {
		if ($settings['zip'] OR $settings['id']) {//RETURN ONE
			$res = execute_single($sql);
			$res['settings'] = form_settings_array($res['settings']);
			if ($settings['aliases']) {
				$res['aliases'] = $this->GetAliases($res['id']);
			}//END ALIASES
		}//END SINGLE
		else {
			$res = execute_multi($sql);
			for ($i=0;count($res) > $i;$i++)
			{
				$res[$i]['settings'] = form_settings_array($res[$i]['settings']);
			}
		}//END MULTI
		}//END ROWS
		return $res;
	}//END FUNCTION
	
	function GetAliases($locationid,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		$q[] = "locationid = $locationid";
		if (is_array($q)) {
			$query = implode(' AND ',$q);
			$mode = 'default';
		}
		else {
			$mode = 'no_where';
		}
		
		if ($this->settings['sort']) {
			$query .= " ORDER BY ".$settings['sort'];
		}
		if ($settings['way']) {
			$query .= " ".$settings['way'];
		}
		if ($settings['limit']) {
			$query .= " LIMIT ".$settings['limit'];
		}
		if ($settings['debug']) {
			echo "SELECT $fields FROM locations_aliases WHERE $query";
		}
		$sql->db_Select("locations_aliases",$fields,$query);
		if ($sql->db_Rows()) {
			$res = execute_multi($sql);
		}
		return $res;
	}//END FUNCTION
	
	function GetAlias($settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		if ($settings['id']) {
			$q[] = "id = ".$settings['id'];
		}
		if ($settings['alias']) {
			$q[] = "alias = '".$settings['alias']."'";
		}
		if (is_array($q)) {
			$query = implode(" AND ",$q);
		}
		if ($settings['debug']) {
			echo "SELECT $fields FROM locations_aliases WHERE $query";
		}
		$sql->db_Select("locations_aliases",$fields,$query);
		if ($sql->db_Rows()) {
			$res = execute_single($sql);
			if ($settings['get_location']) {
				$res['location'] = $this->GetZipCode(array('id'=>$res['locationid']));
			}
			
		}//END ROWS
		return $res;
	}//END FUNCTION
	
	function GetLocationsItems($settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		$q[] = "itemid = ".$settings['itemid'];
		$q[] = "module = '".$this->module['name']."'";
		if ($settings['locationid']) {
			$q[] = "locationid = ".$settings['locationid'];
		}
		if ($settings['aliasid']) {
			$q[] = "aliasid = ".$settings['aliasid'];
		}
		if (is_array($q)) {
			$query = implode(" AND ",$q);
		}
		$sql->db_Select("locations_items",$fields,$query);
		if ($settings['debug']) {
			echo "SELECT $fields FROM locations_items WHERE $query<br>";
		}
		if ($sql->db_Rows()) {
			if ($settings['itemid']) {
				$res = execute_single($sql);	
				$res['details'] = form_settings_array($res['details']);
				$res['item_json'] = json_encode($res);
				if ($settings['getLocation']) {
					$res['location'] = $this->GetZipCode(array('id'=>$res['locationid'],'debug'=>0));
				}
				if ($settings['getAlias']) {
					$res['alias'] = $this->GetAlias(array('id'=>$res['aliasid']));
				}
			}//END SINGLE
			else {
				$res = execute_multi($sql);
				for ($i=0;count($res) > $i;$i++)
				{
					$res[$i]['details'] = form_settings_array($res[$i]['details']);
				}
			}//END MULTI
		}
		return $res;
	}//END FUNCTION
	
	
}//END CLASS
?>