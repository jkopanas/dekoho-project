<?php
include("../../../manage/init.php");//load from manage!!!!

if ($content_module = module_is_active("content",1,1)) 
{
	$smarty->assign("MODULE_FOLDER",URL."/".$content_module['folder']."/admin");
	
	$t = new textparse();
	$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];
	
	

	
################################################## LOAD content ############################################################
	$content = get_content($id,DEFAULT_LANG);

	if (!empty($content)) 
	{  
	$sql->db_Select("content_page_categories","catid","contentid = $id");
	$all_content_categories = execute_multi($sql);
	$smarty->assign("categoryids",$all_content_categories);//assigned template variable categories
	$smarty->assign("allcategories",get_all_content_categories(DEFAULT_LANG));//assigned template variable allcategories
	$smarty->assign("more_content",get_latest_content($content_module['settings']['latest_content'],DEFAULT_LANG,"no",$content['catid']));
	$smarty->assign("extra_fields",get_content_extra_fields("Y",DEFAULT_LANG,$id));

	$smarty->assign("id",$id);
	$smarty->assign("tags",get_content_tags($id,","));
	$smarty->assign("nav",content_cat_nav($content['catid'],DEFAULT_LANG));

	


	$smarty->assign("content",$content);
	$smarty->assign("content_module",$content_module);
	$smarty->assign("action","update");
	}//END OF content found
	else 
	{  
		header("Location: search.php"); 
		exit();
	}//END OF ELSE
################################################ END OF LOAD content #########################################

	####################### LOAD AJAX  ###########################
	include(ABSPATH."/".$content_module['folder']."/admin/ajax_functions.php");
	$smarty->assign("USE_AJAX","/scripts/ajax_content.js");
	###################### END OF AJAX #######################################	
			
}

$smarty->assign("related",get_related_content($id,DEFAULT_LANG));
$smarty->assign("menu",$content_module['name']);
$smarty->assign("submenu","modify");//USED ON SUBMENUS
$smarty->assign("section","modify");//USED ON ADDITIONAL MENUS
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/content/admin/content_modify_related.tpl");
$smarty->display("admin/home.tpl");

?>