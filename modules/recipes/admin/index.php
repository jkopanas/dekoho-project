<?php
include("../../../manage/init.php");//load from manage!!!!

if (array_key_exists("recipes",$loaded_modules)) {
	$current_module = $loaded_modules['recipes'];
	$smarty->assign("MODULE_FOLDER",$current_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$current_module['settings']);
	$items_per_page = ($current_module['settings']['items_per_page_admin']) ? $current_module['settings']['items_per_page_admin'] : 30;
	$latest = latest_recipes(array('results_per_page'=>$items_per_page,'active'=>2,'get_provider'=>1,'get_user'=>1,'debug'=>0));
	$smarty->assign("latest",$latest['results']);
}
else {
	exit();
}


$smarty->assign("menu",$current_module['name']);
$smarty->assign("submenu","main");
$smarty->assign("page_title",SITE_NAME." Administration | ".$current_module['menu_title_admin']);
$smarty->assign("include_file",$current_module['folder']."/admin/main.tpl");
$smarty->display("admin/home.tpl");

?>