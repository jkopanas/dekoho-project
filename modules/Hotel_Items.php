<?php

class Hotel_Items extends Items {
	 
	
	public function __construct() {
		
		
	}
	
	
	public function SaveHotelItem($data) {
		global $sql;
		$t = new Parse();
		$id = $this->SaveItem($data);
		$hotel_id = ($id!=0)?$id:$data['id'];
		
		
		
		$ar = array("totalRooms"=>$data['totalrooms']['value'],"chain"=>$data['hotel_chain']['value']);
		$settings = json_encode($ar);
		
		$sql->q("UPDATE hotel SET stars=".$data['stars']['value'].",pets=".$data['pets']['value'].",settings ="."'$settings'"." WHERE id=".$hotel_id);
		
		
		$sql->db_Delete("hotel_types","hotelid =".$hotel_id);	
	if ($data['typesIDs']) {
			foreach ($data['typesIDs']['value'] as $v) {
				$sql->q("INSERT INTO hotel_types (hotelid, itemid) VALUES (".$hotel_id.",".$v.")");
				
			}
		}
		$sql->db_Delete("hotel_themeshotels","hotelid =".$hotel_id);
	if ($data['themesIDs']) {	
			foreach ((array)$data['themesIDs']['value'] as $v) {
				$sql->q("INSERT INTO hotel_themeshotels (hotelid, itemid) VALUES (".$hotel_id.",".$v.")");
			}
	}
	
	  $sql->db_Delete("hotel_facilities","hotelid =".$hotel_id);
	if ($data['facilitiesIDs']) {	
			foreach ($data['facilitiesIDs']['value'] as $v) {
				$sql->q("INSERT INTO hotel_facilities (hotelid, itemid) VALUES (".$hotel_id.",".$v.")");
				
			}
	}
		$sql->db_Delete("hotel_services","hotelid =".$hotel_id);
	if ($data['servicesIDs']) {
			
			foreach ($data['servicesIDs']['value'] as $v) {
				$sql->q("INSERT INTO hotel_services (hotelid, itemid) VALUES (".$hotel_id.",".$v.")");
				
			}
		}
		
		
	}//end function
	
	
	
} // end class




?>