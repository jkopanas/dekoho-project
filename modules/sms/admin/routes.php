<?php
include("../../../manage/init.php");//load from manage!!!!

$routes = new routes;
$conns = new connections;

$settings = array("data" =>"","secondaryData"=>array("results_per_page"=>"10","orderby"=>"id","way"=>"desc","debug"=>"1"));
$data = $routes->searchRoutes($routes->getSearchData($settings,1));

$connections=$conns->getConnections();
global $sql;
$sql->db_Select("dialing_codes","id,Country,Code");
$countries = execute_multi($sql);
$smarty->assign("countries",$countries);
$smarty->assign("connections",$connections);
$smarty->assign("items",$data);
$smarty->assign("submenu","routes");
$smarty->assign("include_file","modules/sms/admin/routes.tpl");
$smarty->display("admin/home.tpl");
?>