<?php
include("../../../manage/init.php");//load from manage!!!!

$trans = new messages();

foreach ($_POST as $k=>$v)
{
	if($v)	$posted_data[$k] = $v;
}

$smarty->assign("data",$posted_data);

$missing_data = array();
$smarty->assign("avStatus",$trans->getPossibleStatus());
$smarty->assign("items",$trans->searchMessages(array("debug"=>0,'return'=>'paginated','results_per_page'=>10,'page'=>1,'searchFields'=>$posted_data,'missingFields'=>$missing_data,'getUserDetails'=>1)));

$smarty->assign("menu",$users_module['name']);
$smarty->assign("submenu","main");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/sms/admin/main.tpl");
$smarty->display("admin/home.tpl");

?>