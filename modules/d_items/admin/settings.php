<?php
include("../../../manage/init.php");//load from manage!!!!
$cat = ($_GET['cat']) ? $_GET['cat'] : "general";
if ($d_items_module = module_is_active("d_items",1,1,0,$cat)) 
{
	if ($_POST)
	{
		$t = new textparse();
		foreach ($_POST as $key => $val)
		{
			$val = $t->formtpa($val);
			$sql->db_Update("modules_settings","value = '$val' WHERE name = '$key' AND module_id = ".$d_items_module['id']);
//			echo "UPDATE modules_settings SET value = '$val' WHERE name = '$key' AND module_id = ".$d_items_module['id']."<br>";
		}
		$q = (e_QUERY) ? "?".e_QUERY : "";
		header("Location: ".URL."/".$d_items_module['folder']."/admin/settings.php$q");
		exit();
	}
	
	// GET ALL SETTINGS GROUPS
	$sql->db_Select("modules_settings","category","module_id = ".$d_items_module['id']." GROUP BY category");
	if ($sql->db_Rows()) 
	{
		$smarty->assign("all_modules",execute_multi($sql));
	}
	$smarty->assign("MODULE_CAT",$cat);
	$smarty->assign("latest_d_items",get_latest_d_items($d_items_module['settings']['latest_d_items_admin'],DEFAULT_LANG));
	$smarty->assign("MODULE_FOLDER",URL."/".$d_items_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$d_items_module['settings']);
	$smarty->assign("MODULE_PROPERTIES",$d_items_module);
	$smarty->assign("module",$d_items_module['settings_full']);
}

$editor_settings['image_manager'] = 'OpenFileBrowser';
editor('desc','desc',0,0,0,$editor_settings);

$smarty->assign("menu",$d_items_module['name']);
$smarty->assign("submenu","settings");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/d_items/admin/settings.tpl");
$smarty->display("admin/home.tpl");

?>