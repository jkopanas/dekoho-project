<?php
include("../../../manage/init.php");//load from manage!!!!
if ($d_items_module = module_is_active("d_items",1,1,0)) 
{
	$module_path = URL."/".$d_items_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",URL."/".$d_items_module['folder']."/admin");
	$smarty->assign("allcategories",get_all_d_items_categories(DEFAULT_LANG));

$t = new textparse();
$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];



//root categories
if (!$cat) 
{  
 	$cat = 0;
}//END OF IF

$category = (empty($cat)) ? 0 : get_d_items_category($cat,DEFAULT_LANG,1);
$parentid = ($_GET['action']) ? $category['parentid'] : $cat;


##################### ADD NEW CATEGORY ######################3
if ($_POST['action'] == "addcat") 
{
	$new_category = $t->formtpa($_POST['title']);
	$new_parentid = $_POST['parent_catid'];
	$new_cat = get_d_items_category($new_parentid,DEFAULT_LANG);
	$new_meta_descr = $t->formtpa($_POST['meta_desc']);
	$new_meta_keyw = $t->formtpa($_POST['meta_keyw']);
	$new_description = $t->formtpa($_POST['desc']);
	$new_date_added = time();
	$new_orderby = $_POST['orderby'];
	//Check out for settings
$tmp = array();
foreach ($_POST as $k => $v)
{
	if (strstr($k,"settings_")) 
	{
		list($dump,$field)=split("settings_",$k);
//		echo "$field --- $v<br>";
if ($v) 
{
	$tmp[$field] = $v;
}
		
	}
	
	
}
if ($tmp) 
{
	$category_settings = form_settings_string($tmp,"###",":::");
}
	
	$sql->db_Insert("d_items_categories","'','$new_parentid','','$new_orderby','','$new_date_added','$category_settings'");
	$new_categoryid = mysql_insert_id();
	$new_categoryid_path = ($new_cat == 0) ? $new_categoryid : $new_cat['categoryid_path']."/".$new_categoryid;
	$sql->db_Update("d_items_categories","categoryid_path = '$new_categoryid_path' WHERE categoryid = '$new_categoryid'");
	
	$sql->db_Insert("d_items_categories_lng","'".DEFAULT_LANG."','$new_categoryid','$new_category','$new_description',
	'$new_meta_descr','$new_meta_keyw','$image'");
########### REPLICATE DATA FOR TRANSLATIONS ###############################
$countries = get_countries("Y",$trans="yes");
if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
for ($i=0;count($countries) > $i;$i++)
{
$sql->db_Insert("d_items_categories_lng","'".$countries[$i]['code']."','$new_categoryid','$new_category','$new_description',
'$new_meta_descr','$new_meta_keyw','$image'");
}//END OF FOR
}//END OF IF
header("Location: $module_path/categories.php?cat=$cat");
exit();

}//END OF NEW CATEGORY

##################### MODIFY EXISTING CATEGORY ######################3
if ($_POST['action'] == "modify") 
{
$parentid = $_POST['parent_catid'];
if ($parentid != 0) 
{  
$parent_details = get_d_items_category($parentid,DEFAULT_LANG);
$new_catid_path  = $parent_details['categoryid_path']."/$cat";
}//END OF IF
else 
{
$new_catid_path = $cat;	
}
//Check out for settings
$tmp = array();
foreach ($_POST as $k => $v)
{
	if (strstr($k,"settings_")) 
	{
		list($dump,$field)=split("settings_",$k);
//		echo "$field --- $v<br>";
		$tmp[$field] = $v;
	}
	
	
}
if ($tmp) 
{
	$category_settings = form_settings_string($tmp,"###",":::");
}
//now modify the details of the given category
$sql->db_Update("d_items_categories","parentid = '$parentid', settings = '$category_settings', categoryid_path = '$new_catid_path', order_by = '".$_POST['orderby']."' WHERE categoryid = $cat");

//rearange subcategory paths
$children = get_d_items_categories($cat,DEFAULT_LANG);
for ($i=0;count($children) > $i;$i++)
{
$child_id = $children[$i]['categoryid'];
$new_cat_path = $new_catid_path."/$child_id";
$sql->db_Update("d_items_categories","categoryid_path = '$new_cat_path' WHERE categoryid = '$child_id'");
}//END OF FOR

$sql->db_Update("d_items_categories_lng","description = '".$t->formtpa($_POST['desc'])."',
meta_descr = '".$t->formtpa($_POST['meta_desc'])."',meta_keywords = '".$t->formtpa($_POST['meta_keyw'])."',
category = '".$t->formtpa($_POST['title'])."', image = '$image' WHERE categoryid = $cat AND code = '".DEFAULT_LANG."'");
	 header("Location: $module_path/categories.php?action=edit&cat=".$cat);
	 exit();
}

if ($_GET['action'] == "edit") 
{
	
	$smarty->assign("category",$category);
	$smarty->assign("edit",1);
	$smarty->assign("action","modify");
	$smarty->assign("more_categories",get_d_items_categories($category['parentid'],DEFAULT_LANG,0));
}
elseif ($_GET['action'] == "delete")
{
	 $sql->db_Select("d_items_categories","categoryid","d_items_categories.categoryid_path LIKE '%$cat/%'");
	 if ($sql->db_Rows() > 0) {
	 	$tmp = execute_multi($sql,0);
	 	for ($i=0;count($tmp) > $i;$i++)
	 	{
	 	$sql->db_Delete("d_items_categories","categoryid=".$tmp[$i]['categoryid']);
	 	$sql->db_Delete("d_items_categories_lng","categoryid=".$tmp[$i]['categoryid']);
//	 	echo "DELETE FROM d_items_categories WHERE categoryid=".$tmp[$i]['categoryid'].";<br>";
	 	}
	 }
//	 echo "DELETE FROM d_items_categories WHERE categoryid=".$cat.";<br>";
	 $sql->db_Delete("d_items_categories_lng","categoryid= $cat");
	 $sql->db_Delete("d_items_categories","categoryid= $cat");
	 $sql->db_Update("d_items_categories","catid = 0 WHERE groupid = $cat");//RESET d_items
	  header("Location: $module_path/categories.php?cat=".$parentid);
	 exit();
}

			####################### LOAD AJAX  ###########################
			include($_SERVER['DOCUMENT_ROOT']."/".$d_items_module['folder']."/admin/ajax_functions.php");
			###################### END OF AJAX #######################################	

			
// create a new instance of wysiwygPro:
$editor = new wysiwygPro();
// configure your directories:

// Note: folders used by the file browser must be made writable otherwise uploading and file editing will not work.

// full directory path for your images folder:
$editor->imageDir = $_SERVER['DOCUMENT_ROOT'].'/images/';
// URL of your images folder:
$editor->imageURL = '/images';

// full directory path of your documents folder for storing PDF and Word files etc:
$editor->documentDir = $_SERVER['DOCUMENT_ROOT'].'/docs/';
// url of your documents folder:
$editor->documentURL = '/docs/';

// full directory path of your media folder for storing video files:
$editor->mediaDir = $_SERVER['DOCUMENT_ROOT'].'/docs/';
// url of your media folder:
$editor->mediaURL = '/docs';

// File editing permissions:
$editor->editImages = true;
$editor->renameFiles = true;
$editor->renameFolders = true;
$editor->deleteFiles = true;
$editor->deleteFolders = true;
$editor->copyFiles = true;
$editor->copyFolders = true;
$editor->moveFiles = true;
$editor->moveFolders = true;
$editor->upload = true;
$editor->overwrite = true;
$editor->createFolders = true;

$smarty->assign("editor_js",$editor->fetchFileBrowserJS('OpenFileBrowser'));

$editor_settings['image_manager'] = 'OpenFileBrowser';
editor('desc','desc',0,0,0,$editor_settings);
			
}//END OF MODULE

$smarty->assign("MODULE_SETTINGS",$d_items_module['settings']);
$smarty->assign("USE_AJAX","modules/d_items/admin/d_items_ajax.tpl");
$smarty->assign("menu",$d_items_module['name']);
$smarty->assign("submenu","categories");
$smarty->assign("catid",$cat);
$smarty->assign("parentid",$parentid);
$smarty->assign("cat",get_d_items_categories($cat,DEFAULT_LANG,0));//assigned template variable cat
$smarty->assign("featured",get_featured_d_items($cat,"cat",DEFAULT_LANG));//assigned template variable featured
$smarty->assign("allcategories",get_all_d_items_categories(DEFAULT_LANG));//assigned template variable allcategories
$smarty->assign("nav",d_items_cat_nav($cat,DEFAULT_LANG_ADMIN));//assigned template variable a
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("current_category",get_d_items_category($cat,DEFAULT_LANG));//assigned template variable current_cat
$smarty->assign("include_file","modules/d_items/admin/categories.tpl");
$smarty->display("admin/home.tpl");

?>