<?php
include("../../../manage/init.php");//load from manage!!!!

if ($d_items_module = module_is_active("d_items",1,1,1)) 
{
	$module_path = URL."/".$d_items_module['folder']."/admin";
	$smarty->assign("latest_d_items",get_latest_d_items($d_items_module['settings']['latest_d_items_admin'],DEFAULT_LANG));
	$smarty->assign("MODULE_FOLDER",$module_path);
	
	$t = new textparse();

	
	
########################## UPDATE d_items ##################################################################################
if ($_POST['action'] == "add") 
{
	$required_fields = array('items_title');

	//STEP 1: grab POST variables into a formated array
 $posted_data = array();
 foreach ($_POST as $key => $value) 
 {   
	 $posted_data[$key] = $value;
 }//END OF FOREACH
 //check required fields
 for ($i=0;count($posted_data) > $i;$i++)
 {
 	for ($j=0;count($required_fields) > $j;$j++)
 	{
 		if ($posted_data[$required_fields[$j]] == "") 
 		{  
 	 		$error_list[$required_fields[$j]] = $lang["error_".$required_fields[$j]];
 	 		$error = 1;
 		}//END OF IF
 	}//END OF FOR

 }//END OF FOR

 ##### IF NO ERRORS PROCEED #####
 if ($error != 1) 
 {
//Database insertions
//Add the basic d_items details
$categoryid = $_POST['categoryid'];
$extra_categories = $_POST['categoryids'];
$sku = $_POST['sku'];
$orderby = $_POST['orderby'];
$active = $_POST['active'];
$orderby = $_POST['orderby'];
$description = $t->formtpa($_POST['short_desc']);
$long_description = $t->formtpa($_POST['long_desc']);
$meta_desc = $t->formtpa($_POST['meta_desc']);
$meta_keywords = $t->formtpa($_POST['meta_keywords']);
$date_added = time();
$image = ($_POST['image']) ? $_POST['image'] : $d_items_module['settings']['default_thumb'];
$title = $t->formtpa($_POST['items_title']);
//print_r($_POST);//END OF print_r
$sql->db_Insert("d_items","'','$sku','$image','$date_added','$active','".ID."','$orderby'");
$id = mysql_insert_id();
$sql->db_Insert("d_items_lng","'".DEFAULT_LANG."','$id','$title','$description','$long_description',
'$meta_desc','$meta_keywords'");
//TRANSLATIONS

if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
$countries = get_countries(2,$trans="yes");
for ($i=0;count($countries) > $i;$i++)
{
$sql->db_Insert("d_items_lng","'".$countries[$i]['code']."','$id','$title','$description','$long_description',
'$meta_desc','$meta_keywords'");
}//END OF FOR
}//END OF IF


//SECONDARY UPDATES
//d_items CATEGORIES
	$sql->db_Insert("d_items_page_categories","'$categoryid','$id','Y'");

//See if there are any extra categories to insert
if (!empty($extra_categories)) 
{  
 for ($i=0;count($extra_categories) > $i;$i++)
 {
 	$sql->db_Insert("item_page_categories","'".$extra_categories[$i]."','$id','N'");
 }//END OF FOR
}//END OF IF

//See if there are any extra categories to insert
//first drop the old ones
$sql->db_Delete("d_items_page_categories","itemid = '$id' AND main = 'N'");
if (!empty($extra_categories)) 
{  
 for ($i=0;count($extra_categories) > $i;$i++)
 {
 	$sql->db_Insert("d_items_page_categories","'".$extra_categories[$i]."','$id','N'");
 }//END OF FOR
}//END OF IF

//add extra fields
add_d_items_extra_field_values($_POST,$id,DEFAULT_LANG);
if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
for ($i=0;count($countries) > $i;$i++)
{
add_d_items_extra_field_values($_POST,$id,$countries[$i]['code']);
}//END OF FOR
}//END OF IF

//ADD TAGS
$tags = construct_tags_array($_POST['tags'],",");
//insert new tags
for ($i=0;count($tags) > $i;$i++)
{
	$tag = $tags[$i];
	$sql->db_Insert("d_items_tags","'','$id','$tag'");
}
header("Location: $module_path/items_modify.php?id=$id");
exit();
 }//END OF NO ERRORS
 $smarty->assign("error_list",$error_list);//assigned template variable error_list<BR>
}

	
################################################## LOAD d_items FEATURES ############################################################
	$d_items = get_d_items($id,DEFAULT_LANG);
	

	$smarty->assign("allcategories",get_all_d_items_categories(DEFAULT_LANG));//assigned template variable allcategories
	$smarty->assign("extra_fields",get_extra_fields_d_items("Y",DEFAULT_LANG));


	
	if ($_GET['cat']) 
	{
		$tmp_cat['catid']=$_GET['cat'];
		$smarty->assign("d_items",$tmp_cat);
		
	}
	editor("description","description1",0,0,0,$editor_settings);
	editor("long","long1",0,0,0,$editor_settings);
	$smarty->assign("d_items_module",$d_items_module);
	$smarty->assign("action","add");
################################################ END OF LOAD d_items #########################################
}//END OF LOAD MODULE



$smarty->assign("USE_AJAX",$d_items_module['folder']."/admin/ajax_functions.tpl");
$smarty->assign("menu",$d_items_module['name']);
$smarty->assign("submenu","add");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/d_items/admin/items_modify.tpl");
$smarty->display("admin/home.tpl");

?>