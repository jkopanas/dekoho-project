<?php
class tags {
var $settings;
var $module;
var $orderby;
var $way;
var $limit;
var $itemid;
var $active;
var $active_q;
var $tables;
var $tables_join;

	function tags($settings)//INITIALIZE
	{
		$this->settings = $settings;	
		$this->itemid = $settings['itemid'];	
		$this->module = $settings['module'];	
		$this->orderby = ($this->settings['orderby']) ? $this->settings['orderby'] : 'tag';
		$this->way = ($this->settings['way']) ? $this->settings['way'] : 'ASC';
		$this->limit = ($this->settings['limit']) ? "LIMIT ".$this->settings['limit'] : "";	
		//SETUP TABLES
		$this->tables = array();
		if ($this->module['name'] == 'content') {
			$this->tables['item'] = 'content';
			$this->tables['category'] = 'content_categories';
			$this->tables['pages'] = 'content_page_categories';
		}//END IF
		elseif ($this->module['name'] == 'products')
		{
			$this->tables['item'] = 'products';
			$this->tables['category'] = 'products_categories';
			$this->tables['pages'] = 'products_page_categories';
		}//END IF
		elseif ($this->module['name'] == 'courses')
		{
			$this->tables['item'] = 'courses';
			$this->tables['category'] = 'courses_categories';
			$this->tables['pages'] = 'courses_page_categories';
		}//END IF
		if ($this->settings['active'] == 1 OR $this->settings['active'] == 0) {
			$this->active_q = "AND ".$this->tables['item'].".active = ".$this->settings['active'];
			$this->tables_join = " INNER JOIN ".$this->tables['item']." ON (".$this->tables['item'].".id=tags_items.itemid) ";
		}
	}
	
	function GetTagItemsByTagId($tagID)
	{
		global $sql;
		
		$sql->db_Select('tags INNER JOIN tags_items ON (tags.id=tags_items.id)'.$this->tables_join,'tag,tags.id,permalink,itemid',"tags.id = $tagID AND module = '".$this->module['name']."'
		".$this->active_q." ORDER BY ".$this->orderby." ".$this->way." ".$this->limit);
		if ($this->settings['debug']) {
			echo "SELECT tag,tags.id,permalink,itemid FROM tags INNER JOIN tags_items ON (tags.id=tags_items.id)".$this->tables_join." WHERE tags.id = $tagID AND module = '".$this->module['name']."' ORDER BY ".$this->orderby." ".$this->way." ".$this->limit."<Br>";
		}
		if ($sql->db_Rows()) {
			return execute_multi($sql);
		}
	}//END FUNCTION
	
	function GetTagItemsByPermalink($permalink,$settings=0)
	{
		global $sql;

		$sql->db_Select('tags INNER JOIN tags_items ON (tags.id=tags_items.id)'.$this->tables_join,'tag,tags.id,tags.permalink,itemid',"tags.permalink = '$permalink' AND module = '".$this->module['name']."'
		 ".$this->active_q." ORDER BY ".$this->orderby." ".$this->way." ".$this->limit);
		if ($this->settings['debug']) {
			echo "SELECT tag,tags.id,permalink,itemid FROM tags INNER JOIN tags_items ON (tags.id=tags_items.id)".$this->tables_join." WHERE tags.permalink = '$permalink' AND module = '".$this->module['name']."'
			 ".$this->active_q." ORDER BY ".$this->orderby." ".$this->way." ".$this->limit."<Br>";
		}
		if ($sql->db_Rows()) {
			if ($this->settings['return_items']) {
				$ids = execute_multi($sql);
				$C = new Items(array('module'=>$this->module));
				foreach ($ids as $v) {
					$items[] = $C->GetItem($v['itemid'],$this->settings);
				}
				return $items;
			}
			else {
				return execute_multi($sql);
			}
		}
	}//END FUNCTION
	
	function GetTagItemsByTag($tag)
	{
		global $sql;

		$sql->db_Select('tags INNER JOIN tags_items ON (tags.id=tags_items.id)','tag,tags.id,permalink,itemid',"tags.tag = '$tag' AND module = '".$this->module['name']."' ORDER BY ".$this->orderby." ".$this->way." ".$this->limit);
		if ($this->settings['debug']) {
			echo "SELECT tag,tags.id,permalink,itemid FROM tags INNER JOIN tags_items ON (tags.id=tags_items.id) WHERE tags.tag = '$tag' AND module = '".$this->module['name']."' ORDER BY ".$this->orderby." ".$this->way." ".$this->limit."<Br>";
		}
		if ($sql->db_Rows()) {
			return execute_multi($sql);
		}
	}//END FUNCTION
	
	function GetTagItemsByItemid($itemid)
	{
		global $sql;

		$sql->db_Select('tags INNER JOIN tags_items ON (tags.id=tags_items.id)','tag,tags.id,permalink,itemid',"itemid = '$itemid' AND module = '".$this->module['name']."' ORDER BY ".$this->orderby." ".$this->way." ".$this->limit);
		if ($this->settings['debug']) {
			echo "SELECT tag,tags.id,permalink,itemid FROM tags INNER JOIN tags_items ON (tags.id=tags_items.id) WHERE itemid = '$itemid' AND module = '".$this->module['name']."' ORDER BY ".$this->orderby." ".$this->way." ".$this->limit."<Br>";
		}
		if ($sql->db_Rows()) {
			$res = execute_multi($sql);
			return $res;
		}
	}//END FUNCTION
	
	function GetTag($input,$type,$settings=0)
	{
		global $sql;
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		$tables = "tags";
		switch ($type)
		{
			case 'tag': $search_field = 'tag';
			break;
			case 'id': $search_field = 'id';
			break;
			case 'permalink' : $search_field = 'permalink';
			break;
		}//END SWITCH
		$q = "$search_field = '$input' AND module = '".$this->module['name']."'";
		if ($settings['debug']) {
			echo "SELECT $fields FROM $tables WHERE $q";
		}
		$sql->db_Select($tables,$fields,$q);
		if ($sql->db_Rows()) {
			$a = execute_single($sql);
			return $a;
		}
	}//END FUNCTION
	
	
	function GetTags($ids=0,$settings=0)
	{
		global $sql;
		if ($ids) {
			foreach ($ids as $v) {
				$tmp[] = $v['id'];
			}
			$q = " AND tags_items.itemid IN (".implode(',',$tmp).")";
		}
		if ($settings['limit']) {
			$limit = " LIMIT ".$settings['limit'];
		}
		if ($settings['TagCloud']) {
			$counter = ",count(tags_items.id) as item_count";
		}
		$sql->db_Select('tags INNER JOIN tags_items ON (tags.id=tags_items.id)',"tag,tags.id,permalink,itemid $counter ","module = '".$this->module['name']."' $q GROUP BY tag ORDER BY ".$settings['orderby']." ".$settings['way']." $limit");
		if ($settings['debug']) {
			echo "SELECT tag,tags.id,permalink,itemid $count FROM tags INNER JOIN tags_items ON (tags.id=tags_items.id) WHERE  module = '".$this->module['name']."' $q GROUP BY tag ORDER BY ".$settings['orderby']." ".$settings['way']." ".$settings['limit']."<Br>";
		}
		if ($sql->db_Rows()) {
			if ($settings['TagCloud']) {
				$res = execute_multi($sql);
				return $this->CreateTagCloud($res);
			}
			else {
				return execute_multi($sql);
			}
			
		}
	}//END FUNCTION

	function GetMultipleTags($tags)
	{
		global $sql;
		if ($this->settings['SearchField'] == 'id') {
			$searchFor = implode(",",$tags);
		}
		elseif ($this->settings['SearchField'] == 'tag')
		{
			foreach ($tags as $v)
			{
				$tmp[] = "'$v'";//ADD QUOTED FOR TEXT SEARCH
			}
			$searchFor = implode(",",$tmp);
		}
		
		if ($this->settings['count_items']) {
			$count = ",(SELECT count(itemid) FROM tags_items WHERE id = tags.id AND module = '".$this->module['name']."') as item_count";
		}
        if (!$this->settings['simple']) {
        $join = "INNER JOIN tags_items ON (tags.id=tags_items.id)";
        $fields = ',itemid';
        }//END IF
		$sql->db_Select("tags $join","tag,tags.id,permalink $fields $count ","tags.".$this->settings['SearchField']." IN ($searchFor) AND module = '".$this->module['name']."' GROUP BY tag ORDER BY ".$this->orderby." ".$this->way." ".$this->limit);
		if ($this->settings['debug']) {
			echo "SELECT tag,tags.id,permalink $fields $count FROM tags $join WHERE  ".$this->settings['SearchField']." IN ($searchFor) AND module = '".$this->module['name']."' GROUP BY tag ORDER BY ".$this->orderby." ".$this->way." ".$this->limit."<Br>";
		}
		if ($sql->db_Rows()) {
			if ($this->settings['TagCloud']) {
				$res = execute_multi($sql);
				return $this->CreateTagCloud($res);
			}
			else {
				return execute_multi($sql);
			}
			
		}
	}//END FUNCTION

	function CreateTagCloud($res)
	{
		if ($res) 
		{
	
			
			for ($i=0;count($res) > $i;$i++)
			{
				$tags[$res[$i]['tag']] = $res[$i]['item_count'];
			}
		//	echo "SELECT tag , COUNT(id) AS quantity FROM recipes_tags "
			$max_size = ($this->settings['max_size']) ? $this->settings['max_size'] : 250; // max font size in %
			$min_size = ($this->settings['min_size']) ? $this->settings['min_size'] : 100; // min font size in %
			
			$max_qty = max(array_values($tags));
			$min_qty = min(array_values($tags));
				// find the range of values
			$spread = $max_qty - $min_qty;
			if (0 == $spread) 
			{ // we don't want to divide by zero
		    	$spread = 1;
			}
		
			// determine the font-size increment
			// this is the increase per tag quantity (times used)
			$step = ($max_size - $min_size)/($spread);
			
			for ($i=0;count($res) > $i;$i++)
			{
				$size = $min_size + (($res[$i]['item_count'] - $min_qty) * $step);
				$res[$i]['size'] = $size;
			}
			}//END OF ROWS
		return $res;
	}//END FUNCTION
	
	function SaveTags($tags)
	{
		global $sql;
		//UPDATE TAGS
		if ($tags) 
		{
			$tags_array = $this->ConstructTagsArray($tags);
			//FIRST CHECK WHICH ONES EXIST
			$this->settings['SearchField'] = 'tag';
            $this->settings['simple'] = 1;//GET ALL TAGS WITHOUT JOINING THE TABLE ITEMS
            $this->settings['debug'] =0;
			$existing_tags = $this->GetMultipleTags($tags_array);

			if (is_array($tags_array)) {
				$tags_array = array_flip($tags_array);
				$tmp = $tags_array;
				if ($existing_tags) {
				foreach ($existing_tags as $v)
				{
					if (array_key_exists($v['tag'],$tags_array)) {
						$tmp[$v['tag']] = array();
						$tmp[$v['tag']]['order'] = $tags_array[$v['tag']];
						$tmp[$v['tag']]['id'] = $v['id'];
					}//END IF
				}//END FOREACH
				}//END EXISTING TAGS
				//NOW CHECK OUT FOR NEW ITEMS. REMAINING ITEMS FROM TAGS ARRAY

				if ($tmp) {
				    $i = 0;
                    $sql->db_Delete('tags_items',"id IN ((SELECT tags.id FROM tags WHERE module = '".$this->module['name']."' AND tags.id = tags_items.id)) AND
                    itemid = ".$this->itemid);
					foreach ($tmp as $k=>$v)
					{
						if (!is_array($v)) {
						  $v = array();
							$permalink = greeklish($k);
							$sql->db_Insert("tags (tag,module,permalink)","'$k','".$this->module['name']."','$permalink'");//ADD TAG
							echo "INSERT INTO tags (tag,module,permalink) VALUES ('$k','".$this->module['name'],"','$permalink')";
							$n_id = $sql->last_insert_id;
                            $v['id'] = $n_id;
						}

						  //DROP OLD ONES
                         // echo "INSERT INTO tags_items VALUES ('".$v['id']. "','".$this->itemid."','$i')";
                          $sql->db_Insert('tags_items',"'".$v['id']. "','".$this->itemid."','$i'");

					$i++;
					}
				}//END IF
			}
		}//END IF
		
	}//END FUNCTION
	
	function getAllTags($settings=0) {
		global $sql;
		
		$fields = ($settings['fields']) ? $settings['fields'] : 'tag,tags.id,permalink,itemid';
		if ($settings['limit']) {
			$limit = " LIMIT ".$settings['limit'];
		$orderBy = ($settings['orderby']) ? $settings['orderby'] : 'tag';
		$way = ($settings['way']) ? $settings['way'] : 'ASC';
		$sql->db_Select('tags',$fields,"module = '".$this->module['name']."' $q GROUP BY tag ORDER BY $orderBy $way $limit");
		if ($settings['debug']) {
			echo "SELECT $fields FROM tags  module = '".$this->module['name']."' $q GROUP BY tag ORDER BY ".$settings['orderby']." ".$settings['way']." ".$settings['limit']."<Br>";
		}
		if ($sql->db_Rows()) {
			return execute_multi($sql);
			
		}
		}		
	}//END FUNCTION
	
	
	function ConstructTagsString($tags,$seperator=",")
	{
		if (is_array($tags)) {
		foreach ($tags as $v)
		{
			$arr[] = $v['tag'];
		}
		
		return implode($seperator,$arr);
		}

	}//END FUNCTION
	
	function ConstructTagsArray($tags_string,$seperator=",")
	{
		$tmp = explode($seperator,$tags_string);
       
        
		$tmp = array_unique($tmp);//REMOVE DUPLICATES
		if (is_array($tmp)) 
		{
		for ($i=0;count($tmp) > $i;$i++)
		{
			if ($tmp[$i] AND $tmp[$i] != ' ' AND strlen($tmp[$i])>1 ) {
				$tmp[$i] = trim($tmp[$i]);
			}
            else { unset($tmp[$i]);}//DESTROY EMPTY TAG
		}
        ksort($tmp);

		return $tmp;	
		}
		else {
			return false;
		}
	
	}//END FUNCTION
	
}//END CLASS


?>