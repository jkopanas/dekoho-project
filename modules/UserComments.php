<?php

class UserComments
{
    public $tables;
    public $users;
    
    function UserComments()
    {
        $this->tables['users_comments'] = array('table'=>'users_comments','key'=>'id');
        $this->users = new user();
    }
    
    function getComments($settings)
    {
    	//print_r($settings);
        global $sql;
        //$users->tables["users"]["table"]
        $fields = (!$settings['fields']) ? "*" : $settings['fields'];
        $orderby = ($settings['orderby']) ? " ORDER BY ".$settings['orderby'] : "";
    	$way = ($settings['way']) ? "  ".$settings['way'] : "";
    	$limit = ($settings['results_per_page']) ? "LIMIT ".$settings['results_per_page'] : "";
    	$settings['return'] = ($settings['return']) ? $settings['return'] : "multi";
    	if (is_array($settings['searchFields'])) {
    		foreach ($settings['searchFields'] as $k => $v) {
    			$q[] = "$k = '$v'";
    		}
    	}//END SETUP SEARCH FIELDS
    	if(!$settings['searchFields']['archived']==1)
    	{
    		$settings['searchFields']['archived']=0;
    	}
    	if (is_array($settings['searchFieldsLike'])) {
    		foreach ($settings['searchFieldsLike'] as $k => $v) {
    			$q[] = "$k LIKE '%$v%'";
    		}
    	}//END SETUP SEARCH FIELDS WITH LIKE
    	
    	if (is_array($settings['greaterThan'])) {
    		foreach ($settings['greaterThan'] as $k => $v) {
    			$q[] = $k." > ".$v;
    		}
    	}//END SETUP SEARCH FIELDS WITH LIKE
    	
    	if (is_array($settings['lessThan'])) {
    		foreach ($settings['lessThan'] as $k => $v) {
    			$q[] = $k." < ".$v;
    		}
    	}
    	
        if (is_array($settings['missingFields'])) {
    		foreach ($settings['missingFields'] as $k => $v) {
    			$q[] = "$k != '$v'";
    		}
    	}//END SETUP SEARCH FIELDS
    	 
        if($q)
        {
            $query=implode(" AND ",$q);
        }

        
        if (!$query) 
		{
			$query_mode='no_where';
		}
		else {
			$query_mode = 'default';
		}

		if ($settings['return'] == 'multi') //Return all results, NO PAGINATION
		{
			$sql->db_Select($this->tables['users_comments']['table'],$fields,"$query $orderby $way $limit",$query_mode);
			if ($settings['debug']) {
				echo "SELECT ".$fields." FROM ".$this->tables['users_comments']['table']." WHERE $query $orderby $way $limit<br>";
			}
			if ($sql->db_Rows()) 
			{
				$res = execute_multi($sql);
				
			}
		}//END ALL RESULTS
		elseif ($settings['return'] == 'paginated') // RETURN PAGINATED RESULTS
		{
			$sql->db_Select($this->tables['users_comments']['table'],'id',"$query $orderby $way",$query_mode);//GET THE TOTAL COUNT
			
				if ($settings['debug']) 
				{
					echo "SELECT id FROM ".$this->tables['users_comments']['table']." WHERE $query $orderby $way<br>";
				}
			if ($sql->db_Rows()) //FOUND RESULTS NOW GET THE ACTUAL DATA
			{
				$total = $sql->db_Rows();
				$current_page = ($settings['page']) ? $settings['page'] : 1;
				$results_per_page =  $settings['results_per_page'];
				if (isset($settings['start']))
				{
					$start = $settings['start'];
				}
				else {
					$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
				}
				$limit = "LIMIT $start,".$results_per_page;
				$sql->db_Select($this->tables['users_comments']['table'],$fields,"$query $orderby $way $limit",$query_mode);
				if ($settings['debug']) {
					echo "<br>SELECT $fields FROM ".$this->tables['users_comments']['table']." WHERE $query $orderby $way $limit";
				}
				$res = execute_multi($sql,1);
				paginate_results($current_page,$results_per_page,$total);
				
			}//END FOUND RESULTS
		}//END PAGINATION        
    	elseif ($settings['return'] == 'single') // RETURN PAGINATED RESULTS
    	{
			$sql->db_Select($this->tables['users_comments']['table'],$fields,"$query $orderby $way $limit",$query_mode);
			if ($settings['debug']) {
				echo "SELECT ".$fields." FROM ".$this->tables['users_comments']['table']." WHERE $query $orderby $way $limit<br>";
			}
			if ($sql->db_Rows()) 
			{
				$res = execute_single($sql);	
			}
			
			
			if ($settings['getUserDetails']) {
						$res['userDetails'] = $this->users->userDetails($res['uid'],array('fields'=>'id,uname'));
			}//END USER DETAILS
    				
    	}
    	elseif ($settings['return'] == 'count') // RETURN COUNT RESULTS
		{
			$sql->db_Select($this->tables['users_comments']['table'],"count(".$this->tables['users_comments']['table'].".id) as total","$query $orderby $way $limit",$query_mode);
			if ($settings['debug']) {
				echo "SELECT count(".$this->tables['users_comments']['table'].".id) as total FROM ".$this->tables['users_comments']['table']." WHERE $query $orderby $way $limit<br>";
			}
			$res = execute_single($sql);
			return $res['total'];
		}
        	
    	if ($settings['return']!='single') // RETURN PAGINATED RESULTS
    	{
			if ($settings['getUserDetails']) {
				for ($i=0;count($res) > $i;$i++)
				{
					$res[$i]['userDetails'] = $this->users->userDetails($res[$i]['uid'],array('fields'=>'id,uname'));
				}
			}//END USER DETAILS
    	}
			
        return $res;
    }
    
    function addComment($settings)
    {
        global $sql;
        
        foreach($settings as $k=>$v)
        {
            $keys[]=$k;
            $values[]=$v;
        }
        
        $keys[]='date_added';
        $values[]=time();
        
        $keys = " (".implode(",",$keys).")";
        $values = "'".implode("','",$values)."'";
        
        $sql->db_Insert($this->tables['users_comments']['table'].$keys,$values);
        
        return $sql->last_insert_id;
    }
    
    function removeComment($settings)
    {
        global $sql;
        
        foreach($settings as $k=>$v)
        {
            $args[]= $k."='$v'";
        }
        
         if(count($args)>1)
            $args = implode(" AND ",$args);
        elseif(count($args)==1)
            $args = $args[0];
        else
            $mode="no_where";
        
        $sql->db_Delete($this->tables['users_comments']['table'],$args);
        
    }
    
    function updateComment($settings)
    {
        global $sql;
        
        foreach($settings as $k=>$v)
        {
            $args[]= $k."='$v'";
        }
        
         if(count($args)>1)
            $args = implode(" , ",$args);
        elseif(count($args)==1)
            $args = $args[0];
                
        $where = " WHERE id='".$settings['id']."'";
        $sql->db_Update($this->tables['users_comments']['table'],$args.$where);
    }
}

?>