<?php
include("../../../manage/init.php");//load from manage!!!!

if ($locations_module = module_is_active("locations",1,1)) 
{
	$smarty->assign("latest_locations",get_latest_locations($locations_module['settings']['latest_locations_admin'],DEFAULT_LANG));
	$smarty->assign("MODULE_FOLDER",URL."/".$locations_module['folder']."/admin");
	$smarty->assign("MODULE_SETTINGS",$locations_module['settings']);

}


$smarty->assign("menu",$locations_module['name']);
$smarty->assign("submenu","main");
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("include_file","modules/locations/admin/main.tpl");
$smarty->display("admin/home.tpl");

?>