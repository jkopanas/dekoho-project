<?php
include("../../../manage/init.php");//load from manage!!!!
if ($locations_module = module_is_active("locations",1,1,0)) 
{
	$module_path = URL."/".$locations_module['folder']."/admin";
	$smarty->assign("MODULE_FOLDER",URL."/".$locations_module['folder']."/admin");

$cat = ($_GET['cat']) ? $_GET['cat'] : $_POST['cat'];
$category = (empty($cat)) ? 0 : get_locations_category($cat,DEFAULT_LANG);
$parentid = ($_GET['action']) ? $category['parentid'] : $cat;

$active = "0,1";
$categoryid = $category['categoryid'];

 $sql->db_Select("locations_page_categories
 INNER JOIN locations ON (locations_page_categories.locationsid=locations.id)
 INNER JOIN locations_categories ON (locations_page_categories.catid=locations_categories.categoryid)",
 "locations_page_categories.locationsid","(locations_categories.categoryid_path LIKE '%$categoryid/%' 
 OR `locations_categories`.categoryid_path LIKE '%/$categoryid' OR locations_categories.categoryid_path = '$categoryid') 
 AND locations_page_categories.main = 'Y' AND locations.active IN ($active) ");

 
$tmp = execute_multi($sql);
 $lang = DEFAULT_LANG;

 for ($i=0;count($tmp) > $i;$i++)
 {
 	$locations[] = get_locations($tmp[$i]['locationsid'],$lang);
 }
$smarty->assign("locations",$locations);

}

$smarty->assign("allcategories",get_all_locations_categories(DEFAULT_LANG));//assigned template variable allcategories
$smarty->assign("MODULE_SETTINGS",$locations_module['settings']);
$smarty->assign("menu",$locations_module['name']);
$smarty->assign("submenu","categories");
$smarty->assign("catid",$cat);
$smarty->assign("parentid",$parentid);
$smarty->assign("cat",get_locations_categories($cat,DEFAULT_LANG,0));//assigned template variable cat
$smarty->assign("nav",locations_cat_nav($cat,DEFAULT_LANG));//assigned template variable a
$smarty->assign("page_title",SITE_NAME." Administration");
$smarty->assign("current_category",get_locations_category($cat,DEFAULT_LANG));//assigned template variable current_cat
$smarty->assign("include_file","modules/locations/admin/category_locations.tpl");
$smarty->display("admin/home.tpl");

?>