<?php
include("../manage/init.php");//load from manage!!!!
include($_SERVER['DOCUMENT_ROOT']."/modules/common_functions.php");//load from manage!!!!
$module_name = $_GET['module'];
$loaded_module = module_is_active($module_name,1,1);
############## PRODCEED ONLY IF LOGED IN


if (ID) 
{
	ini_set('memory_limit','99M');
//	error_message($_GET,$_SERVER['DOCUMENT_ROOT']."/modules/error.txt");
//----------------------------------------------
//    partitioned upload file handler script
//----------------------------------------------

//
//    specify upload directory - storage 
//    for reconstructed uploaded files
		$upload_dir = $_SERVER['DOCUMENT_ROOT'].$loaded_module['settings']['images_path']."/tmp/"; 

		if (!is_dir($upload_dir)) 
		{
			mkdir($upload_dir,0777);
		}
		
//
//    specify stage directory - temporary storage 
//    for uploaded partitions
$stage_dir = $upload_dir;

//
//    retrieve request parameters
$file_param_name = 'file';
$file_name = $_FILES[ $file_param_name ][ 'name' ];
$file_id = $_REQUEST[ 'fileId' ];
$partition_index = $_REQUEST[ 'partitionIndex' ];
$partition_count = $_REQUEST[ 'partitionCount' ];
$file_length = $_REQUEST[ 'fileLength' ];

//
//    the $client_id is an essential variable, 
//    this is used to generate uploaded partitions file prefix, 
//    because we can not rely on 'fileId' uniqueness in a 
//    concurrent environment - 2 different clients (applets) 
//    may submit duplicate fileId. thus, this is responsibility 
//    of a server to distribute unique clientId values
//    (or other variable, for example this could be session id) 
//    for instantiated applets.
$client_id = $_GET[ 'clientId' ];

//
//    move uploaded partition to the staging folder 
//    using following name pattern:
//    ${clientId}.${fileId}.${partitionIndex}
$source_file_path = $_FILES[ $file_param_name ][ 'tmp_name' ];
$target_file_path = $stage_dir . $client_id . "." . $file_id . 
    "." . $partition_index;
   
if( !move_uploaded_file( $source_file_path, $target_file_path ) ) {
    echo "Error:Can't move uploaded file";
    return;
}

//
//    check if we have collected all partitions properly
$all_in_place = true;
$partitions_length = 0;
for( $i = 0; $all_in_place && $i < $partition_count; $i++ ) {
    $partition_file = $stage_dir . $client_id . "." . $file_id . "." . $i;
    if( file_exists( $partition_file ) ) {
        $partitions_length += filesize( $partition_file );
    } else {
        $all_in_place = false;
    }
}

//
//    issue error if last partition uploaded, but partitions validation failed
if( $partition_index == $partition_count - 1 &&
        ( !$all_in_place || $partitions_length != intval( $file_length ) ) ) {
    echo "Error:Upload validation error";
    return;
}
//error_message($upload_dir . $client_id . "." . $file_id,$_SERVER['DOCUMENT_ROOT']."/images/gallery/products/tmp/error.txt");
//
//    reconstruct original file if all ok
if( $all_in_place ) {
	
    $file = $upload_dir . $client_id . "." . $file_id;
    

    $file_handle = fopen( $file, 'a' );
    for( $i = 0; $all_in_place && $i < $partition_count; $i++ ) {
        //
        //    read partition file
        $partition_file = $stage_dir . $client_id . "." . $file_id . "." . $i;
        $partition_file_handle = fopen( $partition_file, "rb" );
        $contents = fread( $partition_file_handle, filesize( $partition_file ) );
        fclose( $partition_file_handle );
        //
        //    write to reconstruct file
        fwrite( $file_handle, $contents );
        //
        //    remove partition file
        unlink( $partition_file );

    }

    fclose( $file_handle );
 
      if (file_exists($upload_dir.".$file_id")) 
 {
 	$oldfile = $upload_dir.".$file_id";
 	$newfile = $upload_dir.$file_name;
  	rename($oldfile,$newfile);
//  	error_message($upload_dir.".$file_id\n".$upload_dir.$file_name,$_SERVER['DOCUMENT_ROOT']."/images/gallery/products/tmp/error.txt");
 }   
   
 if (file_exists($upload_dir.$file_name)) 
 {
//  	rename($upload_dir.".$file_id",$upload_dir.$file_name);
  	
  	################################## SUCCESS START DEDICATED CODE HERE ########################################
  	############## DETAILED IMAGES ##################
	if ($_GET['action'] == "mass_product_images")
	{
		$itemid = $_GET['id'];//Item id
		############### CALL FUNCTION THAT SAVES THIS ITEM TO DB ###################
		$file_type = ($_GET['type'] == 0) ? 'thumb' : 'image';
		$settings = array(
		'lang' => DEFAULT_LANG,
		'module' => $loaded_module,
		'filename' => $file_name,
		'upload_dir' => $upload_dir,
		'file_type' => $file_type,
		'ImageSet' => 1,
		'itemid' => $itemid,
		'uid' => ID,
		'date_added' => time(),
		'imageFix' => $loaded_module['settings']['imageFix'],
		'type' => $_GET['type']
		);
		
		handleUploadedFiles($settings);
		unlink($upload_dir.$file_name);

	}//END DETAILED IMAGES
	elseif ($_GET['action'] == "insert_videos")
	{
		
		$itemid = $_GET['id'];//Item id
		############### CALL FUNCTION THAT SAVES THIS ITEM TO DB ###################
		$settings = array(
		'lang' => DEFAULT_LANG,
		'module' => $loaded_module,
		'filename' => $file_name,
		'upload_dir' => $upload_dir,
		'file_type' => 'video',
		'itemid' => $itemid,
		'uid' => ID,
		'date_added' => time(),
		'type' => $_GET['type']
		);
		
		$a = handleUploadedFiles($settings);
		unlink($upload_dir.$file_name);
		rename($a['old_file'],$a['new_file']);
	}
	elseif ($_GET['action'] == "insert_aditional_videos")
	{
		
		$itemid = $_GET['itemid'];//Item id
		############### CALL FUNCTION THAT SAVES THIS ITEM TO DB ###################
		$settings = array(
		'lang' => DEFAULT_LANG,
		'module' => $loaded_module,
		'filename' => $file_name,
		'upload_dir' => $upload_dir,
		'file_type' => 'video_aditional',
		'itemid' => $itemid,
		'format_id' => $_GET['format_id'],
		'video_id' => $_GET['video_id'],
		'uid' => ID,
		'date_added' => time(),
		'type' => $_GET['type']
		);
		
		$a = handleUploadedFiles($settings);
		
		
		unlink($upload_dir.$file_name);
		rename($a['old_file'],$a['new_file']);
	}
	elseif ($_GET['action'] == "mass_items_images")
	{
		$itemid = $_GET['id'];//Item id
		############### CALL FUNCTION THAT SAVES THIS ITEM TO DB ###################
		$file_type = ($_GET['type'] == 0) ? 'thumb' : 'image';
		$settings = array(
		'lang' => DEFAULT_LANG,
		'module' => $loaded_module,
		'filename' => $file_name,
		'upload_dir' => $upload_dir,
		'file_type' => $file_type,
		'ImageSet' => 1,
		'itemid' => $itemid,
		'uid' => ID,
		'date_added' => time(),
		'imageFix' => $loaded_module['settings']['imageFix'],
		'type' => $_GET['type']
		);
		
		handleUploadedFiles($settings);
		unlink($upload_dir.$file_name);	
	}
	############## MASS INSERT ##################
	elseif ($_GET['action'] == "mass_insert")
	{
		
######### First try to add the item to the DB ################
if ($loaded_module['settings']['use_sku']) 
{
	$sku = "SKU - $file_name";
}
$date_added = time();
$uid = ID;
$image = $loaded_module['settings']['default_thumb'];


if ($sql->db_Insert($loaded_module['name'],"'','$sku','$image','$date_added','2','$uid',0"))
{
	//NEW ITEM CREATED
$itemid = $sql->last_insert_id;
$title = "PRODUCT - $file_name";
$description = "DESCRIPTION - $file_name";
$long_description = "LONG DESCRIPTION - $file_name";
$sql->db_Insert("lng","'".DEFAULT_LANG."','$itemid','$title','$description','$long_description',
'$meta_desc','$meta_keywords'");
//TRANSLATIONS
if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
$countries = get_countries(2,$trans="yes");
for ($i=0;count($countries) > $i;$i++)
{
$sql->db_Insert($loaded_module['name']."_lng","'".$countries[$i]['code']."','$itemid','$title','$description','$long_description',
'$meta_desc','$meta_keywords'");
}//END OF FOR
}//END OF IF

//PRODUCT CATEGORIES
$sql->db_Insert("page_categories","'".$_GET['cat']."','$itemid','Y'");

############ NOW HANDLE THE UPLOADED FILE ############
		$settings = array(
		'lang' => DEFAULT_LANG,
		'module' => $loaded_module,
		'filename' => $file_name,
		'upload_dir' => $upload_dir,
		'file_type' => 'thumb',//EACH ENTRY IS A THUMB
		'ImageSet' => 1,
		'itemid' => $itemid,
		'uid' => ID,
		'date_added' => time(),
		'imageFix' => $loaded_module['settings']['imageFix']
		);
handleUploadedFiles($settings);
unlink($upload_dir.$file_name);

}//END INSERT
		
		

	}//END MASS INSERT PRODUCTS
	elseif ($_GET['action'] == "mass_insert_items")
	{
######### First try to add the item to the DB ################

$date_added = time();
$uid = ID;
$image = $loaded_module['settings']['default_thumb'];


if ($sql->db_Insert("d_items","'','$sku','$image','$date_added','2','$uid',0"))
{
	//NEW ITEM CREATED
$itemid = $sql->last_insert_id;
$title = "item - $file_name";
$description = "DESCRIPTION - $file_name";
$long_description = "LONG DESCRIPTION - $file_name";
$sql->db_Insert("d_items_lng","'".DEFAULT_LANG."','$itemid','$title','$description','$long_description',
'$meta_desc','$meta_keywords'");
//TRANSLATIONS
if (REPLICATE_TRANSLATION)//Replicate the data for all available languages 
{  
$countries = get_countries(2,$trans="yes");
for ($i=0;count($countries) > $i;$i++)
{
$sql->db_Insert($loaded_module['name']."_lng","'".$countries[$i]['code']."','$itemid','$title','$description','$long_description',
'$meta_desc','$meta_keywords'");
}//END OF FOR
}//END OF IF

//PRODUCT CATEGORIES
$sql->db_Insert("d_items_page_categories","'".$_GET['cat']."','$itemid'");

############ NOW HANDLE THE UPLOADED FILE ############
		$settings = array(
		'lang' => DEFAULT_LANG,
		'module' => $loaded_module,
		'filename' => $file_name,
		'upload_dir' => $upload_dir,
		'file_type' => 'thumb',//EACH ENTRY IS A THUMB
		'ImageSet' => 1,
		'itemid' => $itemid,
		'uid' => ID,
		'date_added' => time(),
		'imageFix' => $loaded_module['settings']['imageFix']
		);
handleUploadedFiles($settings);
unlink($upload_dir.$file_name);

}//END INSERT
	}//END MASS INSERT ITEMS
  	################################## SUCCESS END DEDICATED CODE HERE ########################################
 }//END RENAME FILE AFTER SUCCESS  
}//END ALL IN PLACE 
}//END LOGED IN
?>