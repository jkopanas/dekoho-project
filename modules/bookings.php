<?php
class bookings extends Items 
{
	
	public function __construct($settings=0) {
			parent::Items($settings);	
	}
	
	public function getSeasons($itemID,$settings=0) {
		global $sql;

		$tables = "bookings_seasons";
		$fields = ($settings['fields']) ? $settings['fields'] : '*';
		$orderby = ($settings['orderby']) ? $settings['orderby'] : ' orderby ';
		$way = ($settings['way']) ? $settings['way'] : ' ASC ';
		
		if (is_array($itemID)) {
			$q[] = "itemid IN (".implode(",",$itemID).")";
		}
		else {
			$q[] = "itemid = $itemID";
		}
		
		if ($q) {
			$query = implode(" AND ",$q);
			$mode = "default";
		}
		else { $mode = "no_where"; }
		
		$sql->db_Select($tables,$fields,"$query ORDER BY $orderby $way",$mode);
		if ($settings['debug']) {
			echo "SELECT $fields FROM $tables WHERE $query ORDER BY $orderby $way";
		}
		if ($sql->db_Rows()) {
			$r = execute_multi($sql);
			foreach ($r as $k=>$v){
				$allIds[] = $v['id'];
				$r[$k]['settings'] = json_decode($v['settings']);
			}
			
			if ($settings['getPeriods']) {
				$periods = $this->getPeriods($allIds,$settings['periodSettings']);
				$allIds = array_flip($allIds);
				if ($periods) {
					foreach ($periods as $v) { 
						if (array_key_exists($v['seasonID'],$allIds)) {
							$r[$allIds[$v['seasonID']]]['periods'][] = $v;
						}
					}
				}
			}//END GET PERIODS
		}//END GET SEASONS
	
		return $r;
	}//END FUNCTION
	
	public function getSeason($id,$settings=0) {
		
	}//END FUNCTION
	
	public function addSeason($id,$settings=0) {
		
	}//END FUNCTION

	public function modifySeason($id,$settings=0) {
		
	}//END FUNCTION
	
	public function getPeriods($seasonID,$settings=0) {
		global $sql;
		$tables = "bookings_periods";
		$fields = ($settings['fields']) ? $settings['fields'] : '*';
		$orderby = ($settings['orderby']) ? $settings['orderby'] : ' orderby ';
		$way = ($settings['way']) ? $settings['way'] : ' ASC ';
		
		if (is_array($seasonID)) {
			$q[] = "seasonID IN (".implode(",",$seasonID).")";
		}
		elseif (is_array($seasonID) AND $season['getByItemID'])
		{
			
		}
		else {
			$q[] = "seasonID = $seasonID";
		}
		
		if ($q) {
			$query = implode(" AND ",$q);
			$mode = "default";
		}
		else { $mode = "no_where"; }
		
		$sql->db_Select($tables,$fields,"$query ORDER BY $orderby $way",$mode);	
		if ($settings['debug']) {
			echo "SELECT $fields FROM $tables WHERE $query ORDER BY $orderby $way";
		}
		if ($sql->db_Rows()) {
			$r = execute_multi($sql);
			if ($settings['formatPrice']) {
				foreach ($r as $k=>$v) {
					$r[$k]['price'] = formatprice($v['price']);
				}
			}
		}
		return $r;		
	}//END FUNCTION
	
	public function getPeriod($id,$settings=0) {
		
	}//END FUNCTION
	
	public function addPeriod($id,$settings=0) {
		
	}//END FUNCTION
	
	public function modifyPeriod($id,$settings=0) {
		
	}//END FUNCTION
	
	public function getRooms($itemID,$settings=0) {
		
	}//END FUNCTION
	
	public function getRoom($id,$settings=0) {
		
	}//END FUNCTION
	
	public function addRoom($id,$settings=0) {
		
	}//END FUNCTION
	
	public function modifyRoom($id,$settings=0) {
		
	}//END FUNCTION	
	
	public function getRentalTypes($settings=0)
	{
		global $sql;
		
		$fields = ($settings['fields']) ? $settings['fields'] : "*";
		$orderBy = ($settings['orderby']) ? $settings['orderby'] : "id";
		$way = ($settings['way']) ? $settings['way'] : "ASC";
		
		$sql->db_Select("bookings_rental_types",$fields,"ORDER BY $orderBy $way",$mode="no_where");
		if ($sql->db_Rows()) {
			$r = execute_multi($sql);
			return $r;
		}
	}

	public function getItemType($id,$settings=0) {
		global $sql;
		$tables = "bookings_rental_types_items";
		$fields = "type";
		
		if ($settings['extended']) {
			$tables .= " INNER JOIN bookings_rental_types ON (bookings_rental_types_items.type=bookings_rental_types.id)";
			$fields .= ($settings['fields']) ? ",".$settings['fields'] : ",uses";
		}
		
		$sql->db_Select($tables,$fields,"itemid = $id");
		if ($sql->db_Rows()) {
			if ($settings['mode'] == 'check') {
				return true;
			}
			$r = execute_single($sql);
			return $r;
		}
	}//END FUNCTION	
	
	public function setItemType($id,$typeID,$settings=0) {
		global $sql;
		if ($this->getItemType($id,array('mode'=>'check'))) {
			$sql->db_Update("bookings_rental_types_items","type = $typeID WHERE itemid = $id");
		}
		else {
			$sql->db_Insert("bookings_rental_types_items","$id,$typeID");
		}
	}//END FUNCTION	
	
	public function processSeasonData($postData,$settings=0) { 
		global $sql;
		
		foreach ($postData as $k=>$v) {
			if (strstr($k,'-')) {
				list($key,$id)=split("-",$k);
				$data[$id][$key] = $v;
			}
		}			
		$postedPeriodIDs = array();
			foreach ($data as $k=>$v) {
	//			print_r($v);
				foreach ($v as $key => $val) {
					if ($key == 'dateVal') {
						$t = explode("###",$val);
						$dateFrom = explode('/',$t[0]);
						$dateTo = explode('/',$t[1]);
						$season[$k]['start'] = mktime(0,0,0,$dateFrom[1],$dateFrom[0],$dateFrom[2]);
						$season[$k]['end'] = mktime(0,0,0,$dateTo[1],$dateTo[0],$dateTo[2]);
					}
					elseif ($key == 'periodID') {
						$season[$k][$key] = $val; 
						$postedPeriodIDs = array_merge($val,$postedPeriodIDs);
					}
					else {
						$season[$k][$key] = $val;
					}
					$postedIDs[$k] = $k;
				}
				}
				$postedPeriodIDs = array_flip($postedPeriodIDs);
//print_r($season);
				$postedIDs = array_unique($postedIDs);
			$existing = $this->getSeasons($postData['itemid'],array('getPeriods'=>1,'debug'=>0,'periodSettings'=>array('debug'=>0)));
			if ($existing) {
				foreach ($existing as $v) {
					if (!array_key_exists($v['id'],$postedIDs)) {
						$seasonsToDelete[] = $v['id'];
					}
					if ($v['periods']) {
						foreach ($v['periods'] as $k=>$p){
							if (!array_key_exists($p['id'],$postedPeriodIDs)) {
								$periodsToDelete[] = $p['id'];
							}
						}
					}
				}
			}

			if ($seasonsToDelete) {
				$sql->db_Delete("bookings_seasons","id IN (".implode(',',$seasonsToDelete).")");
				$sql->db_Delete("bookings_periods","seasonID IN (".implode(',',$seasonsToDelete).")");
			}
			if ($periodsToDelete) {
				$sql->db_Delete("bookings_periods","id IN (".implode(',',$periodsToDelete).")");
			}

			$orderby=0;
			foreach ($season as $k=>$v){
				
				$q = array();
				$q['itemid'] = $postData['itemid'];
				$q['title'] = "'".$v['season']."'";
				$q['start'] = $v['start'];
				$q['end'] = $v['end'];
				$q['minStay'] = $v['minStay'];
				$q['orderby'] = ($v['orderby']) ? $v['orderby'] : $orderby++;
				$minStay[] = $v['minStay'];
				if ($v['seasonIDNew']) {//INSERT
					$q['date_added'] = time();
					$sql->db_Insert("bookings_seasons (itemid,title,start,end,minStay,orderby,date_added)",implode(',',$q));
//					echo "INSERT INTO bookings_seasons (itemid,title,start,end,minStay,date_added) VALUES (".implode(',',$q).")<Br>";
					$new = $sql->last_insert_id;
					$ret[$k]['season'] = $new;
				}
				else {//UPDATE
					foreach ($q as $c=>$z) {
						$query[] = "$c = $z";
					}
					$sql->db_Update("bookings_seasons",implode(',',$query)." WHERE id = ".$v['seasonID']);
//					echo implode(',',$query)." WHERE id = ".$v['seasonID']."<Br>";
					unset($query);
				}

				$o = 0;
				foreach ($postData['period'] as $m=>$b) {
					unset($q);
					$q['seasonID'] = ($v['seasonID']) ? ($v['seasonID']) :$new;
					$q['itemid'] = $postData['itemid'];
					$q['title'] = "'$b'";
					$q['price'] = $v['price'][$m];
					$q['orderby'] = ($postData['orderbyPeriod'][$m]) ? $postData['orderbyPeriod'][$m] : $o++;
					$allPrices[] = $v['price'][$m];

					if (strstr($v['periodID'][$m],'new-') AND $q['seasonID']) {//INSERT

						$sql->db_Insert("bookings_periods (seasonID,itemid,title,price,orderby)",implode(',',$q));
//						echo "INSERT INTO bookings_periods (seasonID,title,price) VALUES (".implode(',',$q).")<Br>";
						$ret[$k]['period'][$v['periodID'][$m]] = $sql->last_insert_id;
/*						if (!$ret[$k]['season']) {
							$ret[$k]['season'] = $v['seasonID'];
						}*/
					}
					else {//UPDATE
						foreach ($q as $c=>$z) {
							$query[] = "$c = $z";
						}
						$sql->db_Update("bookings_periods",implode(',',$query)." WHERE id = ".$v['periodID'][$m]);
//						echo "UPDATE bookings_periods SET". implode(',',$query)." WHERE id = ".$v['periodID'][$m]."<Br>";
						unset($query);
					}
				}
			}
			
			if ($settings['saveToItem']) {
				$r['minStayAvg'] = array_sum($minStay)/ count($minStay);
				$r['avgPrice'] = array_sum($allPrices)/ count($allPrices);
				$r['minPrice'] = min($allPrices);
				$r['maxPrice'] = max($allPrices);
				$item = $this->GetItem($postData['itemid'],array('debug'=>0,'fields'=>'id,settings'));
				if (!is_array($item['settings'])) {
					$item['settings'] = array();
				}
				$item['settings'] = array_merge($item['settings'],$r);
				$sql->db_Update($this->settings['module']['name'],"settings = '".json_encode($item['settings'])."' WHERE id = ".$item['id']);
			}
		return $ret;
	}//END FUNCTION
	
	public function uniqueData($ar,$mode='period'){
		if (!$ar) {return ;}
		//input is seasons array
		if ($mode == 'seasons') {
			foreach ($ar as $k=>$v) {
				$periods[$v['id']][] = $v['periods'];
				$minStay[] = $v['minStay'];
			}
			$ar = $periods;
		}
		foreach ($ar as $v) {
			foreach ($v as $b) {
/*				print_r($v);
				echo "<br><br>";*/
			if ($b){
				foreach ($b as $x) {
					$tmp[] = $x['title'];
					$prices[$x['title']][] = $x['price'];
					$orderby[$x['title']] = $x['orderby'];
					$allPrices[] = $x['price'];
				}
			}
			}
		}

		if ($allPrices) {
			$r['minStayAvg'] = array_sum($minStay)/ count($minStay);
			$r['avgPrice'] = array_sum($allPrices)/ count($allPrices);
			$r['minPrice'] = min($allPrices);
			$r['maxPrice'] = max($allPrices);
			$tmp = array_unique($tmp);
			foreach ($tmp as $v) {
				$r['periods'][$v]['min'] = min($prices[$v]);
				$r['periods'][$v]['max'] = max($prices[$v]);
				$r['periods'][$v]['orderby'] = $orderby[$v];
			}
		}
		return $r;
	}//END FUNCTION
	
	public function priceRanges($itemID=0,$settings=0) {
		global $sql;
		$tables = "bookings_periods";
		$fields = "min(price) as min, max(price) as max";
		if ($itemID) {
//			$tables .= " INNER JOIN bookings_seasons ON (bookings_periods.seasonID=bookings_seasons.id)";
			if (is_array($itemID)) {
				$q[] = "itemid IN (".implode(",",$itemID).")";
			}
			else {
				$q[] = "itemid = $itemID";
			}
		}
		if ($q) {
			$query = implode(" AND ",$q);
		}
		
		$sql->db_Select($tables,$fields,$query);
		if ($settings['debug']) {
			echo "SELECT $fields FROM $tables WHERE $query";	
		}
		
		if ($sql->db_Rows()) {
			$r = execute_single($sql);
			if ($settings['return']) {			
				foreach ($r as $k=>$v) {
					if($settings['return'] == 'int') {
						$r[$k] = intval($r[$k]);
					}
					if ($settings['return'] == 'formated') {
						$r[$k] = formatprice($v);
					}
				}

			}
		}
			return $r;		
	}//END FUNCTION
	
	
}//END CLASS
?>