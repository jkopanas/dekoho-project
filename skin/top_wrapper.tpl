{if $layout_settings.page eq "hotel"}
 
<div id="top-wrapper">
	  <div class="content">
		<div id="logo-banner-container">
		<div id="hotel_header" class="float-left" >
	       
	       <span class="title spacer-right float-left">{$hotel.title}</span>
	       {while $hotel.stars > 0}
		       <img src="/images/site/star_green.png" class="float-left star"  alt=""/>
		       {$hotel.stars = $hotel.stars-1}	 
		 	{/while}
		 	<div class="seperator-small"></div>
		 	{foreach from=$location item=a name=b}
				{foreach from=$a item=c name=d}		
						<span>{$c}</span>
					 <div class="seperator-small"></div>	 
					{/foreach}
					{/foreach}
		</div>
		
		<div id="top-banner" class="float-right">
		<div id="logo">
		<a href="/{$FRONT_LANG}/index.html"><img src="/images/site/logo.jpg" alt="dekoho" title="dekoho"/></a>
		</div>
		</div>
		<br class="clear_0"/>
		</div> <!-- end logo-banner-container -->
	
	 </div> <!-- end top-content -->
</div> <!-- end top-wrapper -->
{else}
<div id="top-wrapper">
	  <div class="content">
		<div id="logo-banner-container">
		<div id="logo" class="float-left">
		<a href="/{$FRONT_LANG}/index.html"><img src="/images/site/logo.jpg" alt="dekoho" title="dekoho"/></a>
		</div>
		
		<div id="top-banner" class="float-right">
	<!--  	 <a href="#"><img src="/images/site/top-banner.jpg" alt="" title=""/></a>-->
		</div>
		<br class="clear_0"/>
		</div> <!-- end logo-banner-container -->
	
	 </div> <!-- end top-content -->
</div> <!-- end top-wrapper -->
	
{/if}	