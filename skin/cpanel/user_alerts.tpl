{if $USER}
<div class="navpath"> <a href="{$URL}/index.php" title="{$lang.home}" class="navpath">{$lang.home}</a> :: <a href="{$URL}/cpanel.php" title="{$lang.cpanel}" class="navpath">{$lang.cpanel}</a> :: {if $smarty.get.m} <a href="{$SELF}" title="{$lang.alerts}">{$lang.alerts}</a> :: {$lang.new_alert} {else} {$lang.alerts}{/if}</div>
<table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3" align="center" class="categoryBox">{$lang.available_alerts}</td>
  </tr>
  <tr>
    <td align="center" class="categoryBox">{$lang.title}</td>
    <td align="center" class="categoryBox">{$lang.date_added}</td>
    <td align="center" class="categoryBox">{$lang.last_activated}</td>
  </tr>
  {section name=a loop=$alerts}
  <tr>
    <td align="center"><a href="{$SELF}?m=upda&id={$alerts[a].id}">{$alerts[a].title}</a></td>
    <td align="center">{$alerts[a].date_added}</td>
	<td align="center">{$alerts[a].last_activated}</td>
  </tr>
  {/section}
  <tr>
  <td colspan="3" align="center"><a href="{$SELF}?m=g">{$lang.new_alert}</a></td>
  </tr>
  
</table>
{if $smarty.get.m eq "g" OR $smarty.post.mode eq "addg"}
{if $message}
<div class="error">{$message}</div>
<br>
{/if}
<form name="form" action="{$SELF}" method="post">
  

<div class="tab-pane" id="tabPane1">

<script type="text/javascript">
tp1 = new WebFXTabPane( document.getElementById( "tabPane1" ) );
//tp1.setClassNameTag( "dynamic-tab-pane-control-luna" );
//alert( 0 )
</script>
<br>
	<div class="tab-page" id="tabPage1">
		<h2 class="tab">{$lang.saved_searches}</h2>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2"><strong>Load criteria from previous saved searches</strong></td>
          </tr>
          <tr>
            <td>
			{if $searches}
			<select name="searches">
                <option value="">{$lang.select_one}</option>
              
	  {section name=s loop=$saved_searches}
	  
              <option value="{$saved_searches[s].id}">{$saved_searches[s].title}</option>
              
	  {/section}
      {else}
	  <br><br>
	  <font color="#FF0000"><strong>{$lang.no_searches_found}</strong></font>
	  {/if}
            </select>
            </td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"></td>
          </tr>
        </table>
		<script type="text/javascript">tp1.addTabPage( document.getElementById( "tabPage1" ) );</script>
  </div>
  <div class="tab-page" id="tabPage3">
		<h2 class="tab">{$lang.new_search} </h2>
		
		<script type="text/javascript">tp1.addTabPage( document.getElementById( "tabPage3" ) );</script>

<table width="100%" border=0>
          <tr>
            <td colspan="2"><strong>{$lang.create_new_alert_from_criteria}</strong></td>
          </tr>
        <TR>
          <TD width="94" height="10" nowrap class=FormButton>{$lang.sku} #</TD>
          <TD height="10"><INPUT name=sku type=text id="sku" value="{$data.sku}" size=6></TD>
        </TR>
        <tr>
          <td height="10" nowrap class=FormButton>{$lang.category}</td>
          <td height="10"><select name="categoryid">
              <option value="%">{$lang.all}</option>
                        
{section name=cat_num loop=$allcategories}

    
            <option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $data.categoryid or $default_categoryid}selected{/if}>{$allcategories[cat_num].category}</option>
            
    
{/section}

  
          </select></td>
        </tr>
        <tr>
          <td height="10" class=FormButton nowrap>&nbsp;</td>
          <td height="10"><input name="search_in_subcategories" type="checkbox" value="1" {if $data.search_in_subcategories eq 1}checked{/if}>
            {$lang.search_in_subcategories}</td>
        </tr>
        <tr>
          <td height="10" class=FormButton nowrap>{$lang.location}</td>
          <td height="10"><select name="locid">
              <option value="%">{$lang.all}</option>
            
     
{section name=e loop=$locs}

    
            <option value="{$locs[e].id}" {if $data.locid eq $locs[e].id}selected{/if}>{$locs[e].name}</option>
            
    
{/section}

  
          </select></td>
        </tr>
        <tr>
          <td height="10" class=FormButton nowrap>&nbsp;</td>
          <td height="10"><input name="search_in_sublocations" type="checkbox" value="1" {if $data.search_in_sublocations eq 1}checked{/if}>
            {$lang.search_in_sublocations}</td>
        </tr>
        <tr>
          <td height="10" class=FormButton nowrap>{$lang.min_price}</td>
          <td height="10"><INPUT name="min_price" type=text id="min_price" value="{$data.min_price}" size=6>
            {$lang.max_price}
            <INPUT name="max_price" type=text id="max_price" value="{$data.max_price}" size=6></td>
        </tr>
        <tr>
          <td height="10" class=FormButton nowrap>{$lang.pattern}</td>
          <td height="10"><input type="text" name="substring" size="30" value="{$data.substring}">          </td>
        </tr>
        <tr>
          <td class=FormButton>{$lang.search_in}</td>
          <td><input name="search_main" type="checkbox" id="search_main" value="1" {if $data.search_main ne "1"} {else}checked{/if}>
            {$lang.title}
            <input name="search_short" type="checkbox" id="search_short" value="1" {if $data.search_short ne "1"} {else}checked{/if}>
            {$lang.short_desc}
            <input name="search_long" type="checkbox" id="search_long" value="1" {if $data.search_long ne "1"} {else}checked{/if}>
            {$lang.long_desc}</td>
        </tr>
        {if $extra_fields}
  <tr>
    <td colspan="3" class=FormButton> {* Show Hide Advanced search *}
      <TABLE>
          <TR>
            <TD id="close1" style="cursor: hand;" onClick="visibleBox('1')"><IMG src="{$ImagesDir}/plus.gif" border="0" align="absmiddle" alt="Click to open"></TD>
            <TD id="open1" style="display: none; cursor: hand;" onClick="visibleBox('1')"><IMG src="{$ImagesDir}/minus.gif" border="0" align="absmiddle" alt="Click to close"></TD>
            <TD><A href="javascript:void(0);" onClick="visibleBox('1')"><B>{$lang.advanced_search}</B></A></TD>
          </TR>
        </TABLE>
      <br>
      {* Advanced Search *}
      <TABLE border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none;" id="box1" name="box1">
        <tr>
          <td class=FormButton>{$lang.search_also}</td>
          <td><TABLE width="100%" border=0>
            <TR> {section name=field loop=$extra_fields}
              <TD> {assign var="aaa" value="efields-"|concat:$extra_fields[field].fieldid}
                <input type="checkbox" name="efields-{$extra_fields[field].fieldid}" value="1" {if $data.$aaa eq 1}checked{/if}>
                {$extra_fields[field].field}</TD>
              {* see if we should go to the next row *} 
              {if not ($smarty.section.field.rownum mod $columns)} 
              {if not $smarty.section.field.last} </TR>
            <TR> {/if} 
              {/if} 
              {if $smarty.section.field.last} 
              {* pad the cells not yet created *} 
              {math equation = "n - a % n" n=$columns a=$data|@count assign="cells"} 
              {if $cells ne $columns} 
              {section name=pad loop=$cells}
              <TD>&nbsp;</TD>
              {/section} 
              {/if} </TR>
            {/if} 
            {/section}
          </TABLE></td>
        </tr>
        {section name=field loop=$extra_fields}
        {if $extra_fields[field].type eq "radio"}
        <tr>
              <td class=FormButton>{$extra_fields[field].field}</td>
          <td> {assign var="aaa" value="efields$"|concat:$extra_fields[field].fieldid}
            <input type="checkbox" name="efields${$extra_fields[field].fieldid}" value="1" {if $data.$aaa eq 1}checked{/if}>              </td>
        </tr>
        {/if}
        {/section}
        <tr>
        </table></td>
  </tr>
        {/if}{* End of extra fields *}
    </table>


  </div>
</div>
<table width="100%">
    <tr>
      <td>{$lang.title}</td>
      <td><input name="title" type="text" id="title"></td>
    </tr>
        <tr>
          <td class=FormButton>{$lang.notify_via}</td>
          <td colspan="2"><input name="notify_email" type="checkbox" id="notify_email" value="1">
            {$lang.email} <strong>{$EMAIL} </strong></td>
        </tr>
        <tr>
    <td width="94" class=FormButton>&nbsp;</td>
    <td colspan="2"><input name="submit" type=submit value="{$lang.save}">
        <input name="mode" type="hidden" id="mode" value="addg">
        </td>
  </tr>
 </table>
</form>
{elseif $smarty.get.m eq "upda"}

<form id="form1" name="form1" method="post" action="{$SELF}">
{capture name=dialog}
<table width="100%" border=0>
        <TR>
          <TD width="94" height="10" nowrap class=FormButton>{$lang.sku} #</TD>
          <TD height="10"><INPUT name=sku type=text id="sku" value="{$data.sku}" size=6></TD>
        </TR>
        <tr>
          <td height="10" nowrap class=FormButton>{$lang.category}</td>
          <td height="10"><select name="categoryid">
              <option value="%">{$lang.all}</option>
                        
{section name=cat_num loop=$allcategories}

    
            <option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $data.categoryid or $default_categoryid}selected{/if}>{$allcategories[cat_num].category}</option>
            
    
{/section}

  
          </select></td>
        </tr>
        <tr>
          <td height="10" class=FormButton nowrap>&nbsp;</td>
          <td height="10"><input name="search_in_subcategories" type="checkbox" value="1" {if $data.search_in_subcategories eq 1}checked{/if}>
            {$lang.search_in_subcategories}</td>
        </tr>
        <tr>
          <td height="10" class=FormButton nowrap>{$lang.location}</td>
          <td height="10"><select name="locid">
              <option value="%">{$lang.all}</option>
            
     
{section name=e loop=$locs}

    
            <option value="{$locs[e].id}" {if $data.locid eq $locs[e].id}selected{/if}>{$locs[e].name}</option>
            
    
{/section}

  
          </select></td>
        </tr>
        <tr>
          <td height="10" class=FormButton nowrap>&nbsp;</td>
          <td height="10"><input name="search_in_sublocations" type="checkbox" value="1" {if $data.search_in_sublocations eq 1}checked{/if}>
            {$lang.search_in_sublocations}</td>
        </tr>
        <tr>
          <td height="10" class=FormButton nowrap>{$lang.min_price}</td>
          <td height="10"><INPUT name="min_price" type=text id="min_price" value="{$data.min_price}" size=6>
            {$lang.max_price}
            <INPUT name="max_price" type=text id="max_price" value="{$data.max_price}" size=6></td>
        </tr>
        <tr>
          <td height="10" class=FormButton nowrap>{$lang.pattern}</td>
          <td height="10"><input type="text" name="substring" size="30" value="{$data.substring}">          </td>
        </tr>
        <tr>
          <td class=FormButton>{$lang.search_in}</td>
          <td><input name="search_main" type="checkbox" id="search_main" value="1" {if $data.search_main ne "1"} {else}checked{/if}>
            {$lang.title}
            <input name="search_short" type="checkbox" id="search_short" value="1" {if $data.search_short ne "1"} {else}checked{/if}>
            {$lang.short_desc}
            <input name="search_long" type="checkbox" id="search_long" value="1" {if $data.search_long ne "1"} {else}checked{/if}>
            {$lang.long_desc}</td>
        </tr>
        {if $extra_fields}
  <tr>
    <td colspan="3" class=FormButton> {* Show Hide Advanced search *}
      <TABLE>
          <TR>
            <TD id="close1" style="cursor: hand;" onClick="visibleBox('1')"><IMG src="{$ImagesDir}/plus.gif" border="0" align="absmiddle" alt="Click to open"></TD>
            <TD id="open1" style="display: none; cursor: hand;" onClick="visibleBox('1')"><IMG src="{$ImagesDir}/minus.gif" border="0" align="absmiddle" alt="Click to close"></TD>
            <TD><A href="javascript:void(0);" onClick="visibleBox('1')"><B>{$lang.advanced_search}</B></A></TD>
          </TR>
        </TABLE>
      <br>
      {* Advanced Search *}
      <TABLE border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none;" id="box1" name="box1">
        <tr>
          <td class=FormButton>{$lang.search_also}</td>
          <td><TABLE width="100%" border=0>
            <TR> {section name=field loop=$extra_fields}
              <TD> {assign var="aaa" value="efields-"|concat:$extra_fields[field].fieldid}
                <input type="checkbox" name="efields-{$extra_fields[field].fieldid}" value="1" {if $data.$aaa eq 1}checked{/if}>
                {$extra_fields[field].field}</TD>
              {* see if we should go to the next row *} 
              {if not ($smarty.section.field.rownum mod $columns)} 
              {if not $smarty.section.field.last} </TR>
            <TR> {/if} 
              {/if} 
              {if $smarty.section.field.last} 
              {* pad the cells not yet created *} 
              {math equation = "n - a % n" n=$columns a=$data|@count assign="cells"} 
              {if $cells ne $columns} 
              {section name=pad loop=$cells}
              <TD>&nbsp;</TD>
              {/section} 
              {/if} </TR>
            {/if} 
            {/section}
          </TABLE></td>
        </tr>
        {section name=field loop=$extra_fields}
        {if $extra_fields[field].type eq "radio"}
        <tr>
              <td class=FormButton>{$extra_fields[field].field}</td>
          <td> {assign var="aaa" value="efields$"|concat:$extra_fields[field].fieldid}
            <input type="checkbox" name="efields${$extra_fields[field].fieldid}" value="1" {if $data.$aaa eq 1}checked{/if}>              </td>
        </tr>
        {/if}
        {/section}
        <tr>
        </table></td>
  </tr>
        {/if}{* End of extra fields *}
    <tr>
      <td>{$lang.title}</td>
      <td><input name="title" type="text" id="title" value="{$alert.title}"></td>
    </tr>
        <tr>
          <td class=FormButton>{$lang.notify_via}</td>
          <td colspan="2"><input name="notify_email" type="checkbox" id="notify_email" value="{$alert.notify}" {if $alert.notify eq 1}checked{/if}>
            {$lang.email} <strong>{$EMAIL} </strong></td>
        </tr>
<tr>
<td align="center" colspan="6">
<input type="submit" value="{$lang.save}" /><input type="hidden" name="mode" value="update_alert" /><input name="id" type="hidden" id="mode2" value="{$smarty.get.id}" /><input name="searchid" type="hidden" id="mode2" value="{$alert.searchid}" /></td>
</tr>
    </table>
{/capture}{include file="dialog.tpl" content=$smarty.capture.dialog title=$lang.update_alert extra="width=100%"}

</form>
{/if}
{else}
  <div align="center">{include file="user_login.tpl}</div>
  {if $LOGINMESSAGE ne ""}
  <br>
   <div align="center"><font color="#FF0000"><b>{$LOGINMESSAGE}</b></font></div>
{/if}
{/if}
