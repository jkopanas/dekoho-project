<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>{$product.title}</title>
<link href="{$CssDir}/style_front.css" rel="stylesheet" type="text/css">
</head>

<body onload="window.print()">
{capture name=dialog}
<table width="100%"  border="0">
  <tr align="center">
    <td><a name="top"></a>{include file="menu_product.tpl"}</td>
  </tr>
  <tr>
    <td valign="top"><img src="{$product.image}" border="0" alt="{$product.title}" title="{$product.title}" align="left" class="imgborder">{$lang.sku} : #{$product.sku}<br>
{$lang.location} : 
{section name=w loop=$product.nav_loc}<a href="{$URL}/loc.php?cat={$product.nav_loc[w].categoryid}" class=navpath title="{$product.nav_loc[w].category}">{$product.nav_loc[w].category}</a> {if !$nav[w].last}:: {/if}{/section}
<br />
{$lang.category} :
{section name=w loop=$product.nav_cat}<a href="{$URL}/cat.php?cat={$product.nav_cat[w].categoryid}" class=navpath title="{$product.nav_cat[w].category}">{$product.nav_cat[w].category}</a> {if !$nav[w].last}:: {/if}{/section}
	  <br>	  <span class="description">{$product.description}</span>	</td>
  </tr>
  <tr>
    <td valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
	{if $product.description}	  {/if}
	    {if $product.full_description}
      <tr>
        <td>{$product.full_description}</td>
      </tr>
	    {/if}
      <tr>
        <td>
{if $classes}
<table width="100%" border="0">
{section name=t loop=$classes}
<tr>
<td>
{$classes[t].classtext}: 
{section name=az loop=$classes[t].options}
{$classes[t].options[az].option_name}{if !%az.last%} - {/if}
{/section}
</td>
</tr>
{/section}
</table>
{/if}
		</td>
      </tr>
{if $extra_fields}
      <tr>
        <td>
<TABLE width="100%" border=0>
  <TR> 
{section name=field loop=$extra_fields}
{assign var="aaa" value="efields-"|concat:$extra_fields[field].fieldid}
      <TD valign="top">

        <div style="width:210; float:left;" align=center>
          <table cellpadding=2 cellspacing=1 width=210 height=30>
            <tr bgcolor="#d5dde7">
              <td width=60%><strong>{$extra_fields[field].field}</strong></td>
              <td>
			  {if $extra_fields[field].type eq "radio"} {if $extra_fields[field].value eq 1} {$lang.yes} {else}{$lang.no}{/if} {else} {$extra_fields[field].value} {/if}</td>
            </tr>
          </table>
        </div></TD>
      {* see if we should go to the next row *} {if not ($smarty.section.field.rownum mod 3)} {if not $smarty.section.field.last} </TR>
  <TR> {/if} {/if} {if $smarty.section.field.last} {* pad the cells not yet created *} {math equation = "n - a % n" n=3 a=$products|@count assign="cells"} {if $cells ne 3} {section name=pad loop=$cells}
    <TD>&nbsp;</TD>
    {/section} {/if} </TR>
  {/if} {/section}
</TABLE>
		  </td>
      </tr>
{/if} 
    </table>
{if $contacts}
      {$contacts.name} {if $contacts.email}< <a href="mailto:{$contacts.email}">{$contacts.email}</a> >{/if}<br>
    {$contacts.contact}
{/if}
</td>
  </tr>
</table>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$product.title extra="width=100%"}
<div align="right"><a href="{$SELF}?id={$product.id}#top" title="{$lang.top}">{$lang.top}</a></div>
{if $images}
<a name="images"></a>
<br>
{capture name=dialog}
<br>
{include file="common/detailed_images.tpl"}
{/capture}{include file="dialog.tpl" content=$smarty.capture.dialog title=$lang.detailed_images extra="width=100%"}
<div align="right"><a href="{$SELF}?id={$product.id}#top" title="{$lang.top}">{$lang.top}</a></div>
{/if}
{if $floor_plans}
<a name="floors"></a>
<br>
{capture name=dialog}
<br>
{include file="common/floor_plans.tpl"}
{/capture}{include file="dialog.tpl" content=$smarty.capture.dialog title=$lang.floor_plans extra="width=100%"}
<div align="right"><a href="{$SELF}?id={$product.id}#top" title="{$lang.top}">{$lang.top}</a></div>
{/if}
{if $maps}
<a name="maps"></a>
<br>
{capture name=dialog}
<br>
{include file="common/maps.tpl"}
{/capture}{include file="dialog.tpl" content=$smarty.capture.dialog title=$lang.maps extra="width=100%"}
<div align="right"><a href="{$SELF}?id={$product.id}#top" title="{$lang.top}">{$lang.top}</a></div>
{/if}
{if $vtours}
<a name="vtours"></a>
<br>
{capture name=dialog}
<br>
{include file="common/vtours.tpl"}
{/capture}{include file="dialog.tpl" content=$smarty.capture.dialog title=$lang.panoramic_images extra="width=100%"}
<div align="right"><a href="{$SELF}?id={$product.id}#top" title="{$lang.top}">{$lang.top}</a></div>
{/if}
{if $related}
<a name="related"></a>
<br>
{capture name=dialog}
{section name=o loop=$related}
<img src="{$ImagesDir}/arrow.gif" width="12" height="10"> <a href="{$URL}/product.php?id={$related[o].id}" title="{$related[o].title}">{$related[o].title}</a><br>
{/section}
{/capture}{include file="dialog.tpl" content=$smarty.capture.dialog title=$lang.upselling_links extra="width=100%"}
<div align="right"><a href="{$SELF}?id={$product.id}#top" title="{$lang.top}">{$lang.top}</a></div>
{/if}
{if $municipalities}
<a name="mun"></a>
<br>
{capture name=dialog}
{$municipalities.name}<br>
{$municipalities.municipality}
{/capture}{include file="dialog.tpl" content=$smarty.capture.dialog title=$municipalities.name extra="width=100%"}
<div align="right"><a href="{$SELF}?id={$product.id}#top" title="{$lang.top}">{$lang.top}</a></div>
{/if}
</body>
</html>
