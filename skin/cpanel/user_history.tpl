<table width="400" border="0" align="center" cellpadding="0" cellspacing="1">
  {if $history ne 0}
  <tr>
      <td align="center" class="categoryBox">{$lang.title}</td>
    <td align="center" class="categoryBox">{$lang.date_added}</td>
  </tr>
  {section name=f loop=$history}
  <tr>
    <td><a href="{$URL}/product.php?id={$history[f].id}">{$history[f].title}</a></td>
    <td>{$history[f].htr_date}</td>
  </tr>
  {/section}
  {else}
  <tr>
    <td colspan="3">{$lang.no_history}</td>
  </tr>
  {/if}
</table>
