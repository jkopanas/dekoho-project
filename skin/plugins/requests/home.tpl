<input type="hidden" id="total_users" value="{$total}"/>
<div class="dp100">
    <div class="box_c">
           <h3 class="box_c_heading cf"><span class="fl spacer-left"> {$lang.newsletter} &nbsp;  </span></h3> 
			<div class="box_c_content cf" > 
				  	<div id="ShowRequests" data-id="69" class="LoadBox"></div>
			</div>			
    </div>  
</div>
<div id="OpenRequests" class="hidden formEl_a">
<fieldset>
        <legend>User Changes</legend>    
        <div class="sepH_b  float-left dp100" id="ShowChanges">
        
        </div>
</fieldset>
</div>
<div id="FilterRequests" class="hidden formEl_a">
<form name="requestForm"  id="requestForm">
<fieldset>
        <legend>User Filter</legend>      
        <div class="sepH_b  float-left dp50">
       		<label for="id" class="lbl_a">User ID:</label>
        	<input name="uname" type="text" value="" data-table="users" data-mode="eq" class="filterdata inpt_a">
        </div>
        <div class="sepH_b  float-left dp50">
        	<label for="title" class="lbl_a">Hotel title:</label>
        	<input name="title" id="title" data-table="hotel" data-mode="like" type="text" value="" class="filterdata inpt_a">
        </div>
        <div class="sepH_b float-left dp50" >
        <label for="available" class="lbl_a">Country : </label>
        	<select name="countryid" id="countryies" data-table="geo_relations" data-mode="eq" class="filterdata inpt_d">
        		<option value="">&nbsp;</option>
        		{foreach from=$Countries key=k item=v}
        		<option value="{$k}">{$v}</option>
        		{/foreach}
      		</select> 
        </div>
        <div class="sepH_b float-left dp50" >
        <label for="available" class="lbl_a">Request Status : </label>
        	<select name="status" id="fstatus"  data-table="users_request" data-mode="eq" class="filterdata inpt_d" data-placeholder="Select one">
        		<option value="">&nbsp;</option>
        		<option value="0" selected>Non replied Requests</option>
        		<option value="1">Replied Requests</option>
      		</select> 
        </div>
                <div class="sepH_b float-left dp50">
        <label for="available" class="lbl_a">New Requests: </label>
        	<input type="checkbox" name="new" class="filterdata"/>
        </div>
        <div class="seperator"></div>
         <div class="sepH_b float-left dp50">
        	<label for="date" class="lbl_a float-left">{$lang.subscr_date} : </label>
        	<input type="hidden" name="date-from" data-table="users_request" data-mode="gt" id="joinfrom" class="filterdata" value="" />
        	<input type="hidden" name="date-to" data-table="users_request" data-mode="lt" id="jointo" class="filterdata" value="" />
        	<div class="calendar float-left spacer-right clear" data-trigger="joinfrom"></div>	
     	  	<div class="calendar float-left" data-trigger="jointo"></div>
     	  	<div class="clear"></div>
        </div>
        <div class="spacer-right spacer-top clear">
        	<center><input name="submit" type="button"  id="SearchRequests"  class="btn btn_a fSearch" value="{$lang.search}" /></center> 
        </div>
<style scoped>

.k-calendar {
	width:183px;
}
</style>
        
<style scoped="">

.k-calendar {
	width:183px;
}
</style>


          
        </fieldset>
      </form>
</div>

		
		<script id="template-toolbar-PagesSearch" class="template-toolbar-PagesSearch" type="text/kendo-ui-template">
        		<a href="\\#" data-table='all' class="k-button filterTable pointer spacer-right">All</a>
				<a href="\\#" data-table='hotel' class="k-button filterTable pointer spacer-right">Hotel</a>
				<a href="\\#" data-table='rooms' class="k-button filterTable  pointer spacer-right">Rooms</a>
				<a href="\\#" data-table='hotspots' class="k-button filterTable  pointer spacer-right">Hotspots</a>
				<a href="\\#" data-table='item_images' class="k-button filterTable pointer spacer-right">Photos</a>
				<a href="\\#" data-table='extra_field_values' class="k-button filterTable pointer spacer-right">Locations & Policy</a>
				<a href="\\#" class="k-button k-filter pointer spacer-right">Filters</a>
		</script>

		
		<script id="id-commands-gridview" class="id-commands-gridview" type="text/kendo-ui-template">
				<a href="\\#" class="k-button pointer ShowRequest" data-admin="{$ID}" data-uid="#= userid #" data-id="#= hotelid #">Open Request</a>
        </script>
        
        
        <script type="text/x-kendo-template" id="template-date_added-gridview">
				#= new Date(date_added*1000) #
			</script>
        
        <script id="open-request" class="open-request" type="text/kendo-ui-template">
			# for (i in results) { #
				# if ( i == "rooms" ) { #
 					<a href="/en/hotelier/index.html\\#hotelManager/rooms/room/#= roomid #" target="_blank"> #= i #&nbsp;(#= results[i] #)</a><br/>
				# } else if ( i == "rooms_photos" ) { #
					<a href="/en/hotelier/index.html\\#hotelManager/rooms/room/#= roomid #/photos/rooms" target="_blank"> Rooms Photos&nbsp;(#= results[i] #)</a><br/>
				# } else if ( i == "hotel_photos" ) { #
					<a href="/en/hotelier/index.html\\#hotelManager/hotel/#= hotelid #/photos/hotel"  target="_blank"> Hotel Photos&nbsp;(#= results[i] #)</a><br/>
				# } else { #
					<a href="/en/hotelier/index.html\\#hotelManager/#= i #/#= hotelid #"  target="_blank"> #= i #&nbsp;(#= results[i] #)</a><br/>
				# } # 
			# } #
        </script>

{if $plugin_file}
	{include file=$plugin_file}
{/if}
<script type="text/javascript">
	head.js('/scripts/plugins/requests/requests.js');
	head.js('/scripts/admin2/grid.js');
</script>
