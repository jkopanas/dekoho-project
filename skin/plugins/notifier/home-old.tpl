<div class="debug"></div>
	<div class="dp100">
		<div class="box_c" />
		<h3 class="box_c_heading cf">
			<span class="fl spacer-left">Notifications</span>
		</h3>
		<div class="box_c_content cf">    
			<div id="notifierGrid"></div>
			<div id="details"></div>
		</div>
	</div>
</div>

<script id="confirmationModal" type="text/x-kendo-template">
	<p class="confirmationMessage " style="padding:10px;">{$lang.confirm_deletion}</p>
	<button class="confirmationConfirmBtn k-button dp25 "style="padding:2px; margin-left: 25px;">{$lang.yes}</button>
	<button class="confirmationCancelBtn k-button dp25" style="padding:2px; margin-left: 25px;">{$lang.no}</button>
</script>
<script id="id-toolbar-lang" type="text/x-kendo-template">
	<div class='k-button k-button-icontext clear-button float-left'  >
		<span class=' k-icon k-clear-filter'></span>clear filters
	</div>
	<div class='k-button k-button-icontext active-button float-right' data-code="0"  >
		<span class=' k-icon k-disable'></span>
		<span class="textToShow">Show Inactive</span>
		<span class="textToHide hidden">Show Active</span>
	</div>
</script>

<script>
head.js('../scripts/plugins/notifier/Notifier.js');
</script>
