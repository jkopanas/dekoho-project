<div class="dataTables_wrapper" id="data_table_wrapper">
	<div class="k-toolbar k-grid-toolbar">
		<div id="data_table_length" class="dataTables_length inline-block">
			<label>{$lang.show} 
				<select size="1" name="data_table_length">
					<option value="10" selected="selected">10</option>
					<option value="25">25</option>
					<option value="50">50</option>
					<option value="100">100</option>
				</select> 
				{$lang.items}
			</label>
		</div>
		<a class="k-button k-button-icontext statusChangeBtn" style="display: none;" href="#" id="enableItemsBtn">
			<span class="k-icon k-enable"></span>{$lang.show} {$lang.active} {$lang.items}
		</a>
		<a class="k-button k-button-icontext statusChangeBtn" href="#" id="disableItemsBtn">
			<span class="k-icon k-disable"></span>{$lang.show} {$lang.inactive} {$lang.items}
		</a>
		<a class="k-button k-button-icontext k-grid-delete" href="#" id="deleteItemsBtn">
			<span class="k-icon k-delete"></span>{$lang.delete}
		</a>
	</div>

	<table cellpadding="0" cellspacing="0" border="0" class="table_a smpl_tbl" id="notifier">
		<thead>
			<tr>
			  <th class="chb_col"><div class="th_wrapp"><input type="checkbox" class="chSel_all inpt_c1"></div></th>
				<th width="60"><div class="th_wrapp">#ID</div></th>
			  <th><div class="th_wrapp">Title</div></th>
				<th><div class="th_wrapp">Module</div></th>
				<th><div class="th_wrapp">Date imported</div></th>
				<th><div class="th_wrapp">Status</div></th>
			</tr>
			<tr class="filtersRow">
				<th class="chb_col">
					<div class="th_wrapp">
						<div class="k-button k-button-icontext clear-button float-left">
							<span class=" k-icon k-clear-filter" title="clear filters"></span>
						</div>
					</div>
				</th>
				<th><div class="th_wrapp"><input name="id" type="text" class="filter" size="4" /></div></th>
				<th><div class="th_wrapp"><input name="description" type="text" class="filter"  /></div></th>
				<th>
					<div class="th_wrapp">
						<select name="module" class="filter k-dropdown inline-block">
							<option value="" style="display:none;" selected></option>
							<option value="components">Components</option>
							<option value="ships">Ships</option> 
							<option value="promotions">Promotions</option> 
							<option value="shipareas">Shipareas</option> 
							<option value="cabins">Cabins</option> 
							<option value="brands">Brands</option> 
							<option value="cruises">Cruises</option> 
							<option value="decks">Decks</option> 
							<option value="itinerary">Itinerary</option> 
							<option value="ports">Ports</option> 
							<option value="regions">Regions</option> 
						</select>
					</div>
				</th>
				<th><div class="th_wrapp"><input name="date_added" type="text" data-type="date" class="filter" /></div></th>
				<th>
					<div class="th_wrapp">
						<select name="status" class="filter k-dropdown inline-block">
							<option value="" style="display:none;" selected></option>
							<option value="UPDATED">Updated</option>
							<option value="INSERTED">Inserted</option> 
						</select>
					</div>
				</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	<div class="dataTables_info" id="data_table_info">
		Showing 
		<div id="firstRowIDPTR" style="display: inline;"></div> 
		to 
		<div id="lastRowIDPTR" style="display: inline;"></div>
		of
		<div id="totalRowsPTR" style="display: inline;"></div>
		entries
	</div>
	<div class="dataTables_paginate paging_two_button" id="data_table_paginate">
		<div class="paginate_disabled_previous" title="Previous" id="data_table_previous"></div>
		<div class="paginate_enabled_next" title="Next" id="data_table_next"></div>
	</div>
	<div class="clear"></div>
</div>


{literal}
<div id="notifier-templates">
	<script id="row" type="text/kendo-ui-template" data-type="text">
	<tr>
		<td class="chb_col"><input type="checkbox" name="row_sel" class="inpt_c1" data-id="#= id #"></td>
		<td><a href="\\#" class="editMe" data-id="#= key #" data-module="#= module #">#= id #</a></td>
		<td><a href="\\#" class="editMe" data-id="#= key #" data-module="#= module #">#= description #</a></td>
		<td>#= module #</td>
		<td>#= mcms.strftime( date_added,'%d/%m/%Y @ %H:%M') #</td>
		<td>#= status #</td>
	</tr>
	</script>
	<script id="loader" type="text/kendo-ui-template" data-type="text">
		<tr><td colspan="6" align="center"><img src="../images/ajax-loader.gif" />Loading...</td><tr>
	</script>
	<script id="form" type="text/kendo-ui-template" data-type="text">
	    <div class="dp50">
	    <div class="box_c">
	    <div class="box_c_heading cf">New</div>
	    <div class=class="box_c_content cf">
	    	<div class="formEl_a">
		   		<div class="transactall-btn float-right lbl_a" style="margin-right: -25px; pointer:cursor;" >
					<icon-btn class="transact-btn" title="commit"></icon-btn>
					<span>Merge</span>
				</div>
			</div>
	    #var t = ''; t = me.templates[me.options.forms[a.module]+"-revision"](records.revision)#
		#= t #
	    </div><!-- END CONTENT -->
	    
	    </div><!-- END BOX -->
	    </div><!-- END LEFT -->
	    <div class="dp50">
	    <div class="box_c">
	    <div class="box_c_heading cf">Old</div>
	    <div class=class="box_c_content cf">
	    	<div class="clear" style="height:27px;" ></div>
	    #var t = ''; t = me.templates[me.options.forms[a.module]+"-current"](records.current)#
		#= t #
	    </div><!-- END CONTENT -->
	    <div class="hidden" >
			<input type="hidden" id="target-module" value='#= a.module #' />
			<input type="hidden" id="target-id" value='#= a.id #' />
	    </div>
	    </div><!-- END BOX -->
	    </div><!-- END LEFT -->   
	    <div class="clear"></div>
	</script>
	<script id="item-revision" type="text/kendo-ui-template" data-type="text">
		<div class="formEl_a revision">
			<div class="seperator"></div>
			<div class="sepH_b">
			    <label for="title" class="lbl_a">
			    	#= $.lang.title #
			    	<div class="transact-btn float-right" data-name="title">
						<icon-btn class="transact-btn" title="commit"></icon-btn>
						<span>Commit</span>
					</div>
			    </label>
			    <input type="text" id="title" name="title" class="inpt_a toSave"   value='#= title.value || title #' required  />
			    <span class="f_help">Here you can enter a headline.</span>
			</div>
			<div class="sepH_b">
			    <label for="description" class="lbl_a">
			    	#= $.lang.description #
			    	<div class="transact-btn float-right" data-name="description">
						<icon-btn class="transact-btn" title="commit"></icon-btn>
						<span>Commit</span>
					</div>
			    </label>
				<textarea id="description" name="description" class="inpt_a toSave">#= description.value || description #</textarea>
			    <span class="f_help">Here you can enter a headline.</span>
			</div>
			<div class="sepH_b">
			    <label for="permalink" class="lbl_a">
			    	#= $.lang.permalink #
			    	<div class="transact-btn float-right" data-name="permalink">
						<icon-btn class="transact-btn" title="commit"></icon-btn>
						<span>Commit</span>
					</div>
			    </label>
				<textarea id="permalink" name="permalink" class="inpt_a toSave">#= permalink.value || permalink #</textarea>
			    <span class="f_help">Here you can enter a headline.</span>
			</div>
		</div><!-- END FORM -->
	</script>
	<script id="item-current" type="text/kendo-ui-template" data-type="text">
		<div class="formEl_a current">
			<div class="seperator"></div>
			<div class="sepH_b">
			    <label for="title" class="lbl_a">
			    	#= $.lang.title #
			    </label>
			    <input type="text" id="title" name="title" class="inpt_a toSave"   value='#= title.value || title #' required  />
			    <span class="f_help">Here you can enter a headline.</span>
			</div>
			<div class="sepH_b">
			    <label for="description" class="lbl_a">
			    	#= $.lang.description #
			    </label>
				<textarea id="description" name="description" class="inpt_a toSave">#= description.value || description #</textarea>
			    <span class="f_help">Here you can enter a headline.</span>
			</div>
			<div class="sepH_b">
			    <label for="permalink" class="lbl_a">
			    	#= $.lang.permalink #
			    </label>
				<textarea id="permalink" name="permalink" class="inpt_a toSave">#= permalink.value || permalink #</textarea>
			    <span class="f_help">Here you can enter a headline.</span>
			</div>
			<div class="save-btn float-right clear" data-value='#= id.value || id #' >
				<icon-btn class="save-btn" title="save"></icon-btn>
				<span>Save</span>
			</div>
		</div><!-- END FORM -->
	</script>
	<script id="components" type="text/kendo-ui-template" data-type="text">
	</script>
	<script id="confirmationModal" type="text/x-kendo-template">
		<p class="confirmationMessage " style="padding:10px;">{$lang.confirm_deletion}</p>
		<button class="confirmationConfirmBtn k-button dp25 "style="padding:2px; margin-left: 25px;">{$lang.yes}</button>
		<button class="confirmationCancelBtn k-button dp25" style="padding:2px; margin-left: 25px;">{$lang.no}</button>
	</script>
</div>

<script>
head.js('/scripts/plugins/notifier/importNotifier.js');
head.ready(function() {
	$('#notifier').notifier({templates:'#notifier-templates'});
});
</script>

<style>
.AutoCompleteresults { background:white; z-index:9999; position:absolute; border:1px solid #ebebeb }
.AutoCompleteresults li { padding:5px; color:#777777}
.AutoCompleteresults li:hover, .hovered { background:#eeeeee;}
.AutoCompleteresults  .matched { color:black; font-weight:bold; background:#ffffcc; }
.catItem { border:1px solid #DFDFDF; padding:10px; line-height:130%; color:#555; background-color:#f3f3f3; min-height:31px;   }

#modal-back { position: fixed; top: 0; left: 0; width: 100%;  height: 100%;   background: #000;   opacity: 0.5;   filter: alpha(opacity=50);}
#modal { top:50%; left:50%;position:absolute;z-index:9999; width:900px; height:100%; max-height: 550px;  }
#modal .inner {  background:#fff; height:100%; max-height: 550px; width:100%;  position:relative; color:black; border:1px solid black; padding-top:10px; padding-bottom:10px; overflow-y:scroll; }
#modal .inner h1 { color:#143856; font-size:22px; text-align:center; padding-top:40px; position:relative;font-weight:normal; }
#modal .inner h2 { color:#ed1c24; font:26px; position:absolute; bottom:100px; left:240px; font-weight:normal; }
#modal .inner .noResults { position:absolute; bottom:1px; height:70px; color:#0374a6; font-size:14px; width:100%; }
#modal .inner .noResults h3{  margin-bottom:20px; text-align:center; font-weight:normal; font-size:14px;  }
#modal .inner .noResults p { margin-bottom:10px; text-align:center; }
#modal .close { position:absolute;  background:url('/skin/images/admin2/close.png'); width:36px; height:36px; top:-20px; right:-15px; text-indent:-5000px; z-index:9999; cursor:pointer; }
.filtersRow input { width:95%; }
.filtersRow select { width:95%; }
.filtersRow th { text-align:center; }
.dataTables_paginate{width:auto;float:right;text-align:right}
.paginate_disabled_previous,.paginate_enabled_previous,.paginate_disabled_next,.paginate_enabled_next{height:19px;width:19px;margin-left:3px;float:left}
.paginate_enabled_previous,.paginate_enabled_next{cursor:pointer}
.dataTables_info{float:left}
.paginate_disabled_previous{background-image:url(../images/admin/back_disabled.jpg)}
.paginate_enabled_previous{background-image:url(../images/admin/back_enabled.jpg)}
.paginate_disabled_next{background-image:url(../images/admin/forward_disabled.jpg)}
.paginate_enabled_next{background-image:url(../images/admin/forward_enabled.jpg)}
icon-btn{background-image:url(../images/admin/basic-sprite.png); width: 24px; height: 24px; display: inline-block; position: relative;}
icon-btn span{ padding-left: 24px; }
icon-btn.transact-btn{ background-position: -597px -190px; cursor:pointer; }
icon-btn.save-btn{ background-position: -179px -233px; cursor:pointer; }
div.save-btn{ margin-right: 5px; width: 80px; margin-top: -8px; pointer:cursor;}
div.save-btn span {
	width: 30px;
	height: 20px;
	display: inline-block;
	position: absolute;
	margin-top: 5px;
	margin-left: 5px; 
}
div.transact-btn{ margin-right: 5px; width: 80px; pointer:cursor;}
div.transact-btn span {
	width: 30px;
	height: 20px;
	display: inline-block;
	position: absolute;
	margin-top: 5px;
	margin-left: 5px; 
}

</style>
{/literal}