<input type="hidden" id="total_users" value="{$total}"/>
<div class="dp100">
    <div class="box_c">
           <h3 class="box_c_heading cf"><span class="fl spacer-left"> {$lang.newsletter} &nbsp;  </span></h3> 
			<div class="box_c_content cf" > 
				  	<div id="ShowSupport" data-id="70" class="LoadBox"></div>
			</div>			
    </div>  
</div>
<div id="OpenSupport" class="hidden formEl_a">
<fieldset>
        <legend>User Changes</legend>    
        <div class="sepH_b  float-left dp100" id="ShowChanges">
        
        </div>
</fieldset>
</div>
<div id="FilterSupport" class="hidden formEl_a">
<form name="requestForm"  id="requestForm">
<fieldset>
        <legend>User Filter</legend>      
        <div class="sepH_b  float-left dp50">
       		<label for="id" class="lbl_a">User ID:</label>
        	<input name="uname" type="text" value="" data-table="users" data-mode="like" class="filterdata inpt_a">
        </div>
        <div class="sepH_b float-left dp50" >
        <label for="available" class="lbl_a">Support Title : </label>
        	<select name="title" id="ftitle"  data-table="users_tickets" data-mode="eq" class="filterdata inpt_d" data-placeholder="Select one">
        		<option value="" selected>&nbsp;</option>
        		<option value="Hotel Subscription" >Hotel Subscription</option>
        		<option value="Finance" >Finance</option>
        		<option value="Reservations" >Reservations</option>
        		<option value="Special Offers" >Special Offers</option>
        		<option value="Advertisement" >Advertisement</option>
        		<option value="PayPal" >PayPal</option>
        		<option value="Technical Support" >Technical Support</option>
        		<option value="Change Password">Change Password</option>
        		<option value="Other">Other</option>
      		</select> 
        </div>
        <div class="sepH_b float-left dp50" >
        <label for="available" class="lbl_a">Support Status : </label>
        	<select name="status" id="fstatus"  data-table="users_tickets" data-mode="eq" class="filterdata inpt_d" data-placeholder="Select one">
        		<option value="" selected>&nbsp;</option>
        		<option value="1" >Unread</option>
        		<option value="2">Read</option>
      		</select> 
        </div>
        <div class="sepH_b float-left dp50" >
        <label for="available" class="lbl_a">Support State : </label>
        	<select name="state" id="fstate"  data-table="users_tickets" data-mode="eq" class="filterdata inpt_d" data-placeholder="Select one">
        		<option value="" selected>&nbsp;</option>
        		<option value="1" >Closed</option>
        		<option value="0">Opened</option>
      		</select> 
        </div>
                <div class="sepH_b float-left dp50">
        <label for="available" class="lbl_a">New Tickets: </label>
        	<input type="checkbox" name="new" class="filterdata"/>
        </div>
        <div class="seperator"></div>
         <div class="sepH_b float-left dp50">
        	<label for="date" class="lbl_a float-left">{$lang.subscr_date} : </label>
        	<input type="hidden" name="date-from" data-table="users_request" data-mode="gt" id="joinfrom" class="filterdata" value="" />
        	<input type="hidden" name="date-to" data-table="users_request" data-mode="lt" id="jointo" class="filterdata" value="" />
        	<div class="calendar float-left spacer-right clear" data-trigger="joinfrom"></div>	
     	  	<div class="calendar float-left" data-trigger="jointo"></div>
     	  	<div class="clear"></div>
        </div>
        <div class="spacer-right spacer-top clear">
        	<center><input name="submit" type="button"  id="SearchSupports"  class="btn btn_a fSearch" value="{$lang.search}" /></center> 
        </div>
<style scoped>

.k-calendar {
	width:183px;
}
</style>
        
<style scoped="">

.k-calendar {
	width:183px;
}
</style>


          
        </fieldset>
      </form>
</div>

		
		<script id="template-toolbar-PagesSearch" class="template-toolbar-PagesSearch" type="text/kendo-ui-template">
        		<a href="\\#" data-table='all' class="k-button filterTable pointer spacer-right">Clear Filters</a>
	
				<a href="\\#" class="k-button k-filter pointer spacer-right">Filters</a>
		</script>

		
		<script id="id-commands-gridview" class="id-commands-gridview" type="text/kendo-ui-template">
				<a href="\\#" class="k-button pointer ShowSupport" data-admin="{$ID}" data-uid="#= userid #" data-conversationid="#= conversationid #" target="_blank">Go to Message</a>
        </script>
        
        <script id="open-support" class="open-support" type="text/kendo-ui-template">
			
        </script>

{if $plugin_file}
	{include file=$plugin_file}
{/if}
<script type="text/javascript">
	head.js('/scripts/plugins/support/support.js');
	head.js('/scripts/admin2/grid.js');
</script>
