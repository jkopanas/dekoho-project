<div class="dp100">
	<div class="box_c" />
		<div class="padding wrap">
			<h3 class="box_c_heading cf">
				Advance Search
			</h3>
			<div class="debug"></div>
			<div class="k-toolbar k-grid-toolbar  dp100">
				<a class="k-button k-button-icontext" id="resetcruiseBtn" ><span class="k-icon "></span>Reset</a>
		    </div>
			<div class=" dp100" id="resultsList">
			</div>
			<div class="k-pager-wrap dp100">
		        <div id="pager"></div>
		    </div>
		</div><!-- END WRAP -->
	</div>
</div>

{if count($jsIncludes) gt 0}
	{foreach from=$jsIncludes key=k item=v}
		<script type="text/javascript" src="{$v}"></script>
	{/foreach}
{/if}

{literal}
<script type="text/x-kendo-tmpl" id="listTemplate">
	<div class="list-box-view list-item-${id}">
		<span class="hidden" id="cruise_id">${id}</span>
		 <div  id="left-pm-details" >
			<span id="cruise_id">${id}</span>
			<span id="cruise_key">${key}</span>
			<span id="cruise_shipID">${shipID}</span>
			<span id="cruise_shipKey">${shipKey}</span>
			<span id="cruise_shipName">${shipName}</span>
			<span id="cruise_title">${title}</span>
			<span id="cruise_description">${description}</span>
			<span id="cruise_sailingPortID">${sailingPortID}</span>
			<span id="cruise_sailingPortKey">${sailingPortKey}</span>
			<span id="cruise_sailingDateTime">${sailingDateTime}</span>
			<span id="cruise_returnDateTime">${returnDateTime}</span>
			<span id="cruise_sailingDays">${sailingDays}</span>
			<span id="cruise_sailingNights">${sailingNights}</span>
			<span id="cruise_checkinTime">${checkinTime}</span>
			<span id="cruise_boardingTime">${boardingTime}</span>
			<span id="cruise_brandID">${brandID}</span>
			<span id="cruise_brandKey">${brandKey}</span>
			<span id="cruise_regionID">${regionID}</span>
			<span id="cruise_regionKey">${regionKey}</span>
			<span id="cruise_programID">${programID}</span>
			<span id="cruise_programKey">${programKey}</span>
			<span id="cruise_themeID">${themeID}</span>
			<span id="cruise_themeKey">${themeKey}</span>
			<span id="cruise_isB2B">${isB2B}</span>
			<span id="cruise_active">${active}</span>
			<span id="cruise_settings">${settings}</span>
			<span id="cruise_permaling">${permaling}</span>
			<span id="cruise_date_added">${date_added}</span>
			<span id="cruise_date_modified">${date_modified}</span>
			<span id="cruise_comments_count">${comments_count}</span>
			<span id="cruise_orderby">${orderby}</span>
			<span id="cruise_programName">${programName}</span>
			<span id="cruise_regionName">${regionName}</span>
			<span id="cruise_brandName">${brandName}</span>
			<span id="cruise_portName">${portName}</span>
			<span id="cruise_portCC">${portCC}</span>
			<span id="cruise_portCN">${portCN}</span>
			<span id="cruise_marketpricesKey">${marketpricesKey}</span>
			<span id="cruise_marketID">${marketID}</span>
			<span id="cruise_marketKey">${marketKey}</span>
			<span id="cruise_marketpricesMin">${marketpricesMin}</span>
			<span id="cruise_marketpricesMax">${marketpricesMax}</span>
			<span id="cruise_marketpricesPromoMin">${marketpricesPromoMin}</span>
			<span id="cruise_marketpricesPromoMax">${marketpricesPromoMax}</span>
			<span id="cruise_availabilityTag">${availabilityTag}</span>
			<span id="cruise_cabinsArr">${cabinsArr}</span>
			<span id="cruise_surchargesArr">${surchargesArr}</span>
			<span id="cruise_componentsArr">${componentsArr}</span>
		 </div>
    	<div class="clear" id="right-pm-details" >
    		
    	</div>
	</div>
</script>
{/literal}

<style scoped>
	.list-box-view
	{
	    float: left;
	    width: 250px;
		height: 200px;
	    margin: 10px;
	    padding: 3px;
	    -moz-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    -webkit-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    box-shadow: inner 0 0 50px rgba(0,0,0,0.1);
	    border-top: 1px solid rgba(0,0,0,0.1);
	    -webkit-border-radius: 8px;
	    -moz-border-radius: 8px;
	    border-radius: 8px;
	}
	#right-pm-details, #left-pm-details
	{
		margin: 10px;
	    padding: 3px;
		margin: 10px 0;
	}
</style>


<script>
head.js('../scripts/plugins/advancedSearch/advancedSearch.js');
</script>
