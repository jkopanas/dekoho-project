<!DOCTYPE html>
<html xmlns:ng="http://angularjs.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dekoho | The Hotel Finder</title>


<link href="/scripts/site/plugins/jquery.customSelect.css" rel="stylesheet" type="text/css" media="all"/>
<link href="/css/site/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href="/scripts/jquery-ui-1.10.0.custom.min.css" rel="stylesheet" type="text/css" media="all"/>

   <!--[if lte IE 8]>
   <script src="/scripts/site/app/json3.min.js"></script>
      <script>
        document.createElement('ng-include');
        document.createElement('ng-pluralize');
        document.createElement('ng-view');
 
        // Optionally these for CSS
        document.createElement('ng:include');
        document.createElement('ng:pluralize');
        document.createElement('ng:view');
      </script>
    <![endif]-->
<script type="text/javascript" src="/scripts/jquery-1.8.3.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&language=en"></script>
<script src="/scripts/site/app/markerclusterer.js"></script>
<script type="text/javascript" src="/scripts/angular.min.js"></script>
<script type="text/javascript" src="/scripts/jquery-ui-1.10.0.custom.min.js"></script>
<script type="text/javascript" src="/scripts/site/app/app.js"></script>
<script src="/scripts/site/app/gMaps.js"></script>
<script src="/scripts/site/app/controllers.js"></script>
<script src="/scripts/site/app/directives.js"></script>
<script src="/scripts/site/app/filters.js"></script>
<script src="/scripts/site/app/services.js"></script>
<script src="/scripts/angular-resource.min.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/site/plugins/jquery.uniform.min.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/site/plugins/jquery.customSelect.min.js"></script>
<script src="/scripts/site/jquery.infinitescroll.min.js"></script>

</head>

<body ng-controller="mainCtrl" id="ng-app" ng-app="mcms">
<input type="hidden" value="{$FRONT_LANG}" name="loaded_language" />
<div id="wrapper">
	

{include file="top_wrapper.tpl"}


{include file=$include_file}


</div><!-- end wrapper -->



</body>
</html>
