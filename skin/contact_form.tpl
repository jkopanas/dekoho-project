<form name="requestForm" id="requestForm" class="formEl_a">
<div class="form-list">
<div class="fieldset">
<h2 class="legend">{$lang.contact_us}</h2>
<div class="spacer-bottom">
{$lang[$CONTACT_FORM_ADDRESS]}
<div class="seperator"></div>
{$lang[$CONTACT_FORM_COMMENTS]}
</div>


<table width="100%" border="0" cellpadding="5" cellspacing="5" id="contactForm">
{foreach from=$fields item=a}
<tr>
<td>{$lang[$a.translations.title]}</td>
<td>
{if $a.fieldType eq "text"}<input type="text" name="{$a.fieldName}" {if $a.required}required validationMessage="{$lang[$a.translations.errorMessage]}"{/if} class="contactField inpt_a" />
{elseif $a.fieldType eq "email"}<input type="email" name="{$a.fieldName}" {if $a.required}required validationMessage="{$lang[$a.translations.errorMessage]}" required data-email-msg="{$lang.error_valid_email}"{/if} class="contactField inpt_a" />
{elseif $a.fieldType eq "area"}<textarea name="{$a.fieldName}" {if $a.required}required validationMessage="{$lang[$a.translations.errorMessage]}"{/if} class="contactField inpt_a"></textarea>
{/if}
</td>
</tr>
{/foreach}
</table>
    <div class="buttons-set">
        <p class="required">* Required Fields</p>

        <button type="submit" title="Submit" class="button submitContact"><span><span>{$lang.send}</span></span></button>
    </div>
    
    </div>
    </div>
</form>

<div class="spacer-top"><div id="contactMap" class="contactMap" style="width:100%; height:350px;"></div> </div>
<div class="clear"></div>
<input type="hidden" name="geocoderAddress" value="{$item.geocoderAddress}" class="get" />
<input type="hidden" name="lat" class="get" value="{$item.lat}" />
<input type="hidden" name="lng" class="get" value="{$item.lng}" />
<input type="hidden" name="zoomLevel" class="get" value="{$item.zoomLevel}" />
<input type="hidden" name="MapTypeId" class="get" value="{$item.MapTypeId}" />
<textarea name="mapItemEncoded" id="mapItemEncoded" class="hidden">{$mapItem}</textarea>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> 
<script>
head.js("/scripts/site/maps/gMaps3.js");
head.js('/scripts/site/contactForm.js');
</script>