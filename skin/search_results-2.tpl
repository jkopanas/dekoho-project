<div ng-controller="searchCtrl">
   <div id="map-wrapper" > 

	    <div class="content">
		    <div id="search">
		    <ul class="search-menu float-left">
			    	<li class="search-item"><a href="#">Hotel Finder</a></li>
			    	<li class="search-item"><a href="#">Special Offers</a></li>
			    	<li class="search-item"><a href="#">Hotel Roulette</a></li>
			    </ul>
			    <div class="seperator"></div>
			    <div id="map-filters">
				    <div class="map-filter-item">
				    <span>Destination</span><div class="seperator"></div>
				    <input type="text" name="destination" id="destination" class="grey SearchText" placeholder="city, region" ng-model="filters.destination" />
				    </div>
				    <div class="map-filter-item">
				    <span>Arrival</span><div class="seperator"></div>
				    <input type="text" name="destination" id="destination" class="grey SearchText-small" ng-model="filters.arrival" />
				    </div>
				    <div class="map-filter-item">
				    <span>Departure</span><div class="seperator"></div>
				    <input type="text" name="destination" id="destination" class="grey SearchText-small" ng-model="filters.departure" />
				    </div>
				     <div class="map-filter-item">
				    <span>Rooms</span><div class="seperator"></div>
				    <select id="rooms" name="rooms" class="grey selectrooms-persons" ng-model="filters.rooms">
				     <option value="1">1 Room</option>
				     <option value="2">2 Rooms</option>
				     <option value="3">3 Rooms</option>
				     <option value="4">4 Rooms</option>
				     <option value="5">5 Rooms</option>
				     <option value="6">6 Rooms</option>
				     <option value="7">7 Rooms</option>
				    </select>
				    </div>
				    <div class="map-filter-item">
				    <span>Persons</span><div class="seperator"></div>
				    <select name="persons" id="persons" class="grey selectrooms-persons " ng-model="filters.persons" >
				     <option value="1">1 Person</option>
				     <option value="2">2 Persons</option>
				     <option value="3">3 Persons</option>
				     <option value="4">4 Persons</option>
				     <option value="5">5 Persons</option>
				     <option value="6">6 Persons</option>
				     <option value="7">7 Persons</option>
				    </select>
				    </div>
				     <div class="map-filter-item">
				    <span>Budget</span><div class="seperator"></div>
				    <input type="text" name="destination" id="destination" class="grey SearchText" ng-model="filters.budget" />
				    </div>
				    <div class="more-criteria">
				    <span><img src="/images/site/more-criteria.jpg" alt="" title=""/><a href="#">More search criteria</a></span>
				    </div>
				    <a href="#"><img src="/images/site/map-button.jpg" alt="" title=""/></a>
				    <div class="seperator"></div><div class="seperator"></div>
			    </div>
		    </div>
		 <!--   <div class="float-left"><img src="/images/site/map.jpg" class="float-left" height="400" width="708"/></div> -->
	   </div> <!-- end map-content -->
	   <div class="seperator"></div>
   </div> <!-- end map-wrapper -->
   

  <div id="page-wrapper">
    <div class="content">
  
  <div class="left-column-inner">
     
     <div id="search-filters">
	     <img src="/images/site/search-header.jpg" alt="" title="" width="255" />
	     <div class="seperator"></div>
	     

	     
	     <div class="filter-header">
	    	 <img src="/images/site/type.jpg" alt="" title="" class="filter-img"/><span>Type of hotel</span>
	     </div>
	     {literal}
		<select  ng-options="i.id as i.title for i in fields.types" ng-model="filters.types" multiple multi-select watchfor="fields.types">
        
        </select>
	     <ul class="filter">
         <li ng-repeat="a in fields.types"><label><input type="checkbox" value="{{a.id}}" replace-input ng-checked="isChecked(a,'types')" ng-click="chkModel(a.id,'types')" /> {{a.title}}</label></li>
	     </ul>
         
     	
     <div class="filter-header">
    	 <img src="/images/site/facilities.jpg" alt="" title="" class="filter-img"/><span>Hotel facilities</span>
     </div>
     	
     	     <ul class="filter">
         <li ng-repeat="a in fields.facilities"><label><input type="checkbox" value="{{a.id}}" replace-input ng-checked="isChecked(a,'facilities')" ng-click="chkModel(a.id,'facilities')" /> {{a.title}}</label></li>
	     </ul>
     	
     	<div class="filter-header">
     		<img src="/images/site/room-facilities.jpg" alt="" title="" class="filter-img"/><span>Room facilities</span>
     </div>
     	
     	<div class="filter">
		     <input type="checkbox" id="bathroom" name="bathroom" class="filter-item" />
		     <label for="bathroom"><span></span>Bathrooom</label>           
	     </div>
     	<div class="filter">
		     <input type="checkbox" id="cable" name="cable" class="filter-item" />
		     <label for="cable"><span></span>CableTV</label>          
	     </div>
     	
     	
     	<div class="filter-header">
	    	 <img src="/images/site/services.jpg" alt="" title="" class="filter-img"/><span>Hotel services</span>
	     </div>
     	     <ul class="filter">
         <li ng-repeat="a in fields.services"><label><input type="checkbox" value="{{a.id}}" replace-input ng-checked="isChecked(a,'services')" ng-click="chkModel(a.id,'services')" /> {{a.title}}</label></li>
	     </ul>
     	{/literal}
     	<div class="seperator"></div>
     </div>
     

		</div><!-- end left-column-inner -->
	
		  <div class="right-column-inner">
			{literal}
                  ---  {{filters}}---
			<div id="hotel-list">
			  <div class="finder"><h5>Finder Results</h5></div>
			  <div class="result-icons float-right">The icons</div>
			  <br class="clear_0"/>

			  <div class="hotel-rslt" ng-repeat="a in items">
				  <img src="/images/site/hotel-img.jpg" class="float-left spacer-right" alt="" title=""/>
				  <div class="finder-info spacer-left float-left">
				  <span class="finder-title">{{a.title}}</span>
				  <span class="finder-stars">(stars)</span>
				  <a href="#">view</a>
				  <br class="clear_0"/>
				  <span class="reserv-info">2 double rooms, 1 single room, 5 Nights, 20 Januray - 25 January 2013 , 500&euro;</span>
				  </div>
				  <div class="float-right reserv">
				  <a href="#"><img src="/images/site/reservation.jpg" alt="" title="" class="spacer-right"/></a>
				  <br class="clear_0"/>
				  <input type="checkbox" id="nearby" name="nearby" class="filter-item" />
		           <label for="nearby"><span></span>Hotels Nearby</label>
				  </div>
				  <br class="clear_0"/>
			  </div>
			  
			  
			 {/literal}
			  
			</div><!-- end hotel-list -->
			
			
		  </div><!-- end right-column-inner -->
	
  <div class="seperator"></div>
  </div> <!-- end page-content --> 
  
  </div> <!-- end page-wrapper -->

</div>