<form action="" class="formEl" id="form_a" style="width:740px;">
							<div class="cf sepH_c">
								<div id="content_actions" class="fr">
									<div  id="quickeditform" class="btn btn_d">{$lang.save}</div>
								</div>
								<h1 class="fl clear">#{$item.dealid} | {$item.dealname}</h1>
								<input type="hidden" name="id" id="dealid" value="{$item.dealid}" />
								<input type="hidden" name="dealtype" id="dealtype" value="{$item.type}" />
							</div>
							
							<ul class="tabsB cf">
								<li><a href="#tab-1">{$lang.basicInformation}</a></li>
								<li><a href="#tab-2" id="m">Product Details</a></li>
								<li><a href="#tab-3" id="mapi" class="tab">Deal Cover Area</a></li>
							</ul>
							
							<div class="msg_box msg_error" id="form_a_errors" style="display:none"></div>
							
							<div class="content_panes">		
								<div id="tab-1">	
									<div class="formEl_a">			
										<fieldset>		
											<div class="sepH_b">
												<label for="title" class="lbl_a">Title</label>
												<input type="text" id="title" name="name" class="inpt_a tab1" value="{$item.name}" />
												<span class="f_help">Here you can enter a headline.</span>
											</div>												
											<div class="sepH_c cf">
												<label for="text" class="lbl_a">Description</label>
												<textarea name="description" id="text" cols="30" rows="10" class="tinymce txtar_a tab1">{$item.description}</textarea>
											</div>									
											<div class="sepH_c cf">
												<div style="float:left;"><label for="text" class="lbl_a">Category: </label></div>
												<div style="float:left;padding-left:5px;" id="showcat"><span class="lbl_a" style="font-weight: normal; float:left;">{$category}</span><img src="{$URL}/skin/images/admin2/icons/pencil.png" id="{$lastid}"  class="editcat" style="float:left;padding-left: 15px;"  /></div>
												<br/><div id="regPro" class="inpt_a" style="display:none;overflow:auto;">
            										<br style="clear:both;">
            										<div style="position:relative;float:left; width:100%;">
            												<div class="effect DivRow" id="procat" style="width:100%;height: 170px;overflow:auto; float:left;"></div>
													</div>
													<br style="clear:both;"/>
													<div id="menu" style="float:left;padding-left:5px;" >{$category_span}</div>
													<input type="hidden" name="category_id" id="catpro" value="{$lastid}" class="tab1" />
													<br/>
												</div>	
											</div>
										</fieldset>
									</div>
								</div>
								<div id="tab-2">
									<div class="formEl_a">			
										<fieldset>		
												<div class="sepH_b">
													<div class="sepH_c cf">
														<label for="text" class="lbl_a">Price</label>
														<input type="text" id="price" name="price" class="inpt_a tab2" value="{$item.price}" />
														<label for="text" class="lbl_a">expire</label>
														<input type="text" id="expiredate" name="expiredate" class="inpt_a" value="{$item.date}" />
														<input type="hidden" name="expire" id="expire" class="tab2" value="{$item.expire}" />
														<label for="text" class="lbl_a">quantity</label> 	 	 	
														<input type="text" id="title" name="quantity" class="inpt_a tab2" value="{$item.quantity}" />
														<label for="text" class="lbl_a">Post Code</label> 	 	 	
														<input type="text" id="post_code" name="post_code" class="inpt_a tab2" value="{$item.post_code}" />
														<label for="text" class="lbl_a">Product url</label> 	 	 	
														<input type="text" id="url" name="url" class="inpt_a tab2" value="{$item.url}" />
													</div>
												</div>	
										</fieldset>
									</div>
								</div>	
								<div id="tab-3">
									<div class="formEl_a">			
										<fieldset>		
												<div class="sepH_b">
													<div class="sepH_c cf">
														
														<div id="panel">
<div id="color-palette"></div>
<div>
<br>
<div id="delete-button">Delete Selected Area</div>
</div>

</div>
<!--  -->
<input type="hidden" name="mapItem" value='' />

<label style="float:left;">address : <input name="address" id="mapaddress" type="text" value=""  class="" size="45" /> </label> <div id="lookupdeal" name="lookupdeal" style=" padding-left:10px;float:left;">Look Up</div>
<div id="map_1" class="mapbox" style="height:300px; width:550px;" ></div>
														</div>
													</div>
										</fieldset>
									</div>
								</div>	
							</div>
							
							{foreach from=$map_item key=i item=value}
									<input type="hidden" class="tab3" id="{$i}" name="{$i}" value='{$value}' />
							{/foreach}
							
							
						

</form>

<link href="/css/galoo/map.css"	rel="stylesheet" type="text/css">
<script>head.js("../scripts/forms/jquery.ui.core.js");</script>
<script>head.js("../scripts/forms/jquery.ui.widget.js");</script>
<script>head.js("../scripts/forms/jquery.ui.datepicker.js");</script>
<script>head.js("../scripts/admin2/dealsEditHome.js");</script>

<script>
$(document).ready(function () {
	$("#mapi").live('click',function() {
		$.getScript("/scripts/maps/MarkerClusterer.js");
		$.getScript("https://maps.google.com/maps/api/js?sensor=false&libraries=drawing&callback=MapApiLoaded");
	});
});
</script>