<form action="" id="itemGeneralForm" method="post" class="toValidate">
{if $quickEdit}
<input type="hidden" name="quickEdit" id="quickEdit" value="{$quickEdit}" />
<div class="debug spacer-bottom"></div>
{/if}
<input type="hidden" name="itemID" value="{$item.id}" /> 
<div id="window"></div>
<div id="content_actions" class="fr"><input type="submit" class="btn btn_d saveForm" value="{$lang.save}"></div>
<div class="clear"></div>
<div class="dp75">
<div class="formEl_a">
<div class="padding">
<div class="sepH_b">
    <label for="title" class="lbl_a">Hotel Name</label>
    <input type="text" id="title" name="title" class="inpt_a toSave"   value='{$item.title}' required  />
    <span class="f_help"></span>
</div>
<div class="sepH_b">
    <label for="permalink" class="lbl_a">{$lang.permalink}</label>
    <input type="text" id="permalink" name="permalink" class="inpt_a toSave" value="{$item.permalink}" />
</div>
<div class="sepH_b">
    <label for="title" class="lbl_a">Hotel Chain / alliance</label>
    <input type="text" id="hotel_chain" name="settings_chain" class="inpt_a toSave"   value='{$item.settings.chain}' placeholder="optional"  />
    <span class="f_help"></span>
</div>
<div class="sepH_b">
    <label for="title" class="lbl_a">Stars</label>
   <select name="stars" class="{if !$quickEdit}mSelect{/if} inpt_a toSave" id="stars" data-placeholder="Select one" data-table="stars" style="width:200px;">
	<option value="1" {if $item.stars eq 1} selected="selected" {/if}>1 star hotel</option>
	<option value="2" {if $item.stars eq 2} selected="selected" {/if}>2 star hotel</option>
	<option value="3" {if $item.stars eq 3} selected="selected" {/if}>3 star hotel</option>
	<option value="4" {if $item.stars eq 4} selected="selected" {/if}>4 star hotel</option>
	<option value="5" {if $item.stars eq 5} selected="selected" {/if}>5 star hotel</option>
	</select>
</div>
<div class="sepH_b">
    <label for="title" class="lbl_a">Total rooms</label>
    <input type="text" id="totalrooms" name="settings_totalRooms" class="inpt_a toSave"   value='{$item.settings.totalRooms}' style="width:100px;"  />
    <span class="f_help">Total hotel rooms with shower or bathroom/WC</span>
</div>


<div class="sepH_b cf" style="display:none;">
    <label for="country_multiple" class="lbl_a">{$lang.main_cat}</label>
    <select name="categoryid" class="{if !$quickEdit}mSelect{/if} inpt_a toSave" id="categoryid" data-placeholder="Select one" data-table="categories">
{foreach from=$allcategories item=a}
<option value="{$a.categoryid}"{if $simple_categories|is_array AND  $a.categoryid|array_key_exists:$simple_categories AND $simple_categories[$a.categoryid] eq 1} selected="selected"{/if}>{$a.category}</option>
{/foreach}
</select>
</div>


<div class="sepH_b cf">
    <label for="country_multiple" class="lbl_a">Type Of Hotel</label>
    <select name="types" multiple="MULTIPLE" class="mSelect inpt_a toSave" id="types" data-placeholder="select all that apply" data-table="hotel_types">
{foreach from=$types item=a}
<option value="{$a.id}"{if $hotel_types|is_array AND  $a.id|array_key_exists:$hotel_types} selected{/if}>{$a.title}</option>
{/foreach}
</select>
</div>

<div class="sepH_b cf">
    <label for="country_multiple" class="lbl_a">Theme </label>
    <select name="themeshotels" multiple="MULTIPLE" class="mSelect inpt_a toSave" id="themes" data-placeholder="select all that apply" data-table="hotel_ themeshotels">
{foreach from=$themes item=a}
<option value="{$a.id}"{if $hotel_themes|is_array AND  $a.id|array_key_exists:$hotel_themes} selected{/if}>{$a.title}</option>
{/foreach}
</select>
</div>

<div class="sepH_b cf">
    <label for="country_multiple" class="lbl_a">Hotel Facilities</label>
    <select name="facilities" multiple="MULTIPLE" class="mSelect inpt_a toSave" id="facilities" data-placeholder="select all that apply" data-table="hotel_facilities">
{foreach from=$facilities item=a}
<option value="{$a.id}"{if $hotel_facilities|is_array AND  $a.id|array_key_exists:$hotel_facilities} selected{/if}>{$a.title}</option>
{/foreach}
</select>
</div>

<div class="sepH_b cf">
    <label for="country_multiple" class="lbl_a">Hotel Services</label>
    <select name="services" multiple="MULTIPLE" class="mSelect inpt_a toSave" id="services" data-placeholder="select all that apply" data-table="hotel_services">
{foreach from=$services item=a}
<option value="{$a.id}"{if $hotel_services|is_array AND  $a.id|array_key_exists:$hotel_services} selected{/if}>{$a.title}</option>
{/foreach}
</select>
</div>


<div class="sepH_b cf">
    <label for="country_multiple" class="lbl_a">Hotel Room Facilities</label>
    <select name="roomsfacilities" multiple="MULTIPLE" class="mSelect inpt_a toSave" id="roomsfacilities" data-placeholder="select all that apply" data-table="hotel_facilities">
{foreach from=$facilities item=a}
<option value="{$a.id}"{if $hotel_facilities|is_array AND  $a.id|array_key_exists:$hotel_roomfacilities} selected{/if}>{$a.title}</option>
{/foreach}
</select>
</div>


<div class="sepH_c cf">
    <label for="description" class="lbl_a">Hotel Description</label>
    <textarea name="description" id="description" cols="30" rows="5" class=" txtar_a toSave">{$item.description}</textarea><br />
    <a href="#" class="ckEditor" rel="description" data-height="150">{$lang.advanced_editor}</a>
</div>

<div class="sepH_b">
    <label for="title" class="lbl_a">Pets allowed</label>
   <select name="pets" class="{if !$quickEdit}mSelect{/if} inpt_a toSave" id="pets" data-placeholder="Select one" data-table="" style="width:200px;">
	<option value="0" {if $item.pets eq 0} selected="selected" {/if}>No</option>
	<option value="1" {if $item.pets eq 1} selected="selected" {/if}>Yes</option>
	</select>
</div>


<div class="sepH_b">
    <label for="title" class="lbl_a">Hotel's Telephone </label>
    <input type="text" id="hoteltelephone" name="settings_hoteltelephone" class="inpt_a toSave"   value='{$item.settings.hoteltelephone}'  />
    <span class="f_help">Hotel telephone</span>
</div>

<div class="sepH_b">
    <label for="title" class="lbl_a">Hotel's E-mail </label>
    <input type="text" id="hotelmail" name="settings_hotelmail" class="inpt_a toSave"   value='{$item.settings.hotelmail}'   />
    <span class="f_help">Hotel email</span>
</div>

<div class="sepH_b">
    <label for="title" class="lbl_a">Hotel's Fax</label>
    <input type="text" id="hotelfax" name="settings_hotelfax" class="inpt_a toSave"   value='{$item.settings.hotelfax}' />
    <span class="f_help">Hotel Fax</span>
</div>

 <div class="sepH_b">
    <label for="title" class="lbl_a">Hotel's PayPal</label>
    <input type="text" id="paypalmail" name="settings_paypalmail" class="inpt_a toSave"   value='{$item.settings.paypalmail}' />
    <span class="f_help">Hotel PayPal</span>
</div>

</div>
</div><!-- END PADDING -->
</div><!-- END LEFT -->
<div class="dp25 spacer-top">
<div id="rightForm">
<div class="formEl_a">


<div class="sepH_c cf">
<label class="lbl_a">{$lang.availability}</label>
<select name="active" id="active" class="inpt_a toSave">
        <option value="0" {if $item.active eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $item.active eq "1"}selected{/if}>{$lang.enabled}</option>
      </select>
</div>

<div class="sepH_c cf">
<label class="lbl_a">Enable</label>
<select name="enable" id="enable" class="inpt_a toSave">
        <option value="0" {if $item.enable eq "0"}selected{/if}>{$lang.disabled}</option>
        <option value="1" {if $item.enable eq "1"}selected{/if}>{$lang.enabled}</option>
      </select>
</div>

</div>

{if $box.settings.includes.eshop AND $current_module.name|in_array:$box.settings.includes.eshop.modules}{include file=$box.settings.includes.eshop.file}{/if}

<script id="newThumbTemplate" type="text/x-kendo-template">
	<img src=" #= src #" /> 
</script>
<div id="itemThumb" {if !$item}class="hidden"{/if}>
<h2>{$lang.thumb}</h2>
<div id="imagePlaceHolder" class="spacer-top">{if $item.image_full.thumb}<img src="{$item.image_full.thumb}" /> {/if}</div>
<div class="clear"></div>
<a href="#" class="btn_c btn uploadWindow" rel="#imagePlaceHolder" data-fileType="thumb" data-imageCategory="0" data-callback="quick_thumb_uploaded">{$lang.add_file}</a>
</div>
<div class="seperator"></div>

{if $pageSettings}
<div class="spacer-top">
<h2>{$lang.settings}</h2>
<div class="spacer-bottom"></div>
{include file="common/drawSettings.tpl" prefix="settings_" classes="toSave inpt_a" pageSettings=$pageSettings val=$item.settings}
</div>
{/if}
</div>
</div>
</div><!-- END RIGHT -->
<div class="clear"></div>
<div id="content_actions" class="fr"><input type="submit" class="btn btn_d saveForm" value="{$lang.save}"></div>
</form>

<script id="tagTemplate" type="text/x-kendo-template">
	<li data-orderby="#= orderby #">#= tag # <a href="\\#" class="deleteTag"><img src="/images/icons/icon-pricetable-false.png" align="absmiddle" /></a></li> 
</script>
<style>
#autoCompleteResults { background:white; z-index:9999; position:absolute; border:1px solid #ebebeb }
#autoCompleteResults li { padding:5px; color:#777777}
#autoCompleteResults li:hover, .hovered { background:#eeeeee;}
#autoCompleteResults  .matched { color:black; font-weight:bold; background:#ffffcc; }
#tagsContainer li { float:left; margin-right:10px; margin-bottom:10px; }
</style>

<script src="/scripts/admin2/hotel/itemsGeneralForm.js"></script>