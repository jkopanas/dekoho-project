<form name="searchform" id="searchform" action="{if !$nav_area}{$SELF}{else}{$URL}/search/{/if}" method="POST">
<div class="formEl_a">
<div class="sepH_b">
    <label for="title" class="lbl_a">#ID</label>
    <INPUT name="id" type=text id="id" class="inpt_a SearchData" data-operator="contains"  />
   </div>
   {if $current_module.name eq "products"}
<div class="sepH_b">
    <label for="sku" class="lbl_a">SKU</label>
    <input type="text" id="sku" name="sku" class="inpt_a SearchData" data-operator="contains" />

</div>
{/if}
<div class="sepH_b">
<label for="categoryid" class="lbl_a">{$lang.category}</label>
    <select name="categoryid" id="categoryid" class="inpt_a mSelect SearchData" style="width:380px">
  <option value="">{$lang.all}</option>
{foreach from=$allcategories item=a}
<option value="{$a.categoryid}" {if $a.categoryid eq $data.categoryid}selected{/if}>{$a.category}</option>                
{/foreach}  
</select>
<span class="f_help"><label><input name="search_in_subcategories" type="checkbox" value="1" class="SearchData" />
    {$lang.search_in_subcategories}</label></span>
</div>
<div class="sepH_b">
<label class="lbl_a" for="availability">{$lang.availability}</label>
<select name="active" id="availability" class="inpt_a SearchData" data-operator="eq">
      <option value="">{$lang.all}</option>
    <option value="1">{$lang.yes}</option>
    <option value="0">{$lang.no}</option>
  </select>
</div>
<div class="sepH_b">
    <label for="substring" class="lbl_a">{$lang.search_phrase}</label>
    <input type="text" name="substring" id="substring" size="30" value="{$data.substring}" class="inpt_a SearchData"  /> 
    <div class="f_help"><span>{$lang.search_in} : </span>
    <label><input name="search_main" type="checkbox" id="search_main" value="1" class="SearchData"  checked />
    {$lang.title}</label>
      <label><input name="search_short" type="checkbox" id="search_short" class="SearchData"  />
      {$lang.description}</label>
      <label><input name="search_long" type="checkbox" id="search_long" value="1" class="SearchData"/>
      {$lang.long_desc}</label>
      </div>
   </div>
<div class="sepH_b">
<label class="lbl_a" for="advanced">{$lang.specialFilters}</label>
<select name="advanced" id="advanced" class="inpt_a SearchData">
   <option value="" selected="selected">{$lang.no_filter}</option>
    {if $CURRENT_MODULE.settings.use_providers}<option value="provider" {if $data.advanced eq "provider"}selected{/if}>{$lang.without_owner}</option>{/if}
   {if $loaded_modules.eshop AND $current_module.name eq "products"}<option value="price" >No price{$lang.without_category}</option>{/if}
    <option value="category" >{$lang.without_category}</option>
    <option value="related" >{$lang.without_related}</option>
    <option value="image" >{$lang.without_image}</option>
  </select>
</div>   
<div class="sepH_b fl spacer-right">
<label class="lbl_a">{$lang.date_from}</label>
<input name="date-from" type="hidden" id="dateFrom" value="" class="SearchData">
<div class="calendar" data-trigger="dateFrom" data-name="dateFrom"></div>
</div>
<div class="sepH_b fl spacer-left">
<label class="lbl_a">{$lang.date_to}</label>
<input name="date-to" type="hidden" id="dateTo" value="" class="SearchData">
<div class="calendar" data-trigger="dateTo" data-name="dataTo"></div>
</div>
<div class="clear"></div>
<div class="padding"> <button class="btn btn_d PerformSearchBtn">{$lang.search}</button> <input type="submit" value="{$lang.search}" class="btn btn_d hidden" id="PerformSearch" name="PerformSearch">
  <input name="search" type="hidden" id="search" value="1">
  </div>
   

</div>
</form>