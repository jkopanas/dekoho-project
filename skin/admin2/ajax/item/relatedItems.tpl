<textarea class="hidden" name="arr">{$arr}</textarea>
<div id="related">
<div class="dp50">
<div class="box_c">
<div class="box_c_heading cf">
    <span class="fl">{$lang.search}</span>
    <span class="fr">
    <select class="SearchData inpt_b" id="module" name="module"  data-field="module">
    {foreach from=$loaded_modules item=a name=b}
    	{if $a.active AND $a.is_item}
<option value="{$a.name}">{$a.name}</option>    
    	{/if}
    {/foreach}
</select>
    </span>
    </div><!-- END HEADING -->
    <div class="filters"></div>
    </div><!-- END BOX -->
</div><!-- END LEFT -->
<div class="dp50">
<ol class="sortable" id="menuItems"> 

</ol>
</div><!-- END RIGHT -->
<div class="clear"></div>
  </div> <!-- END CONTAINER -->
    
    <style>
	.catItem { border:1px solid #DFDFDF; padding:10px; line-height:130%; color:#555; background-color:#f3f3f3; min-height:31px; }
.row-actions { margin-left:25px; }
.categoriesResults ul ol{
margin: 0 0 0 25px;
padding: 0;
list-style-type: none;
} 
		 ol.sortable ol {
			margin: 0 0 0 25px;
			padding: 0;
			list-style-type: none;
		}
 
		ol.sortable {
			/* margin: 4em 0; */
		}
 
		.sortable li {
			margin: 7px 0 0 0;
			padding: 0;
		}
 
		.sortable li div.box_c_heading  {
			padding: 3px;
			margin: 0;
			cursor: move;
		}
		.placeholder {
			background-color: #cfcfcf;
			border:2px dashed #999;
		}
 
		.ui-nestedSortable-error {
			background:#fbe3e4;
			color:#8a1f11;
		}
		.module-1 { background-color:#caedff; }
		.module-2 { background-color:#caffd3; }
		.module-3 { background-color:#fafdb3; }
		.module-4 { background-color:#fbffca; }
		.featuredItemcat { background-color:#f6d8fa; }
		.featuredItemitm { background-color:#c2ffcc; }
		.inUse { color:red; text-decoration:line-through; }
		.inUse input { display:none; }
</style>
{literal}
<div id="templates">
<script id="searchItems" type="text/kendo-ui-template" data-type="text">

<div class="cf mAccordion" id="acc-1">
<div class="micro" id="micro-1">
<h4 class="micro-open"><span class="head-inner">#= $.lang.page #</span></h4>
<div class="sub_section micro-content">
<label for="title" class="lbl_a">#= $.lang.filter #</label>
<input type="hidden" name="search" />   <input type="text" id="filter" name="substring" class="inpt_b filter SearchData" data-module="#= me.options.module #" data-field="q"  /> 
<select class="SearchData inpt_b" id="catid" name="categoryid" data-module="#= me.options.module #" data-field="categoryid">
<option value="">#= $.lang.all #</option>    
# t= me.renderRecursive(obj.categories,0,{template:'catSelectTemplate',encStart:'',encEnd:''}); #
#= t #
</select> 
<input type="hidden" class="SearchData" name="module" value="#= me.options.module #" />
<a href="\\#" class="btn btn_d quickFilter" data-type="items"  data-typeAbbr="item" data-module="#= me.options.module #">#= $.lang.search #</a>
<span class="f_help">search for title, id</span>
<ul style="height:250px; overflow-y:scroll; clear:both;" class="itemsResults">

# t = me.templates['pages']({d:obj,me:me}); #
#= t #
</ul>
<a href="\\#" id="#= new Date().getTime() #" class="btn btn_d addItems fr spacer-top" data-type="items" data-typeAbbr="itm" data-itemid="#= $('input[name=itemID]').val() #"  data-module="#= me.options.module #">#= $.lang.add #</a>
<div class="seperator"></div>
</div><!-- END CONTENT -->
</div><!-- END SECTION -->

<div class="micro" id="micro-2">
<h4 class="micro-open"><span class="head-inner">#= $.lang.category #</span></h4>
<div class="sub_section micro-content hidden">
        <div class="categoriesResults" id="results-{$a.id}">
        <ul style="height:250px; overflow-y:scroll">
		# t= me.renderRecursive(obj.categories,0,{template:'categoriesResults',encStart:'<ol>',encEnd:'</ol>',check:me.itemsFlat.categories}); #
		#= t #
        </ul>
                <div class="seperator"></div>
 <a href="\\#" id="#= new Date().getTime() #" class="btn btn_d addItems fr spacer-top" data-type="categories" data-typeAbbr="cat" data-categoryid="" data-module="#= me.options.module #">#= $.lang.add #</a>
 <div class="seperator"></div>
        </div>
</div><!-- END CONTENT -->
</div><!-- END SECTION -->


</div><!-- END ACCORDION -->
</script><!-- END TEMPLATE -->
<script id="catSelectTemplate" type="text/kendo-ui-template">

<option value="#= d.categoryid #" data-level="#= d.level #" data-parentid="#= d.parentid #" data-categoryid_path="#= d.categoryid_path #" style="margin-left:#= 5*parseInt(d.level) #px"># for (j=0;j < parseInt(d.level);j++) { # -- # } #  #= d.category #</option>
</script>
<script id="pages" type="text/kendo-ui-template">
# if (typeof d.allPages !== 'undefined') { d.pages = d.allPages; }  #
	# if (typeof d.pages != 'undefined') { #

	# var a = d.pages.length; for (j=0;a > j;j++) { # 
	<li# if (typeof me.itemsFlat[me.options.module] !== 'undefined' && typeof me.itemsFlat[me.options.module].itm !== 'undefined' && me.itemsFlat[me.options.module].itm.indexOf(d.pages[j]['id']) > -1) { # class="inUse" #}#><label><input type="checkbox" name="item-#= d.pages[j]['id'] #" id="itm-#= d.pages[j]['id'] #" data-el="itm-#= d.pages[j]['id'] #" value="#= d.pages[j]['id'] #" data-type="itm"  data-id="#= d.pages[j]['id'] #"  data-module="#= me.options.module #" /> <strong>\\##= d.pages[j]['id'] #</strong> <span>#= d.pages[j]['title'] #</span></label></li>
	# } #
	 # } #
</script>
<script id="categoriesResults" type="text/kendo-ui-template">

	<li data-level="#= d.level #" data-id="#= d.categoryid #"  data-parentid="#= d.parentid #" ><label # if (typeof me.itemsFlat[me.options.module] !== 'undefined' && typeof me.itemsFlat[me.options.module].cat !== 'undefined' && me.itemsFlat[me.options.module].cat.indexOf(d.categoryid) > -1) { # class="inUse" # } #><input type="checkbox" name="category-#= d.categoryid #" id="cat-#= d.categoryid #" value="#= d.categoryid #" data-type="cat" data-id="#= d.categoryid #" data-level="#= d.level #" data-el="cat-#= d.categoryid #" data-module="#= me.options.module #" /> <strong>\\##= d.categoryid #</strong> <span>#= d.category #</span></label>
	
			# if (typeof d.subs == 'undefined') { #		
		</li>
		# } #
</script>

<script id="items" type="text/kendo-ui-template">

# if (d != null){#

# var p = d.length; for (i=0;p > i;i++) { #

	# if (d[i].item) { #
		<li id="#=d[i].type#-#= d[i].itemid || d[i].destID #" data-destID="#= d[i].itemid || d[i].destID #" data-destModule="#= d[i]['module'] || d[i].destModule #" data-type="#= d[i].type #" data-sourceID="#= me.options.itemid #" data-sourceModule="#= me.options.moduleSource #">
	
<div class="catItem featuredItem#=d[i].type#" data-id="#= d[i].itemid #" data-type="#= d[i].type #">
<img width="16" height="16" src="/images/admin/move-arrow.png" class="spacer-right fl dragHandle"> <span class="fl spacer-right"><strong>\\##= d[i].itemid || d[i].destID #</strong>  <span class="catName">#= d[i].item.title || d[i].item.category #</span></span>
<a href="\\#" rel="#= d[i].itemid || d[i].destID #" data-module="#= d[i]['module'] || d[i].destModule #" data-type="#= d[i]['type'] #"><img data-id="#= d[i].itemid || d[i].destID #" class="fr wRemove" alt="" src="/skin/images/admin2/delete.png" height="11" style="margin-left:5px;"></a>

<span class="fr"><em><strong>#= d[i].module || d[i].destModule || d[i].module #</strong> / # if (d[i].type === 'itm') { # #= $.lang.item # # } else { # #= $.lang.category # # } #</em></span>
		</div>
		</li>
	# } #
# }  }#
</script>
</div>
<script>
head.js('/scripts/jquery-ui-1.8.18.custom.min.js','/scripts/admin2/nestedSortable.js','/scripts/admin2/items/itemsRelated.js');

head.ready(function(){
var sortObj={},relatedObj={}, sortableOptions = {
			disableNesting: 'no-nest',
			forcePlaceholderSize: true,
			handle: 'img.dragHandle',
			items: 'li',
			opacity: .6,
			placeholder: 'placeholder',
			tabSize: 25,
			tolerance: 'pointer',
			toleranceElement: '> div',
			stop: function(event, ui) {
				relatedObj.updateState(ui);
			}
};
sortObj = $('ol.sortable');
	
	var settings = $.parseJSON($('textarea[name=arr]').val());
	$('#related').itemsRelated({ itemid:$('input[name=itemID]').val(),moduleSource: $('select[name=module]').val(),settings:settings,sortableDefualts:sortableOptions });
	relatedObj = $('#related').data('itemsRelated');
});
</script>
{/literal}