<div class="box_c">
									<div class="box_c_heading cf">
										<span class="fl">{$lang.extra_fields}</span>
										<ul class="tabsS fr">
											<li><a href="#" class="current" rel="edit">{$lang.edit}</a></li>
											<li><a href="#" rel="translate">{$lang.translations}</a></li>
										</ul>
									</div>
									<div class="box_c_content cf tabs_content">
										<div id="edit" class="tab">
<div id="content_actions" class="fr"><button class="btn btn_d saveEfields">{$lang.save}</button></div>
<div class="formEl_a">
{foreach from=$extra_fields item=a name=b}
{if $a.type eq "radio" OR $a.type eq "checkbox"}{assign var="checked" value=1}{else}{assign var="checked" value=0}{/if}
<div class="sepH_b cf ">
{if $a.type eq 'radio'}{assign var=classes value="inpt_r"}{elseif $a.type eq 'checkbox'}{assign var=classes value="inpt_c"}{else}{assign var=classes value="inpt_a"}{/if}
<label for="title" class="lbl_a">{$a.field}</label> {include file="common/formFields/`$a.type`.tpl" field=$a prefix=$prefix classes="`$classes` efields" val=$a.value checked=$checked extra="data-id=\"`$a.fieldid`\""}
{if $a.type eq "textarea"}<a href="#" class="ckEditor" rel="{$prefix}{$field.var_name}" data-height="150">{$lang.advanced_editor}</a>
{/if}
</li>
</div>
{/foreach}
</div>


										</div><!-- END TAB -->
										<div class="hidden tab" id="translate">
                                        
                                        <div class="dp75">
<div id="tab-container" class="clear">
{foreach from=$availableLanguages item=b name=c}
<div id="tab-{$b.code}" class="tabcontent {if !$smarty.foreach.c.first}hidden{/if}" data-code="{$b.code}">
<h3>{$lang.defaultLanguage} : {$defaultLanguage.details.country} - {$lang.NowEditing} {$b.country}</h3>



<div class="formEl_a">
{foreach from=$extra_fields item=a name=b}
{if $a.type eq "text" OR $a.type eq "area"}
<div class="sepH_b cf ">
<label for="title" class="lbl_a">{$a.field}</label> {include file="common/formFields/`$a.type`.tpl" field=$a prefix="" classes="inpt_a efields" val=$b.item[$a.var_name].value.translation checked=$checked extra="data-id=\"`$a.fieldid`\" data-code=\"`$b.code`\" data-field=\"value\""}
{if $a.type eq "textarea"}<a href="#" class="ckEditor" rel="{$prefix}{$field.var_name}" data-height="150">{$lang.advanced_editor}</a>
{/if}
</li>
</div>
{/if}
{/foreach}
</div>
</div><!-- END TAB -->
{/foreach}
</div><!-- END TABS -->
</div><!-- END LEFT -->

<div class="dp25">
<div class="formEl_a">
<div class="wrap spacer-bottom">
<h2>{$lang.available_languages}</h2>
<div class="padding">
<ul id="languageTabs"  class="idTabs">
{foreach from=$availableLanguages item=a name=b}
<li><a href="#tab-{$a.code}" rel="tab-{$a.code}" class="tabber {if $smarty.foreach.b.first}selected{/if}" data-code="{$a.code}">{$a.country}</a></li>
{/foreach}
<li class="spacer-top"><button class="btn btn_d saveEfields" data-code="{$availableLanguages[0].code}">{$lang.save}</button></li>
</ul>
</div><!-- END PADDING -->
</div><!-- END BOX -->


<div class="wrap spacer-bottom hidden defTranslation">
<h2>{$lang.defaultTranslation}</h2>
<div class="sepH_c cf">
<textarea  id="defaultTranslationTextArea" rows="15"></textarea>
</div><!-- END PADDING -->
</div><!-- END BOX -->

</div><!-- END FORM -->
</div><!-- END RIGHT -->
                                        
                                        
										</div><!-- END TAB -->

									</div><!-- END TABS -->
								</div><!-- END BOX -->




<script src="/scripts/admin2/items/itemsExtraFields.js"></script>