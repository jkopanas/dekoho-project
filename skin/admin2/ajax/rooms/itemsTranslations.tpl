<div id="original-content">
<input type="hidden" class="originalData" value="{$item.id}" name="id" />
<input type="hidden" class="originalData" value="{$item.title}" name="title" id="title"  />
<input type="hidden" class="originalData" value="{$item.permalink}" name="permalink" id="permalink" />
<textarea class="originalData hidden" name="description" id="description">{$item.description}</textarea>
<textarea class="originalData hidden" name="description_long" id="description_long">{$item.description_long}</textarea>
</div>

<div class="dp75">
<div class="formEl_a">
<div id="tab-container" class="clear">
{foreach from=$availableLanguages item=a name=b}
<div id="tab{$smarty.foreach.b.iteration}" class="tabcontent {if !$smarty.foreach.b.first}hidden{/if}">
<h3>{$lang.defaultLanguage} : {$defaultLanguage.details.country} - {$lang.NowEditing} {$a.country}</h3>
<div class="seperator"></div>
<div class="sepH_c cf">
<label for="title" class="lbl_a">{$lang.title}</label>
<div class="padding">
<input name="title-{$a.code}" type="text" id="title-{$a.code}"  data-table="{$current_module.name}" data-field="title" data-code="{$a.code}" size="45" value="{$a.item.title.translation}" class="ContentTitle languageTranslationBox inpt_a">
    </div><!-- END PADDING -->
    
    </div><!-- END BOX -->
<div class="sepH_c cf">
 <label for="title" class="lbl_a">{$lang.permalink}</label>
<div class="padding">
<input name="permalink-{$a.code}" data-table="{$current_module.name}" data-field="permalink" data-code="{$a.code}" type="text" id="permalink-{$a.code}" size="45" value="{$a.item.permalink.translation}" class="ContentTitle languageTranslationBox inpt_a">
    </div><!-- END PADDING -->
    
    </div><!-- END BOX -->
<div class="sepH_c cf">
<label for="title" class="lbl_a">{$lang.description}</label>
<div class="padding">
<textarea name="description-{$a.code}" data-field="description" data-code="{$a.code}" data-table="{$current_module.name}" cols="80" rows="5" id="description-{$a.code}" class="languageTranslationBox txtar_a">{$a.item.description.translation}</textarea>
<br /><a href="#" class="ckEditor" rel="description-{$a.code}" data-height="150">{$lang.advanced_editor}</a>
</div><!-- END PAADDING -->
</div><!-- END BOX -->
<div class="sepH_c cf">
<label for="title" class="lbl_a">{$lang.long_desc}</label>
<div class="padding">
<textarea name="description_long-{$a.code}" data-field="description_long" data-code="{$a.code}" data-table="{$current_module.name}" cols="80" rows="10" id="description_long-{$a.code}" class="languageTranslationBox">{$a.item.description_long.translation}</textarea> 
<br /><a href="#" class="ckEditor" rel="description_long-{$a.code}" data-height="300">{$lang.advanced_editor}</a>
</div><!-- END PAADDING -->
</div><!-- END BOX -->


    </div><!-- END TAB --> 
{/foreach}
</div>
</div><!-- END FORM -->
</div><!-- END LEFT -->

<div class="dp25">
<div class="formEl_a">
<div class="wrap spacer-bottom">
<h2>{$lang.available_languages}</h2>
<div class="padding">
<ul id="languageTabs"  class="idTabs">
{foreach from=$availableLanguages item=a name=b}
<li><a href="#tab{$smarty.foreach.b.iteration}" rel="tab{$smarty.foreach.b.iteration}" class="tabber {if $smarty.foreach.b.first}selected{/if}">{$a.country}</a></li>
{/foreach}
<li class="spacer-top"><button class="btn btn_d SaveTranslations">{$lang.save}</button></li>
</ul>
</div><!-- END PADDING -->
</div><!-- END BOX -->


<div class="wrap spacer-bottom">
<h2>{$lang.defaultTranslation}</h2>
<div class="sepH_c cf">
<textarea  id="defaultTranslationTextArea" rows="15"></textarea>
</div><!-- END PADDING -->
</div><!-- END BOX -->

</div><!-- END FORM -->
</div><!-- END RIGHT -->

<div class="seperator"></div>
<script src="/scripts/admin2/items/itemsTranslations.js"></script>