{if $module.name eq "eshop"}
<ul style="display: none; ">
<li><a href="eshop.php#!/index" class="{if $submenu eq "main"}current"{/if}" >{$lang.home}</a></li>
<li><a href="eshop.php#!/orders" class="{if $submenu eq "orders"}current"{/if}" >Orders</a></li>
<li><a href="eshop.php#!/shipping_methods" class="{if $submenu eq "shipping"}current"{/if}" >Shipping Methods</a></li>
<li><a href="eshop.php#!/payment_methods" class="{if $submenu eq "payment"}current"{/if}" >Payment Methods</a></li>
{* 
<li><a href="#!/subscriptions" class="{if $submenu eq "payments"}current"{/if}" >Subscriptions</a></li>
<li><a href="#!/pclasses" class="{if $submenu eq "classes"}current"{/if}" >Product Classes</a></li>
<li><a href="#!/mass_insert" class="{if $submenu eq "memberships"}current"{/if}" >Memberships</a></li>
<li><a href="#!/categories" class="{if $submenu eq "gifts"}current"{/if}" >Gift Ceritficates</a></li>
<li><a href="#!/extra_fields" class="{if $submenu eq "discounts"}current"{/if}" >Discounts</a></li>
<li><a href="#!/settings" class="{if $submenu eq "settings"}current"{/if}" >Settings</a></li> 
*}
</ul>
</li>
{/if}

{if !$custom AND $module.is_item}
<ul>
<li><a href="items.php?module={$module.name}">{$lang.add}</a></li>
<li><a href="itemsCategoriesNew.php?module={$module.name}">{$lang.categories}</a></li>
<li><a href="commonCategoriesTree.php?module={$module.name}">{$lang.dynamc_cat}</a></li>
<li><a href="extra_fields.php?module={$module.name}">{$lang.extra_fields}</a></li>
<li><a href="SettingManager.php?module={$module.name}">{$lang.insert_settings}</a></li>
<li><a href="SettingManager.php?general&module={$module.name}">{$lang.module_configuration}</a></li>
</ul>
</li>
{/if}
