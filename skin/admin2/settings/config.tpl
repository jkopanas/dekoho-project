<div class="dp100">
	<div class="box_c" />
		<div class="padding wrap">
			<h3 class="box_c_heading cf">
				MCMS Configuration Settings
			</h3>
			<div class="debug"></div>
			<div class="dp100" id="resultGrid"> </div>
			<div class="k-pager-wrap dp100">
		        <div id="pager"></div>
		    </div>
		</div><!-- END WRAP -->
	</div>
</div>

{if count($jsIncludes) gt 0}
	{foreach from=$jsIncludes key=k item=v}
		<script type="text/javascript" src="{$v}"></script>
	{/foreach}
{/if}

{literal}
<script type="text/x-kendo-tmpl" id="listTemplate">
	<li class="list-box-view list-item-${id}">
		<img src="/images/admin/move-arrow.png" width="16" height="16" class="handle">
		<span class="textVal" id="configSet_name">${name}</span>
		<span class="textVal" id="configSet_configvalue">${configvalue}</span>
	    <div class="edit-buttons">
	    	<span data-itemid="${id}" class="EditSet k-icon k-edit pointer spacer-right k-edit-button" id="configSetListEditBtn"></span>
	    	<span data-itemid="${id}" class="DeleteSet k-icon k-delete pointer k-delete-button" id="configSetListDeleteBtn"></span>
		</div>
	</li>
</script>
{/literal}

{literal}
<script type="text/x-kendo-tmpl" id="editTemplate">
	<div class="list-box-view list-item-${id}">
		<div class="edit-buttons">
		    <a class="k-button k-button-icontext k-update-button" id="configSetEditSaveBtn"><span class="k-icon k-update"></span>Save</a>
		    <a class="k-button k-button-icontext k-cancel-button" id="configSetEditCancelBtn"><span class="k-icon k-cancel"></span>Cancel</a>
		</div>
		<div id="tabStrip" >
			<ul>
				<li class="k-state-active">Name</li>
			</ul>
			<div >
		        <label>
		        	<span class="configSetName">name</span>
		            <input type="text" name="name" class="configSetValue"  value="${name}" id="configSet-name" required="required" validationMessage="required"/>
		        </label>
		        <label>
		        	<span class="configSetName">category</span>
		            <input type="text" name="category" class="configSetValue" value="${category}" id="configSet-category" required="required" validationMessage="required"/>
		        </label>
		        <label>
		        	<span class="configSetName">Type</span>
		        	<select data-role="combobox" id="configSet-type"  name="type" class="configSetValue" value="${type}" required="required" validationMessage="required" >
		        		<option value="text" >Text/Numeric</option>
		        		<option value="textarea" >Textarea</option>
		        		<option value="checkbox" >Checkbox</option>
		        		<option value="radio" >Radio</option>
		        		<option value="select" >Select</option>
		        		<option value="image" >image</option>
		        	</select>
		        </label>
		        <label>
		        	<span class="configSetName">Value</span>
		        	<input type="text" value="${configvalue}" name="configvalue" class="configSetValue" id="configSet-value" required="required" validationMessage="required"/>
		        </label>
		        <label>
		        	<span class="configSetName">Default Value</span>
		        	<input type="text" value="${defvalue}" name="defvalue" class="configSetValue" id="configSet-value" required="required" validationMessage="required"/>
		        </label>
			</div>
		</div>
	</div>
</script>
{/literal}
<script>
head.js('../scripts/multiSelect/chosen.jquery.min.js');
//head.js("../scripts/mcmsSortable.js");
head.js('../scripts/jquery-ui-1.8.18.custom.min.js');
head.js('../scripts/admin2/settings/mcmsconfigure.js');
</script>

<style scoped>
	.list-box-view
	{
		display: inline-block;
		list-style: none;
	    float: left;
	    width: 100%;
	    margin: 0px;
		height: 30px;
	    -moz-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    -webkit-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    box-shadow: inner 0 0 50px rgba(0,0,0,0.1);
	    border-top: 1px solid rgba(0,0,0,0.1);
	}
	div.list-box-view
	{
		display: inline-block;
	    float: left;
	    width: 100%;
	    margin: 0px;
		height: 275px;
	    -moz-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    -webkit-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    box-shadow: inner 0 0 50px rgba(0,0,0,0.1);
	    border-top: 1px solid rgba(0,0,0,0.1);
	}
	.k-editor
	{
		width: 100% !important;
	}
	.list-box-view.k-edit-item
	{
		height: 265px;
	}
	.list-box-view.ui-sortable-placeholder
	{
		height: 35px !important;
	}
	.list-box-view .textVal
	{
		margin: 5px;
		position: relative;
		display: block;
		float: left;
		width: 230px;
		height: 20px;
		overflow: hidden;
	}
	.list-box-view img
	{
		margin: 5px;
		position: relative;
		display: block;
		float: left;
	}
	.list-box-view .k-button
	{
		margin: -2px 5px;
		position: relative;
		display: block;
		float: left;
	}
	#right-pm-details, #left-pm-details
	{
		margin: 10px;
	    padding: 3px;
		margin: 10px 0;
	}
	.k-listview:after, .product-view dl:after
	{
	    content: ".";
	    display: block;
	    height: 0;
	    clear: both;
	    visibility: hidden;
	}
	.edit-buttons
	{
		text-align: right;
	    padding: 5px;
		height: 50px;
		width: 150px;
		float: right;
		possition: relative;
	
	}
	div.list-box-view .edit-buttons
	{
		width: 182px;
		height: 50px;
		margin-right: -7px;
	}
	.k-toolbar, #resultsList, .k-pager-wrap
	{
	    margin: 0 auto;
	}	
	.k-tabstrip .k-content, .k-panelbar .k-tabstrip .k-content {
		border-style: solid;
    	border-width: 1px;
    	position: static;
    	padding 0.3em 0.3em;
    	margin: 0px;
	}
	 .connected, .sortable, .exclude, .handles {
		   margin: auto;
		   padding: 0;
		   width: 310px;
		   -webkit-touch-callout: none;
		   -webkit-user-select: none;
		   -khtml-user-select: none;
		   -moz-user-select: none;
		   -ms-user-select: none;
		   user-select: none;
		  }
		  .sortable.grid {
		   overflow: hidden;
		  }
		  .connected li, .sortable li, .exclude li, .handles li {
		   list-style: none;
		   border: 1px solid #CCC;
		   background: #F6F6F6;
		   font-family: "Tahoma";
		   color: #1C94C4;
		   margin: 5px;
		   padding: 5px;
		   height: 22px;
		  }
		  .handles span {
		   cursor: move;
		  }
		  li.disabled {
		   opacity: 0.5;
		  }
		  .sortable.grid li {
		   line-height: 80px;
		   float: left;
		   width: 80px;
		   height: 80px;
		   text-align: center;
		  }
		  li.highlight {
		   background: #FEE25F;
		  }
		  #connected {
		   width: 440px;
		   overflow: hidden;
		   margin: auto;
		  }
		  .connected {
		   float: left;
		   width: 200px;
		  }
		  .connected.no2 {
		   float: right;
		  }
		  .mcmsSortable-placeholder {
		   border: 1px dashed #CCC;
		  	margin: 10px;
		  }
</style>