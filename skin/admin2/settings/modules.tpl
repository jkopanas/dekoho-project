<div class="dp100">
	<div class="box_c" />
		<div class="padding wrap">
			<h3 class="box_c_heading cf">
				Modules Settings
			</h3>
			<div class="debug"></div>
			<div class="dp100" id="resultGrid"> </div>
			<div class="k-pager-wrap dp100">
		        <div id="pager"></div>
		    </div>
		</div><!-- END WRAP -->
	</div>
</div>

{if count($jsIncludes) gt 0}
	{foreach from=$jsIncludes key=k item=v}
		<script type="text/javascript" src="{$v}"></script>
	{/foreach}
{/if}

{literal}
<script type="text/x-kendo-tmpl" id="listTemplate">
	<li class="list-box-view list-item-${id}">
		<img src="/images/admin/move-arrow.png" width="16" height="16" class="handle">
		<span class="hidden" id="modset_id">${id}</span>
		<span class="textVal" id="modset_name">${name}</span>
		<span class="textVal" id="modset_value">${value}</span>
	    <div class="edit-buttons">
	    	<span data-itemid="${id}" class="EditSet k-icon k-edit pointer spacer-right k-edit-button" id="modSetListEditBtn"></span>
	    	<span data-itemid="${id}" class="DeleteSet k-icon k-delete pointer k-delete-button" id="modSetListDeleteBtn"></span>
		</div>
	</li>
</script>
{/literal}

{literal}
<script type="text/x-kendo-tmpl" id="editTemplate">
	<div class="list-box-view list-item-${id}">
		<div class="edit-buttons">
		    <a class="k-button k-button-icontext k-update-button" id="modSetEditSaveBtn"><span class="k-icon k-update"></span>Save</a>
		    <a class="k-button k-button-icontext k-cancel-button" id="modSetEditCancelBtn"><span class="k-icon k-cancel"></span>Cancel</a>
		</div>
		<div id="tabStrip" >
			<ul>
				<li class="k-state-active">Name</li>
				<li>Value</li>
				<li>Comments</li>
			</ul>
			<div >
				<label>
		        	<span class="modSetName">ID</span>
		        	<span class="modSetName">${id}</span>
		            <input type="hidden" name="id" class="modSetValue"  value="${id}" id="modSet-name" required="required" validationMessage="required"/>
		        </label>
		        <label>
		        	<span class="modSetName">name</span>
		            <input type="text" name="name" class="modSetValue"  value="${name}" id="modSet-name" required="required" validationMessage="required"/>
		        </label>
		        <label>
		        	<span class="modSetName">category</span>
		            <input type="text" name="category" class="modSetValue" value="${category}" id="modSet-category" required="required" validationMessage="required"/>
		        </label>
		        <div class=clear-both ></div>
		        <label>
		        	<span class="modSetName">description</span>
		        	<input type="textarea" class="dp100 modSetValue" data-role="editor" id="modSet-description" value="${description}" name="description" />
		        </label>
			</div>
			<div  id="right-pm-details" >
				
		        <label>
		        	<span class="modSetName">Value</span>
		        	<input type="text" value="${value}" name="value" class="modSetValue" id="modSet-value" required="required" validationMessage="required"/>
		        </label>
		        <div class="padding formEl_a">
					<div class="sepH_b ">
						<label class="lbl_a" for="field_type" >Type</label>
						<select data-role="combobox" name="type" id="type" class="SettingData inpt_a modSetValue" data-id="# if (id) { # #=id # # } else { #0# } #"  data-bind="events: { change: SelectValue }">
							<option value="text" # if (type == "text" ) { # selected # } # >text</option>
							<option value="area"# if (type == "area" ) { # selected # } # >textarea</option>
							<option value="radio" # if (type == "radio" ) { # selected # } # >radio buton</option>
							<option value="hidden" # if (type == "hidden" ) { # selected # } # >hidden</option>	
							<option value="document" # if (type == "document" ) { # selected # } # >file</option>
							<option value="image" # if (type == "image" ) { # selected # } # >image</option>
							<option value="media" # if (type == "media" ) { # selected # } # >media</option>
							<option value="link" # if (type == "link" ) { # selected # } # >link</option>
							<option value="select" # if ( type == "checkbox" ||  type == "radio" || type == "dropdown" || type == "select" ) { # selected # } # >multiselect</option>
						</select> 
					</div>
					<div class="sepH_b # if ( type != "checkbox" &&  type != "radio" && type != "dropdown" && type != "select" ) { # hidden # } #">
						<label class="lbl_a" for="field_type" >type</label>
						<select data-role="combobox" name="select_type" id="select_type" class="SettingData inpt_a">
							<option value="checkbox" # if (type == "checkbox" ) { # selected # } # >checkbox</option>
							<option value="radio" # if (type == "radio" ) { # selected # } # >radiobtn</option>
							<option value="dropdown" # if (type == "dropdown" ) { # selected # } # >dropdown</option>
							<option value="select" # if (type == "select" ) { # selected # } # >Select</option>
						</select> 
					</div>
					<div class="sepH_b multioptions # if ( type != "checkbox" &&  type != "radio" && type != "dropdown" && type != "select" ) { # hidden # } # "  >
						<label class="lbl_a" for="field_type" >data</label>
						<table border="0" cellspacing="5" cellpadding="5" id="dataTable" class="table_a smpl_tbl  spacer-top" >
							<tr>
								<td class="k-header">Key</td>
								<td class="k-header">Value</td>
								<td class="k-header"><a href="\#" id="addRow" class="k-icon k-add"></a></td>
							</tr>
							<tbody class="settingcontent">
								# var i=0; #
								# if ( options ) { options = clearInfection(options); #
									# for (var x in options) { #
										<tr class="dataRow" id="row#= i #">
											<td><input type="text" name="options[]" class="SettingDataJson modSetValue modSetValueKey inpt_a" value="#= x #"></td>
											<td><input type="text" name="options[]" class="SettingDataJson modSetValueVal inpt_a" value="#= options[x] #"></td>
											<td># if ( i != 0 ) {  #<a href="\\#" class="removeRow" rel="row#= i #">remove</a># } #</td>
										</tr>
										# i++; #
									# } #
								# } else { #
										<tr class="dataRow" id="row0">
											<td><input type="text" name="options[]" class="SettingDataJson modSetValue modSetValueKey inpt_a" value=""></td>
											<td><input type="text" name="options[]" class="SettingDataJson modSetValueVal inpt_a" value=""></td>
											<td><a href="\\#" class="removeRow" rel="row0">remove</a></td>
										</tr>
									# i=1; #
								# } #
							</tbody>
						</table>
						<input type="hidden" value="#= i #" id="icounter"/>
					</div>
				</div>
	    	</div>
	    	<div  id="right-pm-opsettings" >
		    	<label>
		    		<span class="modSetName">Comments</span>
		    		<input type="textarea" class="dp100 modSetValue" data-role="editor" id="modSet-comments" value="${comments}" name="comments" />
		    	</label>
	    	</div>
		</div>
	</div>
</script>
{/literal}
<script>
head.js('../scripts/multiSelect/chosen.jquery.min.js');
//head.js("../scripts/mcmsSortable.js");
head.js('../scripts/jquery-ui-1.8.18.custom.min.js');
head.js('../scripts/admin2/settings/modules.js');
</script>

<style scoped>
	.list-box-view
	{
		display: inline-block;
		list-style: none;
	    float: left;
	    width: 100%;
	    margin: 0px;
		/*height: 30px;*/
	    -moz-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    -webkit-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    box-shadow: inner 0 0 50px rgba(0,0,0,0.1);
	    border-top: 1px solid rgba(0,0,0,0.1);
	}
	div.list-box-view
	{
		display: inline-block;
	    float: left;
	    width: 100%;
	    margin: 0px;
		/*height: 375px;*/
	    -moz-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    -webkit-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    box-shadow: inner 0 0 50px rgba(0,0,0,0.1);
	    border-top: 1px solid rgba(0,0,0,0.1);
	}
	.k-editor
	{
		width: 100% !important;
	}
	.list-box-view.k-edit-item
	{
		height: 265px;
	}
	.list-box-view.ui-sortable-placeholder
	{
		height: 35px !important;
	}
	.list-box-view .textVal
	{
		margin: 5px;
		position: relative;
		display: block;
		float: left;
		width: 230px;
		height: 20px;
		overflow: hidden;
	}
	.list-box-view img
	{
		margin: 5px;
		position: relative;
		display: block;
		float: left;
	}
	.list-box-view .k-button
	{
		margin: -2px 5px;
		position: relative;
		display: block;
		float: left;
	}
	#right-pm-details, #left-pm-details
	{
		margin: 10px;
	    padding: 3px;
		margin: 10px 0;
	}
	.k-listview:after, .product-view dl:after
	{
	    content: ".";
	    display: block;
	    height: 0;
	    clear: both;
	    visibility: hidden;
	}
	.edit-buttons
	{
		text-align: right;
	    padding: 5px;
		height: 50px;
		width: 150px;
		float: right;
		possition: relative;
	
	}
	div.list-box-view .edit-buttons
	{
		width: 182px;
		height: 50px;
		margin-right: -7px;
	}
	.k-toolbar, #resultsList, .k-pager-wrap
	{
	    margin: 0 auto;
	}	
	.k-tabstrip .k-content, .k-panelbar .k-tabstrip .k-content {
		border-style: solid;
    	border-width: 1px;
    	position: static;
    	padding 0.3em 0.3em;
    	margin: 0px;
	}
	 .connected, .sortable, .exclude, .handles {
		   margin: auto;
		   padding: 0;
		   width: 310px;
		   -webkit-touch-callout: none;
		   -webkit-user-select: none;
		   -khtml-user-select: none;
		   -moz-user-select: none;
		   -ms-user-select: none;
		   user-select: none;
		  }
		  .sortable.grid {
		   overflow: hidden;
		  }
		  .connected li, .sortable li, .exclude li, .handles li {
		   list-style: none;
		   border: 1px solid #CCC;
		   background: #F6F6F6;
		   font-family: "Tahoma";
		   color: #1C94C4;
		   margin: 5px;
		   padding: 5px;
		   height: 22px;
		  }
		  .handles span {
		   cursor: move;
		  }
		  li.disabled {
		   opacity: 0.5;
		  }
		  .sortable.grid li {
		   line-height: 80px;
		   float: left;
		   width: 80px;
		   height: 80px;
		   text-align: center;
		  }
		  li.highlight {
		   background: #FEE25F;
		  }
		  #connected {
		   width: 440px;
		   overflow: hidden;
		   margin: auto;
		  }
		  .connected {
		   float: left;
		   width: 200px;
		  }
		  .connected.no2 {
		   float: right;
		  }
		  .mcmsSortable-placeholder {
		   border: 1px dashed #CCC;
		  	margin: 10px;
		  }
</style>