<form class="formEl_a" name="{$formID|default:'saveMenuItem'}Form" id="{$formID|default:'saveMenuItem'}Form">
<div id="menuItemDetails-{$class}" class="box_c_content cf">
		<div class="sepH_b  spacer-right">
       		<label for="title" class="lbl_a">{$lang.title} *:</label>
        	<input name="title" id="title" type="text"  value="{$menu.title}" class="{$class}  inpt_a" {if $menu.settings.syncWithOriginal}disabled{/if} required validationMessage=" {$lang.error_requiredField}"/>
        </div>
        <div class="sepH_b  spacer-right">
        	<label for="permalink" class="lbl_a">{$lang.permalink} *:</label>
        	<input name="permalink" id="permalink" type="text"  value="{$menu.permalink}" class="{$class} inpt_a" {if $menu.settings.syncWithOriginal}disabled{/if} required validationMessage=" {$lang.error_requiredField}"/>
        </div>
        <div class="sepH_b  spacer-right">
        	<label for="url" class="lbl_a">{$lang.url} *:</label>
        	<input name="link" id="link" type="text" value="{$menu.link}" class="{$class}  inpt_a"  required validationMessage=" {$lang.error_requiredField}"/>
        </div>
        <div class="sepH_b  spacer-right">
        	<label for="seotitle" class="lbl_a">{$lang.lbl_seoTitle}: </label>
          	<input name="settings_title" type="text" class="{$class}  inpt_a" id="desc" value="{$menu.settings.title}" />
        </div>
        <div class="sepH_b  spacer-right">
        <label for="available" class="lbl_a">{$lang.availability}: </label>
        	<select name="active" id="active" class="{$class}  inpt_a">
        		<option value="0" {if $menu.active eq "0"}selected{/if}>{$lang.disabled}</option>
        		<option value="1" {if $menu.active eq "1"}selected{/if}>{$lang.enabled}</option>
      		</select> 
      </div>
      <div class="sepH_b spacer-right">
      		<label for="available" class="lbl_a">{$lang.lbl_syncWithOriginalItem}: </label>
            <input type="checkbox" name="syncWithOriginal" class="inpt_a" id="syncWithOriginal-{$class}"  {if $menu.settings.syncWithOriginal}checked{/if} />
            <input type="hidden" name="settings_syncWithOriginal" id="settings_syncWithOriginal-{$class}" class="{$class}"  value="{if $menu.settings.syncWithOriginal}1{else}0{/if}" />
        </div>
        <div class="sepH_b spacer-right">
        	<input name="submit" type="button"  data-id="{$id}"  class="btn btn_a {if $action=="edit"} editMenuItem {else}addLink{/if}"  value="{$lang.save}" />
        </div>
</div><!-- END MENU ITEM DETAILS -->
</form>