<div style="min-width:600px; height:400px; overflow:auto;">
	
	 <div class="box_c_heading cf">
			 <ul class="tabsS fr" id="helptabs">
			 {foreach from=$items item=a}
                <li><a href="#">{$a.title}</a></li>
             {/foreach}
            </ul>
	</div>
	 <div class="box_c_content cf tabs_help">
	  	{foreach from=$items item=b}
            <div>
				{$b.description}
			</div>
		{/foreach}
	</div>

<script>
head.ready(function(){

	$("#helptabs").flowtabs("div.tabs_help > div");
	  
});
</script>