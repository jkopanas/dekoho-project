<div id="editExtra"></div>

{include file="admin2/extra_fields/extra_fields_groups.tpl"} {if $shown}

<div id="debugArea"></div>
<div class="dp100">
	<div class="box_c">
		<h3 class="box_c_heading cf">{$lang.extra_fields}</h3>
		<div class="box_c_content cf">
			<table id="gridview">
				<thead>
					<tr>
						<th data-field="checkbox" data-filterable="false"
							data-width="5%" data-type="integer" data-title="<input
							type='checkbox' id='total_check' name='ids[1]' class='check-all' />"><input
							type="checkbox" id="total_check" name="ids[1]" class="check-all" />
						</th>
						<th data-field="fieldid" data-key="true" data-width="10%"
							data-filterable="false" data-type="integer" data-title="ID">#ID</th>
						<th data-field="field" data-type="string" data-filterable="false" data-width="30%"
							data-title="{$lang.field_name}">{$lang.field_name}</th>
						<th data-field="var_name" data-type="string" data-width="30%"
							data-filterable="false" data-title="{$lang.var_name}">{$lang.var_name}</th>
						<th data-field="active" data-type="string" data-filterable="false"
							data-width="8%" data-title="{$lang.active}">{$lang.active}</th>
						<th data-field="type" data-type="string" data-filterable="false"
							data-width="10%" data-title="{$lang.type}">{$lang.type}</th>
						<th data-template="">&nbsp;</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

{/if}

<script id="id-commands-gridview" class="id-commands-gridview" type="text/kendo-ui-template">
				<div data-id="#= id #" class="k-button k-button-icontext EditExtra">Edit&nbsp;&nbsp;<span class="k-icon k-edit"></span></div>
        </script>

<script id="id-toolbar-gridview" type="text/kendo-ui-template">	
						<div class="fl spacer-right" style="padding-top:5px;">
							{$lang.withSelectedDo}  (<span class="messages">0</span> {$lang.selected} )
						</div>    		           	
    	                	<a class="k-button k-button-icontext ToolBarActions" data-action="ActivateChecked" data-trigger="activated" href="\\#" ><span class="k-icon k-enable"></span>{$lang.enable}</a>
    	                	<a class="k-button k-button-icontext ToolBarActions" data-action="DeactivateChecked" data-trigger="disabled" href="\\#" ><span class="k-icon k-disable"></span>{$lang.disable}</a>
							<a class="k-button k-button-icontext ToolBarActions" data-action="DeleteChecked" data-trigger="Deletechecked" href="\\#" ><span class="k-icon k-delete"></span>{$lang.delete}</a>
						<div class="k-button k-button-icontext float-right margin-right"  id="CreateNewExtra">
    	                	Create New Extra Field&nbsp;&nbsp;<span class=" k-icon k-add"></span>
    	                </div>
						<div class="float-right">Search: <input type="text" class="k-input SearchText" data-eq="fieldid" data-contains="field"  data-grid="gridview" placeholder=" Search ID,Field Name ..." /></div>
		</script>
		
			<script type="text/x-kendo-template" id="template-checkbox-gridview">
				<div id="checkBox-#= fieldid #">
					<input type="checkbox" class="check-values" value="#= fieldid #"  name="selectedID" # if (typeof(checked) != "undefined" ) { # #= checked # # } # />
				</div>
           </script>
           

<script type="text/x-kendo-template" id="template-active-gridview">
				<div id="active-#= fieldid #">
					<div style="text-align:center;"># if (active != "N" ) { # <span class="notification ok_bg">#= $.lang.yes #</span>  # } else { # <span class="notification error_bg">#= $.lang.no #</span> # } #</div>
				</div>
			</script>

<div class="seperator"></div>

<link href="/scripts/multiSelect/chosen.css" rel="stylesheet" />
<script>
head.js('../scripts/multiSelect/chosen.jquery.min.js');
head.js('../scripts/jquery-ui-1.8.18.custom.min.js');
head.js('/scripts/admin2/grid.js');
head.js('../scripts/admin2/extra_fields/efields.js');
head.js('../scripts/admin2/extra_fields/Extrafields.js');
</script>
{if $field}
<script
	src="/scripts/AdminLanguages.js"></script>
{/if}
