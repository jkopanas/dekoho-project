<div id="delete-confirmation"></div>

<script id="delete-confirmation_menu" type="text/x-kendo-template">
		<p class="delete-message" >{$lang.confirm_deletion}</p> <br/>
		<button class="k-button delete-confirm" data-id="#= item #">{$lang.delete}</button>
		<a href="\\#" class="float-right spacer-right delete-cancel">{$lang.lbl_cancel}</a>
</script>

<div id="editGroup"></div>

<script id="newGroups" type="text/x-kendo-template">

<form id="GroupForm" name="GroupForm" class="formEl_a">
	<div class="box_c_content cf" id="menuItemDetails">
		<div class="sepH_b  spacer-right">
       		<label class="lbl_a" for="title">{$lang.title} :</label>
        	<input type="text" class="groupData inpt_a" value="#= group.title #" id="title" name="title" required>
        </div>
       <div class="sepH_b  spacer-right">
       		<label class="lbl_a" for="description">{$lang.description} :</label>
        	<input type="text" class="groupData inpt_a" value="#= group.description #" id="description" name="description">
        </div>
		<div class="sepH_b  spacer-right">
        <label class="lbl_a" for="theme">{$lang.theme} :</label>
        	<select name="themes" multiple="MULTIPLE" class="mSelect inpt_d groupData" id="themes" data-placeholder="Select one" data-table="themes">
				# for (var item in themes) { #
          				<option value="#= themes[item].id #" #= themes[item].checked # > #= themes[item].name # </option>
 				# } #
      		</select> 
      	</div>
		<div class="float-left dp50">
		<fieldset>
		<legend>{$lang.availitems}</legend>
		<div class="sepH_b" >
        <label class="lbl_a" for="availableitems"></label>
			<ul id="gridextrafields" style="height: 150px; overflow-y:scroll;">
				# for (var item in extrafields) { #
					<li><input type="checkbox" name="AddField[]"  data-title="#= extrafields[item].field #" id="check_#= extrafields[item].fieldid #"/><b>\\##= extrafields[item].fieldid #</b>  #= extrafields[item].field #</li>
				# } #
			</ul>
			<a href="\\#" class="btn btn_d AddToItem fr spacer-top">Add</a>
      	</div>
		</fieldset>
		</div>
		<div class="float-left dp50">
		<fieldset>
		<legend>{$lang.groupitems}</legend>
		<div class="sepH_b">
			<div id="groupitems">
			</div>
      	</div>
		</fieldset>
		</div>
        <div class="sepH_b spacer-right clear tac">
  			<input type="hidden" name="module" value="{$CURRENT_MODULE.name}" class="groupData" />
  			<input type="hidden" value="Efields" name="action" class="groupData" />
			<input id="save_cat"  data-id="#= group.id #" class="btn btn_b SaveGroupExtra" type="button" name="Enter" value="Αποθήκευση">
        </div>
</div>
</form>
</script>


    <div class="formEl_a dp100">
    	<div class="box_c">
    	<h3 class="box_c_heading cf">{$lang.group_extra_fields}</h3>
    	<div class="box_c_content cf" >
    	<div class="k-grid k-widget">
    	<div class="k-toolbar k-grid-toolbar">
    			<div class="k-button k-button-icontext margin-left float-right"  id="CreateNewGroup">{$lang.add_new_group}&nbsp;&nbsp;<span class=" k-icon k-add"></span></div>
    	</div>
    	<div class="k-grid-header">
    	<div class="k-grid-header-wrap">
    		<table cellspacing="0">
    			<thead>
        			<tr>
	  					<td width="5%" class="k-header" ><input type="hidden" value="SaveEfieldsGroupsOrder" name="action" /></td>
      					<td width="25%"  class="k-header" >{$lang.field_name}</td>
      					<td width="25%" class="k-header" >{$lang.description}</td>
      					<td width="15%" class="k-header" >{$lang.numberOfElements}</td>
      					<td width="25%" class="k-header">&nbsp;</td>
    				</tr>
    			</thead>
    		</table>
    	</div>
    	</div>
    		<div class="k-grid-content">
    			<table cellspacing="0" class="k-focusable"  id="t-groups">
    				<colgroup>
    					<col style="width:5%">
    					<col style="width:25%">
    					<col style="width:25%">
    					<col style="width:15%">
    					<col style="width:25%">
    				</colgroup>
    				<tbody class="content">
  					</tbody>
    			</table>
    		</div>
    	</div>
    	</div>
    	</div>
	</div>

 <script id="existingGroups" type="text/x-kendo-template">
	<tr data-id="#= id #">
	    <td width="5%" class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
	    <td width="25%" data-field="title"><a href="\\#" class="FilterGroup" data-in="groupid" data-grid="gridview" data-id="#= id #"  >#= title #</td>
		<td width="25%" data-field="description">#= description #</td>
	    <td width="15%" data-field="count">#= count #</td>  
		<td width="25%">  
           	<div class="k-button k-button-icontext GroupEdit" data-id="#= id #" >Edit&nbsp;&nbsp;<span class="k-icon k-edit"></span></div>
			<div class="k-button k-button-icontext GroupDelete" data-id="#= id #" >Delete&nbsp;&nbsp;<span class="k-icon k-delete"></span></div>
		</td>
	</tr>
</script>
