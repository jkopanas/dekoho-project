{assign var='sum' value="100"}
{assign var='flag' value="false"}
{foreach from=$layout.boxes item=a name=b}
	{foreach from=$a item=c name=d }
			{if $c.fetchType == "static"} {assign var="file" value=$c.template} {/if}
			{assign var='sum' value=$sum-$layout.settings[$c.id]}
			{if $sum < 0 }
				</div>
				{assign var='sum' value="100"}
				{assign var='flag' value="false"}
				{assign var='sum' value=$sum-$layout.settings[$c.id]}
			{/if}
			{if $sum >= 0 && $flag =='false' }
				<div class="column float-left dp100">
				{assign var='flag' value="true"}
			{/if}
				{include file="admin2/dashboard/dashBox.tpl" class="dp{$layout.settings[$c.id]}" id="`$c.id`" name="`$c.name`" content=$file title={$lang[$c.title]|default:$c.title} dataProperties=$c.data}
			{if $sum <= 0 && $flag =='true' }
				</div>
				{assign var='sum' value="100"}
				{assign var='flag' value="false"}
			{/if}			
	{/foreach}	
{/foreach}

<style>
		.placeholder {
			background-color: #cfcfcf;
			border:2px dashed #999;
		}
</style>



		<script id="id-commands-gridview" class="id-commands-gridview" type="text/kendo-ui-template"></script>
        
        <script type="text/x-kendo-template" id="template-title-gridview" data-width="89%">
		<div class="OverTitle dp100">
		<div class="dp75 float-left"><a href="\\#">#= title #</a></div>
		<div class="dp25 float-right hidden toolbar">
			<div class="float-right">
			<a href="/manage/items.php?module=#= module #&id=#= id #" ><img src="/images/admin/content.png" width="14" class="spacer-left" title="{$lang.manage_content}" /></a>
			<a href="/manage/items.php?module=#= module #&id=#= id #\#!/mediaFiles" ><img src="/images/admin/media.png" width="14" class="spacer-left" title="{$lang.mediaFiles}" /></a>
			<a href="/manage/items.php?module=#= module #&id=#= id #\#!/itemsTranslations" ><img src="/images/admin/translate.png" width="14" class="spacer-left" title="{$lang.translations}" /></a>
			<a href="/manage/items.php?module=#= module #&id=#= id #\#!/extraFields" ><img src="/images/admin/extra_field.png" width="14" class="spacer-left" title="{$lang.extra_fields}" /></a>
			</div>
		</div>
		</div>
		</script>
        
       
         <script id="template-toolbar-PagesSearch" type="text/kendo-ui-template">	
						{foreach from=$curr_modules item=a name=b}
    		           	<div  {if $smarty.foreach.b.first}class="k-button k-button-icontext margin-left ModuleFilter k-state-selected"{else}class="k-button k-button-icontext margin-left ModuleFilter"{/if}  data-id="{$a}">{$lang[$a]}</div>
						{/foreach}
						<div class="float-right">{$lang.search}: <input type="text" class="k-input SearchText" data-eq="id" data-contains="title"  data-grid="gridview" placeholder=" {$lang.search} ID,text ..." /></div>
		</script>
        
          <script type="text/x-kendo-template" id="template-active-gridview"  data-width="11%">
                 # if (active != 0) { # <span class="fl notification ok_bg">#= $.lang.yes #</span>  # } else { # <span class="fl notification error_bg">#= $.lang.no #</span> # } #
          </script>
	 
	      <script type="text/x-kendo-template" id="template-id-gridview" data-width="5%">#= id #</script>

         
<script src="/scripts/kendo/js/kendo.dataviz.min.js"></script>
<script src="/scripts/kendo/js/kendo.web.min.js"></script>

<script>
	head.js('/scripts/admin2/grid.js');
	head.js('/scripts/admin2/dashboard.js');
	head.js('/scripts/jquery-ui-1.8.18.custom.min.js');
</script>



