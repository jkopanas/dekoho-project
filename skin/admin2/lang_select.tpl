<div id="tmp" class="hidden"><div id="tmpContent"></div></div>
{if $mode eq "single"}
<form name="singleVar" id="singleVar" class="formEl_a">
  {if $uid}
<input type="hidden" name="Varname" value="{$name}" />
 	{/if}
<input type="hidden" name="VarCode" value="{$code}" />
<input type="hidden" name="uid" value="{$uid}" />
 {if !$uid}
				<div class="formEl_a">				
                    <div class="sepH_b dp50">
                        <label for="title" class="lbl_a">{$lang.variable}</label>
                        <input type="text" value="{$a.name}" name="Varname" class="SaveVar inpt_b" required validationMessage="Η μεταβλητή δεν μπορέι να είναι κενή"/>
                       	<span class="f_help  spacer-right">{$lang.name_help_headline}</span>
                    </div>												
                    <div class="sepH_c ">
                        <label for="text" class="lbl_a">{$lang.topic}</label>
                       <select name="topic" id="topic" class="SaveVar" required>
        					<option value="common" {if $smarty.post.lang_topic eq "common"}selected{/if}>Common</option>
        					<option value="front" {if $smarty.post.lang_topic eq "front"}selected{/if}>Front End</option>
        					<option value="admin" {if $smarty.post.lang_topic eq "admin"}selected{/if}>Admin</option>
        					<option value="js" {if $smarty.post.lang_topic eq "js"}selected{/if}>Javascript</option>
      					</select>
                        <span class="f_help  spacer-right">{$lang.surname_help_headline}</span>
                    </div>		
  {/if}
<table class="table_a" >
<TR class="TableHead">
  <th width="120"><center>{$lang.language}</center></th>
      <th width="120"><center>{$lang.description}</center></th>
      <th width="250"><center>{$lang.value}</center></th>
  </tr>
{foreach from=$var item=a}
<tr>
  <td class="vam tac" ><strong>{$a.code}</strong></td>
  <td  class="vam"><input type="text" value="{$a.descr}" name="descr-{$a.code}" class="SaveVar inpt_b" /></td>
  <td style="width:150px;"><textarea id="value-{$a.code}" cols="10" rows="5"  class="SaveVar" name="value-{$a.code}" required validationMessage="Η τιμή δεν μπορέι να είναι κενή">{$a.value}</textarea>
      <br>
      <a href="#" class="ckEditor" data-height="150" rel="value-{$a.code}">{$lang.advanced_editor}</a>
  </td>
</tr>
{/foreach}
{if $uid}
<tr><td colspan="3" ><input type="button" class="btn btn_a saveSingleTranslation float-right" value="{$lang.save}" /></td></tr>
{else}
<tr><td colspan="3" ><input type="button" class="btn btn_a saveSingleTranslation float-right" value="{$lang.save}" /></td></tr>
{/if}
</table>
</div>
</form>
{elseif $mode eq "translateEfield1"}
<form  method="post" name="trans">
<input type="hidden" class="originalData" value="{$fieldid}" name="id" />
<div class="debug"></div>
<table width="100%"  border="0" cellpadding="5" cellspacing="5">
<TR class="TableHead">
  <th width="120">{$lang.language}</th>
      <th width="120">{$lang.field_name}</th>
    </tr>
{foreach from=$availableLanguages item=a}
<tr>
  <td><strong>{$a.code}</strong></td>
  <td><input name="extra_fields@field-{$a.code}" type="text" id="field-{$a.code}" size="45" value="{$a.item.field.translation}" class="ContentTitle languageTranslationBox"></td>
</tr>
{/foreach}
<tr><td colspan="2" align="center"><input type="button" value="{$lang.save}" class="SaveTranslations button" /></td></tr>
</table>
</form>
{elseif $mode eq "translateEfield"}
<form  method="post" name="trans">
<input type="hidden" class="originalData" value="{$fieldid}" name="id" />
<div class="debug"></div>
<div id="lang-container">
<div>
<ul>
<li class="radios">
    <span class="field-name">{$lang.language}:</span>
        <ul>
{foreach from=$availableLanguages item=a name=b}
            <li>
                <input id="lang-{$a.code}-check" type="radio" class="langchooser" name="lang" rel="#lang-{$a.code}" {if $smarty.foreach.b.first}CHECKED="CHECKED"{/if}><label for="lang-{$a.code}-check">{$a.code}</label>
            </li>
{/foreach}
        </ul>
</li>

</ul>
</div>

<div class="clear">
{foreach from=$availableLanguages item=a name=b}
<div id="lang-{$a.code}" class="langcontainer {if !$smarty.foreach.b.first}hidden{/if}">
    <ul>
        <li><label><span class="field-name">{$lang.title}: </span><input name="extra_fields@field-{$a.code}" type="text" id="field-{$a.code}" size="45" value="{$a.item.field.translation}" class="ContentTitle languageTranslationBox"></label></li>
        <li><label><span class="field-name">{$lang.description}: </span><input name="extra_fields@settings_description-{$a.code}" type="text" id="settings_description-{$a.code}" size="45" value="{$a.item.settings_description.translation}" class="ContentTitle languageTranslationBox"></label></li>
        <li><label><span class="field-name">{$lang.description}({$lang.search}): </span><input name="extra_fields@settings_searchdes-{$a.code}" type="text" id="settings_searchdes-{$a.code}" size="45" value="{$a.item.settings_searchdes.translation}" class="ContentTitle languageTranslationBox"></label></li>
    </ul>
</div>
{/foreach}
</div>
<input type="button" value="{$lang.save}" class="SaveTranslations button" />
</div>
</form>

{elseif $mode eq "translateEfieldValues"}

<form  method="post" name="transValues" id="EfieldForm">
<input type="hidden" class="originalData" value="{$fieldid}" name="id" />
<input type="hidden" class="originalData" value="{$itemid}" name="itemid" />
<div class="debug"></div>
<table width="100%"  border="0" cellpadding="5" cellspacing="5" id="EfieldValue">
<TR class="TableHead">
  <th width="120">{$lang.language}</th>
      <th width="120">{$lang.value}</th>
    </tr>
{foreach from=$availableLanguages item=a}
<tr>
  <td><strong>{$a.code}</strong></td>
  <td >
  {if $field.type eq "text"}
  <input name="extra_field_values@value-{$a.code}" type="text" id="value-{$a.code}" size="45" value="{$a.item.value.translation}" class="ContentTitle languageTranslationBox">
  {elseif $field.type eq "area"}
  <textarea name="extra_field_values@value-{$a.code}" id="value-{$a.code}" class="ContentTitle languageTranslationBox" cols="10" rows="5">{$a.item.value.translation}</textarea>
   <br>
       <a href="javascript:void(0);" class="open-editor" rel="value-{$a.code}">{$lang.advanced_editor}</a>
  {/if}
  </td>
</tr>
{/foreach}
<tr><td colspan="2" align="center"><input type="button" value="{$lang.save}" class="SaveTranslations button" /></td></tr>
</table>
</form>

{elseif $mode eq "translateEfieldGroup"}

<form  method="post" name="trans">
<input type="hidden" class="originalData" value="{$groupid}" name="id" />
<div class="debug"></div>
<div id="lang-container">
<div>
<ul>
<li class="radios">
    <span class="field-name">{$lang.language}:</span>
        <ul>
{foreach from=$availableLanguages item=a name=b}
            <li>
                <input id="lang-{$a.code}-check" type="radio" class="langchooser" name="lang" rel="#lang-{$a.code}" {if $smarty.foreach.b.first}CHECKED="CHECKED"{/if}><label for="lang-{$a.code}-check">{$a.code}</label>
            </li>
{/foreach}
        </ul>
</li>

</ul>
</div>

<div class="clear">
{foreach from=$availableLanguages item=a name=b}
<div id="lang-{$a.code}" class="langcontainer {if !$smarty.foreach.b.first}hidden{/if}">
    <ul>

        <li><label><span class="field-name">{$lang.title}: </span><input name="extra_fields_groups@title-{$a.code}" type="text" id="title-{$a.code}" size="45" value="{$a.item.title.translation}" class="ContentTitle languageTranslationBox"></label></li>
        <li><label><span class="field-name">{$lang.description}: </span><input name="extra_fields_groups@description-{$a.code}" type="text" id="description-{$a.code}" size="45" value="{$a.item.description.translation}" class="ContentTitle languageTranslationBox"></label></li>
    </ul>
</div>
{/foreach}
</div>
<input type="button" value="{$lang.save}" class="SaveTranslations button" />
</div>
</form>
{elseif $mode eq "translateRoomValues"}



<form  method="post" name="trans">
<input type="hidden" class="originalData" value="{$roomid}" name="id" />
<div class="debug"></div>
<div id="lang-container">
<div>
<ul>
<li class="radios">
    <span class="field-name">{$lang.language}:</span>
        <ul>
{foreach from=$availableLanguages item=a name=b}
            <li>
                <input id="lang-{$a.code}-check" type="radio" class="langchooser" name="lang" rel="#lang-{$a.code}" {if $smarty.foreach.b.first}CHECKED="CHECKED"{/if}><label for="lang-{$a.code}-check">{$a.code}</label>
            </li>
{/foreach}
        </ul>
</li>

</ul>
</div>

<div class="clear">
{foreach from=$availableLanguages item=a name=b}
<div id="lang-{$a.code}" class="langcontainer {if !$smarty.foreach.b.first}hidden{/if}">
    <ul>

        <li><label><span class="field-name">{$lang.title}: </span><input name="rooms@name-{$a.code}" type="text" id="title-{$a.code}" size="45" value="{$a.item.name.translation}" class="ContentTitle languageTranslationBox"></label></li>
        <li><label><span class="field-name">{$lang.description}: </span>
        <textarea name="rooms@description-{$a.code}" id="description-{$a.code}" class="ContentTitle languageTranslationBox" cols="50" rows="5">{$a.item.description.translation}</textarea>
        <br>
       <a href="javascript:void(0);" class="open-editor" rel="description-{$a.code}">{$lang.advanced_editor}</a>
        {*<input name="rooms@description-{$a.code}" type="text" id="description-{$a.code}" size="45" value="{$a.item.description.translation}" class="ContentTitle languageTranslationBox">*}</label></li>
    </ul>
</div>
{/foreach}
</div>
<input type="button" value="{$lang.save}" class="SaveRoomTranslations button" />

</div>
</form>




{else}
<form  method="post" name="trans">
<input type="hidden" class="settings" name="SavedData" value="{$SavedData}" />
<input type="hidden" name="code" value="{$posted_data.code}">
{if $list.total gt $items|@count}
{include file="modules/content/pagination.tpl" mode='languagesPaginate'}
{/if}
<table width="100%"  border="0" cellpadding="5" cellspacing="5">
<TR class="TableHead">
      <th width="100">{$lang.variable}</th>
      <th width="120">{$lang.description}</th>
      <th width="700">{$lang.value}</th>
      <th width="100">{$lang.topic}</th>
      <th width="16">&nbsp;</th>
	  <th width="16">&nbsp;</th>
       <th width="16">&nbsp;</th>
    </tr>
	
    {foreach from=$items item=a}
    <tr {cycle values="'FFFFFF', class='alternate'"} id="Var-{$a.name}">
      <td class="variable"><div class="text" id="name-{$a.name}">{$a.name}</div><input name="name-{$a.name}" type="hidden" value="{$a.name}" class="Save"></td>
      <td class="descr"><div class="text" id="descr-{$a.name}">{$a.descr}</div><input name="descr-{$a.name}" type="hidden" value="{$a.descr}" class="Save"></td>
      <td class="value"><div class="text" id="value-{$a.name}">{$a.value}</div><input name="value-{$a.name}" type="hidden" value="{$a.value}" class="Save"></td>
      <td><span class="forumheader2">{$a.topic} </span></td>
      <td><a href="#" class="editLanguageVariableGlobal" title="{$lang.edit_all_translations}" rel="Var-{$a.name}"><img src="/images/admin/language_16.png" width="16" height="16" /></a></td>
	       <td><a href="#" class="editLanguageVariable" title="{$lang.edit}" rel="#Var-{$a.name}"><img src="/images/admin/edit_16.png" width="16" height="16" /></a></td><td><a href="#" class="deleteLanguageVariable" title="{$lang.delete}" rel="{$a.name}"><img src="/images/admin/delete_16.png" width="16" height="16" /></a></td>
    </tr>
	{/foreach}

  </table>
{if $list.total gt $items|@count}
{include file="modules/content/pagination.tpl" mode='languagesPaginate'}
{/if}
</form>
{/if}
<!--  -->