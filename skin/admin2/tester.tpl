
<div class="dataTables_wrapper" id="data_table_wrapper">
<div id="data_table_length" class="dataTables_length"><label>Show <select size="1" name="data_table_length"><option value="10" selected="selected">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div>
  <table cellpadding="0" cellspacing="0" border="0" class="table_a smpl_tbl" id="notifier">
							<thead>
								<tr>
								  <th class="chb_col"><div class="th_wrapp"><input type="checkbox" class="chSel_all inpt_c1"></div></th>
									<th width="60"><div class="th_wrapp">#ID</div></th>
								  <th><div class="th_wrapp">Title</div></th>
									<th><div class="th_wrapp">module</div></th>
									<th><div class="th_wrapp">Date imported</div></th>
                                    <th><div class="th_wrapp">Status</div></th>
								</tr>
								<tr class="filtersRow">
									<th class="chb_col"><div class="th_wrapp">&nbsp;</div></th>
									<th><div class="th_wrapp"><input name="id" type="text" class="filter" size="4" /></div></th>
									<th><div class="th_wrapp"><input name="title" type="text" class="filter"  /></div></th>
									<th><div class="th_wrapp"><input name="module" type="text" class="filter"  /></div></th>
									<th><div class="th_wrapp"><input name="date_added" type="text" class="filter" /></div></th>
                      <th><div class="th_wrapp"><input name="status" type="text" class="filter"/></div></th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
                        <div class="dataTables_info" id="data_table_info">Showing 1 to 9 of 9 entries</div>
<div class="dataTables_paginate paging_two_button" id="data_table_paginate"><div class="paginate_disabled_previous" title="Previous" id="data_table_previous"></div><div class="paginate_enabled_next" title="Next" id="data_table_next"></div></div>
<div class="clear"></div>
</div>

{literal}
<div id="notifier-templates">
<script id="row" type="text/kendo-ui-template" data-type="text">
<tr>
	<td class="chb_col"><input type="checkbox" name="row_sel" class="inpt_c1"></td>
	<td><a href="\\#" class="editMe" data-id="#= key #" data-module="#= module #">#= id #</a></td>
	<td><a href="\\#" class="editMe" data-id="#= key #" data-module="#= module #">#= description #</a></td>
	<td>#= module #</td>
	<td>#= mcms.strftime( date_added,'%d/%m/%Y @ %H:%M') #</td>
	<td>#= status #</td>
</tr>
</script>
<script id="loader" type="text/kendo-ui-template" data-type="text">
<tr><td colspan="6" align="center">Loading...</td><tr>
</script>

<script id="form" type="text/kendo-ui-template" data-type="text">
                        <div class="dp50">
                        <div class="box_c">
                        <div class="box_c_heading cf">New</div>
                        <div class=class="box_c_content cf">
                        #var t = ''; t = me.templates[me.options.forms[a.module]](records.revision)#
						#= t #
                        </div><!-- END CONTENT -->
                        
                        </div><!-- END BOX -->
                        </div><!-- END LEFT -->
                        <div class="dp50">
                        <div class="box_c">
                        <div class="box_c_heading cf">Old</div>
                        <div class=class="box_c_content cf">
                        #var t = ''; t = me.templates[me.options.forms[a.module]](records.current)#
						#= t #
                        </div><!-- END CONTENT -->
                        
                        </div><!-- END BOX -->
                        </div><!-- END LEFT -->   
                        <div class="clear"></div>
</script>

<script id="item" type="text/kendo-ui-template" data-type="text">
<div class="formEl_a">
<div class="seperator"></div>
<div class="sepH_b">
    <label for="title" class="lbl_a">#= $.lang.title #</label>
    <input type="text" id="title" name="title" class="inpt_a toSave"   value='#= title.value || title #' required  />
    <span class="f_help">Here you can enter a headline.</span>
</div>
<div class="sepH_b">
    <label for="title" class="lbl_a">#= $.lang.description #</label>
	<textarea name="description" class="inpt_a toSave">#= description.value || description #</textarea>
    <span class="f_help">Here you can enter a headline.</span>
</div>

</div><!-- END FORM -->
</script>
<script id="components" type="text/kendo-ui-template" data-type="text">

</script>
</div>
{/literal}
<textarea name="fileTypes" class="hidden">{$fileTypes}</textarea>
<textarea name="mediaFiles" class="hidden">{$data}</textarea>

<a href="#" class="crop">crop</a>
                <ul id="manualUploadModeExample" class="unstyled"></ul>
                <span id="triggerUpload" class="btn btn-primary">Upload Queued Files</span>
<div id="mediaGallery"></div>
<div id="mediaGallery2"></div>
<link rel="stylesheet" href="/scripts/jcrop/css/jquery.Jcrop.css" type="text/css" />
<link rel="stylesheet" href="/scripts/uploader/uploader.css" type="text/css" />
{literal}
<script>

	head.js('/scripts/jcrop/js/jquery.Jcrop.js','/scripts/uploader/uploader.js','/scripts/admin2/importNotifier.js');
	//head.js('/scripts/admin2/mediaGallery/index.js');
	///ajax/loader.php?file=items/upload.php&module=content&action=xhr
var i=0;
	head.ready(function(){
$('#notifier').notifier({templates:'#notifier-templates'});

		var previewWidth = 201,previewHeight = 124;
	var db = new mcms.dataStore({
		method: "POST",
		url:"/ajax/loader.php?file=mediaFiles/imageActions.php&action=cropImage&module=content",
		dataType: "json",
		trigger:"loadComplete"	
	});	
		
	function showCoords(c)
    {
      $('#x1').val(c.x);
      $('#y1').val(c.y);
      $('#x2').val(c.x2);
      $('#y2').val(c.y2);
      $('#w').val(c.w);
      $('#h').val(c.h);
	  
	 
    };

    function clearCoords()
    {
      $('#coords input').val('');
    };
	var o = jQuery.parseJSON($('textarea[name=mediaFiles]').val());
	var r = ratio(o.sizes.thumb.width,o.sizes.thumb.height);
		function startCropTool(){
      var jcrop_api, boundx, boundy;
      $('#preview').attr('src',$('#target').attr('src'));
      $('#target').Jcrop({
        onChange: updatePreview,
        onSelect: updatePreview,
        aspectRatio: r[0]/r[1]
      },function(){
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        // Store the API in the jcrop_api variable
        jcrop_api = this;
      });

      function updatePreview(c)
      {
		  showCoords(c);
        if (parseInt(c.w) > 0)
        {
          var rx = previewWidth / c.w;
          var ry = previewHeight / c.h;

          $('#preview').css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
          });
        }
      };
		}
var p = {
	request:{
		endpoint:'/ajax/loader.php?file=items/upload.php&module=content'
	}
}







	$('#mediaGallery').mediaGallery({ imageTypes:o.sizes, modifyImage: { crop:false},uploader:p, fileType:'thumb', 
	modules:[{id:'addFromUpload',text:'upload from computer'}],
	fileTypes:jQuery.parseJSON($('textarea[name=fileTypes]').val()),onUploaderSelect:function(e,b,r){ console.log(this) },onAppLoaded:function(e){ 
	//startCropTool();
	
	}});
//$('#mediaGallery2').mediaGallery({ a:2});
var x = $('#mediaGallery').data('mediaGallery');


function gcd(a,b) { if(b>a) { temp = a; a = b; b = temp} while(b!=0) { m=a%b; a=b; b=m;} return a;}

// ratio is to get the gcd and divide each component by the gcd, then return a string with the typical colon-separated value 
function ratio(x,y) { c=gcd(x,y); return [(x/c),(y/c)]}

$('.crop').on('click',function(e){
	e.preventDefault();
	
	var p = mcms.formData($('.toSave'),{ getData:false})
	db.data = $.extend({ coords:p,oldWidth:$('#target').width(),oldHeight:$('#target').height()},o.image.images.thumbs.image_full.originals);
	$(db).loadStore(db);
});


		/*
		    var delay = (function(){
      var timer = 0;
      return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
      };
    })();
		var uploader2 = new qq.FineUploader({
        element: $('#manualUploadModeExample')[0],
        autoUpload: true,
		debug:false,
        text:{ uploadButton:  "Select Files"},
       fileTemplate: '<li>' +
            '<div class="qq-progress-bar"></div>' +
            '<span class="qq-upload-spinner"></span>' +
            '<span class="qq-upload-finished"></span> <span class="fr"><a href="#">DELETE ++ {retryButtonText}</a></span>' +
            '<span class="qq-upload-file"></span>' +
            '<span class="qq-upload-size"></span>' +
            '<a class="qq-upload-cancel" href="#">{cancelButtonText}</a>' +
            '<a class="qq-upload-retry" href="#">{retryButtonText}</a>' +
            '<span class="qq-upload-status-text">{statusText}</span>' +
            '</li>',
        request: {
			params:{ data:1},
            endpoint: "/ajax/loader.php?file=items/upload.php&module=content&action=xhr"
        },
        callbacks: {
			onComplete: function(id,fileName,obj){
				
				var me = this;

				

				rm(this,id)

			},
            onSubmit: function(id, fileName){
				var x = fileName.split('.').pop();
				uploader2.setParams({ ext:x});
			},
			onValidate: function(fileData,isBatch){
				
			},
			onUpload: function(id,fileName){

			}
        },
		validation: {
			allowedExtensions: '',
			//acceptFiles:['image/jpeg','image/png']
		}
    });
		    $('#triggerUpload').click(function() {
        uploader2.uploadStoredFiles();
    });
function rm(me,el){
					delay(function(){

					console.log($(me._options.element).find('li.qq-upload-success').eq(el))
				  }, 1000 );
	
}
*/
});//END HEAD
</script>


<style>
.AutoCompleteresults { background:white; z-index:9999; position:absolute; border:1px solid #ebebeb }
.AutoCompleteresults li { padding:5px; color:#777777}
.AutoCompleteresults li:hover, .hovered { background:#eeeeee;}
.AutoCompleteresults  .matched { color:black; font-weight:bold; background:#ffffcc; }
.catItem { border:1px solid #DFDFDF; padding:10px; line-height:130%; color:#555; background-color:#f3f3f3; min-height:31px;   }

#modal-back { position: fixed; top: 0; left: 0; width: 100%;  height: 100%;   background: #000;   opacity: 0.5;   filter: alpha(opacity=50);}
#modal { top:50%; left:50%;position:absolute;z-index:9999; width:900px; height:550px;  }
#modal .inner {  background:#fff; height:350px; width:100%;  position:relative; color:black; border:1px solid black; padding-top:10px; padding-bottom:10px; overflow-y:scroll; }
#modal .inner h1 { color:#143856; font-size:22px; text-align:center; padding-top:40px; position:relative;font-weight:normal; }
#modal .inner h2 { color:#ed1c24; font:26px; position:absolute; bottom:100px; left:240px; font-weight:normal; }
#modal .inner .noResults { position:absolute; bottom:1px; height:70px; color:#0374a6; font-size:14px; width:100%; }
#modal .inner .noResults h3{  margin-bottom:20px; text-align:center; font-weight:normal; font-size:14px;  }
#modal .inner .noResults p { margin-bottom:10px; text-align:center; }
#modal .close { position:absolute;  background:url('/skin/images/admin2/close.png'); width:36px; height:36px; top:-20px; right:-15px; text-indent:-5000px; z-index:9999; cursor:pointer; }
.filtersRow input { width:95%; }
.filtersRow th { text-align:center; }
.dataTables_paginate{width:auto;float:right;text-align:right}
.paginate_disabled_previous,.paginate_enabled_previous,.paginate_disabled_next,.paginate_enabled_next{height:19px;width:19px;margin-left:3px;float:left}
.paginate_enabled_previous,.paginate_enabled_next{cursor:pointer}
.dataTables_info{float:left}
.paginate_disabled_previous{background-image:url(../images/admin/back_disabled.jpg)}
.paginate_enabled_previous{background-image:url(../images/admin/back_enabled.jpg)}
.paginate_disabled_next{background-image:url(../images/admin/forward_disabled.jpg)}
.paginate_enabled_next{background-image:url(../images/admin/forward_enabled.jpg)}
</style>
{/literal}