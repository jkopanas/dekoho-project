<div id="window" class="">
<form id="packageForm" method="post">
<div id="packageSettings" class="formEl_a  cf">
    <div class="sepH_b  spacer-right">
        <label for="version" class="lbl_a">Version no. :</label>
        <input name="version" id="version"  type="text"  value="{$VERSION_NUMBER}" class="toSave inpt_a" data-field="version"  />
    </div>
    <div class="sepH_b  spacer-right">
        <label for="title" class="lbl_a">Package name :</label>
        <input name="title" id="title"  type="text"  value="" class="toSave inpt_a" data-field="title"  />
    </div>
    <div class="sepH_b  spacer-right">
        <label for="changelog" class="lbl_a">Changelog :</label>
        <textarea name="changelog" id="changelog" class="toSave inpt_a" data-field="changelog"></textarea>
    </div>
    <div class="sepH_b  spacer-right">
        <label for="sqltables" class="lbl_a">SQL :</label>
        <textarea name="sqltables" id="sqltables" class="toSave inpt_a" data-field="sqltables" rows="20"></textarea>
    </div>
    <div class="sepH_b  spacer-right">
    
    </div>
           <div class="spacer-right spacer-top">
        	<a href="#" id="saveManifest"  class="btn btn_d saveManifest">{$lang.save}</a> 
        </div>
</div><!-- END FORM DIV -->
</form>
</div>
<div class="spacer-left breadCrumb"></div>
<div id="gr"></div>
<div class="seperator"></div>
<h2>File Manifest</h2>
<div id="selectedFiles"></div>
<script type="text/x-kendo-template" id="file" data-name="file" class="kTemplate">
 # if (type == 'folder') { #<a href="\\#" class="expand" data-name="#= file #" data-path="#= path #" data-md="#= md5 #">#= file #</a> # if (typeof subs != 'undefined') { # ( #= subs # items ) # } } else { #	#= file # # } # 
</script>

<script type="text/x-kendo-template" id="fileSimple" data-name="fileSimple" class="kTemplate">
#= file #
</script>

<script type="text/x-kendo-template" id="checkTemplate" class="kTemplate" data-name="checkBox">
<input type="checkbox" class="check-values" value="#= file #" name="selectedID" data-file="#= path #" data-md="#= md5 #" data-id="#= id #"  />
</script>

<script type="text/x-kendo-template" id="type" class="kTemplate" data-name="type">
<img src="/scripts/kendo/styles/treeview/#= type #.png" />
</script>

<script type="text/x-kendo-template" id="modified" class="kTemplate" data-name="modified">
#= mcms.strftime(modified,'%d/%m/%Y @ %H:%M') #
</script>

<script type="text/x-kendo-template" id="fileSize" class="kTemplate" data-name="fileSize">

#= sizeFormated #
</script>

<script type="text/x-kendo-template" id="breadCrumb" class="kTemplate" data-name="breadCrumb">
# var x = new Array;  #
<a href="\\#" class="expand" data-path="" data-md="d41d8cd98f00b204e9800998ecf8427e">Root</a>
#  $.each(data,function(k,v){ #
# x.push(v);#
# if (data.length != 1 && k != data.length-1) { # <a href="\\#" class="expand" data-md="#= v.md5 #" data-path="#= x.join('/') #">#= v #</a> # } else { # #= v # # } #
# if (data.length != 1 && k != data.length-1) { # / # } #
# }); #
</script>

<script type="text/x-kendo-template" id="manifest" class="kTemplate" data-name="manifest">
<ul>
#  $.each(data,function(k,v){ #
<li>#= v # </li>
# }); #
</ul>
</script>


<script type="text/x-kendo-template" id="toolBar">
<div class="fl spacer-right" style="padding-top:5px;">
{$lang.withSelectedDo}  (<span class="messages">0</span> {$lang.selected} )
</div>
</script>
<script>
head.js('/scripts/admin2/items/itemsSearch.js');
head.js('/scripts/updater/packager.js');
</script>

    