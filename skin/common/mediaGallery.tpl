<div id="templates">
<script id="main" type="text/kendo-ui-template" data-type="text">

<div class="box_c">
<h3 class="box_c_heading cf">
<ul class="tabsS fr spacer-right">
# for(i=0;options.modules.length>i;i++){ #
<li><a href="\\#/#= options.modules[i].id #" rel="#= options.modules[i].id #" # if (i ==0) { #class="current" # } #> #= options.modules[i].text #</a></li>
# } #
        </ul>
    
</h3>
</div>


<div id="tabs" class="tabs_content">
# for(i=0;options.modules.length>i;i++){ #
# var t = templates[options.modules[i].id](data); #
<div id="#= options.modules[i].id #" class="tab # if (i !=0) { #hidden#}#">#= t #</div>
# } #
</div>
</script>


<script id="addFromUpload" type="text/kendo-ui-template" data-type="text">

<ul class="uploader"></ul>
<div class="seperator"></div>

</script>

<script id="addFromUrl" type="text/kendo-ui-template" data-type="text">
addFromUrl
</script>

<script id="addFromLibrary" type="text/kendo-ui-template" data-type="text">
addFromLibrary
</script>

<script id="addFromGallery" type="text/kendo-ui-template" data-type="text">
addFromGallery
</script>

<script id="modifyImage" type="text/kendo-ui-template" data-type="text">

# var t= templates['imageEditor'](options.modifyImage.image) #
#= t #
</script>

<script id="imageEditor" type="text/kendo-ui-template" data-type="text">

<div class="fl spacer-right"><img src="/ajax/loader.php?file=items/itemsActions.php&imageid=#= imageid #&action=imagePreview" id="target"  /></div>
          <div style="width:#= arguments[1].options.imageTypes.thumb.width #px;height:#= arguments[1].options.imageTypes.thumb.height #px;overflow:hidden;" class="fl">
            <img src="/images/fancybox/blank.gif" id="preview" alt="Preview" class="jcrop-preview" />
          </div>
		  
		  <div class="seperator"></div>

	  <input type="hidden" size="4" id="x1" name="x" class="toSave"  />
      <input type="hidden" size="4" id="y1" name="y" class="toSave" />
      <input type="hidden" size="4" id="x2" name="x2" class="toSave" />
      <input type="hidden" size="4" id="y2" name="y2" class="toSave" />
      <input type="hidden" size="4" id="w" name="w" class="toSave" />
      <input type="hidden" size="4" id="h" name="h" class="toSave" />
</script>


<script id="uploadHandler" type="text/kendo-ui-template" data-type="text">

</script>
</div>