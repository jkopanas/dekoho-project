{if $extra_fields}
<div id="boxes"> 
 
<div id="mask"></div> 
<div id="FileTree" class="window"></div></div>
<table width="600"  border="0" cellspacing="5" cellpadding="5">
{foreach from=$extra_fields item=a name=b}
<tr class="{cycle values=",alternate"}" id="Item{$a.fieldid}">
<td width="300"><strong>{$a.field} :</strong> </td>
<td>
{if $a.type eq "text"}
<input type="text" name="efields-{$a.fieldid}" value="{$a.value}" /> <a href="#" class="translateEfieldValues" title="{$lang.edit_all_translations}" rel="{$a.fieldid}"  data-itemid="{$id}"><img src="/images/admin/language_16.png" width="16" height="16" /> {$lang.translate}</a>
{elseif $a.type eq "area"}
<textarea name="efields-{$a.fieldid}" id="efields-{$a.fieldid}" cols="50" rows="5">{$a.value}</textarea><br /><a href="javascript:void(0);" class="open-editor" rel="efields-{$a.fieldid}">{$lang.advanced_editor}</a> <a href="#" class="translateEfieldValues" title="{$lang.edit_all_translations}" rel="{$a.fieldid}" data-itemid="{$id}"><img src="/images/admin/language_16.png" width="16" height="16" /> {$lang.translate}</a>
{elseif $a.type eq "checkbox"}
<input type="checkbox" name="efields-{$a.fieldid}" value="{$a.value}" {if $a.value}checked{/if} />
{elseif $a.type eq "radio"}
<label><input type="radio" value="1" name="efields-{$a.fieldid}" {if $a.value eq 1}checked{/if}> {$lang.yes}</label> <label><input type="radio" value="0" name="efields-{$a.fieldid}" {if $a.value eq 0}checked{/if}> {$lang.no}</label>
{elseif $a.type eq "image"}
<input type="text" name="efields-{$a.fieldid}" value="{$a.value}" disabled="disabled" size="50" id="efields{$a.fieldid}" /> <A href="/modules/file_manager.php?module={$CURRENT_MODULE.name}&media=image&itemid={$id}&mode=FillField&rel=efields{$a.fieldid}&width=700&height=600" class="InsertFile">Επιλογή αρχείου</A>

{elseif $a.type eq "document"}
<input name="efields-{$a.fieldid}" type="text" disabled="disabled" value="{$a.value}" size="50" id="efields{$a.fieldid}" />
<A href="/modules/file_manager.php?module={$CURRENT_MODULE.name}&media=document&itemid={$id}&mode=FillField&rel=efields{$a.fieldid}&width=700&height=600" class="InsertFile">Επιλογή αρχείου</A>
{elseif $a.type eq "media"}
{/if}
</td>
</tr>
{/foreach}
</table>

{/if}