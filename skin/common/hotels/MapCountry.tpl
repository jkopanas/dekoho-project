<div id="MapChange">
<input type="hidden" name="mapCountry" value='{$mapItem}' />
<input type="hidden" name="loaded_language" value="{$BACK_LANG}" />
 <form name="addressForm" id="addressForm" method="post" class="float-left">
<label>{$lang.lbl_address} : <input name="address" type="text" value="{$item.geocoderAddress}"  class="ContentTitle" size="45" /> </label> 
<input type="submit" value="Look up" name="searchAddress" class="btn btn_d button" />
</form>
<form method="post" class="float-left">
<input type="hidden" name="geocoderAddress" value="{$item.countryid_geocoderAddress}" class="get" />
<input type="hidden" name="lat" class="get" value="{$item.countryid_lat}" />
<input type="hidden" name="lng" class="get" value="{$item.countryid_lng}" />
<input type="hidden" name="zoomLevel" class="get" value="{$item.countryid_zoomLevel}" />
<input type="hidden" name="MapTypeId" class="get" value="ROADMAP" />
<input type="hidden" name="itemid" value="{$id}" class="get" />
<input type="hidden" name="countryid" value="{$item.countryid_id}" class="get" />
<input type="hidden" name="regionid" value="{$item.regionid_id}" class="get" />
<input type="hidden" name="cityid" value="{$item.cityid_id}" class="get" />
<input type="hidden" name="module" value="{$current_module.name}" class="get" />
<button class="btn btn_d spacer-left float-left"  id="savePlaceOnMap" >{$lang.save}</button>
</form>
<br class="clear" />
  <div class="dx"> 
        <h2>{$lang.searchResults}</h2>
        <ul id="geocodingResults" class="list"> 
        
        </ul>
  </div>
<div class="clearfix example spacer-top"> 
      <div class="sx"> 
      <div class="float-left" style="width:20%; height:350px;">
       <select name="MapOption" id="MapOption">
			<option value="43" >Country</option>
			<option value="45" >Region Map</option> 
			<option value="44">City Map</option>
			<option value="46">Hotel Map</option>
		</select>
		<div class="seperator"></div>
		<ul class="spacer-top" style="height: 350px;">
		{foreach from=$countries item=a}
			<li><span><a href="#" id="resultsCountry" class="results" data-id="{$a.id}">{$a.geocoderAddress}</a> ({if $a.tid == "" }0{else}{$a.total}{/if})</span></li>
		{/foreach}
		</ul>
      </div> 
      <div  id="mapCountry" class="mapbox float-left" style="width:80%; height:350px;"></div> 
      </div> 
</div>
<div id="geocodingDataNow">
<p class="currentPosition">{$lang.lbl_currentPosition} : <strong><span></span></strong></p>
<p class="address">{$lang.lbl_closestAddress} : <strong><span></span></strong></p>
</div>
<div class="seperator"></div>
<script type="text/javascript" src="/scripts/maps/gMaps3.js"></script> 
<script type="text/javascript" src="/scripts/hotels/MapCountry.js"></script> 
</div>