<div id="MapChange">
<select name="MapOption" id="MapOption">
<option value="43">Country Map</option>
<option value="45">Region Map</option>
<option value="44">City Map</option>
<option value="46" selected>Hotel Map</option>
</select>
<input type="hidden" name="mapHotel" value='{$mapItem}' />
<input type="hidden" name="loaded_language" value="{$BACK_LANG}" />
<div>
 <form name="addressForm" id="addressForm" method="post" class="float-left">
<label>{$lang.lbl_address} : <input name="address" type="text" value="{$item.geocoderAddress}"  class="ContentTitle" size="45" /> </label> <input type="submit" value="Look up" name="searchAddress" class="btn btn_d button" />
</form>
<form method="post" class="float-left">
<input type="hidden" name="geocoderAddress" value="{$item.hoteid_geocoderAddress}" class="get" />
<input type="hidden" name="lat" class="get" value="{$item.hotelid_lat}" />
<input type="hidden" name="lng" class="get" value="{$item.hotelid_lng}" />
<input type="hidden" name="zoomLevel" class="get" value="{$item.hotelid_zoomLevel}" />
<input type="hidden" name="MapTypeId" class="get" value="ROADMAP" />
<input type="hidden" name="itemid" value="{$id}" class="get" />
<input type="hidden" name="module" value="hotel" class="get" />
<button class="btn btn_d spacer-left float-left"  id="savePlaceOnMap" >{$lang.save}</button>
</form>
<br class="clear" />
<div class="clearfix example spacer-top"> 
      <div class="sx"> 
        <div  id="mapHotel" class="mapbox" style="width:100%; height:350px;"></div> 
      </div> 
  <div class="dx"> 
        <h2>{$lang.searchResults}</h2>
        <ul id="geocodingResults" class="list"> 
        
        </ul>
  </div>
</div>
<div id="geocodingDataNow">
<p class="currentPosition">{$lang.lbl_currentPosition} : <strong><span></span></strong></p>
<p class="address">{$lang.lbl_closestAddress} : <strong><span></span></strong></p>
</div>
<div class="seperator"></div>
</div>

<script type="text/javascript" src="/scripts/hotels/MapHotel.js"></script> 
</div>