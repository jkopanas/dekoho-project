<style>

.stars, .stars1 {
	background: url("/images/site/sprite.png") no-repeat scroll 0 -9200px transparent;
display:block;
height:10px;
margin-bottom:.3em;
margin-top:6px;
margin-left:5px;
text-indent:-100000px;
width:10px;
float:left;
}



.stars2,.notepad span.stars2,span.rating_star2 {
width:20px;
}

.stars3,.notepad span.stars3,span.rating_star3 {
width:30px;
}

.stars4,.notepad span.stars4,span.rating_star4 {
width:40px;
}

.stars5,.notepad span.stars5,span.rating_star5 {
width:50px;
}
.filters li { list-style-type:none; }
.filters .selected {
    background: none repeat scroll 0 0 #B2081C;
    padding: 2px;
    position: relative;
}

.filters .selected a {
    background: url("/images/site/close-filter.gif") no-repeat scroll 0 0 transparent;
    float: right;
    height: 7px;
    margin: 2px;
    text-indent: -3000px;
    width: 7px;
}
.filters .selected label {
    color: white;
    font-size: 11px;
    font-weight: bold;
}
.filters label {
    font-size: 13px;
}
label.disabled { color:#adacac; text-decoration:line-through;}
.header_space {
 background: url("/images/site/finder_tab.png") repeat-x scroll left top transparent; 
color: #fff;
height:23px;
text-indent:5px;
padding: 1px;
padding-top:5px;
font-weight: bold;
font-size: 14px;
}
.scroll {
overflow-y: auto;
max-height: 215px;
padding-top:5px;
padding-bottom:5px;
background:#dee6ec;
color:#3b3838;
}
.overlay-container {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.4);
  text-align: center;
  vertical-align: center;
  font-size: 50px;
  z-index: 9999;
}
.overlay-container .overlay-text {
  position: absolute;
  margin: auto;
  height: 80px;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  color: #FFE;
  text-shadow: 0px 1px 0px #999, 0px 2px 0px #888, 0px 3px 0px #777, 0px 4px 0px #666, 0px 5px 0px #555, 0px 6px 0px #444, 0px 7px 0px #333, 0px 8px 7px #001135;
  font-size: 80px;
  font-weight: bold;
}
#map-infobox {
position: absolute;
bottom: 6px;
left: 0;
margin-left: 7%;
margin-right: 7%;
width: 86%;
border: 1px solid #bcb8ac;
border-radius: 6px;
background-color: #fff;
color: #333;
z-index: 10;
-moz-box-shadow: 0 0 8px 0 #888;
-webkit-box-shadow: 0 0 8px 0 #888;
box-shadow: 0 0 8px 0 #888;
font-size: 13px;
padding-bottom: 3px;
}

#map-infobox-close {
position: absolute;
width: 18px;
height: 18px;
border: 1px solid #0b78c1;
border-radius: 6px;
top: -4px;
right: -4px;
cursor: pointer;
background: #fff url(/images/site/sprite_icons_index.png) no-repeat -45px -352px;
text-indent: 100px;
overflow: hidden;
display: block;
z-index: 20;
}
#map { position:relative; }

#moreResults  a{
	margin-top:15px;
	margin-bottom:15px;
	width:100%;
	border:1px solid #8bb7dd;
	background:#72a5d5;	
	text-align:center;
	height:35px;
	text-decoration:none; color:#fff; font-size:18px; padding-top:10px; display:block;
}

#moreResults a:hover { background:#5b94ce; }
.selected { background:#ccc;}
.clear { clear:both;}
</style>

<div ng-controller="searchCtrl">
<div class="container" loading-message="Loading...">
  <div id="page-wrapper">
    <div class="content">
<div class="left-column-inner">
<div id="search">
<form name="searchForm" id="searchForm"  class="searchForm">
		    <ul class="search-menu float-left">
            {literal}
			    	<li class="search-item" ng-class="{'active-search':filters.mode=='normal'}"><a href="" ng-click="mode('normal')">Hotel Finder</a></li>
			    	<li class="search-item" ng-class="{'active-search':filters.mode=='offers'}"><a href="" ng-click="mode('offers')">Special Offers</a></li>
			    	<li class="search-item" ng-class="{'active-search':filters.mode=='roulete'}"><a href="" ng-click="mode('roulete')">Suprise Me</a></li>
                    {/literal}
			    </ul>
			    <div class="seperator"></div>
			    <div id="map-filters">
				    <div class="map-filter-item">
				    <span>Destination</span><div class="seperator"></div>
				    <input type="text" name="destination" id="destination" class="grey SearchText" placeholder="city, region" ng-model="filters.destination" required suggest-city/>
				    </div>
				    <div class="map-filter-item">
				    <span>Arrival</span><div class="seperator"></div>
				    <input type="text" name="arrival" id="arrival" class="grey SearchText-small" ng-model="filters.arrival" date-picker dest="arrival" required />
				    </div>
				    <div class="map-filter-item">
				    <span>Departure</span><div class="seperator"></div>
				    <input type="text" name="departure" id="departure" class="grey SearchText-small" ng-model="filters.departure" date-picker dest="departure" required />
				    </div>
                    
                    <div ng-repeat="a in roomsNum">
				     <div class="map-filter-item">
                     {literal}
				    <span ng-hide="$index>0">Room {{$index+1}}</span><div class="seperator"></div>
				    <select id="rooms" name="rooms" class="grey selectrooms-persons" ng-model="filters.roomsNum" ng-init="filters['room-'+$index] = a.room"  ng-change="moreRooms('room-'+$index)" ng-hide="$index>0">
				     <option value="1">1 Room</option>
				     <option value="2">2 Rooms</option>
				     <option value="3">3 Rooms</option>
				     <option value="4">4 Rooms</option>
				     <option value="5">5 Rooms</option>
				     <option value="6">6 Rooms</option>
				     <option value="7">7 Rooms</option>
				      <option value="8">8 Rooms</option>
				    </select>
                    <p ng-show="$index>0">Room {{$index+1}}</p>
                    {/literal}
				    </div>
				    <div class="map-filter-item">
				    <span ng-hide="$index>0">Persons</span><div class="seperator"></div>
				    <select name="persons" id="persons" class="grey selectrooms-persons " ng-model="filters['persons-'+$index]" >
				     <option value="1">1 Person</option>
				     <option value="2">2 Persons</option>
				     <option value="3">3 Persons</option>
				     <option value="4">4 Persons</option>
				     <option value="5">5 Persons</option>
				     <option value="6">6 Persons</option>
				     <option value="7">7 Persons</option>
				     <option value="8">8 Persons</option>
				    </select>
				    </div>
                    <div class="seperator"></div>
                    </div>
                    
				    <div class="map-filter-item spacer-right">
                    
				    <span>Budget from</span><div class="seperator"></div>
				    
				    <div class="control-group">
					      <div class="input-prepend input-append float-left ">
				    			<input type="text" name="budget_from" id="budgetfrom" class="grey SearchText-mini" ng-model="filters.budget_from" min="0" max="10" integer dest="from" />
				    			<span class="add-on"> {$current_currency.currency} </span> 
					    	</div>
						</div>
				    </div>
				    
				    <div class="map-filter-item spacer-left">
				    <span>Bugdet to</span><div class="seperator"></div>
				    
				    	<div class="control-group">
					      <div class="input-prepend input-append float-left ">
				    			<input type="text" name="departure" id="departure" class="grey SearchText-mini" ng-model="filters.budget_to" min="0" max="10" integer dest="to" />
				    			<span class="add-on"> {$current_currency.currency} </span> 
					      </div>
						</div>
				    </div>
				    <div class="more-criteria">
                    <span ng-show="searchForm.budget_from.$error.float">This is not valid integer!</span>
                    <span ng-show="errors.departSmall">Departure date must be larger than arrival data</span>
                    <div class="map-filter-item">
				    <span>Hotel name</span><div class="seperator"></div>
				    <input type="text" name="title" id="title" class="grey SearchText" placeholder="Hotel name" ng-model="filters.title"  />
				    </div>
				    </div>
                    
                    <a href="" class="btn btn-primary btn-large" ng-click="filter()" ng-disabled="searchForm.$invalid">Find Out Now</a>
                    
                    <div id="search-filters">    
	     
	     <div class="seperator"></div>
	     {literal}
         <a href="" ng-click="resetFilters()" class="label label-important float-left" ng-show="appliedFilters.length>0">Reset filters</a>
         	     <div class="seperator-small"></div>
         <h1 class="header_space spacer-top">Hotel stars</h1>
         <ul class="scroll filters">
         <li ng-repeat="a in stars" class="clear">
         <label><input type="checkbox" value="{{a}}" ng-checked="isChecked('stars_'+a,'stars')" ng-click="chkModel('stars_'+a,'stars'); filter();" class="float-left" id="stars_{{a}}" /> <span class="stars stars{{a}}"></span>     </label>
        </li> 
         </ul>
         
         

         
         
         <h1 class="header_space spacer-top">Type of hotel</h1>
         <ul class="scroll  filters">
         <li ng-repeat="a in fields.types | orderBy:'title'" ng-class="{selected:filters.types.indexOf(fieldMap.types.prefix+''+a.id) !=-1}" ng-hide="a.count==0">
         <label ng-class="{disabled:a.count==0}"><input type="checkbox"   value="{{a.id}}"  ng-disabled="a.count==0"  ng-checked="isChecked(fieldMap.types.prefix+''+a.id,'types')" ng-click="chkModel(fieldMap.types.prefix+''+a.id,'types'); filter();" ng-hide="filters.types.indexOf(fieldMap.types.prefix+''+a.id) !=-1" /> 
        {{a.title}} ({{a.count}}) </label>
        <a ng-click="unsetFilter(a.id,'types')" ng-show="filters.types.indexOf(fieldMap.types.prefix+''+a.id) !=-1" class="closeFilter" href="" style="">close</a>
        
        </li>
        </ul>
        

         
         <h1 class="header_space spacer-top">Hotel facilities</h1>
         <ul class="scroll filters">
         <li ng-repeat="a in fields.facilities | orderBy:'title'" ng-class="{selected:filters.facilities.indexOf(fieldMap.facilities.prefix+''+a.id) !=-1}"  ng-hide="a.count==0">
         <label ng-class="{disabled:a.count==0}"><input type="checkbox"  value="{{a.id}}" ng-disabled="a.count==0"  ng-checked="isChecked(fieldMap.facilities.prefix+''+a.id,'facilities')" ng-click="chkModel(fieldMap.facilities.prefix+''+a.id,'facilities'); filter();" ng-hide="filters.facilities.indexOf(fieldMap.facilities.prefix+''+a.id) !=-1" /> 
        {{a.title}} ({{a.count}})</label>
        
        <a ng-click="unsetFilter(a.id,'facilities')" ng-show="filters.facilities.indexOf(fieldMap.facilities.prefix+''+a.id) !=-1" class="closeFilter" href="" style="">close</a>
        </li>
        </ul>
         
         <h1 class="header_space spacer-top">Hotel services</h1>
         <ul class="scroll filters">
         <li ng-repeat="a in fields.services | orderBy:'title'" ng-class="{selected:filters.services.indexOf(fieldMap.services.prefix+''+a.id) !=-1}"  ng-hide="a.count==0">
         <label ng-class="{disabled:a.count==0}"><input type="checkbox"  value="{{a.id}}" ng-disabled="a.count==0"  ng-checked="isChecked(fieldMap.services.prefix+''+a.id,'services')" ng-click="chkModel(fieldMap.services.prefix+''+a.id,'services'); filter();" ng-hide="filters.services.indexOf(fieldMap.services.prefix+''+a.id) !=-1" /> 
        {{a.title}} ({{a.count}})</label>
        <a ng-click="unsetFilter(a.id,'services')" ng-show="filters.services.indexOf(fieldMap.services.prefix+''+a.id) !=-1" class="closeFilter" href="" style="">close</a>
        </li>
        </ul>
     	
			 <h1 class="header_space spacer-top">Hotel themes</h1>
         <ul class="scroll filters">
         <li ng-repeat="a in fields.themeshotels | orderBy:'title'" ng-class="{selected:filters.themeshotels.indexOf(fieldMap.themeshotels.prefix+''+a.id) !=-1}"  ng-hide="a.count==0">
         <label ng-class="{disabled:a.count==0}"><input type="checkbox"  value="{{a.id}}" ng-disabled="a.count==0"  ng-checked="isChecked(fieldMap.themeshotels.prefix+''+a.id,'themeshotels')" ng-click="chkModel(fieldMap.themeshotels.prefix+''+a.id,'themeshotels'); filter();" ng-hide="filters.themeshotels.indexOf(fieldMap.themeshotels.prefix+''+a.id) !=-1" /> 
        {{a.title}} ({{a.count}})</label>
        <a ng-click="unsetFilter(a.id,'themeshotels')" ng-show="filters.themeshotels.indexOf(fieldMap.themeshotels.prefix+''+a.id) !=-1" class="closeFilter" href="" style="">close</a>
        </li>
        </ul>
        
        			 <h1 class="header_space spacer-top">Rooms facilities</h1>
         <ul class="scroll filters">
         <li ng-repeat="a in fields.roomsfacilities | orderBy:'title'" ng-class="{selected:filters.roomsfacilities.indexOf(fieldMap.roomsfacilities.prefix+''+a.id) !=-1}"  ng-hide="a.count==0">
         <label ng-class="{disabled:a.count==0}"><input type="checkbox"  value="{{a.id}}" ng-disabled="a.count==0"  ng-checked="isChecked(fieldMap.roomsfacilities.prefix+''+a.id,'roomsfacilities')" ng-click="chkModel(fieldMap.roomsfacilities.prefix+''+a.id,'roomsfacilities'); filter();" ng-hide="filters.roomsfacilities.indexOf(fieldMap.roomsfacilities.prefix+''+a.id) !=-1" /> 
        {{a.title}} ({{a.count}})</label>
        <a ng-click="unsetFilter(a.id,'roomsfacilities')" ng-show="filters.roomsfacilities.indexOf(fieldMap.roomsfacilities.prefix+''+a.id) !=-1" class="closeFilter" href="" style="">close</a>
        </li>
        </ul>
     	{/literal}
     	<div class="seperator"></div>
     </div>
	    
                    
				    <div class="seperator"></div><div class="seperator"></div>
			    </div>
                </form>
                </div>
                <div class="seperator"></div><div class="seperator"></div>
</div><!-- END LEFT -->

<div class="right-column-inner"  id="hotels">
			{literal}

            <div ng-show="mapVisible" id="map">
            <div id="map-infobox" ng-show="itemShown">
    <div id="map-infobox-close" ng-click="closeInfoWindow(item.id)">X</div>
    <div id="map-infobox-wrap" class="clearfix">
      <div id="map-infobox-carousel">
        <div id="map-infobox-carousel-wrap" class="float-left"> 
                <ul>
                <li class="first"><img src="{{item.media.big_thumb}}" height="100" alt="" style="padding:7px;"><span class="img-frame"></span></li>
                </ul>
          </div>
      </div>
        <div id="map-infobox-content" class="clearfix float-left width423">
            <div id="map-infobox-content-wrap">
               <div class="float-left">
                <h4 id="map-infobox-title" class="float-left"><a href="/{{code}}/hotel/information/{{item.id}}/{{item.permalink}}.html" class="hotel-title-map">{{item.title}}</a></h4>
                <span class="finder-stars float-left spacer-right"><span class="stars stars{{item.stars}}" style="margin-top:10px;"></span></span>
				<span id="map-infobox-price-from" class="from-map">from</span ><span class="from-map-price"> {{item.price}} {/literal}{$current_currency.symbol}{literal}</span>
				</div><br class="clear_0"/>
               <span class="map-hotel-desc1"> {{item.description.substring(0,280)}}...</span>
                <div id="map-infobox-additional-content">
                    
                   <div class="seperator"></div>
                    <div id="map-infobox-cta">
                        <div id="map-infobox-urgency-message"></div>
                       <a class="btn btn-warning btn-small float-right" href="/{/literal}{$FRONT_LANG}{literal}/reservation/index.html?{{hotelUrl}}{literal}&hotelid={{item.id}}{/literal}">Reservation</a>
                        <a class="btn btn-info btn-small spacer-right float-right" href="/{/literal}{$FRONT_LANG}{literal}/hotel/information/{{item.id}}/{{item.permlink}}.html?{{hotelUrl}}">view</a>
                        <div class="seperator"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
           	<div class="google-map" 
		center="center"
		zoom="zoom" 
		markers="markers"
		latitude="latitude"
        fit="true"
        useCluster="true"
		longitude="longitude"
		mark-click="false"
		draggable="true"
		style="height: 400px; width: 100%">
        
        
	</div>
    </div>	
    
    

            <div class="pagination pull-right">
            
          <!--   <h4>Looking for HOTEL TYPES with FACILITIES having SERVICES</h4> -->
            <div class="seperator"></div><div class="seperator"></div>

<div class="pagination tac">
<button ng-click="showMap()" class="btn float-left "  ng-show="!mapVisible">Show map</button> &nbsp;
<button ng-click="showMap(true)" class="btn float-left "  ng-show="mapVisible">Hide map</button>&nbsp;
<ul>
<li ng-hide="pagination.page == 1 "><a href="" ng-click="changePage(pagination.prev || 1)"> << </a></li>
<li ng-repeat="b in pagination.pages" ng-class="{active:pagination.page == b}"><a href="" ng-click="changePage(b)">{{ b }}</a></li>
<li ng-hide="pagination.page == pagination.total"><a href="" ng-click="changePage(pagination.next)"> >> </a></li>
</ul>
</div>
<div class="clear"></div>
</div>
                 
                  <br />
			<div id="hotel-list">
			  <div class="finder"><h5>Finder Results ( {{items.count}} hotels found )</h5></div><!-- Todo Ascending -->
			  <div class="result-icons float-right"><a href="" title="sort by train distance" ng-click="order('train')"><img src="/images/site/train_search.png"/></a> <a href="" title="sort by airport distance" ng-click="order('plane')" style="margin-left:-4px;"><img src="/images/site/plane_search.png"/></a> <a href="" ng-click="order('port')" title="sort by port distance" style="margin-left:-4px;"><img src="/images/site/port_search.png"/></a> <a href="" ng-click="order('price')" title="sort by price" style="margin-left:-4px;"><img src="/images/site/price_search.png"/></a><span class="label label-success spacer-left asceding ">Asceding</span></div>
			  <br class="clear_0"/>
				<div class="error" ng-show="error=='noDestination'"><h3>No destination selected</h3></div>
                {{predicate}}
			  <div class="hotel-rslt" ng-repeat="a in res| orderBy:'order':false">
				  <img src="{{a.media.big_thumb}}" alt=""  height="100" class="float-left spacer-right" title=""/>
				  <div class="finder-info spacer-left float-left">
				<a href="/{/literal}{$FRONT_LANG}{literal}/hotel/information/{{a.id}}/{{a.permalink}}.html?{{hotelUrl}}" class="main-search-title">  <span class="finder-title float-left" ng-bind-html-unsafe="a.title"></span></a>
				  <div class="seperator"></div>
                  
                  
				  <div class="float-right"><a href="/{/literal}{$FRONT_LANG}{literal}/hotel/information/{{a.id}}/{{a.permalink}}.html?{{hotelUrl}}" class="btn btn-info btn-small spacer-right">view</a><a href="" class="btn btn-success btn-small spacer-right" ng-click="miniMap(a.id)">Show on map</a> 
                  <a href="/{/literal}{$FRONT_LANG}{literal}/reservation/index.html?{{hotelUrl}}{literal}&hotelid={{a.id}}{/literal}" class="btn btn-warning btn-small">Reservation</a>
                  </div>
                  <span class="finder-stars float-left"><span class="stars stars{{a.stars}}"></span></span>
				 <div class="seperator-small"></div>
				  <span class="reserv-info">{{roomsNum.length}} rooms, {{personsTotal}} persons, {{a.nights}} Nights, {{filters.arrival}} - {{filters.departure}} , <span class="hotel-price-results">{{a.price}} {/literal}{$current_currency.symbol}{literal} </span></span>
                  
				  </div>

				  <div class="float-right reserv">
				  
				  <br class="seperator"/>
				  <input type="checkbox" id="nearby" name="nearby"  ng-click="nearBy(a.id)"  ng-checked="filters.nearby==a.id" />
		           <label for="nearby"><span></span>Hotels Nearby</label> <span ng-show="a.distance" class="label label-info" >{{a.distance}}</span>
				  </div>
				  <br class="clear_0"/>
                  <div ng-show="selectedItem==a.id">
                             	<div class="show-mini-map" 
                                id="{{a.id}}"
                                center="center"
                                zoom="zoom" 
                                markers="markers"
                                latitude="latitude"
                                fit="true"
                                useCluster="true"
                                longitude="longitude"
                                mark-click="false"
                                draggable="true"
                                style="height: 380px; width: 100%">
                  </div>
			  </div>
			  
			 
			 
			  
			</div><!-- end hotel-list -->
            <div id="moreResults" ><a href="" ng-click="loadMore()" ng-show="pagination.total > filters.page">Load more results</a></div>
            <div class="seperator"></div><div class="seperator"></div>
{/literal}
			
		  </div>
</div>
</div><!-- END WRAP -->
</div>
</div>
