<div id="file_upload">
    <form action="/modules/upload.php?module={$CURRENT_MODULE.name}" method="POST" enctype="multipart/form-data">
        <input type="file" name="file[]" multiple>
    <input type="hidden" name="module" value="listings" class="SaveData" />
    <input type="hidden" name="action" value="Save{$media}" class="SaveData" />
    <input type="hidden" name="itemid" value="{$item.id}" class="SaveData" />
    <input type="hidden" name="return" value="Thumb" class="SaveData" />
    <input type="hidden" name="ImageCategory" value="{$type}" class="SaveData" />
        <button type="submit">{$lang.upload}</button>
        <div class="file_upload_label">{$lang.lbl_uploadFiles}</div>
       
    </form>
    <table class="files" id="uploadFiles">
        <tr class="file_upload_template" style="display:none;">
            <td class="file_upload_preview"></td>
            <td class="file_name"></td>
            <td class="file_size"></td>
            <td class="file_upload_progress"><div></div></td>
            <td class="file_upload_start"><button>{$lang.lbl_start}</button></td>
            <td class="file_upload_cancel"><button>{$lang.lbl_cancel}</button></td>
        </tr>
    </table>
    <table id="download_files"  >
            <tr class="file_download_template" style="display:none;">
            <td class="file_download_preview"></td>
            <td class="file_name"><a></a></td>
            <td class="file_size"></td>
            <td class="file_download_delete" colspan="3"><button>Delete</button></td>
        </tr>
    </table>
         <h2>{$lang.options}</h2>
<label><input type="checkbox" value="1" name="CustomImageSet"  class="SaveData ShowImageCopies" alt="uploadifyMulti"  /> Επιλογή Αντιγράφων</label>
<ul class="hidden" id="ImageSet">
{foreach from=$image_types item=a}
<li><label><input type="checkbox" name="ImageSet{$a.id}" value="{$a.id}" class="SaveData ImageSet" {if $a.required}disabled{/if}  alt="uploadifyMulti"  /> {$a.title} ({$a.width}x{$a.height}px)</label> {if $a.required}<span class="red">*</span>{/if}</li>
{/foreach}
<li>Τα πεδιία με <span class="red">*</span> είναι υποχρεωτικά</li>
</ul>
<p>Τρόπος δημιουργίας αντιγράφων :
</p><ul class="ImageResizeSettings">
<li><label><input type="radio" name="ImageFix" value="ImageBox" class="SaveData" {if $loaded_modules.listings.settings.imageFix eq "ImageBox"}checked="checked"{/if}/> 
  <strong>Image Box</strong> - Γεμίζει με κενό τον καμβά ώστε να διατηρηθεί το μέγεθος που θέλουμε</label></li>
<li><label><input type="radio" name="ImageFix" value="ByWidth" class="SaveData" {if $loaded_modules.listings.settings.imageFix eq "ByWidth"}checked="checked"{/if} /> 
  <strong>By Width</strong> - Διατηρεί σταθερό το πλάτος. Το ύψος μεταβάλεται</label></li>
<li><label><input type="radio" name="ImageFix" value="ByWHeight" class="SaveData" {if $loaded_modules.listings.settings.imageFix eq "ByWHeight"}checked="checked"{/if} /> 
  <strong>By Height</strong> - Διατηρεί σταθερό το ύψος. Το πλάτος μεταβάλεται</label></li>
<li><label><input type="radio" name="ImageFix" value="ByWidthAndHeight" class="SaveData" {if $loaded_modules.listings.settings.imageFix eq "ByWidthAndHeight"}checked="checked"{/if} /> 
  <strong>By Both</strong> - Διατηρεί σταθερό το ύψος και το πλάτος. Πιθανότητα παραμόρφοσης</label></li>
</ul>
<div class="seperator"></div>
    <div class="file_upload_overall_progress"><div style="display:none;"></div></div>
    <div class="file_upload_buttons">
        <button class="file_upload_start">{$lang.lbl_startAll}</button> 
        <button class="file_upload_cancel">{$lang.lbl_cancelAll}</button> 
        <button class="file_download_delete">{$lang.lbl_deleteAll}</button>
    </div>
</div><!-- END UPLOADER -->

{if $media eq "image" AND $load}
<table width="100%" border="0" cellspacing="5" cellpadding="5" class="widefat" id="ImagesTable">
      <tr class="nodrop nodrag">
        <td colspan="8">Με τα επιλεγμένα  (<span id="messages">0 επιλέχθηκαν</span> ): <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ActivateChecked">Ενεργοποίηση</option>
        <option value="DeactivateChecked">Απενεργοποίηση</option>
        <option value="DeleteChecked">Διαγραφή</option>
        <option value="SavePrefs">Αποθήκευση προτιμήσεων</option>
        </select>
         <input type="button" class="button PerformActionMedia" value="Εκτέλεση" />

        </td>
      </tr>
  <tr class="nodrop nodrag">
    <th width="16">&nbsp;<input type="hidden" name="TableOrder" class="settings" value="" /></th>
    <th width="120">&nbsp;</th>
    <th width="16"><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /></th>
    <th width="145" scope="col">{$lang.availability}</th>
    <th width="150" scope="col">{$lang.category}</th>
    <th scope="col">{$lang.alt}</th>
    <th scope="col">{$lang.title}</th>
    <th>&nbsp;</th>
  </tr>
  {foreach from=$images item=a}
  <tr id="Copies{$a.id}">
    <td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>
    <td><a href="{$a.main}" rel="pick"><img src="{$a.thumb}" width="60" height="60" /></a></td>
    <td><input name="id" type="checkbox" id="check_values" value="{$a.id}" class="check-values" /></td>
    <td>{if $a.available eq 1}<font color="#336600">{$lang.yes}</font>{else}<font color="#FF0000">{$lang.no}</font>{/if}</td>
    <td width="150"><select name="type-{$a.id}" id="type-{$a.id}" class="settings">
{section name=b loop=$image_categories}
<option value="{$image_categories[b].id}" {if $image_categories[b].id eq $a.type}selected{/if}>{$image_categories[b].title}</option>
{/section}
</select></td>
    <td width="150"><input type="text" value="{$a.alt}" name="alt-{$a.id}" class="settings" /></td>
    <td width="150"><input type="text" value="{$a.title}" name="title-{$a.id}" class="settings" /></td>
    <td><a href="content_modify_img.php?id={$item.id}&media={$media}&type={$type}&imageid={$a.id}&action=EditImage">Τροποποίηση</a> - <a href="#TB_inline?height=400&width=800&inlineId=CopiesTable{$a.id}&modal=true" rel="CopiesTable{$a.id}" class="thickbox">Αντίγραφα</a></td>
  </tr>
  {/foreach}
</table>

{/if}

<input type="hidden" name="filesAllowed" value="{$filesAllowed}" />
<link rel="stylesheet" href="/css/jquery.fileupload-ui.css">
<script src="/scripts/jquery.fileupload.js"></script>
<script src="/scripts/jquery.fileupload-ui.js"></script>
<script src="/scripts/jquery.fileupload-uix.js"></script>
<script src="/scripts/fileUploader.js"></script>