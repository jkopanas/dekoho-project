{if {$smarty.get.tableID}==""}

{include file="modules/common_categories/create_tables.tpl"}


	{else}
	
<input type="hidden" value="{$smarty.get.tableID}" name="table_id" />

<div class="dp50">
   <div class="box_c">
           <h3 class="box_c_heading cf">
	           <span class="fl spacer-left">{$lang.categories} <a class="spacer-left k-edit" href="javascript:void(0);">{$lang.add_category}</a> </span></h3> 
	        <div class="box_c_content cf">   
					<div id="treeview"></div>
				</div>
    	</div>  
 </div>


		<div id="edit_form"></div> 

      	<script id="id-template" type="text/kendo-ui-template">	
			# if (item.sub_cat==1) { #	
					<img width="16" height="16" class="spacer-right dragHandle" src="/images/admin/move-arrow.png">	
					<span id='list#= item.id #' class="clicked k-icon k-plus pointer" ></span> 
			 # } else { #
				<img width="16" height="16" class="spacer-right dragHandle" src="/images/admin/move-arrow.png">
				 <span id='list#= item.id #' class=""></span> 
				
				 # } #
           <span id='item#= item.id #' class="new_item pointer"> #= item.text #</span><span id='edit#= item.id #' class="spacer-left k-icon k-edit pointer" style="margin-top:5px;"> </span><span id='delete#= item.id #' class="spacer-left  k-icon k-delete delete pointer" style="margin-top:5px;"></span><span id='link#= item.id #' class="spacer-left k-icon k-link feature pointer" style="margin-top:5px;"></span>      
          </script>
        
 		
 		<script id="delete-confirmation" type="text/x-kendo-template">
			<p class="delete-message" >{$lang.confirm_deletion}</p> <br/>
			<button class="delete-confirm k-button" data-id="#= item.id #" data-type="#= item.type #">{$lang.delete}</button>
			<a href="javascript:void(0);" class="float-right spacer-right delete-cancel">{$lang.lbl_cancel}</a>
		</script>
		
 <style scoped>
 		.k-state-selected
		{
 			background-color:  #FFF; !important;
 			color: #000000;
 			border-color:#FFF;
}
</style>
 		<div id="OpenLangWnd"></div>
 	
          
        <script type="text/javascript">
             head.js('/scripts/admin2/tree.js');
             head.js('/scripts/admin2/LanguagesBox.js');
		     head.js('/scripts/admin2/categories/categories.js');
		     head.js('/scripts/admin2/categories/editcategories.js');
		     
		  </script>
 {/if}