{include file="modules/products/admin/menu.tpl"}
{if $action}
<form name="products_form" method="post">
{include file="modules/products/admin/display_products_table.tpl" products=$products type="editor" form_name="products_form" page="mass_insert.php"}
</form>
{else}
<div class="wrap">
<div id="zeitgeist">
  <h3>{$lang.main_cat}</h3>
  <form name="cat_form" id="cat_form" method="post">
<select name="categoryid">         
{section name=cat_num loop=$allcategories}
<option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $product.catid}selected{/if}>{$allcategories[cat_num].category}</option>
{/section}
</select>
</form>
</div>
<h2>{$lang.mass_insert_products}</h2>
<h3>{$lang.mass_product_insert_help}</h3>
<div>
{include file="common/uploader.tpl" mode="mass_products" form="cat_form"}
</div>
<div style="clear:both"></div>
</div>
{/if}