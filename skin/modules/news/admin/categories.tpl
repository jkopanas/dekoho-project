{include file="modules/news/admin/menu.tpl"}
{if !$edit}
<div class="wrap">

<h2><a href="{$MODULE_FOLDER}/categories.php">{$lang.categories}</a> (<a href="#addcat">add new</a>) {include file="modules/news/nav_categories.tpl" management=$lang.categories target="admin_categories"}</h2>
<table cellpadding="3" cellspacing="3" width="100%">
<tr>
		<th scope="col">ID</th>
        <th scope="col">{$lang.title}</th>
        <th scope="col">{$lang.description}</th>
        <th scope="col">#{$lang.subcategories}</th>
        <th scope="col"># {$lang.news}</th>
        <th colspan="3">Action</th>
	</tr>
{section name=a loop=$cat}
<tr class="{cycle values='alternate,'}"><th scope="row">{$cat[a].categoryid}</th><td> <a href="{$MODULE_FOLDER}/categories.php?cat={$cat[a].categoryid}">{$cat[a].category}</a></td>
				<td>{$cat[a].description}</td>
				<td>{$cat[a].num_sub|default:$lang.txt_not_available}</td>
				<td align="center"><A href="category_news.php?cat={$cat[a].categoryid}" class="ItemsList">{$cat[a].num_news|default:0}</A></td>
	            <td><a href="{$MODULE_FOLDER}/news_new.php?cat={$cat[a].categoryid}" class="edit">{$lang.add}</a></td>
      <td><a href="{$MODULE_FOLDER}/categories.php?action=edit&amp;cat={$cat[a].categoryid}" class="edit">Edit</a></td><td><a href="{$MODULE_FOLDER}/categories.php?action=delete&amp;cat={$cat[a].categoryid}" onclick="return confirm('You are about to delete the category \'{$cat[a].category}\'.  All of its posts will go to the default category.\n  \'OK\' to delete, \'Cancel\' to stop.')" class="delete">Delete</a></td>
				</tr>
{/section}
                </table>
  <input type="hidden" name="featured" id="featured" value="1" />
</div> 

 <div class="wrap">
 
 <h2>{$lang.featured_news} (<a href="#featured_add">add new</a>)</h2>
 <table width="100%" cellpadding="4" cellspacing="1" id="FeaturedTable">

  <tr>
    <td valign="top">
<div>
<a name="featured_add" id="featured_add"></a>
<form name="search_cat" id="search_cat">
<SELECT name="cat" size="10" style="width: 100%" onChange="submitGetCategory('search_cat','cat_results','cat');return false;">
{section name=cat_idx loop=$allcategories}
<OPTION value="{$allcategories[cat_idx].categoryid}"{if $smarty.get.cat eq $allcategories[cat_idx].categoryid} selected{/if}>{$allcategories[cat_idx].category_path}{$allcategories[cat_idx].category}</OPTION>
{/section}
</SELECT>
</form>
</div></td>
    <td><div id="cat_results" style="border:#000000 solid 1px"></div></td>
  </tr>
 </table>
 <br />
 <div id="related_products">{include file="modules/news/admin/related_news_table.tpl" links=$featured}</div>
</div>

{/if}   
<form name="addcat" id="addcat" action="{$MODULE_FOLDER}/categories.php" method="post">               
          <div class="wrap">
          <div id="zeitgeist">
  <h3>{$lang.general_settings}</h3>
  <div><img src="{if $category.settings.image}{$category.settings.image}{else}{$news_IMAGES}{$DEFAULT_THUMB}{/if}" name="preview" id="preview"  border="1"  style="border:#000000 1px solid" width="120">
  <br />
  <input type="hidden" name="settings_image" id="settings_image" value="{$category.settings.image}" />
  <button type="button" {literal}onClick="OpenFileBrowser('image', function(url) {document.addcat.settings_image.value=url;
document.getElementById('preview').src=document.getElementById('settings_image').value;
}, function() {return document.addcat.settings_image.value;} )">Choose Image...{/literal}</button>

{if $more_categories}
<h3>More Categories</h3>
{section name=a loop=$more_categories}
{if $smarty.get.cat eq $more_categories[a].categoryid}<strong>{$more_categories[a].category}</strong>{else}<a href="{$MODULE_FOLDER}/categories.php?action=edit&cat={$more_categories[a].categoryid}">{$more_categories[a].category}</a>{/if}<br />
{/section}
{/if}
</div>

</div>
    <h2>{if $edit eq 1}{$lang.modify} {$category.category} -> <a href="{$MODULE_FOLDER}/categories.php">{$lang.categories}</a>   {include file="modules/products/nav_categories.tpl" management=$lang.categories target="admin_categories"} {else}Add New Category{/if}</h2>
    
         
      <p>{$lang.pos}:<br>
        <input name="orderby" type="text" class="editform" style="background-color: rgb(255, 255, 160);" value="{$category.order_by}" size="4" id="orderby">
         </p>
        <p>Name:<br>
        <input name="title" type="text" style="background-color: rgb(255, 255, 160);" value="{$category.category}">
        </p>
        <p>Category parent:<br>
          <select name="parent_catid" class="postform" id="parent_catid">
        <option value="0">{$lang.root_category}</option>    
{section name=cat_num loop=$allcategories}
<option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $parentid}selected{elseif $allcategories[cat_num].categoryid eq $catid AND !$smarty.get.action}selected{/if}>{$allcategories[cat_num].category}</option>
{/section}
</select></p>
        <p>Description: (optional) <br>
        <textarea name="desc" rows="5" cols="100"  id="desc">{$category.description}</textarea>
        </p>
      <p>Meta Keywords: (optional) <br>
      <textarea name="meta_keyw" rows="2" cols="50" id="meta_keyw">{$category.meta_keywords}</textarea>
      </p>
        <p>Meta Description: (optional) <br>
      <textarea name="meta_desc" rows="2" cols="50" id="meta_desc">{$category.meta_descr}</textarea>
      </p>
        <p  align="left"><input name="action" value="{$action|default:'addcat'}" type="hidden"><input type="hidden" name="cat" value="{$catid|default:$smarty.get.cat}" /><input name="submit" type="submit" class="button" value="{$lang.save} �">
        </p>
    
</div>      
</form>