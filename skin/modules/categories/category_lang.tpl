<form name="singleVar" id="singleVar" class="formEl_a padding">
<h2>Translations</h2>
<table class="table_a spacer-top" >
<tr class="TableHead">
  <th width="50"><center>{$lang.language}</center></th>
      <th ><center>{$lang.category}</center></th>
      <th ><center>{$lang.description}</center></th>
  </tr>
	{foreach from=$LangTranslate item=a}
	<tr>
  		<td class="vam tac" ><strong>{$a.code}</strong></td>
  		<td style="width:150px;"><textarea id="category-{$a.code}" cols="10" rows="5"  class="SaveTranslate" name="category-{$a.code}" data-code="{$a.code}" data-field="category" required validationMessage="{$lang.not_left_empty}">{$a.value.category}</textarea>
      		<br>
      		<a href="#" class="ckEditor" data-height="150" rel="category-{$a.code}">{$lang.advanced_editor}</a>
  		</td>
  		<td style="width:150px;"><textarea id="description-{$a.code}" cols="10" rows="5"  class="SaveTranslate" name="description-{$a.code}" data-code="{$a.code}" data-field="description" required validationMessage="{$lang.not_left_empty}">{$a.value.description}</textarea>
      		<br>
      		<a href="#" class="ckEditor" data-height="150" rel="description-{$a.code}">{$lang.advanced_editor}</a>
  		</td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="3" ><input type="button" class="btn btn_a saveSingleTranslation float-right" data-id="{$a.uid}" data-table="{$a.table}" data-action="{$action}" value="{$lang.save}" /></td>
	</tr>
</table>
</form>