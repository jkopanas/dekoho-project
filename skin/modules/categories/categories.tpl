{foreach from=$boxL  item=a name=b}
 	{include file=$a.template}
{/foreach}
                    
<div class="dp50">
    <div class="box_c">
           <h3 class="box_c_heading cf">
	           <span class="fl">{$lang.add_featured_items} : &nbsp;</span> <span id="f_name" class="fl"> <a href="#" id="link0" class="feature"></span><img src="/skin/images/admin2/home_cat.png" width="24" style="float:right; margin-top:1px;" /></a></h3> 
       	
					    <div class="box_c_content cf">
					   		  {foreach from=$boxH item=a name=b}
  						  			<div id="{$a.name}" class="boxItem" data-boxid="{$a.id}"  data-dataType="{$a.settings.dataType}" data-trigger="{$a.settings.trigger}"></div>		  			
  							  {/foreach}  
  							  
					    <div id="tabStrip" class="tabStrip">
  						  <ul>
    						{foreach from=$boxR item=a name=b}
    						   <li class="{if $smarty.foreach.b.first}k-state-active{/if} boxItem" data-boxid="{$a.id}"  data-dataType="{$a.settings.dataType}" data-trigger="{$a.settings.trigger}">{$lang[$a.title]|default:$a.title}</li>
							{/foreach}
  							  </ul>
  					  		{foreach from=$boxR item=a name=b}
  							  <div>
  						  	  <div id="{$a.name}" class="padding"><img src="/images/ajax-loader.gif" /></div>
  							  </div><!-- END TAB -->
  					  		{/foreach}  
					</div><!-- END TABS -->
		
				</div>
					
    	</div>  
 </div>
 		
  <div class="dp50 float-right">
    <div class="box_c">
				<h3 class="box_c_heading cf">  <span class="fl">{$lang.featured_at_category} :&nbsp;</span> <span id="f_name_f" class="fl"> </span></h3>
				 <div class="box_c_content cf">
				<div id="treeview-featured"></div>
				
				</div>
			</div>
	</div>
 		
 		  <script id="f-template" type="text/kendo-ui-template">	
					<img width="16" height="16" class="spacer-right dragHandle" src="/images/admin/move-arrow.png">
			 		<span id='item_f#=item.id #' data-type='#=item.type#' class="new_item parent_item_f#=item.type#" >  #= item.text # </span><span id='delete_f#= item.id #' data-id="#= item.id #" data-type="#= item.type #" style="margin-top:5px;" class=" spacer-left k-icon k-delete k-delete-f pointer"></span>
          </script>
          
          		<script id="id-commands-gridview" class="id-commands-gridview" type="text/kendo-ui-template">
				<span  id='#= id #' data-itemid='#= id #' class="saveGrid k-icon k-add pointer" data-type="item"  data-module="#= module #" ></span>		
        </script>
        
        <script id="id-commands-gridview_cat" class="id-commands-gridview_cat" type="text/kendo-ui-template">
				</span><span id="#= id #" class="k-icon k-add k-add-cat pointer" data-type="cat" data-module="{$smarty.get.module}" ></span>  
        </script>
        
        <script type="text/x-kendo-template" id="template-text-gridview_cat">
			#= unescape(text) #
		</script>
 
         <script type="text/x-kendo-template" id="template-toolbar-PagesSearch">
 				<div class="float-left">{$lang.search}: <input type="text" class="k-input SearchText" data-eq="id" data-contains="title"  data-grid="gridview" placeholder=" {$lang.search} ID,text ..." /></div>
                <div data-id="#= id #" id="Filter-PagesSearch" class="k-button k-button-icontext Filter float-right">
                    Filter&nbsp;&nbsp;<span class=" k-icon k-filter"></span>
                </div>

          </script>
          
             <script type="text/x-kendo-template" id="template-active-gridview">
			# if (active != 0) { # <span class="fl notification ok_bg">#= $.lang.yes #</span>  # } else { # <span class="fl notification error_bg">#= $.lang.no #</span> # } #
		</script>
          
           <script type="text/x-kendo-template" id="template-toolbar-gridview_cat">	
               <div class="dp100">{$lang.search}: <input type="text" class="k-input SearchText"data-eq="categoryid" data-contains="category" data-grid="gridview_cat"  placeholder=" {$lang.search} ID,text ..." /></div>
          </script>
         

 		<script type="text/javascript">
 		   head.js('/scripts/admin2/grid.js');
 		   head.js('/scripts/admin2/categories/featured_categories.js');
 		  head.js('/scripts/admin2/categories/filterCategories.js');
			</script>