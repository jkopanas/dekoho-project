<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;hl=en&amp;key={$GOOGLE_MAPS_API_KEY}" type="text/javascript"></script>
<script type="text/javascript"> var adv_gmarkers = []; var gmarkers = [];</script>

,
{if $mode eq "display_map"}
 <script type="text/javascript">
{literal}
var startZoom = {/literal}{$map.zoom_level|default:$DEFAULT_MAP_ZOOM_LEVEL}{literal};

var map;



function init()
{
    if (GBrowserIsCompatible()) {	
        map = new GMap2(document.getElementById("map_{/literal}{$map_id}{literal}"));
		
		{/literal}
		{foreach from=$map_controls item=control}
		 map.addControl(new {$control}());
		{/foreach}
       {literal}
	   
	   
        var location = new GLatLng({/literal}{$map.coords_x|default:$DEFAULT_MAP_COORDS_X},{$map.coords_y|default:$DEFAULT_MAP_COORDS_Y}{literal});//SET THE MAP TO THE DEFAULT LOCAITON
        map.setCenter(location, startZoom);
		map.enableDoubleClickZoom();
		
		//ADD MARKER
		var point = new GLatLng({/literal}{$main_marker.coords_x|default:$map.coords_x},{$main_marker.coords_y|default:$map.coords_y}{literal});//SET THE MARKER TO THE DEFAULT LOCAITON
              var marker = new GMarker(point, {draggable: false});
              map.addOverlay(marker);
			  html = '{/literal}<strong>{$main_marker.title}</strong><p>{$main_marker.content}</p></div>{literal}';
              marker.openInfoWindowHtml('<div><div style="overflow: scroll; width: 271px; height: 150px; text-align:left;" id="main_marker">'+ html + '</div></div>');
			  
			 GEvent.addListener(marker, "click", function() {
			  map.setCenter(point, {/literal}{$main_marker.zoom_level|default:$map.zoom_level}{literal});
		  	marker.openInfoWindowHtml('<div><div style="overflow: scroll; width: 271px; height: 150px;" id="main_marker">'+ html + '</div></div>');
          });
		
		{/literal}
{if $advertisers}
{include file="advertisers_google_header.tpl"}
{/if}



	{section name=a loop=$map_links_list}

		{literal}
		

  var point_{/literal}{$map_links_list[a].id}{literal} = new GLatLng({/literal}{$map_links_list[a].coords_x},{$map_links_list[a].coords_y}{literal});
  var marker_{/literal}{$map_links_list[a].id}{literal} = new GMarker(point_{/literal}{$map_links_list[a].id}{literal})
  map.addOverlay(marker_{/literal}{$map_links_list[a].id}{literal});
  
  
              GEvent.addListener(marker_{/literal}{$map_links_list[a].id}{literal}, "click", function() {
			//location.href = {/literal}"{$URL}/view-image/{$images[a].id|crypt}/"{literal};
			marker_{/literal}{$map_links_list[a].id}{literal}.openInfoWindowHtml('{/literal}<div align="center"><a href="{$URL}/{if $country eq 1}destination{else}country{/if}/{$map_links_list[a].id}/" class="navpath" style="font-size:14px">{$map_links_list[a].title}</a></div>{literal}');
var location_{/literal}{$map_links_list[a].id}{literal} = new GLatLng({/literal}{$map_links_list[a].coords_x},{$map_links_list[a].coords_y}{literal});//SET THE MAP TO THE DEFAULT LOCAITON
map.setCenter({/literal}location_{$map_links_list[a].id},{$map_links_list[a].zoom_level|default:7}{literal});
          });
  
{/literal}
gmarkers[{$map_links_list[a].id}] = marker_{$map_links_list[a].id};
{/section}

		
		{literal}
		
    }
}

	  

window.onload = init;
window.onunload = GUnload;
{/literal}
</script>
{/if}
{literal}
<script type="text/javascript">
	  		      function advclick(i) {
        GEvent.trigger(adv_gmarkers[i],"click");
      }
	  		      function myclick(i) {

        GEvent.trigger(gmarkers[i],"click");
      }				
</script>
{/literal}