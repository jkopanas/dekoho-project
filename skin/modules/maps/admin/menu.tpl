<ul id="adminmenu2">
	<li><a href="{$MODULE_FOLDER}/" {if $submenu eq "main"}class="current"{/if}>{$lang.home}</a></li>
	<li><a href="{$MODULE_FOLDER}/products_new.php" {if $submenu eq "add"}class="current"{/if}>Add Map</a></li>
    <li><a href="{$MODULE_FOLDER}/modify_map.php" {if $submenu eq "modify"}class="current"{/if}>Modify Map</a></li>
    <li><a href="{$MODULE_FOLDER}/categories.php" {if $submenu eq "categories"}class="current"{/if}>Categories</a></li>
	<li><a href="{$MODULE_FOLDER}/settings.php" {if $submenu eq "settings"}class="current"{/if}>Module Settings</a></li>
</ul>