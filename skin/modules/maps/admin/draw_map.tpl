<markers>
{if $mode eq "item"}
{if $point}
<marker lat="{$point.map_x}" lng="{$point.map_y}" html="{$point.item.decription}"  label="{$point.location_title}" icontype="/images/maps/colour086.png"/>
{/if}
{elseif $mode eq "loc_cat"}
<marker lat="{$item.map_x}" lng="{$item.map_y}" html="{$item.title}"  label="{$item.title}" icontype="{$item.icontype}" mapZoom="{$item.mapZoom}" markerCategory="{$item.markerCategory}" URL="{$item.URL}" id="{$item.categoryid}" catid="{$item.categoryid}" map_id="{$item.mapid}" />
{elseif $mode eq "map" OR $mode eq "location"}
{foreach from=$items item=a name=b}
<marker lat="{$a.map_x}" lng="{$a.map_y}" html="{$a.title}"  label="{$a.title}" icontype="{$a.icontype}" selected="{$a.selected}" mapZoom="{$a.mapZoom}" markerCategory="{$a.markerCategory}" URL="{$a.URL}" id="{$a.itemid}" catid="{$a.catid}" map_id="{$a.mapid}" image="{$a.image}"/>
{/foreach}
{elseif $mode eq "map" OR $mode eq "product"}
{foreach from=$items item=a name=b}
<marker lat="{$a.map_x}" lng="{$a.map_y}" html="{$a.title}"  label="{$a.title}" icontype="{$a.icontype}" selected="{$a.selected}" mapZoom="{$a.mapZoom}" markerCategory="{$a.markerCategory}" URL="{$a.URL}" id="{$a.itemid}" catid="{$a.catid}" map_id="{$a.mapid}" image="{$a.image}"/>
{/foreach}
{elseif $mode eq "map" OR $mode eq "content"}
{foreach from=$items item=a name=b}
<marker lat="{$a.map_x}" lng="{$a.map_y}" html="{$a.title}"  label="{$a.title}" icontype="{$a.icontype}" selected="{$a.selected}" mapZoom="{$a.mapZoom}" markerCategory="{$a.markerCategory}" URL="{$a.URL}" id="{$a.itemid}" catid="{$a.catid}" map_id="{$a.mapid}" image="{$a.image_full.thumb}"/>
{/foreach}
{elseif $mode eq "missing_items"}
{foreach from=$items item=a name=b}
<marker title="{$a.title}" id="{$a.id}" catid="{$a.catid}"/>
{/foreach}
{else}

{/if}
</markers> 

