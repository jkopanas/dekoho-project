<div class="seperator"></div>

<div class="pager spacer-top">
{*Items {$list.from} to {$list.to} of*}
<p class="amount float-left"><strong>{$list.total} total records</strong></p>  
<div class="pages">

<ul class="float-right">
<li class="float-left"><strong>Page:</strong></li>
{foreach item=a from=$num_links} 
<li {if $a.page eq $PAGE}class="current float-left spacer-left"{else}class="float-left spacer-left SearchPage"{/if}>{if $a.page eq $PAGE}{$a.page}{else}<a href="{if $ajax}#{else}/{$FRONT_LANG}/{$smarty.get.module}/{$a.page}/{$smarty.get.uri}.html{/if}" title="{$a.category}" class="inactive {if $ajax}paginateFilter{/if}" rel="{$a.page}">{$a.page}</a>{/if}</li>
{/foreach}                                      
</ul>
<div class="clear"></div>
</div><!-- END PAGES -->
</div><!-- END PAGER -->