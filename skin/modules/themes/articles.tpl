<div class="wrap shiplist"> 
	<!--TOP LINKS -->
	<div class="top-links">
		<ul>
			<li><a href="/{$FRONT_LANG}/index.html">{$lang.home}</a></li>
			<li><a class="active" href="/{$FRONT_LANG}/category/{$cat.alias}/index.html">{$cat.category}</a></li>
		</ul>
	</div>
	<!--END TOP LINKS --> 
	<br class="clear" />
	{if $featured[0]['item']}
	<div class="headerTitle">
		<h2>{$featured[0]['item']['title']}</h2>
	</div>
	<div class="top-img">
		<div class="mainImg">
			<h1 style="width:390px;"><a><span>{$featured[0]['item']['description']|truncate:50}</span></a></h1>
			<img class="img" title="" alt="" src="{$featured[0]['item']['image_full']['slider']}" width="940" height="125"/> 
		</div>
	</div>
	{include file="social_networks.tpl"}
	{/if}
	<div class="main_column_inner"> 
		{foreach from=$items item=a name=dy}   
		{if $smarty.foreach.dy.iteration <= 2}
		<div class="top-ships" {if $smarty.foreach.dy.first} id="padd-top-ships" {/if}>
			<div class="blueHeader">
				<a href="/{$FRONT_LANG}/cruising/{$a.id}/{$a.permalink}.html">
					<h2>{$a.title}</h2>
				</a>
			</div>
			<div> 
			<a href="/{$FRONT_LANG}/cruising/{$a.id}/{$a.permalink}.html"> 
				<img src="{$a.image_full.category_thumb|default:'/images/site/cruise-package.jpg'}" alt="" title="" width="440" height="200" class="fiximage" /> 
			</a> 
			</div>
			<div class="ship-description">
				<span>{$a.description}</span>
				<div class="seperator"></div>
				<div><a class="button blue larger float-right" href="/{$FRONT_LANG}/cruising/{$a.id}/{$a.permalink}.html">{$lang.more}</a></div>
				<div class="seperator"></div>
			</div>
		</div>
		{/if}
		{/foreach} 
		<br class="clear" />
		{if count($items) gt 2}
		<div class="redHeader">
			<h2>{$lang.article_red_header_h2}</h2>
		</div>
		{/if}
		{assign var=i value=1}			
		{foreach from=$items item=a name=dy}
		{if $smarty.foreach.dy.iteration > 2}
		<div {if $i is div by 3} class="all-ships-list"{else}class="all-ships-list all-ships-list-padd"{/if}>
			<div class="blueHeader"><a  href="/{$FRONT_LANG}/cruising/{$a.id}/{$a.permalink}.html" title="{$a.title}">
				<h2>{$a.title|truncate:15}</h2>
				</a></div>
			<div class="allships-description"> <span>{$a.description|truncate:120} </span>
				<div><a class="button blue small float-right" href="/{$FRONT_LANG}/cruising/{$a.id}/{$a.permalink}.html">{$lang.more}</a></div>
				<div class="seperator"></div>
			</div>
		</div>
		{if $i is div by 3}<br class="clear"/>
		{/if}
		{$i=$i+1}
		{/if}
		{/foreach}
		<div class="seperator"></div>
		<div class="seperator"></div>
		<div class="seperator"></div>
	</div>
</div>