{capture name=sideBar}
<div class="block block-layered-nav">
                    <div class="block-title">
        <strong><span>Shop By</span></strong>
    </div>
    <div class="block-content">
                            <p class="block-subtitle">Shopping Options</p>
                            
      <dl id="narrow-by-list">
            {if $more_categories}
        <dt>Category</dt>
                    <dd>
<ol>
{foreach from=$more_categories item=a}
    <li>
                <a href="/{$FRONT_LANG}/products/{$a.alias}/index.html">{$a.category}</a>
                ({$a.num_items})
    </li>
    {/foreach}
</ol>
</dd>
{/if}


      </dl>

  </div>
</div>

 {/capture}{include file="modules/themes/sideBar.tpl" classes="spacer-right" content=$smarty.capture.sideBar}
{capture name=mainCol}
<div class="page-title category-title">
        <h1>{$cat.category}</h1>
</div>

{include file="modules/themes/toolBar.tpl"}

    	{include file="modules/themes/items.tpl" prefix="product" navPrefix=$prefix}
{include file="modules/themes/toolBar.tpl"}
{/capture}{include file="modules/themes/mainCol.tpl" classes="spacer-left" content=$smarty.capture.mainCol}

<div class="seperator"></div>
