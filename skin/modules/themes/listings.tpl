{capture name=sideBar}
<div class="box235 float-left spacer-right">
<h2 class="header-magenda">Filters</h2>
<div class="containerBlue arrowMagenda">


  <div id="appliedFilters" class="hidden">
  <h4>{$lang.txt_appliedFilters}</h4>
  <div><a href="#" class="resetEfields">{$lang.txt_reset}</a></div>
<ul id="filtersList">
</ul>
</div>


</div><!-- END CONTAINER -->
  <div id="extraFieldsFilters">

{include file="modules/themes/extraFieldsFilters.tpl"}
</div>

</div><!-- END BOX -->



{/capture}{include file="modules/themes/sideBar.tpl" spacer="right" content=$smarty.capture.sideBar}

{capture name=mainCol}
<div class="hidden"><div id="noItems">{$lang.lbl_noItemsFound}</div></div>
<div class="debugArea"></div>
<h1>{$cat.category}</h1>

<div class="clear">
<div class="float-left">{$lang.total} : <span class="itemsCount">{$list.total}</span>  {$lang.from} <span class="itemsFrom">{$list.from}</span> {$lang.to} <span class="itemsTo">{$list.to}</span></div>
<div class="float-right">{$lang.order_by} : 
<select class="mSelect sort" name="orderby" id="orderbyResults">
<option value="date_added">{$lang.date_added}</option>
<option value="title">{$lang.title}</option>
<option value="price-asc">{$lang.price} : low -> high</option>
<option value="price-desc">{$lang.price} : high -> low</option>
</select> </div>
<div class="clear"></div>
</div>
<div class="seperator"></div>
<div id="loadingMessage" class="hidden">{$lang.loading}<br /><img src="/images/ajax-loader.gif" /></div>
<div class="hidden" style="position:relative">
<a id="closeMap" class="btn btn_f closeMap" href="#">Close map</a>
<div id="map" style="width:100%; height:350px;"></div>
</div>
    
    	{include file="modules/themes/items.tpl" prefix="listing" navPrefix=$prefix}

{/capture}{include file="modules/themes/mainCol.tpl" spacer="right" content=$smarty.capture.mainCol}

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> 
<script>
head.js('/scripts/kendo/js/kendo.popup.min.js');
head.js('/scripts/kendo/js/kendo.list.min.js');
head.js('/scripts/kendo/js/kendo.panelbar.min.js');
head.js('/scripts/kendo/js/kendo.dropdownlist.min.js');
head.js('/scripts/kendo/js/kendo.data.min.js');
head.js('/scripts/kendo/js/kendo.fx.min.js');
head.js("/scripts/site/maps/gMaps3.js");
head.js('/scripts/kendo/js/kendo.draganddrop.min.js');
head.js('/scripts/kendo/js/kendo.slider.min.js');
head.js('/scripts/site/filters.js');
</script>
