<input type="hidden" name="posted_data" class="post" value="{$posted_data|json_encode|base64_encode}" />
<input type="hidden" name="module" class="post" value="{$current_module.name}" />
<input type="hidden" name="prefix" class="post" value="{$prefix}" />
<input type="hidden" name="filter"  value="{$filter}" />
<ol class="products-list" id="products-list">
{foreach from=$items item=a name=b}
<li class="item">
<a href="/{$FRONT_LANG}/{$prefix|default:"pages"}/{$a.id}/{$a.permalink}.html" title="{$a.title}" class="product-image"><img src="{$a.image_full.big_thumb}" alt="{$a.title}" width="155" height="155" class="itemImg" title="{$a.title}" /></a>
<div class="product-shop">
<div class="f-fix">
<h2 class="product-name"><a href="/{$FRONT_LANG}/{$prefix|default:"pages"}/{$a.id}/{$a.permalink}.html" title="{$a.title}">{$a.title}</a></h2>
<div class="price-box">
<span class="regular-price" id="product-price-15"><span class="price"><strong>&euro;{$a.eshop.price|formatprice}</strong></span></span>
                        
        </div>
<p><button type="button" title="Add to Cart" class="button btn-cart addToCart" rel="{$a.id}"><span><span>Add to Cart</span></span></button></p>
<div>
 {$a.description}                        


</div>
                </div>
            </div>
            <div class="clear"></div>
        </li><!-- END ITEM -->
{/foreach}
</ol>
