
<div class="seperator"></div>

<div class="page-title category-title">
<h1>Αρχεία</h1>
</div>
{foreach from=$docs item=a}
<div class="brochure"><!-- brochure section -->
        	<a href="{$a.file_url}" title="{$a.title}" target="_blank" ><img src="/images/site/pdf_icon.png" alt="{$a.title}" width="43" height="53" align="left" class="spacer-right" /></a>
            <div class="brochure-title">
                <h3>{$a.title}</h3>
                <p class="header-desc">{$a.original_file} - {$a.type} {$a.FileSize}</p>
      </div>   
    </div>
<div class="seperator"></div>
{/foreach}
