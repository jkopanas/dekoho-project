<div class="hidden">
<div id="requestDiv">{include file="modules/themes/requestForm.tpl"}</div>
</div>
<div id="listing">
<h1>{$item.title}</h1>
    <div class="seperator"></div>
<div class="box235 float-left spacer-right">
<h2 class="header-brown">Quick facts</h2>
<div class="containerBrown arrowBrown">
<ul class="contentList">
{foreach from=$item.efields item=x}          
{if $x.settings.type eq "info"}<li><span>{$x.field}:</span> <strong>{$x.value}</strong></li>{/if}
{/foreach}
</ul>
<div class="seperator"></div>
<div class="spacer-top spacer-bottom spacer-left"><a id="contactForm" class="btn btn_f" href="#requestForm"><span class="btnImgR" style="background-image: url('/images/buttons/goIcon.png');">Request info</span></a></div>
</div>
</div><!-- END INFO -->
<div class="box755 float-left">
<div style="position:relative;">

<div class="clear"></div>
<div class="hidden tabber" id="mapContainer">
<input type="hidden" name="geocoderAddress" value="{$mapItem.geocoderAddress}" class="mapItem" />
<input type="hidden" name="lat" class="mapItem" value="{$mapItem.lat}" />
<input type="hidden" name="lng" class="mapItem" value="{$mapItem.lng}" />
<input type="hidden" name="zoomLevel" class="mapItem" value="{$mapItem.zoomLevel}" />
<input type="hidden" name="MapTypeId" class="mapItem" value="{$mapItem.MapTypeId}" />
<input type="hidden" name="mapItemEncoded" id="mapItemEncoded" value='{$mapItemEncoded}' />
<input type="hidden" name="moreItems" id="moreItems" value='{$moreItems}' />
<div id="mapItem" style="width:100%; height:325px;">aaaa</div></div>
{foreach from=$item.detailed_images item=b key=k}
{if $k ne "thumbs"}
<div id="gallery-{$k}" class="camera_wrap camera_azure_skin tabber">
{foreach from=$b item=a}
            <div data-thumb="{$a.thumb}" data-src="{$a.slider}">
            {if $a.title}
                <div class="camera_caption fadeFromLeft">
                    {$a.title}
                </div>
                {/if}
                </div>
{/foreach}
</div>
{/if}
{/foreach}
</div>



</div><!-- END RIGHT -->
<div class="seperator"></div>
<div class="box235 float-left spacer-right">
{include file="modules/themes/newsLetter.tpl"}
<div class="seperator"></div>
{include file="modules/themes/liveChat.tpl"}
<div class="seperator"></div>
<h2 class="header-blue">{$lang.more}</h2>
<div class="containerBlue arrowBlue">
<ul>
{foreach from=$more_items item=a}
<li><a href="/{$FRONT_LANG}/listing/{$a.id}/{$a.permalink}.html" title="{$a.title}"><img src="{$a.image_full.big_thumb}" /></a><br /><a href="/{$FRONT_LANG}/listing/{$a.id}/{$a.permalink}.html" title="{$a.title}">{$a.title}</a></li>
<li class="itemSpacer"></li>
{/foreach}
</ul>
</div>
</div><!-- END LEFT -->
<div class="box755 float-left">

    <div style="position:relative">
    <span id="activeArrow"><img src="/images/site/arrows/header-arrow-brown.png" /></span>
    <ul id="tabStrip">
        <li><a href="#" rel="general" data-type="slider" data-target="gallery-general">General information</a></li>
        <li><a href="#" rel="localInfo" data-type="slider" data-target="gallery-localInfo">Local Info</a></li>
        <li><a href="#" rel="map" data-type="map" data-target="mapContainer">Map</a></li>
        <li><a href="#" rel="rates" data-type="slider" data-target="gallery-general">Rates</a></li>
        {foreach from=$efields item=a name=b key=k}
        {if $a.data}<li><a href="#" rel="tab-{$k}" data-type="slider" data-target="gallery-general">{$a.title}</a></li>{/if}
        {/foreach}
    </ul>
    <div id="tabsContainer" class="containerBrown">
    	<div class="tab" id="general">{$item.description_long}<div class="clear"></div></div><!-- END TAB -->
	    <div class="tab" id="localInfo">{$item.efields.localInfo.value}</div><!-- END TAB -->
        <div class="tab" id="map">na emfaniso me ali pineza ta more items sto xarti</div><!-- END TAB -->
        <div class="tab" id="rates"><div id="bookings">{include file="modules/bookings/type_`$itemType.uses|default:periods`.tpl"}</div></div><!-- END TAB -->
        {foreach from=$efields item=a name=b key=k}
        <div class="tab" id="tab-{$k}">
        <div id="eFields">
                <TABLE  border="0">
  <TR> 
{foreach from=$a.data item=b name=c}

      <TD valign="top"><table width=100% border="1" cellpadding=0 cellspacing=5 >
            <tr>
              <td>
                          <strong>{$b.field}</strong> : {if $b.settings.search}<a href="/{$FRONT_LANG}/filters/{$current_module.name}/{$b.var_name}/{if !$b.value|is_numeric}{$b.value|base64_encode}{else}{$b.value}{/if}/index.html">
            {if $b.type eq "radio"}{if $b.value eq 1}{$lang.yes}{else}{$lang.no}{/if}
            {else}
            {$b.value}{/if}</a>{else}{if $b.type eq "radio"}{if $b.value eq 1}<span class="green">{$lang.yes}</span>{else}<span class="red">{$lang.no}</span>{/if}{else}{$b.value}{/if}{/if}
              </td>
            </tr>
          </table></TD>
      
      {* see if we should go to the next row *} {if not ($smarty.foreach.c.iteration mod 3)} {if not $smarty.foreach.c.last} </TR>
  <TR> {/if} {/if} {if $smarty.foreach.c.last} {* pad the cells not yet created *} {math equation = "n - a % n" n=3 a=$b|@count assign="cells"}{if $cells ne 3} 
  {section name=pad loop=$cells}
    <TD>&nbsp;</TD>
    {/section} {/if} </TR>
  {/if} {/foreach}
</TABLE>
{*
            {foreach from=$a.data item=b}
            <strong>{$b.field}</strong> : {if $b.settings.search}<a href="/{$FRONT_LANG}/filters/{$current_module.name}/{$b.var_name}/{if !$b.value|is_numeric}{$b.value|base64_encode}{else}{$b.value}{/if}/index.html">
            {if $b.type eq "radio"}{if $b.value eq 1}{$lang.yes}{else}{$lang.no}{/if}
            {else}
            {$b.value}{/if}</a>{else}{if $b.type eq "radio"}{if $b.value eq 1}<span class="green">{$lang.yes}</span>{else}<span class="red">{$lang.no}</span>{/if}{else}{$b.value}{/if}{/if}<br />
			<br />
            {/foreach}
            *}
            </div>
        </div><!-- END TAB -->
        {/foreach}
	</div>
    <div class="clear"></div>
    </div>
</div><!-- END RIGHT -->
<div class="seperator"></div>


<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> 
<script>
head.js("/scripts/fancybox/jquery.fancybox-1.3.4.pack.js?v=2.0.5");
head.js("/scripts/slider/camera.min.js");
head.js("/scripts/slider/jquery.easing.1.3.js");
head.js("/scripts/site/maps/gMaps3.js");
head.js("/scripts/site/listing.js");

</script>
<link href="/scripts/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet"/>
