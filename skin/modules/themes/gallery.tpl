<div class="page-title category-title">
<h1>{$cat.category}</h1>
</div>
<div class="seperator"></div>
<ul class="products-grid first odd">
{foreach from=$items item=a name=b}
                    <li class="item {if $smarty.foreach.b.first}first{elseif $smarty.foreach.b.last}last{/if}">
			
			<div class="imagecontainers imagecontainers2" data-overlayid="overlayid-1" data-overlayid2="overlayid2-1" style="width: 190px; height: 80px; position: relative; overflow-x: hidden; overflow-y: hidden; ">														
                <a href="/{$FRONT_LANG}/cruising/{$a.id}/{$a.permalink}.html" title="{$a.title}" class="product-image"><img class="product_image" src="{$a.image_full.big_thumb}" width="175" alt="{$a.title}" title="{$a.title}"></a></div>
                <h2 class="product-name"><a href="/{$FRONT_LANG}/cruising/{$a.id}/{$a.permalink}.html" title="{$a.title}">{$a.title}</a></h2>
                                                                
                <div class="actions">                    
                                    </div>
            </li>
            {/foreach}
</ul>
<div class="seperator"></div>
