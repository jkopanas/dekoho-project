<div class="wrap">
<div class="seperator"></div>
<div class="seperator"></div>
<div class="left_column spacer-top">
<div class="themeList">
 <form name="searchform" id="searchform" class="searchform" method="post" action="/{$FRONT_LANG}/search/1/">
<h3>Search for:</h3><br/>
<input type="text" name="title" id="title" value="{$needle}" class="SearchText blue title-search field" placeholder="{$lang.enter_your_search}">
<input type="hidden" id="description" name="description" class="field description-search" value="{$needle}" />
<div class="seperator"></div>
<div class="seperator"></div>
<div class="float-left spacer-top"><h3>Search within:</h3></div>
<div class="float-left spacer-top spacer-left"><input type="radio" value="search" name="" id="All" class="search-module" {if $smarty.get.module == "search"}checked{/if}>&nbsp;All<br/></div>
<div class="float-left spacer-top spacer-left"><input type="radio" value="search-content" class="search-module" id="content" {if $smarty.get.module == "search-content"}checked{/if}>&nbsp;Content<br/></div>
<div class="float-left spacer-top spacer-left"><input type="radio" value="search-itinerary" class="search-module" id="itinerary" {if $smarty.get.module == "search-itinerary"}checked{/if}>&nbsp;Itinerary<br/></div>
<div class="float-left spacer-top spacer-left"><input type="radio" value="search-ports" class="search-module" id="ports" {if $smarty.get.module == "search-ports"}checked{/if}>&nbsp;Ports<br/></div>
<div class="float-left spacer-top spacer-left"><input type="radio" value="search-components" class="search-module" id="components" {if $smarty.get.module == "search-components"}checked{/if}>&nbsp;Excursions<br/></div>
<div class="float-right"><a href="#" class="next button blue larger float-right spacer-right Search-btn">SEARCH</a></div>
<input type="hidden" name="pageSize" value="10" />
<input type="hidden" name="logic" value="or" />
<input type="hidden" name="operator" value="LIKE" />
</form>
<div class="seperator"></div>
{if  $list.total gt $items|@count}
				{include file="modules/themes/pagination.tpl" prefix=$navPrefix|default:"content" ajax=$ajax}
				{/if}
</div>
<div id="results" >
<div class="SearchList">
{foreach from=$items item=a key=k name=search}
{if $a.results[0]!= ""}
{foreach from=$a.results item=b name=searchItems}
{if $smarty.foreach.searchItems.first}<h2>{$k}</h2>{/if}
	<div class="block">
        <div class="content">
        <h3>{$b.title}</h3>
         {if $k == "content"} 
        	<p>{$b.description|truncate:140}<a href="/{$FRONT_LANG}/{$a.data.url}/{$b.id}/{$b.permalink}.html"><span> {$lang.read_more}</span></a></p>
         {else}
        	<p>{$b.description|truncate:140}<a href="/{$FRONT_LANG}/{$a.data.url}/{$b.key}/{$b.permalink}.html"><span> {$lang.read_more}</span></a></p>
         {/if}
        </div>
	</div>
	{/foreach}
{/if}
{/foreach}
<div class="seperator"></div>
</div>
<div class="seperator"></div>
</div>
<div class="seperator"></div>
</div>
 <div class="right_column">

<div class="seperator"></div>
<div class="seperator"></div>
 <div class="blueHeader-right offer-block-right" id="max-wdt">
		<h2>Contact Us</h2>
		</div>
		 <div class="destinations">
 		<div class="block-5 img-box1">
		<div class="right-block-bottom">
			<div class="left-strip">
				<span>You can use our helpdesk for all answers, use our contact form or call us at</span>
				<span id="phone-main">+30 210 4583400</span>
			</div>
			
		</div>
 </div>
 </div>

</div>
<div class="seperator"></div>
</div>
<script id="resultsTemplate" type="text/x-kendo-template">

</script>
<script>
		head.js('/scripts/site/ModuleSearch.js');
</script>