<div class="dp100">
	<div class="box_c" />
		<div class="padding wrap">
			<h3 class="box_c_heading cf">
				Payment Methods
			</h3>
			<div class="debug"></div>
			<div class="k-toolbar k-grid-toolbar  dp100">
		        <a class="k-button k-button-icontext k-add-button" ><span class="k-icon k-add"></span>Add new record</a>
		    </div>
			<div class=" dp100" id="resultsList">
			</div>
			<div class="k-pager-wrap dp100">
	        <div id="pager"></div>
	    </div>
		</div><!-- END WRAP -->
	</div>
</div>

{if count($jsIncludes) gt 0}
	{foreach from=$jsIncludes key=k item=v}
		<script type="text/javascript" src="{$v}"></script>
	{/foreach}
{/if}

<script type="text/x-kendo-template" id="gridTemplate">
{if isset($gridTemplate)}
	{$gridColumns}
{else}
	Title #= titleVar #
{/if}
</script>

{literal}
<script type="text/x-kendo-tmpl" id="listTemplate">
	<div class="list-box-view list-item-${id}">
		<span class="hidden" id="pmethod_id">${id}</span>
	   	 <dl>
		 <div  id="left-pm-details" >

		        <dt>Payment Method ID</dt>
		        <dd>
		        	<span id="pmethod_id">${id}</span>
		        </dd>
		        <dt>Title</dt>
		        <dd>${title}</dd>
		        <dt>Ενεργοποίηση</dt>
		        <dd> #if ( active > 0) { # Ενεργοποίημένο # } else { # Ανενεργό # } #</dd>
		        <dt>Υπερτίμηση</dt>
		        <dd>${surcharge} ${surcharge_type}</dd>
		     </div >   
	    	<div class="clear" id="right-pm-details" >
	    		${description}
	    	</div> 
		    </dl>
	    <div class="edit-buttons">
	    	<a class="k-button k-button-icontext" id="openASettingsBtn" ><span class="k-icon k-edit"></span>Επιπλέον Ρυθμίσεις<span class="hidden" id="openASettingsVal" >#= id #</span></a>
		    <a class="k-button k-button-icontext k-edit-button" id="pmethodListEditBtn"><span class="k-icon k-edit"></span>Edit</a>
		    <a class="k-button k-button-icontext k-delete-button" id="pmethodListDeleteBtn"><span class="k-icon k-delete"></span>Delete</a>
		</div>
	</div>
</script>
{/literal}

{literal}
<script type="text/x-kendo-tmpl" id="editTemplate">
<div class="list-box-view list-item-${id}">
	<div id="tabStrip" >
			<ul>
				<li class="k-state-active">Ρυθμίσεις</li>
				<li>Περιγραφή</li>
				<li>Μεθόδοι Μεταφοράς</li>
			</ul>
			<div >
			    <dl >
			        <dt>Payment Method ID</dt>
			        <dd>
			        	<span id="pmethod_id">${id}</span>
			        </dd>
			        <dt>Title</dt>
			        <dd>
			            <input type="text" data-bind="value:title" name="title" id="PaymentTitle" required="required" validationMessage="required"/>
			            <span data-for="PaymentTitle" class="k-invalid-msg"></span>
			        </dd>
			        <dt>Ενεργοποίηση</dt>
			        <dd>
			            <input type="checkbox" id="PaymentActive" data-bind="checked:active" name="active" # if ( active > 0) { # checked="checked" # } # />
			        </dd>
			        <dt>Υπερτίμηση</dt>
			        <dd>
			            <input type="text" id="PaymentSurcharge" data-bind="value:surcharge" data-role="numerictextbox" data-type="number" min="0" name="surcharge" required="required" validationMessage="required" />
			            <span data-for="PaymentSurcharge" class="k-invalid-msg"></span>
			        </dd>
			        <dt>Τύπος</dt>
			        <dd>
			        	<select title="surcharge_type" data-role="combobox" id="PaymentSurchargeType"  name="surcharge_type" data-bind="value:surcharge_type" required="required" validationMessage="required" >
			        		<option value="&euro;" >&euro;</option>
			        		<option value="%" >%</option>
			        		<option value="$" >$</option>
			        	</select>
			      </dd>
			    </dl>
			</div>
			<div  id="right-pm-details" >
        		<input type="textarea" class="dp100" data-role="editor" id="PaymentDescription" data-bind="value:description" name="description"  />
	    	</div>
	    	<div  id="right-pm-details"  >
	    		<input style="width: 100%;" id="shipMethdInnerCB-${id}" placeholder="Add A new.." />
		    	<div class=" dp100" style="background-color: transparent;" id="shipMethdInnerList-${id}" />
	    	</div>
	</div>
	<div class="edit-buttons">
      	 <a class="k-button k-button-icontext k-update-button" id="pmethodEditSaveBtn"><span class="k-icon k-update"></span>Save</a>
         <a class="k-button k-button-icontext k-cancel-button" id="pmethodEditCancelBtn"><span class="k-icon k-cancel"></span>Cancel</a>
	</div>
</div>
</script>
{/literal}

{literal}
<script type="text/x-kendo-tmpl" id="pmethodModalTemplate">
	<div class="pmethodModal-box-view">
		<div class="dp100" id="left-pm-details" >
		    <dl>
		    	<div class="box-row" >
			        <dt>Account ID </dt>
			        <dd>
			        	<input type="text" value="${accid}" data-bind="value:accid" name="accid" id="PaymentSettingsAccid" />
			        </dd>
		        </div>
			    <div class="box-row" >
			        <dt>Account Name</dt>
			        <dd>
			        	<input type="text" value="${accname}" data-bind="value:accname" name="accname" id="PaymentSettingsAccname" />
			        </dd>
		        </div>
		        <div class="box-row" >
			        <dt>User Mail</dt>
			        <dd>
			        	<input type="text" value="${email}" data-bind="value:email" name="email" id="PaymentSettingsEmail" />
			        </dd>
		        </div>
		        <div class="box-row" >
			        <dt>Key</dt>
			        <dd>
			        	<input type="text" value="${key}" data-bind="value:key" name="key" id="PaymentSettingsKey" />
			        </dd>
		        </div>
			    <div class="box-row" >
			        <dt>AuthKey</dt>
			        <dd>
			        	<input type="text" value="${authkey}" data-bind="value:authkey" name="authkey" id="PaymentSettingsAuthKey" />
			        </dd>
		        </div>
			    <div class="box-row" >
			        <dt>APIKey</dt>
			        <dd>
			        	<input type="text" value="${apikey}" data-bind="value:apikey" name="apikey" id="PaymentSettingsApiKey" />
			        </dd>
		        </div>
			    <div class="box-row" >
			        <dt>User Name</dt>
			        <dd>
			        	<input type="text" value="${uname}" data-bind="value:uname" name="uname" id="PaymentSettingsUname" />
			        </dd>
		        </div>
			    <div class="box-row" >
			        <dt>User Pass</dt>
			        <dd>
			        	<input type="text" value="${pass}" data-bind="value:pass" name="pass" id="PaymentSettingsPass" />
			        </dd>
		        </div>
			    <div class="box-row" >
			        <dt>Saccesfull URL</dt>
			        <dd>
			        	<input type="text" value="${sucurl}" data-bind="value:sucurl" name="sucurl" id="PaymentSettingsSucurl" />
			        </dd>
			        </div>
			    <div class="box-row" >
			        <dt>Fail URL</dt>
			        <dd>
			        	<input type="text" value="${failurl}" data-bind="value:failurl" name="failurl" id="PaymentSettingsFailurl" />
			        </dd>
		        </div>
			    <div class="box-row" >
			        <dt>Callback URL</dt>
			        <dd>
			        	<input type="text" value="${cburl}" data-bind="value:cburl" name="cburl" id="PaymentSettingsCBurl" />
			        </dd>
		        </div>
			    <div class="box-row" >
			        <dt>Extra Variables</dt>
			        <dd>
			        	<input type="text" value="${extra}" data-bind="value:extra" name="extra" id="PaymentSettingsExtra" />
			        </dd>
		        </div>
		    </dl>
	    </div >
	    <div class="edit-buttons">
	        <a class="k-button k-button-icontext " id="pmethodModalSaveBtn"><span class="k-icon k-update"></span>Save</a>
	        <a class="k-button k-button-icontext " id="pmethodModalCancelBtn"><span class="k-icon k-cancel"></span>Cancel</a>
	    </div>
	</div>
</script>
{/literal}

{literal}
<script type="text/x-kendo-tmpl" id="innerEditTemplate">
<div class="innerList-box-view innerList-item-${id}">
	<div >
	    <ul >
	        <li>
	        	<input type="text" data-bind="value:id" name="id" id="ID" data-role="numerictextbox" data-type="number" min="0" required="required" validationMessage="required"/>
	            <span data-for="ID" class="k-invalid-msg"></span>
	            <input type="text" data-bind="value:shipping" name="shipping" id="Shipping" required="required" validationMessage="required"/>
	            <span data-for="Shipping" class="k-invalid-msg"></span>
	        </li>
	    </ul>
	</div>
	<div class="edit-buttons">
      	 <a class="k-button k-button-icontext " id="innerEditSaveBtn"><span class="k-icon k-update"></span>Save</a>
         <a class="k-button k-button-icontext " id="innerEditCancelBtn"><span class="k-icon k-cnancel"></span>Cancel</a>
	</div>
</div>
</script>
{/literal}

{literal}
<script type="text/x-kendo-tmpl" id="innerListTemplate">
<div class="innerList-box-view innerList-item-${id}">
	<div >
	    <ul >
	        <li>
	        	<span class="innerUniqueID">${id}</span> <span class="innerTitle">${shipping}</span>
	        </li>
	    </ul>
	</div>
	<div class="edit-buttons">
	    <a class="k-button k-button-icontext" id="innerListDeleteBtn"><span class="k-icon k-delete"></span>Delete</a>
	</div>
</div>
</script>
{/literal}	

<style scoped>
	.box-row
	{
		width: 153px;
		float: left;
		display: inline-block;
		margin: 3px;
	}
	.list-box-view, .innerList-box-view
	{
	    float: left;
	    width: 45%;
	    margin: 10px;
	    padding: 3px;
	    -moz-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    -webkit-box-shadow: inset 0 0 50px rgba(0,0,0,0.1);
	    box-shadow: inner 0 0 50px rgba(0,0,0,0.1);
	    border-top: 1px solid rgba(0,0,0,0.1);
	    -webkit-border-radius: 8px;
	    -moz-border-radius: 8px;
	    border-radius: 8px;
	}
	.innerList-box-view
	{
		height: 55px;
		color: #777;
		background-color:  #fff;
		border-color:  #00B0F0;
	}
	#right-pm-details, #left-pm-details
	{
		margin: 10px;
	    padding: 3px;
		margin: 10px 0;
	}
	
	.list-box-view dl
	{
	    margin: 10px 0;
	    padding: 0;
	    min-width: 0;
		height: 200px;
	}
	.list-box-view dt, dd
	{
	    float: left;
	    margin: 0;
	    padding: 0;
	    //height: 30px;
	    //line-height: 30px;
	}
	.list-box-view dt
	{
	    clear: left;
	    padding: 0 5px 0 15px;
	    text-align: right;
	    opacity: 0.6;
	    width: 120px;
	}
	.k-listview
	{
	    border: 0;
	    padding: 0;
	    min-width: 0;
	}
	.k-listview:after, .product-view dl:after
	{
	    content: ".";
	    display: block;
	    height: 0;
	    clear: both;
	    visibility: hidden;
	}
	.edit-buttons
	{
		text-align: right;
	    padding: 5px;
	    border-top: 1px solid rgba(0,0,0,0.1);
	    -webkit-border-radius: 8px;
	    -moz-border-radius: 8px;
	    border-radius: 8px;
	}
	.k-toolbar, #resultsList, .k-pager-wrap
	{
	    margin: 0 auto;
	    -webkit-border-radius: 11px;
	    -moz-border-radius: 11px;
	    border-radius: 11px;s
	}
	span.k-invalid-msg
	{
	    position: absolute;
	    margin-left: 160px;
	    margin-top: -26px;
	}
	
	.k-tabstrip .k-content, .k-panelbar .k-tabstrip .k-content {
		border-style: solid;
    	border-width: 1px;
    	position: static;
    	padding 0.3em 0.3em;
    	margin: 0px;
	}
	
	
</style>
