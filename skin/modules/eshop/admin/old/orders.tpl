<div class="padding seperator wrap">
<h2>{$lang.orders}</h2>
{if $order}

<div class="padding seperator wrap">
<h2>Αριθμός παραγγελίας  #{$order.id}</h2>
<form name="order-form" action="" method="post">
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="189"><strong>#Αριθμός παραγγελίας </strong></td>
    <td width="1429">{$order.id}</td>
    <td width="1429" rowspan="11" valign="top">
    <table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th>Κωδικός MgManager</th>
    <th>Προϊόν</th>
    <th>Τιμή μονάδας με Φ.Π.Α</th>
    <th>Ποσότητα</th>
    <th>Σύνολο</th>
  </tr>
  {foreach from=$order.products item=a key=k name=b}
  <tr>
    <td><strong>{$a.productid}</strong></td>
    <td><a href="{$URL}/product/{$a.productid}/{$a.permalink}.html" target="_blank">{$a.title}</a></td>
    <td>&euro; {$a.price|formatprice:".":","}</td>
    <td>{$a.quantity}</td>
    <td>&euro; <span id="total-{$k}">{$a.total|formatprice:".":","}</span></td>
  </tr>
  {/foreach}
  <tr>
    <td class="seperator"></td></tr>
  <tr><td colspan="5" align="right">Μερικό Σύνολο : <strong>&euro;<span class="total-price-no-vat">{$total_price_no_vat|formatprice:".":","}</span></strong></td></tr>
  <tr><td colspan="5" align="right">Αξία Φ.Π.Α : <strong>&euro;<span class="vat-price">{$vat_price|formatprice:".":","}</span></strong></td></tr>
  {if $order.details.ExtraCharge}
  <tr><td colspan="5" align="right">επιβάρυνση δόσεων : <strong>&euro; {$order.details.ExtraCharge|formatprice:".":","}</strong></td></tr>
  {/if}
  <tr><td colspan="5" align="right">Γενικό Σύνολο : <strong class="price">&euro;<span class="total-price">{$total_price_vat|formatprice:".":","}</span></strong></td></tr>
  <tr>
    <td class="seperator"></td></tr>
  </table>
  
  
  
  
  
  
  
  
  
  
  
  </td>
  </tr>
  <tr>
    <td width="189"><strong>email</strong></td>
    <td>{$order.email}</td>
  </tr>
  <tr>
    <td width="189"><strong>Ποσό</strong></td>
    <td class="price"><strong>&euro;{$total_price_vat|number_format:2:".":","}</strong></td>
  </tr>
  <tr>
    <td width="189"><strong>Κατάσταση</strong></td>
  <td>{if !$order.archive}<select name="status">
          {foreach from=$status_codes item=a key=k}
	      <option value="{$k}" {if $order.status eq $k}selected{/if}>{$a}</option>
          {/foreach} 
    </select> 
     <input type="submit" value="{$lang.save}" class="submit" />
     {else}
     <strong>Παραγγελία στο αρχείο</strong>
     {/if}</td>  </tr>
  <tr>
    <td width="189"><strong>Τρόπος Πληρωμής</strong></td>
    <td>{$order.payment_method.title}</td>
  </tr>
  <tr>
    <td width="189"><strong>Ημερομηνία</strong></td>
    <td>{$order.date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
  </tr>
  <tr>
    <td width="189"><strong>Όνομα</strong></td>
    <td>{$order.details.order_name} {$order.details.order_surname}</td>
  </tr>
  <tr>
    <td width="189"><strong>Τηλέφωνο</strong></td>
    <td>{$order.details.order_phone}</td>
  </tr>
  <tr>
    <td><strong>Κινητό</strong></td>
    <td>{$order.details.order_mobile}</td>
  </tr>
  <tr>
    <td width="189"><strong>Διεύθηνση</strong></td>
    <td>{$order.details.order_address} - {$order.details.order_city} {$order.details.order_postcode}</td>
  </tr>
    <tr>
    <td width="189"><strong>Τύπος Παραστατικού</strong></td>
    <td>{if $details.invoice_type eq "reciept"}Απόδειξη{else}Τιμολόγιο{/if}</td>
  </tr>
    {if $order.details.invoice_type eq "invoice"}
  <tr>
    <td><strong>Επωνυμία Εταιρίας</strong></td>
    <td>{$order.details.company_name}</td>
  </tr>
    <tr>
    <td><strong>Δραστηριότητα</strong></td>
    <td>{$order.details.proffession}</td>
  </tr>
    <tr>
    <td><strong>Α.Φ.Μ.</strong></td>
    <td>{$order.details.afm}</td>
  </tr>
    <tr>
    <td><strong>Δ.Ο.Υ.</strong></td>
    <td>{$order.details.doy}</td>
  </tr>
    <tr>
    <td><strong>Τηλέφωνο</strong></td>
    <td>{$order.details.phone}</td>
  </tr>
    <tr>
    <td><strong>Διεύθυνση</strong></td>
    <td>{$order.details.address}</td>
  </tr>
    <tr>
    <td><strong>Ταχ. Κώδικας</strong></td>
    <td>{$order.details.postcode}</td>
  </tr>
    <tr>
    <td><strong>Σχόλια</strong></td>
    <td><em>"{$order.details.comments}"</em></td>
  </tr>
  {/if}
 <tr><td colspan="3" align="center"><a href="/modules/eshop/admin/orders.php?id={$order.id}&action=print">Εκτύπωση παραγγελίας</a></td></tr>
</table>
<input type="hidden" value="update" name="action" /><input type="hidden" value="{$order.id}" name="id" />
</form>
{if $order.items}
<div class="seperator"></div>
<h2>Λαχνοί</h2>
{foreach from=$order.items item=a key=k}
<table width="100%" border="0" cellspacing="0" cellpadding="4">
    <tr>
      <td>Επιλογή : <a href="javascript:void(0)" class="select-all">Όλα</a> / <a href="javascript:void(0)" class="select-none">Κανένα</a></td>
      <td><form method="post" action="/print_tickets.html" name="print_form">Με τα επιλεγμένα :{if $order.status ne 2} <a href="javascript:void(0)" class="delete-tickets">Διαγραφή από την παραγγελία</a> |{/if} 
<input type="submit" value="Εκτύπωση λαχνών" class="submit" /> Λαχνοί για εκτύπωση <strong><span class="to_print">{$tickets_to_print|default:0}</span></strong>
<input name="tickets_to_print" id="tickets_to_print" type="hidden" /> <input type="hidden" name="admin" value="{$order.uid}" /> <input type="hidden" name="orderid" value="{$order.id}" />
</form></td>
    </tr>
    {foreach from=$a.t item=b key=c}
    <tr>
    <td width="170"><div class="product-image float-left big_space"><a href="/item_{$b.item.id}.html" title="{$b.item.title}"><img src="{$b.item.image_full.thumb}" /></a></div></td>
    <td>
<a href="/item_{$b.item.id}.html">{$b.item.title}</a>
<div class="seperator clear"></div>
<ul id="ticket-list">
{foreach from=$b.tickets item=g}
<li><input type="checkbox" name="tickets[]" rel="{$g}|{$b.item.id}" class="ticket-link" /> #{$g}</li>
{/foreach}
</ul>
<div class="clear"></div>
</td>
  </tr>
{/foreach}
</table>
<hr />
{/foreach}
</div>
{/if}
{literal}
<script>
$(function(){
function gather_values()
{
	var tickets = new Array();	
$(".ticket-link").each(
	function( intIndex ){
		if ($(this).is(':checked'))
		{
		tickets.push($(this).attr('rel'));
		}
	});//END FOREACH
$('.to_print').html("" + tickets.length + "");
return tickets;
}
$(".select-all").click(function(){
$('.ticket-link').each(
	function( intIndex ){
		$(this).attr({'checked':true});
	});//END FOREACH

$('#tickets_to_print').val(gather_values());
});//END FUNCTION

$(".select-none").click(function(){
$('.ticket-link').each(
	function( intIndex ){
		$(this).attr({'checked':false});
	});//END FOREACH

$('#tickets_to_print').val(gather_values());
});//END FUNCTION

$(".ticket-link").click(function(){
		if ( !$(this).is('.ticket-selected') )
		{
		  $(this).addClass('ticket-selected'); 
		  $('#tickets_to_print').val(gather_values());
		}
		else
		{
			$(this).removeClass('ticket-selected'); 
			$('#tickets_to_print').val(gather_values());
		}
$('.to_print').html("" + $('.ticket-selected').length + "");
});//END CLICK FUNCTION
});// END JQUERY
</script>
{/literal}
{else}

<div class="padding seperator wrap">
<h2>Latest Orders</h2>
{include file="modules/eshop/admin/orders_table.tpl"}
{if $list.total > $MODULE_SETTINGS.admin_users_per_page}{$list.from} {$lang.to} {$list.to} {$lang.of_total} : {$list.total} {include file="modules/users/pagination.tpl" mode="admin"}{/if}
</div>

{/if}
</div>