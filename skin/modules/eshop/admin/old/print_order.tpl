<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Παραγγελία {$order.id}</title>
</head>

<body>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="189"><strong>#&Alpha;&rho;&iota;&theta;&mu;ό&sigmaf; &pi;&alpha;&rho;&alpha;&gamma;&gamma;&epsilon;&lambda;ί&alpha;&sigmaf; </strong></td>
    <td width="1429">{$order.id}</td>
  </tr>
  <tr>
    <td width="189"><strong>email</strong></td>
    <td>{$order.email}</td>
  </tr>
  <tr>
    <td width="189"><strong>Ποσό</strong></td>
    <td class="price"><strong>&euro;{$order.amount|add_vat|number_format:2:".":","}</strong></td>
  </tr>
  <tr>
    <td width="189"><strong>Τρόπος Πληρωμής</strong></td>
    <td>{$order.payment_method.title}</td>
  </tr>
  <tr>
    <td width="189"><strong>Ημερομηνία</strong></td>
    <td>{$order.date_added|date_format:"%d/%m/%Y @ %H:%M"}{$order.date_added}</td>
  </tr>
  <tr>
    <td width="189"><strong>Ό&nu;&omicron;&mu;&alpha;</strong></td>
    <td>{$order.details.order_name} {$order.details.order_surname}</td>
  </tr>
  <tr>
    <td width="189"><strong>Τηλέφωνο</strong></td>
    <td>{$order.details.order_phone}</td>
  </tr>
  <tr>
    <td width="189"><strong>Διεύθυνση</strong></td>
    <td>{$order.details.order_address} - {$order.details.order_city} {$order.details.order_postcode}</td>
  </tr>
  <tr>
    <td><strong>Σχόλια</strong></td>
    <td>"<em>{$order.details.order_comments}</em>"</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th>Κωδικός MgManager</th>
    <th>Προϊόν</th>
    <th>Τιμή μονάδας με Φ.Π.Α</th>
    <th>Ποσότητα</th>
    <th>Σύνολο</th>
  </tr>
  {foreach from=$order.products item=a key=k name=b}
  <tr>
    <td><strong>{$a.productid}</strong></td>
    <td><a href="{$URL}/product/{$a.productid}/{$a.title|clean_url}.html" target="_blank">{$a.title}</a></td>
    <td>&euro; {$a.price|formatprice:".":","}</td>
    <td>{$a.quantity}</td>
    <td>&euro; <span id="total-{$k}">{$a.total|formatprice:".":","}</span></td>
  </tr>
  {/foreach}
  <tr>
    <td class="seperator"></td></tr>
  <tr><td colspan="5" align="right">Μερικό Σύνολο : <strong>&euro;<span class="total-price-no-vat">{$total_price_no_vat|formatprice:".":","}</span></strong></td></tr>
  <tr><td colspan="5" align="right">Αξία Φ.Π.Α : <strong>&euro;<span class="vat-price">{$vat_price|formatprice:".":","}</span></strong></td></tr>
  <tr><td colspan="5" align="right">Γενικό Σύνολο : <strong class="price">&euro;<span class="total-price">{$total_price_no_vat|add_vat|formatprice:".":","}</span></strong></td></tr>
  <tr>
    <td class="seperator"></td></tr>
</table>
</body>
</html>
