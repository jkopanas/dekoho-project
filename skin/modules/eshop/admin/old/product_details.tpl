{if $mode eq "editor"}
    <tr>
      <td>{$lang.price}</td>
      <td><input name="price" type="text" id="price" size="20" value="{$products.details.price}" style="float:left">
      {if $providers}
      <table border="0" style="float:left; margin-left:15px;">
      <th align="center">Provider</th>
      <th align="center">Code</th>
      <th align="center">{$lang.price}</th>
      <th align="center">Last Update</th>
      <th align="center">Availability</th>
      {foreach item=a from=$providers}
        <tr>
        <td>{$a.user_name}</td>
        <td><input type="text" name="provider_code-{$a.id}" value="{$a.eshop.provider_code}" /></td>
        <td><input type="text" name="provider_price-{$a.id}" value="{$a.eshop.price}" /></td>
        <td>{$a.date_modified}</td>
        <td><select name="provider_availability-{$a.id}" >
          <option value="X" {if $a.eshop.availability eq "X"}selected{/if}>Διαθεσιμο εντος 24 ωρων</option>
          <option value="0" {if $a.eshop.availability eq "0"}selected{/if}>Παραλαβη σε 1 - 7 ημερες</option>
          <option value="1" {if $a.eshop.availability eq "1"}selected{/if}>Παραλαβη σε 8 - 15 ημερες</option>
          <option value="2" {if $a.eshop.availability eq "2"}selected{/if}>Παραλαβη σε 16+ ημερες</option>
          <option value="3" {if $a.eshop.availability eq "3"}selected{/if}>Παραλαβη σε 30+ ημερες</option>
          <option value="4" {if $a.eshop.availability eq "4"}selected{/if}> μη διαθεσιμο</option>
          <option value="9" {if $a.eshop.availability eq "9"}selected{/if}>αγνωστη ημερομινια παραλαβης</option>
        </select>
        </td>
        </tr>
    {/foreach}
      </table>
      {/if}
      </td>
    </tr>

    <tr>
      <td width="120">{$lang.list_price}</td>
      <td><input name="list_price" type="text" id="list_price" size="20" value="{$products.details.list_price}"></td>
    </tr>

{else}
{include file="modules/products/admin/menu.tpl"}
{include file="modules/products/admin/additional_links.tpl" mode="nav_menu" base_file="products_modify"}

<div class="padding seperator wrap">
<h2>Δόσεις</h2>
<div class="debug"></div>
<form name="calcDoseis"> 
<input type="hidden" name="id" value="{$item.id}" />
<input type="button" name="SaveException" value="Αποθήκευση" class="button" />
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th scope="col">Αριθμός δόσεων</th>
    <th scope="col">Επιτόκιο</th>
    </tr>
  {foreach from=$payments item=a}
  <tr {if $a.show}class="alternate"{/if}>
    <td align="center">{$a.payments}</td>
    <td align="center"><input type="text" value="{$a.monthly_charge}" name="monthly_charge-{$a.id}" class="SaveData" />%</td>
    </tr>
  {/foreach}
</table>
<input type="button" name="SaveException" value="Αποθήκευση" class="button" />
</form>

</div>

<div class="seperator"></div>
<script type="text/javascript" src="/scripts/monthly_payments.js"></script>
{/if}