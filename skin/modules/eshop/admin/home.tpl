<div class="dp100">
	<div class="box_c" />
		<div class="padding wrap">
			<h3 class="box_c_heading cf">
				Eshop Dashboard
			</h3>
			<div id="drawToolbar" class="dp100" style="position: relative; ">
				<div id="dashboardBaseDDL" style="float: left; possition: relative;">
				    <label class="DBDDL-Status-label" for="DBDDL">Show Stats for</label>
				    <input type="search" id="DBDDL" style="width: 230px" data-text-field="text" data-value-field="value" data-bind="value:text" />
				</div>
		    </div>
		    <div class="clear"></div>
			<div id="drawAreaCanvas" style="position: relative; margin-top: 20px; "></div>
			<div class="debug"></div>
			<div id="resultsGrid"> </div>
			
		</div><!-- END WRAP -->
	</div>
</div>

{if count($jsIncludes) gt 0}
	{foreach from=$jsIncludes key=k item=v}
		<script type="text/javascript" src="{$v}"></script>
	{/foreach}
{/if}

<script type="text/x-kendo-template" id="eshopDashboardDDLTPL">
	<div class="#= ddlId #DDL">
	    <label class="Order-Status-label" for="orderStatusDDL">#= message #</label>
	    <input type="search" id="eshopDashboardDDL-#= ddlId #" style="width: 230px"></input>
	</div>
</script>
<script type="text/javascript" src="/scripts/kendo/js/kendo.dataviz.min.js"></script>
<script type="text/javascript" src="/scripts/kendo/js/kendo.web.min.js"></script>
<script>
//head.js('/scripts/kendo/js/kendo.dataviz.min.js');
//head.js('/scripts/admin2/dashboard.js');
//head.js('/scripts/admin2/charts.js');
//head.js('/scripts/admin2/home.js');
</script>
