{if $loaded_modules.eshop}
<div class="sepH_c cf">
<label class="lbl_a">{$lang.price}</label>
<input type="text" name="price" value="{$item.eshop.price}" class="inpt_a toSave" data-module="eshop" /> 
</div>
<div class="sepH_c cf">
<label class="lbl_a">{$lang.list_price}</label>
<input type="text" name="list_price" value="{$item.eshop.list_price}" class="inpt_a toSave" data-module="eshop"  /> 
</div>
{/if}