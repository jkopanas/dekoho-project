<div id="AccountCard" class="hidden">
	<table cellpadding="5" cellspacing="5" class="invoice_table">
					<tr>
						<td  valign="middle" width="50%">Όνομα:</td><td  valign="middle">
							<input gtbfieldid="37" size="40" name="user_name" value="{$USER_NAME}" type="text"  required class="AccountData Accountsave">
						</td>
					</tr>
					<tr>
						<td  valign="middle" width="50%">Eπώνυμο:</td><td  valign="middle">
							<input gtbfieldid="37" size="40" name="user_surname" value="{$USER_SURNAME}" required type="text" class="AccountData Accountsave">
						</td>
					</tr>
					<tr>
						<td  valign="middle" width="50%">Διεύθυνση:</td><td  valign="middle">
							<input gtbfieldid="39" size="40" name="address" value="{$settings_user.address}" required type="text" class="AccountData AccountsaveSettings">
						</td>
					</tr>
					<tr>
						<td  valign="middle" width="50%">email:</td><td  valign="middle">
							<input gtbfieldid="42" size="40" name="email" value="{$EMAIL}" type="text" required class="AccountData Accountsave">
						</td>
					</tr>
					<tr>
						<td  valign="middle" width="50%">τηλέφωνο επικοινωνίας:</td><td  valign="middle">
							<input gtbfieldid="42" size="40" name="user_mobile" value="{$USER_MOBILE}" required type="text" class="AccountData Accountsave">
						</td>
					</tr>
	</table>
	<div  data-id="{$ID}" data-mode=""   class="k-button k-button-icontext  float-right SaveAccount" >{$lang.save} this time&nbsp;&nbsp;<span class="k-icon k-save "></span></div>
	<div  data-id="{$ID}" data-mode="save" class="k-button k-button-icontext  float-right SaveAccount" >{$lang.save}&nbsp;&nbsp;<span  class="k-icon k-save "></span></div>
</div>


<div class="col2-set ">

<div class="col-1">
<h3>Τόπος Αποστολης </h3>    
<div class="seperator"></div>  
<ul id="shipping-info">
	<li><input type="radio" value="home" name="shipping_info" id="home" class="shipping_info save" checked /> <label for="home">Σπίτι</label></li>
	<li><input type="radio" value="hospital" name="shipping_info" id="hospital" class="shipping_info save" /> <label for="hospital">Νοσοκομείο</label></li>
</ul>

</div>
<div class="col-2">
<h3>Στοιχεια Διευθυνσης</h3>  
<div class="content" id="shipping_info">

</div><!-- END CONTENT -->

</div>


 <script id="hospital_field" type="text/x-kendo-template">
<div class="seperator"></div>	
<strong> Όνοματεπώνυμο: </strong><br/><input size="40" name="name" value="" type="text" required class="spacer-bottom input-text AccountDataShippingInfo"><br/>
<strong> Όνομα Κλινικής: </strong><br/><input size="40" name="hospital_name" value="" type="text" required class="spacer-bottom input-text AccountDataShippingInfo"><br/>
<strong>Οροφος: </strong><br/><input size="5" name="Orofos" value="" type="text" required class="spacer-bottom input-text AccountDataShippingInfo"><br/>
<strong> Αριθμός Δωματίου: </strong><br/><input size="5" name="room_no" value="" type="text" required class="spacer-bottom input-text AccountDataShippingInfo"><br/>
<strong> Τηλέφωνο: </strong><br/><input size="40" name="tel" value="" type="text" required class="spacer-bottom input-text AccountDataShippingInfo"><br/>
<strong>Κινητό: </strong><br/><input size="40" name="mobile" value="" type="text" required class="spacer-bottom input-text AccountDataShippingInfo"><br/>
</script>

<script id="home_field" type="text/x-kendo-template">	
<div class="seperator"></div>	
<strong>Ονοματεπώνυμο</strong><span data-bind="text: user_name">{$USER_NAME}</span> <span data-bind="text: user_surname" >{$USER_SURNAME}</span> 
<div class="seperator"></div>
<strong> Διεύθυνση: </strong><span data-bind="text: address">{$settings_user.address}</span>
<div class="seperator"></div>
<strong> Τηλέφωνο: </strong><span data-bind="text: user_mobile">{$settings_user.tel}</span>
<div class="seperator"></div>
<strong> Κινητό: </strong><span data-bind="text: user_mobile">{$USER_MOBILE}</span>
<div class="seperator"></div>
<strong> Email: </strong><span data-bind="text: email">{$EMAIL}</span>
<div class="seperator"></div>
<div class="k-button k-button-icontext EditAcount float-right" >{$lang.edit}&nbsp;&nbsp;<span class=" k-icon k-edit EditAcount"></span></div>
</script>		
					