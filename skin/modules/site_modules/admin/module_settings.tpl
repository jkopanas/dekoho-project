<form action="{$SELF}" method="post" id="settings_form" name="settings_form">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th colspan="3" scope="cols">{$module.title}</th>
    </tr>
  <tr>
    <th scope="cols">Areas</th>
    <th scope="cols">Pages</th>
    <th scope="cols">Settings</th>
  </tr>
  <tr>
    <td align="center" valign="top"><select name="settings_areas[]" size="5" multiple id="select">
      <option value="L" {foreach from=$module.available_areas item=a}{if $a eq "L"}selected{/if}{/foreach}>Left</option>
      <option value="C" {foreach from=$module.available_areas item=a}{if $a eq "C"}selected{/if}{/foreach}>Center</option>
      <option value="R" {foreach from=$module.available_areas item=a}{if $a eq "R"}selected{/if}{/foreach}>Right</option>
    </select></td>
    <td align="center" valign="top"><select name="settings_pages[]" size="5" multiple id="select">
    {section name=a loop=$files}
    
      <option value="{$files[a].page}" {foreach from=$module.pages item=a}{if $a eq $files[a].page}selected{/if}{/foreach}>{$files[a].page}</option>
    {/section}
    </select></td>
    <td align="center" valign="top">
    <table>
    {foreach from=$module.settings item=v key=k}
    <tr><td>{$k}</td><td><input type="text" value="{$v}" name="settings_settings[{$k}]" id="settings_{$k}"></td></tr>
    {/foreach}
    </table></td>
  </tr>
  <tr>
    <td colspan="3" align="center" valign="top"><input type="submit" name="Submit" id="button" value="{$lang.save}" class="button">
      <input name="mode" type="hidden" id="mode" value="save_module">
      <input name="mid" type="hidden" id="mid" value="{$module.id}"></td>
    </tr>
</table>

</form>