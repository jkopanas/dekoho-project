<div id="explore_box">
{if $message}
{$message}
{else}
<ul class="list">
{foreach from=$items item=a}
<li><a href="{$URL}/page/{$a.id}/" title="{$a.title}">{$a.title}</a></li>
{/foreach}
</ul>
{/if}
</div>