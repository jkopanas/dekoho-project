<div id="plaisio_news_box">
{if $message}
{$message}
{else}

<table width="100%" border="0" cellpadding="0" cellspacing="5" bgcolor="#eeeeee" class="plaisio_news">
  <tr>
    <td colspan="2" valign="top" bgcolor="#ED1C24"><img src="/images/plaisio_news_box.gif" /></td>
    </tr>
    {section name=a loop=$plaisio_news}
  <tr>
    <td valign="top">&nbsp;</td>
    <td align="right" valign="top">{$plaisio_news[a].date_added|date_format:"%d/%m/%Y @ %H:%M"}</td>
  </tr>
  <tr>
{if $plaisio_news[a].image AND $plaisio_news[a].image ne $plaisio_news_box_settings.default_thumb}<td valign="top" width="{$plaisio_news_box_settings.news_images_thumb_width}"><img src="{$plaisio_news[a].image}" align="left" /></td>
{/if}
<td valign="top" colspan="2"><strong>{$plaisio_news[a].title}</strong>
<br />
{$plaisio_news[a].description|truncate:130:"..."}</td>
</tr>
<tr><td colspan="2" align="right"><a href="{$URL}/plaisio-news/{$plaisio_news[a].catid}/{$plaisio_news[a].id}/{$PAGE}/">{$lang.more} &gt;&gt;</a></td></tr>
</tr>
  <tr>
    <td style="background:#FFFFFF; height:1px" colspan="2"><img src="/images/filler.gif" height="1" width="1"></td>
    </tr>
{/section}
</table>
{/if}
<div align="right"><div style="float:left"><a href="{$URL}/plaisio-news/" title="{$lang.more_news}" class="box_pagination" style="color:#000000">{$lang.more_news} &gt;&gt;</a></div>
{include file="common/pagination.tpl" mode="ajax" type="plaisio_news" res_div="plaisio_news_box"}</div>
</div>