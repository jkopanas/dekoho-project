
<form>
<div class="seperator padding wrap">
<h2>{$lang.sms_list} [ {$list.from} {$lang.to} {$list.to} {$lang.of_total} : {$list.total} ]</h2>
<table width="100%"  border="0" class="widefat">
  <tr>
  <td colspan="8">
  {$lang.withSelectedDo} (<span class="messages">0</span> επιλέχθηκαν) : 
        <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ArchiveChecked">{$lang.archive}</option>
        <option value="ExportChecked">{$lang.export}</option>
        </select>
         <input type="button" class="button messageActions" value="{$lang.apply}" alt="#searchMessagesForm" />
         <input type="button" class="button selectAllResults hidden" value="{$lang.selectAllResults}" alt="#searchMessagesForm" />
         
         </td>
  </tr>
       <tr class="nodrop nodrag hidden" id="selectAllResultsInfo">
        <td colspan="8">
    		<div class="messagePlaceHolder messageWarning">{$lang.allResultsSelected}</div>
            </td>
      </tr>    
        <TR class="TableHead" {cycle values=", class='TableSubHead'"}>
    <TD><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /></TD>
    <td width="10"><strong>ID</strong></td>
    <td><strong>{$lang.username}</strong></td>
    <td><strong>{$lang.destination}</strong></td>
    <td><strong>{$lang.originator}</strong></td>
    <td><strong>{$lang.message}</strong></td>
    <td><strong>{$lang.status}</strong></td>
    <td><strong>{$lang.sentDate}</strong></td>
    </tr>
{foreach from=$items item=a}
  <TR {cycle values=", class='alternate'"}>
<TD width="1%"><input name="id" type="checkbox" id="check_values" value="{$a.id}" class="check-values" /></TD>
<td width="10"><a href="#" class="dlrInfo" rel="{$a.id}"><strong>{$a.id}</strong></a></td>
    <td>{$a.userDetails.uname}</td>
    <td>{$a.destination}</td>
    <td>{$a.originator}</td>
    <td>
    {if $a.messageText|strlen > 30} 
      {$a.messageText|truncate:30} 
    {else} 
      {$a.messageText} 
    {/if}
    
    </td>
    <td>{if $a.dlrStatus eq 'DELVRD'}<span style="color:green">{$a.dlrStatus}</span>
    {elseif $a.dlrStatus eq 'UNDELVRD'}<span style="color:red">{$a.dlrStatus}</span>
    {else}<span style="color:yellow">{$a.dlrStatus}</span>{/if}</td>
    <td>{$a.dateSend}</td>
    </tr>
  {/foreach}
    <tr>
  <td colspan="8">{$lang.withSelectedDo} (<span class="messages">0</span> επιλέχθηκαν ) : 
    <select name="actions2" id="actions" class="CheckedActions">
      <option value="ArchiveChecked">{$lang.archive}</option>
      <option value="ExportChecked">{$lang.export}</option>
    </select>
    <input type="button" class="button messageActions" value="{$lang.apply}" alt="#searchMessagesForm" /></td>
  </tr>
  </table>
</div>
<input type="hidden" class="CheckedActions" value="" name="allItemsSelected" />
<input name="page" type="hidden" id="page" value="{$page}" class="CheckedActions"/>
{if $list.total gt $users|@count}
{include file="modules/content/pagination.tpl" mode="ajax" class="messagePage" target="searchMessagesForm"}
{/if}
</form>
