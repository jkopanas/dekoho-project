<div id="connectionFormHolder" class="hidden">
<div class="content">
    <div class="seperator padding wrap">
    <h2>{$lang.addConnection}</h2>
		    <form id="connectionForm">
            <table>
            <tr>
            	<td align="left">{$lang.title}</td>
                <td align="left">
                <input type="text" name="title" class="formData required" value="{$item.title}" />
                <input type="hidden" name="id" class="formData" value="{$item.id}" />
                </td>
            </tr>
            <tr>
            	<td align="left">{$lang.conectionType}</td>
                <td align="left">
                <select name="connectionType" class="formData">
                <option value="SMPP" {if $item.connectionType=="SMPP"}selected="selected"{/if}>SMPP</option>
                <option value="HTTP" {if $item.connectionType=="HTTP"}selected="selected"{/if}>HTTP</option>
                </select>
                </td>
            </tr>
            <tr>
            	<td align="left">Hostname</td>
                <td align="left"><input class="connectionSettings required" name="hostname" value="{$item.settings.hostname}"></td>
            </tr>
            <tr>
            	<td align="left">Connection Port</td>
                <td align="left"><input class="connectionSettings required" name="port" value="{$item.settings.port}"></td>
            </tr>
            <tr>
            	<td align="left">Username</td>
                <td align="left"><input class="connectionSettings required" name="username" value="{$item.settings.username}"></td>
            </tr>
            <tr>
            	<td align="left">Password</td>
                <td align="left"><input class="connectionSettings required" name="password" value="{$item.settings.password}"></td>
            </tr>
            <tr>
            	<td align="left">Logging</td>
                <td align="left">
                <select name="loggingSettings" class="formData">
                <option value="1" {if $item.loggingSettings=="1"}selected="selected"{/if}>Yes</option>
                <option value="0" {if $item.loggingSettings=="0"}selected="selected"{/if}>No</option>
                </select>
                </td>
            </tr>
             <tr>
            	<td align="left">{$lang.active}</td>
                <td align="left"><input type="checkbox" name="Active" class="formData" value="1" {if $item.active==1} checked="checked"{/if}></td>
            </tr>
            <tr>
            	<td align="left">Default</td>
                <td align="left"><input type="checkbox" name="setDefault" class="formData" value="1" {if $item.setDefault==1} checked="checked"{/if}><input type="hidden" name="wasDefault" class="formData" value="{if $item.setDefault==1}1{else}0{/if}"></td>
            </tr>
            <tr>
            	<td colspan="2"><input type="submit" value="Submit"/></td>
            </tr>
             
            </table>
            </form>
    </div>
    </div>
</div>