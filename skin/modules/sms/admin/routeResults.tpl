<table width="100%"  border="0" class="widefat">
<tr>
	<td>
    	<strong>ID</strong>
    </td>
	<td>
    	<strong>{$lang.routeName}</strong>
    </td>
    <td>
    	<strong>{$lang.routeType}</strong>
    </td>
    <td>
	    <strong>{$lang.conectionName}</strong>
    </td>
    <td><strong>{$lang.country}</strong></td>
    <td><strong>{$lang.user}</strong></td>
    <td>
    	<strong>{$lang.status}</strong>
    </td>
     <td>
     <strong>{$lang.cost}</strong>
    </td>
    <td>&nbsp;
    
    </td>
</tr>
{foreach from=$items item=a}
<tr {cycle values=", class='alternate'"}>
	<td>
    	{$a.id}
    </td>
	<td>
    	{$a.title}
    </td>
    <td>
    	{$a.routingType}
    </td>
    <td>
    	{$a.connection}
    </td>
    <td>{$a.country}</td>
    <td>
    {if $a.uid!=0}
    {$a.userDetails.uname}
    {else}
    {$lang.allusers}
    {/if}
    </td>
    <td>
    	{if $a.active==1}<span style="color:green;">{$lang.active}</span>{/if}
        {if $a.active==0}<span style="color:red;">{$lang.inactive}</span>{/if}
    </td>
    <td>
    	{$a.cost}
    </td>
    <td>
    	<a href="#" class="editRoute" rel="{$a.id}"><img height="16" width="16" src="/images/admin/edit_16.png"></a>
    	<a href="#" class="addCustomRoute" rel="{$a.id}"><img height="16" width="16" src="/images/admin/add_16.gif"></a>
        <a href="#" class="addFavoriteRoute" rel="{$a.id}"><img height="16" width="16" src="/images/admin/reviews_16.png"></a>
        {if $a.hasCustomRoutes==1}
        <a href="#" class="showCustomRoute" rel="{$a.id}"><img height="16" width="16" src="/images/admin/docs_16.png"></a>
        {elseif $a.routeUniqueID==""}
        <a href="#" class="deleteRoute" rel="{$a.id}"><img height="16" width="16" src="/images/admin/delete_16.png"></a>
        {/if}
        
    </td>
</tr>
<tr class="infobox hidden">
<td colspan="8"></td>
</tr>
{/foreach}
</table>
<input name="page" type="hidden" id="page" value="{$page}" class="CheckedActions"/>
{if $list.total gt $users|@count}
{include file="modules/content/pagination.tpl" mode="ajax" class="routesPage" target="searchRoutesForm"}
{/if}