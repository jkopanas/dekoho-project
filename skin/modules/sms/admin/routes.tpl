{include file="modules/sms/admin/menu.tpl"}

<div class="seperator padding wrap">
<h2><a href="javascript:void(0)" class="ShowSearchTable Slide" rel="#SearchTable">Αναζήτηση</a></h2>
<div class="debug"></div>

<form name="searchform" id="searchRoutesForm"  method="POST">
<table width="100%" border=0 cellpadding="5" cellspacing="5" id="SearchTable" class="hidden">

<TR>
  <TD nowrap class=FormButton>{$lang.results_per_page}</TD>
  <TD width="933"><select name="results_per_page" id="results_per_page" class="secondarySearchData">
    <option value="10" {if $data.results_per_page eq 10}selected{/if}>10</option>
  	<option value="20" {if $data.results_per_page eq 20}selected{/if}>20</option>
    <option value="60" {if $data.results_per_page eq 60}selected{/if}>60</option>
    <option value="100" {if $data.results_per_page eq 100}selected{/if}>100</option>
  </select>  </TD>
</TR>
<TR>
  <TD nowrap class=FormButton>{$lang.order_by}</TD>
  <TD> <select name="orderby" class="secondarySearchData">
  <option value="id">#ID</option> 
   <option value="smsConnections.title">{$lang.connections}</option>
   <option value="smsRoutingTable.title">{$lang.title}</option>
   <option value="dialing_codes.Country">{$lang.country}</option>
   <option value="smsRoutingTable.date_updated">{$lang.updateDate}</option>
</select>  
{$lang.way} : 
<select name="way" id="sort_direction" class="secondarySearchData">
<option value="desc">{$lang.descending}</option>
<option value="asc">{$lang.ascending}</option>             
        </select></TD>
</TR>
<TR>
  <TD width="94" nowrap class=FormButton>#ID</TD>
  <TD>
  <INPUT name="smsRoutingTable.id" type=text id="smsRoutingTable.id" value="{$data.id}" class="SearchData"  /></TD>
</TR>

<TR>
  <TD width="94" nowrap class=FormButton>{$lang.title}</TD>
  <TD>
  <INPUT name="smsRoutingTable.title" type=text id="smsRoutingTable.title" value="{$data.title}" class="likeData"  /></TD>
</TR>
<tr>
  <td nowrap class=FormButton>{$lang.country}</td>
  <td><INPUT name="countries" type=text id="countries" value="{$data.country}" class="SearchData"  /></td>
</tr>
<tr>
  <td nowrap class=FormButton>{$lang.connections}</td>
  <td>
  
  <select name="connections" id="connections" class="SearchData multiSelect" size="5" multiple="multiple">
    <option value="">{$lang.all}</option>
          {foreach from=$connections item=a key=k}
	      <option value="{$a.title}" >{$a.title}</option>
          {/foreach}
	      </select>
  </td>
</tr>
<tr>
  <td class=FormButton nowrap>{$lang.date}</td>
  <td>{$lang.from} : <input type="text" id="dateFrom" name="dateFrom" class="datePicker secondarySearchData" size="30"/> {$lang.to} : <input type="text" id="dateTo" name="dateTo" class="datePicker secondarySearchData" size="30"/> </td>
</tr>
<tr>
  <td colspan="2" align="center" class=FormButton>
    <input type="submit" value="{$lang.search}" class="button" id="searchUsers" name="searchUsers">
    <input name="page" type="hidden" id="page" value="{$page}" class="secondarySearchData">
    <input name="ResultsDiv" type="hidden"  value="#ResultsPlaceHolder" class="secondarySearchData">
    <input name="FormToHide" type="hidden"  value="#SearchTable" class="secondarySearchData">
    <input name="FormToShow" type="hidden"  value="#ResultsPlaceHolder" class="secondarySearchData"></td>
</tr>

</table>
</form>
</div>


<div class="seperator padding wrap">
<h2>{$lang.connections} <a href="#" id="newRoute">({$lang.addRoute})</a></h2>
<div id="ResultsPlaceHolder" >
<div id="routeResults">
{include file="modules/sms/admin/routeResults.tpl"}
</div>
</div>
<div id="FormDiv">
{include file="modules/sms/admin/routeForm.tpl"}
</div>
<div id="customRouteFormDiv">
{include file="modules/sms/admin/customRouteForm.tpl"}
</div>
<script src="/scripts/jquery.multiselect.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.multiselect.filter.min.js"></script>
<script src="/scripts/routes.js"></script>