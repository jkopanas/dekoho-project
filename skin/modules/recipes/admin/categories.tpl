{include file="modules/recipes/admin/menu.tpl"}
<div id="in_this_section"><strong>In this section :</strong> <a href="#featured_add">Add Featured Items</a> | <a href="categories.php{if $catid}?cat={$catid}{/if}#addcategory">Add new category</a></div>
{include file="modules/recipes/search_box.tpl" mode="start_page"}
{if !$edit}
<TABLE class="open_close_tab seperator ">
      <TR>
        <TD id="close3" style="display: none; cursor: hand;" onClick="visibleBox('3')"><IMG src="/skin/images/plus.gif" border="0" align="absmiddle" alt="Click to open"></TD>
        <TD id="open3" style="cursor: hand;" onClick="visibleBox('3')"><IMG src="/skin/images/minus.gif" border="0" align="absmiddle" alt="Click to close"></TD>
        <TD><A href="javascript:void(0);" onClick="visibleBox('3')"><B>Show - Hide</B></A></TD>

      </TR>
</TABLE>
<div class="wrap padding" id="box3">
<h2><a href="{$MODULE_FOLDER}/categories.php">{$lang.categories}</a> (<a href="#addcat">add new</a>) {include file="modules/recipes/nav_categories.tpl" management=$lang.categories target="admin_categories"}</h2>
<form name="cat_form" id="cat_form" action="" method="post">
<table cellpadding="3" cellspacing="3" width="100%">
<tr>
		<th scope="col">ID</th>
		<th scope="col">{$lang.pos}</th>
        <th scope="col">{$lang.title}</th>
        <th scope="col">{$lang.description}</th>
        <th scope="col">#{$lang.subcategories}</th>
        <th scope="col"># {$lang.items}</th>
        <th colspan="3">Action</th>
	</tr>
{section name=a loop=$cat}
<tr class="{cycle values='alternate,'}"><th scope="row">{$cat[a].categoryid}</th>
  <td><input name="order_by-{$cat[a].categoryid}" type="text" value="{$cat[a].order_by}" size="3" /></td>
  <td> <a href="categories.php?cat={$cat[a].categoryid}">{$cat[a].category}</a></td>
				<td>{$cat[a].description|truncate:50:" ...":true}</td>
				<td>{$cat[a].num_sub|default:$lang.txt_not_available}</td>
				<td align="center"><A href="category_content.php?cat={$cat[a].categoryid}" class="ItemsList">{$cat[a].num_content|default:0}</A></td>
                <td><a href="{$MODULE_FOLDER}/mass_insert.php?cat={$cat[a].categoryid}" class="edit" title="Mass Insert into category  {$cat[a].category}"><img src="/images/admin/arrow_16.png" width="16" height="16" alt="Mass Insert" /></a></td>
	            <td><a href="{$MODULE_FOLDER}/content_new.php?cat={$cat[a].categoryid}" class="edit" title="Add Item"><img src="/images/admin/add_16.gif" width="16" height="16" alt="Add Item" /></a></td>
      <td><a href="{$MODULE_FOLDER}/categories.php?action=edit&amp;cat={$cat[a].categoryid}" class="edit" title="Edit Category {$cat[a].category}"><img src="/images/admin/edit_16.png" width="16" height="16" /></a></td><td><a href="{$MODULE_FOLDER}/categories.php?action=delete&amp;cat={$cat[a].categoryid}" onclick="return confirm('You are about to delete the category \'{$cat[a].category}\'.  All of its items will go to the default category.\n  \'OK\' to delete, \'Cancel\' to stop.')" class="delete" title="Delete Category {$cat[a].category}"><img src="/images/admin/delete_16.png" width="16" height="16" /></a></td>
				</tr>
{/section}
<tr><td colspan="7"><input type="submit" value="{$lang.save}" class="button" />
                </table>
                <input type="hidden" name="cat" id="cat" value="{$smarty.get.cat}" />
                <input type="hidden" name="mode" id="mode" value="quick_update" />
  <input type="hidden" name="featured" id="featured" value="1" />
  </form>
</div> 

 <div class="wrap seperator padding">
 
 <h2>{$lang.featured_content}</h2>
 <table width="100%" cellpadding="4" cellspacing="1" id="FeaturedTable">

  <tr>
    <td valign="top">
<div>
<a name="featured_add" id="featured_add"></a>
<form name="search_cat" id="search_cat">
<SELECT name="cat" size="10" style="width: 100%" onChange="submitGetCategory('search_cat','cat_results','cat');return false;">
<option value="0">Root Level</option>
{section name=cat_idx loop=$allcategories}
<OPTION value="{$allcategories[cat_idx].categoryid}"{if $smarty.get.cat eq $allcategories[cat_idx].categoryid} selected{/if}>{$allcategories[cat_idx].category_path}{$allcategories[cat_idx].category}</OPTION>
{/section}
</SELECT>
</form>
</div></td>
  </tr>
 <tr>  <td><div id="cat_results" style="border:#000000 solid 1px"></div></td></tr>
 </table>
 <br />
 <div id="related_content"><form name="aForm" id="aForm" method="post">{include file="modules/content/admin/related_content_table.tpl" links=$featured}</form></div>
</div>

{/if}   
<form name="addcat" id="addcat" action="" method="post">               
          <div class="wrap seperator padding"><a name="addcategory" id="addcategory"></a>
          <div id="zeitgeist" style="width:300px;">
  <h3>{$lang.general_settings}</h3>
  <div><img src="{if $category.settings.image}{$category.settings.image}{else}/images/admin/no_photo.jpg{/if}" name="preview" id="preview"  border="1"  style="border:#000000 1px solid" width="120">
  <br />
  <input type="hidden" name="settings_image" id="settings_image" value="{$category.settings.image}" />
  <button type="button" {literal}onClick="OpenFileBrowser('image', function(url) {document.addcat.settings_image.value=url;
document.getElementById('preview').src=document.getElementById('settings_image').value;
}, function() {return document.addcat.settings_image.value;} )">Choose Category Image...{/literal}</button>
<br />
<img src="{if $category.settings.slogan}{$category.settings.slogan}{else}/images/admin/no_photo.jpg{/if}" name="preview_slogan" id="preview_slogan"  border="1"  style="border:#000000 1px solid" width="120"><br />
  <input type="hidden" name="settings_slogan" id="settings_slogan" value="{$category.settings.slogan}" />
  <button type="button" {literal}onClick="OpenFileBrowser('image', function(url) {document.addcat.settings_slogan.value=url;document.getElementById('preview_slogan').src=document.getElementById('settings_slogan').value;}, function() {return document.addcat.settings_slogan.value; } )">Choose Slogan...{/literal}</button>
<br />
  <input type="text" name="settings_cat_alias" id="settings_cat_alias" value="{$category.settings.cat_alias}" /> Category Alias
  <Br />
  <select name="settings_theme">
  {foreach item=a from=$themes}
  <option value="{$a.name}" {if $a.name eq $category.settings.theme}selected{/if}>{$a.title}</option>
  {/foreach}
  </select>
   Select Theme
  <br />
  <input type="checkbox" value="1" name="settings_htaccess" id="settings_htaccess" {if $category.settings.htaccess eq 1}checked{/if} /> Create .htaccess entry
  <br />
  <select name="settings_on_menu">
  <option value="1" {if  $category.settings.on_menu eq 1}selected{/if}>{$lang.yes}</option>
  <option value="0" {if  $category.settings.on_menu eq 0}selected{/if}>{$lang.no}</option>
  </select> Display on main menu
  <br />
{if $more_categories}
<h3>More Categories</h3>
{section name=a loop=$more_categories}
{if $smarty.get.cat eq $more_categories[a].categoryid}<strong>{$more_categories[a].category}</strong>{else}<a href="{$MODULE_FOLDER}/categories.php?action=edit&cat={$more_categories[a].categoryid}">{$more_categories[a].category}</a>{/if}<br />
{/section}
{/if}
</div>

</div>
    <h2>{if $edit eq 1}{$lang.modify} {$category.category} -> <a href="{$MODULE_FOLDER}/categories.php">{$lang.categories}</a>   {include file="modules/content/nav_categories.tpl" management=$lang.categories target="admin_categories"} {else}Add New Category{/if}</h2>
    
         
      <p>{$lang.pos}:<br>
        <input name="orderby" type="text" class="editform" style="background-color: rgb(255, 255, 160);" value="{$category.order_by}" size="4" id="orderby">
         </p>
        <p>Name:<br>
        <input name="title" type="text" style="background-color: rgb(255, 255, 160);" value="{$category.category}">
        </p>
        <p>Alias:<br>
        <input name="alias" type="text" style="background-color: rgb(255, 255, 160);" value="{$category.alias}">
        </p>
        <p>Category parent:<br>
          <select name="parent_catid" class="postform" id="parent_catid">
        <option value="0">{$lang.root_category}</option>    
{section name=cat_num loop=$allcategories}
<option value="{$allcategories[cat_num].categoryid}" {if $allcategories[cat_num].categoryid eq $category.parentid}selected{elseif $allcategories[cat_num].categoryid eq $catid AND !$smarty.get.action}selected{/if}>{$allcategories[cat_num].category}</option>
{/section}
</select></p>
        <p>Description: (optional) <br>
        <textarea name="description" rows="5" cols="60"  id="desc">{$category.description}</textarea>
        <br />
        <a href="javascript:void(0);" class="open-editor" rel="description">Advanced Editor</a>
        </p>

        <p  align="left"><input name="action" value="{$action|default:'addcat'}" type="hidden"><input type="hidden" name="cat" value="{$catid|default:$smarty.get.cat}" /><input name="submit" type="submit" class="button" value="{$lang.save}">
        </p>
    
</div>      
</form>