{include file="modules/recipes/admin/menu.tpl"}
<div id="in_this_section"><strong>In this section :</strong> <a href="#upload">Upload files</a> | <a href="#view">Inspect uploaded files</a></div>
{include file="modules/recipes/admin/additional_links.tpl" mode="nav_menu" base_file="recipes_modify"}
{if $updated}{include file="common/error_list.tpl" mode="message" message=$lang.update_success}{/if}


<div class="wrap seperator">
<div class="padding">
  <h2>Type of media (<a href="#view">Inspect Uploaded media</a>)</h2>
{if $media eq "thumb"}<strong>Item Thumbnail</strong>{else}<a href="{$e_FILE}?id={$id}&media=thumb">Item Thumbnail</a>{/if} | {if $media eq "images"}<strong>Images</strong>{else}<a href="{$e_FILE}?id={$id}&media=images">Images</a>{/if} | {if $media eq "videos"}<strong>Videos</strong>{else}<a href="{$e_FILE}?id={$id}&media=videos">Videos</a>{/if} | {if $media eq "docs"}<strong>Documents</strong>{else}<a href="{$e_FILE}?id={$id}&media=docs"> Documents </a>{/if}
{if $media eq "images"}<div style="margin-left:80px; margin-top:5px;">Image Category : {section name=b loop=$image_categories}
{if $image_categories[b].id ne 0}
{if $image_categories[b].id eq $type}<strong>{$image_categories[b].title}</strong>{else}<a href="{$e_FILE}?id={$id}&media=images&type={$image_categories[b].id}">{$image_categories[b].title}</a> {/if}
{if !$smarty.section.b.last} | {/if}
{/if}
{/section}</div>
{/if}
</div>
<div style="clear:both"></div>
</div>

<TABLE class="open_close_tab seperator">
      <TR>
        <TD id="close8" style="display: none; cursor: hand;" onClick="visibleBox('8')"><IMG src="/skin/images/plus.gif" border="0" align="absmiddle" alt="Click to open"></TD>
        <TD id="open8" style="cursor: hand;" onClick="visibleBox('8')"><IMG src="/skin/images/minus.gif" border="0" align="absmiddle" alt="Click to close"></TD>
        <TD><A href="javascript:void(0);" onClick="visibleBox('8')"><B>Upload Tool</B></A></TD>

      </TR>
      </TABLE>
<div id="box8" class="wrap padding">
<a name="upload" id="upload"></a>

<form name="images_form" id="images_form">
{if $media eq "images"}
<strong>Create Image Set :</strong> <input name="create_set" type="checkbox" id="create_set" value="1" checked="checked" /> 
{/if}
{if $media eq "images" OR $media eq "thumb"}
{assign var="action" value="mass_product_images"}
{elseif $media eq "videos"}
{assign var="action" value="insert_videos"}
{/if}
{include file="common/uploader.tpl" mode="jumploader" form="images_form" id=$id  type=$type WIDTH="100%" HEIGHT="400" action=$action module="recipes" media=$media}
{literal}
<script language="javascript">
function uploaderStatusChanged( uploader ) {
    if( document.jumpLoaderApplet1.getUploader().equals( uploader ) ) {
        // first applet
		if (uploader.isReady()) {
window.location.href = "{/literal}{$e_FILE}?id={$id}&media={$media}&type={$type}{literal}";

}
    } else
    if( document.jumpLoaderApplet2.getUploader().equals( uploader ) ) {
        // second applet
		if (uploader.isReady()) {
		var a = xajax.$('target_div').value;
		var b = xajax.$('target_itemid').value;
		var c = xajax.$('target_videoid').value;
		var d = xajax.$('target_formatid').value;
		updateVideoFormat(a,b,c,d);
		tb_remove();
}
    } 


}

</script> 
{/literal}
</form>

</div>

<div class="wrap seperator padding">
<h2>Uploaded Files</h2>
<a name="view" id="view"></a>
<FORM action="{$e_FILE}#view" method="POST" name="processcategoryform" id="processcategoryform">
{if $media eq "images"}
<input type="hidden" name="media" id="media" value="{$media}"
<div>
{section name=a loop=$image_categories}<span class="{if $image_categories[a].id eq $type}wpv-tab-button{else}wpv-tab-button-disabled{/if}"><a href="{$e_FILE}.php?id={$id}&type={$image_categories[a].id}#view">{$image_categories[a].title}</a></span>
{/section}</div>
{/if}
{if $media eq "images" OR $media eq "thumb"}
<div style="    border: 5px solid #c0c0c0;
    border-top: 10px solid #c0c0c0;
    border-bottom: 10px solid #c0c0c0;
    padding: 5px 5px 5px 5px;
    margin: 0px 0px 0px 0px; ">
<TABLE width="100%" border="0" cellpadding="2" cellspacing="1" class="widefat">



<INPUT type="hidden" name="cat_org" value="{$smarty.get.cat|escape:"html"}"> {assign var="form_name" value="processcategoryform"} 
<TR>
  <th scope="col">&nbsp;</th>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">{$lang.thumb}</th>
  <th scope="col">{$lang.pos}</th>
  <th scope="col">{$lang.availability}</th>
  <th scope="col">{$lang.category}</th>
  <th scope="col">{$lang.alt}</th>
  <th scope="col">{$lang.title}</th>
</TR>
{if $images}

{section name=a loop=$images}
<TR {cycle values=", class='TableSubHead'"}>
<TD width="1%"><a href="{$URL}/{$images[a].main}" target="_blank"><img src="{$URL}/{$images[a].thumb}" width="30" height="30"></a></TD>
<TD width="2%"><input name="ids[{$images[a].id}]" type="checkbox" id="check_values" value="1"></TD>
<TD width="2%" align="center"><input type="radio" name="thumb" id="thumb" value="{$images[a].id}" {if $images[a].type eq 0}checked{/if} /></TD>
<TD width="2%"><input name="orderby-{$images[a].id}" type="text" value="{$images[a].orderby}" size="3" maxlength="3"></TD>
<TD><select name="available-{$images[a].id}">
<option value="0" {if $images[a].available eq "0"}selected{/if}>{$lang.disabled}</option>
<option value="1" {if $images[a].available eq "1"}selected{/if}>{$lang.enabled}</option>
</select></TD>
<TD><select name="type-{$images[a].id}" id="type-{$images[a].id}">
{section name=b loop=$image_categories}
<option value="{$image_categories[b].id}" {if $image_categories[b].id eq $images[a].type}selected{/if}>{$image_categories[b].title}</option>
{/section}
</select></TD>
<TD><input type="text" value="{$images[a].alt}" name="alt-{$images[a].id}"></TD>
<TD><input type="text" value="{$images[a].title}" name="title-{$images[a].id}"></TD>
</TR>






{/section}

{else}

<TR>
<TD colspan="8" align="center">{$lang.txt_no_images}</TD>
</TR>

{/if}
<TR>
<TD colspan="8"><BR>
<INPUT type="button" class="button" onClick="document.processcategoryform.mode.value='modify'; document.processcategoryform.submit();" value="{$lang.update}">
&nbsp;
<input name="button" type="button" class="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete_images"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
&nbsp;
<input name="button" type="button" class="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="mass_functions.php";document.{$form_name}.mode.value="activate_all_images"; document.{$form_name}.submit();{rdelim}' value="Activate ALL" /></TD>
</TR>
</table>
</div>
{elseif $media eq "videos"}
<div style="    border: 5px solid #c0c0c0;
    border-top: 10px solid #c0c0c0;
    border-bottom: 10px solid #c0c0c0;
    padding: 5px 5px 5px 5px;
    margin: 0px 0px 0px 0px; ">
<TABLE width="100%" border="0" cellpadding="2" cellspacing="1" class="widefat">



<INPUT type="hidden" name="cat_org" value="{$smarty.get.cat|escape:"html"}"> {assign var="form_name" value="processcategoryform"} 
<TR>
  <th scope="col">&nbsp;</th>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">{$lang.pos}</th>
  <th scope="col">Filename</th>
  <th scope="col">{$lang.availability}</th>
  <th scope="col">{$lang.title}</th>
  <th scope="col">{$lang.description}</th>
  <th scope="col">Info</th>
</TR>
{if $videos}
<script language=JavaScript>
videosArray = new Array();
</script>
{section name=a loop=$videos}
<TR {cycle values=", class='TableSubHead'"}>
<script language="javascript">
videosArray[{$videos[a].id}] = 1;
thumbsDir[{$videos[a].id}]='';
thumbsArray[{$videos[a].id}] = new Array();
{foreach from=$videos[a].screen_caps item=a name=b}
thumbsArray[{$videos[a].id}][{$smarty.foreach.b.iteration-1}] = '{$URL}{$a}';
{/foreach}
</script>
<TD width="1%"><a href="{$URL}/{$videos[a].thumb}" target="_blank" onmouseover="startPreview({$videos[a].id})" onmouseout="stopPreview({$videos[a].id})"><img src="{$URL}/{$videos[a].thumb}" name="img_src_{$videos[a].id}" width="100" height="100"id="img_src_{$videos[a].id}"></a></TD>
<TD width="2%"><input name="ids[{$videos[a].id}]" type="checkbox" id="check_values" value="1"></TD>
<TD width="2%"><input name="orderby-{$videos[a].id}" type="text" value="{$videos[a].orderby}" size="3" maxlength="3"></TD>
<TD>{$videos[a].filename}</TD>
<TD><select name="available-{$videos[a].id}">
  <option value="0" {if $videos[a].available eq "0"}selected{/if}>{$lang.disabled}</option>
  <option value="1" {if $videos[a].available eq "1"}selected{/if}>{$lang.enabled}</option>
</select></TD>
<TD><input type="text" value="{$videos[a].title}" name="title-{$videos[a].id}"></TD>
<TD><textarea name="description-{$videos[a].id}" rows="3">{$videos[a].description}</textarea></TD>
<TD>Size : {$videos[a].info.width}x{$videos[a].info.height}<br />
Duration : {$videos[a].info.duration}<br />
  FPS : {$videos[a].info.framerate}<br />
  Codec : {$videos[a].info.video_codec}<br />
  Audio : {$videos[a].info.audio_codec|default:"no"}</TD>
</TR>

{if $video_formats}
<tr>
<td colspan="8">
{foreach from=$videos[a].formats item=a name=b}
<div id="format_{$videos[a].id}_{$a.id}">
{if !$a.video_id}<input type="text" id="format_val_{$videos[a].id}_{$a.id}" name="format_val_{$videos[a].id}_{$a.id}" /> <a href="/ajax/load_uploader.php?itemid={$id}&format_id={$a.id}&module={$CURRENT_MODULE.name}&media={$media}&video_id={$videos[a].id}&height=650&width=600&modal=true" class="thickbox">upload {$a.format}</a>
{else}
{include file="modules/recipes/admin/ajax_functions.tpl" mode="videos_aditional" video=$a}
{/if}

</div>
{/foreach}
</td>
</tr>
{/if}


{/section}

{else}

<TR>
<TD colspan="8" align="center">No videos uploaded</TD>
</TR>

{/if}
<TR>
<TD colspan="8"><BR>
<INPUT type="button" class="button" onClick="document.processcategoryform.action.value='modify_videos'; document.processcategoryform.submit();" value="{$lang.update}">
&nbsp;
<input name="button" type="button" class="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete_videos"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
&nbsp;
<input name="button" type="button" class="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="activate_all_videos"; document.{$form_name}.submit();{rdelim}' value="Activate ALL" /></TD>
</TR>
</table>
</div>
{/if}
<input type="hidden" value="{if $smarty.get.id}{$smarty.get.id}{else}{$smarty.post.id}{/if}" name="id">
<input type="hidden" name="action" value="modify">
<input type="hidden" value="{$type}" name="type" />
<input type="hidden" value="{$media}" name="media" />
<input type="hidden" name="page" id="page" value="recipes_modify_img.php?id={$id}&type={$type}&media={$media}#view">
<input type="hidden" name="mode" id="mode" value="">
</FORM>
</div>

