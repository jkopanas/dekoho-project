{include file="$MODULE_FOLDER/menu.tpl"}
{include file="modules/recipes/search_box.tpl" mode="start_page"}
<div class="wrap seperator padding">
  <h2 class="tab">Latest ({$latest|@count} {$lang.items})</h2>
<br><br>
<form name="latest_recipes_form" method="post">
{include file="modules/recipes/admin/display_content_table.tpl" content=$latest type="edit" form_name="latest_recipes_form" page="index.php"}
</form>
</div>