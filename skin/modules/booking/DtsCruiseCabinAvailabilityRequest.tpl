<?xml version="1.0" encoding="utf-8" ?>
<DtsCruiseCabinAvailabilityRequest xmlns="DTS" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <SessionInfo>
    <SessionID>{$Booking.SessionID}</SessionID>
    <Profile>A</Profile>
    <Language>ENG</Language>
    <Version>1</Version>
  </SessionInfo>
  <BookingContext>
    <AgencyID>0000002</AgencyID>
    <BookingContactName>{$Booking.BookingContactName}</BookingContactName>
    <MarketCode>{$Booking.MarketCode}</MarketCode>
    <BookingCurrencyCode>EUR</BookingCurrencyCode>
    <OfficeCode>{$Booking.OfficeCode}</OfficeCode>
  </BookingContext>
  <CruiseComponent>
    <ComponentID>{$Booking.ComponentID}</ComponentID>
    <CategoryCode>{$Booking.CategoryCode}</CategoryCode>
  </CruiseComponent>
</DtsCruiseCabinAvailabilityRequest>