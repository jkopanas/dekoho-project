<div id="templates">

<script id="DtsShopRequestMessage" type="text/kendo-ui-template" data-type="text" data-skip="true">
# app.SetCurrentData(data) #
<div id="DtsShopRequestMessage" class="main">

# if (data.CruiseProducts) { #
<input type="hidden" value="#=data.BookingContext.BookingContactName#" class="UserData" name="BookingContactName" id="BookingContactName"/>
<b> THIS STEP WILL BE OMMITED IN THE LIVE PROCESS. JUST CLICK THE RADIO BUTTON </b> <br/><br/>
<span style="height:200px;">  
<div style=" float:left; height:180px; width:120px;"><img src="#=data.CruiseProducts.CruiseSailing.CruiseInfo.ShipGraphicsUrl#" style="float:left; padding-right:15px; padding-botom:20px; "/> </div>
 
<b>Cruise Info </b><br/>
 Status: #=data.CruiseProducts.CruiseSailing.AvailabilityInfo.AvailabilityStatusDesc# <br/>
Depart. Port: #=data.CruiseProducts.CruiseSailing.CruiseInfo.DepartingPort# <br/>
Sailing Date: #=data.CruiseProducts.CruiseSailing.CruiseInfo.SailingDate# <br/>
ReturnDate Date: #=data.CruiseProducts.CruiseSailing.CruiseInfo.ReturnDate# <br/>
Sailing Length Days: #=data.CruiseProducts.CruiseSailing.CruiseInfo.SailingLengthDays# <br/>
Sailing Time: #=data.CruiseProducts.CruiseSailing.CruiseInfo.SailingTime# <br/>
Ship Name: #=data.CruiseProducts.CruiseSailing.CruiseInfo.ShipName# <br/>
Select this: <input type="radio" value="#=data.CruiseProducts.CruiseSailing.ComponentInfo.ComponentID#" class="UserData" name="ComponentID" id="ComponentID" />
</span><br/><br/>
<br style="clear:both;"/><p class="hr"></p>
<a href="\\#" class="previous" data-template_previous="first-form" data-div="myDiv">previous</a>
<a href="\\#" class="next" data-template="DtsCruisePricingAvailabilityRequest" data-div="myDiv">next</a> 
#} else { #
<b>no cruises found. Please go back and change your criteria</b><br/><br/>
<a href="\\#" class="previous" data-template_previous="test" data-div="myDiv">back</a>

#}#

</div>
</script><!-- END TEMPLATE -->

<script id="DtsCruisePricingAvailabilityRequest" type="text/kendo-ui-template" data-type="text" data-msg="Loading Cruise Prices and Promotions">
# app.SetCurrentData(data) #


<div id="DtsCruisePricingAvailabilityRequest" class="main">
<h3>Your Rate</h3>

# if(data.PricingPromotionsAvailable) { #

   # if (Object.prototype.toString.call(data.PricingPromotionsAvailable)==='[object Array]') { #

<input type="hidden" value="#= data.BookingContext.MarketCode #" class="UserData" name="MarketCode" id="MarketCode"/>
<input type="hidden" value="#= data.BookingContext.OfficeCode #" class="UserData" name="OfficeCode" id="OfficeCode"/>
<input type="hidden" value="#= data.CruiseComponent.ComponentID #" class="UserData" name="ComponentID" id="ComponentID"/>
<input type="hidden" value="#= data.CruiseComponent.SailingLengthDays #" class="UserData" name="SailingLengthDays" id="SailingLengthDays"/>


# if(clientdata.DtsCruisePricingAvailabilityRequest.NoChildren) { #
 # var children = " - "+clientdata.DtsCruisePricingAvailabilityRequest.NoChildren+" child(ren)";#
 # } #



 # $("\\#cruise-info").find("div").last().append('<div class="first-left"> <span>Persons</span> <p>'+ clientdata.DtsCruisePricingAvailabilityRequest.NoAdults  + ' adults'+children+'</p></div>'); # 

#var numb = $(data.PricingPromotionsAvailable).length;#
<div id="avail-promo"><span>We have #= numb # available  promotions rates based on your Criteria. Please select</span></div>
	
 <div class="main-selection">  
	# $.each(data.PricingPromotionsAvailable,function(k,a) { #
	# if(typeof a.PricesStartingAt != "undefined") { #
	
	<div class="pricing">
		<div class="fl-left">
		     <div class="promo-desc-img">
				<input type="radio" value="#=a.CruisePromotionCode#" class="UserData CruisePromotionCodeClick" name="CruisePromotionCode" id="CruisePromotionCode#=a.CruisePromotionCode#" required/>		
				<span class="promo-desc">#=a.CruisePromotionDesc#</span><div class="seperator15"></div>
			   <span class="promo-price">Prices starting From:</span><span class="promo-price-inner"> #=a.PricesStartingAt# &euro;</span> <br/>
					<div class="seperator"></div>
			</div> 
		</div>
	</div>
   

# } #
# }); # 
<br class="clear"/>
	<span class="k-invalid-msg" data-for="CruisePromotionCode"></span>
<div class="spacer-40"></div>
<a href="\\#" class="previous spacer-left back-btn" data-template_previous="first-form" data-div="myDiv">back</a>
<a href="\\#" class="next button blue larger float-right spacer-right-big" data-function="ProcessData" data-template="DtsCruiseCategoryAvailabilityRequest" data-div="myDiv">CONTINUE</a>
<div class="seperator"></div><div class="seperator"></div>
</div>

</div>


# } else { #

# var PreviousClientData = app.clientdata['DtsShopRequestMessage']; #
# app.clientdata['DtsCruiseCategoryAvailabilityRequest']= $.extend(PreviousClientData,{"CruisePromotionCode":data.PricingPromotionsAvailable.CruisePromotionCode,"MarketCode":data.BookingContext.MarketCode,
"OfficeCode":data.BookingContext.OfficeCode,"ComponentID":data.CruiseComponent.ComponentID,"SailingLengthDays":data.CruiseComponent.SailingLengthDays}); #

# app.ExecFunc="ProcessData"; #
# app.RenderPage("DtsCruiseCategoryAvailabilityRequest","myDiv"); #

# } #
# } #
</script><!-- END TEMPLATE -->

<script id="DtsCruiseCategoryAvailabilityRequest" type="text/kendo-ui-template" data-type="text" data-msg="Loading Categories">
# app.SetCurrentData(data); #

#if(data.CruiseComponent) { #
<div id="DtsCruiseCategoryAvailabilityRequest" class="main">
   <input type="hidden" value="#= data.CruiseComponent.SailingLengthDays #" name="SailingLengthDays" id="SailingLengthDays" />
 <div>
# } #
   <h3>Select Stateroom Type</h3/>
		<div id="first-step">
		#if(!data.ProcessedData.SuiteImage && !data.ProcessedData.InteriorImage && !data.ProcessedData.ExteriorImage ) { #
			<span>There are 0 available types for the specified number of persons.</span>
			# } else { #
		 <span>There are #= data.ProcessedData.count# available types. Please Select Your type</span>
			# } # 
			</div>
<div class="seperator"></div><div class="seperator"></div>

<div class="main-selection" id="cat-height">
#if(data.ProcessedData.SuiteImage) { #
<div class="category-item">
<img src="http://www.louiscruises.com#= data.ProcessedData.SuiteImage # " width="188"/>
<div class="category-title">
<span>Suites</span>
  </div>
<div class="category-description">
Prices starting from <span class="red">#= data.ProcessedData.minSuite # - #= data.ProcessedData.maxSuite # &euro;</span> per stateroom for 
#= clientdata.DtsCruiseCategoryAvailabilityRequest.NoAdults # adults # if(clientdata.DtsCruiseCategoryAvailabilityRequest.NoChildren!="0") { # - #= clientdata.DtsCruiseCategoryAvailabilityRequest.NoChildren # child(ren) # } #
</div>
<a id="S" data-type="Suites" data-maxprice="#= data.ProcessedData.maxSuite #" class="RoomSelection button blue large" href="\\#">SELECT</a>
<div class="seperator"></div>
</div>
# } #
#if(data.ProcessedData.InteriorImage) { #
<div class="category-item">
<img src="http://www.louiscruises.com#= data.ProcessedData.InteriorImage # " width="188"/>
<div class="category-title">
<span>Inside</span>
</div>
<div class="category-description">
Prices starting from<span class="red"> #= data.ProcessedData.minInterior # - #= data.ProcessedData.maxInterior # &euro;</span> per stateroom for 
#= clientdata.DtsCruiseCategoryAvailabilityRequest.NoAdults # adults # if(clientdata.DtsCruiseCategoryAvailabilityRequest.NoChildren!="0") { # - #= clientdata.DtsCruiseCategoryAvailabilityRequest.NoChildren # child(ren) # } #
</div> 
<a id="I" data-type="Inside" data-maxprice="#= data.ProcessedData.maxInterior #" class="RoomSelection button blue large" href="\\#">SELECT</a>
<div class="seperator"></div>
</div>
# } #
#if(data.ProcessedData.ExteriorImage) { #
<div class="category-item">
<img src="http://www.louiscruises.com#= data.ProcessedData.ExteriorImage # " width="188"/>
<div class="category-title">
<span>Outside</span>
</div>
<div class="category-description">
Prices starting from <span class="red"> #= data.ProcessedData.minExterior # - #= data.ProcessedData.maxExterior # &euro;</span> per stateroom for 
#= clientdata.DtsCruiseCategoryAvailabilityRequest.NoAdults # adults # if(clientdata.DtsCruiseCategoryAvailabilityRequest.NoChildren!="0") { # - #= clientdata.DtsCruiseCategoryAvailabilityRequest.NoChildren # children # } #
</div>
<a id="X" data-type="Outsise" data-maxprice="#= data.ProcessedData.maxExterior #" class="RoomSelection button blue large" href="\\#">SELECT</a>
<div class="seperator"></div>
</div>
# } #
</div>



<input type="hidden" value="" name="maxprice" id="maxprice" />
<div id="cabininfo"></div>
<br/><br/>

</div>
</script><!-- END TEMPLATE -->

<script id="cabininfo" type="text/kendo-ui-template" data-type="text">
<div class="main">
<h2></h2>

#var persons = parseInt(clientdata.DtsCruiseCategoryAvailabilityRequest.NoAdults) + parseInt(clientdata.DtsCruiseCategoryAvailabilityRequest.NoChildren);#
<div class="main-selection">
#if (typeof data.AvailableCategories =='undefined') {#
	#var i=0;#	
	# $.each(data,function(k,a) { #	
	# if(persons<=a.MaximumOccupancy){ #
	<div class="category-img"><img src="http://www.louiscruises.com#=a.CategoryGraphicsUrl#" width="180" height="120"/></div>
		<div class="float-left">
	  <div class="filter-category-title">  #=a.CategoryCode# Stateroom # if(a.CabinsAvailable==2){ # <span class="red">( only 2 left )</span> # } else if(a.CabinsAvailable==1) { # <span class="red">( only 1 left )</span> # } #
 </div>
		<div class="filter-category-description">
		#=a.CategoryDesc#  for  #= clientdata.DtsCruiseCategoryAvailabilityRequest.NoAdults # adults # if(clientdata.DtsCruiseCategoryAvailabilityRequest.NoChildren!="0") { # - #= clientdata.DtsCruiseCategoryAvailabilityRequest.NoChildren # children # } # <br/><br/>
			
			</div>
			<div>
			<div class="float-left filter-category-price">
				Price: <span class="red"> #=a.TotalCabinPrice# &euro;</span> 
			</div>
		<div class="float-right filter-category-btn">	
		 	<a class="button blue large CabinSelection"  href="\\#" data-value="#= a.CategoryCode #" data-max="# if(a.TotalCabinPrice==a.MAXprice){ #1# } else { #0# } #" data-price="#= a.TotalCabinPrice #" data-id=#=i#>SELECT</a>
			</div>	
			
			</div>

		<input type="hidden" class="UserData" value="#= a.TotalCabinPrice #" id="TotalCabinPrice" name="TotalCabinPrice#= a.CategoryCode #"/>
     </div>


   <br style="clear:both;"/><p class="hr"></p>
    #i++;#
		# } #
  # }); # 
	<input type="hidden" value="" class="UserData" name="CategoryCode" id="CategoryCode" /><br/>
#}#

<div class="category-selection">
</div>
</script>

 <script id="DtsCruiseCabinAvailabilityRequest" type="text/kendo-ui-template" data-type="text"  data-msg="Loading Cabins"> 
<div id="DtsCruiseCabinAvailabilityRequest" class="main">
 #if (data.SelectedCategory.CategoryCode.charAt(0) =="S") { # <h2> Suites Category Stateroom</h2> # } else if(data.SelectedCategory.CategoryCode.charAt(0) =="I") { # <h2>Inside Category Stateroom</h2> # } else { # <h2>Outsise Category Stateroom</h2># } #

<div class="main-selection">
<div class="category-img"><img src="http://www.louiscruises.com#= data.SelectedCategory.CategoryGraphicsUrl #" width="180" height="120"/></div>
	<div class="float-left">
	  <div class="filter-category-title"> 
	    #= data.SelectedCategory.CategoryCode # Stateroom  
	     </div>
	     <div class="filter-category-description">
	     #= data.SelectedCategory.CategoryDesc # for : #= clientdata.DtsCruiseCabinAvailabilityRequest.NoAdults # adults # if(clientdata.DtsCruiseCabinAvailabilityRequest.NoChildren!="0") { # - #= clientdata.DtsCruiseCabinAvailabilityRequest.NoChildren # child(ren) # } #
	     </div>
	     <div class="seperator"></div>
	     <div class="float-left filter-category-price">
				Price: <span class="red"> #=clientdata.DtsCruiseCabinAvailabilityRequest["TotalCabinPrice"+data.SelectedCategory.CategoryCode]# €</span> 
			</div>
	</div>

  <br class="clear"/>
<div class="all-available-cabins float-right">
<div class="filter-category-title">Select Stateroom Number</div> 
# if (typeof data.AvailableCabins !='undefined') { #
  # if (Object.prototype.toString.call(data.AvailableCabins.AvailableCabin)==='[object Array]') { #
      # $.each(data.AvailableCabins.AvailableCabin,function(k,a) { #	
        	<div class="filter-category-description spacer-bottom">
			<div class="red">#= a.CabinNo #</div>
			Bed Arrangement: #= a.BedArrangement #
		    <a class="button blue small SelectCabin float-right"  href="\\#" data-value="#=a.CabinNo#">SELECT</a><br/><br/>
			</div>
			
     # }); # 
  # } else { #
		Bed Arrangement: #= data.AvailableCabins.AvailableCabin.BedArrangement #</br>
		Cabin Number : #= data.AvailableCabins.AvailableCabin.CabinNo #
		<a class="button blue small SelectCabin spacer-left"  href="\\#" data-value="#= data.AvailableCabins.AvailableCabin.CabinNo #">SELECT</a><br/><br/>
   # } #
# } # 
	<input type="hidden" value="" class="UserData" name="CabinNo" id="CabinNo" />
</div>
<br class="clear"/>
</div>
<input type="hidden" value="#=data.SelectedCategory.CategoryCode#" name="CategoryCode" class="UserData"/>
</div>
</script><!-- END TEMPLATE -->


 <script id="BookForm" type="text/kendo-ui-template" data-type="text" data-msg="Loading the Last Step"> 
<div id="BookForm" class="main">

<h3>Contact Details - Personal Data</h3>
	<div id="first-step">
		<span>Please type in all the needed information</span>
	</div>

<div class="main-selection" id="email-fix">

<div class="pass-email">
<span class="ad-ch">e-mail</span>
<input type="email" class="passeng-mail UserData blue" id="Email" name="Email" required>
</div>
</div>

<div class="main-selection">

# for (var i = 0; i < clientdata.DtsCruiseCabinAvailabilityRequest.NoAdults; i++) { #

<h4>Passenger #=i+1# ( Adult )</h4>
<input type="hidden" name="PersonNo#=i#" id=="PersonNo#=i#" value="#=i+1#" class="UserData"/>

<div class="details">
	<div class="detail float-left"><div class="detail-color">Name</div><input type="text" name="FirstName#=i#" id="FirstName#=i#" class="passeng-inpt UserData blue" required validationMessage="Name is required"/></div> 
	<div class="detail float-left"><div class="detail-color">Surname</div><input type="text" name="LastName#=i#" id="LastName#=i#" class="passeng-inpt UserData blue" required validationMessage="SurName is required"/></div>
</div>
<div class="details">
	<div class="detail float-left"><div class="detail-color">Gender</div><select id="Gender#=i#" name="Gender#=i#" class="passeng-sel UserData blue"> <option value="M"> Male </option><option value="F"> Female </option> </select></div>
	<div class="detail float-left"><div class="detail-color">Passenger Type</div><select id="PersonType#=i#" name="PersonType#=i#" class="passeng-sel UserData blue"> <option value="A"> Adult </option> </select></div>
</div>
<div class="details">
		<div class="detail-width float-left"><div class="detail-color">Date Of Birth</div>
# var maxbirthyear = GetCurrentYear() - 16; #
# var minbirthyear  = maxbirthyear - 85; #
	<select id="YearOfBirth#=i#" name="YearOfBirth#=i#" class="passeng-sel UserData blue marg-r"> 
# for (var k = minbirthyear; k <= maxbirthyear; k++) { #
<option value="#= k #">#= k #</option>
# } #
</select>
		<select id="MonthOfBirth#=i#" name="MonthOfBirth#=i#" class="passeng-sel UserData blue marg-r"> 
<option value="01">January</option>
<option value="02">February</option>
<option value="03">March</option>
<option value="04">April</option>
<option value="05">May</option>
<option value="06">June</option>
<option value="07">July</option>
<option value="08">August</option>
<option value="09">September</option>
<option value="10">October</option>
<option value="11">November</option>
<option value="12">December</option>
</select>
<select id="DayOfBirth#=i#" name="DayOfBirth#=i#" class="passeng-sel UserData blue "> 
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select></div>		
</div>

<div class="details">
	<div class="detail float-left"><div class="detail-color">Language</div>
<select id="LanguageCode#=i#" name="LanguageCode#=i#" class="passeng-sel UserData blue"> 
<option value="BOS">BOSNIAN</option>
<option value="BRG">BULGARIAN</option>
<option value="CRO">CROATIAN</option>
<option value="DUT">DUTCH</option>
<option value="ENG">ENGLISH</option>
<option value="FIN">FINNISH</option> 
<option value="FLE">FLEMISH</option>
<option value="FRE">FRENCH</option>
<option value="GER">GERMAN</option>
<option value="GRE">GREEK</option>
<option value="HUN">HUNGERIAN</option>  
<option value="ITA">ITALIAN</option>
<option value="JAP">JAPANESE</option>
<option value="KOR">KOREA</option>
<option value="MLT">MALTESE</option>
<option value="POL">POLISH</option>   
<option value="POR">PORTUGUSE</option>
<option value="ROM">ROMANIAN</option>
<option value="RUS">RUSSIAN</option>
<option value="SER">SERBIAN</option>
<option value="SLO">SLOVENIAN</option>     
<option value="SPA">SPANISH</option>
<option value="SVK">SLOVAKIAN</option>
<option value="SWE">SWEDISH</option>
<option value="TUR">TURKISH</option>
</select> 
</div>
	<div class="detail float-left"><div class="detail-color">Nationality</div>
		<select id="NationalityCode#=i#" name="NationalityCode#=i#" class="passeng-sel UserData blue"> 
<option value="AGO">ANGOLAN</option> 
<option value="ALB">ALBANIAN</option>
<option value="AND">ANDORRAN</option> 
<option value="ARE">UNITED ARAB EMIRATES</option> 
<option value="ARG">ARGENTINE</option>
<option value="ARM">ARMENIAN</option>  
<option value="AUT">AUSTRIAN</option>  
<option value="AZE">AZERBAIJAN</option>
<option value="BEL">BELGIAN</option>
<option value="BEN">BENIN</option> 
<option value="BER">BERMUDIAN</option>
<option value="BFA">BURKINA FASO</option>
<option value="BGD">BANGLADESHI</option>
<option value="BGR">BULGARIAN</option>
<option value="BHR">BAHRAINI</option>
<option value="BHS">BAHAMAS</option>
<option value="BIH">BOSNIAN</option>
<option value="BLR">BELARUSIAN</option>
<option value="BLZ">BELIZEAN</option>
<option value="BOL">BOLIVIAN</option>
<option value="BRA">BRAZILIAN</option>
<option value="BRB">BARBADOS</option>
<option value="CAN">CANADIAN</option>
<option value="CHE">SWISS</option> 
<option value="CHL">CHILEAN</option>    
<option value="CHN">CHINESE</option>    
<option value="CIV">Cote D'Ivoire</option>
<option value="CMR">CAMEROONIAN</option>  
<option value="COD">CONGOLESE</option>  
<option value="COL">COLOMBIAN</option>  
<option value="COM">COMOROS</option>
<option value="CRI">COSTA RICAN</option>
<option value="CUB">CUBAN</option>
<option value="CYP">CYPRIOT</option>   
<option value="CZE">CZECK</option>
<option value="D">GERMAN</option>
<option value="DNK">DANISH</option>
<option value="DOM">DOMINICAN</option>
<option value="DZA">ALGERIAN</option>
<option value="ECU">ECUADORIAN</option>
<option value="EGY">EGYPTIAN</option>
<option value="ERI">ERITREAN</option>
<option value="ESP">SPANISH</option>
<option value="EST">ESTONIA</option>
<option value="ETH">ETHIOPIAN</option>
<option value="FIN">FINNISH</option>
<option value="FJI">FIJIAN</option>
<option value="FRA">FRENCH</option>
<option value="FYR">SKOPJAN</option>
<option value="GBR">BRITISH</option>
<option value="GEO">GEORGIAN</option>
<option value="GHA">GHANA</option>
<option value="GIN">GUINEA</option>
<option value="GRC">GREEK</option>
<option value="GRD">GRENADIAN</option>
<option value="GTM">GUATEMALAN</option>
<option value="HAI">HAITIAN</option>
<option value="HKG">HONG KONG</option>
<option value="HND">HONDURAN</option>
<option value="HRV">CROATIAN</option>
<option value="HTI">HAITI</option>
<option value="HUN">HUNGARIAN</option>
<option value="IDN">INDONESIAN</option>
<option value="IND">INDIAN</option>
<option value="IRL">IRISH</option>
<option value="IRN">IRANIAN</option>
<option value="IRQ">IRAQ</option>
<option value="ISL">ICELANDER</option>
<option value="ISR">ISRAELI</option>
<option value="ITA">ITALIAN</option>
<option value="JAM">JAMAICAN</option>
<option value="JOR">JORDANIAN</option>
<option value="JPN">JAPANESE</option>
<option value="KAZ">KAZAKHSTANI</option>
<option value="KEN">KENYAN</option>
<option value="KGZ">KYRGYZSTAN</option>
<option value="KNA">Saint Kitts and Nevi</option>
<option value="KOR">KOREAN</option>
<option value="KWT">KUWAITI</option>
<option value="LBN">LEBANESE</option>
<option value="LBR">LIBERIAN</option>
<option value="LBY">LIBYAN</option>
<option value="LIE">LIECHTENSTEINER</option>
<option value="LKA">SRI LANKAN</option>
<option value="LSO">LESOTHO</option>
<option value="LTU">LITHUANIAN</option>
<option value="LUX">LUXEMBOURGER</option>
<option value="LVA">LATVIAN</option>
<option value="MAR">MAROCCIAN</option>
<option value="MCO">MONACAN</option>
<option value="MDG">MADAGASCAR</option>
<option value="MDV">MALDIVES</option>
<option value="MEX">MEXICAN</option>
<option value="MKD">FYR</option>
<option value="MLI">MALI</option>
<option value="MLT">MALTESE</option>
<option value="MMR">MYANMAR</option>
<option value="MOL">MOLDOVAN</option>
<option value="MOZ">MOZAMBICAN</option>
<option value="MRT">MAURITIAN</option>
<option value="MUS">MAURITIUS</option>
<option value="MYS">MALAYSIAN</option>
<option value="NAM">NAMIBIA</option>
<option value="NAP">Nepal</option>
<option value="NGA">NIGERIAN</option>
<option value="NIC">NICARAGUAN</option>
<option value="NIG">NIGERIAN</option>
<option value="NLD">DUTCH</option>
<option value="NOR">NORWEGIAN</option>
<option value="NPL">NEPAL</option>
<option value="NZL">NEW ZEALANDER</option>
<option value="OMN">OMAN</option>
<option value="OND">ON DURAN</option>
<option value="PAK">PAKISTANI</option>
<option value="PAL">PALESTINIAN</option>
<option value="PAN">PANAMANIAN</option>
<option value="PER">PERUVIAN</option>
<option value="PHL">PHILIPPINO</option>
<option value="PNG">PAPUA NEW GUINEA</option>
<option value="POL">POLISH</option>
<option value="PRT">PORTUGUESE</option>
<option value="PRY">PARAGUAYAN</option>
<option value="PSE">PALESTINIAN</option>
<option value="PUE">PUERTO RICAN</option>
<option value="QAT">QATARI</option>
<option value="ROM">ROMANIAN</option>
<option value="RUS">RUSSIAN</option>
<option value="SAL">SALVADORIAN</option>
<option value="SAR">S.ARABIAN</option>
<option value="SAU">SAUDI ARABIAN</option>
<option value="SDN">SUDANESE</option>
<option value="SEN">SENEGALESE</option>
<option value="SGP">SINGAPOREAN</option>
<option value="SLV">EL SALVADORAN</option>
<option value="SOM">SOMALIA</option>
<option value="SRB">SERBIAN</option>
<option value="SUR">SURINAME</option>
<option value="SVK">SLOVAKIAN</option>
<option value="SVN">SLOVENIAN</option>       
<option value="SWE">SWEDISH</option>    
<option value="SYC">SEYCHELLES</option>     
<option value="SYR">SYRIAN</option>
<option value="TAN">TANZANIAN</option>       
<option value="TGO">TOGO</option>    
<option value="THA">THAI</option>     
<option value="TRI">TRINIDADIAN</option>     
<option value="TTO">TRINIDAD AND TOBAGO</option>       
<option value="TUN">TUNISIAN</option>    
<option value="TUR">TURKISH</option>     
<option value="TWN">TAIWAN</option>              
<option value="UGA">UGANDA</option>       
<option value="UKR">UKRAINIAN</option>    
<option value="URY">URUGUAYAN</option>     
<option value="USA">AMERICAN</option>    
<option value="UZB">UZBEKISTAN</option>       
<option value="VEN">VENEZUELAN</option>    
<option value="VNM">VIETNAMESE</option>     
<option value="YEM">YEMEN</option>        
<option value="YUG">YUGASLAVIAN</option>    
<option value="ZAF">SOUTH AFRICAN</option>       
<option value="ZAR">ZAIROISE</option>    
<option value="ZMB">ZAMBIAN</option>     
<option value="ZWE">ZIMBABWEAN</option> 
</select> 
</div>
</div>

<div class="details">
	<div class="detail float-left"><div class="detail-color">Passport No</div><input type="text" name="PassportNo#=i#" id="PassportNo#=i#" class="passeng-inpt UserData blue" required validationMessage="Passport No is required"/></div>
	
</div>
<div class="details">
		<div class="detail-width float-left"><div class="detail-color">Passport Expires</div>
# var currentpassyear = GetCurrentYear(); #
# var maxpassyear = GetCurrentYear()+10; #
<select id="YearOfPassport#=i#" name="YearOfPassport#=i#" class="passeng-sel UserData blue marg-r"> 
# for (var l = currentpassyear; l <= maxpassyear; l++) { #
<option value="#= l #">#= l #</option>
# } #
</select>

<select id="MonthOfPassport#=i#" name="MonthOfPassport#=i#" class="passeng-sel UserData blue marg-r"> 
<option value="01">January</option>
<option value="02">February</option>
<option value="03">March</option>
<option value="04">April</option>
<option value="05">May</option>
<option value="06">June</option>
<option value="07">July</option>
<option value="08">August</option>
<option value="09">September</option>
<option value="10">October</option>
<option value="11">November</option>
<option value="12">December</option>
</select>


<select id="DayOfPassport#=i#" name="DayOfPassport#=i#" class="passeng-sel UserData blue"> 
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select>
		</div>
		</div>
<br/><br/>

<br style="clear:both;"/>

# } # 
<br/><br/>
# if (clientdata.DtsCruiseCabinAvailabilityRequest.NoChildren) { #

#var adults_count = parseInt(clientdata.DtsCruiseCabinAvailabilityRequest.NoAdults); #

# for (var i = 0; i < clientdata.DtsCruiseCabinAvailabilityRequest.NoChildren; i++) { #

<h4>Passenger #=adults_count+i+1# ( child )</h4>
<input type="hidden" name="ChildPersonNo#=i#" id=="ChildPersonNo#=i#" value="#=adults_count+i+1#" class="UserData"/>

<div class="details">
	<div class="detail float-left"><div class="detail-color">Name</div><input type="text" name="ChildFirstName#=i#" id="ChildFirstName#=i#" class="passeng-inpt UserData blue" required validationMessage="Name is required"/></div> 
	<div class="detail float-left"><div class="detail-color">Surname</div><input type="text" name="ChildLastName#=i#" id="ChildLastName#=i#" class="passeng-inpt UserData blue" required validationMessage="SurName is required"/></div>
</div>
<div class="details">
	<div class="detail float-left"><div class="detail-color">Gender</div><select id="ChildGender#=i#" name="ChildGender#=i#" class="passeng-sel UserData blue"> <option value="M"> Male </option><option value="F"> Female </option> </select></div>
	<div class="detail float-left"><div class="detail-color">Passenger Type</div><select id="PersonType#=i#" name="ChildPersonType#=i#" class="passeng-sel UserData blue"> <option value="C"> Child </option> </select></div>
</div>
<div class="details">
		<div class="detail-width float-left"><div class="detail-color">Date Of Birth</div>
# var minchildbirthyear = GetCurrentYear() - 16; #
# var maxchildbirthyear  = GetCurrentYear(); #
	<select id="ChildYearOfBirth#=i#" name="ChildYearOfBirth#=i#" class="passeng-sel UserData blue marg-r"> 
# for (var m = minchildbirthyear; m <= maxchildbirthyear; m++) { #
<option value="#= m #">#= m #</option>
# } #
</select>
		<select id="ChildMonthOfBirth#=i#" name="ChildMonthOfBirth#=i#" class="passeng-sel UserData blue marg-r"> 
<option value="01">January</option>
<option value="02">February</option>
<option value="03">March</option>
<option value="04">April</option>
<option value="05">May</option>
<option value="06">June</option>
<option value="07">July</option>
<option value="08">August</option>
<option value="09">September</option>
<option value="10">October</option>
<option value="11">November</option>
<option value="12">December</option>
</select>
<select id="ChildDayOfBirth#=i#" name="ChildDayOfBirth#=i#" class="passeng-sel UserData blue"> 
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select></div>		
</div>

<div class="details">
	<div class="detail float-left"><div class="detail-color">Language</div>
<select id="ChildLanguageCode#=i#" name="ChildLanguageCode#=i#" class="passeng-sel UserData blue"> 
<option value="BOS">BOSNIAN</option>
<option value="BRG">BULGARIAN</option>
<option value="CRO">CROATIAN</option>
<option value="DUT">DUTCH</option>
<option value="ENG">ENGLISH</option>
<option value="FIN">FINNISH</option> 
<option value="FLE">FLEMISH</option>
<option value="FRE">FRENCH</option>
<option value="GER">GERMAN</option>
<option value="GRE">GREEK</option>
<option value="HUN">HUNGERIAN</option>  
<option value="ITA">ITALIAN</option>
<option value="JAP">JAPANESE</option>
<option value="KOR">KOREA</option>
<option value="MLT">MALTESE</option>
<option value="POL">POLISH</option>   
<option value="POR">PORTUGUSE</option>
<option value="ROM">ROMANIAN</option>
<option value="RUS">RUSSIAN</option>
<option value="SER">SERBIAN</option>
<option value="SLO">SLOVENIAN</option>     
<option value="SPA">SPANISH</option>
<option value="SVK">SLOVAKIAN</option>
<option value="SWE">SWEDISH</option>
<option value="TUR">TURKISH</option>
</select> 
</div>
	<div class="detail float-left"><div class="detail-color">Nationality</div>
		<select id="ChildNationalityCode#=i#" name="ChildNationalityCode#=i#" class="passeng-sel UserData blue"> 
<option value="AGO">ANGOLAN</option> 
<option value="ALB">ALBANIAN</option>
<option value="AND">ANDORRAN</option> 
<option value="ARE">UNITED ARAB EMIRATES</option> 
<option value="ARG">ARGENTINE</option>
<option value="ARM">ARMENIAN</option>  
<option value="AUT">AUSTRIAN</option>  
<option value="AZE">AZERBAIJAN</option>
<option value="BEL">BELGIAN</option>
<option value="BEN">BENIN</option> 
<option value="BER">BERMUDIAN</option>
<option value="BFA">BURKINA FASO</option>
<option value="BGD">BANGLADESHI</option>
<option value="BGR">BULGARIAN</option>
<option value="BHR">BAHRAINI</option>
<option value="BHS">BAHAMAS</option>
<option value="BIH">BOSNIAN</option>
<option value="BLR">BELARUSIAN</option>
<option value="BLZ">BELIZEAN</option>
<option value="BOL">BOLIVIAN</option>
<option value="BRA">BRAZILIAN</option>
<option value="BRB">BARBADOS</option>
<option value="CAN">CANADIAN</option>
<option value="CHE">SWISS</option> 
<option value="CHL">CHILEAN</option>    
<option value="CHN">CHINESE</option>    
<option value="CIV">Cote D'Ivoire</option>
<option value="CMR">CAMEROONIAN</option>  
<option value="COD">CONGOLESE</option>  
<option value="COL">COLOMBIAN</option>  
<option value="COM">COMOROS</option>
<option value="CRI">COSTA RICAN</option>
<option value="CUB">CUBAN</option>
<option value="CYP">CYPRIOT</option>   
<option value="CZE">CZECK</option>
<option value="D">GERMAN</option>
<option value="DNK">DANISH</option>
<option value="DOM">DOMINICAN</option>
<option value="DZA">ALGERIAN</option>
<option value="ECU">ECUADORIAN</option>
<option value="EGY">EGYPTIAN</option>
<option value="ERI">ERITREAN</option>
<option value="ESP">SPANISH</option>
<option value="EST">ESTONIA</option>
<option value="ETH">ETHIOPIAN</option>
<option value="FIN">FINNISH</option>
<option value="FJI">FIJIAN</option>
<option value="FRA">FRENCH</option>
<option value="FYR">SKOPJAN</option>
<option value="GBR">BRITISH</option>
<option value="GEO">GEORGIAN</option>
<option value="GHA">GHANA</option>
<option value="GIN">GUINEA</option>
<option value="GRC">GREEK</option>
<option value="GRD">GRENADIAN</option>
<option value="GTM">GUATEMALAN</option>
<option value="HAI">HAITIAN</option>
<option value="HKG">HONG KONG</option>
<option value="HND">HONDURAN</option>
<option value="HRV">CROATIAN</option>
<option value="HTI">HAITI</option>
<option value="HUN">HUNGARIAN</option>
<option value="IDN">INDONESIAN</option>
<option value="IND">INDIAN</option>
<option value="IRL">IRISH</option>
<option value="IRN">IRANIAN</option>
<option value="IRQ">IRAQ</option>
<option value="ISL">ICELANDER</option>
<option value="ISR">ISRAELI</option>
<option value="ITA">ITALIAN</option>
<option value="JAM">JAMAICAN</option>
<option value="JOR">JORDANIAN</option>
<option value="JPN">JAPANESE</option>
<option value="KAZ">KAZAKHSTANI</option>
<option value="KEN">KENYAN</option>
<option value="KGZ">KYRGYZSTAN</option>
<option value="KNA">Saint Kitts and Nevi</option>
<option value="KOR">KOREAN</option>
<option value="KWT">KUWAITI</option>
<option value="LBN">LEBANESE</option>
<option value="LBR">LIBERIAN</option>
<option value="LBY">LIBYAN</option>
<option value="LIE">LIECHTENSTEINER</option>
<option value="LKA">SRI LANKAN</option>
<option value="LSO">LESOTHO</option>
<option value="LTU">LITHUANIAN</option>
<option value="LUX">LUXEMBOURGER</option>
<option value="LVA">LATVIAN</option>
<option value="MAR">MAROCCIAN</option>
<option value="MCO">MONACAN</option>
<option value="MDG">MADAGASCAR</option>
<option value="MDV">MALDIVES</option>
<option value="MEX">MEXICAN</option>
<option value="MKD">FYR</option>
<option value="MLI">MALI</option>
<option value="MLT">MALTESE</option>
<option value="MMR">MYANMAR</option>
<option value="MOL">MOLDOVAN</option>
<option value="MOZ">MOZAMBICAN</option>
<option value="MRT">MAURITIAN</option>
<option value="MUS">MAURITIUS</option>
<option value="MYS">MALAYSIAN</option>
<option value="NAM">NAMIBIA</option>
<option value="NAP">Nepal</option>
<option value="NGA">NIGERIAN</option>
<option value="NIC">NICARAGUAN</option>
<option value="NIG">NIGERIAN</option>
<option value="NLD">DUTCH</option>
<option value="NOR">NORWEGIAN</option>
<option value="NPL">NEPAL</option>
<option value="NZL">NEW ZEALANDER</option>
<option value="OMN">OMAN</option>
<option value="OND">ON DURAN</option>
<option value="PAK">PAKISTANI</option>
<option value="PAL">PALESTINIAN</option>
<option value="PAN">PANAMANIAN</option>
<option value="PER">PERUVIAN</option>
<option value="PHL">PHILIPPINO</option>
<option value="PNG">PAPUA NEW GUINEA</option>
<option value="POL">POLISH</option>
<option value="PRT">PORTUGUESE</option>
<option value="PRY">PARAGUAYAN</option>
<option value="PSE">PALESTINIAN</option>
<option value="PUE">PUERTO RICAN</option>
<option value="QAT">QATARI</option>
<option value="ROM">ROMANIAN</option>
<option value="RUS">RUSSIAN</option>
<option value="SAL">SALVADORIAN</option>
<option value="SAR">S.ARABIAN</option>
<option value="SAU">SAUDI ARABIAN</option>
<option value="SDN">SUDANESE</option>
<option value="SEN">SENEGALESE</option>
<option value="SGP">SINGAPOREAN</option>
<option value="SLV">EL SALVADORAN</option>
<option value="SOM">SOMALIA</option>
<option value="SRB">SERBIAN</option>
<option value="SUR">SURINAME</option>
<option value="SVK">SLOVAKIAN</option>
<option value="SVN">SLOVENIAN</option>       
<option value="SWE">SWEDISH</option>    
<option value="SYC">SEYCHELLES</option>     
<option value="SYR">SYRIAN</option>
<option value="TAN">TANZANIAN</option>       
<option value="TGO">TOGO</option>    
<option value="THA">THAI</option>     
<option value="TRI">TRINIDADIAN</option>     
<option value="TTO">TRINIDAD AND TOBAGO</option>       
<option value="TUN">TUNISIAN</option>    
<option value="TUR">TURKISH</option>     
<option value="TWN">TAIWAN</option>              
<option value="UGA">UGANDA</option>       
<option value="UKR">UKRAINIAN</option>    
<option value="URY">URUGUAYAN</option>     
<option value="USA">AMERICAN</option>    
<option value="UZB">UZBEKISTAN</option>       
<option value="VEN">VENEZUELAN</option>    
<option value="VNM">VIETNAMESE</option>     
<option value="YEM">YEMEN</option>        
<option value="YUG">YUGASLAVIAN</option>    
<option value="ZAF">SOUTH AFRICAN</option>       
<option value="ZAR">ZAIROISE</option>    
<option value="ZMB">ZAMBIAN</option>     
<option value="ZWE">ZIMBABWEAN</option> 
</select> 
</div>
</div>

<div class="details">
	<div class="detail float-left"><div class="detail-color">Passport No</div><input type="text" name="ChildPassportNo#=i#" id="ChildPassportNo#=i#" class="passeng-inpt UserData blue" required validationMessage="Passport No is required"/></div>
	
</div>
<div class="details">
		<div class="detail-width float-left"><div class="detail-color">Passport Expires</div>
# var currentpassyear = GetCurrentYear(); #
# var maxpassyear = GetCurrentYear()+10; #
<select id="ChildYearOfPassport#=i#" name="ChildYearOfPassport#=i#" class="passeng-sel UserData blue marg-r"> 
# for (var n = currentpassyear; n <= maxpassyear; n++) { #
<option value="#= n #">#= n #</option>
# } #
</select>

<select id="ChildMonthOfPassport#=i#" name="ChildMonthOfPassport#=i#" class="passeng-sel UserData blue marg-r"> 
<option value="01">January</option>
<option value="02">February</option>
<option value="03">March</option>
<option value="04">April</option>
<option value="05">May</option>
<option value="06">June</option>
<option value="07">July</option>
<option value="08">August</option>
<option value="09">September</option>
<option value="10">October</option>
<option value="11">November</option>
<option value="12">December</option>
</select>


<select id="ChildDayOfPassport#=i#" name="ChildDayOfPassport#=i#" class="passeng-sel UserData blue"> 
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select>
		</div>
		</div>
<br/><br/>

<br style="clear:both;"/>
# } # 
#}#
<br class="clear">
<a href="\\#" data-div="myDiv" data-template="DtsBookRequestMessage" class="next button blue larger float-right spacer-right-big" >CONTINUE</a>
<div class="seperator"></div>
</div>

</div>
</script><!-- END TEMPLATE -->

<script id="DtsBookRequestMessage" type="text/kendo-ui-template" data-type="text" data-msg="Loading the Last Step">
<div id="DtsBookRequestMessage" class="main"> 

<h3>Payment information</h3>

<div class="main-selection">
<div id="payment-text">
<form method="post" action="http://www.louisres.com/cgi-bin/testres.sh/webinit.p?SCR=PM2&FCT=ADDPMT" id="checkout">
<p>
Dear Customer the total amount for our cruise is <span class="red"> #= data.BookingInfo.BookingCharges.TotalGrossPrice # #= data.BookingContext.BookingCurrencyCode # </span>.<div class="seperator"></div>
The minimum amount you need to pay so as to confirm your booking is <div class="seperator"></div>
<input type="text" value="#= data.BookingInfo.BookingCharges.DepositAmountDue #" name="TxtAmount" id="TxtAmount" class="payment-inpt blue"/> #= data.BookingContext.BookingCurrencyCode # .<br/><br/><br/>
If you choose to pay a deposit you will have to pay the rest amount until <span class="red">#= data.BookingInfo.BookingCharges.FinalPaymentDate #</span><br/><br/><br/>
If you want to pay as a deposit a bigger amount than <span class="red">#= data.BookingInfo.BookingCharges.DepositAmountDue # #= data.BookingContext.BookingCurrencyCode #</span> or if you want can make a <br/>
whole payment now type in the above box an amount from <span class="red">#= data.BookingInfo.BookingCharges.DepositAmountDue # - #= data.BookingInfo.BookingCharges.TotalGrossPrice # #= data.BookingContext.BookingCurrencyCode # </span>
<br/><br/><br/>
<input type="hidden" name="CONTEXT_SES-ID" id="CONTEXT_SES-ID" value="#= data.SessionInfo.SessionID #"/>
<input type="hidden" name="CONTEXT_PROFILE" id="CONTEXT_PROFILE" value="PUB"/>
<input type="hidden" name="CONTEXT_USER-ID" id="CONTEXT_USER-ID" value="PUBLIC"/>
<input type="hidden" name="CONTEXT_AGENT-ID" id="CONTEXT_AGENT-ID" value="lhcxml"/>
<input type="hidden" name="CONTEXT_BKGNO" id="CONTEXT_BKGNO" value="#= data.BookingContext.BookingNo #"/>
<input type="hidden" name="CONTEXT_CURRENCY" id="CONTEXT_CURRENCY" value="#= data.BookingContext.BookingCurrencyCode #"/>
<input type="hidden" name="CONTEXT_OFFICE" id="CONTEXT_OFFICE" value="#= data.BookingContext.OfficeCode #"/>
<input type="hidden" name="TxtCurrcy" id="TxtCurrcy" value="#= data.BookingContext.BookingCurrencyCode #"/>
<input type="hidden" name="TxtPtp" id="TxtPtp" value="CC"/>
<input type="hidden" name="ReturnURL" id="ReturnURL" value="http://marketers.gr/bookNow.php"/>
<br class="clear"/><div class="spacer-40"></div>
<a href="\\#"class="button blue larger float-right spacer-right-big" id="proceed-checkout" >CONTINUE</a>
<div class="seperator"></div>
<div id="hidden-msg-proceed"></div>
<p>
</form>

<input type="hidden" name="MinDeposit" id="MinDeposit" value="#= data.BookingInfo.BookingCharges.DepositAmountDue #"/>
<input type="hidden" name="MaxDeposit" id="MaxDeposit" value="#= data.BookingInfo.BookingCharges.TotalGrossPrice #"/>
</div>
</div>
</div>
</script>



<script id="loadingimg" type="text/kendo-ui-template" data-type="text">
<div id="popup-msg">
<h3>Luis Cruises... Starts</h3>
<h4>Please Wait...</h4>
<h5>#= message #</h5>
</div>
</script>


<script id="upgrade" type="text/kendo-ui-template" data-type="text">

<div class="UpgradePopUp">
<div class="seperator"></div>
<div class="seperator"></div>
<h2>Upgrade Now</h2>
<div class="seperator"></div>
<div class="float-left imgUpgrade spacer-right">
<span class="title">from</span>
<div class="seperator"></div>
<img src="http://www.louiscruises.com#= data.CurrentElement.CategoryGraphicsUrl#" width="150"  />
<br style="clear:both;"/>
<span class="cat">#= data.CurrentElement.CategoryCode # Stateroom</span>
<div class="seperator"></div>
<div class="seperator"></div>
<span class="desc">
#= data.CurrentElement.CategoryDesc #
</span>
<div class="seperator"></div>
<div class="seperator"></div>
<span>Price:
<span class="price">
#= data.CurrentElement.TotalCabinPrice # &euro;</span> 
</span>
</div>
<div class="float-left imgUpgrade">
<span class="title">to</span>
<div class="seperator"></div>
<img src="http://www.louiscruises.com#= data.NextElement.CategoryGraphicsUrl#" class="spacer-left" width="150" />
<br style="clear:both;"/>
<span class="cat">#= data.CurrentElement.CategoryCode # Stateroom</span>
<div class="seperator"></div>
<div class="seperator"></div>
<span class="desc">
#= data.CurrentElement.CategoryDesc #
</span>
<div class="seperator"></div>
<div class="seperator"></div>
<span>Price:
<span class="price">
#= data.NextElement.TotalCabinPrice # &euro;</span> 
</span>
</div>

<div class="ButtonPrice">
<span class="onlyfor">only for <span class="price">#= data.ExtraPrice #</span> &euro; per day.</span><br/><br/>
<a href="\\#" class="button blue large UpgradeCategory" name="NextElementCategoryCode" id="#= NextElement.CategoryCode#">YES, UPGRADE</a>
<div class="seperator"></div>
<a href="\\#" class="UpgradeCategory thanks" name="CurrentElementCategoryCode" id="#= CurrentElement.CategoryCode#">No thanks</a>
</div>
<input type="hidden" class="userData" value="#= NextElement.TotalCabinPrice #" id="TotalCabinPrice1" name="TotalCabinPrice#= NextElement.CategoryCode #"/>
<input type="hidden" class="userData" value="#= CurrentElement.TotalCabinPrice #" id="TotalCabinPrice2" name="TotalCabinPrice#= CurrentElement.CategoryCode #"/>
<input type="hidden" id="hiddencat" class="UserData" value="" name="CategoryCode" />
</div>
</script>

</div>