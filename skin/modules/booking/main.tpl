
<input type="hidden" value="{$session_id}" class="UserData" name="SessionID" id="SessionID"/>
<input type="hidden" value="B2C-Booking" class="UserData" name="BookingContactName" id="BookingContactName"/>


<div class="top-links">
			<ul>
				<li><a href="/en/index.html">Home </a></li>				
								<li><a class="active" href="#">Book your Cruise</a></li>
				
			</ul>
		</div>
         
      <br class="clear"/>
      <div class="spacer-40"></div>
      <div class="blueHeader">
     		 <h2>
     		 	<span class="firstHeader">1.<span id="HeaderBr" class="UpdateHeader firstHeader"> Your Cruise&nbsp;&nbsp;</span></span>
     		 	<span>2.<span id="HeaderBrDtsCruiseCategoryAvailabilityRequest" class="hidden UpdateHeader"> Your Stateroom&nbsp;&nbsp;</span></span>
     		 	<span>3.<span id="HeaderBrBookForm" class="hidden UpdateHeader"> Your Data&nbsp;&nbsp;</span></span>
     		 	<span>4.<span id="HeaderBrDtsBookRequestMessage" class="hidden UpdateHeader"> Payment&nbsp;&nbsp;</span></span>
     		 	<span>5.<span id="HeaderBr" class="hidden UpdateHeader">&nbsp;&nbsp;</span></span>
     		 </h2>
      </div>
         
<div class="seperator"></div><div class="seperator"></div><div class="seperator"></div>
 
      <div class="booking">
<div class="left_column">
		<div id="myDiv">
		<div id="first-form" class="main">
			<h3>how many guests will be travelling in this stateroom? </h3>
			<div id="first-step">
			 <span>Standard staterooms allow for a maximum of four people</span>
			 </div>
			
		<div class="main-selection">
		
		<span class="ad-ch">Adults</span>
		<select id="NoAdults" name="NoAdults" class="UserData selectpersons blue">
		  <option value=1>1 Adult</option>
		  <option value=2 selected="selected">2 Adults</option>
		  <option value=3>3 Adults</option>
		  <option value=4>4 Adults</option>
		</select> <br/><br/>
		
		<span class="ad-ch1">Children</span>
		<select id="NoChildren" name="NoChildren" class="UserData selectpersons blue">
		  <option  value=0>0 children</option>
		  <option value=1>1 child</option>
		  <option value=2>2 children</option>
		  <option value=3>3 children </option>
		</select> <br/><br/>
		<div  class="inputs spacer-bottom"></div>
		<br class="clear"/>
		<a href="#" class="next button blue larger float-right spacer-right-big" data-trigger="BeforeRender" data-function="SendRequest" data-template="DtsShopRequestMessage" data-div="myDiv">CONTINUE</a>
		<div class="seperator"></div>
		</div>
		</div>
			</div> <!-- end myDiv -->
		<div class="seperator"></div><div class="seperator"></div><div class="seperator"></div>
	  </div> <!-- left -->
	  <div class="right_column">
	  <div class="blueHeader-right">
                	<h2>Your Cruise</h2>
                </div>
	  <div id="cruise-info">	
	  <h6>{$cruise.itineraryTitle}</h6>
	  <div class="first-left"><span>{$cruise.sailingDays} day cruise</span></div>
	  <div class="left-text"><span>{$cruise.itineraryDescription} </span></div>
	  <div class="seperator"></div>
	  <div class="first-left"><span>Ship</span><p>{$cruise.shipName}</p></div>
	  <div class="seperator"></div>
	  <div class="first-left"><span>Depart at</span><p>{$cruise.sailingDateTime|date_format:"%A, %B %e, %Y"}</p></div>
	  <div class="seperator"></div>
	  </div>
	  <div id="safe"><span>You are safe with us</span><div class="seperator"></div>
	  <img width="110" height="41" alt="" src="/images/site/add6.gif">
	  <img width="135" height="31" alt="" src="/images/site/add7.gif">
	  </div>
	  </div>
	  </div>
	  <br class="clear"/>
<div class="spacer-40"></div>
<div id="windowpopup"></div>
<script type="text/javascript">
head.js('/scripts/kendo/js/kendo.web.min.js');
head.js('/scripts/site/validate_booking.js');
head.js('/scripts/site/booking_plugin.js');
head.js('/scripts/site/script.js');
</script>

 <style scoped>
 				.k-numerictextbox {
 					width: 90px;
 				}

                .k-numeric-wrap {    
                    width: 60px;
                    margin:0px;
                }
                .k-numeric-wrap span.k-tooltip {
                	position: absolute;
                	top:50px;
                	left:0px;
                }

                .k-numerictextbox .k-link {
                	padding-top: 6px;

                }
                div.k-window {
                	padding-top: 0px;
                }
                div.k-window-content {
                	padding:0px;
                }
            </style>