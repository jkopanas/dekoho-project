<?xml version="1.0" encoding="UTF-8" ?>
<DtsShopRequestMessage>
  <SessionInfo xmlns="DTS">
    <SessionID>{$Booking.SessionID}</SessionID>
    <Profile>A</Profile>
    <Language>ENG</Language>
    <Version>1</Version>
  </SessionInfo>
  <AdvisoryInfo xmlns="DTS"/>
  <BookingContext>
    <BookingContactName>{$Booking.BookingContactName}</BookingContactName>
    <BookingCurrencyCode>EUR</BookingCurrencyCode>
  </BookingContext>
  <PricingShopInfo>
    <NoAdults>{$Booking.NoAdults}</NoAdults>
    <NoChildren>{$Booking.NoChildren}</NoChildren>
    {if $Booking.NoChildren gt 0}
    <ChildInfo>
    {section name=a loop=$Booking.NoChildren step=1}
    <ChildAge>{$Booking['ChildAge'|cat:$smarty.section.a.index]}</ChildAge>
    {/section}
    </ChildInfo>
    {/if}
  </PricingShopInfo>
  <CruiseShop>
  <PricingMethod>Actual</PricingMethod>
    <BookingType>IND</BookingType>
    <CruiseID>LC03130315</CruiseID>
  </CruiseShop>
</DtsShopRequestMessage>