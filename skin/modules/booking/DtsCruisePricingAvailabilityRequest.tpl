<?xml version="1.0" encoding="UTF-8" ?>
<DtsCruisePricingAvailabilityRequest>
  <SessionInfo>
    <SessionID>{$Booking.SessionID}</SessionID>
    <Profile>A</Profile>
    <Language>ENG</Language>
    <Version>1</Version>
  </SessionInfo>
  <BookingContext>
    <AgencyID>0000002</AgencyID>
    <BookingContactName>{$Booking.BookingContactName}</BookingContactName>
    <BookingCurrencyCode>EUR</BookingCurrencyCode>
  </BookingContext>
  <CruiseComponent>
    <ComponentID>{$Booking.ComponentID}</ComponentID>
  </CruiseComponent>
</DtsCruisePricingAvailabilityRequest>
