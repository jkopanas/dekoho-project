<div class="seperator"></div>
<div class="content-middle-full">
<div class="chef-header spacer-bottom">
<img src="{$user.user_image}" title="{$user.user_name[0]} {$user.user_name[1]}" class="float-left spacer" />
<div class="float-left">
<h1 class="cpanel-head-smaller">{$user.user_name[0]} {$user.user_name[1]}</h1>
<h1 class="cpanel-head-smaller indent">{if $mode eq "articles"}Η στήλη μου{elseif $mode eq "recipes"}Συνταγές από μένα{elseif $mode eq "step_by_step"}Βήμα - Βήμα{elseif $mode eq "tips"}Δάσκαλε που δίδασκες!{else}Αμαρτίες...{/if}</h1>
</div><!-- END RIGHT -->

<div class="clear"></div>
</div><!-- END HEADER -->
<div class="seperator"></div>

{if !$mode}
<h1 class="big-header">Αμαρτίες...</h1>
<div class="bio spacer-top">{$user.profile.profile.bio}</div>
{elseif $mode eq "recipes"}
<h1 class="big-header spacer-bottom">Συνταγές από μένα</h1>
{include file="modules/themes/recipes_items.tpl" items=$recipes}
{elseif $mode eq "articles" OR $mode eq "tips"}
{if $item}
<h1 class="big-header">{$item.title}</h1>
<div class="description list">
{if $item.image_full.big_thumb}<img src="{$item.image_full.big_thumb}" title="{$item.title}" alt="{$item.title}" align="left" class="spacer" />{/if} {$item.description}
</div><!-- END DESCRIPTION -->
<div class="clear"></div>
{literal}
<script>

		   var i =0;
$('ol li').each(function( intIndex ){		   
$(this).html('<p>'+$(this).html()+'</p>');
i = i+1;
$(this).prepend('<span class="commentnumber"> #'+i+'</span>');
});

</script>
{/literal}
{else}
{foreach from=$items item=a}
<div class="article">
{if $a.image_full}
<a href="/page_{$a.id}.html" title="{$a.title}"><img src="{$a.image_full.thumb}" title="{$a.title}" alt="{$a.title}" align="left" /></a>
{/if}
<div class="float-left article-description"><h1><a href="/chefs/{$user.uname}/{$mode}/{$a.id}.html" title="{$a.title}">{$a.title}</a></h1><p>{$a.description|strip_tags|truncate:200:"..."}</p></div>
<div class="clear"></div>
<ul>
<li>Σχόλια : <a HREF="/page_{$a.id}.html#comments" title="{$a.title}">{$a.count_comments|default:0}</a></li>
{if $a.provider.uid}
<li> / από <a HREF="/author_{$a.provider.uid}.html" title="διαβάστε όλα τα άρθρα του χρήστη {$a.provider.user_data.user_name}">{$a.provider.user_data.user_name}</a></li>
{/if}
</ul>
<div class="clear"></div>
</div><!-- END ARTICLE -->
<div class="seperator"></div>
{/foreach}
{/if} {* END ARTICLES *}
{elseif $mode eq "step_by_step"}
{if $images}
<h1 class="big-header">{$item.title}</h1>
{if $item.description}
<p>{$item.description}</p>
<div class="seperator"></div>
{/if}
<div id="gallery" class="ad-gallery spacer-top">
      <div class="ad-image-wrapper">
      </div>
      <div class="ad-controls">
      </div>
      <div class="ad-nav">
        <div class="ad-thumbs">
          <ul class="ad-thumb-list">
          {foreach from=$images item=a name=b}
            <li>
              <a href="{$a.main}">
                <img src="{$a.thumb}" class="image{$smarty.foreach.b.iteration}" title="{$a.title}" alt="{$a.alt}">
              </a>
            </li>
            {/foreach}
          </ul>
        </div>
      </div>

    </div>
  {/if}
 {if $items AND !$images}
<div id="step_by_step">
{foreach from=$items item=a}
<div class="article">
{if $a.image_full}
<a href="/page_{$a.id}.html" title="{$a.title}"><img src="{$a.image_full.thumb}" title="{$a.title}" alt="{$a.title}" align="left" /></a>
{/if}
<div class="float-left article-description"><h1><a href="/chefs/{$user.uname}/step_by_step{$a.id}.html" title="{$a.title}">{$a.title}</a></h1><p>{$a.description|strip_tags|truncate:200:"..."}</p></div>
<div class="clear"></div>
</div>
{/foreach}

</div><!-- END STEP BY STEP -->

{/if}
{/if}{* end mode if *}

</div><!-- END MIDDLE -->
<div class="recipe-far-right">
<div class="efields-container spacer-bottom">
<ul>
<li>{if !$mode}Αμαρτίες...{else}<a href="/chefs/{$user.uname}/index.html" title="{$user.user_name[0]} {$user.user_name[1]} βιογραφικό">Αμαρτίες...</a>{/if}</li>
{if "recipes"|in_array:$options_available}<li>{if $mode eq "recipes"}Συνταγές από μένα{else}<a href="/chefs/{$user.uname}/recipes.html" title="συνταγές ζαχαροπλαστικής από {$user.user_name[0]} {$user.user_name[1]}">Συνταγές από μένα</a>{/if}</li>{/if}
{if "tips"|in_array:$options_available}<li>{if $mode eq "tips"}Δάσκαλε που δίδασκες!{else}<a href="/chefs/{$user.uname}/tips.html">Δάσκαλε που δίδασκες!</a>{/if}</li>{/if}
{if "gallery"|in_array:$options_available}<li>{if $mode eq "gallery"}Τα πειστήρια{else}<a href="/chefs/{$user.uname}/gallery.html">Τα πειστήρια</a>{/if}</li>{/if}
{if "step_by_step"|in_array:$options_available}<li>{if $mode eq "step_by_step"}Βήμα - Βήμα{else}<a href="/chefs/{$user.uname}/step_by_step.html">Βήμα - Βήμα</a>{/if}</li>{/if}
{if "articles"|in_array:$options_available}<li>{if $mode eq "articles"}Η στήλη μου{else}<a href="/chefs/{$user.uname}/articles.html">Η στήλη μου</a>{/if}</li>{/if}
</ul>
</div>
<div class="spacer-top">{include file="modules/themes/ads.tpl" place="right_sidebar"}</div>
 {if $items}
 <h2 class="spacer-top">Δείτε επίσης</h2>
<div class="efields-container">
<ul>
{foreach from=$items item=a}
<li><a href="/chefs/{$user.uname}/{$mode}/{$a.id}.html" title="{$a.title}">{$a.title}</a></li>
{/foreach}
</ul>
</div><!-- END STEP BY STEP -->

{/if}

</div><!-- END RIGHT -->