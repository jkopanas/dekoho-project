                <!-- Sidebar -->
{*
                <aside class="grid_1">

                    <nav class="global">
                        <ul class="clearfix">

                            <li {if $action eq "addAudience"}class="active"{/if}><a href="/userCP/addAudience.html" class="nav-icon icon-house">{$lang.add}</a></li>
                            <li {if $action eq "audience"}class="active"{/if}><a href="/userCP/audience.html" class="nav-icon icon-book">{$lang.lbl_audience}</a></li>
                            <li {if $action eq "sendSMS"}class="active"{/if}><a href="/userCP/sendSMS.html" class="nav-icon icon-mobile">{$lang.lbl_sendSMS}</a></li>
                            <li {if $action eq "myOrders"}class="active"{/if}><a href="/userCP/myOrders.html" class="nav-icon icon-orders">{$lang.lbl_myOrders}</a></li>
                            <li {if $action eq "myAccount"}class="active"{/if}><a href="/userCP/myAccount.html" class="nav-icon icon-account">{$lang.lbl_myAccount}</a></li>
                        </ul>
                    </nav>

                    <nav class="subnav recent">
                        <h4>Recent Contacts</h4>
                        <ul class="clearfix">
                            <li>
                                <a class="contact" href="profile.html"><h5>John Doe</h5><h6>Some Company LTD</h6></a>
                                <div class="tooltip left">
                                    <span class="avatar">
                                    </span>
                                    <h5>John Doe</h5>
                                    <h6>Some Company LTD</h6>
                                    <address>123 Some Street, LA</address>
                                </div>
                            </li>
                            <li>
                                <a class="contact" href="profile.html"><h5>Jane Roe</h5><h6>Other Company Inc.</h6></a>
                                <div class="tooltip left">
                                    <span class="avatar">
                                    </span>
                                    <h5>Jane Roe</h5>
                                    <h6>Other Company Inc.</h6>
                                    <address>456 Other Street, LA</address>
                                </div>
                            </li>
                        </ul>
                    </nav>


                </aside>
*}
                <!-- Sidebar End -->
<section class="main-section grid_7">
                    <div class="main-content grid_5 alpha">
                        <header>
                            <ul class="action-buttons clearfix fr">
                                <li><a href="#" class="button button-gray no-text current" title="View as a List" onclick="$(this).addClass('current').parent().siblings().find('a').removeClass('current');$('#contacts').removeClass('grid-view').addClass('list-view');return false;">List View<span class="view-list"></span></a></li>
                                <li><a href="#" class="button button-gray no-text" title="View as a Grid" onclick="$(this).addClass('current').parent().siblings().find('a').removeClass('current');$('#contacts').removeClass('list-view').addClass('grid-view');return false;">Grid View<span class="view-grid"></span></a></li>
                                <li><a href="documentation/index.html" class="button button-gray no-text help" rel="#overlay">Help<span class="help"></span></a></li>
                            </ul>
                            <div class="view-switcher">
                                <h2>{$lang.lbl_allLists} <a href="#">&darr;</a></h2>
                                <ul>
                                    <li><a href="#">{$lang.all}</a></li>
                                    {foreach from=$lists item=a}
                                    <li><a href="#" rel="{$a.id}">{$a.title}</a></li>
                                    {/foreach}
                                </ul>
                            </div>
                        </header>
                        <section>
                        <section>
                            <div class="searchBar spacer-bottom">
                            <form id="filtersForm">
<table cellpadding="5" cellspacing="5" class="datatable full" width="100%">
<TR>
  <TD>{$lang.results_per_page}</TD>
  <TD><select name="results_per_page" id="results_per_page" class="secondarySearchData">
    <option value="20" {if $data.results_per_page eq 20}selected{/if}>20</option>
    <option value="60" {if $data.results_per_page eq 60}selected{/if}>60</option>
    <option value="100" {if $data.results_per_page eq 100}selected{/if}>100</option>
  </select>  </TD>
</TR>
<TR>
  <TD>{$lang.order_by}</TD>
  <TD> <select name="orderby" class="secondarySearchData">
   <option value="date_added">{$lang.date_added}</option>
   <option value="name">{$lang.user_name}</option>
   <option value="surname">{$lang.surname}</option>
   <option value="id">#ID</option> 
   <option value="mobile">{$lang.lbl_mobileNumber}</option> 
   <option value="email">email</option> 
</select>  
{$lang.way} : 
<select name="way" id="sort_direction" class="secondarySearchData">
<option value="desc">{$lang.descending}</option>
<option value="asc">{$lang.ascending}</option>             
        </select></TD>
</TR>
<tr><td>{$lang.user_name}</td><td><input type="text" name="name"  class="likeData autocomplete" autocomplete="off" /></td></tr>
<tr><td>{$lang.surname}</td><td><input type="text" name="surname" class="likeData autocomplete" autocomplete="off" /></td></tr>
<tr>
  <td>{$lang.lbl_mobileNumber}</td>
  <td><input type="text" name="mobile" class="likeData autocomplete" data-lookUp="mobile" autocomplete="off" /></td>
</tr>
<tr>
  <td>{$lang.email}</td>
  <td><input type="text" name="email" class="likeData autocomplete" data-lookUp="email" autocomplete="off" /></td>
</tr>
<tr>
  <td>{$lang.date_added}</td>
  <td>{$lang.from} : <input type="text" id="dateFrom" name="dateFrom" class="datePicker secondarySearchData smallBox" size="30" autocomplete="off"/> {$lang.to} : <input type="text" id="dateTo" name="dateTo" class="datePicker secondarySearchData smallBox" size="30" autocomplete="off"/> </td>
</tr>
<tr><td>{$lang.lbl_group}</td><td>
<select name="listid" id="audienceLists" class="SearchData multiSelect" size="5" multiple="multiple">
    <option value="">{$lang.all}</option>
					{foreach from=$lists item=a}
                    <option value="{$a.id}">{$a.title}</option> 
                     {/foreach}
	      </select>
</td></tr>

<tr>
  <td colspan="2"><p><button class="button button-blue addMoreAudience">{$lang.filter}</button></p></td>
  </tr>
</table>
    <input name="page" type="hidden" id="page" value="{$page}" class="secondarySearchData">
    <input name="ResultsDiv" type="hidden"  value="#ResultsPlaceHolder" class="secondarySearchData">
    <input name="FormToHide" type="hidden"  value="#SearchTable" class="secondarySearchData">
</form>
</div><!-- END SEARCH BAR -->
</section>
                      <div  class="separator"></div>
                      				
                      <div id="ResultsPlaceHolder">
						{include file="modules/users/audienceList.tpl" class="audiencePage" target="filtersForm"}
                            </div><!-- END PLACE HOLDER -->
                        </section>
                    </div>

                    <div class="preview-pane grid_2 omega">
                        <div class="content">
                            <h3>Preview Pane</h3>
                            <p>This is the preview pane. Click on the more button on an item to view more information.</p>
                            <div class="message info">
                                <h3>Helpful Tips</h3>
                                <img src="images/lightbulb_32.png" class="fl" />
                                <p>Phasellus at sapien eget sapien mattis porttitor. Donec ultricies turpis pulvinar enim convallis egestas. Pellentesque egestas luctus mattis. Nulla eu risus massa, nec blandit lectus. Aliquam vel augue eget ante dapibus rhoncus ac quis risus.</p>
                            </div>
                        </div>
                        <div class="preview">
                        </div>
                    </div>

                </section>

<script src="/scripts/jquery.multiselect.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.multiselect.filter.min.js"></script>