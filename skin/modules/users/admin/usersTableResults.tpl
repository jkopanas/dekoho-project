<form>
<div class="seperator padding wrap">
<h2>{$lang.user_accounts} [ {$list.from} {$lang.to} {$list.to} {$lang.of_total} : {$list.total} ] ( <a href="index.php#addcategory">Νέος λογαριασμός</a> )</h2>
<table width="100%"  border="0" class="widefat">
  <tr>
  <td colspan="8">
  {$lang.withSelectedDo} (<span class="messages">0</span> επιλέχθηκαν) : <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ActivateChecked">{$lang.enable}</option>
        <option value="DeactivateChecked">{$lang.disable}</option>
        <option value="DeleteChecked">{$lang.delete}</option>
        </select>
         <input type="button" class="button userActions" value="{$lang.apply}" />
         <input type="button" class="button selectAllResults hidden" value="{$lang.selectAllResults}" alt="#searchUsersForm">
         
         </td>
  </tr>
       <tr class="nodrop nodrag hidden" id="selectAllResultsInfo">
        <td colspan="8">
    		<div class="messagePlaceHolder messageWarning">{$lang.allResultsSelected}</div>
            </td>
      </tr>    
        <TR class="TableHead" {cycle values=", class='TableSubHead'"}>
    <TD><input type="checkbox" id="total_check" name="ids[1]" class="check-all"  /></TD>
    <td width="10"><strong>ID</strong></td>
    <td><strong>{$lang.uname}</strong></td>
    <td><strong>{$lang.user_name}</strong></td>
    <td><strong>{$lang.email}</strong></td>
    <td><strong>{$lang.admin}</strong></td>
    <td><strong>{$lang.availability}</strong></td>
    <td><strong>{$lang.date_added}</strong></td>

  </tr>
{foreach from=$users item=a}
  <TR {cycle values=", class='alternate'"} id="{$a.id}">
<TD width="1%"><input name="id" type="checkbox" id="check_values" value="{$a.id}" class="check-values" /></TD>
<td width="10"><strong>{$a.id}</strong></td>
    <td id="uname" class="change"><a href="#" class="editUser" rel="{$a.id}">{$a.uname}</a></td>
    <td id="user_name" class="change">{$a.user_name} {$a.user_surname}</td>
    <td id="email" class="change">{$a.email}</td>
    <td id="admin" class="change bool">{if $a.admin eq 1}<span style="color:green">{$lang.yes}</span>{else}<span style="color:red">{$lang.no}</span>{/if}</td>
    <td id="user_login" class="change bool">{if $a.user_login eq 1}<span style="color:green">{$lang.yes}</span>{else}<span style="color:red">{$lang.no}</span>{/if}</td>
    <td id="user_join" class="change">{$a.user_join|date_format:"%d/%m/%Y"}</td>
    </tr>
  {/foreach}
    <tr>
  <td colspan="8">{$lang.withSelectedDo} (<span class="messages">0</span> επιλέχθηκαν ) : <select name="actions" id="CheckedActions" class="CheckedActions">
        <option value="ActivateChecked">{$lang.enable}</option>
        <option value="DeactivateChecked">{$lang.disable}</option>
        <option value="DeleteChecked">{$lang.delete}</option>
        </select>
         <input type="button" class="button userActions" value="{$lang.apply}" /></td>
  </tr>
  </table>
</div>
<input type="hidden" class="CheckedActions" value="" name="allItemsSelected" />
<input name="page" type="hidden" id="page" value="{$page}" class="CheckedActions">
{if $list.total gt $users|@count}
{include file="modules/content/pagination.tpl" mode="ajax" class="userPage" target="searchUsersForm"}
{/if}
</form>