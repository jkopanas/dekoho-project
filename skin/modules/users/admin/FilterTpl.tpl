<div name="FilterForm-{$ParentSettings}" id="FilterForm-{$ParentSettings}" class="formEl_a hidden">
{foreach from=$boxFilter item=a name=b}
	<div class="float-left spacer-left" >
		<fieldset>
                <legend>{$a.title}</legend>
                        	{include file=$a.file}
        </fieldset>
	</div>
	{if $a@iteration is div by 2}<div class="clear"></div>{/if}
{/foreach}
      	<input type="hidden"  id="modules-{$ParentSettings}" data-bind="value: module" />
        <div class="spacer-right spacer-top clear">
        	<center><input name="submit" type="button"  id="Search-{$ParentSettings}"  class="btn btn_a fSearch" value="{$lang.search}" /></center> 
        </div>
</div>

        <script type="text/x-kendo-template" id="template-toolbar-{$ParentSettings}">
                <div data-id="#= id #" id="Filter-{$ParentSettings}" class="k-button k-button-icontext Filter float-right">
                    Filter&nbsp;&nbsp;<span class=" k-icon k-filter"></span>
                </div>
          </script>
          
<script type="text/javascript" src="/scripts/admin2/users/user_search.js"></script>
          