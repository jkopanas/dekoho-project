<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/skin/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/scripts/jquery-1.5.min.js"></script>
<script src="/scripts/common.js"></script>
<script src="/scripts/user.js"></script>
</head>

<body>
<input type="hidden" name="loaded_language" value="{$BACK_LANG}" />
{include file=$include_file}
</body>
</html>
