{if $ID}
{if $no_products}
Δεν έχετε προϊόντα στο καλάθι σας.
{else}
{$data.order_name}
<ul id="mainNav" class="threeStep">
		<li class="lastDone"><a title="Βήμα 1: Καλάθι αγορών" href="/cart.html"><em>Βήμα 1: Καλάθι αγορών</em><span>δείτε συνοπτικά την παραγγελία σας</span></a></li>
		<li class="current"><a title=""><em>Βήμα 2: Πληρωμή</em><span>Επιλογή τρόπου πληρωμής</span></a></li>
        <li class="mainNavNoBg"><a title=""><em>Βήμα 3: Ολοκλήρωση</em><span>Τέλος παραγγελίας</span></a></li>
	</ul>
<div class="clear seperator"></div>
<div class="float_right spacer">
  <h2>Τελικό ποσό πληρωμής : <strong class="price">&euro;<span class="total">{$total_price|formatprice:".":","}</span></strong></h2>
  <h3 class="hidden installments-container price_small">επιβάρυνση δόσεων : &euro; <span class="total_extra">0</span></h3>
  <div class="shipping_method_text hidden">Τρόπος αποστολής : <strong><span></span></strong></div>
  </div>

<div class="seperator"></div>  
<form action="{$payment_methods[0].processor.url}" method="post" name="details_form" id="full-form" class="order_form"> 
<div class="box">
<h1 class="header">Απόδειξη πληρωμής</h1>
<div class="content ">

<ul id="invoice-type">
<li><input type="radio" value="reciept" name="invoice_type" id="receipt" class="invoice_method save" checked /> <label for="receipt">Απόδειξη</label></li>
<li><input type="radio" value="invoice" name="invoice_type" id="invoice" class="invoice_method save" /> <label for="invoice">Τιμολόγιο</label></li>
</ul>

<table cellpadding="5" cellspacing="5" class=" invoice_table"><tr><td  valign="middle" width="50%">Επωνυμία:</td><td  valign="middle">
					<input gtbfieldid="37" size="40" name="company_name" value="" type="text" class="save">
					</td></tr><tr><td  valign="middle" width="50%">Επάγγελμα:</td><td  valign="middle">
					<input gtbfieldid="38" size="40" name="proffession" value="" type="text" class="save">
					</td></tr>
					<tr><td  valign="middle" width="50%">Διεύθυνση:</td><td  valign="middle">
					<input gtbfieldid="39" size="40" name="address" value="" type="text" class="save">
					</td></tr>
					<tr><td  valign="middle" width="50%">Τ.Κ.:</td><td  valign="middle"><input gtbfieldid="41" size="40" name="postcode" value="" type="text" class="save"></td></tr>
					<tr><td  valign="middle" width="50%">ΑΦΜ:</td><td  valign="middle">
					<input gtbfieldid="40" size="40" name="afm" value="" type="text" class="save">
					</td></tr>
					<tr><td  valign="middle" width="50%">ΔΟΥ:</td><td  valign="middle"><input gtbfieldid="41" size="40" name="doy" value="" type="text" class="save"></td></tr>
					<tr><td  valign="middle" width="50%">τηλέφωνο εταιρίας:</td><td  valign="middle">

					<input gtbfieldid="42" size="40" name="phone" value="" type="text" class="save">
					</td></tr>
					</table>
</div><!-- END CONTENT -->
</div><!-- END BOX -->
  <div class="seperator"></div>
<div class="box">
<h1 class="header">Πληρωμή με</h1>
<div class="content ">
<ul>
{foreach from=$payment_methods item=a name=b}
<li><input type="radio" value="{$a.id}" name="payment_method" id="payment{$a.id}" class="payment_method save {if $a.settings.installments}has-installments{/if}" {if $smarty.foreach.b.first}checked{/if} /> <label for="payment{$a.id}">{$a.title}</label></li>
{/foreach}
</ul>
<div class="clear"></div>

</div><!-- END CONTENT -->
</div><!-- END BOX -->
<div class="error spacer-top hidden" id="no_method">
	<ol> 
	</ol> 
</div><div id="debug" class="hidden"></div>
<div class="installments box spacer-top spacer-bottom {if !$payment_methods[0].settings.installments}hidden{/if}" id="installments">
<h1 class="header">Δόσεις</h1>
<div class="content">
<select name="rates">
<option value="0" title="0">Επιλέξτε αριθμό δόσεων</option>
{foreach item=a from=$rates}
<option value="{$a.monthly_charge}" title="{$a.payments}" rel="{$a.payments}">{$a.payments} δόσεις {if $a.monthly_charge eq 0}(άτοκα){/if}</option>
{/foreach}
</select>
<div class="monthly_charge hidden spacer-top">Από <strong>&euro; <span class="installment"></span></strong> το μήνα</div>
</div>
</div><!-- END BOX -->
<div class="seperator"></div>  
<div class="box">
<h1 class="header">Μέθοδος Αποστολής</h1>
<div class="content">

{foreach from=$payment_methods item=a name=b}
<div id="payment_method_div{$a.id}" class="payment_method_div {if !$smarty.foreach.b.first}hidden{/if}">
<div class="float-right">{$a.description}</div>
<div class="options">
<input type="hidden" value="{$a.processor.url}" id="url{$a.id}" />
{foreach from=$a.shipping item=c name=n key=k}
<h2>{$c.parent_array.shipping}</h2>
<ul id="shipping-methods" class="{$c.parent_array.settings.form}-form">
{foreach from=$c.methods item=x name=s}
<li><input type="radio" value="{$x.id}" name="shipping" id="s_{$x.id}" class="shipping_method save"  alt="0" /> <label for="s_{$x.id}">{$x.shipping}</label></li>
{/foreach}
</ul>
{/foreach}
</div><!-- END OPTIONS -->
<div class="clear"></div>
</div><!-- END TAB -->
{/foreach}
<input type="hidden" name="total_amount" id="total_amount" value="{$total_price}" />
<input type="hidden" name="original_amount" id="original_amount" value="{$total_price}" />
<div class="mini_wrap clear">
{foreach from=$shipping item=x}
<table width="100%" border="1" cellspacing="4" cellpadding="4" id="fields-{$x.id}" class="hidden fields">
{foreach from=$x.settings.fields item=a key=k}
<tr>
<td>{$a.label}</td>
<td>{include file="common/formFields/`$a.type`.tpl" field=$a prefix=$prefix classes='save' val=$val[$a.varName]}</td>
</tr>
{/foreach}
</table>
{/foreach}
</div>
</div><!-- END CONTENT -->
</div><!-- END BOX -->
<div class="spacer-top" align="center"><input type="submit" name="checkOut-Button" value="Ολοκλήρωση παραγγελίας" class="submit-order"/><input type="hidden" name="act" value="local" /></div>
   <input type="hidden" Name="APACScommand" value="NewPayment"> 
        <INPUT TYPE="hidden" Name="merchantID" VALUE="90001999"> 
        <INPUT TYPE="hidden" Name="amount" VALUE="{$int_amount}"> 
        <INPUT TYPE="hidden" Name="merchantRef" VALUE="{$orderid}" > 
        <INPUT TYPE="hidden" Name="merchantDesc" VALUE="sddssd"> 
        <INPUT TYPE="hidden" Name="currency" VALUE="0978"> <!--0978 equals Euro--> 
        <INPUT TYPE="hidden" Name="lang" VALUE="GR"> 
        <INPUT TYPE="hidden" Name="Var1" VALUE="{$productids}"> 
        <INPUT TYPE="hidden" Name="Var2" VALUE="VAR"> 
        <INPUT TYPE="hidden" Name="Var3" VALUE="reciept"> 
        <INPUT TYPE="hidden" Name="Var4" VALUE="{$payment_methods[0].id}"> 
        <INPUT TYPE="hidden" NAME="Var5" VALUE="VAR"> 
        <INPUT TYPE="hidden" NAME="Var6" VALUE="VAR"> 
        <INPUT TYPE="hidden" Name="Var7" VALUE="VAR"> 
        <INPUT TYPE="hidden" Name="Var8" VALUE="VAR"> 
        <INPUT TYPE="hidden" Name="Var9" VALUE="VAR"> 
        <INPUT TYPE="hidden" Name="CustomerEmail" VALUE="mbouclas@gmail.com"> 
        <INPUT TYPE="hidden" Name="Period"  VALUE="0" class="save">
        <INPUT TYPE="hidden" Name="ExtraCharge"  VALUE="0" class="save">
</form>

<script>head.js("/scripts/site/eshop.js");</script>
{/if}{* PRODUCTS *}
{else}
{include file="modules/themes/members.tpl"}
{/if}