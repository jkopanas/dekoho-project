<ul id="mainNav" class="threeStep">
		<li class="current"><a title="Βήμα 1: Καλάθι αγορών" href="/cart.html"><em>Βήμα 1: Καλάθι αγορών</em><span>δείτε συνοπτικά την παραγγελία σας</span></a></li>
		<li><a title="Βήμα 2: Πληρωμή"><em>Βήμα 2: Πληρωμή</em><span>Επιλογή τρόπου πληρωμής</span></a></li>
        <li class="mainNavNoBg"><a title=""><em>Βήμα 3: Ολοκλήρωση</em><span>Τέλος παραγγελίας</span></a></li>
	</ul>
<div class="clear seperator"></div>
<h1>Η παραγγελία σας</h1>

<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th>Διαγραφή</th>
    <th>#ID</th>
    <th>Προϊόν</th>
    <th>Τιμή μονάδας με Φ.Π.Α</th>
    <th>Ποσότητα</th>
    <th>Σύνολο</th>
  </tr>
  {foreach from=$items item=a key=k name=b}
  <tr>
    <td align="center"><a href="javascript:void(0);" class="remove-item" rel="{$k}"><img src="/images/delete.jpg" width="16" height="16" /></a></td>
    <td><strong>{$k}</strong></td>
    <td><a href="/product/{$k}/{$a.permalink}.html">{$a.title}</a></td>
    <td class="price">&euro; {$a.price|number_format:2:".":","}</td>
    <td align="center"><input type="input" name="quantity-{$k}" id="quantity-{$k}" value="{$a.quantity|default:0}" class="quantity" size="2" maxlength="4" alt="{$k}|{$a.price}"  disabled /></td>
    <td align="right" class="price">&euro; <span id="total-{$k}">{$a.total|number_format:2:".":","}</span></td>
  </tr>
  {/foreach}
  
  <tr>
    <td class="seperator"></td>
  <td class="seperator"></td></tr>
  <tr><td colspan="6" align="right">Μερικό Σύνολο : <strong>&euro;<span class="total-price-no-vat">{$total_price_no_vat|number_format:2:".":","}</span></strong></td></tr>
  <tr><td colspan="6" align="right">Αξία Φ.Π.Α : <strong>&euro;<span class="vat-price">{$vat_price|number_format:2:".":","}</span></strong></td></tr>
  <tr><td colspan="6" align="right">Γενικό Σύνολο : <strong class="price">&euro;<span class="total-price">{$total_price|number_format:2:".":","}</span></strong></td></tr>
  <tr>
    <td class="seperator"></td>
  <td class="seperator"></td></tr>
  <tr><td colspan="6" align="right"><a href="/empty_cart.html"  class="large button blue spacer-right" rel="{$a.id}">Αφαίρεση όλων</a> <a href="/{$FRONT_LANG}/checkout.html"  class="large button blue" rel="{$a.id}">Ολοκλήρωση παραγγελίας &raquo;</a></td></tr>
</table>
<script>head.js("/scripts/site/eshop.js");</script>