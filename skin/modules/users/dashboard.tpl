<section class="main-section grid_7">

<div class="main-content">
                        <header>
                            <ul class="action-buttons clearfix fr">
                                <li><a href="documentation/index.html" class="button button-gray no-text help" rel="#overlay">Help<span class="help"></span></a></li>
                            </ul>
                            <h2>
                              {$lang.welcome} {$UNAME}
                            </h2>
                        </header>
                        <section class="container_6 clearfix">
                            <div class="grid_6">
                                <div class="message info ac">
                                    <h3>Get started: <a href="#">Add contacts to your account</a></h3>
                                    <p>Vestibulum ultrices vehicula leo ac tristique. Mauris id nisl nibh. Cras egestas vestibulum nisl, nec eleifend nunc pulvinar non.</p>
                                </div>
                            </div>
                            </section>



<section class="container_6 clearfix">
                            <div class="grid_6">
                                <h3>Table with pagination and sorting</h3>

                                <hr />

                                <table class="datatable paginate sortable full">
                                    <thead>
                                        <tr>
                                            <th>Browser</th>
                                            <th>Platform</th>
                                            <th>Table Cell</th>
                                            <th>Table Cell</th>
                                            <th>Table Cell</th>
                                            <th style="width:70px"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Firefox 3.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.0</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.6</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.6</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.6</td>
                                            <td>Ubuntu</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chrome 6.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chrome 7.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chrome 7.0</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 6.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 7.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 8.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 9.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Safari 5.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Safari 5.0</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Opera 9.5</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.0</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.6</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.6</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.6</td>
                                            <td>Ubuntu</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chrome 6.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chrome 7.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chrome 7.0</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 6.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 7.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 8.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 9.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Safari 5.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Safari 5.0</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Opera 9.5</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.0</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.6</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.6</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.6</td>
                                            <td>Ubuntu</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chrome 6.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chrome 7.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chrome 7.0</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 6.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 7.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 8.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 9.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Safari 5.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Safari 5.0</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Opera 9.5</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.0</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.6</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.6</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Firefox 3.6</td>
                                            <td>Ubuntu</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chrome 6.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chrome 7.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chrome 7.0</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 6.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 7.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 8.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Internet Explorer 9.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Safari 5.0</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Safari 5.0</td>
                                            <td>OS X</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Opera 9.5</td>
                                            <td>Windows</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>Table Cell</td>
                                            <td>
                                                <ul class="action-buttons">
                                                    <li><a href="#" class="button button-gray no-text"><span class="pencil"></span>Edit</a></li>
                                                    <li><a href="#" class="button button-gray no-text"><span class="bin"></span>Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                            
                        </section>
           
</div>
</section>
