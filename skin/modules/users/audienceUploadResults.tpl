<div class="resultsDiv">
{if $res.duplicates}
<div class="message warning"><h3>{$lang.lbl_success}!</h3>
<p>{$lang.lbl_setupCsvHeaders}</p>
</div>

<div class="hidden" id="Duplicates">
<h3>Duplicate mobile numbers</h3>
<ul>
{foreach from=$res.duplicates.mobile item=a key=k}
<li>{$k}
<ul>
{foreach from=$a item=b key=key}
<li>Row {$key} : {$b.name} <strong class="errorMessage">{$b.mobile}</strong> </li>
{/foreach}
</ul>
</li>
{/foreach}
</ul>

</div>
Total uploaded : <strong>{$res.total}</strong><br>
{if $res.dupesMobile}Duplicate mobile numbers : <strong>{$res.dupesMobile}</strong> ( <a href="{$res.dupesMobileFile}" target="_blank">Download Files</a> ) <a href="#" class="Slide" rel="#Duplicates">Show conflicts</a><br>{/if}
{if $res.dupesEmail}Duplicate email addresses : <strong>{$res.dupesEmail}</strong> ( <a href="{$res.dupesEmailFile}" target="_blank">Download Files</a> )<br>{/if}

{else}
<div class="message success"><h3>{$lang.lbl_success}!</h3>
<p>{$lang.lbl_setupCsvHeaders}</p>
</div>
{/if}
Total added : <strong>{$res.totalAdded}</strong><br>
Total updated : <strong>{$res.totalUpdated}</strong><br>
Memory used : <strong>{$res.memoryUsed}</strong><br>
Time taken : <strong>{$res.titmeTaken}</strong><br>
<p><button class="button button-blue addMoreAudience">{$lang.lbl_addMore}</button></p>

      
</div>