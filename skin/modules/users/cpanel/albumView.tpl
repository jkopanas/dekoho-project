<div class="AlbumInfo">

<div class="al_title">{$album.title}</div>
<div class="al_description">{$album.description}</div>
</div>
<div class="al_actions">
<div class="title">Album Actions</div>
<input type="hidden" value="{$album.id}" name="albumid"/>
<a href="#" class="albumEdit" rel="{$album.id}" >Edit</a> &nbsp; <a href="#" class="albumDelete" rel="{$album.id}">Delete</a>
</div>
<div>
<div class="al_images" >
{if $album.images != 0}
{foreach from=$album.images item=a name=b}
    {if $smarty.foreach.b.first}
    <div class="imagerow">
    {/if}
    
    {if $smarty.foreach.b.index % 4 == 0 AND $smarty.foreach.b.index!=0}
    </div>
    <div class="imagerow">
    {/if}
    <div class="image" style="display:inline">
        <div class="thumb" >
        	<a href="#" class="imageLink" rel="{$a.images.big}"><img src="{$a.images.thumb}" /></a>
        </div>
        <div class="img_actions">
            <a href="#" rel="{$a.id}" class="edit_img">Edit</a>
            &nbsp;
            <a href="#" rel="{$a.id}" class="delete_img">Delete</a>
            &nbsp;
            <a href="#" rel="{$a.id}" class="default_img">Set as Default</a>
        </div>
        <div class="im_title">
        	<a href="#" class="imageLink" rel="/media_files/users/{$a.uid}/images/original/{$a.original_file}">{$a.title}</a>
        </div>
    </div>
    {if $smarty.foreach.b.last}
    </div>
    {/if}
{/foreach}
{else}
<div class="message warning">{$lang.txt_no_images}</div>
{/if}
</div>