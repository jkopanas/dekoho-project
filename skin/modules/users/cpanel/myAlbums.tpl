<div><a href="#" id="albumsSwitch">Albums List</a>/<a href="#" class="newAlbum">new Album</a></div>
<div class="albums">
	{foreach from=$albums item=a}
    <div class="al_item">
        <div class="al_title" style="display:inline"><a href="#" rel="{$a.id}" class="openAlbum">{$a.title}</a></div>
        <div class="al_description" style="display:inline">{$a.title}</div>
        <div class="al_photocount" style="display:inline">{$a.images}</div>
    </div>
    {/foreach}
</div>



<div id="imageUpload" class="hidden">
    <form enctype="multipart/form-data" method="post">
    <div>
    <input type="file" name="uploadify" id="uploadify"/>
    <div id="fileQueue" style="width:180px;"></div>
    <ul class="msg hidden"></ul>
    <a href="javascript:$('#uploadify').uploadifyUpload();">{$lang.upload_file}</a>
    
    <input type="hidden" name="module" value="user" class="ImageData" />
    <input type="hidden" name="action" value="Savethumb" class="ImageData" />
    <input type="hidden" name="userid" value="{$ID}" class="ImageData" />
    <input type="hidden" name="albumid" value="" id="albumID" class="ImageData" />
    <input type="hidden" name="return" value="Thumb" class="ImageData" />
    <input type="hidden" name="ExistingThumb" value="{$item.image_full.thumb}" class="ImageData"/>
    
    </div>
    </form>
</div>

<div class="albumContainer"></div>

<div id="tmp" class="hidden">
	<div class="container1">
        <div class="image"><img id="imageHolder" src="" /></div>
        <div class="title"></div>
    </div>
    <div class="container2"></div>
</div>

<link rel="stylesheet" type="text/css" href="/css/colorbox.css" />                        
<script type="text/javascript" src="/scripts/jquery.colorbox-min.js"></script>

<link rel="stylesheet" type="text/css" href="/css/uploadify.css" />                        
<script type="text/javascript" src="/scripts/uploadify/swfobject.js"></script>
<script type="text/javascript" src="/scripts/uploadify/jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript" src="/scripts/myAlbums.js"></script>
