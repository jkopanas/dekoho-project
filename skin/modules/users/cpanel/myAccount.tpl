<div id="tmp"></div>

<div class="efieldsContainer">

<form>
{foreach from=$groups item=fields}
	<div class="group">
    	<div class="groupTitle">GROUP:{$fields.title}</div>
    	<div class="groupsItems">
        {foreach from=$fields.items item=a}
            <div class="item">
                <div class="itemDescription">{$a.settings.description}</div>
                <div class="itemFields">
                {include file="modules/users/efields.tpl" item=$a}
                </div>
            </div>
        {/foreach}
    	</div>
    </div>
{/foreach}

<input type="button" id="saveButton" value="{$lang.save}"/>
</form>
</div>

<script type="text/javascript" src="/scripts/userAccount.js"></script>