{if $LOGINMESSAGE} <div class="spacer-bottom error">{$LOGINMESSAGE}</div>{/if}
<div class="box size-350 float-left spacer-right">
<h1 class="header">{$lang.member_entrance}</h1>
<div class="content slide-wrapper">

<form id="login_form" name="login_form" method="post" action="{$SELF}">
      <table style="font-weight: bold;" width="400">
      <tbody><tr>
        <td style="height: 40px;" valign="middle">{$lang.username} (Username) :</td>

        <td style="height: 40px;" valign="middle"><input name="username" id="username"  type="text"></td>
      </tr>
      <tr>
        <td style="height: 40px;" valign="middle">{$lang.password} :</td>
        <td style="height: 40px;" valign="middle"><input name="password" type="password"></td>
      </tr>
       <tr>
        <td style="height: 40px;" valign="middle">{$lang.remember_me} :</td>
        <td style="height: 40px;" valign="middle"><input type="checkbox" name="autologin" value="1" /></td>
      </tr>
      <tr>
        <td colspan="2" style="height: 40px;" align="center" valign="middle"><input name="Submit" value="{$lang.login}" type="submit" class="submit">
			{foreach from=$smarty.post item=a key=k}
            {if $k ne "password" AND $k ne "username"}
            <input name="{$k}" value="{$a}" type="hidden">
            {/if}
			{/foreach}

          </td>
      </tr>
      <tr>
        <td colspan="2" align="center" valign="middle"><a href="{$URL}/lost-password.html">{$lang.lost_password} (password)</a> </td>

      </tr>
    </tbody></table>  
    {if $nav_area eq "checkout"}<input type="hidden" name="return_to" value="/checkout.html" />{/if}
</form>
</div><!-- END CONTENT -->
</div><!-- END BOX -->
<div class="box size-350 float-left">
<h1 class="header">{$lang.returning_member}</h1>
<div class="content slide-wrapper">
Γίνετε μέλος στο MgManager.gr<br>

  <a href="/signup.html" title="{$lang.become_a_member}">{$lang.create_new_account} &gt;&gt;</a>
</div><!-- END CONTENT -->
</div><!-- END BOX -->
<div class="clear"></div>