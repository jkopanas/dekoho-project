<div class="profileContainer">
    <div class="pageTitle">User Profile</div>
        <div class="field">
            <div class="fieldTitle">{$lang.user_name}</div>
            <div class="fieldValue">{$user.user_name}</div>
        </div>
        <div class="field">
            <div class="fieldTitle">{$lang.surname}</div>
            <div class="fieldValue">{$user.user_surname}</div>
        </div>
        <div class="field">
            <div class="fieldTitle">{$lang.email}</div>
            <div class="fieldValue">{$user.email}</div>
        </div>
        <div class="field">
            <div class="fieldTitle">{$lang.phone}</div>
            <div class="fieldValue">{$user.user_phone}</div>
        </div>
        <div class="field">
            <div class="fieldTitle">{$lang.mobile}</div>
            <div class="fieldValue">{$user.user_mobile}</div>
        </div>
        <div class="field">
            <div class="fieldTitle">{$lang.birthday}</div>
            <div class="fieldValue">{$user.user_birthday|date_format:"%d/%m/%Y"}</div>
        </div>
        {foreach from=$user.efields item=a}
        <div class="field">
            <div class="fieldTitle">{$a.fieldname}</div>
            <div class="fieldValue">{$a.fieldvalue}</div>
        </div>
        {/foreach}

</div>


{if $ID}
{assign var="commentitemid" value=$user.id}
{assign var="commentmodule" value="users"}
{assign var="commenttype" value="profile"}
{assign var="commentstatus" value="1"}
{assign var="comments" value=$user.comments}

{include file="modules/users/UserComment.tpl"}
{/if}