{if !$items}
Δεν έχετε προϊόντα στο καλάθι σας.
{else}

<ul id="mainNav" class="threeStep">
		<li id="first" class="done"><a title="Βήμα 1: Καλάθι αγορών" href="/cart.html"><em>Βήμα 1: Καλάθι αγορών</em><span>δείτε συνοπτικά την παραγγελία σας</span></a></li>
		<li id="second" class="lastDone"><a  title="Βήμα 2: Καλάθι αγορών" href="/{$FRONT_LANG}/checkout.html"><em>Βήμα 2: Πληρωμή</em><span>Επιλογή τρόπου πληρωμής</span></a></li>
        <li id="third" class="current"><a title=""><em>Βήμα 3: Ολοκλήρωση</em><span>Τέλος παραγγελίας</span></a></li>
	</ul>

<div class="clear seperator"></div>
<h1>Η παραγγελία σας</h1>

<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <th>#ID</th>
    <th>Προϊόν</th>
    <th>Τιμή μονάδας με Φ.Π.Α</th>
    <th>Ποσότητα</th>
    <th>Σύνολο</th>
  </tr>
  {foreach from=$items item=a key=k name=b}
  <tr>
    <td><strong>{$k}</strong></td>
    <td><a href="/product/{$k}/{$a.permalink}.html">{$a.title}</a></td>
    <td class="price">&euro; {$a.price|number_format:2:".":","}</td>
    <td align="center">{$a.quantity|default:0}</td>
    <td align="right" class="price">&euro; <span id="total-{$k}">{$a.total|number_format:2:".":","}</span></td>
  </tr>
  {/foreach}
  
  <tr><td colspan="6" align="right">Μερικό Σύνολο : <strong>&euro;<span class="total-price-no-vat">{$total_price_no_vat|number_format:2:".":","}</span></strong></td></tr>
  <tr><td colspan="6" align="right">Αξία Φ.Π.Α : <strong>&euro;<span class="vat-price">{$vat_price|number_format:2:".":","}</span></strong></td></tr>
  <tr><td colspan="6" align="right">Γενικό Σύνολο : <strong class="price">&euro;<span class="total-price" id="total">{$total_price|number_format:2:".":","}</span></strong></td></tr>
</table>

{$button_pay} 
								
{/if}

<script type="text/javascript">
head.ready(function(){
	$('#pp-amount').val($('#total').html());	
});
</script>

