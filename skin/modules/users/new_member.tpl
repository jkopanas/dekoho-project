<div class="content-middle-full">
<h1>{$lang.new_user}</h1>
{if $action eq "step1"}
<div id="login"><p>Η εγγραφή σας έγινε με επιτυχία. Θα σας αποσταλεί ένα email επιβεβαίωσης με οδηγίες για την ολοκλήρωση της διαδικασίας.</p>
<p>Μπορείτε να συνδεθείτε <a href="/user_cp.html">εδώ</a></div>
{elseif $action eq "step2"}
<div id="error_list">{$lang.account_activated}</div>
{else}
<form name="form" action="{$SELF}" method="post" id="signup">
  <table width="100%" border="0" align="center" cellpadding="5" cellspacing="5" class="quicksearch">
    <tr>
      <td>{$lang.username}  (Username)<font color="#FF0000">*</font> <br>
        <span style="color:#999999">[</span> <a href="#" id="check_availability"> {$lang.check_availability}</a> <span style="color:#999999">]</span></td>
      <td><input name="uname" type="text"  id="uname" value="{$data.uname}"><div id="avail_result"></div></td>
    </tr>
    <tr>
      <td>{$lang.password} <font color="#FF0000">*</font></td>
      <td><input name="password" type="password" class="brown_input" id="password" value="{$data.password}"></td>
    </tr>
    <tr>
      <td>{$lang.password_confirm} <font color="#FF0000">*</font></td>
      <td><input name="confirm_password" type="password" class="brown_input" id="confirm_password" value="{$data.confirm_password}"></td>
    </tr>
    <tr>
      <td>{$lang.full_name} <font color="#FF0000">*</font></td>
      <td><input name="user_name" type="text" class="brown_input" value="{$data.user_name}"></td>
    </tr>
<tr>
      <td>{$lang.country} <font color="#FF0000">*</font></td>
      <td>
	    <SELECT name="user_location" class="brown_input">
	      <OPTION value="">Επιλογή χώρας</OPTION>
	      
		{section name=ni loop=$new_languages}
     
	      <OPTION value="{$new_languages[ni].code}" {if $new_languages[ni].code eq $user.user_location}selected{/if}>{$new_languages[ni].country}</OPTION>
	       
		{/section}
      
        </SELECT>	  </td>
    </tr>
    <tr>
      <td>{$lang.email} <font color="#FF0000">*</font></td>
      <td><input name="email" type="text" class="brown_input" value="{$data.email}"></td>
    </tr>
    <tr>
      <td>{$lang.sign_up_newsletter}</td>
      <td valign="top"><input name="newsletter" type="checkbox" class="brown_input" value="1" checked="checked" {if $data.newsletter eq 1}checked{/if}> 
        {$lang.signup_to_newsletter} 	
</td>
    </tr>
  <tr>
      <td>{$lang.validate}: {$validation.num1} + {$validation.num2} {$lang.is}</td>
      <td valign="top"><input name="validate" type="text" id="validate" /> <div id="validation_result"></div></td>
    </tr>
    <tr>
      <td align="center" colspan="2"><input name="mode" type="hidden" id="mode" value="add">
        <input type="button" name="button" id="submit_form" value="{$lang.create_account}" disabled="disabled" class="submit-button" /></td>
    </tr>
  </table>
  {literal}
<script>
$(document).ready(function(){ 

					   
$("#validate").keyup(function(){
							   
    if ({/literal}{$validation.num1} + {$validation.num2} == {literal} $("#validate").attr('value')) 
	{
		$("#validation_result").html('<span class="green">Σωστό</span>');
		$("#submit_form").removeAttr('disabled');

    }
	else {$("#validation_result").html('<span class="red">Λάθος</span>'); }
	});

						   
						   
var allInputs = $("#signup :input");//will get all the form elements of this form

$("#submit_form").click(function(){
$.post("/ajax/users_ajax.php?action={/literal}{if $id}update_user{else}add_user{/if}{literal}",
   allInputs,
   	function(data){
		if (data == 1)
		{
			{/literal}
			{if $id}
			alert('{$lang.user_updated}');
			{else}
			window.location = '{$URL}/user_registered.html'; 
			{/if}{literal}
		}
		else{
		$("#error_list").html(data);
		}
	},'html'
);//END POST AJAX								
});		//END ADD USER							
$("#check_availability").click(function(){

$.post("/ajax/users_ajax.php?action=check_availability",
   allInputs,
   	function(data){
		if (data == 1)
		{
			$("#avail_result").html('<span class="red">{/literal}{$lang.username_unavailable}{literal}</span>');
			$("#uname").attr({'style' : 'border:1px solid red'});
		}
		else{
		$("#avail_result").html('<span class="green">{/literal}{$lang.username_available}{literal}</span>');
		$("#uname").attr({'style' : 'border:1px solid #d5d5d5'});
		}
	},'html'
);//END POST AJAX
});//END CHECK AVAILABILITY
}); 
</script>
{/literal}
</form>
{/if}
<div class="clear"></div>
</div>
<div class="content-right">
 
<div class="search-list-title">
        <h2>Γιατί να γίνω μέλος</h2>
        <h1>στο PastryChef.gr;</h1>
        </div>
<div>{$lang.join_us}</div>
</div><!-- END RIGHT -->
