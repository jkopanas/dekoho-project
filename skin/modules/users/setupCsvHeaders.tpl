<div class="debugArea"></div>
<div class="scrollingH">
<div class="message success"><h3>{$lang.lbl_success}!</h3>
<p>{$lang.lbl_setupCsvHeaders}</p>
</div>
<table width="100%" border="0" cellspacing="5" cellpadding="5" class="datatable full">
  <tr class="headerOptions">
    <td colspan="{$csv.headers|@count}">Seperator : <select name="delimiter" class="newSettings">
<option value=";" {if $posted_data.delimiter eq ";"}selected{/if}>;</option>
<option value="," {if $posted_data.delimiter eq ","}selected{/if}>,</option>
<option value="\t" {if $posted_data.delimiter eq "\t"}selected{/if}>tab</option>
<option value=" " {if $posted_data.delimiter eq " "}selected{/if}>space</option>
</select> Enclosure : <select name="enclose" class="newSettings">
<option value='"'  {if $posted_data.enclose eq '"'}selected{/if}>"</option>
<option value="'" {if $posted_data.enclose eq "'"}selected{/if}>'</option>
</select> First row is a header : <input type="checkbox" name="firstRowIsHeader" class="newSettings" {if $posted_data.firstRowIsHeader}checked{/if} /> Split name field into <select name="splitName" class="newSettings" disabled="disabled">
<option value=""  {if $posted_data.splitName eq ""}selected{/if}>{$lang.none}</option>
<option value='nameSurname'  {if $posted_data.splitName eq 'nameSurname'}selected{/if}>{$lang.user_name} {$lang.surname}</option>
<option value="surnameName" {if $posted_data.splitName eq "surnameName"}selected{/if}>{$lang.surname} {$lang.user_name}</option>
</select> <button class="button button-blue changeUploadSettings">{$lang.lbl_changeSettings}</button> <input type="hidden" name="file" value="{$file}" class="newSettings" /></td>
  </tr>
  <tr>
  {foreach from=$csv.headers item=b key=k}
    <td>
    {if $posted_data.firstRowIsHeader}{$b}<br>{/if}
    <select name="row-{$k}" class="newSettings headerField">
    <option value="">{$lang.noneSelected}</option>
    <option value="name">{$lang.user_name}</option>
    <option value="surname">{$lang.surname}</option>
    <option value="phone">{$lang.phone}</option>
    <option value="mobile">{$lang.lbl_mobileNumber}</option>
    <option value="email">{$lang.email}</option>
    <option value="list">{$lang.lbl_group}</option>
    </select>
    <input type="hidden" name="column-{$k}" value="" />
    </td>
    {/foreach}
  </tr>
  {foreach from=$csv.results item=a name=b}
  <tr class="data" id="TR-{$smarty.foreach.b.iteration}">
  {foreach from=$csv.headers item=b key=k}
    <td>{$a[$k]}</td>
  {/foreach}
  </tr>
  {/foreach}
  <tr><td colspan="4">{$lang.total} : {$csv.total}</td></tr>
  <tr><td colspan="4"><button class="button button-red backToUpload">{$lang.back}</button> <button class="button button-green saveAudience">{$lang.save}</button></td></tr>
</table>
</div>