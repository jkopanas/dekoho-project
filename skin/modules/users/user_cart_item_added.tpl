<h1>{$lang.productAddedToOrder} :</h1>
<img src="{$item.image_full.thumb}" align="left" class="spacer-right" /> <div class="float-left"><h2>{$item.title}</h2>
<span class="price_big">&euro;{$item.eshop.vatPrice|number_format:2:".":","}</span>
</div>
<div class="seperator"></div>
<div class="clear spacer-bottom spacer-top">
<h1>Σύνολο : {$total_quantities|default:0}</span> Προϊόντα ( <span class="price">&euro;</span> <span class="price total-price">{$total_price|number_format:2:".":","}</span> )</h1>

</div>

<div class="float-right"><a href="javascript:void(0);"  class="large button blue close-alert" rel="{$a.id}">&laquo; Συνέχιση πλοήγησης</a> <a href="/cart.html"  class="large button blue" rel="{$a.id}">Ολοκλήρωση παραγγελίας &raquo;</a></div>
<div class="clear"></div>
