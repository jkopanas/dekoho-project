<div class="seperator padding wrap">
<h2>Αναζήτηση</h2>
<div class="debug"></div>
<a href="javascript:void(0)" class="ShowSearchTable Slide" rel="#SearchTable">Εμφάνιση φόρμας / Απόκρυψη φόρμας</a>
<form name="searchform" id="searchform" action="{if !$nav_area}{$SELF}{else}{$URL}/search/{/if}" method="POST">
<table width="100%" border=0 cellpadding="5" cellspacing="5" id="SearchTable">

<TR>
  <TD nowrap class=FormButton>Αποτελέσματα ανά σελίδα</TD>
  <TD width="933"><select name="results_per_page" id="results_per_page" class="SearchData">
    <option value="20" {if $data.results_per_page eq 20}selected{/if}>20</option>
    <option value="60" {if $data.results_per_page eq 60}selected{/if}>60</option>
    <option value="100" {if $data.results_per_page eq 100}selected{/if}>100</option>
  </select>  </TD>
</TR>
<TR>
  <TD nowrap class=FormButton>Ταξινόμιση κατά</TD>
  <TD> <select name="sort" class="SearchData">
   <option value="date_added" {if $data.sort eq 'date_added'}selected{/if}>Ημερομηνία εισαγωγής</option>
   <option value="title" {if $data.sort eq 'title'}selected{/if}>Τίτλος</option>
   <option value="id" {if $data.sort eq 'id'}selected{/if}>#ID</option> 
</select>  
πορεία : 
<select name="sort_direction" id="sort_direction" class="SearchData">
<option value="desc" {if $data.sort_direction eq 'desc'}selected{/if}>Φθήνουσα</option>
<option value="asc" {if $data.sort_direction eq 'asc'}selected{/if}>Αύξουσα</option>             
        </select></TD>
</TR>
<TR>
  <TD width="94" nowrap class=FormButton>#ID</TD>
  <TD>
  <INPUT name="id" type=text id="id" value="{$data.id}" class="SearchData"  /></TD>
</TR>
<tr>
  <td width="94" nowrap class=FormButton>{$lang.title}</td>
  <td>
  <INPUT name="title" type=text id="title" value="{$data.title}" class="SearchData"  /></td>
</tr>
<tr>
  <td nowrap class=FormButton>{$lang.category}</td>
  <td>
    <select name="categoryid" class="SearchData">
  <option value="%">Όλες οι κατηγορίες</option>
{foreach from=$categories item=a}
<option value="{$a.categoryid}" {if $a.categoryid eq $data.categoryid}selected{/if}>{$a.category}</option>                
{/foreach}  
</select>
  </td>
</tr>
<tr>
  <td nowrap class=FormButton>{$lang.shop}</td>
  <td>
    <select name="shopid" class="SearchData">
  <option value="%">Όλες οι κατηγορίες</option>
{foreach from=$shops item=a}
<option value="{$a.id}" {if $a.id eq $data.shopid}selected{/if}>{$a.title}</option>                
{/foreach}  
</select>
  </td>
</tr>
<tr> 
<td colspan="2" align="center" class=FormButton>
  <input type="button" value="Αναζήτηση" class="button" id="DealsSearch" >
  <input name="search" type="hidden" id="search" value="1">
  <input name="ResultsDiv" type="hidden"  value="#ResultsPlaceHolder-div" class="SearchData">
    <input name="FormToHide" type="hidden"  value="#SearchTable" class="SearchData"></td>
</tr>
</table>
</form>
</div>



<div id="SearchResults" class="wrap seperator padding">
<h2>Αποτελέσματα αναζήτησης</h2>
<form name="ResultsPlaceHolder">
<div id="ResultsPlaceHolder-div">{include file="modules/deals/admin/list.tpl"}</div>
</form>
</div>

<div class="seperator"></div>
<script type="text/javascript" src="/scripts/deals.js"></script>