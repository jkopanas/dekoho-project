{include file="modules/d_items/admin/menu.tpl"}
{include file="modules/d_items/admin/additional_links.tpl" mode="nav_menu" base_file="items_modify"}
{if $error_list}{include file="common/error_list.tpl"}{/if}
{if $updated}{include file="common/error_list.tpl" mode="message" message=$lang.update_success}{/if}
<script src="/skin/common/multirow.js" language="JavaScript" type="text/javascript"></script>
<div class="wrap seperator padding">
  <h2>{$lang.available_classes} {if $items} (<a href="{$SELF}#newclass">add new</a>){/if}</h2>
<form name="availableclass" method="post" action="{$PHP_SELF}">
  <table width="600"  border="0">
    <TR>
      <th scope="col">&nbsp;</th>
      <th scope="col">{$lang.pos}</th>
      <th scope="col">{$lang.class}</th>
      <th scope="col">{$lang.class_text}</th>
	  <th scope="col">{$lang.class_options}</th>
    </tr>
  {if $all_classes} 
  {section name=zq loop=$all_classes}
  <tr>
    <td width="2" align="center"><input name="classid[]" type="checkbox" value="{$all_classes[zq].classid}"></td>
    <td align="center"><input name="orderby-{$all_classes[zq].classid}" type="text" id="orderby-{$all_classes[zq].classid}" size="3" maxlength="3" value="{$all_classes[zq].orderby}"></td>
    <td align="center"><a href="{$SELF}?id={$id}&amp;cid={$all_classes[zq].classid}">{$all_classes[zq].class}</a></td>
    <td align="center">{$all_classes[zq].classtext}</td>
	<td align="center"><select name="class_options-{$all_classes[zq].classid}">
{section name=zx loop=$all_classes[zq].options}
	<option value="{$all_classes[zq].options[zx].optionid}">{$all_classes[zq].options[zx].option_name} ( {$all_classes[zq].options[zx].price_modifier} {$all_classes[zq].options[zx].modifier_type})</option>
{/section}
</select></td>
  </tr>
  {/section} 
  {else}
  <tr>
    <td colspan="5">{$lang.txt_no_classes}</td>
  </tr>
    {/if}
	<TD colspan="5"><BR>
<INPUT type="button" value="{$lang.add_new} {$lang.class}" onClick="self.location='options.php'">
<input name="mode" type="hidden" id="mode" value="delete">
<input type="hidden" name="id" value="{if $smarty.get.id}{$smarty.get.id}{else}{$smarty.post.id}{/if}"></TD><TR>
  </table>
</form>
<div style="clear:both"></div>
</div>

{capture name=dialog}
<a name="newclass" id="newclass"></a>
<form action="{$SELF}" method="post" name="addclass" id="addclass">
  <table width="100%"  border="0">
    <tr>
      <td>{$lang.pos}</td>
      <td><input name="orderby" type="text" id="orderby" size="4" /></td>
    </tr>
    <tr>
      <td>{$lang.availability}</td>
      <td><select name="active" id="active">
        <option value="1">yes</option>
        <option value="0">no</option>
      </select>
      </td>
    </tr>
    <tr>
      <td width="18%">{$lang.class}</td>
      <td width="82%"><input type="text" name="class"></td>
    </tr>
    <tr>
      <td>{$lang.class_text}</td>
      <td><input type="text" name="classtext"></td>
    </tr>
    <tr>
      <td>{$lang.class_options}</td>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
          <th align="left" scope="col">{$lang.pos}</th>
          <th scope="col">{$lang.title}</th>
          <th scope="col">{$lang.value}</th>
          <th scope="col">&nbsp;</th>
        </tr>
        <tr>
          <td align="left" id="popt_box_1"><input name="new_list[pos][0]" type="text" size="4" /></td>
          <td align="center" id="popt_box_2"><input type="text" name="new_list[title][0]" /></td>
          <td align="center" id="popt_box_3"><input name="new_list[price_modifier][0]" type="text" size="5" />
            <select name="new_list[modifier_type][0]">
              <option value="%" selected="selected">Percent</option>
              <option value="$">Absoloute</option>
            </select></td>
          <td align="center"><a href="javascript: void(0);" onclick="javascript: add_inputset('popt', this, true);">ass</a></td>
        </tr>
        {if $cid}
        {assign var="count" value="0"}
        {foreach from=$classes.options item=a name=cl}
         <tr>
          <td align="left" id="popt_box_1_{$smarty.foreach.cl.iteration-1}"><input name="new_list[pos][{$smarty.foreach.cl.iteration}]" value="{$a.orderby}" type="text" size="4" /></td>
          <td align="center" id="popt_box_2_{$smarty.foreach.cl.iteration-1}"><input type="text" name="new_list[title][{$smarty.foreach.cl.iteration}]" value="{$a.option_name}" /></td>
          <td align="center" id="popt_box_3_{$smarty.foreach.cl.iteration-1}"><input name="new_list[price_modifier][{$smarty.foreach.cl.iteration}]" value="{$a.price_modifier}" type="text" size="5" />
            <select name="new_list[modifier_type][{$smarty.foreach.cl.iteration}]">
              <option value="%" {if $a.modifier_type eq '%'}selected{/if}>Percent</option>
              <option value="$" {if $a.modifier_type ne '%'}selected{/if}>Absoloute</option>
            </select></td>
          <td align="center" id="popt_box_4_{$smarty.foreach.cl.iteration-1}"><a href="javascript: void(0);" onclick="javascript: add_inputset_subrow(this);">Add row</a>&nbsp;&nbsp;<a href="javascript: void(0);" onclick="javascript: remove_inputset(this);">Remove row</a></td>
        </tr>
        {math equation="x + y" x=$count y=1}

        {/foreach}
        {/if}
      </table></td>
    </tr>
    
    <tr>
      <td>&nbsp;</td>
      <td><INPUT type="button" value="{$lang.add}" onClick="document.addclass.action.value='new_class'; document.addclass.submit();">
          <input name="action" type="hidden" id="action"><input name="cid" type="hidden" id="cid" value="{$cid}"><input name="id" type="hidden" id="id" value="{$id}">          </td>
    </tr>
  </table>

</form>
{/capture}{include file="admin/dialog.tpl" content=$smarty.capture.dialog title=$lang.new_class extra="width=100%"}

{capture name=dialog}
<form action="{$PHP_SELF}" method="post" name="updclass" id="updclass">
  <table width="100%"  border="0">
    <tr>
      <td width="18%">{$lang.class}</td>
      <td width="82%"><input type="text" name="class"  value="{$class.class}"></td>
    </tr>
    <tr>
      <td>{$lang.class_text}</td>
      <td><input type="text" name="classtext"  value="{$class.classtext}"></td>
    </tr>
    <tr>
      <td colspan="2">
      <table width="100%"  border="0">
		    <TR class="TableHead">
 		     <td width="2">&nbsp;</td>
      		<td>{$lang.option_name}</td>
      		<td>{$lang.pos}</td>
      		<td>{$lang.description}</td>
      		<td>{$lang.value}</td>
    		</tr>
			{if $class.options}
			{section name=as loop=$class.options}
			<tr align="center">
			<td width="2"><input type="checkbox" name="optionid[]" value="{$class.options[as].optionid}"></td>
			<td><input name="option_name-lng-{$class.options[as].optionid}" type="text" value="{$class.options[as].option_name}"></td>
			<td><input name="orderby-{$class.options[as].optionid}" type="textfield" size="3" maxlength="3" value="{$class.options[as].orderby}" /></td>
			<td><textarea name="description${$class.options[as].optionid}" id="description-{$class.options[as].optionid}">{$class.options[as].description}</textarea>
			<br />
<input name="button42" type="button" onclick="openEditor(document.updclass.description${$class.options[as].optionid});" value="{$lang.advanced_edit}"/>
</td>
			<td><input name="option_value-{$class.options[as].optionid}" type="textfield" size="4" value="{$class.options[as].option_value}" /></td>
    		</tr>
	{/section}
		{/if}
		<tr align="center">
		 <td colspan="6"><b>{$lang.add_new} {$lang.option}</b></td>
		  </tr>
		<tr align="center">
			<td>&nbsp;</td>
			<td><input name="new_option_name" type="text"></td>
			<td><input name="new_orderby" type="textfield" size="3" maxlength="3" /></td>
			<td><textarea name="new_option_description" id="new_option_description"></textarea>
		    <br />
			<input name="button42" type="button" onclick="openEditor(document.updclass.new_option_description);" value="{$lang.advanced_edit}"/>
			</td>
			<td><input name="new_option_value" type="textfield" size="4" /></td>
   		  </tr>
	  </table>
 <tr>
      <td colspan="2">        <input name="mode" type="hidden" id="mode">
      <INPUT type="button" value="{$lang.add}/{$lang.update} {$lang.option}" onClick="document.updclass.mode.value='updclass'; document.updclass.submit();">
&nbsp; &nbsp;
<INPUT type="button" value="{$lang.delete_selected}" onClick="document.updclass.mode.value='delopt'; document.updclass.submit();">
<input type="hidden" name="classid" value="{if $smarty.get.id}{$smarty.get.id}{else}{$smarty.post.id}{/if}"></td>
    </tr>
  </table>

</form>
{/capture}{include file="admin/dialog.tpl" content=$smarty.capture.dialog title=$lang.modify_class extra="width=100%"}
