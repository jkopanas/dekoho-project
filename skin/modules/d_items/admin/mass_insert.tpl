{include file="modules/d_items/admin/menu.tpl"}
<div id="in_this_section" style="margin-bottom:20px;"><strong>In this section :</strong> {if $action}<a href="{$e_FILE}">Insert more items</a>{/if} {if !$action}<a href="{$e_FILE}?action=1">Recently Uploaded ( {$inactive.counter} )</a>{/if}{if $cat} | <a href="{$e_FILE}">Change category</a>{/if}</div>
{if $action}
<div class="seperator padding wrap">
  <form name="items_form" method="post">
  <h2>New Items</h2>
{include file="modules/d_items/admin/display_items_table.tpl" items=$items type="editor" form_name="items_form" page="mass_insert.php"}
</form>
</div>
{else}
<div class="wrap seperator padding">
  <h2>{$lang.mass_insert_items} {if $category} - <a href="{$e_FILE}">Select category</a> / {include file="modules/d_items/nav_categories.tpl"  target="other" destination="$e_FILE"} {/if}</h2>

<div>
{if !$cat}
<div id="all_categories" class="seperator">
<h3>Select category</h3>
<ul>
{foreach from=$allcategories item=a}
<li><a href="{$e_FILE}?cat={$a.categoryid}">{$a.category}</a></li>
{/foreach}
</ul>
</div>
{else}
{include file="common/uploader.tpl" mode="jumploader" form="cat_form" WIDTH="100%" HEIGHT="500" action="mass_insert_items" module="d_items"}
{literal}
<script language="javascript">
function uploaderStatusChanged( uploader ) {
if (uploader.isReady()) {
window.location.href = "{/literal}{$e_FILE}?action=1{literal}";
}
}

</script> 
{/literal}
{/if}
</div>
<div style="clear:both"></div>
</div>
{/if}