{include file="modules/d_items/admin/menu.tpl"}
<div id="in_this_section"><strong>In this section :</strong> <a href="#upload">Upload files</a> | <a href="#view">Inspect uploaded files</a></div>
{include file="modules/d_items/admin/additional_links.tpl" mode="nav_menu" base_file="items_modify"}
{if $updated}{include file="common/error_list.tpl" mode="message" message=$lang.update_success}{/if}


<div class="wrap seperator">
<div class="padding">
  <h2>Type of media (<a href="#view">Inspect Uploaded media</a>)</h2>
{if $media eq "thumb"}<strong>Item Thumbnail</strong>{else}<a href="{$e_FILE}?id={$id}&media=thumb">Item Thumbnail</a>{/if} | {if $media eq "images"}<strong>Images</strong>{else}<a href="{$e_FILE}?id={$id}&media=images">Images</a>{/if} | {if $media eq "videos"}<strong>Videos</strong>{else}<a href="{$e_FILE}?id={$id}&media=videos">Videos</a>{/if} | {if $media eq "docs"}<strong>Documents</strong>{else}<a href="{$e_FILE}?id={$id}&media=docs"> Documents </a>{/if}
{if $media eq "images"}<div style="margin-left:80px; margin-top:5px;">Image Category : {section name=b loop=$image_categories}
{if $image_categories[b].id ne 0}
{if $image_categories[b].id eq $type}<strong>{$image_categories[b].title}</strong>{else}<a href="{$e_FILE}?id={$id}&media=images&type={$image_categories[b].id}">{$image_categories[b].title}</a> {/if}
{if !$smarty.section.b.last} | {/if}
{/if}
{/section}</div>
{/if}
</div>
<div style="clear:both"></div>
</div>

<TABLE class="open_close_tab seperator">
      <TR>
        <TD id="close8" style="display: none; cursor: hand;" onClick="visibleBox('8')"><IMG src="/skin/images/plus.gif" border="0" align="absmiddle" alt="Click to open"></TD>
        <TD id="open8" style="cursor: hand;" onClick="visibleBox('8')"><IMG src="/skin/images/minus.gif" border="0" align="absmiddle" alt="Click to close"></TD>
        <TD><A href="javascript:void(0);" onClick="visibleBox('8')"><B>Upload Tool</B></A></TD>

      </TR>
      </TABLE>
<div id="box8" class="wrap padding">
<a name="upload" id="upload"></a>

<form name="images_form" id="images_form">
<strong>Create Image Set :</strong> <input name="create_set" type="checkbox" id="create_set" value="1" checked="checked" /> 
{include file="common/uploader.tpl" mode="jumploader" form="images_form" id=$id  type=$type WIDTH="100%" HEIGHT="400" action="mass_items_images" module="d_items"}
{literal}
<script language="javascript">
function uploaderStatusChanged( uploader ) {
if (uploader.isReady()) {
window.location.href = "{/literal}{$e_FILE}?id={$id}&media={$media}&type={$type}{literal}";
}
}

</script> 
{/literal}
</form>

</div>

<div class="wrap seperator padding">
<h2>Uploaded Files</h2>
<a name="view" id="view"></a>
<FORM action="{$PHP_SELF}#view" method="POST" name="processcategoryform" id="processcategoryform">
{if $media eq "images"}
<div>
{section name=a loop=$image_categories}<span class="{if $image_categories[a].id eq $type}wpv-tab-button{else}wpv-tab-button-disabled{/if}"><a href="{$MODULE_FOLDER}/products_modify_img.php?id={$id}&type={$image_categories[a].id}#view">{$image_categories[a].title}</a></span>
{/section}</div>
{/if}
<div style="    border: 5px solid #c0c0c0;
    border-top: 10px solid #c0c0c0;
    border-bottom: 10px solid #c0c0c0;
    padding: 5px 5px 5px 5px;
    margin: 0px 0px 0px 0px; ">
<TABLE width="100%" border="0" cellpadding="2" cellspacing="1" class="widefat">



<INPUT type="hidden" name="cat_org" value="{$smarty.get.cat|escape:"html"}"> {assign var="form_name" value="processcategoryform"} 
<TR>
  <th scope="col">&nbsp;</th>
  <th scope="col"><input type="checkbox" id="total_check" name="ids[1]" onclick="check_all_boxes(this.form.check_values)" /></th>
  <th scope="col">{$lang.thumb}</th>
  <th scope="col">{$lang.pos}</th>
  <th scope="col">{$lang.availability}</th>
  <th scope="col">{$lang.category}</th>
  <th scope="col">{$lang.alt}</th>
  <th scope="col">{$lang.title}</th>
</TR>
{if $images}

{section name=a loop=$images}
<TR {cycle values=", class='TableSubHead'"}>
<TD width="1%"><a href="{$URL}/{$images[a].main}" target="_blank"><img src="{$URL}/{$images[a].thumb}" width="30" height="30"></a></TD>
<TD width="2%"><input name="ids[{$images[a].id}]" type="checkbox" id="check_values" value="1"></TD>
<TD width="2%" align="center"><input type="radio" name="thumb" id="thumb" value="{$images[a].id}" {if $images[a].type eq 0}checked{/if} /></TD>
<TD width="2%"><input name="orderby-{$images[a].id}" type="text" value="{$images[a].orderby}" size="3" maxlength="3"></TD>
<TD><select name="available-{$images[a].id}">
<option value="0" {if $images[a].available eq "0"}selected{/if}>{$lang.disabled}</option>
<option value="1" {if $images[a].available eq "1"}selected{/if}>{$lang.enabled}</option>
</select></TD>
<TD><select name="type-{$images[a].id}" id="type-{$images[a].id}">
{section name=b loop=$image_categories}
<option value="{$image_categories[b].id}" {if $image_categories[b].id eq $images[a].type}selected{/if}>{$image_categories[b].title}</option>
{/section}
</select></TD>
<TD><input type="text" value="{$images[a].alt}" name="alt-{$images[a].id}"></TD>
<TD><input type="text" value="{$images[a].title}" name="title-{$images[a].id}"></TD>
</TR>






{/section}

{else}

<TR>
<TD colspan="8" align="center">{$lang.txt_no_images}</TD>
</TR>

{/if}
<TR>
<TD colspan="8"><BR>
<INPUT type="button" class="button" onClick="document.processcategoryform.mode.value='modify'; document.processcategoryform.submit();" value="{$lang.update}">
&nbsp;
<input name="button" type="button" class="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="delete_images"; document.{$form_name}.submit();{rdelim}' value="Delete Checked" />
&nbsp;
<input name="button" type="button" class="button" onclick='javascript: if(confirm(&quot;{$lang.u_sure|strip_tags}&quot;)){ldelim}document.{$form_name}.action="{$MODULE_FOLDER}/mass_functions.php";document.{$form_name}.mode.value="activate_all_images"; document.{$form_name}.submit();{rdelim}' value="Activate ALL" /></TD>
</TR>
</table>
</div>
<input type="hidden" value="{if $smarty.get.id}{$smarty.get.id}{else}{$smarty.post.id}{/if}" name="id">
<input type="hidden" name="action" value="modify">
<input type="hidden" value="{$type}" name="type" />
<input type="hidden" value="{$media}" name="media" />
<input type="hidden" name="page" id="page" value="products_modify_img.php?id={$id}&type={$type}&media={$media}#view">
<input type="hidden" name="mode" id="mode" value="">
</FORM>
</div>

