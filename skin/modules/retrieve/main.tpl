
		<div class="top-links">
				<ul>
					<li><a href="/en/index.html">Home </a></li>				
					<li><a class="active" href="#">Your Booking</a></li>
					
				</ul>
		</div>
         
      <br class="clear"/>
      <div class="first-msg">
      Manage your Booking - Add extra Excursions , Packages, etc, Complete Payment
      
      </div>
      <div class="blueHeader">
     		 <h2>
     		 	Welcome {$retrieve.ParticipantList.ParticipantData.0.FirstName} {$retrieve.ParticipantList.ParticipantData.0.LastName}
     		 </h2>
      </div>
         <div class="seperator"></div><div class="seperator"></div>
	
	
	<div class="left_column">
	
	 <div class="bkgnd marg">
		
			<div class="booking-info-header">Your Booking Info</div>
			
			<div class="passenger-info">
				<span class="info-header">Passengers</span>
				
				<div class="all-passengers">
					<ol>	
					{foreach from=$retrieve.ParticipantList.ParticipantData item=a name=b}
					<li>{$a.FirstName} {$a.LastName} </li>
					{/foreach}
					</ol>
				</div>
			
			</div>
			<div class="seperator"></div>
	 </div>
	 
	 <div class="bkgnd marg">
			
			<div class="cruise-info">
				<span class="info-header">Cruise</span>
				<div class="seperator"></div>
				
				<div class="cruise-details">
					<div class="detail-left">Cruise Name</div>
					<div class="detail-right">{$retrieve.CruiseBookings.CruiseSailing.ItineraryDescription}</div>
					
				</div>
					<div class="seperator-small"></div>
				
				<div class="cruise-details">
					<div class="detail-left">Ship Name</div>
					<div class="detail-right">{$retrieve.CruiseBookings.CruiseSailing.ShipName}</div>
					
				</div>
					<div class="seperator-small"></div>
					
				<div class="cruise-details">
					<div class="detail-left">Departure</div>
					<div class="detail-right">{$retrieve.CruiseBookings.CruiseSailing.SailingDate} ( {$retrieve.CruiseBookings.CruiseSailing.SailingTime} )</div>					
				</div>
				<div class="seperator-small"></div>
				
			
				<div class="cruise-details">
					<div class="detail-left">Number of Days</div>
					<div class="detail-right">{$retrieve.CruiseBookings.CruiseSailing.SailingLengthDays}  Days</div>
				</div>
				
				<div class="seperator-small"></div>
				
				<div class="cruise-details">
					<div class="detail-left">---</div>
					<div class="detail-right"></div>					
				</div>
				
				<div class="seperator-small"></div>
				
				<div class="cruise-details">
					<div class="detail-left">Deck</div>
					<div class="detail-right">{$retrieve.CruiseBookings.CruiseSailing.DeckDesc}</div>					
				</div>
				
			<div class="seperator-small"></div>
			
			<div class="cruise-details">
					<div class="detail-left">Cabin</div>
					<div class="detail-right">{$retrieve.CruiseBookings.CruiseSailing.CabinNo} ( {$retrieve.CruiseBookings.CruiseSailing.CategoryDesc} )</div>					
				</div>
			</div>
			<div class="seperator"></div>
	 </div>
	 
	 
	 <div class="bkgnd-blue marg">
			
			<div class="cruise-info">
				<span class="info-header">Booking Charges</span>
				<div class="seperator"></div>
				
				<div class="cruise-details">
					<div class="detail-left">Total Price</div>
					<div class="detail-right">{$retrieve.BookingInfo.BookingCharges.TotalGrossPrice} {$retrieve.BookingContext.BookingCurrencyCode}</div>
					
				</div>
				
				<div class="seperator-small"></div>
				
				<div class="cruise-details">
					<div class="detail-left">Amount Paid</div>
					<div class="detail-right">{$retrieve.BookingInfo.BookingCharges.TotalPaymentsReceived} {$retrieve.BookingContext.BookingCurrencyCode}</div>
				</div>
			
			<div class="seperator-small"></div>
			
				<div class="cruise-details">
					<div class="detail-left">Amount Left</div>
					<div class="detail-right">{$retrieve.BookingInfo.BookingCharges.GrossBalanceDue} {$retrieve.BookingContext.BookingCurrencyCode} ( until {$retrieve.BookingInfo.BookingCharges.FinalPaymentDate} ) </div>
					
				</div>
			
				
			
			</div>
			<div class="seperator"></div>
	 </div>
	 
	 
	 <div class="bkgnd marg">
			
			<div class="cruise-info">
				<span class="info-header">Charge Detail per person</span>
				<div class="seperator"></div>
				
				
				{foreach from=$retrieve.ParticipantList.ParticipantData item=a name=b}
				
				<div class="cruise-details">
					<div class="detail-left-all">{$a.FirstName} {$a.LastName}</div>
										
				</div>
				
				<div class="seperator-small"></div>
				{foreach from=$retrieve.ChargeDetails.ChargesForComponent item=c name=d}
				{if $a.PassengerNo eq $c.PersonNo}
				<div class="cruise-details">
					<div class="detail-right">{$c.ChargeDesc}</div>
					<div class="detail-left">{$c.GrossChargeAmount} {$retrieve.BookingContext.BookingCurrencyCode}</div>	
				</div>
				<div class="seperator-small"></div>
				{/if}
				{/foreach}
				
					<div class="seperator"></div><div class="seperator"></div>
					
				{/foreach}
			</div>
			<div class="seperator"></div>
	 </div>
	 
	 
	 
	</div> <!-- end left -->
	
	<div class="right_column">
	<div class="excursions-header">
	<h2>Excursions</h2>
	
	</div>
	
	<div class="excursions-message">
	<span>Select any of the following shore excursions for your cruise </span>
	</div>
	<div class="all-excursions">
		
		{foreach from=$components.ActivityProducts.ActivityOccurence item=a name=b}
		<div class="float-left">
		<div class="excrusion-item">
		<input type="checkbox" class="excursion" id="excursion{$a.ComponentInfo.ComponentID}" name="ex{$a.ComponentInfo.ComponentID}" value="{$a.PriceInfo.PriceAmount}" data-price="{$a.PriceInfo.PriceAmount}" data-newprice="{$a.PriceInfo.PriceAmount}" data-componentid="{$a.ComponentInfo.ComponentID}"/>
			<span class="exc-desc">{$a.ActivityInfo.ActivityDescription}</span>
			<div class="seperator15"></div>
			<span class="exc-more-info">{$a.AvailabilityInfo.AvailabilityStatusDesc} {$a.ActivityInfo.OccurrenceDate}</span>
			<span class="exc-more-info-bold">Price:&nbsp;</span><span class="exc-price" id="excprice{$a.ComponentInfo.ComponentID}">{$a.PriceInfo.PriceAmount}</span><span class="exc-price-cur"> {$retrieve.BookingContext.BookingCurrencyCode}</span>
			<ul style="padding-left:30px; display:none; padding-top:20px; padding-bottom:20px;" id="allpass{$a.ComponentInfo.ComponentID}">
			<li style="height:30px; color: #113A58;">Please select or deselect the attending</li>
			{foreach from=$retrieve.ParticipantList.ParticipantData item=c name=d}
				<li style="height:30px; color: #113A58;" > <input type="checkbox" data-price="{$a.PriceInfo.PriceAmount}" name="person{$c.PersonNo}" data-id="{$a.ComponentInfo.ComponentID}" class="individuals" data-persontype="{$c.PersonType}" data-personno="{$c.PersonNo}" data-componentid="{$a.ComponentInfo.ComponentID}"/> {$c.FirstName} {$c.LastName} </li>
					{/foreach}
					 <a class="button blue small recalculate float-right" style="margin-right:300px;  margin-top: -60px; "  href="#" data-value="">recalculate price</a>
			
			</ul>
		
			
		
		</div>
		
		</div>
		
		{/foreach}
		</div>
	<div class="seperator"></div><div class="seperator"></div>
	
	<div class="excursions-header">
	<h2>Shorex Packages</h2>
	</div>
	<div class="excursions-message">
	<span>Select any of the following available packages for your cruise </span>
	</div>
	<div class="all-excursions">
		
		{foreach from=$components.MiscellaneousProducts.MiscellaneousItem item=a name=b}
		{if $a.MiscellaneousInfo.MiscItemType eq "SXP"}
		<div class="float-left">
		<div class="excrusion-item">
		<input type="checkbox" class="excursion" id="excursion{$a.ComponentInfo.ComponentID}" name="ex{$a.ComponentInfo.ComponentID}" value="{$a.PriceInfo.PriceAmount}" data-price="{$a.PriceInfo.PriceAmount}" data-newprice="{$a.PriceInfo.PriceAmount}" data-componentid="{$a.ComponentInfo.ComponentID}"/>
			<span class="exc-desc">{$a.MiscellaneousInfo.MiscItemDescription}</span>
			<div class="seperator15"></div>
			<span class="exc-more-info">{$a.AvailabilityInfo.AvailabilityStatusDesc} {$a.ActivityInfo.OccurrenceDate}</span>
			<span class="exc-more-info-bold">Price:&nbsp;</span><span class="exc-price" id="excprice{$a.ComponentInfo.ComponentID}">{$a.PriceInfo.PriceAmount}</span><span class="exc-price-cur"> {$retrieve.BookingContext.BookingCurrencyCode}</span>
			<ul style="padding-left:30px; display:none; padding-top:20px; padding-bottom:20px;" id="allpass{$a.ComponentInfo.ComponentID}">
			<li style="height:30px; color: #113A58;">Please select or deselect the attending</li>
			{foreach from=$retrieve.ParticipantList.ParticipantData item=c name=d}
				<li style="height:30px; color: #113A58;" > <input type="checkbox" data-price="{$a.PriceInfo.PriceAmount}" name="person{$c.PersonNo}" data-id="{$a.ComponentInfo.ComponentID}" class="individuals" data-persontype="{$c.PersonType}" data-personno="{$c.PersonNo}" data-componentid="{$a.ComponentInfo.ComponentID}"/> {$c.FirstName} {$c.LastName} </li>
					{/foreach}
					 <a class="button blue small recalculate float-right" style="margin-right:300px;  margin-top: -60px; "  href="#" data-value="">recalculate price</a>
			
			</ul>
		
			
		
		</div>
		
		</div>
		{/if}
		{/foreach}
		</div>
	<div class="seperator"></div><div class="seperator"></div>
	
	<div class="excursions-header">
	<h2>Drink Packages</h2>
	</div>
	<div class="excursions-message">
	<span>Select any of the following available packages for your cruise </span>
	</div>
	<div class="all-excursions">
		
		{foreach from=$components.MiscellaneousProducts.MiscellaneousItem item=a name=b}
		{if $a.MiscellaneousInfo.MiscItemType eq "DRP"}
		<div class="float-left">
		<div class="excrusion-item">
		<input type="checkbox" class="excursion" id="excursion{$a.ComponentInfo.ComponentID}" name="ex{$a.ComponentInfo.ComponentID}" value="{$a.PriceInfo.PriceAmount}" data-price="{$a.PriceInfo.PriceAmount}" data-newprice="{$a.PriceInfo.PriceAmount}" data-componentid="{$a.ComponentInfo.ComponentID}"/>
			<span class="exc-desc">{$a.MiscellaneousInfo.MiscItemDescription}</span>
			<div class="seperator15"></div>
			<span class="exc-more-info">{$a.AvailabilityInfo.AvailabilityStatusDesc} {$a.ActivityInfo.OccurrenceDate}</span>
			<span class="exc-more-info-bold">Price:&nbsp;</span><span class="exc-price" id="excprice{$a.ComponentInfo.ComponentID}">{$a.PriceInfo.PriceAmount}</span><span class="exc-price-cur"> {$retrieve.BookingContext.BookingCurrencyCode}</span>
			
			
		</div>
		
		</div>
		{/if}
		{/foreach}
		</div>
	<div class="seperator"></div><div class="seperator"></div>
	</div> <!-- end right -->
	
	
	 <br class="clear"/>
	 <div class="spacer-40"></div>
	<center> <b>Total Amount: <span id="total-amount-to-pay">{$retrieve.BookingInfo.BookingCharges.GrossBalanceDue}</span> {$retrieve.BookingContext.BookingCurrencyCode}</b><a data-div="myDiv" class=" button blue larger spacer-left" href="#">PAY NOW</a></center>
	 <input type="hidden" id="total-amount" value="{$retrieve.BookingInfo.BookingCharges.GrossBalanceDue}"/>
	 <input type="hidden" id="balance" value="{$retrieve.BookingInfo.BookingCharges.GrossBalanceDue}"/>
	 <div class="spacer-40"></div>
	 
<input type="hidden" value="{$retrieve.SessionInfo.SessionID}" class="" name="SessionID" id="SessionID"/>	 
	 
	 
	 <br/><br/><br/>
	 
	 <form method="post" action="http://www.louisres.com/cgi-bin/testres.sh/webinit.p?SCR=PM2&FCT=ADDPMT" id="checkout">
<p>
Dear Customer the total amount for our cruise is <span class="red"> #= data.BookingInfo.BookingCharges.TotalGrossPrice # #= data.BookingContext.BookingCurrencyCode # </span>.<div class="seperator"></div>
The minimum amount you need to pay so as to confirm your booking is <div class="seperator"></div>
<input type="text" value="#= data.BookingInfo.BookingCharges.DepositAmountDue #" name="TxtAmount" id="TxtAmount" class="payment-inpt blue"/> #= data.BookingContext.BookingCurrencyCode # .<br/><br/><br/>
If you choose to pay a deposit you will have to pay the rest amount until <span class="red">#= data.BookingInfo.BookingCharges.FinalPaymentDate #</span><br/><br/><br/>
If you want to pay as a deposit a bigger amount than <span class="red">#= data.BookingInfo.BookingCharges.DepositAmountDue # #= data.BookingContext.BookingCurrencyCode #</span> or if you want can make a <br/>
whole payment now type in the above box an amount from <span class="red">#= data.BookingInfo.BookingCharges.DepositAmountDue # - #= data.BookingInfo.BookingCharges.TotalGrossPrice # #= data.BookingContext.BookingCurrencyCode # </span>
<br/><br/><br/>

<input type="hidden" value="#= data.BookingInfo.BookingCharges.DepositAmountDue #" name="TxtAmount" id="TxtAmount" class="payment-inpt blue"/>
<input type="hidden" name="CONTEXT_SES-ID" id="CONTEXT_SES-ID" value="#= data.SessionInfo.SessionID #"/>
<input type="hidden" name="CONTEXT_PROFILE" id="CONTEXT_PROFILE" value="PUB"/>
<input type="hidden" name="CONTEXT_USER-ID" id="CONTEXT_USER-ID" value="PUBLIC"/>
<input type="hidden" name="CONTEXT_AGENT-ID" id="CONTEXT_AGENT-ID" value="lhcxml"/>
<input type="hidden" name="CONTEXT_BKGNO" id="CONTEXT_BKGNO" value="#= data.BookingContext.BookingNo #"/>
<input type="hidden" name="CONTEXT_CURRENCY" id="CONTEXT_CURRENCY" value="#= data.BookingContext.BookingCurrencyCode #"/>
<input type="hidden" name="CONTEXT_OFFICE" id="CONTEXT_OFFICE" value="#= data.BookingContext.OfficeCode #"/>
<input type="hidden" name="TxtCurrcy" id="TxtCurrcy" value="#= data.BookingContext.BookingCurrencyCode #"/>
<input type="hidden" name="TxtPtp" id="TxtPtp" value="CC"/>
<input type="hidden" name="ReturnURL" id="ReturnURL" value="http://marketers.gr/bookNow.php"/>
<br class="clear"/><div class="spacer-40"></div>
<a href="\\#"class="button blue larger float-right spacer-right-big" id="proceed-checkout" >CONTINUE</a>
<div class="seperator"></div>
<div id="hidden-msg-proceed"></div>
<p>
</form>
	 
	 
	 <script type="text/javascript">
	 head.js("/scripts/site/retrieve.js");
	 </script>
	 