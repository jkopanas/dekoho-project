<?xml version="1.0" encoding="UTF-8" ?>
<DtsRepriceItemRequestMessage>
  <SessionInfo>
    <SessionID>{$recalculate.sessionid}</SessionID>
    <Profile>P</Profile>
    <Language>ENG</Language>
    <Version>1</Version>
  </SessionInfo>
  <PricingShopInfo>
    <NoAdults>{$numberofadults}</NoAdults>
    <NoChildren>{$numberofchildren}</NoChildren>
    <ChildInfo>
      <ChildAge/>
    </ChildInfo>
    <PersonNos>
     {foreach from=$personsno item=a name=b}
      <PersonNo>{$a}</PersonNo>
      {/foreach}
    </PersonNos>
  </PricingShopInfo>
  <ComponentID>{$componentid}</ComponentID>
</DtsRepriceItemRequestMessage>