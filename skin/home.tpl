<!DOCTYPE html>
<html xmlns:ng="http://angularjs.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dekoho | The Hotel Finder</title>


<link href="/scripts/site/plugins/jquery.customSelect.css" rel="stylesheet" type="text/css" media="all"/>
<link href="/css/site/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href="/scripts/jquery-ui-1.10.0.custom.min.css" rel="stylesheet" type="text/css" media="all"/>

   <!--[if lte IE 8]>
   <script src="/scripts/site/app/json3.min.js"></script>
      <script>
        document.createElement('ng-include');
        document.createElement('ng-pluralize');
        document.createElement('ng-view');
 
        // Optionally these for CSS
        document.createElement('ng:include');
        document.createElement('ng:pluralize');
        document.createElement('ng:view');
      </script>
    <![endif]-->
<script type="text/javascript" src="/scripts/jquery-1.8.3.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&language=en"></script>
<script src="/scripts/site/app/markerclusterer.js"></script>
<script type="text/javascript" src="/scripts/angular.min.js"></script>
<script type="text/javascript" src="/scripts/jquery-ui-1.10.0.custom.min.js"></script>
<script src="/scripts/site/moment.min.js"></script>
<script type="text/javascript" src="/scripts/site/app/app.js"></script>
<script src="/scripts/site/app/gMaps.js"></script>
<script src="/scripts/site/app/controllers.js"></script>
<script src="/scripts/site/app/directives.js"></script>
<script src="/scripts/site/app/filters.js"></script>
<script src="/scripts/site/app/services.js"></script>
<script src="/scripts/angular-resource.min.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/site/plugins/jquery.uniform.min.js"></script>
<script language="javascript" type="text/javascript" src="/scripts/site/plugins/jquery.customSelect.min.js"></script>
<script src="/scripts/site/jquery.infinitescroll.min.js"></script>
<script src="/scripts/bootstrap.min.js"></script>
<script type="text/javascript" src="/scripts/site/swfobject.js"></script>

{literal}
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5113159f386716f1"></script>
{/literal}
</head>

<body ng-controller="mainCtrl" id="ng-app" ng-app="mcms">

<!-- analytics tracking -->
{literal}
<script>
var _gaq=_gaq||[];_gaq.push(['_setAccount','UA-41127468-1']);_gaq.push(['_trackPageview']);(function(){var ga=document.createElement('script');ga.type='text/javascript';ga.async=true;ga.src=('https:'==document.location.protocol?'https://ssl':'http://www')+'.google-analytics.com/ga.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);})();
</script>
{/literal}
<!-- end analytics tracking -->


<input type="hidden" value="{$FRONT_LANG}" name="loaded_language" id="loaded_language"/>
<input type="hidden" value="{$exchange}" name="exchange" />
<input type="hidden" value="{$current_currency.currency}" name="currencyType" />
<div id="wrapper">

	<div id="header-wrapper">
		  <div class="content">
		  		
		  		<ul class="top-menu float-left">
			  		<li class="menu-item"><a href="/{$FRONT_LANG}/index.html" {if $nav_area eq "home"}class="active"{/if}>Home</a></li>
			  		{include file="modules/themes/menu_recursive.tpl" menu=$TopMenu.items level=0 class="active"}
			  		
			  		<li class="menu-item pad_last"><a href="/{$FRONT_LANG}/hotelier/index.html" class="bold-hotelier">Sign in</a><span style="color:#fff;"> for Hotel Partners. Not a Hotel Partner yet?</span> <a href="/{$FRONT_LANG}/register_hotelier.html" class="bold-hotelier" style="margin-left:0px;">Sign up now!
                     </a></li>
		  		</ul>
		  		
		  		
		  		<form method="POST" action="" name="currencyForm" id="currencyForm">
		  		<div id="currency-language" class="float-right">
			  		<div class="currency float-left" >
			  			<ul class="nav ">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$current_currency.symbol} {$current_currency.currency} <b class="caret"></b></a>
                <ul class="dropdown-menu all-currency">
                 {foreach from=$currencies item=a name=b}
                  <li><a href="#" data-link="{$a.currency}" class="currency" >{$a.symbol}  {$a.currency}</a></li>      
                 {/foreach}
                </ul>
               
              </li>
            </ul>
            
			  		</div>
			  		 <input type="hidden" name="currency" value="" id="currency" />
			  		<div class="language float-left">
			  		<ul class="nav ">
              <li class="dropdown">
               {foreach from=$availableLanguages item=a name=b}
						{if $a.code eq $FRONT_LANG} 
                <a data-toggle="dropdown" class="dropdown-toggle" href="{$a.thisURL}"><img src="/images/flags/{$a.code}.png" class="flag" />&nbsp; &nbsp;{$a.country} <b class="caret"></b></a>
                {/if}
                {/foreach}
               <!--   <ul class="dropdown-menu">
                {foreach from=$availableLanguages item=a name=b}
						{if $a.code ne $FRONT_LANG}
                  <li><a href="{$a.thisURL}"><img src="/images/flags/{$a.code}.png" class="flag-hidden" />&nbsp; &nbsp;{$a.country}</a></li> 
                      {/if}
                {/foreach}     
                </ul>-->
              </li>
             
            </ul>
			  		</div>
		  		</div>
				</form>
		  		<div id="social" class="float-right spacer-right">
			  		    <div class="addthis_toolbox addthis_default_style ">
	<a class="addthis_button_facebook"></a>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_google_plusone_share"></a>
<a class="addthis_button_email"></a>
	<a class="addthis_counter addthis_bubble_style"></a>
	</div>
		  		</div> <!-- end social-currency  -->
		  		
		  </div> <!--end header-content -->
		  <br class="clear_0"/>
	</div> <!--end header-wrapper -->
	
	
	{include file="top_wrapper.tpl"}


{include file=$include_file}


  <div id="footer-wrapper">
	 <div class="content">
	
	<div id="bottom-banner">
	<div id="dekoho3"></div>
	{literal}
<script type="text/javascript">
	var flashvars = {
	xml_path: "/xml/xmlfile.xml"
	};
	var params = {wmode:"transparent"};
	var attributes = {};
	swfobject.embedSWF("/swf/dekoho3.swf", "dekoho3", "1000", "300", "9.0.0",
	"expressInstall.swf", flashvars, params, attributes);
</script>
{/literal}
	</div>
	
	<div id="fotoer-menu" class="float-left">
	          <ul class="bottom-menu float-left">
			  		<li class="bottom-menu-item"><a href="/{$FRONT_LANG}/index.html">Home</a>&nbsp;|</li>
			  		<li class="bottom-menu-item"><a href="#">About</a>&nbsp;|</li>
			  		<li class="bottom-menu-item"><a href="#">Terms of Use</a>&nbsp;|</li>
			  		 <li class="bottom-menu-item"><a href="#">FAQ</a>&nbsp;</li>
			  		<!--<li class="bottom-menu-item"><a href="#">Contact Us</a>&nbsp;|</li>
			  		<li class="bottom-menu-item"><a href="#">Newsletter</a></li>-->
		  		</ul>
		  		
	</div>
	<div class="float-right">
		  <span class="copy">&copy; Copyright 2013 Dekoho All Rights Reserved</span>
		  </div>
	</div> <!-- end footer-content --> 
	<div class="seperator"></div>
	</div><!--   end footer-wrapper -->
	
<input type="hidden" id="popuponce" name="popuponce" value="{$popuponce}" />
<div id="myModal" class="modal hidden " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">DEKOHO.COM</h3>
  </div>
  
  <div class="modal-body">
  <iframe width="525" height="315" src="http://www.youtube.com/embed/bEOTDCzADfI?rel=0" frameborder="0" allowfullscreen></iframe>
  </div>
  
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>
</div><!-- end wrapper -->

<script src="/scripts/site/global.js" type="text/javascript"></script>
</body>
</html>
