	<div id="page-wrapper">
			<div class="content">
			{if $moreurl!="?"}
		<span class="special-item-back"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#{$moreurl}">Back to search results</a></span><br/><br/>	
		{/if}
		<ul class="hotel-menu">
		    <li class="special-item"><a  href="/{$FRONT_LANG}{$preview}/hotel/information/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Hotel Information</a>&nbsp;|</li>
		    <li class="special-item"><a  href="/{$FRONT_LANG}{$preview}/hotel/rooms/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Rooms</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/location/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Location</a>&nbsp;|</li>
		    <li class="special-item"><a class="active" href="/{$FRONT_LANG}{$preview}/hotel/hot-spots/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Hotspots</a>&nbsp;|</li>
		    <!--  <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/offers/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Special Offers</a>&nbsp;|</li>-->
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/policy/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Policy</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/contact/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Contact</a></li>
		  </ul>	
		  

		<div class="seperator"></div><div class="seperator"></div>			
			
			<div class="location-map">
			
			<div id="map" class="float-left" style="width:100%; height:500px;"></div>
	
			<input type="hidden" id="title" name="title" value="{$title}" />
			<input type="hidden" id="description" name="description" class="get" value="{$description}" />
			<input type="hidden" id="lat" name="lat" value="{$latt}" />
			<input type="hidden" id="lng" name="lng"  value="{$lngg}" />
			<input type="hidden" id="image" name="image" value="{$image}" />
			<input type="hidden" id="zoomLevel" name="zoomLevel"value="13" />
			<input type="hidden" id="MapTypeId" name="MapTypeId" value="roadmap" />
			
			<input type="hidden" id="hotelgeocoderAddress" name="hotelgeocoderAddress" value="{$mapHotel.0.geocoderAddress}" />
			<input type="hidden" id="hotellat" name="hotellat" value="{$mapHotel.0.lat}" />
			<input type="hidden" id="hotellng" name="hotellng" value="{$mapHotel.0.lng}" />
			<input type="hidden" id="hotelzoomLevel" name="hotelzoomLevel" value="13" />
			<input type="hidden" id="hotelMapTypeId" name="hotelMapTypeId" value="roadmap" />
				
				
			</div> <!-- end location-map -->
			
			<div class="seperator"></div><div class="seperator"></div>
			
		<div class="location-items-container">
		
		    <h2 class="location-header float-left">Hotspots</h2><div class="the-line" style="width:90%;"></div>
		
		<div class="seperator"></div>
			<div id="location-hotspots-items">
			
			
			
			<div id="loc-hot-all-items">
		
		<div class="float-left">
		{assign var="i" value="1"}
				{foreach from=$spot_items item=a name=b}
				{if $smarty.foreach.b.index lt 5}
				<div class="loc-spot-item">
						  <img src="/images/site/{$i}.png" alt="" class="spacer-right icons-hot-spots" width="20"/>
						  <span class="loc-title">{$a.title}</span>
						<div class="seperator-small"></div>
							<div class="loc-description"><span class="loc-desc">{$a.description}</span></div>
					</div>
					{$i=$i+1}
					{/if}
			{/foreach}
			</div>
			
			
			
			
			<div class="float-left" style="padding-left:130px;">	  
		{assign var="i" value="6"}
				{foreach from=$spot_items item=a name=b}
				{if $smarty.foreach.b.index gt 4 and $smarty.foreach.b.index lt 9}
				<div class="loc-spot-item">
						  <img src="/images/site/{$i}.png" alt="" class="spacer-right icons-hot-spots" width="20" />
						  <span class="loc-title">{$a.title}</span>
						<div class="seperator-small"></div>
							<div class="loc-description"><span class="loc-desc">{$a.description}</span></div>
					</div>
					{$i=$i+1}
					{/if}
			{/foreach}
			
			</div>
			
			
			
			<div class="float-left" style="padding-left:130px;">	  
	{assign var="i" value="10"}
				{foreach from=$spot_items item=a name=b}
				{if $smarty.foreach.b.index gt 8 and $smarty.foreach.b.index lt 12}
				<div class="loc-spot-item">
						  <img src="/images/site/{$i}.png" alt="" class="spacer-right icons-hot-spots" width="20" />
						  <span class="loc-title">{$a.title}</span>
						<div class="seperator-small"></div>
							<div class="loc-description"><span class="loc-desc">{$a.description}</span></div>
					</div>
					{$i=$i+1}
					{/if}
			{/foreach}
			</div>
			
			
			
			</div>
			
			</div>
		

		
		</div>	<!-- end location-items-container -->
	
			<div class="seperator"></div><div class="seperator"></div>
			
			</div>


</div>

<script src="/scripts/site/maps/gMaps3.js"></script>
<script src="/scripts/site/maphotspots.js"></script>