<div id="page-wrapper">
	<div class="content">
		{if $moreurl!="?"}
		<span class="special-item-back"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#{$moreurl}">Back to search results</a></span><br/><br/>	
		{/if}	
		<ul class="hotel-menu">
		    <li class="special-item"><a  href="/{$FRONT_LANG}{$preview}/hotel/information/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Hotel Information</a>&nbsp;|</li>
		    <li class="special-item"><a  href="/{$FRONT_LANG}{$preview}/hotel/rooms/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Rooms</a>&nbsp;|</li>
		    <li class="special-item"><a class="active" href="/{$FRONT_LANG}{$preview}/hotel/location/3/{$hotel.permalink}.html{$moreurl}">Location</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/hot-spots/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Hotspots</a>&nbsp;|</li>
		<!--    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/offers/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Special Offers</a>&nbsp;|</li>--> 
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/policy/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Policy</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/contact/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Contact</a></li>
		  </ul>	
		<div class="seperator"></div><div class="seperator"></div>			
			
			<div class="location-map">
			
			<div id="map" class="float-left" style="width:100%; height:500px;"></div>
			
			<input type="hidden" id="geocoderAddress" name="geocoderAddress" value="{$mapItems.0.geocoderAddress}" />
			<input type="hidden" id="lat" name="lat" class="get" value="{$mapItems.0.lat}" />
			<input type="hidden" id="lng" name="lng" class="get" value="{$mapItems.0.lng}" />
			<input type="hidden" id="zoomLevel" name="zoomLevel" class="get" value="{$mapItems.0.zoomLevel}" />
			<input type="hidden" id="MapTypeId" name="MapTypeId" class="get" value="{$mapItems.0.MapTypeId}" />
			
				
			</div> <!-- end location-map -->
			
			<div class="seperator"></div><div class="seperator"></div>
			
		<div class="location-items-container">
		
		    <h2 class="location-header float-left">Location Map and Basic Distances</h2><div class="the-line"></div>
		<div class="seperator"></div>
			<div id="location-hotspots-items">
			
			<div class="seperator"></div>
			<div id="loc-hot-all-items">
		
		<div class="float-left">	  
				
				{foreach from=$places item=a name=b}
				{if $smarty.foreach.b.index lt 5}
				<div class="loc-spot-item">
						  <img src="/images/site/{$a.var_name}.png" alt="" class="spacer-right" />
						  <span class="loc-title">{$a.field}</span>
						<div class="seperator-small"></div>
							<div class="loc-description"><span class="loc-desc">{$a.value}</span></div>
					</div>
					{/if}
			{/foreach}
					
					
			</div>
			
			
			
			
			<div class="float-left" style="padding-left:130px;">	  
		{foreach from=$places item=a name=b}
				{if $smarty.foreach.b.index gt 4 and $smarty.foreach.b.index lt 9}
				<div class="loc-spot-item">
						  <img src="/images/site/{$a.var_name}.png" alt="" class="spacer-right" />
						  <span class="loc-title">{$a.field}</span>
						<div class="seperator-small"></div>
							<div class="loc-description"><span class="loc-desc">{$a.value}</span></div>
					</div>
					{/if}
			{/foreach}
			
			</div>
			
			
			
			<div class="float-left" style="padding-left:130px;">	  
				{foreach from=$places item=a name=b}
				{if $smarty.foreach.b.index gt 8 and $smarty.foreach.b.index lt 12}
				<div class="loc-spot-item">
						  <img src="/images/site/{$a.var_name}.png" alt="" class="spacer-right" />
						  <span class="loc-title">{$a.field}</span>
						<div class="seperator-small"></div>
							<div class="loc-description"><span class="loc-desc">{$a.value}</span></div>
					</div>
					{/if}
			{/foreach}
			</div>
			
			
			
			
			
			
			
			</div>
			
			
			
			</div>
			
			</div>
		
	</div>	<!-- end location-hotspots container -->
	
			<div class="seperator"></div><div class="seperator"></div>
 </div>



<script src="/scripts/site/maps/gMaps3.js"></script>
<script src="/scripts/site/mapfront.js"></script>