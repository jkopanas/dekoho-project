	<div id="page-wrapper">
			<div class="content">
			{if $moreurl!="?"}
		<span class="special-item-back"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#{$moreurl}">Back to search results</a></span><br/><br/>	
		{/if}
		<ul class="hotel-menu">
		    <li class="special-item"><a  href="/{$FRONT_LANG}{$preview}/hotel/information/{$hotelid}/aavox.html{$moreurl}">Hotel Information</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/rooms/{$hotelid}/aavox.html{$moreurl}">Rooms</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/location/{$hotelid}/aavox.html{$moreurl}">Location</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/hot-spots/{$hotelid}/aavox.html{$moreurl}">Hotspots</a>&nbsp;|</li>
			<!-- - <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/offers/{$hotelid}/aavox.html{$moreurl}">Special Offers</a>&nbsp;|</li>-->
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/policy/{$hotelid}/aavox.html{$moreurl}">Policy</a>&nbsp;|</li>
		    <li class="special-item"><a  class="active" href="/{$FRONT_LANG}{$preview}/hotel/contact/{$hotelid}/aavox.html{$moreurl}">Contact</a></li>
		  </ul>	
		<div class="seperator"></div><div class="seperator"></div>			
			
			<div id="contact-container">
			
				<div class="float-left">
				
				<div class="contact-item">
				 <span>First Name</span>
				<div class="seperator"></div>
				<input type="text" class="grey SearchText"/>
				</div>
				
				<div class="contact-item">
				 <span>Last Name</span>
				<div class="seperator"></div>
				<input type="text" class="grey SearchText"/>
				</div>
				
				
				<div class="contact-item">
				 <span>Subject</span>
				<div class="seperator"></div>
				<input type="text" class="grey SearchText"/>
				</div>
				
				<div class="contact-item">
				 <span>Message</span>
				<div class="seperator"></div>
				<textarea class="grey SearchText" style="height:200px;" rows="15" cols="25"></textarea>
				</div>
				<div class="seperator"></div>
				<a class="btn btn-info btn-midium float-right">SEND</a>
			</div>
			
			
			<div class="float-right contact-details">
			
			<span>{$hotel.title}</span><br/>
			{foreach from=$location item=a name=b}
				{foreach from=$a item=c name=d}	
			<span>{$c}</span><br/>
			{/foreach}
			{/foreach}
			<div class="seperator"></div><div class="seperator"></div>
			<span><b>P:</b> {$hotel.settings.hoteltelephone}</span> <br/>
			<span><b>F:</b> {$hotel.settings.hotelfax}</span> <br/>
			<span><b>E:</b> {$hotel.settings.hotelmail}</span>
			</div>
			
			<div class="seperator-small"></div>
			
			
		</div>
			
			
			
			</div>
</div>

<script src="/scripts/site/hotel_contact.js"></script>
