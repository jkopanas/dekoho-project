	<div id="page-wrapper">
			<div class="content">
			{if $moreurl!="?"}
		<span class="special-item-back"><a href="http://dekoho.net-tomorrow.com/{$FRONT_LANG}/search.html#{$moreurl}">Back to search results</a></span><br/><br/>	
		{/if}
		<ul class="hotel-menu">
		    <li class="special-item"><a  href="/{$FRONT_LANG}{$preview}/hotel/information/{$hotelid}/aavox.html{$moreurl}">Hotel Information</a>&nbsp;|</li>
		    <li class="special-item"><a class="active" href="/{$FRONT_LANG}{$preview}/hotel/rooms/{$hotelid}/aavox.html{$moreurl}">Rooms</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/location/{$hotelid}/aavox.html{$moreurl}">Location</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/hot-spots/{$hotelid}/aavox.html{$moreurl}">Hotspots</a>&nbsp;|</li>
		   <!--   <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/offers/{$hotelid}/aavox.html{$moreurl}">Special Offers</a>&nbsp;|</li>-->
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/policy/{$hotelid}/aavox.html{$moreurl}">Policy</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/contact/{$hotelid}/aavox.html{$moreurl}">Contact</a></li>
		  </ul>	
		<div class="seperator"></div><div class="seperator"></div>			
			
			<div class="main-room">
			
				<div class="float-left room-desc">
					
					<span class="title">{$cheaper_room.title} ({$cheaper_room.room_type} persons)</span>
					
					<div class="seperator"></div>
					
						<span class="title">Room Description</span>
				  <div class="seperator-small"></div>
					<span class="description">
					{$cheaper_room.description} 
						</span>
							<div class="seperator"></div>
						<span class="title">Facilities</span>
						<div class="seperator-small"></div>
						<span class="description">
					{foreach from=$cheaper_room.roomsfacilities item=a name=b}
						{if !$smarty.foreach.b.last}
						 {$a},
						{else}
						{$a}
						{/if}
					{/foreach}

							</span>
						<div class="seperator"></div>	
							
						<!--  <a class="btn btn-warning btn-medium book-room-btn" href="/{$FRONT_LANG}/hotel/rooms/{$hotelid}/aavox.html"><span>Book this Room</span></a>-->	
							
				</div>
	
				<div class="imgGallery float-right">
				   <div class="thumbs room-images float-left">
				   {foreach from=$item_room.detailed_images.images item=a name=b}
					<a href="{$a.image_full.big_thumb.image_url}" class="img" title="" ><img src="{$a.image_full.thumb.image_url}" width="100" height="65" alt="" class="float-left img-room-small"/></a>
					{/foreach}
				   </div>
				   
				   <div class="mainImg main-room-img float-left">
				   <img src="{$item_room.detailed_images.images.0.image_full.big_thumb.image_url}" class="img" width="450" height="295" />
				   </div>
				</div>
				
			</div> <!-- end img container -->
			
			<div class="seperator"></div>
			
			
			
			<div class="more-rooms-container">
			
			<div class="dotted-seperator"></div> 
				 
				 
			  {foreach from=$next_hotel_rooms item=a name=b}	 
				 <div class="more-room">
				 		<div class="float-left the-room">
				 		<span class="title">{$a.title}</span>&nbsp;<a class="btn btn-success btn-small spacer-left" style="padding:0px 6px 0px 6px; margin-bottom:10px;" href="/{$FRONT_LANG}{$preview}/room/{$a.id}/room.html">view</a>
				 		<div class="seperator-small"></div>
				 		<span class="title">({$a.room_type} persons)</span>
				 		</div>
				 		{foreach from=$next_rooms_details item=c name=d}
				 		{if $a.id eq $c.id}
				 		{foreach from=$c.detailed_images.images item=e name=f}
				 		<img src="{$e.image_full.thumb.image_url}" width="100" height="65" alt="" class="float-right img-room-small-more"/>
				 		{/foreach}
				 		{/if}
				 		{/foreach}
							<div class="float-right" style="padding-right:40px;">
							<span class="room-price">{$a.price} {$current_currency.symbol}</span><br/>
						
						<!-- <a href="#" class="btn btn-warning btn-medium book-room-btn"><span >Book this Room</span></a> -->

						</div>
				<div class="seperator-small"></div>
				 </div> 

			<div class="dotted-seperator"></div> 
				
			{/foreach}
					
			
			</div><!-- end more-rooms-container -->
			
			</div>


</div>


<script src="/scripts/site/gallery.js"></script>
		