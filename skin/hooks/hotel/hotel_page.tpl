	<div id="page-wrapper">
			<div class="content">
			{if $moreurl!="?"}
		<span class="special-item-back"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#{$moreurl}">Back to search results</a></span><br/><br/>	
		{/if}
		<ul class="hotel-menu">
		    <li class="special-item"><a class="active" href="/{$FRONT_LANG}{$preview}/hotel/information/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Hotel Information</a>&nbsp;|</li>
		    <li class="special-item"><a  href="/{$FRONT_LANG}{$preview}/hotel/rooms/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Rooms</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/location/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Location</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/hot-spots/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Hotspots</a>&nbsp;|</li>
		   <!--   <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/offers/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Special Offers</a>&nbsp;|</li>-->
		    <li class="special-item"><a  href="/{$FRONT_LANG}{$preview}/hotel/policy/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Policy</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/contact/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Contact</a></li>
		  </ul>
		<div class="seperator"></div><div class="seperator"></div>			
			  
			<div class="imgGallery">
				<div class="thumbs float-left" style="width:410px;">
					{foreach from=$hotel.detailed_images.images item=a name=b}
						{if $smarty.foreach.b.index == 12}
	   						{break}
						{/if}
					<a href="{$a.big_thumb}" class="img" title="" ><img src="{$a.thumb}" width="115" height="75" alt="" class="float-left img-hotel-small"/></a>
					  
					{/foreach}
				
				</div>
				<div class="mainImg float-right">
				<div class="loader hidden"><img src="/images/ajax-loader.gif" /></div>
				   <img src="{$hotel.detailed_images.images.0.big_thumb}" width="570" class="img" height="375" alt=""/>
				</div>		
			</div> <!-- end img container -->
			
			<div class="seperator"></div>
			
			<div class="hotel-info-container">
			 
				  <span class="title">Hotel Description</span>
				  <div class="seperator-small"></div>
					<span class="description">
					{$hotel.description} 
					</span>
							<div class="seperator"></div>
							
						{if $hotel_prop.types}	
						<span class="title">Type of Hotel</span>
				  <div class="seperator-small"></div>
					<span class="description">
					{foreach from=$hotel_prop.types item=a name=b}
						{if !$smarty.foreach.b.last}
						 {$a.title},
						{else}
						{$a.title}
						{/if}
					{/foreach}
					</span>
					<div class="seperator"></div>
					{/if}
						
						{if $hotel_prop.facilities}	
						<span class="title">Hotel Facilities</span>
				  <div class="seperator-small"></div>
					<span class="description">
					{foreach from=$hotel_prop.facilities item=a name=b}
						{if !$smarty.foreach.b.last}
						 {$a.title},
						{else}
						{$a.title}
						{/if}
					{/foreach}
					</span>
						
						<div class="seperator"></div>
						{/if}
						
				{if $hotel_prop.services}	
						<span class="title">Services at Hotel</span>
				  <div class="seperator-small"></div>
					<span class="description">
					{foreach from=$hotel_prop.services item=a name=b}
						{if !$smarty.foreach.b.last}
						 {$a.title},
						{else}
						{$a.title}
						{/if}
					{/foreach}
					</span>
				<div class="seperator"></div>
				{/if}
				
				{if $hotel_prop.themeshotels}
				<span class="title">Hotel Themes</span>
				  <div class="seperator-small"></div>
					<span class="description">
					 {foreach from=$hotel_prop.themeshotels item=a name=b}
						{if !$smarty.foreach.b.last}
						 {$a.title},
						{else}
						{$a.title}
						{/if}
					{/foreach}
					</span>
				{/if}
			</div><!-- end hotel-info-container -->
			
			</div>


</div>


<script src="/scripts/site/gallery.js"></script>
		<script type="text/javascript">
			
		</script>