
	<div id="page-wrapper">
			<div class="content">
			{if $moreurl!="?"}
		<span class="special-item-back"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#{$moreurl}">Back to search results</a></span><br/><br/>	
		{/if}
		<ul class="hotel-menu">
		    <li class="special-item"><a  href="/{$FRONT_LANG}{$preview}/hotel/information/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Hotel Information</a>&nbsp;|</li>
		    <li class="special-item"><a  href="/{$FRONT_LANG}{$preview}/hotel/rooms/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Rooms</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/location/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Location</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/hot-spots/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Hotspots</a>&nbsp;|</li>
		    <li class="special-item"><a class="active" href="/{$FRONT_LANG}{$preview}/hotel/offers/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Special Offers</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/policy/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Policy</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/contact/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Contact</a></li>
		  </ul>	
		<div class="seperator"></div><div class="seperator"></div>			
			
			<div class="main-room">
			
				<div class="float-left room-desc" id="special-top-room">
					
					<div class="float-left spacer-right" style="width:140px;">
					<span class="title" id="font15">Bussiness Room (single room)</span>
					
					<div class="seperator"></div>
					
					<span class="special-top-hotel spacer-right">Discount</span><span class="special-hotel-color" >50%</span>
					<div class="seperator-small"></div>
					
					<span class="special-top-hotel spacer-right">Starts</span><span class="special-hotel-color">30/03/2013</span>	
					<div class="seperator-small"></div>
					
					<span class="special-top-hotel spacer-right">Ends</span><span class="special-hotel-color">30/04/2013</span>
					
					<div class="seperator"></div>
					<span class="special-top-hotel spacer-right">Reservation</span>
					<div class="seperator-small"></div>
					<span class="special-top-hotel spacer-right">From</span>
					<input type="text" class="grey SearchText-small" id="arrival" size="10" autocomplete="off">
					<div class="seperator-small"></div>
					<span class="special-top-hotel spacer-right">To</span>
					<input type="text" class="grey SearchText-small" id="departure" name="departure" size="10" autocomplete="off">
					</div>
					
					<div class="float-right top60">
					<a href="#" class="btn btn-primary btn-large width116"><span class="font15">Availability<br/>0/32 rooms</span> <br/><br/><span class="font36"> {$current_currency.symbol} 30</span></a> 
					<div class="seperator"></div>
					<a href="#" class="btn btn-warning btn-medium book-room-btn width130"><span>BOOK NOW</span></a>
					</div>
					
				</div>
				
				<div class="float-right">
				   <div class="room-images float-left">
				   <img src="/images/site/small-hotel-photo.jpg" width="100" height="65" alt="" class="float-left img-room-small"/>
				    <img src="/images/site/small-hotel-photo.jpg" width="100" height="65" alt="" class="float-left img-room-small"/>
				    
				    <img src="/images/site/small-hotel-photo.jpg" width="100" height="65" alt="" class="float-left img-room-small"/>
				    <img src="/images/site/small-hotel-photo.jpg" width="100" height="65" alt="" class="float-left img-room-small"/>
				    
				    <img src="/images/site/small-hotel-photo.jpg" width="100" height="65" alt="" class="float-left img-room-small"/>
				    <img src="/images/site/small-hotel-photo.jpg" width="100" height="65" alt="" class="float-left img-room-small"/>
				    
				    <img src="/images/site/small-hotel-photo.jpg" width="100" height="65" alt="" class="float-left img-room-small"/>
				    <img src="/images/site/small-hotel-photo.jpg" width="100" height="65" alt="" class="float-left img-room-small"/>
				    
				   
				   </div>
				   
				   <div class="main-room-img float-left">
				   <img src="/images/site/main_hotel_photo.jpg" width="450" height="295" />
				   </div>
				</div>
				<div class="seperator"></div>
				<div class="hotel-info-container">
					<span class="title">Description</span>
					 <div class="seperator-small"></div>
					<span class="description">
					Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum 
					</span>
				</div>
				
			</div> <!-- end img container -->
			
			<div class="seperator"></div>
			
			
			
			<div class="more-rooms-container">
			
			<div class="dotted-seperator"></div> 
				 <div class="more-room">
				 		<div class="float-left the-room">
				 		<span class="title">Rose Room</span>
				 		<span class="title">(double)</span>
				 		<div class="seperator-small"></div>
				 		<span class="special-top-hotel spacer-right">Discount</span><span class="special-hotel-color" >50%</span>
					<div class="seperator-small"></div>
					
					<span class="special-top-hotel spacer-right">Starts</span><span class="special-hotel-color">1/03/2013</span>	
					<div class="seperator-small"></div>
					
					<span class="special-top-hotel spacer-right">Ends</span><span class="special-hotel-color">25/03/2013</span>
				 		</div>
				 		<div class="float-left spacer-left spacer-top">
					<a href="#" class="btn btn-primary btn-large width134" style="padding-left:9px; padding-right:9px;"><span class="font13 italic">Availability 0/32 rooms</span> <br class="seperator-small"/><span class="font22 italic"> {$current_currency.symbol} 30</span></a> 
								<div class="seperator"></div>
						<a href="#" class="btn btn-warning btn-medium book-room-btn width130 "><span>BOOK NOW</span></a>
					</div>
				
				
				 		<img src="/images/site/small-hotel-photo.jpg" width="100" height="65" alt="" class="float-right img-room-small-more top20"/>
				 		<img src="/images/site/small-hotel-photo.jpg" width="100" height="65" alt="" class="float-right img-room-small-more top20"/>
				 		<img src="/images/site/small-hotel-photo.jpg" width="100" height="65" alt="" class="float-right img-room-small-more top20"/>
				 		<img src="/images/site/small-hotel-photo.jpg" width="100" height="65" alt="" class="float-right img-room-small-more top20"/>
				 		<img src="/images/site/small-hotel-photo.jpg" width="100" height="65" alt="" class="float-right img-room-small-more top20"/>
						<div class="seperator-small"></div>	
			 </div> 
			
			<div class="dotted-seperator"></div> 
		
		
				
			
			</div><!-- end more-rooms-container -->
			
			</div>


</div>
<script type="text/javascript" src="/scripts/site/offers.js"></script>

