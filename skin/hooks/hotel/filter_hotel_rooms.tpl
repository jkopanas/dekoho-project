	<div id="page-wrapper">
			<div class="content">
			{if $moreurl!="?" || $moreurl}
		<span class="special-item-back"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#{$moreurl}">Back to search results</a></span><br/><br/>	
		{/if}
		<ul class="hotel-menu">
		    <li class="special-item"><a  href="/{$FRONT_LANG}{$preview}/hotel/information/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Hotel Information</a>&nbsp;|</li>
		    <li class="special-item"><a class="active" href="/{$FRONT_LANG}{$preview}/hotel/rooms/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Rooms</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/location/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Location</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/hot-spots/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Hotspots</a>&nbsp;|</li>
		   <!--   <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/offers/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Special Offers</a>&nbsp;|</li>-->
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/policy/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Policy</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/contact/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Contact</a></li>
		  </ul>	
		<div class="seperator"></div><div class="seperator"></div>			
		{if !$user_rooms_simple!= ""}	
		<div class="seperator"></div><div class="seperator"></div>	
			<div class="main-room">
			
				<div class="float-left room-desc">
					
					<span class="title">{$cheaper_room.title} ({$cheaper_room.room_type} persons)</span>
					
					<div class="seperator"></div>
					
						<span class="title">Room Description</span>
				  <div class="seperator-small"></div>
					<span class="description">
					{$cheaper_room.description} 
						</span>
							<div class="seperator"></div>
						<span class="title">Facilities</span>
						<div class="seperator-small"></div>
						<span class="description">
					{foreach from=$cheaper_room.roomsfacilities item=a name=b}
						{if !$smarty.foreach.b.last}
						 {$a},
						{else}
						{$a}
						{/if}
					{/foreach}

							</span>
						<div class="seperator"></div>	
							
						<a class="btn btn-warning btn-medium book-room-btn" href="/{$FRONT_LANG}/hotel/rooms/{$hotelid}/{$hotel.permalink}.html{$moreurl}"><span>Back to Rooms</span></a>	
							
				</div>
	
				<div class="imgGallery float-right">
				   <div class="thumbs room-images float-left">
				   {foreach from=$item_room.detailed_images.images item=a name=b}
				   {if $smarty.foreach.b.index == 8}
   							 {break}
						 {/if}
					<a href="{$a.image_full.big_thumb.image_url}" class="img" title="" ><img src="{$a.image_full.thumb.image_url}" width="100" height="65" alt="" class="float-left img-room-small"/></a>
					{/foreach}
				   </div>
				   
				   <div class="mainImg main-room-img float-left">
				   <img src="{$item_room.detailed_images.images.0.image_full.big_thumb.image_url}" class="img" width="450" height="295" />
				   </div>
				</div>
				
			</div> <!-- end img container -->
			<div class="seperator"></div><div class="seperator"></div>	
			{/if}
			
			
			
			
	{if $user_rooms_simple!= ""}	
		
			
			<div class="seperator"></div><div class="seperator"></div>
			
			<a class="btn btn-primary btn-large float-right" id="make-reservation"  href="#">To reservation</a>
			<span style="font-size: 20px; margin-right: 50px; padding: 10px; float:right;" class="label label-success">Total for {$stays} nights : <span id="total-pirce"></span> {$current_currency.symbol}</span> 
    <div class="seperator"></div><div class="seperator"></div>
			{/if}
			
		<form name="reservation-form" id="reservation-form" method="post" action="/{$FRONT_LANG}/reservation/index.html">	
		
		<input type="hidden" name="hotelid" id="hotelid" value="{$hotelid}" />
		<input type="hidden" name="arrival" id="arrival" value="{$arrival}" />
		<input type="hidden" name="departure" id="departure" value="{$departure}" />
		<input type="hidden" name="charge_total" id="charge_total" value="" />
		<input type="hidden" name="currency_code" id="currency_code" value="" />
		<input type="hidden" value="{$stays}" name="nights" id="nights"/>	
			<div class="more-rooms-container">
			
			<div class="dotted-seperator"></div> 
				 
			 
			  {foreach from=$user_rooms_simple item=a name=b}
	
				 <div class="more-room">
				 		<div class="float-left the-room">
				 		
				 		<span class="title">{$a.title}</span>
				 		<div class="seperator-small"></div>
				 		<span class="title">({$a.room_type} persons) <a href="/{$FRONT_LANG}{$preview}/room/{$a.id}/room.html{$moreurl}" style="padding:0px 6px 0px 6px;" class="btn btn-success btn-small spacer-left">view</a></span>
				 		</div>
				 		
				 		{foreach from=$user_rooms_details item=c name=d}
				 		{if $a.id eq $c.id}
				 		{foreach from=$c.detailed_images.images item=e name=f}
				 		{if $smarty.foreach.f.index == 5}
   							 {break}
						 {/if} 
						 <a data-img="{$e.image_full.big_thumb.image_url}" rel="popover" class="float-right">
				 		      <img src="{$e.image_full.thumb.image_url}" data-originals="{$e.image_full.thumb.image_url}" width="100" height="65" alt="" class=" img-room-small-more the-photo"/>
				 		</a>
				 		{/foreach}
				 		{/if}
				 		{/foreach}
				 		
				 	
							<div class="float-right" style="padding-right:40px;">
							<span class="room-price-filter">{if $a.discount!="0" && $a.end_discount_date>=$the_timestamp}from {$a.price} {$a.discount}{else}{$a.price}{/if} {$current_currency.symbol}/night</span><br/>
						{if $lowest_prices[$a.room_type].id == $a.id}
						{assign var=selected value=$new_persons[$a.room_type]}
						{/if}
						<select class="grey selectrooms-persons" name="rooms[{$a.id}]" id="rooms" style="width:155px;">
						  <option value="0">0 Rooms</option>
						    {for $foo=1 to $new_persons[$a.room_type]}
								     <option value="{$foo}" data-price="{if $a.discount!="0" && $a.end_discount_date>=$the_timestamp}{$a.discount}{else}{$a.price}{/if}"  {if $foo==$selected} selected="selected" {/if}>{$foo} Rooms</option>
							{/for} 
				       </select>
													
						</div>
				<div class="seperator-small"></div>
				 </div> 
			
			
			<div class="dotted-seperator"></div> 
{assign var=selected value=0}
			
			{/foreach}
			
				
			
			</div><!-- end more-rooms-container -->
		</form>	
			</div>


</div>

<script src="/scripts/site/gallery.js"></script>
<script src="/scripts/site/filter_hotel_rooms.js"></script>

