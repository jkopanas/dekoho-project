<div id="page-wrapper">
			<div class="content">
			<div id="hidden_msg" class="hidden succ-reserv"><h4>Thank you. An email with your reservation has been send to you.</h4></div>
		<div class="right-column-inner">
				<div class="form-horizontal" id="the-individual">
				
			<!--  	<span class="individual">I am </span><input type="radio" name="individual" id="individual" checked="true" style="margin-left:44px;" /> <span style="color:#535354;">An individual</span><br/>
				<span class="company"><input type="radio" name="company" id="company"/> A Comany</span>
				<div class="seperator"></div><div class="seperator"></div> -->
				
				
				<div class="control-group">
		              <label class="control-label" for="">First Name</label>
		              <div class="controls">
		                <input type="text" class="ReservationText personalData" name="first name" id="" value="" placeholder="type your first name" required>
		              </div>
		            </div>
				
				<div class="control-group">
		              <label class="control-label" for="">Last Name</label>
		              <div class="controls">
		                <input type="text" class="ReservationText personalData" name="last name" id="" value="" placeholder="type your last name" required>
		              </div>
		            </div>
		            
		            <div class="control-group">
		              <label class="control-label" for="">E-mail</label>
		              <div class="controls">
		                <input type="text" class="ReservationText personalData" name="email" id="" value="" placeholder="type your e-mail" required>
		              </div>
		            </div>
		             
		            <div class="control-group">
		              <label class="control-label" for="">E-mail confirmation</label>
		              <div class="controls">
		                <input type="text" class="ReservationText" name="" id="" value="" placeholder="type your e-mail again" required>
		              </div>
		            </div>  
		            
		             <div class="control-group">
		              <label class="control-label" for="">Address</label>
		              <div class="controls">
		                <input type="text" class="ReservationText personalData" name="address" id="" value="" placeholder="type your address" required>
		              </div>
		            </div>
		            
		            <div class="control-group">
		              <label class="control-label" for="">Country/City</label>
		              <div class="controls">
		                <select class="ReservationText personalData" id="reservation-country" name="country" style="height:30px;" required>
		                <option>Select Your Country</option>
		               {foreach from=$countries item=a name=b}
		       			 <option value="{$a.country}">{$a.country}</option>
		       					 {/foreach}
		                </select>
		                <input type="text" class="ReservationText personalData" name="city" id="" value="" placeholder="type your city" required style="width:180px;">
		              </div>
		            </div>
		            
		            <div class="control-group">
		              <label class="control-label" for="">State/Zip Code</label>
		              <div class="controls">
		                <input type="text" class="ReservationText personalData" name="state" id="" value="" placeholder="type your state" required style="width:205px; margin-right:4px;">
		                <input type="text" class="ReservationText personalData" name="zip code" id="" value="" placeholder="type your zip code" required style="width:140px;">
		              </div>
		            </div>
		            
		            <div class="control-group">
		              <label class="control-label" for="">Telephone</label>
		              <div class="controls">
		                <input type="text" class="ReservationText personalData" name="telephone" id="" value="" placeholder="type your telephone" required>
		              </div>
		            </div>
		            
		            <div class="control-group">
		              <label class="control-label" for="">Mobile Phone (optional)</label>
		              <div class="controls">
		                <input type="text" class="ReservationText personalData" name="mobile phone" id="" value="" placeholder="type your mobile">
		              </div>
		            </div>
		            <!-- company -->
		            <div style="border-top:2px solid; border-color:#fff;" class=""></div>
		            		<div class="seperator"></div><div class="seperator"></div>
		            <a href="#" class="case-company"><span class="">In case of company please click here</span></a>
		                <div class="seperator"></div><div class="seperator"></div>
		           <div class="hidden" id="show-company"> 
			            <div class="control-group">
	              <label class="control-label" for="">Company Nane</label>
	              <div class="controls">
	                <input type="text" class="ReservationText personalData" name="company name" id="" value="" placeholder="type your company name" required>
	              </div>
	            </div>
			
			<div class="control-group">
	              <label class="control-label" for="">Legal Form</label>
	              <div class="controls">
	                <input type="text" class="ReservationText personalData" name="" id="legal form" value="" placeholder="type your first legal form" required>
	              </div>
	            </div>
			            
			            		<div class="control-group">
	              <label class="control-label" for="">Tax Number</label>
	              <div class="controls">
	                <input type="text" class="ReservationText personalData" name="tax number" id="" value="" placeholder="type your tax number">
	              </div>
	            </div>
	             <div class="control-group">
	              <label class="control-label" for="">Tax Office</label>
	              <div class="controls">
	                <input type="text" class="ReservationText personalData" name="tax office" id="" value="" placeholder="type your tax office">
	              </div>
	            </div>
	            <div class="control-group">
	              <label class="control-label" for="">Commercial register no.</label>
	              <div class="controls">
	                <input type="text" class="ReservationText personalData" name="Commercial register no" id="" value="" placeholder="type your Commercial register no">
	              </div>
	            </div>
	          </div>  
             <div class="" style="border-top:2px solid; border-color:#fff;"></div>
             <div class="seperator"></div>
		        
		     <div id="payment_types"> 
		        <div class="float-left payment-type">    
		          <span id="select-payment-type">Please select your payment type</span><br/><br/>
		          <input type="radio"  data-id="pay_hotel" class="payment" name="payment"/> <span>Pay at the hotel</span> <br/>
		      <!--      <input type="radio"  data-id="pay_paypal" class="payment" name="payment"/> <span>Pay via paypal</span> <br/>-->
		         {if $hotel.settings.iban!=""}
		          <input type="radio"  data-id="pay_bank_tr" class="payment" name="payment"/> <span>Pay via bank transfer</span> <br/>
		          {/if}
		          </div>
		          
		           <div class="float-right spacer-top" id="all-payment-types">
		            
		          <!--    <div class="float-right hidden the-payment" id="pay_paypal">{$button_paypal}</div>-->
		           <div class="float-right hidden" id="pay_hotel"><a class="btn spacer-right spacer-top complete-reservation" data-type="pay_at_hotel">Complete Reservation</a></div>
		            <div class="float-right hidden" id="pay_bank_tr">
		            <span><strong>Account owner</strong> : {$hotel.settings.hotelaccountowner}</span> <br/>
		             <span><strong>Bank Name</strong> : {$hotel.settings.bankname}</span> <br/>
		              <span><strong>IBAN</strong> : {$hotel.settings.iban}</span> <br/>
		              <span><strong>BIC</strong> : {$hotel.settings.bic}</span> <br/>
		            <a class="btn float-right spacer-top complete-reservation" data-type="pay_to_bank">Complete Reservation</a>
		            	<input type="hidden" name="account owner" class="accountData" value="{$hotel.settings.hotelaccountowner}" />
		            	<input type="hidden" name="bank name" class="accountData" class="postedData" value="{$hotel.settings.bankname}" />
		            	<input type="hidden" name="iban" class="accountData" value="{$hotel.settings.iban}" />
		            	<input type="hidden" name="bic" class="accountData" value="{$hotel.settings.bic}" />
		            </div>
		           
		            </div>
		           </div> 
		           <div class="seperator"></div><div class="seperator"></div>
		            <input type="hidden" name="hotel name" class="postedData" value="{$hotel.title}" />
		           <input type="hidden" name="hotel email" class="postedData" value="{$hotel.settings.hotelmail}" />
		           <input type="hidden" name="hotel phone" class="postedData" value="{$hotel.settings.hoteltelephone}" />
		           <input type="hidden" name="arrival" class="postedData" value="{$arrival}" />
		           <input type="hidden" name="departure" class="postedData" value="{$departure}" />
		           <input type="hidden" name="total price" class="postedData" value="{$total} {$current_currency.symbol}" />
		           
		           {if $persons}	
			{foreach from=$persons key=key item=value}
			{foreach from=$value key=k item=v}
			 <input type="hidden" name="total rooms-persons" class="postedData" value="{$key} room(s) X {$v['room_type']} person(s)" />
			
			 
			{/foreach}
			{/foreach}
			{else}
			
			{foreach from=$the_persons key=k item=v}
			  <span class="reservation-total">
			  <input type="hidden" name="total persons" class="postedData" value="{$v} room(s) X {$k} persons(s)" />
			  </span><br/>		
			  {/foreach}
			{/if}
		           
		       <input type="hidden" class="payment_front" name="rooms_ids" value="{$rooms_ids}"/>    
		</div>
		          
		            
		
		<div class="seperator"></div>
		 		</div>
		
		
		<!-- for company -->
	<!--  
		<div class="form-horizontal hidden" id="the-company">
		<span class="individual">I am </span><input type="radio" name="individual" id="individual" style="margin-left:44px;" /> An individual<br/>
		<span class="company"><input type="radio" name="company" id="company"/> A Comany</span>
		<div class="seperator"></div><div class="seperator"></div>
		
		<div class="control-group">
              <label class="control-label" for="">Company Nane</label>
              <div class="controls">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your company name" required>
              </div>
            </div>
		
		<div class="control-group">
              <label class="control-label" for="">Legal Form</label>
              <div class="controls">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your first name" required>
              </div>
            </div>
            
            
		<div class="control-group">
              <label class="control-label" for="">First Name</label>
              <div class="controls">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your first name" required>
              </div>
            </div>
		
		<div class="control-group">
              <label class="control-label" for="">Last Name</label>
              <div class="controls">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your last name" required>
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label" for="">E-mail</label>
              <div class="controls">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your e-mail" required>
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label" for="">E-mail confirmation</label>
              <div class="controls">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your e-mail again" required>
              </div>
            </div>
            
             <div class="control-group">
              <label class="control-label" for="">Address</label>
              <div class="controls">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your address" required>
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label" for="">Country/City</label>
              <div class="controls">
                <select class="ReservationText" id="reservation-country">
                <option>Greece</option>
                </select>
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your city" required style="width:180px;">
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label" for="">State/Zip Code</label>
              <div class="controls">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your state" required style="width:205px; margin-right:4px;">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your zip code" required style="width:140px;">
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label" for="">Telephone</label>
              <div class="controls">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your telephone" required>
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label" for="">Mobile Phone (optional)</label>
              <div class="controls">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your mobile">
              </div>
            </div>
            
            <div class="" style="border-top:2px solid; border-color:#fff;"></div>
             <div class="seperator"></div>             <div class="seperator"></div>
            <div class="control-group">
              <label class="control-label" for="">Tax Number</label>
              <div class="controls">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your mobile">
              </div>
            </div>
             <div class="control-group">
              <label class="control-label" for="">Tax Office</label>
              <div class="controls">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your mobile">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">Commercial register no.</label>
              <div class="controls">
                <input type="text" class="ReservationText postedData" name="" id="" value="" placeholder="type your mobile">
              </div>
            </div>
             <div class="" style="border-top:2px solid; border-color:#fff;"></div>
             <div class="seperator"></div>
            <div class="float-right spacer-right" id="the_paypal">{$button_paypal}</div>
            <div class="seperator"></div>
		</div>
		-->
		
		</div> <!-- end right-column-inner -->
		
		<div class="left-column-inner" style="margin:0px 0px 0px 50px;">

		<img src="{$hotel.image_full.big_thumb}" alt="" width="255" />
		<div class="seperator"></div>
		
		<span class="hotel-reservation-title spacer-right float-left">{$hotel.title}</span><br class="clear_0"/>
	       {while $hotel.stars > 0} 
		       <img src="/images/site/star_green.png" class="float-left hotel-reservation-star"  alt=""/>
		       {$hotel.stars = $hotel.stars-1}	 
		 	{/while}
		 	<div class="seperator"></div>
		 	{foreach from=$location item=a name=b}
				{foreach from=$a item=c name=d}		
						<span class="hotel-reservation-address">{$c}</span>
					 
					{/foreach}
					{/foreach}
				<br class="clear_0"/>
				<span class="hotel-reservation-address">Email: {$hotel.settings.hotelmail}</span><br/>
				<span class="hotel-reservation-address">Telephone: {$hotel.settings.hoteltelephone}</span>	
			<div class="seperator"></div>
			
			<span class="reservation-total">{$total_persons} Persons Total</span>	<br/>
			<span class="reservation-total">{$arrival} to {$departure} ({$stays} nights)</span>	<br/>
			<div class="seperator"></div>
		{if $persons}	
			{foreach from=$persons key=key item=value}
			{foreach from=$value key=k item=v}
			  <span class="reservation-total">
			{$key}X {$v['room_type']} persons room
			  </span><br/>		
			{/foreach}
			{/foreach}
			{else}
			
			{foreach from=$the_persons key=k item=v}
			  <span class="reservation-total">
			{$v}X {$k} persons room
			  </span><br/>		
			  {/foreach}
			{/if}
			<div class="seperator"></div>
			 <span class="reservation-total">Total amount:</span><span class="reservation-total-ammount"> {$total} {$current_currency.symbol}</span>
		</div>
			
	</div>
	<div class="seperator"></div><div class="seperator"></div><div class="seperator"></div>
	</div>
	
<script src="/scripts/mcmsFront.js"></script> 
<script src="/scripts/kendo/js/kendo.core.min.js"></script>
<script src="/scripts/kendo/js/kendo.validator.min.js"></script>
<script src="/scripts/site/reservation.js"></script>			