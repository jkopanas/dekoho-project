	<div id="page-wrapper">
			<div class="content">
			{if $moreurl!="?"}
		<span class="special-item-back"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#{$moreurl}">Back to search results</a></span><br/><br/>	
		{/if}
		<ul class="hotel-menu">
		    <li class="special-item"><a  href="/{$FRONT_LANG}{$preview}/hotel/information/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Hotel Information</a>&nbsp;|</li>
		    <li class="special-item"><a  href="/{$FRONT_LANG}{$preview}/hotel/rooms/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Rooms</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/location/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Location</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/hot-spots/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Hotspots</a>&nbsp;|</li>
		   <!--  <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/offers/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Special Offers</a>&nbsp;|</li>--> 
		    <li class="special-item"><a class="active" href="/{$FRONT_LANG}{$preview}/hotel/policy/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Policy</a>&nbsp;|</li>
		    <li class="special-item"><a href="/{$FRONT_LANG}{$preview}/hotel/contact/{$hotelid}/{$hotel.permalink}.html{$moreurl}">Contact</a></li>
		  </ul>	
		<div class="seperator"></div><div class="seperator"></div>			
			
		
			
			<div class="more-rooms-container">
			
				 <div class="more-room">
				 		<div class="float-left">
				 		<span class="title">Out Hotel's Policy</span>
				 		<div class="seperator-small"></div>
				 		<span class="description">Check in: {$hotel.efields.83.data.0.value}</span>
				 		<div class="seperator-small"></div>
				 		<span class="description">Check out: {$hotel.efields.83.data.1.value}</span>
				 		
				 		</div>
				 		
				<div class="seperator-small"></div>
				 </div> 
				 
				 
		{foreach from=$hotel.efields.83.data item=a name=b}
		{if $a.type eq "area" && $a.enable eq 1}	
			<div class="dotted-seperator"></div> 
				 <div class="more-room">
				 		<div class="float-left ">
				 		<span class="title">{$a.field}</span>
				 		<div class="seperator-small"></div>
				 		<span class="description">{$a.value}</span>
				 		
				 		
				 		</div>
				 		
				<div class="seperator-small"></div>
				 </div>
				 {/if} 
			{/foreach}
		
				 
				 <div class="dotted-seperator"></div> 
			
				 		
				<div class="seperator-small"></div>
				 </div> 
			<div class="dotted-seperator"></div> 
			
			
			</div><!-- end more-rooms-container -->
			
			</div>


</div>