<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dekoho | The Hotel Finder | Hotelier</title>

<link href="/scripts/kendo/styles/kendo.common.min.css" rel="stylesheet"/>
<link href="/scripts/kendo/styles/kendo.metro.min.css" rel="stylesheet"/>
<link href="/css/hotelier/hotelier.css" rel="stylesheet" type="text/css" media="all"/>
<link href="/css/hotelier/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>

<script type="text/javascript" src="/scripts/head.load.min.js"></script>
<script type="text/javascript">
var mcms;
head.js('/scripts/jquery-1.8.3.min.js','/scripts/mcmsFront.js','/scripts/kendo/js/kendo.web.min.js','/scripts/kendo/js/kendo.validator.min.js','/scripts/kendo/js/kendo.timepicker.min.js','/scripts/kendo/js/kendo.data.min.js','/scripts/kendo/js/kendo.binder.min.js','/scripts/kendo/js/kendo.upload.min.js','/scripts/bootstrap.min.js');
head.ready(function(){
mcms =  $(document).mCms({ lang:$('input[name=loaded_language]').val()}).data().mcms;//Initialize
$.lang = mcms.getLang();
$('body').tooltip( { selector: '[rel=tooltip]' });
});
</script>
</head>

<body>

<div id="wrapper">
<input type="hidden" value="{$FRONT_LANG}" name="loaded_language" id="loaded_language"/>
{include file=$include_file}
</div><!-- end wrapper -->

<div id="page-dark" style=" display: none; position: fixed; width: 100%;height: 100%;background-color: #000;z-index: 999;top: 0;left: 0; opacity: 0.3;" ></div>
<img id="loaderPage" src='/images/ajax-loader-bar.gif' style='position:absolute;z-index: 1000; top:40%; left:48%; width:300px; display:none;' />
</body>
</html>
