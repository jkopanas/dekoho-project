 <script id="account" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/account.js" data-type="text">

<div id="account" >
<div class="span9">
         <div class="navbar">
    <div class="navbar-inner">
    <span class="brand help" ><i class="icon-user"></i></span>
 <ul class="nav">
    <li  class="active"><a href="\\#profileManager/account">{$lang.account_information}</a></li>
    <li ><a href="\\#profileManager/billing_profiles">{$lang.billing_profiles}</a></li>
    <li><a href="\\#profileManager/email_notification">{$lang.email_notification}</a></li>
    <li><a href="\\#profileManager/change_password">{$lang.change_password}</a></li>
    </ul>
    </div>
    </div>
			<div class="alert alert-success hidden" id="success">
 				{$lang.profile_succ_up}
			</div>

			<div id="hotelier-pr">	
			 <form class="form-horizontal the-form" >
				<div class="seperator"></div><div class="seperator"></div><div class="seperator"></div>
				<div class="control-group">
              <label for="" class="control-label">UserID</label>
              <div class="controls">
                <input type="text" id="" value="#= results.uname #" class="input-xlarge" disabled>
              </div>
            </div>
				
            <div class="control-group">
              <label for="" class="control-label">{$lang.op_company}</label>
              <div class="controls">
                <input type="text" placeholder="{$lang.op_companyname}" id="" name="company_name" data-field="usersecondary"  value="#= results.details.profile.company_name #" class="input-xlarge postedData"/>
              </div>
            </div>
            
            <div class="control-group">
              <label for="" class="control-label"></label>
              <div class="controls">
               <select class="span2 postedData" name="gender" data-field="usersecondary">
               <option value="0" # if (results.details.profile.gender==0) { # selected="selected" # } # >{$lang.mr}</option>
               <option value="1" # if (results.details.profile.gender==1) { # selected="selected" # } # >{$lang.ms}</option>
               </select>
              </div>
            </div>
            <div class="control-group">
              <label for="" class="control-label">{$lang.first_name}
				 <span class="mand">*</span>
              </label>
              <div class="controls">
                <input type="text" placeholder="{$lang.your_f_name}" id="" name="user_name" data-field="usermain" value="#= results.user_name #" class="input-xlarge postedData" for="First Name" required/>
              </div>
            </div>
            
            <div class="control-group">
              <label for="" class="control-label">{$lang.last_name}
				<span class="mand">*</span>         
   			  </label>
              <div class="controls">
                <input type="text" placeholder="{$lang.your_l_name}" id=""  name="user_surname"  data-field="usermain" value="#= results.user_surname #" class="input-xlarge postedData" for="Last Name" required/>
              </div>
            </div>
             <div class="control-group">
              <label for="" class="control-label">{$lang.primary_email}
				<span class="mand">*</span>
              </label>
              <div class="controls">
                <input type="text" placeholder="{$lang.type_email}" id="email" name="email"  data-field="usermain" value="#= results.email#" class="input-xlarge postedData" required/>
              </div>
            </div>
            
            
             <div class="control-group">
              <label for="" class="control-label" style="postion:relative; botttom:5px;">{$lang.primary_emailconfirm} 
				<span class="mand">*</span>
              </label>
              <div class="controls">
                <input type="text" placeholder="{$lang.retype_email}" id="" name="confirm_email" value="#= results.email#" class="input-xlarge" required data-verifymails-msg="E-mails don't match"/>
              </div>
            </div>
            
            
            <div class="control-group">
              <label for="" class="control-label">{$lang.email_2}</label>
              <div class="controls">
                <input type="text" placeholder="{$lang.type_alt_email}"  value="#= results.details.profile.settings.email2 #" id="email2" data-field="usersecondary" name="settings-email2" value="" id="" class="input-xlarge postedData"/>
              </div>
            </div>
            
            <div class="control-group">
              <label for="" class="control-label">{$lang.email_2confirm}</label>
              <div class="controls">
                <input type="text" placeholder="{$lang.retype_alt_email}" id="" value="#= results.details.profile.settings.email2 #" class="input-xlarge" name="confirm_email2" data-verifymails2-msg="E-mails don't match"/>
              </div>
            </div>
            
            <div class="control-group">
              <label for="" class="control-label">{$lang.coun_try}
				<span class="mand">*</span>
              </label>
              <div class="controls">
               <select id="" class="span3 postedData" data-field="usermain" name="user_location" required>
                <option>{$lang.sel_country}...</option>
              # $.each(results.countrycodes,function(k,a) { #
                <option value="#= a.code #"# if(a.code==results.user_location) { # selected="selected" # } # >#= a.country #</option>
                    # }); #
                 </select>
              </div>
            </div>

					<div class="control-group">
              <label for="" class="control-label">{$lang.ci_ty}
					<span class="mand">*</span>    
          </label>
              <div class="controls">
                <input type="text" placeholder="{$lang.type_city}" id="" value="#= results.details.profile.city #"  class="input-xlarge postedData" name="city" data-field="usersecondary" for="City" required/>
              </div>
            </div>
				
            
            <div class="control-group">
              <label for="" class="control-label">{$lang.ad_dress} 
				<span class="mand">*</span>     
         </label>
              <div class="controls">
                <input type="text" placeholder="{$lang.type_address}" da id="" value="#= results.details.profile.settings.address #" class="input-xlarge postedData" name="settings-address" data-field="usersecondary" for="Address" required/>
              </div>
            </div>
            
            <div class="control-group">
              <label for="" class="control-label">{$lang.state_zipcd} 
					<span class="mand">*</span>  
            </label>
              <div class="controls">
                <input type="text" placeholder="{$lang.type_state}" id="" value="#= results.details.profile.state #" class="input-medium spacer-right postedData" data-field="usersecondary" name="state" for="state" required/>&nbsp;
                <input type="text" placeholder="{$lang.type_zip}" id="" value="#= results.details.profile.zip_code #" class="input-small postedData" data-field="usersecondary" name="zip_code" for="Zip Code" required/>
              </div>
            </div>
            
            
            <div class="control-group">
              <label for="" class="control-label">{$lang.pho_ne} 
              <span class="mand">*</span>
              </label>
              <div class="controls">
               <select id="" name="" style="width:150px; margin-right:27px;">
                <option>{$lang.country_code}</option>
              # $.each(results.countryphonecodes,function(k,a) { #
                <option value="#= a.prefix #" # if(a.code==user_location) { # selected="selected" # } #>(+#= a.prefix #) #= a.country # </option>
                    # }); #
                 </select>
                  <input type="text" placeholder="{$lang.type_phone}" id="" value="#= results.details.profile.user_telephone #" class="input-small postedData" name="user_telephone" data-field="usersecondary" required/>
              </div>
            </div>
            
            <div class="control-group">
              <label for="" class="control-label">{$lang.mo_bile} 
              </label>
              <div class="controls">
               <select id="" name="" style="width:150px; margin-right:27px;">
                <option>{$lang.country_code}</option>
            	 # $.each(results.countryphonecodes,function(k,a) { #
                 	<option value="#= a.prefix #" # if(a.code==user_location) { # selected="selected" # } #>(+#= a.prefix #) #= a.country # </option>
                 # }); #
                 </select>
                  <input type="text" placeholder="{$lang.type_mobile}" id="" value="#= results.user_mobile #" class="input-small postedData" name="user_mobile" data-field="usermain"/>
              </div>
            </div>
            
             <div class="control-group">
              <label for="" class="control-label">{$lang.f_ax} 
              </label>
              <div class="controls">
               <select id="" name="" style="width:150px; margin-right:27px;">
                <option>{$lang.country_code}</option>
              # $.each(results.countryphonecodes,function(k,a) { #
                <option value="#= a.prefix #" # if(a.code==user_location) { # selected="selected" # } #>(+#= a.prefix #) #= a.country #</option>
                    # }); #
                 </select>
                  <input type="text" placeholder="{$lang.type_fax}" id="" class="input-small postedData" value="#= results.details.profile.settings.fax #"  name="settings-fax" data-field="usersecondary"/>
              </div>
            </div>
             <div class="control-group">
              <label for="" class="control-label">{$lang.fb_account}</label>
              <div class="controls">
                <input type="text" placeholder="{$lang.type_fb_acc}" id="" value="#= results.details.profile.facebook_uid #"  class="input-xlarge postedData" name="facebook_uid" data-field="usersecondary"/>
              </div>
            </div>
            
             <div class="control-group">
              <label for="" class="control-label">{$lang.twitter_account}</label>
              <div class="controls">
                <input type="text" placeholder="{$lang.type_twit_acc}" id="" value="#= results.details.profile.twitter_uid #"  class="input-xlarge postedData" name="twitter_uid" data-field="usersecondary"/>
              </div>
            </div>
            
            
            <div class="control-group">
              <div class="controls">
                <label for="" class="">{$lang.all_fields_mand}</label>
                <a href="\\#" data-bind="click: save"  class="btn btn-primary" >{$lang.save_changes}</a>
              </div>
            </div>
           <div class="seperator"></div>
          </form>
		</div>
          </div> <!-- end span9 -->
</div>

<style scoped>

span.k-tooltip {
    border-width: 0px;
    display: inline-block;
    padding: 2px 5px 1px 6px;
    margin-bottom:10px;
    position: static;
    color:red;
    /*background:none;*/
    background:url("/images/site/validation_advice_bg1.gif") no-repeat scroll 2px 5px transparent;
}

</style>
</script>