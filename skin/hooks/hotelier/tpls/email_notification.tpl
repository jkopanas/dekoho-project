<script id="email_notification" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/email_not.js" data-type="text">    
<div id="email_notification">
<div class="span9">
         <div class="navbar">
    <div class="navbar-inner">
    <span class="brand help"><i class="icon-user"></i></span>
    <ul class="nav">
    <li><a href="\\#profileManager/account">{$lang.account_information}</a></li>
    <li ><a href="\\#profileManager/billing_profiles">{$lang.billing_profiles}</a></li>
    <li class="active"><a href="\\#profileManager/email_notification">{$lang.email_notification}</a></li>
    <li><a href="\\#profileManager/change_password">{$lang.change_password}</a></li>
    </ul>
    </div>
    </div>
<div class="alert hidden" id="wrong_notifications">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Warning!</strong> Something went wrong email notifications were not updated.
</div>
				<form class="form-horizontal the-form" >
				<div class="seperator"></div><div class="seperator"></div><div class="seperator"></div>
			
			<div class="control-group">
             <label for="" class="control-label">{$lang.first_warn}</label>
              <div class="controls">
               <select class="span2" name="" id="">
                    <option>60 {$lang.days}</option>
					<option>50 {$lang.days}</option>
					<option>40 {$lang.days}</option>
					<option>30 {$lang.days}</option>
					<option>20 {$lang.days}</option>
					<option>10 {$lang.days}</option>							
                </select>
              </div>
            </div>

		<div class="control-group">
             <label for="" class="control-label">{$lang.second_warn}</label>
              <div class="controls">
               <select class="span2" name="" id="">
                    <option>60 {$lang.days}</option>
					<option>50 {$lang.days}</option>
					<option selected="selected">40 {$lang.days}</option>
					<option>30 {$lang.days}</option>
					<option>20 {$lang.days}</option>
					<option>10 {$lang.days}</option>							
                </select>
              </div>
            </div>

			<div class="control-group">
             <label for="" class="control-label">{$lang.third_warn}</label>
              <div class="controls">
               <select class="span2" name="" id="">
                    <option>60 {$lang.days}</option>
					<option>50 {$lang.days}</option>
					<option>40 {$lang.days}</option>
					<option selected="selected">30 {$lang.days}</option>
					<option>20 {$lang.days}</option>
					<option>10 {$lang.days}</option>							
                </select>
              </div>
            </div>
			<div class="control-group" style="padding-left:50px;">		
			<label class="checkbox">
          <input type="checkbox"  name="" value=""> I want to be informed by e-mail on the latest offers<br/> for Hoteliers and news od Dekoho.com
									 
        </label>
		</div>
				<div class="controls">
          	      <a class="btn btn-primary" data-bind="click: updateNotifications" href="\\#">{$lang.save_changes}</a>
              </div>
					<div class="seperator"></div><div class="seperator"></div><div class="seperator"></div>
		</form>

          </div> <!-- end span9 -->
</div>
</script>