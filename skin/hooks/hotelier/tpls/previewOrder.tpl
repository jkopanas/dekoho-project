<script id="previewOrder" type="text/kendo-ui-template"  data-script="/scripts/hotelier/tpls/previewOrder.js" data-type="text"> 
<div id="previewOrder">
<div class="span9">

<div id="hotelbar"></div>

# if (results.id !== undefined) { #
# t=app.renderTpl('ToolbarHotelManager'); #
	#= t(results)  #
# } #

<textarea name="PaymentData" id="PaymentData" class="hide">#= mcms.objectToString(results.PaymentData) #</textarea>
<div  class="seperator"></div>
<select id="billing" class="multiselect postedData spacer-left float-left" name="billing"  data-field="UserData" data-bind="value: SelectBilling,click: BillingOption" >
				<option value="0">Select Billing Profile</option>
              # for (i in results.billing) { #
				# var name = (results.billing[i].operating_company) ? results.billing[i].operating_company : results.billing[i].first_name+" "+results.billing[i].last_name; #
			   <option value="#= results.billing[i].id #" >#= name #</option>
			   # } #
</select>
<select id="payment" class="multiselect spacer-left float-left postedData" name="payment" data-bind="click: SelectOption"  data-field="UserData">
			  <option value="ShowPayment">Select Payment Method</option>
              <option value="PayPal">PAYPAL</option>
			  <option value="jcc">Jcc</option>
</select>
<div class="seperator"></div>
  <table  id="dashboard" class="table table-striped" >
              <thead>
				<tr>
                  <th>\\#</th>
                  <th class="">Product</th>
                  <th>Price</th>
				  <th>discount</th>                  
				  <th>Coupon discount</th>
				  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
				# var j=1; #
				# var pr = 0; #
				# for (i in results.items) { #
				
				<input type="hidden" name="start_date-#= results.items[i].id #" value="#= results.items[i].startdate #" class="postedData"  />
				<input type="hidden" name="end_date-#= results.items[i].id #" value="#= results.items[i].enddate #" class="postedData"  />
				<input type="hidden" name="discount-#= results.items[i].id #" value="#= results.items[i].discount #" class="postedData"  />
                <tr>
                  <td>#= j #</td>
				  <td class="span3">
							#= controller.returnCurrentObj().get("ShowItemTitle")(results.items[i].title) #
							<input type="hidden" name="id[]" value="#= results.items[i].id #" class="postedData"  />
				  </td>
 				  <td class="">#= app.calculateCurrency(results.price)+" "+app.symbol #</td>
				  <td id="discount-#= results.items[i].id #"></td>  
				  <td class="">
						<input type="text" class="postedData input-small spacer-right" id="#= results.items[i].id #" name="coupons-#= results.items[i].id #" data-field="coupon" data-bind="events: { change: changeDiscount }"  value="" />
						<i id="coupons-#= results.items[i].id #" class="" style="width: 23px; height:23px;"></i>
				  </td>
				  <td id="amount-#= results.items[i].id #" class="price span2" data-price="#= app.calculateCurrency(results.price) #">#= app.calculateCurrency(results.price)+" "+app.symbol #</td>
                </tr>
				# pr = pr +  parseInt(results.price); #
				# j++; #
                # } #
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td >TotalAmount</td>
					<td id="totalAmount"  class="span2">#= app.calculateCurrency(pr)+" "+app.symbol #</td>
				</tr>
              </tbody>
            </table>
			<hr>
					<a class="btn btn-primary btn-large spacer-right float-right" data-bind="invisible: BuyNow,click: finishPayment">Buy Now</a>
	<div id="ShowPay">
	</div>
<div class="seperator"></div>
</div>
<div class="seperator"></div>
</div>
</div>
</script>