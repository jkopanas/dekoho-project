<script id="photos" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/photos.js,/scripts/hotelier/tpls/bootstrap-confirm.js" data-type="text">  
<div id="photos" >
<div class="span9">
<input type="hidden" name="hotelid" id="hotelid" value="#= results.id #"  />
<input type="hidden" name="itemid" id="itemid" value="#= results.itemid #"  />
<input type="hidden" name="module" id="module" value="#= results.module #" class="postedData" data-field="PhotoModule" />
	# t=app.renderTpl('ToolbarHotelManager'); #
	#= t(results)  #
 <div class="btn-group float-right">
    <!--<li><a href="\\#" data-bind="click: videos">add #= results.module # videos</a></li>-->
	<a href="\\#" name="photoid[]" data-bind="click: activated" class="btn  tip   float-left" data-placement="top" rel="tooltip" data-original-title="">{$lang.hm_activate}</a>
	<a href="\\#" name="photoid[]" data-bind="click: deactivated" class="btn  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">{$lang.hm_deactivate}</a>
	<a href="\\#" name="photoid[]" data-bind="click: deleteSelected" class="btn  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">{$lang.hm_remove}</a>

	<a class="btn " href="\\#Popup" data-bind="click: displaypopup" data-route="popup/upload/#= results.itemid #/#= results.module #" data-toggle="modal" >Upload</a>
	{if $USER_CLASS == "A"}
			<a href="\\#" name="photoid[]" id="enableBtn" data-bind="click: enableDisableSelected" data-id="#= results.id  #" data-action="enablePhotos" class="btn btn-success  tip" data-placement="top" rel="tooltip" data-original-title="">Enable Photo</a>
			<a href="\\#" name="photoid[]" id="disableBtn" data-bind="click: enableDisableSelected" data-id="#= results.id  #" data-action="disablePhotos" class="btn btn-danger  tip" data-placement="top" rel="tooltip" data-original-title="">Disable Photo</a>
	{/if}

	# if (results.module == "rooms" ) { # 
		<a href="\\#hotelManager/rooms/room/#= results.itemid #" class="btn " > << back</a>
	# } else { #
		<a href="\\#hotelManager/hotel/#= results.id #" class="btn " > << back</a>
	# } # 
</div>

<div class="seperator"></div>
			    <ul id ="thumbs" class="thumbnails">
				# j=1; #
				# if(typeof results.image.images!='undefined') { #
				# if (results.image.images.length != 0 ) { #
				# for (i in results.image.images) { #
   					 <li class="span3 # if ( j % 3 != 0  ) { # spacer-photo # } # ">
    					<div href="\\#" id="photo-#= results.image.images[i].id #" data-id="#= results.image.images[i].id #"  class="thumbnail #= results.image.images[i].imageActive #" style="position: relative;" data-bind="events:{ mouseenter: listener, mouseleave: listener }">
    						<img src="#= results.image.images[i].image_full.big_thumb.image_url #" alt="" >
							<div class="caption hide" id="caption-#= results.image.images[i].id #" style="position: absolute; z-index: 1; top:0px; right: 0px;" >	
									<div class="checkboxFive">
  										<input type="checkbox" id="checkboxFiveInput-#= results.image.images[i].id #" data-id="#= results.image.images[i].id #" value="#= results.image.images[i].id #" data-bind="checked: selection.photo,click: SelectClicked" />
	  									<label for="checkboxFiveInput-#= results.image.images[i].id #"></label>
  									</div>
									<br/>
                  			</div>
    					</div>
    				</li>
					# j++;#
				# } #
				# } #
				# } #
    			</ul>

				<!--<span class="float-right bold">selected <span data-bind="text: countSelected" >0</span> of #= j #</span>-->
				<div class="seperator"></div>
			<hr>
			<div class="float-left">
			# if (results.module == "rooms" ) { #
				<span class="label">Optimal size: 450 * 295 </span>
			# } else { #
				<span class="label">Optimal size: 570 * 375 </span>
			# } #
			</div>
			<div class="float-right">
			<span class="label label-success">&nbsp;&nbsp;</span>&nbsp; Active Photos<br/>
			<span class="label label-warning">&nbsp;&nbsp;</span>&nbsp; Photos Pending for Approval<br/>
			<span class="label label-important">&nbsp;&nbsp;</span>&nbsp; Deactivate Photos<br/>
			</div>
          </div> <!-- end span9 -->
			<input type="hidden" name="ids" data-bind="value: selected" value="" data-field="selection" class="postedData"/>
</div>
</script>

<script id="uploadthumb" type="text/kendo-ui-template"  data-script="/scripts/hotelier/tpls/fileUploader.js"  data-force="true" data-type="text">  
<div id="uploadthumb" >
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>{$lang.upload_popup_photo}</h3>
  </div>
<div class="modal-body">
<input type="hidden" name="itemid" id="itemid" value="#= results.itemid #" />
<input type="hidden" name="module" id="module" value="#= results.module #" />
<textarea name="fileTypes" class="hidden">#= mcms.objectToString(results.images) #</textarea>
<input type="hidden" name="fileType" id="fileType" value="thumb" />
<input type="hidden" name="callBack" id="callBack" value="quick_thumb_uploaded" />
<input type="hidden" name="imageCategory" id="imageCategory" value="0" />
 <input type="hidden" name="parent" id="parent" value="" />
{$lang.allowed_image_formats}: #= results.images.images.extentions #<br/>
 # if (results.module == "hotel") { #
{$lang.optimal_size}: 570x375
# } else { #
{$lang.optimal_size}:450x295
# } #
<div class="seperator"></div>
    <div class="uploadContainer">
     
    </div>
    
    <ul id="uplaodedFiles"></ul>
</div><!-- END TAB UPLOAD-->
<div>
</div><!-- END TAB URL -->
<div>

</div><!-- END TAB CMS -->
</div><!-- END TABS -->
<div class="clear"></div>

<div class="modal-footer">
	<div class="uploadContainer">
              <input name="image" id="image" type="file" class="uploader"  data-fileType="image" />
    </div>
  </div>
</div>
<style>
.modal-footer {
	text-align: left;
}
</style>
</script>

<script id="upload" type="text/kendo-ui-template"  data-script="/scripts/hotelier/tpls/fileUploader.js"  data-force="true" data-type="text">  
<div id="upload" >
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>{$lang.upload_popup_photo}</h3>
  </div>
<div class="modal-body">

<textarea name="fileTypes" class="hidden">#= mcms.objectToString(results) #</textarea>
<input type="hidden" name="fileType" id="fileType" value="image" />
<input type="hidden" name="callBack" id="callBack" value="image_uploaded" />
<input type="hidden" name="imageCategory" id="imageCategory" value="#= results.images.id #" />
 <input type="hidden" name="parent" id="parent" value="#= results.images.title #" />
{$lang.allowed_image_formats}: JPG, PNG<br/>
#  if (results.images.module == "rooms") { #
{$lang.optimal_size}: 450x295
# } else { #
{$lang.optimal_size}: 570x375
# } #
<div class="seperator"></div>
    <ul id="uplaodedFiles"></ul>
</div><!-- END TAB UPLOAD-->
<div>
</div><!-- END TAB URL -->
<div>

</div><!-- END TAB CMS -->
</div><!-- END TABS -->
<div class="clear"></div>
</div>
<div class="modal-footer">
	<div class="uploadContainer">
            <input name="image" id="image" type="file" class="uploader" data-fileType="image"/>
    </div>
  </div>
  </div>
<style>
.modal-footer {
	text-align: left;
}
</style>
</script>

<script id="newimageTemplate" type="text/x-kendo-template">
<div id="newimageTemplate">
<table wdith="100%" class="t">
		<tr valign="top">
			<td style="vertical-align:auto">
			<p><img src=" #= response.uploadedFile.thumb.CleanDir #/#= response.uploadedFile.thumb.name #" /> </p>
			<p></p>
			</td>
			<td>
			<p><strong>File name:</strong> #= response.uploadedFile.name #</p>
			<p><strong>File size:</strong> #= response.uploadedFile.filesize #</p>
			<p><strong>File type:</strong> #= response.uploadedFile.mime #</p>
			</td></tr>

</table>
</div>
</script>

<script id="newImages" type="text/x-kendo-template">
 <li class="span3 #= results.class # ">
    					<div href="\\#"  id="photo-#= results.thumb.imageid #" class="thumbnail pending" data-id="#= results.thumb.imageid #" style="position: relative;" data-bind="events:{ mouseenter: listener, mouseleave: listener }">
    						<img src="#= results.thumb.CleanDir #/#= results.big_thumb.name #" alt="">
							<div class="caption hide" id="caption-#= results.thumb.imageid #" style="position: absolute; z-index: 1; top:0px; right: 0px;" >	
									<div class="checkboxFive">
  										<input type="checkbox" id="checkboxFiveInput-#= results.thumb.imageid #" data-id="#= results.thumb.imageid #" value="#= results.thumb.imageid #" data-bind="checked: selection.photo,click: SelectClicked" />
	  									<label for="checkboxFiveInput-#= results.thumb.imageid #"></label>
  									</div>
									<br/>
                  			</div>
    					</div>
    				</li>

</script>

