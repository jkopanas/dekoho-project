<script id="hotels" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/hotels.js,/scripts/hotelier/tpls/bootstrap-confirm.js"  data-type="text">  
<div id="hotels" >
<div class="span9">
			# if (results.length) { #			
<div class="btn-group">	
		<a href="\\#" name="hotelid[]" data-bind="click: renew" class="btn  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">{$lang.hm_renew}</a>
		<a href="\\#" name="hotelid[]" data-bind="click: activated" class="btn  tip   float-left" data-placement="top" rel="tooltip" data-original-title="">{$lang.hm_activate}</a>
		<a href="\\#" name="hotelid[]" data-bind="click: deactivated" class="btn  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">{$lang.hm_deactivate}</a>
		<a href="\\#" name="hotelid[]" data-bind="click: deleteSelected" class="btn  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">{$lang.hm_remove}</a>

		{if $USER_CLASS == "A"}
			<a href="\\#" name="hotelid[]" data-bind="click: enableDisableSelected" data-action="enableHotels" class="btn btn-success  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">Enable Hotel</a>
			<a href="\\#" name="hotelid[]" data-bind="click: enableDisableSelected" data-action="disableHotels" class="btn btn-danger  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">Disable Hotel</a>
		{/if}
</div>
<div class="seperator"></div>
			<div class="the-form">
			<table id="dashboard" class="table table-striped" >
              <thead>
                <tr>
                <th><input type="checkbox" data-bind="checked: allselect,click: selectAll" /></th>
                  <th>{$lang.my_hotels}</th>
                  <th>{$lang.hm_renew_bf}</th>
                  <th>{$lang.hm_renewal_md}</th>
                  <th>{$lang.hm_status}</th>
				  <th>{$lang.enable}</th>
                </tr>
              </thead>
              <tbody>

				# for (i in results) { #
                <tr>
                  <th><input type="checkbox" value="#= results[i].id #" data-bind="checked: selection.hotel,click: selectedfn" /></th>
                  <td> 
					# if (results[i].image_full != null) { #
 						<img src="#= results[i].image_full.thumb #" width="115" height="75" class="float-left spacer-right"/>
					# } else { #
						<img src="/images/site/dash-hotel.jpg" width="115" height="75" class="float-left spacer-right"/>
					# } #
					<a href="\\#hotelManager/hotel/#= results[i].id #">#= results[i].title #</a><br/>
					<span class="font-10">#= results[i].geocoderAddress #</span>
				  </td>
					# var date = new Date(results[i].date_added*1000); #
					# var datevalues = [ date.getFullYear(),date.getMonth()+1,date.getDate()]; #
                  <td >#= datevalues[1]+" / "+datevalues[2]+" / "+(datevalues[0]+1) #</td>
                  <td>
				  	<select class="span2">
						<option>{$lang.hm_autorenew}</option>
						<option>{$lang.hm_autoexpire}</option>
					</select>
				  </td>
                  <td><span class="label #if (parseInt(results[i].active)) { # btn-success # } else { # btn-danger # } #">#if (parseInt(results[i].active)) { # {$lang.hm_active} # } else { # {$lang.hm_inact} # } #</span></td>
                 
					<td id="status-#= results[i].id #"><span class="label #if (parseInt(results[i].enable)) { # btn-success # } else { # btn-danger # } #">#if (parseInt(results[i].enable)) { # {$lang.enable} # } else { # {$lang.disable} # } #</span></td>
   
				</tr>
				# } #
              </tbody>
            </table>
<br class="clear_0"/>
						<hr>
    <div class="pagination tac">
    <ul>
	# var p=pages-1; #
 	# if (p != 0) { #
    <li><a href="\\#hotelManager/hotels/#= p #" >{$lang.previous}</a></li>
	# } #
	# var j=0; #
	# for (i=1; i<=total; i += 10 ) { #
		# j++; #
		<li  class="# if ( j == pages ) { # active # } #" ><a href="\\#hotelManager/hotels/#= j #" >#= j #</a></li>
	# } #
 	# if (j > pages) { #
 	<li><a href="\\#hotelManager/hotels/#= p+2 #" >{$lang.next}</a></li>
	# } #
    </ul>
    </div>



			<input type="hidden" name="selected" data-bind="value: selected" value="" data-field="selection" class="postedData"/>
				</div>
          </div> <!-- end span9 -->
			# } else { #

				# var k = app.renderTpl("EmptyRender") #	 
				#= k({ message: "There are no Hotels Added" }) #
			# } #
			<style type="text/css">
			\\#dashboard .highlight { background-color: \\#ffffcc; } 
			</style>
</div>
</script>