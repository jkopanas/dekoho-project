<script id="support" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/support.js,/scripts/hotelier/tpls/bootstrap-confirm.js" data-type="text">
<div id="support" >
    <div class="span9">
     
	<div id="supportMessage" class="alert alert-success" data-bind="text: message,invisible: isInvisible">
 				Your hotel has been successfully updated.
	</div>
    <div class="navbar">
    <div class="navbar-inner">
    <span data-placement="top" rel="tooltip" data-original-title="support" class="brand tip help" href="\\#"><i class="icon-comment"></i></span>
    <ul class="nav">
    	<li class="active"><a href="\\#support">{$lang.tickets_overview}</a></li>
    	<li><a href="\\#supportNew/view">{$lang.open_newticket}</a></li>
    </ul>
    </div>
    </div>
		# if (results.length > 0 ) { # 
<div class="btn-group">	
	<a href="\\#" data-bind="click: deleteSelected" class="btn  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">REMOVE</a>
	{if $USER_CLASS == "A"}
	<a href="\\#"  data-bind="click: CloseTicketSelected" class="btn  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">CLOSE TICKET</a>
	<a href="\\#"  data-bind="click: OpenTicketSelected" class="btn  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">OPEN TICKET</a>
	{/if}
</div>
	<div class="the-form spacer-top">
		<table class="table table-striped">
              <thead>
                <tr>
                <th><input type="checkbox"  data-bind="checked: allselect,click: selectAll"  /></th>
                  <th>{$lang.da_te}</th>
                  <th>{$lang.sub_ject}</th>
				  <th>{$lang.description}</th>
                 <!-- <th>{$lang.ac_tion}</th> -->         
                </tr>
              </thead>
               <tbody>
				# var total=results.length; #
				# for (i=0; i<total; i++) { #
                <tr id="ticket-#= results[i].id #" class="# if ( results[i].senderid == results[i].currentuser || ( results[i].senderid == 1 && results[i].userclass == "A" ) && results[i].status == 1  ) { # unread # } #">
                  <td><input type="checkbox" value="#= results[i].id #" data-bind="checked: selection.ticket,click: selectedfn" /></td>
                  <td>#= results[i].date_add #</td>
                  <td><a href="\\#supportDetails/#= results[i].conversationid #/details" >#= results[i].title #</a></td>
				  <td>#= results[i].description.substring(0,20)+"..." #</td>
				  <td># if ( results[i].state == 1 ) { # <span class="label label-important">Closed</span> # } else { # <span class="label label-success">Open</span> # } #</td>
                  <!--<td><a href="\\#supportDetails/#= results[i].conversationid #/details" >{$lang.see_details}</a></td>-->
                </tr>
				# } #
              </tbody>
            </table>
			<input type="hidden" name="selected" data-bind="value: selected" value="" data-field="selection" class="postedData"/>
            <br class="clear_0"/>
						<hr>
    <div class="pagination tac">
    <ul>
	# var p=pages-1; #
 	# if (p != 0) { #
    <li><a href="\\#support/#= p #/view" >{$lang.previous}</a></li>
	# } #
	# var j=0; #
	# for (i=1; i<=totals; i += 10 ) { #
		# j++; #
		<li  class="# if ( j == pages ) { # active # } #" ><a href="\\#support/#= j #/view" >#= j #</a></li>
	# } #
 	# if (j > pages) { #
 	<li><a href="\\#support/#= p+2 #/view" >{$lang.next}</a></li>
	# } #
    </ul>
    </div>
</div>
 # } else { #

			# var k = app.renderTpl("EmptyRender"); #				
			#= k({ message: "There are no Tickets. Create <a href='\\#supportNew/view'>new</a>" }) #	
# } #
          </div> <!-- end span9 -->
			<style type="text/css">
			\\#support .highlight { background-color: \\#ffffcc; } 
			\\#support tr.unread td { background-color: \\#eeeeee; font-weight:bold; } 
			</style>
</div>
</script>

<script id="supportNew" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/support.js,/scripts/hotelier/tpls/bootstrap-confirm.js" data-type="text">  
<div id="supportNew">
      <div class="span9">
    <div class="the-form">
	<div class="btn-group spacer-left">
		 	<button class="btn dropdown-toggle" data-toggle="dropdown">
		 		Select Subject&nbsp;
		 		<span class="caret"></span>
		 	</button>
		 	<ul class="dropdown-menu">
				<li><a href="\\#" data-bind="{ click: ChangeSubject }">Hotel Subscription</a></li>
				<li><a href="\\#" data-bind="{ click: ChangeSubject }">Finance</a></li>
				<li><a href="\\#" data-bind="{ click: ChangeSubject }">Reservations</a></li>
				<li><a href="\\#" data-bind="{ click: ChangeSubject }">Special Offers</a></li>
				<li><a href="\\#" data-bind="{ click: ChangeSubject }">Advertisement</a></li>
				<li><a href="\\#" data-bind="{ click: ChangeSubject }">PayPal</a></li>
				<li><a href="\\#" data-bind="{ click: ChangeSubject }">Technical Support</a></li>
				<li><a href="\\#" data-bind="{ click: ChangeSubject }">Change Password</a></li>
				<li><a href="\\#" data-bind="{ click: ChangeSubject }">Other</a></li>
			</ul>
	</div>
		<input type="text" id="title" name="title" data-field="openTicket" class="postedData" value="{*$lang.click_to_add_title*}"  style="font-size:18px;" autocomplete="off" onblur="if ( this.value == '' ) { this.value='{$lang.click_to_add_title}'; this.style.opacity=0.3; } return false;" data-bind="click: focus" />
		 
	<ul id ="toolbar" class="nav nav-pills float-right spacer-top">
    	<li class="spacer-right" ><a href="\\#" data-bind="{ click: Send }" class="btn">{$lang.send}</a></li>
    	<li class="spacer-right"><a href="\\#" data-bind="{ click: Cancel }" class="btn">{$lang.lbl_cancel}</a></li> 
	</ul>
	<br />
	<hr/>
  	<textarea id="content" name="description" data-field="openTicket" class="postedData"></textarea>
	# if ( results.length > 0 ) { #
		# var l=app.templates['supportDetails']; #
		#= l({ 'results': results,'users': users, 'app': app, 'reply': reply })  #
		<input type="hidden" id="reply" name="reply" data-field="openTicket" class="postedData" value="#= results[0].conversationid #" />
	# } #
	</div>
</div>
</div>
<style>
hr { margin: 0px; }
\\#title {
-webkit-box-shadow:none;
	-moz-box-shadow: none;
	box-shadow: none;
opacity: 0.3;
border-color: \\#ffffff;
color: \\#333333;
border:0px;
padding:0px;
font-size: 25px;
font-weight: normal;
letter-spacing: 1px;
height: 30px;	
width:50%;
margin: 15px 0px 10px 20px;
}
\\#ViewHotelier ul.nav li {
	font-size: 16px;
}
\\#ViewHotelier ul.nav {
margin-bottom:10px;
}
\\#content {
    -webkit-box-shadow:none;
	-moz-box-shadow: none;
	box-shadow: none;
height: 400px;
font-size: 16px;
width: 98%;
border:0;
}
</style>

</script>

<script id="supportDetails-template-close"  type="text/kendo-ui-template" data-type="text">
<div data-i="#= i #" id="supportDetails-template-open" data-bind="events: { mouseover: OverOut, mouseout: OverOut, click: showDescription  }">
		<p class="padding deactivatemail btn-link" >
			# if ( users[results[i].user_id][0].admin == 1 ) { # support@dekoho.com # } else { # #= users[results[i].user_id][0].user_name+" "+users[results[i].user_id][0].user_surname # ( #= users[results[i].user_id][0].email # ) # } #<br/>
			#= results[i].description #
		</p>
</div>
</script>

<script id="supportDetails-template-open"  type="text/kendo-ui-template" data-type="text">
	<div data-i="#= i #" id="supportDetails-template-close" # if (i != 0) { # data-bind="events: { click: showDescription  }" # } #>	
			<p  class="float-left padding-left address spacer-top btn-link" >
			From: # if ( users[results[i].user_id][0].admin == 1 ) { # support@dekoho.com # } else { # #= users[results[i].user_id][0].user_name+" "+users[results[i].user_id][0].user_surname # # } # <br/>
			Date: #= results[i].date_add #<br/>
    		To: # if ( users[results[i].user_id][0].admin == 0 ) { # support@dekoho.com # } else { # #= users[results[i].user_id][0].email # # } #<br/>
			Subject: #= results[i].title #
			</p>
			<br class="clear" />
			<blockquote>
  				<p>#= results[i].description #</p>
			</blockquote>
		</div>
</script>


<script id="supportDetails" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/support.js,/scripts/hotelier/tpls/bootstrap-confirm.js" data-type="text"> 
<div id="supportDetails">
	# if (!reply) { # 
    <div class="span9">
    <div class="the-form">
	<span class="lead float-left spacer-top spacer-left">#= results[0].title #</span>
	<ul id ="toolbar" class="nav nav-pills float-right">
    	<li ><a href="\\#" data-id="#= results[0].conversationid #" data-bind="{ click: Reply }">Reply</a></li>
    	<li ><a href="\\#support">{$lang.back}</a></li>
	</ul>
	# } #
<br/>
<br/>
	<hr/>
# i=0; #
# for (i in results) { #
	
	# if (i == 0 )  { #
	    # t=app.renderTpl('supportDetails-template-open'); #
		#= t({ 'results': results,'users': users, 'i': i })  #
		<hr/>

	# } else { #
		# t=app.renderTpl('supportDetails-template-close'); #
		#= t({ 'results': results,'users': users,'i': i })  #
		<hr/>

	# } #
# } #

	# if (!reply) { # 
	</div>
	# } #

</div>
<style>
.lead {
	margin-top: 10px;	
	line-height: 20px;
	margin-bottom:0px;
}
\\#ViewHotelier ul.nav {
margin-bottom:0px;
}
\\#supportDetails hr {
 margin: 0;
}
\\#supportDetails .address {
line-height:15px;
color: \\#0088cc;
}

.deactivatemail {
 background-color: \\#F0F0F0;
}

</style>
</script>
