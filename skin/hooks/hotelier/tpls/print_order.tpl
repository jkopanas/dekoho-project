<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Παραγγελία {$item[0].transaction_id}</title>
</head>
<body>
<p>
{$debug}
</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="font-size: 30px;">
<tr>
	<td width="20%">Company Name</td>
	<td width="45%">{$profile.operating_company}</td>
	<td width="20%">VAT No</td>
	<td width="40%">{$profile.settings->tax_no}</td>
</tr>
<tr>
	<td width="20%">Address</td>
	<td width="45%">{$profile.settings->address}</td>
	<td width="20%">Tax Office</td>
	<td width="40%">{$profile.settings->tax_office}</td>
</tr>
<tr>
	<td width="20%">Postal Code - City</td>
	<td width="45%">{$profile.settings->zip_code} - {$profile.city}</td>
	<td width="20%">Commercial Reg. No</td>
	<td width="35%">{$profile.settings->commercial_re_no}</td>
</tr>
<tr>
	<td width="20%">Country</td>
	<td width="45%">{$profile.country}</td>
	<td width="20%">DEKOHO UserID No </td>
	<td width="40%">{$UNAME}</td>
</tr>
</table>
<hr/>
<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="font-size: 30px;">
<tr>
<td width="50%">Date {$smarty.now|date_format}</td>
<td style="text-align:right;">Invoice number: {$smarty.get.id}</td>
</tr>
</table>
<hr/>
<p></p>
<span style="text-align: center; font-size: 50px;">INVOICE</span>
<p></p>
<table width="100%" border="1" cellspacing="1" cellpadding="5">
  <tr>
    <th>Κωδικός</th>
    <th>Προϊόν</th>
    <th>Τιμή μονάδας με Φ.Π.Α</th>
    <th>Σύνολο</th>
  </tr>
  {foreach from=$item item=a key=k name=b}
  <tr>
    <td><strong>{$a.item.id}</strong></td>
    <td><strong>{$a.item.title}</strong></td>
    <td>{$current_currency.symbol} {$a.price|formatprice:".":","}</td>
    <td>{$current_currency.symbol} <span id="total-{$k}">{$a.price|formatprice:".":","}</span></td>
  </tr>
  {/foreach}
  <tr><td class="seperator"></td></tr>
  <tr><td colspan="5" align="right"> Σύνολο : <strong>{$current_currency.symbol} <span class="total-price-no-vat">{$total|formatprice:".":","}</span></strong></td></tr>
  <!-- <tr><td colspan="5" align="right">Αξία Φ.Π.Α : <strong>{$current_currency.symbol}<span class="vat-price">{$order.details.extra_charge_vat|formatprice:".":","}</span></strong></td></tr>-->
  <tr>
    <td class="seperator"></td></tr>
</table>
<p></p>
<p></p>
<pre style="float:right; font-size: 30px;">
Payment of this invoice shall be made within 15 days by
credit card or PayPal through your Online DEKOHO Account
or by money transfer to the following Bank Account:

Bank: HELLENIC BANK
Account Name: DEKOHO LIMITED
IBAN: CY02 0050 0140 0001 4001 6121 4001
SWIFT: HEBACY2N
Reference: GR1234567

All services according to our terms and conditions.
See our terms and conditions at http://www.dekoho.com/termsandconditions
If you have any questions please contact us at invoice@dekoho.com
</pre>

</body>
</html>