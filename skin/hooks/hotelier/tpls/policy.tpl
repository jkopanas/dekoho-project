<script id="policy" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/policy.js,/scripts/hotelier/tpls/bootstrap-confirm.js"  data-type="text">
<div id="policy">
 <div class="span9"> 
# t=app.renderTpl('ToolbarHotelManager'); #
	#= t(results) #
# results.app = {} #
<div id="thumbs" class="the-form">
<textarea name="policyData" id="policyData" class="hide">#= mcms.objectToString(results) #</textarea>
<h3 class="spacer-left">{$lang.standard_policy}</h3>
	<table class="table table-striped" >     					
		    # for (i in results.extrafield.policy ) { #
					# if (results.extrafield.policy[i].type == "text" ) { # 

                <tr id="policy-#= results.extrafield.policy[i].fieldid #"  >
                  <td class="span2">
					<span class="field">#= results.extrafield.policy[i].field #</span>
					
				  </td>
                  <td class="span2">
					<span data-description="#= results.extrafield.policy[i].settings.description #" data-type="#= results.extrafield.policy[i].type #" class="value"># if ( results.extrafield.policy[i].value == "" ) { #Empty# } else { # #=results.extrafield.policy[i].value # # } # </span>
				  </td>
				  <td class="span4">
						<a href="\\#" data-itemid="policy-#= results.id #" data-id="policy-#= results.extrafield.policy[i].fieldid #" data-trid="#= i #" data-time="true" data-bind="click: EditSelected" class="btn  tip  float-left edit" >Edit</a>
				  </td>
             	</tr>
					# } #
			# } #
      </table>
<hr/>
<h3 class="spacer-left float-left">{$lang.add_policy}</h3>
<div class="btn-group float-right spacer-right">

{if $USER_CLASS == "A"}
		<a href="\\#" name="policyid[]" data-bind="click: enableDisableSelected" data-id="#= results.id  #" data-action="activateExtras" class="btn  tip spacer-top" data-placement="top" rel="tooltip" data-original-title="">Active</a>
		<a href="\\#" name="policyid[]" data-bind="click: enableDisableSelected" data-id="#= results.id  #" data-action="deactivateExtras" class="btn  tip  spacer-top" data-placement="top" rel="tooltip" data-original-title="">Deactivate</a>
		<a href="\\#" name="policyid[]" id="enableBtn" data-bind="click: enableDisableSelected" data-id="#= results.id  #" data-action="enableExtras" class="btn btn-success  spacer-top " data-placement="top" rel="tooltip" data-original-title="">Enable Policy</a>
		<a href="\\#" name="policyid[]" id="disableBtn" data-bind="click: enableDisableSelected" data-id="#= results.id  #" data-action="disableExtras" class="btn btn-danger spacer-top " data-placement="top" rel="tooltip" data-original-title="">Disable Policy</a>
{/if}

<a href="\\#" data-bind="click: CreateNew" class="btn btn-primary spacer-top" >{$lang.add_new_policy}</a>
<a href="\\#translations/translatepolicy/#= results.id  #" class="btn  spacer-top " data-bind="invisible: ShowTranslate">Translations</a>
</div>
<div class="seperator"></div>
	<table id="policyFields" class="table table-striped" >
				<tbody>
				# for (i in results.extrafield.policy ) { #
						# if (results.extrafield.policy[i].type != "text" ) { #

                <tr id="policy-#= results.extrafield.policy[i].fieldid #"class=" #= results.extrafield.policy[i].extraEnable #">
				  {if $USER_CLASS == "A"}
					<td class="span2">
									<span class="checkboxFive float-left spacer-right">
  										<input type="checkbox" id="checkboxFiveInput-#= results.extrafield.policy[i].fieldid #" data-id="#= results.extrafield.policy[i].fieldid #" value="#= results.extrafield.policy[i].fieldid #"  data-bind="checked: selection.extra" />
	  									<label for="checkboxFiveInput-#= results.extrafield.policy[i].fieldid #"></label>
  									</span>
						<span class="var_name float-left">#= results.extrafield.policy[i].var_name #</span>
					</td>
				  {/if}
                  <td class="span2">
					<span class="field span2">#= results.extrafield.policy[i].field #</span></td>
                  <td class="span2"> 
					<span data-description="#= results.extrafield.policy[i].settings.description #" data-type="#= results.extrafield.policy[i].type #" class="value span2"># if ( results.extrafield.policy[i].value == "" ) { #Empty# } else { # #=results.extrafield.policy[i].value # # } # </span>
				 	<input type="hidden"  class="translations" data-tfield="value" data-tid="#= results.extrafield.policy[i].fieldid #" data-tkey="itemid" data-ttable="extra_fields" name="#= results.extrafield.policy[i].var_name #" value="#= results.extrafield.policy[i].value #" />
				  </td>
				  <td class="span4">
						<a href="\\#" data-itemid="policy-#= results.id #" data-id="policy-#= results.extrafield.policy[i].fieldid #" data-trid="#= i #" data-time="false" data-bind="click: EditSelected" class="btn  tip  float-left edit" >Edit</a>
						<a href="\\#" data-itemid="policy-#= results.id #" data-id="policy-#= results.extrafield.policy[i].fieldid #" data-bind="click: deleteSelected" class="btn  tip  float-right delete" >Clear</a>			
				  </td>
             </tr>
				# } #
			# } #
			</tbody>
      </table>
<input type="hidden" name="ids" data-bind="value: selected" value="" data-field="selection" class="postedData"/>
	</div>
</div>
</script>

<script id="EditPolicy" type="text/kendo-ui-template" data-type="text">
<tr id="EditPolicy">
				{if $USER_CLASS == "A"}
					<td class="span2">
						<input type="text" name="var_name" class='postedData span2' data-field='saveExtra' value="#= results.extrafield.policy[results.trid].var_name #" />						
					</td>
				{/if}
				<td class="span2">
				{if $USER_CLASS == "A"}
							<input type="text" name="field" class='postedData span2' data-field='saveExtra' value="#= results.extrafield.policy[results.trid].field #" />							
				{else}
							<span class="field">#= results.extrafield.policy[results.trid].field #</span>
				{/if}
				  </td>
                  <td class="span2"> 
					<span  data-type="#= results.extrafield.policy[results.trid].type #" class="value span2">
						<input type='hidden' name='id' value='#= results.extrafield.policy[results.trid].fieldid #' class='postedData' data-field='saveExtra' />
					    <input type="hidden"  class="translations" data-tfield="value" data-tid="#= results.extrafield.policy[results.trid].fieldid #" data-tkey="itemid" data-ttable="extra_fields" name="#= results.extrafield.policy[results.trid].var_name #" value="#= results.extrafield.policy[results.trid].value #" />
				  
						# if (results.time) { #
							<input  id='select-#= results.id #'   name='value'  value="#= results.itemid #" class='postedData' data-type="notresponse" data-field='saveExtra' />
						# } else { #
							<textarea name='value' id='select-#= results.id #' value="#= results.itemid #" class='postedData span2' data-field='saveExtra'>#= results.extrafield.policy[results.trid].value #</textarea>
						# } #
					</span>
				  </td>
				  <td class="span4">
						<a href="\\#" data-itemid="policy-#= results.id #" data-id="policy-#= results.extrafield.policy[results.trid].fieldid #" data-bind="click: update" class="btn  tip  float-left edit" >Update</a>
						<a href="\\#" data-trid="#= results.trid #" data-time="#= (results.time) ? true: false #" data-bind="click: cancel" class="btn  tip  float-right cancel" >Cancel</a>		
				  </td>
</tr>
</script>

<script id="LoadPolicy" type="text/kendo-ui-template" data-type="text">
	<tr id="policy-#= results.extrafield.policy[results.trid].fieldid #">
				{if $USER_CLASS == "A"}
					<td class="span2">
						<span class="checkboxFive float-left spacer-right">
  										<input type="checkbox" id="checkboxFiveInput-#= results.extrafield.policy[results.trid].fieldid #" data-id="#= results.extrafield.policy[results.trid].fieldid #" value="#= results.extrafield.policy[results.trid].fieldid #"  data-bind="checked: selection.extra"  />
	  									<label for="checkboxFiveInput-#= results.extrafield.policy[results.trid].fieldid #"></label>
  									</span>
						<span class="var_name float-left">#= results.extrafield.policy[results.trid].var_name #</span>
					</td>
				  {/if}
                  <td class="span2">
					<span class="field span2">#= results.extrafield.policy[results.trid].field #</span>
				  </td>
                  <td class="span2"> 
					<span data-type="#= results.extrafield.policy[results.trid].type #" class="value span2"># if ( results.extrafield.policy[results.trid].value == "" ) { #Empty# } else { # #= results.extrafield.policy[results.trid].value # # } #  </span>
				 	<input type="hidden"  class="translations" data-tfield="value" data-tid="#= results.extrafield.policy[results.trid].fieldid #" data-tkey="itemid" data-ttable="extra_fields" name="#= results.extrafield.policy[results.trid].var_name #" value="#= results.extrafield.policy[results.trid].value #" />  
				  </td>
				  <td class="span4">
						<a href="\\#" data-itemid="policy-#= results.id #" data-id="policy-#= results.extrafield.policy[results.trid].fieldid #" data-trid="#= results.trid #" data-time="#= (results.time) ? true : false #" data-bind="click: EditSelected" class="btn  tip  float-left edit" >Edit</a>
						# if (results.extrafield.policy[results.trid].type == "area" ) { #
							<a href="\\#" data-itemid="policy-#= results.id #" data-id="policy-#= results.extrafield.policy[results.trid].fieldid #" data-bind="click: deleteSelected" class="btn  tip  float-right delete" >Clear</a>
						# } #
				  </td>
    	</tr>
</script>


<script id="createNewPolicy" type="text/kendo-ui-template" data-type="text">
		<tr  id="policy-#= id #">
				{if $USER_CLASS == "A"}
					<td  class="span2">
						<input type="text" name="var_name" class='postedData span2' data-field='saveExtra' value=" Type a Variable" />						
					</td>
				 {/if}
                  <td class="span2"> 
					<span class="field"><input type='text' name='field' class='postedData span2' data-field='saveExtra' value='Type a title'  /></span>
				  </td>
                  <td class="span2"> 
					<span class="value"><input type='hidden' name='id' value='#= itemid #' class='postedData ' data-field='saveExtra' /><textarea name='value' class='postedData span2' data-field='saveExtra'>Type policy description</textarea></span>
				  </td>
                  <td class="span4">
						<input type='hidden' name='policyid' value='#= id #' class='postedData' data-field='saveExtra' />
						<a href="\\#" data-itemid="policy-#= itemid #" data-bind="click: insert" class="btn  tip  float-left" >Create New</a>
						<a href="\\#"  data-itemid="policy-#= itemid #" data-time="false" data-bind="click: cancelInsert" class="btn  tip  float-right" >Cancel</a>
				  </td>
             </tr>
</script>


<script id="translatepolicy" type="text/kendo-ui-template"  data-script="/scripts/hotelier/tpls/translatepolicy.js" >
<div id="translatepolicy">
<textarea name="translateMap" id="translateMap" class="hide">#= mcms.objectToString(results.defaultdata) #</textarea>
<textarea name="translateMapLang" id="translateMapLang" class="hide">#= mcms.objectToString(results.data) #</textarea>
<div class="span9">
<div class="the-form">
<div class="alert alert-success hidden" >
 				Information around the hotel successfully updated.
			</div>
<a href="\\#" data-bind="click: goBack" class="btn SaveTranslations float-left spacer-left spacer-top"><< Back</a>
<button class="btn SaveTranslations float-right spacer-right spacer-top" data-id="#= app.getArguments[1] #" data-bind="click: save">Save</button>
<div class="seperator"></div>
<form class="spacer-left padding-left">
<input type="hidden" name="itemid" id="itemid" value="#= app.getArguments[1] #" class="translations" />
<input type="hidden" name="module" id="moduleItem" value="hotel" class="translations" />
<input type="hidden" name="currentLang" id="currentLang" value="gr" class="translations lang" />
<div class="float-left span6">
<h4 data-bind="text: EditLang"></h4>

<h4><a href="\\#" data-id="extra_fields">Policy Fields</a></h4>
<div id="extra_fields" class="active Properties">
# var j=1; #
# for ( k in results.defaultdata.extra_fields) { # 
			<div class="control-group">
              <label class="control-label" for="">#= results.extraFields[k] #&nbsp;</label>
              <div class="controls">
				<textarea id="#= 'extra_fields-'+k #" name="#= 'extra_fields-'+k #" class="translations" data-bind="click: GetLanguage" data-id="#= 'extra_fields-'+k #"></textarea>				
              </div>
            </div>
# j++; #
# } #
</div>

</div>

<div class="float-left">
<div class="wrap spacer-bottom">
<h4>Available languages</h4>
<div class="padding">
<ul id="languageTabs" class="idTabs">
# j =0; #
# for (i in results.availableLang) { #
		# if (!j) {  #
			<li><strong><a href="\\#tab1" rel="tab1" data-code="#= i #" data-bind="click: ChangeLang">#= results.availableLang[i] #</a></strong></li>		
		# } else { #
			<li><a href="\\#tab1" rel="tab1" data-code="#= i #"  data-bind="click: ChangeLang">#= results.availableLang[i] #</a></li>
		# } #
	    # j++; #
# } #
</ul>
</div><!-- END PADDING -->
</div><!-- END BOX -->


<div class="wrap spacer-bottom">
<h4>Default translation</h4>
<div class="sepH_c cf">
<textarea id="defaultTranslationTextArea" rows="15"></textarea>
</div><!-- END PADDING -->
</div><!-- END BOX -->

</div>
</form>
<div class="seperator"></div>
</div>
</div>
</div>
</script>