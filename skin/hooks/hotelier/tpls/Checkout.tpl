<script id="Checkout" type="text/kendo-ui-template" data-script=""  data-type="text">
<div id="Checkout">
<div class="span9">
<div class="hero-unit tac">
# if (results.msg) { #
 #= results.msg #
# } else { #
 Your have one new hotel. Please wait to be approved.Until then go to
# }#
 <a href="\\#dashboard" class="btn btn-link"> Dashboard</a>
</div>
</div>
</div>
</script>

<script id="CheckoutpreRender" type="text/kendo-ui-template" data-script=""  data-type="text">
<div id="CheckoutpreRender">
<div class="span9">
<div class="hero-unit tac">
Please Wait the transaction to finish.
<div class="seperator"></div>
<img src="/images/site/ajax_loader.gif" width="100" />
</div>
</div>
</div>
</script>
