<script id="change_password" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/password.js" data-type="text">    
<div id="change_password">
<div class="span9">
         <div class="navbar">
    <div class="navbar-inner">
    <span class="brand help"><i class="icon-user"></i></span>
    <ul class="nav">
    <li><a href="\\#profileManager/account">{$lang.account_information}</a></li>
    <li ><a href="\\#profileManager/billing_profiles">{$lang.billing_profiles}</a></li>
    <li><a href="\\#profileManager/email_notification">{$lang.email_notification}</a></li>
    <li  class="active"><a href="\\#profileManager/change_password">{$lang.change_password}</a></li>
    </ul>
    </div>
    </div>
		<div class="alert hidden" id="wrong_pass">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Warning!</strong> {$lang.curr_pass_inc}.
</div>

<div class="alert hidden" id="wrong_pass_no">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Warning!</strong> Something went wrong password was not updated.
</div>

<div class="alert alert-success hidden" id="success-pass">
 				{$lang.pass_update_succ}.
			</div>

		<form class="form-horizontal the-form" >
				<div class="seperator"></div><div class="seperator"></div><div class="seperator"></div>
			
			<div class="control-group">
              <label for="" class="control-label">{$lang.current_password}</label>
              <div class="controls">
                <input type="password" placeholder="{$lang.your_currentpass}" id="pass" name="pass" class="input-xlarge postedData" data-verifypassword-msg="Current pass is necessery in order to update your password" />
              </div> 
            </div>

			<div class="control-group">
              <label for="" class="control-label">{$lang.new_password}</label>
              <div class="controls">
                <input type="password" placeholder="{$lang.your_currentpass}" id="newpass" name="newpass" class="input-xlarge postedData" />
              </div>
            </div>

			<div class="control-group">
              <label for="" class="control-label">{$lang.confirm_new}</label>
              <div class="controls">
                <input type="password" placeholder="{$lang.confirm_yours}" id="confirmnewpass" name="confirmnewpass" class="input-xlarge" data-verify2passwords-msg="{$lang.pass_no_match}" />
              </div>
            </div>
					
				<div class="controls">
          	      <a class="btn btn-primary" data-bind="click: updatePassword"  href="\\#" id="pass_update">{$lang.update_pass}</a>
              </div>
					<div class="seperator"></div><div class="seperator"></div><div class="seperator"></div>
		</form>

          </div> <!-- end span9 -->

   <style scoped>

span.k-tooltip {
    border-width: 0px;
    display: inline-block;
    padding: 2px 5px 1px 6px;
    margin-bottom:10px;
    position: static;
    color:red;
    /*background:none;*/
    background:url("/images/site/validation_advice_bg1.gif") no-repeat scroll 2px 5px transparent;
}

</style>
</div>
</script>

