<script id="dashboard" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/dashboard.js" data-type="text">  
#var hotel_count = 0; #
#var total_rooms = 0;#
# if(results) { #
# $.each(results,function(k,a) { #
 # hotel_count++; #
# if(a.settings!='null') { #
   #if (a.settings.totalRooms!='null') { #
      # total_rooms  +=  parseInt(a.settings.totalRooms); #
   # } #
 # } #	
#});#
# } #


    <div id="dashboard">  
 <div class="span9">
     <div class="hero-unit" id="home-bkg" >
     <div class="float-left"> <h2 class="float-left white">{$lang.welcome} #= ( userdetails.name == "" ) ? userdetails.username : userdetails.name #</h2><a href="/index.php?logout"class="float-left spacer-left spacer-top2">( {$lang.logout} )</a></div>
     
	  # if( hotel_count > 0 ) { # 
	  <div class="float-left"> <h2 class="float-left white">UserID #= userdetails.username #</h2><a href="\\#profileManager/change_password" class="float-left spacer-left spacer-top2">( {$lang.ch_p} )</a></div>
      <div class="float-left"> <h2 class="float-left white">{$lang.last_notification} - </h2><a href="\\#notifications" class="float-left spacer-left spacer-top2">( {$lang.see_all1} )</a></div>
      		<div class="seperator"></div><div class="seperator"></div>
      		<span class="label label-success" id="label-dash">{$lang.total_hotels} #= hotel_count #</span>
            <span class="label label-info tip" id="label-dash">{$lang.total_rooms} #= total_rooms #</span>
             <span class="label label-inverse" id="label-dash-last">{$lang.total_offers}</span>    
		# } else { #
			<div class="float-left"> <h2 class="float-left white"> Complete your profile</h2><a href="\\#profileManager/account" class="float-left spacer-left spacer-top2">( here )</a></div>
      		<div class="float-left"> <h2 class="float-left white"> Create your Hotel</h2><a href="\\#hotelManager/hotel" class="float-left spacer-left spacer-top2">( here )</a></div>	
		# } #
		<div class="seperator"></div>
      </div>
	
# if( hotel_count > 0 ) { #    
      <table class="table table-striped">
              <thead>
                <tr>
                  <th>{$lang.my_hotels}</th>
                  <th>{$lang.st_ars}</th>
                  <th>{$lang.total_room}</th>
                  <th>{$lang.offers_applied}</th>
                  <th>{$lang.offers_ended}</th>
                </tr>
              </thead>
              <tbody>

			# $.each(results,function(k,a) { #
				<tr>
                  <td>

					# if (a.image_full != null) { #
 						<img src="#= a.image_full.thumb #" width="115" height="75" class="float-left spacer-right"/>
					# } else { #
						<img src="/images/site/dash-hotel.jpg" width="115" height="75" class="float-left spacer-right"/>
					# } #
					<a href="\\#hotelManager/hotel/#= a.id #">#=a.title#</a><br/><span class="font-10">#= a.geocoderAddress #</span>
				</td>
                  <td id="paddlr0">
					# for (var i=0;i<a.stars;i++) { #
				    	<i class="icon-star-empty float-left" style="width:14px; height:14px;"></i>
					# } # 
				</td>
                  <td># if(a.settings.totalRooms!='null') { # #= a.settings.totalRooms # # } else { # 0 # } # {$lang.r_ooms}</td>
                  <td>0 {$lang.of_fers}</td>
                  <td>0 {$lang.of_fers}</td>
                </tr>
				# }); #            
              </tbody>
            </table>
<br class="clear_0"/>
						<hr>
    <div class="pagination tac">
    <ul>
	# var p=pages-1; #
 	# if (p != 0) { #
    <li><a href="\\#dashboard/#= p #" >{$lang.previous}</a></li>
	# } #
	# var j=0; #
	# for (i=1; i<=total; i += 10 ) { #
		# j++; #
		<li  class="# if ( j == pages ) { # active # } #" ><a href="\\#dashboard/#= j #" >#= j #</a></li>
	# } #
 	# if (j > pages) { #
 	<li><a href="\\#dashboard/#= p+2 #" >{$lang.next}</a></li>
	# } #
    </ul>
    </div>
# } #    
</div>
          
          </div> <!-- end span9 -->
  </div>
</script>