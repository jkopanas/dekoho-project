<script id="OfferHotelSelect" type="text/kendo-ui-template" data-type="text">  
<div id="OfferHotelSelect">

# if (results.length > 0 ) { #

    <table class="table table-striped table-addoffer ">
              <thead>
                <tr>
                  <th>{$lang.select} </th>
                  <th>{$lang.my_hotels}</th>
                  <th>{$lang.st_ars}</th>
                  <th>{$lang.total_room}</th>           	
                </tr>
              </thead>
              <tbody>
# for (i in results) { #

                <tr>
                  <th><input type="radio" name="HotelSelect" value="#= results[i].id #" data-bind="click: CheckedHotel" #= results[i].checked #  /></th>
                  <td> 
					# if (results[i].image_full != null) { #
 						<img src="#= results[i].image_full.thumb #" width="115" height="75" class="float-left spacer-right"/>
					# } else { #
						<img src="/images/site/dash-hotel.jpg" width="115" height="75" class="float-left spacer-right"/>
					# } #
					#= results[i].title #<br/>
					<span class="font-10">#= results[i].geocoderAddress #</span>
				  </td>
                  <td> 
					# for (j=0; j<results[i].stars; j++) { #
						<i class="icon-star-empty float-left" style="width:13px; height:25px;"></i>
					# } #
				</td>
                  <td>
						#= results[i].roomCount # {$lang.room}<br/><br/>
					<a  id="#= results[i].id #" class="btn btn-primary hide offerAdd" href="\\#"  data-bind="click: selectHotel">{$lang.add}<br/>{$lang.of_fers}</a>
				  </td>
				 </tr>
				# } #
              </tbody>
            </table>
# } else { #

					# var k = app.renderTpl("EmptyRender"); #				
						#= k({ message: "There are 0 Hotels Available" }) #	

# } #
<div class="seperator"></div>
</div>
</script>