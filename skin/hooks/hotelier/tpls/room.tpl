<script id="room" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/bootstrap-multiselect.js,/scripts/hotelier/tpls/room.js,/scripts/hotelier/tpls/bootstrap-confirm.js" data-type="text">    
<div id="room">
<div  id="thumbs" class="span9"> 
	# t=app.renderTpl('ToolbarHotelManager'); #
	#= t(results) #
    <div data-bind="invisible: newRoom">
    	<a href="\\#hotelManager/rooms/room/#= results.room.id  #/photos/rooms" class="btn" >{$lang.add_more_ho_photos}</a>
		<a href="\\#translations/translateroom/#= results.room.id  #" class="btn" >{$lang.add_translations}</a>
		<a href="\\#hotelManager/rooms/hotel/#= results.id #" class="btn float-right" ><< {$lang.back}</a>
   		<!-- <li><a href="\\#">add room videos</a></li>-->
    </div>

			<div class="seperator"></div>
 			<div class="alert alert-success hidden" id="success-room">
 				{$lang.room_success_update}
			</div>
			<div class="alert alert-success hidden" id="success-room-create">
 				{$lang.room_success_create}
			</div>
			
			<div class="the-form">
			<form class="form-horizontal">
			<div class="float-left">
			  <div class="seperator"></div><div class="seperator"></div><div class="seperator"></div>
			  <div class="control-group">
              <label class="control-label" for="">{$lang.roomname}&nbsp;<span class="mand">*</span></label>
              <div class="controls">
                <input type="text" class="input-xlarge" value="#= results.room.title #" for="name" class="postedData" name="title" data-field="roommain"  data-bind="events: { change: addClassPosteddata }" placeholder="type the name of your room" required>
            	{if $USER_CLASS == "A"}
					# e=app.renderTpl('EnableDisableFields'); #
					#= e({ "results": results.room,"id": results.room.id,"table": "rooms", "field": "title" })  #
				{/if}  
			</div>
            </div>
			<div class="control-group">
       			<label class="control-label" for="types">{$lang.room_type}</label>
       			<div class="controls">
         			<select id="room_type" class="hidden  postedData" name="room_type" data-field="roomsecondary" style="">
            			# var j=1; #
						# for (var i=0; i<=7;i++) { #
						<option value="#= j #" # if ( results.room.room_type  == j ) { # selected # } # >#= j # people</option>
						# j++; #
						# } #
         			</select>
     			</div>
    		</div>
             <div class="control-group">
              <label class="control-label" for="">{$lang.room_price}&nbsp;<span class="mand">*</span></label>
              <div class="controls">
				<div class="input-prepend input-append float-left ">
	                <input type="text" class="input-small postedData float-left" value="#= results.room.price #"  name="price" data-field="roomsecondary" placeholder="type the price of room" required>
					<span class="add-on"> #= app.symbol # </span> 
				</div>
              </div>
            </div>
            
             <div class="control-group">
              <label class="control-label" for="">{$lang.room_quantity}&nbsp;<span class="mand">*</span></label>
              <div class="controls">
                <input type="text" class="input-small postedData" value="#= results.room.availability #" class="" name="availability" data-field="roomsecondary" placeholder="" required>
              </div>
            </div>       
            
             <div class="control-group">
              <label class="control-label" for="">{$lang.room_description}&nbsp;<span class="mand">*</span></label>
              <div class="controls">
                <textarea style="width:275px; height:160px;"  class="translations"  data-bind="events: { change: addClassPosteddata }" data-tfield="description" data-tid="#= results.room.id #" data-tkey="id" data-ttable="rooms"  name="description" data-field="roommain" for="description" required >#= results.room.description #</textarea>
				{if $USER_CLASS == "A"}
					# e=app.renderTpl('EnableDisableFields'); #
					#= e({ "results": results.room,"id": results.room.id,"table": "rooms", "field": "description" })  #
				{/if}  
              </div>
            </div>
			</div>
			<div class="float-right spacer-right">
			<div class="seperator"></div>
			<div class="seperator"></div>
			<div class="seperator"></div>
				<div class="" data-bind="invisible: newRoom">
								# if (results.room.image_full !== null && results.room.hasOwnProperty('image_full') ) { #	
									<img id="FileThumb" src="#= results.room.image_full.thumb #" width="160"  />
								# } else { #
									<img id="FileThumb" src="" class="img-polaroid" width="160" height="160" />
								# } #
								<div class="seperator"></div>
           			  		   <a href="\\#Popup" data-bind="click: displaypopup" data-route="popup/uploadthumb/#= results.room.id #/rooms" data-toggle="modal"  class="btn">{$lang.upload_main_hotel_photo}</a>
									<div class="seperator-small"></div>	
									{$lang.optimal_size}: 450*295
			    </div>
			</div>
			<div class="float-left">
             <div class="control-group">
              <label class="control-label" for="">{$lang.room_facilities}</label>
              <div class="controls">

				# var t = app.renderTpl("selectProperties"); #
				#= t({ id: "roomsfacilities", table: "roomsfacilities", module: "rooms", data: results.room.roomsfacilities })  #

				<a href="\\#" data-bind="click: visible" data-id="roomsfacilities">&nbsp; {$lang.other} ... </a>

				{if $USER_CLASS == "A"}
 				<div class="btn-group spacer-left">
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="roomsfacilities" data-action="enableFilters" class="btn btn-success  tip" data-placement="top" rel="tooltip" data-original-title="">Enable</a>
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="roomsfacilities" data-action="disableFilters" class="btn btn-danger  tip" data-placement="top" rel="tooltip" data-original-title="">Disable</a>
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="roomsfacilities" data-action="deleteFilters" class="btn" data-placement="top" rel="tooltip" data-original-title="">Delete</a>
				</div>
				{/if}
				<br/><br/>
				<div data-bind="invisible: roomsfacilities">
					<input type="text" name="addnew" id="addnew" class="roomsfacilities" value="" placeholder="Your choice" />&nbsp;&nbsp;
					<a href="\\#" data-bind="click: addoption" data-id="roomsfacilities"><i class="icon-plus" style="width:25px;height:25px;"></i></a>
				</div>
              </div>
            </div>
				
				<div class="controls">
				# if ( results.room.id == "" ) { #
                	<button type="button" class="btn btn-primary" data-id="#= results.id #" data-bind="click: create">{$lang.save_cont}</button>
				# } else { #
					<button type="button" class="btn btn-primary" data-id="#= results.room.id #" data-bind="click: save">{$lang.save_cont}</button>
				# } #
              </div>
				
			</div>	
<input type="hidden" name="hotelid" value="#= results.id #" data-field="hotelKey" class="postedData"/>			
<input type="hidden" name="ids" data-bind="value: selected" value="" data-field="selection" class="postedData"/>
            </form>
			<div class="seperator"></div>
			</div>
			
          </div> <!-- end span9 -->

</div>

<style scoped>

span.k-tooltip {
    border-width: 0px;
    display: inline-block;
    padding: 2px 5px 1px 6px;
    margin-bottom:10px;
    position: static;
    color:red;
    /*background:none;*/
    background:url("/images/site/validation_advice_bg1.gif") no-repeat scroll 2px 5px transparent;
}

</style>

</script>

<script id="translateroom" type="text/kendo-ui-template"  data-script="/scripts/hotelier/tpls/translateroom.js" >
<div id="translateroom">
<textarea name="translateMap" id="translateMap" class="hide">#= mcms.objectToString(results.defaultdata) #</textarea>
<textarea name="translateMapLang" id="translateMapLang" class="hide">#= mcms.objectToString(results.data) #</textarea>
<div class="span9">
<div class="the-form">
<div class="seperator"></div>
<div class="alert alert-success hidden" >
 				Information around the hotel successfully updated.
			</div>
<a href="\\#hotelManager/rooms/room/#= app.getArguments[1] #" class="btn SaveTranslations float-left spacer-left spacer-top"><< Back</a>
<button class="btn SaveTranslations float-right spacer-right spacer-top" data-id="#= app.getArguments[1] #" data-bind="click: save">Save</button>
<div class="seperator"></div>
<form class="spacer-left padding-left">
<input type="hidden" name="currentLang" id="currentLang" value="gr" class="translations lang" />
<div class="float-left span6">
<h4 data-bind="text: EditLang"></h4>
<h4><a href="\\#" data-id="rooms">Rooms</a></h4>
<div id="rooms" class="active">
<div class="control-group">
              <label class="control-label" for="">Rooms description&nbsp;</label>
              <div class="controls">
			  <textarea style="width:275px; height:160px;" class="translations" id="rooms-description-#= app.getArguments[1] #" data-id="rooms-description-#= app.getArguments[1] #" name="rooms-#= Object.keys(results.defaultdata.rooms)[0] #" for="rooms description" data-bind="click: GetLanguage"></textarea>
              </div>
            </div>
</div>

{if USER_CLASS == "A"}
# for (i in results.defaultdata) { #

# if ( i != "rooms" ) { #
<h4><a href="\\#" data-bind="click: roomsProperties" data-id="rooms-#= i #">Rooms #= i #</a></h4>
<div id="rooms-#= i #" class="active Properties" data-bind="invisible: translateTab">
# var j=1; #
# for ( k in results.defaultdata[i] ) { # 
<div class="control-group">
              <label class="control-label" for="">Rooms #= i # #= j #&nbsp;</label>
              <div class="controls">
				<input type="text" id="#= i+'-'+k #" name="#= i+'-'+k #" value="" class="translations" data-bind="click: GetLanguage" data-id="#= i+'-'+k #" />				
              </div>
            </div>
# j++; #
# } #
</div>
# } #
# } # 
{/if}


</div>
<div class="float-left">
<div class="wrap spacer-bottom">
<h4>{$lang.avaivable_languages}</h4>
<div class="padding">
<ul id="languageTabs" class="idTabs">
# j =0; #
# for (i in results.availableLang) { #
		# if (!j) {  #
			<li><strong><a href="\\#tab1" rel="tab1" data-code="#= i #" data-bind="click: ChangeLang">#= results.availableLang[i] #</a></strong></li>		
		# } else { #
			<li><a href="\\#tab1" rel="tab1" data-code="#= i #"  data-bind="click: ChangeLang">#= results.availableLang[i] #</a></li>
		# } #
	    # j++; #
# } #
</ul>
</div><!-- END PADDING -->
</div><!-- END BOX -->


<div class="wrap spacer-bottom">
<h4>{$lang.defaultTransaltion}</h4>
<div class="sepH_c cf">
<textarea id="defaultTranslationTextArea" rows="15"></textarea>
</div><!-- END PADDING -->
</div><!-- END BOX -->

</div>
</form>
<div class="seperator"></div>
</div>
</div>
</div>
</script>
