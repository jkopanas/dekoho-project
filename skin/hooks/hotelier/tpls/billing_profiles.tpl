<script id="billing_profiles" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/billing.js,/scripts/hotelier/tpls/bootstrap-confirm.js" data-type="text">
<div id="billing_profiles" >
<div class="span9">
         <div class="navbar">
    <div class="navbar-inner">
    <span class="brand help"><i class="icon-user"></i></span>
   <ul class="nav">
    <li><a href="\\#profileManager/account">{$lang.account_information}</a></li>
    <li   class="active"><a href="\\#profileManager/billing_profiles">{$lang.billing_profiles}</a></li>
    <li><a href="\\#profileManager/email_notification">{$lang.email_notification}</a></li>
    <li><a href="\\#profileManager/change_password">{$lang.change_password}</a></li>
    </ul>
    </div>
    </div>

		<div class="alert alert-success hidden" id="success-billing">
 				Your billing profile has been successfully deleted.
			</div>

		 <div class="the-form">
		    <div class="the-form">
					<table class="table table-striped" id="billing_profiles">
              <thead>
                <tr>
                  <th>{$lang.billing_profiles}</th>
                  <th>{$lang.actions}</th>
                </tr>
              </thead>
              <tbody>
                # $.each(results,function(k,a) { #
				<tr id="billingpr#=a.id #">
                  <td> #= a.first_name # &nbsp; #= a.last_name # # if(a.operating_company!="") { # /  #= a.operating_company # # } #<br/>Vat No #= a.settings.tax_no #</td>
                  <td><a href="\\#Popup" data-bind="click: displaypopup" data-route="popup/new_billing/#= a.id #" class="spacer-right" data-toggle="modal" id= #= a.id #>{$lang.ed_pr}</a>
				  <a data-id= #= a.id # data-bind="click: deleteprofile" class="del-profile" href="\\#">{$lang.de_pr}</a></td>
                </tr>
				# }); #
					
					</tbody>
			</table>
					</div>
					<div class="controls tac spacer-top">
          	      <a  href="\\#Popup" class="btn btn-primary" data-bind="click: displaypopup" data-route="popup/new_billing" data-toggle="modal">{$lang.add_new_billing_prof}</a>
              </div>
					<div class="seperator"></div>
					
			</div>

          </div> <!-- end span9 -->
</div>
</script>

<script id="update-billing"  type="text/kendo-ui-template" data-type="text">
				<tr id="billingpr#=a.id #">
                  <td> #= a.first_name # &nbsp; #= a.last_name # # if(a.operating_company!="") { # /  #= a.operating_company # # } #<br/>Vat No #= a.settings.tax_no #</td>
                  <td><a href="\\#Popup" data-bind="click: displaypopup" data-route="popup/new_billing/#= a.id #" class="spacer-right" data-toggle="modal" id= #= a.id #>{$lang.ed_pr}</a>
				  <a data-id= #= a.id # data-bind="click: deleteprofile" class="del-profile" href="\\#">{$lang.de_pr}</a></td>
                </tr>
</script>


<script id="new_billing" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/billing.js" data-type="text">
<div id="new_billing" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="form-horizontal the-form">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"># if(results.first_name!='') { # {$lang.edit_billing_prof} # } else { # {$lang.add_new_billing_prof} # } #</h3>
		</div>
<div class="modal-body">
 		<div class="control-group">
              <label for="" class="control-label">{$lang.op_company}</label>
              <div class="controls">
                <input type="text" id="" value="#= results.operating_company #" for="{$lang.op_company}" placeholder="operating company" data-field="billing" name="operating_company" class="input-xlarge postedData">
              </div>
            </div>

		<div class="control-group">
              <label for="" class="control-label">Legal form</label>
              <div class="controls">
                 <select class="span3 postedData" name="legal_form" data-field="billing">
                     <option value="0" # if (results.legal_form==0) { # selected="selected" # } # >Legal Company</option>
                     <option value="1" # if (results.legal_form==1 ) { # selected="selected" # } # >Public Limited Company</option>
				     <option value="2" # if (results.legal_form==2) { # selected="selected" # } # >Limited Partnership</option>
					 <option value="3" # if (results.legal_form==3) { # selected="selected" # } # >General Partnership</option>
					 <option value="4" # if (results.legal_form==4) { # selected="selected" # } # >Sole Trader</option>
					 <option value="5" # if (results.legal_form==5) { # selected="selected" # } # >Company constituded under civil law</option>
					 <option value="6" # if (results.legal_form==6) { # selected="selected" # } # >Other</option>
               </select>
              </div>
            </div>
				 <div class="control-group">
             		 <label for="" class="control-label">Company Activity <span class="mand">*</span></label>
             	 <div class="controls">
              	  <input type="text" placeholder="your company activity" for="Company Activity" id="" name="activity" data-field="billing" value="#= results.activity #" class="input-xlarge postedData" required/>
                </div>
            </div>

				<div class="control-group">
             		 <label for="" class="control-label">First Name <span class="mand">*</span></label>
             	 <div class="controls">
              	  <input type="text" for="First Name" placeholder="your first name"  id="" name="first_name" data-field="billing" value="#= results.first_name #" class="input-xlarge postedData" required/>
                </div>
            </div>

				<div class="control-group">
             		 <label for="" class="control-label">Last Name <span class="mand">*</span></label>
             	 <div class="controls">
              	  <input type="text" for="Last Name" placeholder="your last name" id="" name="last_name" data-field="billing" value="#= results.last_name #" class="input-xlarge postedData" required/>
                </div>
            </div>

				<div class="control-group">
             		 <label for="" class="control-label">Address <span class="mand">*</span></label>
             	 <div class="controls">
              	  <input type="text" for="Address" placeholder="your address" id="" name="settings-address" data-field="billing" value="#= results.settings.address #" class="input-xlarge postedData" required/>
                </div>
            </div>

			<div class="control-group">
              <label for="" class="control-label">Country  </label>
              <div class="controls">
               <select id="" class="span3 postedData" data-field="billing" name="country">
                <option>{$lang.sel_country}...</option>
              # $.each(results.countrycodes,function(k,a) { #
                <option value="#= a.code #"# if(a.code==results.country) { # selected="selected" # } # >#= a.country #</option>
                    # }); #
                 </select>
              </div>
            </div>

				<div class="control-group">
             		 <label for="" class="control-label">City <span class="mand">*</span></label>
             	 <div class="controls">
              	  <input type="text" placeholder="your city" for="City" id="" name="city" data-field="billing" value="#= results.city #" class="input-xlarge postedData" required />
                </div>
            </div>
				 <div class="control-group">
              <label for="" class="control-label">State/Zip code <span class="mand">*</span></label>
              <div class="controls">
                <input type="text" for="State" placeholder="type your state" id="" value="#= results.settings.state #" class="input-medium spacer-right postedData" data-field="billing" name="settings-state" required />&nbsp;
                <input type="text" for="Zip" placeholder="Your zip code" id="" value="#= results.settings.zip_code #" class="input-small postedData" data-field="billing" name="settings-zip_code" required />
              </div>
            </div>
			 <div class="control-group">
              <label for="" class="control-label">Phone <span class="mand">*</span></label>
              <div class="controls">
               
                  <input type="text" for="Phone" placeholder="type your phone" id="" value="#= results.telephone #" class="input-xlarge postedData" name="telephone" data-field="billing" required />
              </div>
            </div>
			<div class="control-group">
              <label for="" class="control-label">Fax <span class="mand">*</span></label>
              <div class="controls">
                  <input type="text" for="Fax" placeholder="type your fax" id="" value="#= results.settings.fax #" class="input-xlarge postedData" name="settings-fax" data-field="billing" required />
              </div>
            </div>

				<div class="control-group">
             		 <label for="" class="control-label">Tax Number <span class="mand">*</span></label>
             	 <div class="controls">
              	  <input type="text" for="Tax Number" placeholder="your tax number" id="" name="settings-tax_no" data-field="billing" value="#= results.settings.tax_no #" class="input-xlarge postedData" required/>
                </div>
            </div>
			
			<div class="control-group">
             		 <label for="" class="control-label">Tax Office <span class="mand">*</span></label>
             	 <div class="controls">
              	  <input type="text" for="Tax Office" placeholder="your tax office" id="" name="settings-tax_office" data-field="billing" value="#= results.settings.tax_office #" class="input-xlarge postedData" required/>
                </div>
            </div>
				
				<div class="control-group">
             		 <label for="" class="control-label">Commercial register No. <span class="mand">*</span></label>
             	 <div class="controls">
              	  <input type="text" for="Commercial register No." placeholder="your commercial register number" id="" name="settings-commercial_re_no" data-field="billing" value="#= results.settings.commercial_re_no #" class="input-xlarge postedData" required/>
                </div>
            </div>
				
				<div class="control-group">
             		 <label for="" class="control-label">Receive invoices by e-mail</label>
             	 <div class="controls">
					<select class="span1 postedData" data-field="billing" name="settings-email_notification">
					<option value="0" # if(results.settings.email_notification==0) { # selected="selected" # } # >No</option>
					<option value="1" # if(results.settings.email_notification==1) { # selected="selected" # } # >Yes</option>
					</select>
                </div>
            </div>

				<div class="control-group">
             		 <label for="" class="control-label">Billing email address <span class="mand">*</span></label>
             	 <div class="controls">
              	  <input type="text" for="Billing email address" placeholder="your billing email address" id="" name="invoice_email" data-field="billing" value="#= results.invoice_email #" class="input-xlarge postedData" required/>
                </div>
            </div>
	</div>
          <div class="modal-footer">
		      <div class="control-group">
              <div class="controls">
				# if (results.id === undefined ) { #
                		<a href="\\#" data-bind="click: save"  class="btn btn-primary" >Save</a>
				# } else { #
                		<a href="\\#" data-bind="click: update" data-id="#= results.id #"  class="btn btn-primary" >Save</a>
				# } #
				<button aria-hidden="true" data-dismiss="modal" id="cl" class="btn">Close</button>
              </div>
            </div>

	</div>

</div>
	
</div>
</script>