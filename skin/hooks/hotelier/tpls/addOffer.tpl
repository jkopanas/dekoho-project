<script id="addOffer" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/addOffer.js" data-type="text">  
<div id="addOffer">
<div class="span9">
      
			 <div class="navbar">
    <div class="navbar-inner">
    <span data-placement="top" rel="tooltip" data-original-title="add an offer" class="brand tip help"><i class="icon-plus-sign" style="width:20px; height:18px;"></i></span>
    <ul class="nav">
    <li id="OfferHotelSelect-bar" class="active OfferBar" data-bind="click: VisibleHotel"><a href="\\#">{$lang.sel_hotel}</a></li>
    <li id="OfferRoomSelect-bar" class="OfferBar" data-bind="invisible: ShowRoom,click: VisibleRoom"><a href="\\#">{$lang.sel_rooms}</a></li>
    <li id="previewOrder-bar" class="OfferBar" data-bind="invisible: ShowPreview,click: VisiblePayment"><a href="\\#">{$lang.payment}</a></li>
    </ul>
    </div>
    </div>
	  <div id="messages" class="alert alert-error hidden" >
	  
      </div>
      <div id="OffersContent" >
	</div>
    </div> <!-- end span9 -->
	<input type="hidden" name="price" value="#= results.settings.price #" />
	<input type="hidden" name="order_type_id" value="#= results.id #" />
</div>
</script>