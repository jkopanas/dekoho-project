<script id="hotel" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/bootstrap-multiselect.js,/scripts/hotelier/tpls/hotel.js,/scripts/hotelier/tpls/bootstrap-confirm.js" data-force="true">
<div id="hotel">
<div id="thumbs" class="span9">
	# t=app.renderTpl('ToolbarHotelManager'); #
	#= t(results)  #
 <div data-bind="invisible: newHotel" >
		<div class="btn-group">
    		<a href="\\#hotelManager/hotel/#= results.id  #/photos/hotel" class="btn float-left" >{$lang.add_more_ho_photos}</a>
			<a href="\\#translations/translatehotel/#= results.id  #" class="btn float-left" >{$lang.add_translations}</a>
			{if $USER_CLASS == "A"}
					<a href="\\#" name="hotelid[]" id="enableBtn" data-bind="click: enableDisableSelected" data-id="#= results.id  #" data-action="enableHotels" class="btn btn-success  tip  float-left # if ( parseInt(results.enable))  { # hide # } #" data-placement="top" rel="tooltip" data-original-title="">Enable Hotel</a>
					<a href="\\#" name="hotelid[]" id="disableBtn" data-bind="click: enableDisableSelected" data-id="#= results.id  #" data-action="disableHotels" class="btn btn-danger  tip  float-left # if ( !parseInt(results.enable))  { # hide # } #" data-placement="top" rel="tooltip" data-original-title="">Disable Hotel</a>
			{/if}
    		<!--<li><a href="\\#" data-bind="click: videos">add hotel videos</a></li>-->
		</div>
    </div> 
	<br />
			<div class="alert alert-success hidden" id="success-hotel">
 				{$lang.alert_up_hotel_msg}
			</div>
			
			<div id="msg"></div>
			
		<div class="the-form" >
					<form class="form-horizontal" >
				<div class="float-left">
				<div class="seperator"></div><div class="seperator"></div><div class="seperator"></div>
				 <div class="control-group">
              <label class="control-label" for="">{$lang.name_of_the_hotel}&nbsp;<span class="mand">*</span></label>
              <div class="controls">
                <input type="text" name="title" class="input-xlarge" value="#= results.title  #" data-field="hotelmain" data-bind="events: { change: addClassPosteddata }" placeholder="{$lang.fi_hotel_name}" for="hotel name" required>
				{if $USER_CLASS == "A"}
					# e=app.renderTpl('EnableDisableFields'); #
					#= e({ "results": results,"id": results.id,"table": "hotel", "field": "title" })  #
				{/if}
              </div>
            </div>

			<div class="control-group">
              <label class="control-label" for="">{$lang.hotel_url_name}&nbsp;<span class="mand">*</span></label>
              <div class="controls">
                <input type="text" name="permalink" class="input-xlarge" value="#= results.permalink  #" data-field="hotelmain" data-bind="events: { change: addClassPosteddata }" placeholder="{$lang.fi_hotel_url_name}" for="hotel alias" >
            	{if $USER_CLASS == "A"}
					# e=app.renderTpl('EnableDisableFields'); #
					#= e({ "id": results.id,"table": "hotel", "field": "permalink" })  #
				{/if}
				<br/><span class="inline">e.x. your-hotel-name</span>   
			</div>
            </div>

             <div class="control-group">
              <label class="control-label" for="">{$lang.hotel_chain_alliance}</label>
              <div class="controls">
                <input type="text" name="settings_chain" class="input-xlarge postedData" data-field="hotelmain" #if (typeof results.settings.chain!='undefined') { # value="#= results.settings.chain #" # } else # value="" } id="" placeholder="{$lang.fi_hotel_chain_alliance}">
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label" for="">{$lang.st_ars}</label>
              <div class="controls">
               <select class="span2 postedData" name="stars" data-field="hotelmain">
				# for (i=1;i<=5;i++) { #
               <option value="#= i #" # if ( i == parseInt(results.stars) ) { # selected # } #>#= i # {$lang.st_ars}</option>
				# } #
               </select>
              </div>
            </div>
            
             <div class="control-group">
              <label class="control-label" for="">{$lang.hotel_description}&nbsp;<span class="mand">*</span></label>
              <div class="controls">
                <textarea style="width:275px; height:160px;" class="translations"  data-bind="events: { change: addClassPosteddata }" data-tfield="description" data-tid="#= results.id #" data-tkey="id" data-ttable="hotel" name="description" data-field="hotelmain" for="hotel description" required >#= results.description #</textarea>
				{if $USER_CLASS == "A"}
					# e=app.renderTpl('EnableDisableFields'); #
					#= e({ "id": results.id,"table": "hotel", "field": "description" })  #
				{/if}
              </div>
            </div>
            
            
             <div class="control-group">
              <label class="control-label" for="">{$lang.total_hotel_rooms}&nbsp;<span class="mand">*</span></label>
              <div class="controls">
                <input type="text" class="input-small postedData" name="settings_totalRooms" data-field="hotelmain" value="#= results.settings.totalRooms #" id="" placeholder="e.g. 25" required for="Total rooms">
              </div>
            </div>
			</div>
						<div class="float-right spacer-right">
					<div class="seperator"></div>
					<div class="seperator"></div>
					<div class="seperator"></div>
					<div class="float-right spacer-right" data-bind="invisible: newHotel">
								# if (results.image_full !== null  && results.hasOwnProperty('image_full') ) { # 
									<img id="FileThumb" src="#= results.image_full.thumb #" width="160"  />
								# } else { #
									<img id="FileThumb" src="" class="img-polaroid" width="160" height="160"  />
								# } #
								<div class="seperator"></div>
           			  		   <a href="\\#Popup" data-bind="click: displaypopup" data-route="popup/uploadthumb/#= results.id #/hotel" data-toggle="modal"  class="btn">{$lang.upload_main_hotel_photo}</a>
								<div class="seperator-small"></div>	
									{$lang.optimal_size}: 570x375
			       </div>
			</div>
			<div class="float-left">
       		 <div class="control-group ">
              <label class="control-label" for="">{$lang.hotel_type}</label>
              <div class="controls">
				<a class="tip" style="text-decoration:none;"  data-original-title="{$lang.tooltip_hotel_type}" rel="tooltip" data-placement="top" >  

				# t=app.renderTpl('selectProperties'); #
				#= t({ id: "hoteltypes",table: "types", module: "hotel", data: results.types })  #
				</a>&nbsp; 
				<a href="\\#" data-bind="click: visible" data-id="hoteltypes">{$lang.other} ... </a>
				{if $USER_CLASS == "A"}
 				<div class="btn-group spacer-left btn-types filterAdmin hide">
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="types" data-action="enableFilters" class="btn btn-success  tip" data-placement="top" rel="tooltip" data-original-title="">Enable </a>
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="types" data-action="disableFilters" class="btn btn-danger  tip" data-placement="top" rel="tooltip" data-original-title="">Disable </a>
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="types" data-action="deleteFilters" class="btn" data-placement="top" rel="tooltip" data-original-title="">Delete Hotel</a>
				</div>
				{/if}
				<br/><br/>
				<div data-bind="invisible: hoteltypes">
					<input type="text" name="addnew" class="hoteltypes" value="" placeholder="Your choice" />&nbsp;&nbsp;
					<a href="\\#" data-bind="click: addoption" data-id="hoteltypes"><i class="icon-plus" style="width:25px;height:25px;"></i></a><br/>{$lang.to_be_approved}
				</div>
              </div>
	
            </div>

					<div class="control-group">
              <label class="control-label" for="">{$lang.hotel_facilities}</label>
              <div class="controls">
             <a class="tip" style="text-decoration:none;"  data-original-title="{$lang.tooltip_hotel_facilities}" rel="tooltip" data-placement="top" >  
				#= t({ id: "facilities",table: "facilities", module: "hotel", data: results.facilities })  #
				</a>&nbsp; 
				<a href="\\#" data-bind="click: visible" data-id="facilities">{$lang.other} ... </a>
				{if $USER_CLASS == "A"}
 				<div class="btn-group spacer-left btn-facilities filterAdmin hide">
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="facilities" data-action="enableFilters" class="btn btn-success  tip" data-placement="top" rel="tooltip" data-original-title="">Enable </a>
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="facilities" data-action="disableFilters" class="btn btn-danger  tip" data-placement="top" rel="tooltip" data-original-title="">Disable </a>
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="facilities" data-action="deleteFilters" class="btn" data-placement="top" rel="tooltip" data-original-title="">Delete </a>
				</div>
				{/if}
				<br/><br/>
				<div data-bind="invisible: facilities">
					<input type="text" name="addnew"  class="facilities" value="" placeholder="{$lang.fi_other}" />&nbsp;&nbsp;
					<a href="\\#" data-bind="click: addoption" data-id="facilities"><i class="icon-plus" style="width:25px;height:25px;"></i></a><br/>{$lang.to_be_approved}
				</div>
              </div>
            </div>

            
            <div class="control-group">
              <label class="control-label"  for="">{$lang.services_at_the_hotel}</label>
              <div class="controls">
             <a class="tip" style="text-decoration:none;"  data-original-title="{$lang.tooltip_services_at_the_hotel}" rel="tooltip" data-placement="top" >  
				#= t({ id: "services",table: "services", module: "hotel", data: results.services })  #
				 </a>&nbsp; 
				<a href="\\#" data-bind="click: visible" data-id="services">{$lang.other} ... </a>
				{if $USER_CLASS == "A"}
 				<div class="btn-group spacer-left btn-services filterAdmin hide">
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="services" data-action="enableFilters" class="btn btn-success  tip" data-placement="top" rel="tooltip" data-original-title="">Enable </a>
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="services" data-action="disableFilters" class="btn btn-danger  tip" data-placement="top" rel="tooltip" data-original-title="">Disable </a>
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="services" data-action="deleteFilters" class="btn" data-placement="top" rel="tooltip" data-original-title="">Delete </a>
				</div>
				{/if}
				<br/><br/>
				<div data-bind="invisible: services">
					<input type="text" name="addnew" class="services" value="" placeholder="{$lang.fi_other}" />&nbsp;&nbsp;
					<a href="\\#" data-bind="click: addoption" data-id="services"><i class="icon-plus" style="width:25px;height:25px;"></i></a><br/>{$lang.to_be_approved}
				</div>
              </div>
            </div>
            
             <div class="control-group">
              <label class="control-label" for="">{$lang.hotel_theme}</label>
              <div class="controls">
                <a class="tip" style="text-decoration:none;"  data-original-title="{$lang.tooltip_hotel_theme}" rel="tooltip" data-placement="top" >  
				#= t({ id: "themeshotels",table: "themeshotels", module: "hotel", data: results.themeshotels })  #
				</a>&nbsp; 
				<a href="\\#" data-bind="click: visible" data-id="themeshotels">{$lang.other} ... </a>
				{if $USER_CLASS == "A"}
 				<div class="btn-group spacer-left btn-themeshotels filterAdmin hide">
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="themeshotels" data-action="enableFilters" class="btn btn-success  tip" data-placement="top" rel="tooltip" data-original-title="">Enable </a>
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="themeshotels" data-action="disableFilters" class="btn btn-danger  tip" data-placement="top" rel="tooltip" data-original-title="">Disable </a>
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="themeshotels" data-action="deleteFilters" class="btn" data-placement="top" rel="tooltip" data-original-title="">Delete </a>
				</div>
				{/if}
				<br/><br/>
				<div data-bind="invisible: themeshotels">
					<input type="text" name="addnew" class="themeshotels" value="" placeholder="{$lang.fi_other}" />&nbsp;&nbsp;
					<a href="\\#" data-bind="click: addoption" data-id="themeshotels"><i class="icon-plus" style="width:25px;height:25px;"></i></a><br/>{$lang.to_be_approved}
				</div>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="">{$lang.hotel_room_facilities}</label>
              <div class="controls">
               <a class="tip" style="text-decoration:none;"  data-original-title="{$lang.fi_hotel_room_facilities}" rel="tooltip" data-placement="top" >   
				#= t({ id: "roomfacilities",table: "roomsfacilities", module: "hotel", data: results.roomsfacilities })  #
				</a>&nbsp; 
				<a href="\\#" data-bind="click: visible" data-id="roomfacilities">{$lang.other} ... </a>
				{if $USER_CLASS == "A"}
 				<div class="btn-group spacer-left btn-roomsfacilities filterAdmin hide">
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="roomfacilities" data-action="enableFilters" class="btn btn-success  tip" data-placement="top" rel="tooltip" data-original-title="">Enable </a>
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="roomfacilities" data-action="disableFilters" class="btn btn-danger  tip" data-placement="top" rel="tooltip" data-original-title="">Disable </a>
					<a href="\\#" name="filterid[]" data-bind="click: enableDisableSelected" data-id="roomfacilities" data-action="deleteFilters" class="btn" data-placement="top" rel="tooltip" data-original-title="">Delete </a>
				</div>
				{/if}
				<br/><br/>
				<div data-bind="invisible: roomfacilities">
					<input type="text" name="addnew" class="roomfacilities" value="" placeholder="{$lang.fi_other}" />&nbsp;&nbsp;
					<a href="\\#" data-bind="click: addoption" data-id="roomfacilities"><i class="icon-plus" style="width:25px;height:25px;"></i></a><br/>{$lang.to_be_approved}
				</div>
              </div>
            </div>
				
				<div class="control-group">
              <label class="control-label" for="">{$lang.pets_allowed}</label>
              <div class="controls">
               <select class="span2 postedData" name="pets" data-field="hotelmain">
               <option value="1" # if (results.pets == 1 ) { # selected="selected" # } #>Yes</option>
               <option value="0" # if (results.pets == 0) { # selected="selected" # } #>No</option>
               </select>
              </div>
            </div>


					<hr>
				
				 <label class="control-label" style="width:250px; margin-left:-55px;" for="paypal email">&nbsp;&nbsp;&nbsp;<h4>{$lang.paypal_email_title}</h4></label><br/><br/><span style="margin-left:-140px;">{$lang.to_receive_payments}</span> <br class="clear_0"/>				
				<div class="control-group">
              <label class="control-label" for="">{$lang.paypal_email}&nbsp;<span class="mand">*</span></label>
              <div class="controls width284">
                <input type="text" class="input-xlarge postedData" name="settings_paypalmail" data-field="hotelmain" value="#= results.settings.paypalmail #" id="" placeholder="paypal e-mail"  for="Paypal Email">
              </div>
            </div>
				

			<hr>
				
				 <label class="control-label" style="width:250px; margin-left:-45px;" for=""><h4 >{$lang.hotel_contact_details}</h4></label> <br class="clear_0"/>
					
					<div class="control-group">
              <label class="control-label" for="">{$lang.contact_person} &nbsp;<span class="mand">*</span></label>
              <div class="controls width284">
                <input type="text" class="input-xlarge postedData" name="settings_hotelcontactperson" data-field="hotelmain" value="#= results.settings.hotelcontactperson #" id="" placeholder="hotel's contact person" required for="hotel's contact person">
              </div>
				</div>
					
	<div class="control-group">
              <label class="control-label" for="">{$lang.position} &nbsp;<span class="mand">*</span></label>
              <div class="controls width284">
                <input type="text" class="input-xlarge postedData" name="settings_position" data-field="hotelmain" value="#= results.settings.position #" id="" placeholder="hotel's contact person" required for="position">
              </div>
				</div>

				<div class="control-group">
              <label class="control-label" for="">{$lang.hotel_tel}&nbsp;<span class="mand">*</span></label>
              <div class="controls width284">
                <input type="text" class="input-xlarge postedData" name="settings_hoteltelephone" data-field="hotelmain" value="#= results.settings.hoteltelephone #" id="" placeholder="hotel's telephone" required for="hotel telephone">
              </div>
            </div>

				<div class="control-group">
              <label class="control-label" for="">{$lang.hotel_tel2}&nbsp;<span class="mand"></span></label>
              <div class="controls width284">
                <input type="text" class="input-xlarge postedData" name="settings_hoteltelephone2" data-field="hotelmain" value="#= results.settings.hoteltelephone2 #" id="" placeholder="hotel's telephone 2" for="hotel telephone 2">
              </div>
            </div>
					
					<div class="control-group">
              <label class="control-label" for="">{$lang.hotel_mobile}&nbsp;</label>
              <div class="controls width284">
                <input type="text" class="input-xlarge postedData" name="settings_hotelmobile" data-field="hotelmain" value="#= results.settings.hotelmobile #" id="" placeholder="hotel's mobile">
              </div>
            </div>
			

				<div class="control-group">
              <label class="control-label" for="">{$lang.hotel_fax}&nbsp;<span class="mand">*</span></label>
              <div class="controls width284">
                <input type="text" class="input-xlarge postedData" name="settings_hotelfax" data-field="hotelmain" value="#= results.settings.hotelfax #" id="" placeholder="hotel's fax" required for="Hotel's Fax">
              </div>
            </div>
		
				<div class="control-group">
              <label class="control-label" for="">{$lang.hotel_email}&nbsp;<span class="mand">*</span></label>
              <div class="controls width284">
                <input type="email" class="input-xlarge postedData" name="settings_hotelmail" data-field="hotelmain" value="#= results.settings.hotelmail #" id="" placeholder="hotel's e-mail" required for="Hotel's Email">
              </div>
            </div>

				<hr>
				
				 <h4 >&nbsp;&nbsp;&nbsp;{$lang.bank_details}</h4> <br class="clear_0"/>
					
			<div class="control-group">
              <label class="control-label" for="">{$lang.account_owner}</label>
              <div class="controls width284">
                <input type="text" class="input-xlarge postedData" name="settings_hotelaccountowner" data-field="hotelmain" value="#= results.settings.hotelaccountowner #" id="" placeholder="account owner">
              </div>
		  </div>

			<div class="control-group">
              		<label class="control-label" for="">{$lang.bank_name}</label>
              		<div class="controls width284">
                	<input type="text" class="input-xlarge postedData" name="settings_bankname" data-field="hotelmain" value="#= results.settings.bankname #" id="" placeholder="bank name">
              </div>
		  </div>

				<div class="control-group">
              		<label class="control-label" for="">{$lang.iban}</label>
              		<div class="controls width284">
                	<input type="text" class="input-xlarge postedData" name="settings_iban" data-field="hotelmain" value="#= results.settings.iban #" id="" placeholder="iban/account number">
              </div>
		  </div>			

				<div class="control-group">
              		<label class="control-label" for="">{$lang.bic}</label>
              		<div class="controls width284">
                	<input type="text" class="input-xlarge postedData" name="settings_bic" data-field="hotelmain" value="#= results.settings.bic #" id="" placeholder="BIC-SC/NCS">
              </div>
		  </div>

				<div class="control-group">
              		<label class="control-label" for="">{$lang.bank_address}</label>
              		<div class="controls width284">
                	<input type="text" class="input-xlarge postedData" name="settings_bankaddress" data-field="hotelmain" value="#= results.settings.bankaddress #" id="" placeholder="bank address">
              </div>
		  </div>			
				
				<div class="controls">
				# if ( app.getArguments[1] == undefined ) { #
                	<button type="button" class="btn btn-primary" data-bind="click: add">{$lang.save_cont}</button>
				# } else { #
 					<button type="button" class="btn btn-primary" data-id="#= results.id #" data-bind="click: save">{$lang.save_cont}</button>
				# } #
              </div>
				
			</div>				
            </form>


			<div class="seperator"></div>
			</div> 
			<input type="hidden" name="ids" data-bind="value: selected" value="" data-field="selection" class="postedData"/>
          </div> <!-- end span9 -->
<div>

<style scoped>

span.k-tooltip {
    border-width: 0px;
    padding: 2px 5px 1px 0px;
    color:red;
	position: relative;
	text-align: left;
    display: block;
    background:none;
  /*  background:url("/images/site/validation_advice_bg.gif") no-repeat scroll 2px 5px transparent;*/
}

</style>

</script>



<script id="translatehotel" type="text/kendo-ui-template"  data-script="/scripts/hotelier/tpls/translatehotel.js" data-posted="hotel">
<div id="translatehotel">
<textarea name="translateMap" id="translateMap" class="hide">#= mcms.objectToString(results.defaultdata) #</textarea>
<textarea name="translateMapLang" id="translateMapLang" class="hide">#= mcms.objectToString(results.data) #</textarea>
<div class="span9">
<div class="the-form">
<div class="alert alert-success hidden">
 				{$lang.alert_up_hotel_msg}
			</div>
<a href="\\#hotelManager/hotel/#= app.getArguments[1] #" class="btn SaveTranslations float-left spacer-left spacer-top"><< {$lang.back}</a>
<button class="btn SaveTranslations float-right spacer-right spacer-top" data-id="#= app.getArguments[1] #" data-bind="click: save">Save</button>
<div class="seperator"></div>
<form class="spacer-left padding-left">
<input type="hidden" name="currentLang" id="currentLang" value="gr" class="translations lang" />
<div class="float-left span6">
<h4 data-bind="text: EditLang"></h4>
<h4><a href="\\#" data-id="hotel">Hotel</a></h4>
<div id="hotel" class="active">
<div class="control-group">
              <label class="control-label" for="">Hotel description&nbsp;</label>
              <div class="controls">
			  <textarea style="width:275px; height:160px;" class="translations" id="hotel-description-#= app.getArguments[1] #" data-id="hotel-description-#= app.getArguments[1] #" name="hotel-#= Object.keys(results.defaultdata.hotel)[0] #" for="hotel description" data-bind="click: GetLanguage"></textarea>
              </div>
            </div>
</div>


{if $USER_CLASS == "A" }

# for (i in results.defaultdata) { #

# if ( i != "hotel" ) { #

<h4><a href="\\#" data-bind="click: HotelProperties" data-id="hotel-#= i #">Hotel #= i #</a></h4>
<div id="hotel-#= i #" class="active Properties" data-bind="invisible: translateTab">
# var j=1; #
# for ( k in results.defaultdata[i] ) { # 
<div class="control-group">
              <label class="control-label" for="">Hotel #= i # #= j #&nbsp;</label>
              <div class="controls">
				<input type="text" id="#= i+'-'+k #" name="#= i+'-'+k #" value="" class="translations" data-bind="click: GetLanguage" data-id="#= i+'-'+k #" />				
              </div>
            </div>
# j++; #
# } #
</div>
# } #
# } # 
{/if}



</div>
<div class="float-left">
<div class="wrap spacer-bottom">
<h4>{$lang.avaivable_languages}</h4>
<div class="padding">
<ul id="languageTabs" class="idTabs">
# j =0; #
# for (i in results.availableLang) { #
		# if (!j) {  #
			<li><strong><a href="\\#tab1" rel="tab1" data-code="#= i #" data-bind="click: ChangeLang">#= results.availableLang[i] #</a></strong></li>		
		# } else { #
			<li><a href="\\#tab1" rel="tab1" data-code="#= i #"  data-bind="click: ChangeLang">#= results.availableLang[i] #</a></li>
		# } #
	    # j++; #
# } #
</ul>
</div><!-- END PADDING -->
</div><!-- END BOX -->


<div class="wrap spacer-bottom affix">
<h4>{$lang.defaultTransaltion}</h4>
<div class="sepH_c cf">
<textarea id="defaultTranslationTextArea" rows="15"></textarea>
</div><!-- END PADDING -->
</div><!-- END BOX -->

</div>
</form>
<div class="seperator"></div>
</div>
</div>
</div>
</script>