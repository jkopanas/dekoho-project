<script id="offers" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/offers.js,/scripts/hotelier/tpls/bootstrap-confirm.js" data-type="text">  
<div id="offers"> 
     <div class="span9">
    <div class="the-form">
			# var total=results.hotel.length; #
			# if (total > 0) { # 
					<table class="table table-striped" >
              <thead>
                <tr>
                  <th>{$lang.na_me}</th>
                  <th>{$lang.date_added}</th>
                  <th>{$lang.total_offers}</th>
                  <th>{$lang.active_of} </th>
				  <th>{$lang.finished_of} </th>
                  <th>{$lang.actions}</th>
                </tr>
              </thead>
              <tbody>
			# total--; #
				# for (i=0; i<=total; i++) { #
						<tr id="row-#= results.hotel[i].id #">
							<td>#= results.hotel[i].title #</td>
							<td>#= results.hotel[i].date_added #</td>
							<td>#= results.hotel[i].totaloffer # {$lang.of_fers}</td>
							<td>#= results.hotel[i].active # {$lang.of_fers}</td>
							<td>#= results.hotel[i].passed # {$lang.of_fers}</td>
							<td>
								# if ( results.hotel[i].totaloffer > 0  ) { # 
									<a href="\\#" data-id="#= results.hotel[i].id #" data-bind="click: editable"><i class=" icon-edit" style="width:25px; height:25px;"></i></a> 
									<a href="\\#" data-id="#= results.hotel[i].id #" data-bind="click: deleteOffer"><i class=" icon-remove" style="width:25px; height:25px;"></i></a>
								# } else { #
									<a href="\\#" data-id="#= results.hotel[i].id #" data-bind="click: editable" class="tip" data-original-title="add an offer" rel="tooltip" data-placement="right"><i class=" icon-plus-sign" style="width:25px; height:25px;"></i></a>
								# } #
							</td>
						</tr>	
				# } #
              </tbody>
            </table>
			# } else { #

						# var k = app.renderTpl("EmptyRender") #	 
						#= k({ message: "There are no Enabled Hotels" }) #
						
						{*# location.hash="addOffer" #*}
			# } #
            </div>

          </div> <!-- end span9 -->
</div>
</script>