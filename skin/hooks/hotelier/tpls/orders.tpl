<script id="orders" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/orders.js" data-type="text">
<div id="orders"> 
     <div class="span9">
			# if (totals > 0) { # 
     <div class="the-form">
     <div>
			<table class="table table-striped" >
              <thead>
                <tr>
                  <th>{$lang.order_number}</th>
                  <th>{$lang.am_mount}</th>
                  <th>{$lang.transaction_date}</th>
                  <th>{$lang.payment_method} </th>
                  <th>{$lang.sta_tus}</th>
                </tr>
              </thead>
              <tbody>
				# for (i=0; i<totals; i++) { #
					# var date = new Date(results[i].date_added*1000); #
					# var datevalues = [ date.getFullYear(),date.getMonth()+1,date.getDate()]; #
                <tr>
				   <td><img src="/images/site/pdf-icon.png" width="45" class="float-left spacer-right"/><a href="\\#" data-id="#= results[i].txn_id #" data-bind="click: print">#= results[i].txn_id #</a></td>                  
                  <td>#= results[i].total # {$current_currency.symbol}</td>
                  <td>#= datevalues[1]+" / "+datevalues[2]+" / "+datevalues[0] #</td>
                  <td>#= results[i].payment_method #</td>
                  <td>
					# if (results[i].status == 1 ) { #
						Completed
					# }  #
					# if ( results[i].status == 2 ||  results[i].status == 0   ) { #
						Pending
					# } #
				  </td>
                </tr>	
				# } #
				
              </tbody>
            </table>
				
			</div>
		 <hr/>
    <div class="pagination tac" >
    <ul>
	# var p=pages-1; #
 	# if (p != 0) { #
    <li><a href="\\#orders/#= p #/view" >Prev</a></li>
	# } #
	# var j=0; #
	# for (i=1; i<=totals; i += 10 ) { #
		# j++; #
		<li  class="# if ( j == pages ) { # active # } #" ><a href="\\#orders/#= j #/view" >#= j #</a></li>
	# } #
 	# if (j > pages) { #
 	<li><a href="\\#orders/#= p+2 #/view" >Next</a></li>
	# } #
    </ul>
    </div>

     </div>
			# } else { #	
						# var k = app.renderTpl("EmptyRender"); #				
						#= k({ message: "There are no Orders History" }) #	
			# } #
          </div> <!-- end span9 -->
 </div> 
</script>