<script id="rooms" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/rooms.js,/scripts/hotelier/tpls/bootstrap-confirm.js"  data-type="text">  
<div id="rooms" >
<div class="span9">

	# t=app.renderTpl('ToolbarHotelManager'); #
	#= t(results) #
	<div class="seperator"></div>
		<div class="btn-group " >	
		<a href="\\#" name="roomid[]" data-id="#=id#" data-bind="click: deleteSelected" class="btn  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">{$lang.hm_remove}</a>
		<a href="\\#" name="roomid[]" data-id="#=id#" data-bind="click: activated" class="btn  tip   float-left" data-placement="top" rel="tooltip" data-original-title="">{$lang.hm_activate}</a>
		<a href="\\#" name="roomid[]" data-id="#=id#" data-bind="click: deactivated" class="btn  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">{$lang.hm_deactivate}</a>
		<a href="\\#hotelManager/rooms/hotel/#= id #/new" class="btn btn-primary  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">{$lang.add_new_room}</a>
		{if $USER_CLASS == "A"}
			<a href="\\#" name="roomid[]" data-bind="click: enableDisableSelected" data-action="enableRooms" class="btn btn-success  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">Enable Room</a>
			<a href="\\#" name="roomid[]" data-bind="click: enableDisableSelected" data-action="disableRooms" class="btn btn-danger  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">Disable Room</a>
		{/if}
		</div>
	<div class="seperator"></div>
	<div class="the-form">
			<table  id="dashboard" class="table table-striped" >
              <thead>
				<tr>
                <th><input type="checkbox" data-bind="checked: allselect,click: selectAll" /></th>
                  <th>{$lang.my_rooms}</th>
                  <th>{$lang.added_at}</th>
                  <th>{$lang.price}</th>
                  <th>{$lang.hm_status}</th>
                </tr>
              </thead>
              <tbody>
				# for (i in results) { #
                <tr>
                  <th><input type="checkbox" value="#= results[i].id #" data-bind="checked: selection.room,click: selectedfn" /></th>
                  <td>
					# if (results[i].image_full != null) { #
 						<img src="#= results[i].image_full.thumb #" class="float-left spacer-right"/>
					# } else { #
						<img src="/images/site/dash-hotel.jpg"  class="float-left spacer-right"/>
					# } #
						<a href="\\#hotelManager/rooms/room/#= results[i].id #">#= results[i].title #</a><br/>
						<span class="font-10"> Can accomodate #= results[i].extra.RoomType # <br/> Currently are #= results[i].extra.roomCount # rooms available</span>
				  </td>
                  <td>#= kendo.toString(new Date( results[i].date_added * 1000),"d") # </td>
                  <td><span class="badge badge-success">#= results[i].extra.price+" "+app.symbol #</span></td>
                  <td><span class="label #if (parseInt(results[i].active)) { # btn-success # } else { # btn-danger # } #">#if (parseInt(results[i].active)) { # Active # } else { # Inactive # } #</span></td>
 				  {if $USER_CLASS == "A"}
				  		<td id="status-#= results[i].id #"><span class="label #if (parseInt(results[i].enable)) { # btn-success # } else { # btn-danger # } #">#if (parseInt(results[i].enable)) { # {$lang.enable} # } else { # {$lang.disable} # } #</span></td>
				  {/if}  
                </tr>
                # } #
              </tbody>
            </table>
			<input type="hidden" name="selected" data-bind="value: selected" value="" data-field="selection" class="postedData"/>
			</div>
          </div> <!-- end span9 -->
			<style type="text/css">
			\\#dashboard .highlight { background-color: \\#ffffcc; } 
			</style>
</div>
</script>