<script id="hotspots" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/hotspots.js,/scripts/hotelier/tpls/bootstrap-confirm.js,http://maps.googleapis.com/maps/api/js?sensor=false&callback=loadMaps,/scripts/site/maps/gMaps3.js"  data-type="text">
<div id="hotspots">
 <div class="span9"> 

# t=app.renderTpl('ToolbarHotelManager'); #
	#= t(results) #


<div class="alert alert-success hidden" id="success-spot">
 				Your hot-spot has been successfully saved.
			</div>


	<div class="the-form">
	<div id="GetLocation">
<div id="LocationCountry">
<div class="seperator"></div>
<span class="spacer-left spacer-right">Find Your Hotspot:</span>&nbsp;&nbsp;<a class="tip" style="text-decoration:none;"  data-original-title="Type a hotspot address,zoom and point ``the must know`` hotspot close to your hotel area ." rel="tooltip" data-placement="top" ><input type="text" id="AddressPoint" name="title" data-field="data" class="spacer-right input-xlarge search-query typeahead postedData" value="" data-bind="events: { keypress: Press }"></a>
<a href="\\#" data-bind="click: findPointByName" class="btn btn-primary">Find Point</a>

<div class="seperator"></div>
<div id="geocodingResults"></div>
<div class="seperator"></div>
<div id="mapHotspots" class="float-right map" style="width: 755px; height: 400px;"></div>
<div class="seperator"></div>
<textarea name="mapsData" id="mapsData" class="hide">#= mcms.objectToString(results.hotspots) #</textarea>
</div>
</div>
</div>
<div class="seperator"></div>
 <div class="btn-group float-right" data-bind="invisible: ShowForm" >
<a href="\\#" data-bind="invisible: EditHotspots,click: SavePoint" class="btn btn-primary ">Save Point</a>
<a href="\\#" data-bind="invisible: EditHotspots,click: DeletePoint" class="btn btn-primary ">Delete Point</a>
<a href="\\#" data-bind="invisible: CreateHotspots,click: CreatePoint" class="btn btn-primary ">Create Point</a>
<a href="\\#" data-bind="click: TranslateHotspot" class="btn btn-primary " >Translations</a>
	{if $USER_CLASS == "A"}
					<a href="\\#" name="hotspotsid[]" id="enableBtn" data-id="" data-bind="click: enableDisableSelected" data-action="enableHotspots" class="btn btn-success  tip hide " data-placement="top" rel="tooltip" data-original-title="">Enable Hotspots</a>
					<a href="\\#" name="hotspotsid[]" id="disableBtn" data-id="" data-bind="click: enableDisableSelected" data-action="disableHotspots" class="btn btn-danger  tip hide" data-placement="top" rel="tooltip" data-original-title="">Disable Hotspots</a>
	{/if}
</div>
<div class="seperator"></div>

<div class="well well-large" data-bind="invisible: ShowForm">
<label class="control-label"><h4 data-bind="text: LabelForm"	></h4></label>
<div class="float-left spacer-right">
	    <label>Title</label>
		<input id="hotspot-title" type="text" name="title" data-field="dataItem" data-bind="value: hotspotTitle,events: { change: addClassPosteddata }" class=" input-large span7 translations" data-tfield="title" data-tid="#= results.hotspots[0].id #" data-tkey="id" data-ttable="hotspots" value="#= results.hotspots[0].title #"  />
			<span id="tpl-title"> 
			</span>
</div>
<div class="float-left spacer-right">
	    <label>Description</label>
	 <textarea id="hotspot-description" class="input-large span7  translations"  data-tfield="description" data-tid="#= results.hotspots[0].id #" data-tkey="id" data-ttable="hotspots"  rows="3" name="description" data-field="dataItem" data-bind="value: hotspotDesc,events: { change: addClassPosteddata }">#= results.hotspots[0].description #</textarea>
		<span id="tpl-description">
		</span>
</div>
<div class="seperator"></div>

</div>
 </div> <!-- end span9 -->
# results.app = {} #
<input type="hidden" name="MapTypeId" id="MapTypeId" data-field="data" value="roadmap" class="postedData" />
<input type="hidden" name="module" id="module" data-field="data" value="hotspots" class="postedData" />
<input type="hidden" name="itemid" id="itemid" data-field="data" value="" class="postedData" />

<input type="hidden" name="hotel_id" id="hotel_id" data-field="dataItem" value="" class="postedData" />
<input type="hidden" name="zoomLevel" data-field="data" value="#= results.zoomLevel #" class="postedData" />
<input type="hidden" name="lat" id="lat" data-field="data" value="#= results.lat #" class="postedData" />
<input type="hidden" name="lng" id="lng" data-field="data" value="#= results.lng #" class="postedData" />
<input type="hidden" name="geocoderAddress"  id="geocoderAddress" data-field="data" value="" class="loc postedData" />

</div>
<style type="text/css">
.map img {
     max-width: none;
}
</style>
</script>

<script id="translatehotspots" type="text/kendo-ui-template"  data-script="/scripts/hotelier/tpls/translatehotspots.js" >
<div id="translatehotspots">
<textarea name="translateMap" id="translateMap" class="hide">#= mcms.objectToString(results.defaultdata) #</textarea>
<textarea name="translateMapLang" id="translateMapLang" class="hide">#= mcms.objectToString(results.data) #</textarea>
<div class="span9">
<div class="the-form">
<div class="alert alert-success hidden">
 				{$lang.alert_up_hotel_msg}
			</div>
<a href="\\#" data-bind="click: goBack" class="btn SaveTranslations float-left spacer-left spacer-top"><< Back</a>
<button class="btn SaveTranslations float-right spacer-right spacer-top" data-id="#= app.getArguments[1] #" data-bind="click: save">Save</button>
<div class="seperator"></div>
<form class="spacer-left padding-left">
<input type="hidden" name="currentLang" id="currentLang" value="gr" class="translations lang" />
<div class="float-left span6">
<h4 data-bind="text: EditLang"></h4>
<h4><a href="\\#" data-id="hotspots">Hotspots</a></h4>
<div id="hotspots" class="active">
<div class="control-group">
              <label class="control-label" for="">Hotspots title&nbsp;</label>
              <div class="controls">
			  		<input type="text"  class="translations" id="hotspots-title-#= app.getArguments[1] #" data-id="hotspots-title-#= app.getArguments[1] #" name="hotspots-title-#= app.getArguments[1] #" for="hotspots title" data-bind="click: GetLanguage" />
              </div>
            </div>
</div>
		<div class="control-group">
              <label class="control-label" for="">hotspots description&nbsp;</label>
              <div class="controls">
			  <textarea style="width:275px; height:160px;" class="translations" id="hotspots-description-#= app.getArguments[1] #" data-id="hotspots-description-#= app.getArguments[1] #" name="hotspots-description-#= app.getArguments[1] #" for="hotspots description" data-bind="click: GetLanguage"></textarea>
              </div>
            </div>
</div>
<div class="float-left">
<div class="wrap spacer-bottom">
<h4>Available languages</h4>
<div class="padding">
<ul id="languageTabs" class="idTabs">
# j =0; #
# for (i in results.availableLang) { #
		# if (!j) {  #
			<li><strong><a href="\\#tab1" rel="tab1" data-code="#= i #" data-bind="click: ChangeLang">#= results.availableLang[i] #</a></strong></li>		
		# } else { #
			<li><a href="\\#tab1" rel="tab1" data-code="#= i #"  data-bind="click: ChangeLang">#= results.availableLang[i] #</a></li>
		# } #
	    # j++; #
# } #
</ul>
</div><!-- END PADDING -->
</div><!-- END BOX -->


<div class="wrap spacer-bottom">
<h4>Default translation</h4>
<div class="sepH_c cf">
<textarea id="defaultTranslationTextArea" rows="15"></textarea>
</div><!-- END PADDING -->
</div><!-- END BOX -->
</div>
<div class="seperator"></div>
</form>
</div>
</div>
</div>
</script>