<script id="ToolbarHotelManager" type="text/kendo-ui-template" data-type="text"> 
 <div class="navbar">
    <div class="navbar-inner">
		<div class="container" style="width: auto;">
	<input type="hidden" name="hotelid" id="hotelid" value="#= results.id #"  />
    # if (results.title) { #
	<span data-placement="top" class="brand help">#= results.title #</span>
    # } else { #
		<span data-placement="top" rel="tooltip" data-original-title="Add a new Hotel" class="brand tip help"><i class="icon-plus"></i></span>
	# } #	
	<ul id ="toolbar" class="nav nav-pills" style="">
    <li class="SelectHotel" id="infoTab"><a href="\\#hotelManager/hotel/#= results.id #">{$lang.hmh_hotelinfo}</a></li>
    <li data-bind="invisible: newHotel" class="SelectHotel" id="roomsTab"><a href="\\#hotelManager/rooms/hotel/#= results.id #">{$lang.hmh_rooms}</a></li> 
    <li data-bind="invisible: newHotel" class="SelectHotel" id="locationTab"><a href="\\#hotelManager/Locations/#= results.id #">{$lang.hmh_location}</a></li> 
    <li data-bind="invisible: newHotel" class="SelectHotel" id="hotspotTab"><a href="\\#hotelManager/hotspots/#= results.id #">{$lang.hmh_hotspots}</a></li>
    <li data-bind="invisible: newHotel" class="SelectHotel" id="policyTab"><a href="\\#hotelManager/policy/#= results.id #">{$lang.hmh_policy}</a></li>
    <li data-bind="invisible: newHotel" class="SelectHotel" id="previewTab"><a href="\\#Popup" data-bind="click: displaypopup" data-route="popup/preview/#= results.id #"  data-toggle="modal">{$lang.hmh_pr_pub}</a></li>
    </ul>
	</div>
    </div>
    </div>
<style>
\\#ViewHotelier .brand {
	padding: 3px;	
	margin-left: 6px;
	margin-right: 20px;
	margin-top: 9px;
}
\\#ViewHotelier .navbar .nav {
	padding: 5px 10px 5px 0;
}
\\#ViewHotelier .navbar-inner {
	min-height: 30px;
}
</style>
</script>

<script id="items-rooms" type="text/kendo-ui-template" data-type="text">  
                 <tr>
				  <td>
						<input type="checkbox"  value="#= id #" class="postedData checkbox" name="id-#= id #" data-bind="click: checkRoom" #= enabled # />
				  </td>
                  <td>#= RoomType #</td>
				  <td  class="span5">
					   #= title #
					   <input type="hidden" name="title-#= id #" data-id="#= id #" class="postedData" value="#= title #"   />
				  </td>
 				  <td>#= quantity #</td>
				  <td id="price-#= id #">#= price+" "+controller.symbol #</td>
				  <td class="">
						<input type="text" class="postedData priceDiscount span1" name="discount-#= id #"  data-id="#= id #" data-field="DiscountData" value="#= discount #" #= enabled # min="1" max="#= price-1 #" step="1" validationMessage="Field discount of room #= title # is required" />
				  </td>
				  <td id="discount-#= id #" class="span3"></td>
                  <td><input  for="start discount date of room #= title #" class="postedData dates span3  input-small" name="startdate-#= id #" data-field="DatesData" value="#= startdate #" #= enabled # /></td>
                  <td><input for="end discount date of room #= title #" class="postedData dates span3  input-small" name="enddate-#= id #" data-field="DatesData" value="#= enddate #" #= enabled #  /></td>
				</tr>
				<style>
					.k-autocomplete, .k-combobox, .k-datepicker, .k-timepicker, .k-datetimepicker, .k-numerictextbox, .k-dropdown, .k-selectbox {
					width: 7.4em;
				}
				</style>
</script>

<script id="LocationToolbar" type="text/kendo-ui-template"  data-type="text">
<div id="LocationToolbar">
<div id="messages" class="alert alert-success hidden"></div>
 <ul class="nav nav-tabs" data-bind="invisible: newLocation">
{if $USER_CLASS == "A" }
    	<li class="LocationTab #= results.active.country #"><a href="\\#hotelManager/Locations/country/#= app.getArguments[1] #" id="countryid">Country Map</a></li>
    	<li class="LocationTab #= results.active.region # # if  (!parseInt(results.MapsEnable.countryid)) { #disabled# } #" # if  (!parseInt(results.MapsEnable.countryid)) { #  data-toggle="tab" # } #><a href="\\#hotelManager/Locations/region/#= app.getArguments[1] #" id="regionid">Region Map</a></li>
		<li class="LocationTab #= results.active.city # # if  (!parseInt(results.MapsEnable.regionid)) { #disabled# } #" # if  (!parseInt(results.MapsEnable.regionid)) { #  data-toggle="tab" # } #><a href="\\#hotelManager/Locations/city/#= app.getArguments[1] #" id="cityid">City Map</a></li>
{/if} 
		<li class="LocationTab #= results.active.hotel # "><a href="\\#hotelManager/Locations/#= app.getArguments[1] #" id="Locationsid">{$lang.hotel_map}</a></li>
		# for (i in results.active) { #

			# if ( results.active[i] != "" && i != "hotel" ) { #

				<a href="\\#translations/translate#= i  #/#= results.id  #" class="btn float-right" >Translations</a>
			# } #
		# } #
    </ul>
	
</div>
</script>


<script id="LocationRender" type="text/kendo-ui-template"  data-type="text">
<div id="LocationRender">
<div class="seperator"></div>
<div class="seperator"></div>
<h6>Update <span data-bind="text: SelectTitle"></span>:</h6>
<div class="control-group">
       <label class="control-label" for="">Title</label>
       <div class="controls">
             <input type="text" id="testAttr" name="UpdateTitle" class=" postedData input-small" value="" data-bind="value: SelectTitle" placeholder="name" for="title">
			 <a href="\\#" data-bind="click: UpdateTitle" class="btn btn-secondary float-right spacer-right">Save Title</a><br/><br/>
       </div>
</div>
<h6>Choose hotel's Country:</h6>
<ul class="unstyled" >

	# for (i in results) { #
		# if (countItems[results[i].id] !== undefined) { #
			# var total  = countItems[results[i].id]; #
		# } else {  #
		    # var total =0; # 
		# } #
	<li>
		# if (!parseInt(total)) { #
			<a href="\\#" data-bind="click: DeleteItem" id="deleteid-#= results[i].id #" class="delete">x</a>&nbsp;
		# } else { #
			&nbsp;&nbsp;&nbsp;
		# } #
		<a href="\\#" id="itemid-#= results[i].id #" data-bind="click: UpdateMap"  data-lat="#= results[i].lat #" data-lng="#= results[i].lng #" data-zoomLevel="#= results[i].zoomLevel #" data-geocoderAddress="#= results[i].geocoderAddress #">#= results[i].title #</a> <span>(#= total #)</span>
		<input type="hidden" class="translations" data-tfield="title" data-tkey="id" data-ttable="#= table #" data-tid="#= results[i].id #" name="geo_country-#= results[i].id #" value="#= results[i].title #" />
	</li>
	# } #
</ul>
</div>
</script>


<script id="jcc" type="text/kendo-ui-template" data-type="text" >
<form method="post" name="paymentForm" id="paymentForm" action="https://tjccpg.jccsecure.com/EcomPayment/RedirectAuthLink">
<input type="hidden" name="Version" value="#= gcc.version #"><br>
<input type="hidden" name="MerID" value="#= gcc.merchantID #"><br>
<input type="hidden" name="AcqID" value="#= gcc.acquirerID #"><br>
<input type="hidden" name="MerRespURL" value="#= gcc.responseURL #"><br>
<input type="hidden" name="PurchaseAmt" value="#= gcc.formattedPurchaseAmt #"><br>
<input type="hidden" name="PurchaseCurrency" value="#= gcc.currency #"><br>
<input type="hidden" name="PurchaseCurrencyExponent" value="#= gcc.currencyExp #"><br>
<input type="hidden" name="OrderID" value="#= gcc.orderID #"><br>
<input type="hidden" name="CaptureFlag" value="#= gcc.captureFlag #"><br>
<input type="hidden" name="Signature" value="#= gcc.base64Sha1Signature #"><br>
<input type="hidden" name="SignatureMethod" value="#= gcc.signatureMethod #"><br><!-- -->
<input type="submit" value="test" name="s" />
</form>


</script>

<script id="StatusRender" type="text/kendo-ui-template"  data-type="text">
<span class="label #if (parseInt(status)) { # btn-success # } else { # btn-danger # } #">#if (parseInt(status)) { # {$lang.enable} # } else { # {$lang.disable} # } #</span>
</script>

<script id="EnableDisableLink" type="text/kendo-ui-template"  data-type="text">
{if $USER_CLASS == "A" }
<li class="float-left" style="width:15%;">
<span id="li-#= table+"-"+id #" class="checkboxFive float-left spacer-right">
	<input type="checkbox" id="#= table+"-"+id #" data-id="#= id #" value="#= id #" class="admin"  data-bind="checked: selection.filters" style="margin-left:1px;" />
	<label for="#= table+"-"+id #" class="#if (parseInt(status) == 0 ) { # pending # } #" style=" margin-top:1px;margin-left:1px;"></label>
</span>
</li>
<li class="float-left" style="width:83%; margin-right:0px;margin:1px;">
{else}
<li class="float-left" style="width:100%; margin-right:0px;margin:1px;">
{/if}
</script>

<script id="selectProperties" type="text/kendo-ui-template"  >
		# if (module === "rooms" ) { #
                <select id="#= id #" class="multiselect hide  postedData" name="#= table #" data-field="roomProperties" multiple="multiple" >
				# for (i in data) { #
			   <option value="#= data[i].id #" # if (data[i].roomid != "" ) { # selected="selected" # } # data-enable="#= data[i].enable #">#= data[i].title #</option>
			   # } #
				</select>
		# } else { #
				<select id="#= id #" name="#= table #" class="multiselect hide postedData" data-field="hotelProperties" multiple="multiple" >
                # for (i in data) { #
			   		<option value="#= data[i].id #" # if (data[i].hotelid != "" ) { # selected="selected" # } # data-enable="#= data[i].enable #" >#= data[i].title #</option>
			   	# } #
				</select>
		# } #
</script>


<script id="EnableDisableFields" type="text/kendo-ui-template"  data-type="text">
{if $USER_CLASS == "A" }
# if  ( $.inArray(field, results.ReqFields) !== -1) { # 
<span id="Admin-#= field #">
	<a href="\\#" data-bind="click: RejectField" data-action="AcceptField"  data-id="#= field #-#= id #" class="btn btn-success spacer-left"><i class=" icon-ok" style="width:13px;height:15px;" ></i></a>
	<a href="\\#" data-bind="click: AcceptField" class="btn btn-danger" data-action="RejectField" data-id="#= field #-#= id #"><i class=" icon-remove" style="width:13px;height:15px;"></i></a>
	<input type="hidden" name="table-#= id #"  value="#= table #" class="postedData" />
</span>
# } #
{/if}
</script>

<script id="EmptyRender" type="text/kendo-ui-template"  data-type="text">

<div class="hero-unit tac" style="margin-bottom:0px;">
#= message #
<div class="seperator"></div>
</div>
</script>

<script id="MessageTpl" type="text/kendo-ui-template"  data-type="text">
<div class="alert #= status #" >
 				#= message #
</div>
</script>

<script id="wait-loader"  type="text/kendo-ui-template" data-type="text">
<div id="wait-loader" class="tac">
<br/>
<br/>
<img src="/images/site/ajax_loader.gif" width="100" />
<div class="seperator"></div>
<div class="seperator"></div>
<p>
Please Wait ...
</p>
</div>
</script>



