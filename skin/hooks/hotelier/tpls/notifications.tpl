<script id="notifications" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/notifications.js,/scripts/hotelier/tpls/bootstrap-confirm.js" data-type="text">  
<div id="notifications">
      <div class="span9">
<div class="btn-group">	
	<a href="\\#" name="hotelid[]" data-bind="click: deleteSelected" class="btn  tip  float-left" data-placement="top" rel="tooltip" data-original-title="">REMOVE</a>
</div>
    <div class="the-form spacer-top">
		<table class="table table-striped">
              <thead>
                <tr>
                <th><input type="checkbox" data-bind="checked: allselect,click: selectAll"  /></th>
                  <th>{$lang.date}</th>
                  <th>{$lang.sub_ject}</th>
                  <!--<th>{$lang.action}</th>-->         
                </tr>
              </thead>
              <tbody>
				# var total=results.length; #
				# if (totals > 0 ) { #
				# for (i=0; i<total; i++) { #
					# var date = new Date(results[i].date_added*1000); #
					# var datevalues = [ date.getFullYear(),date.getMonth()+1,date.getDate()]; #
                <tr id="notifications-#= results[i].id #">
                  <td><input type="checkbox" value="#= results[i].id #" data-bind="checked: selection.notifications,click: selectedfn"  /></td>
                  <td>#= datevalues[1]+" / "+datevalues[2]+" / "+datevalues[0] #</td>
                  <td><a href="\\#notificationsDetails/#= results[i].id #/details" >#= results[i].title #</a></td>
                  <td>#= results[i].title #</td>
                  <!--<td><a href="\\#notificationsDetails/#= results[i].id #/details" >{$lang.see_details}</a></td>-->
                </tr>
				# } #
				# } #
              </tbody>
            </table>
			<input type="hidden" name="selected" data-bind="value: selected" value="" data-field="selection" class="postedData"/>
             <hr/>
    <div class="pagination tac" >
    <ul>
	# var p=pages-1; #
 	# if (p != 0) { #
    <li><a href="\\#orders/#= p #/view" >Prev</a></li>
	# } #
	# var j=0; #
	# for (i=1; i<=totals; i += 10 ) { #
		# j++; #
		<li  class="# if ( j == pages ) { # active # } #" ><a href="\\#orders/#= j #/view" >#= j #</a></li>
	# } #
 	# if (j > pages) { #
 	<li><a href="\\#orders/#= p+2 #/view" >Next</a></li>
	# } #
    </ul>
    </div>
				</div>
					
          </div> <!-- end span9 -->
	</div>
</script>

<script id="notificationsDetails" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/notifications.js" data-type="text">  
<div id="notificationsDetails">
      <div class="span9">
    <div class="the-form">
	<span class="lead padding float-left">#= results.title #</span>
	<span class="padding float-right"><a href="\\#notifications" >{$lang.back_to_not}</a></span>
	<br />
	<hr/>
	<blockquote>
  	<p>#= results.description #</p>
	</blockquote>

	</div>
</div>
<style>
.lead {
	margin-bottom: 0px;	
	line-height: 18px;
}
</style>
</script>