<script id="OfferRoomSelect" type="text/kendo-ui-template" data-type="text">  
<div id="OfferRoomSelect">
   <table  id="dashboard" class="table table-striped" >
              <thead>
				<tr>
                <th></th>
 				  <th>{$lang.room_type}</th>
                  <th>Room Title</th>
                  <th>Qty</th>
                  <th class="span3"> Current Price</th>
				  <th>Offer Price</th>
				  <th  class="span2">Discount (%)</th>
                  <th>Date Started</th>
				  <th>Date Ended</th>
                </tr>
              </thead>
              <tbody data-template="items-rooms" data-bind="source: ItemRooms" >

              </tbody>
            </table>
			<div class="seperator"></div>
				<a rel="tooltip" data-placement="top" class="btn btn-primary btn-large spacer-right float-right" data-bind="click: ApplyOffer">Apply Special Offer(s)</a>
				<div class="seperator"></div>
</div>
<style scoped>

span.k-tooltip {
    border-width: 0px;
    display: inline-block;
    padding: 2px 5px 1px 6px;
    margin-bottom:10px;
    position: static;
    color:red;
	clear: both;
    /*background:none;*/
    background:url("/images/site/validation_advice_bg.gif") no-repeat scroll 2px 5px transparent;
}


</style>
</script>
