<script id="Locations" type="text/kendo-ui-template" data-script="http://maps.googleapis.com/maps/api/js?sensor=false&callback=loadMaps,/scripts/site/maps/gMaps3.js,/scripts/hotelier/tpls/extendtypehead.js,/scripts/hotelier/tpls/bootstrap-confirm.js,/scripts/hotelier/tpls/location.js"  data-type="text">    
<div id="Locations">
 <div class="span9">
	<input type="hidden" value="Locations" name="templateid" id="templateid" />
	<input type="hidden"  id="itemid-#= results.itemid #" data-bind="click: UpdateMap"  data-lat="#= results.location.lat #" data-lng="#= results.location.lng #" data-zoomLevel="#= results.location.zoomLevel #" data-geocoderAddress="#= results.location.geocoderAddress #"/>
	
	# t=app.renderTpl('ToolbarHotelManager'); #
	#= t(results) #
   
			# t=app.renderTpl('LocationToolbar'); #
			#= t(results) #
			<div class="alert alert-success hidden" id="success-location">
 				{$lang.loc_succ_save}
			</div>
			<div class="alert alert-success hidden" id="success-extras">
 				{$lang.basic_info_succ_hotel}
			</div>

	<div class="the-form">
	<div id="GetLocation">
		<div class="seperator"></div>
		<span class="spacer-left spacer-right">{$lang.point_hotel}:</span>&nbsp;&nbsp;  <a class="tip" style="text-decoration:none;"  data-original-title="{$lang.add_hotel_location_tip}" rel="tooltip" data-placement="top" ><input type="text" id="AddressPoint" name="title"  class="spacer-right input-xlarge search-query" value="" data-bind="value: SelectGeocoderAddress, events: { keypress: Press }"></a>
		<a href="\\#" data-bind="click: findPoint" class="btn btn-primary">{$lang.find_point}</a>
		<a href="\\#" id="SaveUpdate" data-id="#= results.id #" data-action="update" data-bind="invisible: hotelid,click: SavePoint" class="btn btn-primary">{$lang.save_point}</a>
		<div class="seperator"></div>
		<div id="geocodingResults"></div>
		<div class="seperator"></div>
		<div  id="Map" class="float-right map" style="width: 755px; height: 400px;"></div>
		<div class="seperator"></div>
		<input type="hidden" name="draggable"  value="1" class="loc" />
		<input type="hidden" name="zoomLevel" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectZoomLevel"  />
		<input type="hidden" name="lat"  id="lat" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectLat" />
		<input type="hidden" name="lng"  id="lng" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectLng" />
		<input type="hidden" name="geocoderAddress"  id="geocoderAddress" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectGeocoderAddress" />

		<style type="text/css">
		.map img {
     		max-width: none;
		}
		</style>


	</div>
	<div class="seperator"></div>
	</div>

<div class="seperator"></div>
<div class="seperator"></div>

<div class="well well-large" data-bind="invisible: hotelidTriggered ">

<label class="control-label" for=""><h4>{$lang.main_trans_dist}</h4></label>
<div class="seperator"></div>
# for (i in results.extrafield.locations ) { #
	# if (results.extrafield.locations[i].type == "text" ) { #
	<div class="float-left transportion">
	    <label>#= results.extrafield.locations[i].field #</label>
<div class="input-prepend input-append float-left ">
	 	<input type="text" class="span1 input-large postedData"  name="#= results.extrafield.locations[i].var_name #" data-type="notresponse" data-name="#= results.extrafield.locations[i].var_name #" data-field="ExtraItem" value="#= results.extrafield.locations[i].value #" data-bind="events: { change: calculateMeter } " />
		<span class="add-on"> meters </span> 
</div>
<span class="float-left spacer-left " id="calc-#= results.extrafield.locations[i].var_name #"> 
# if (results.extrafield.locations[i].value != " " ) { #
#= parseFloat(results.extrafield.locations[i].value,0,2)/1000 # km
# } else { #
0 km
# } #
 </span>
		<input type="hidden" name="id-#= results.extrafield.locations[i].var_name #" data-type="notresponse" data-field="ExtraItem" value="#= results.extrafield.locations[i].fieldid #" class="postedData" />
	</div>
	# } #
# j++; #
# } #

<div class="seperator"></div>
<div id="thumbs">
<label class="control-label spacer-bottom" for=""><h4>{$lang.basic_location_near}</h4></label>
<div class="seperator"></div>
<div class="seperator"></div>
# var j=0;  #
# for (i in results.extrafield.locations ) { #
	# if (results.extrafield.locations[i].type == "area" ) { #
	<div class="float-left spacer-left spacer-right">
		{if $USER_CLASS == "A"}
		<div class="checkboxFive float-left">
  										<input type="checkbox" id="checkboxFiveInput-#= results.extrafield.locations[i].fieldid #" data-id="#= results.extrafield.locations[i].fieldid #" value="#= results.extrafield.locations[i].fieldid #" data-bind="checked: selection.extra" />
	  									<label for="checkboxFiveInput-#= results.extrafield.locations[i].fieldid #"></label>
  									</div>
		{/if}
	    <label class="float-left padding-left">#= results.extrafield.locations[i].field #</label><div class="seperator"></div>
		 <textarea id="extra-#= results.extrafield.locations[i].fieldid #" class="input-large span2 postedData translations #= results.extrafield.locations[i].extraEnable #" data-tfield="value" data-tid="#= results.extrafield.locations[i].fieldid #" data-tkey="itemid" data-ttable="extra_fields"  rows="2" name="#= results.extrafield.locations[i].var_name #" data-field="ExtraItem" >#= results.extrafield.locations[i].value #</textarea>
		<input type="hidden" name="id-#= results.extrafield.locations[i].var_name #" data-field="ExtraItem" value="#= results.extrafield.locations[i].fieldid #" class="postedData" />  
	</div>
	# } #
# j++; #
# } #

</div>
<div class="seperator"></div>
	{if $USER_CLASS == "A"}
			<a href="\\#" name="extraLocationid[]" id="enableBtn" data-bind="click: enableDisableSelected" data-id="#= results.id #"  data-action="enableExtras" class="btn btn-success  tip" data-placement="top" rel="tooltip" data-original-title="">Enable Photo</a>
			<a href="\\#" name="extraLocationid[]" id="disableBtn" data-bind="click: enableDisableSelected" data-id="#= results.id #" data-action="disableExtras" class="btn btn-danger  tip" data-placement="top" rel="tooltip" data-original-title="">Disable Photo</a>
	{/if}
<a href="\\#translations/translateLocations/#= results.id  #" class="btn btn-primary float-right spacer-left">Translations</a>
<a href="\\#" data-bind="click: SaveExtraField" class="btn btn-primary float-right">{$lang.save_info}</a>
<input type="hidden" name="ids" data-bind="value: selected" value="" data-field="selection" class="postedData"/>
<div class="seperator"></div>
</div>

 </div> <!-- end span9 -->
# results.app = {} #
<input type="hidden" name="itemid" value="#= results.id #" class="postedData" />
<textarea name="mapsData" id="mapsData" class="hide">#= mcms.objectToString(results.location) #</textarea>

</div>
<style>
.transportion  span {
margin-left: 20px;
margin-right:5px;
}

.transportion {
width: 230px;
}
</style>
</script>

{if $USER_CLASS == "A" }
<script id="country" type="text/kendo-ui-template" data-script="http://maps.googleapis.com/maps/api/js?sensor=false&callback=loadMaps,/scripts/site/maps/gMaps3.js,/scripts/hotelier/tpls/extendtypehead.js,/scripts/hotelier/tpls/bootstrap-confirm.js,/scripts/hotelier/tpls/location.js"   data-type="text">   
<div id="country">
 <div class="span9">

	<input type="hidden" value="country" name="templateid" id="templateid" />
	# t=app.renderTpl('ToolbarHotelManager'); #
	#= t(results) #
   
	# t=app.renderTpl('LocationToolbar'); #
	#= t(results) #


<div class="the-form">
	<div class="float-left span3 border-loc" >

			# var k = app.renderTpl("LocationRender"); #				
			#= k({ "table": "geo_country","results": results.location, "countItems": results.countItem }) #

	</div>


<div class="places float-left padding">
<div class="seperator"></div>
<div class="seperator"></div>
<span class="spacer-left spacer-right">Find Country:</span>&nbsp;&nbsp;<input type="text" id="AddressPoint" name="title" data-provide="typeahead" data-field="saveMap" class="spacer-right input-medium search-query typeahead postedData" value="" data-bind="value: SelectGeocoderAddress" autocomplete="off">
<a href="\\#" data-bind="click: findPoint" class="btn btn-primary">Find Point</a>
<a href="\\#" data-action="insert" data-bind="invisible: insertBtn,click: SavePoint" data-id="#= app.getArguments[1] #" class="btn btn-secondary">Add Country</a>
<a href="\\#" data-action="update" data-bind="invisible: updateBtn,click: SavePoint" data-id="#= app.getArguments[1] #" class="btn btn-secondary float-right">Update Hotel</a>
<div class="seperator"></div>
<div class="seperator"></div>
<ul id="geocodingResults" class="unstyled"></ul>
<div class="seperator"></div>
<div id="Map" class="float-right map places"></div>
<div class="seperator"></div>
<div id="GetLocation">
<input type="hidden" name="draggable"  value="0" class="loc" />
<input type="hidden" name="id" value="" class="postedData" data-bind="value: SelectId" />
<input type="hidden" name="zoomLevel" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectZoomLevel" />
<input type="hidden" name="lat" id="lat" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectLat" />
<input type="hidden" name="lng" id="lng" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectLng" />
<input type="hidden" name="geocoderAddress"  id="geocoderAddress" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectGeocoderAddress" />
</div>
</div>
<textarea name="mapsData" id="mapsData" class="hide">#= mcms.objectToString(results.location) #</textarea>
<div class="seperator"></div>
</div>
</div>
</div>
<style type="text/css">
.map img {
     max-width: none;
}
</style>
</script>

<script id="translatecountry" type="text/kendo-ui-template"  data-script="/scripts/hotelier/tpls/translateLocations.js">
<div id="translatecountry">
<textarea name="translateMap" id="translateMap" class="hide">#= mcms.objectToString(results.defaultdata) #</textarea>
<textarea name="translateMapLang" id="translateMapLang" class="hide">#= mcms.objectToString(results.data) #</textarea>
<div class="span9">
<div class="the-form">
<div class="alert alert-success hidden">
 				Information around the hotel successfully updated.
			</div>
<a href="\\#" data-bind="click: goBack" class="btn SaveTranslations float-left spacer-left spacer-top"><< {$lang.back}</a>
<button class="btn SaveTranslations float-right spacer-right spacer-top" data-id="#= app.getArguments[1] #" data-bind="click: save">{$lang.save}</button>
<div class="seperator"></div>
<form class="spacer-left padding-left">
<input type="hidden" name="itemid" id="itemid" value="#= app.getArguments[1] #" class="translations" />
<input type="hidden" name="module" id="moduleItem" value="hotel" class="translations" />
<input type="hidden" name="currentLang" id="currentLang" value="gr" class="translations lang" />
<div class="float-left span6">

# for (i in results.defaultdata) { #


<h4><a href="\\#" data-bind="click: HotelProperties" data-id="geo_country-#= i #">Hotel #= i #</a></h4>
<div id="geo_country-#= i #" class="active Properties" data-bind="invisible: translateTab">
# for ( k in results.defaultdata[i] ) { # 
<div class="control-group">
              <label class="control-label" for="">#= results.defaultdata[i][k] #&nbsp;</label>
              <div class="controls">
				<input type="text" id="#= i+'-'+k #" name="#= i+'-'+k #" value="" class="translations" data-bind="click: GetLanguage" data-id="#= i+'-'+k #" />				
              </div>
            </div>
# } #
</div>

# } # 

</div>

<div class="float-left">
<div class="wrap spacer-bottom">
<h4>Available languages</h4>
<div class="padding">
<ul id="languageTabs" class="idTabs">
# j =0; #
# for (i in results.availableLang) { #
		# if (!j) {  #
			<li><strong><a href="\\#tab1" rel="tab1" data-code="#= i #" data-bind="click: ChangeLang">#= results.availableLang[i] #</a></strong></li>		
		# } else { #
			<li><a href="\\#tab1" rel="tab1" data-code="#= i #"  data-bind="click: ChangeLang">#= results.availableLang[i] #</a></li>
		# } #
	    # j++; #
# } #
</ul>
</div><!-- END PADDING -->
</div><!-- END BOX -->


<div class="wrap spacer-bottom">
<h4>Default translation</h4>
<div class="sepH_c cf">
<textarea id="defaultTranslationTextArea" rows="15"></textarea>
</div><!-- END PADDING -->
</div><!-- END BOX -->

</div>
</form>
<div class="seperator"></div>
</div>
</div>
</div>
</script>

<script id="translateregion" type="text/kendo-ui-template"  data-script="/scripts/hotelier/tpls/translateLocations.js">
<div id="translateregion">
<textarea name="translateMap" id="translateMap" class="hide">#= mcms.objectToString(results.defaultdata) #</textarea>
<textarea name="translateMapLang" id="translateMapLang" class="hide">#= mcms.objectToString(results.data) #</textarea>
<div class="span9">
<div class="the-form">
<div class="alert alert-success hidden">
 				Information around the hotel successfully updated.
			</div>
<a href="\\#" data-bind="click: goBack" class="btn SaveTranslations float-left spacer-left spacer-top"><< {$lang.back}</a>
<button class="btn SaveTranslations float-right spacer-right spacer-top" data-id="#= app.getArguments[1] #" data-bind="click: save">{$lang.save}</button>
<div class="seperator"></div>
<form class="spacer-left padding-left">
<input type="hidden" name="itemid" id="itemid" value="#= app.getArguments[1] #" class="translations" />
<input type="hidden" name="module" id="moduleItem" value="hotel" class="translations" />
<input type="hidden" name="currentLang" id="currentLang" value="gr" class="translations lang" />
<div class="float-left span6">

# for (i in results.defaultdata) { #


<h4><a href="\\#" data-bind="click: HotelProperties" data-id="geo_region-#= i #">Hotel #= i #</a></h4>
<div id="geo_region-#= i #" class="active Properties" data-bind="invisible: translateTab">
# for ( k in results.defaultdata[i] ) { # 
<div class="control-group">
              <label class="control-label" for="">#= results.defaultdata[i][k] #&nbsp;</label>
              <div class="controls">
				<input type="text" id="#= i+'-'+k #" name="#= i+'-'+k #" value="" class="translations" data-bind="click: GetLanguage" data-id="#= i+'-'+k #" />				
              </div>
            </div>
# } #
</div>

# } # 

</div>

<div class="float-left">
<div class="wrap spacer-bottom">
<h4>Available languages</h4>
<div class="padding">
<ul id="languageTabs" class="idTabs">
# j =0; #
# for (i in results.availableLang) { #
		# if (!j) {  #
			<li><strong><a href="\\#tab1" rel="tab1" data-code="#= i #" data-bind="click: ChangeLang">#= results.availableLang[i] #</a></strong></li>		
		# } else { #
			<li><a href="\\#tab1" rel="tab1" data-code="#= i #"  data-bind="click: ChangeLang">#= results.availableLang[i] #</a></li>
		# } #
	    # j++; #
# } #
</ul>
</div><!-- END PADDING -->
</div><!-- END BOX -->


<div class="wrap spacer-bottom">
<h4>Default translation</h4>
<div class="sepH_c cf">
<textarea id="defaultTranslationTextArea" rows="15"></textarea>
</div><!-- END PADDING -->
</div><!-- END BOX -->

</div>
</form>
<div class="seperator"></div>
</div>
</div>
</div>
</script>


<script id="translatecity" type="text/kendo-ui-template"  data-script="/scripts/hotelier/tpls/translateLocations.js">
<div id="translatecity">
<textarea name="translateMap" id="translateMap" class="hide">#= mcms.objectToString(results.defaultdata) #</textarea>
<textarea name="translateMapLang" id="translateMapLang" class="hide">#= mcms.objectToString(results.data) #</textarea>
<div class="span9">
<div class="the-form">
<div class="alert alert-success hidden" >
 				Information around the hotel successfully updated.
			</div>
<a href="\\#" data-bind="click: goBack" class="btn SaveTranslations float-left spacer-left spacer-top"><< Back</a>
<button class="btn SaveTranslations float-right spacer-right spacer-top" data-id="#= app.getArguments[1] #" data-bind="click: save">Save</button>
<div class="seperator"></div>
<form class="spacer-left padding-left">
<input type="hidden" name="itemid" id="itemid" value="#= app.getArguments[1] #" class="translations" />
<input type="hidden" name="module" id="moduleItem" value="hotel" class="translations" />
<input type="hidden" name="currentLang" id="currentLang" value="gr" class="translations lang" />
<div class="float-left span6">

# for (i in results.defaultdata) { #


<h4><a href="\\#" data-bind="click: HotelProperties" data-id="geo_cityr-#= i #">Hotel #= i #</a></h4>
<div id="geo_cityr-#= i #" class="active Properties" data-bind="invisible: translateTab">
# for ( k in results.defaultdata[i] ) { # 
<div class="control-group">
              <label class="control-label" for="">#= results.defaultdata[i][k] #&nbsp;</label>
              <div class="controls">
				<input type="text" id="#= i+'-'+k #" name="#= i+'-'+k #" value="" class="translations" data-bind="click: GetLanguage" data-id="#= i+'-'+k #" />				
              </div>
            </div>
# } #
</div>

# } # 

</div>

<div class="float-left">
<div class="wrap spacer-bottom">
<h4>Available languages</h4>
<div class="padding">
<ul id="languageTabs" class="idTabs">
# j =0; #
# for (i in results.availableLang) { #
		# if (!j) {  #
			<li><strong><a href="\\#tab1" rel="tab1" data-code="#= i #" data-bind="click: ChangeLang">#= results.availableLang[i] #</a></strong></li>		
		# } else { #
			<li><a href="\\#tab1" rel="tab1" data-code="#= i #"  data-bind="click: ChangeLang">#= results.availableLang[i] #</a></li>
		# } #
	    # j++; #
# } #
</ul>
</div><!-- END PADDING -->
</div><!-- END BOX -->


<div class="wrap spacer-bottom">
<h4>Default translation</h4>
<div class="sepH_c cf">
<textarea id="defaultTranslationTextArea" rows="15"></textarea>
</div><!-- END PADDING -->
</div><!-- END BOX -->

</div>
</form>
<div class="seperator"></div>
</div>
</div>
</div>
</script>

<script id="region" type="text/kendo-ui-template" data-script="http://maps.googleapis.com/maps/api/js?sensor=false&callback=loadMaps,/scripts/site/maps/gMaps3.js,/scripts/hotelier/tpls/extendtypehead.js,/scripts/hotelier/tpls/bootstrap-confirm.js,/scripts/hotelier/tpls/location.js"   data-type="text">   
<div id="region">
 <div class="span9">
	<input type="hidden" value="region" name="templateid" id="templateid" />
	# t=app.renderTpl('ToolbarHotelManager'); #
	#= t(results) #
	# t=app.renderTpl('LocationToolbar'); #
	#= t(results) #


<div class="the-form">
	<div class="float-left span3 border-loc" >
			# var k = app.renderTpl("LocationRender"); #				
			#= k({ "table": "geo_region","results": results.location, "countItems": results.countItem }) #
	</div>

<div class="places float-left padding">
<div class="seperator"></div>
<div class="seperator"></div>
<span class="spacer-left spacer-right">Find Country:</span>&nbsp;&nbsp;<input type="text" id="AddressPoint" name="title" data-provide="typeahead" data-field="saveMap" class="spacer-right input-medium search-query typeahead postedData" value="" data-bind="value: SelectGeocoderAddress" autocomplete="off">
<a href="\\#" data-bind="click: findPoint" class="btn btn-primary">Find Point</a>
<a href="\\#" data-action="insert" data-bind="invisible: insertBtn,click: SavePoint" data-id="#= app.getArguments[1] #" class="btn btn-secondary">Add Region</a>
<a href="\\#" data-action="update" data-bind="invisible: updateBtn,click: SavePoint" data-id="#= app.getArguments[1] #" class="btn btn-secondary float-right">Update Hotel</a>
<div class="seperator"></div>
<div class="seperator"></div>
<ul id="geocodingResults" class="unstyled"></ul>
<div class="seperator"></div>
<div id="Map" class="float-right map places"></div>
<div class="seperator"></div>
<div id="GetLocation">
<input type="hidden" name="draggable"  value="0" class="loc" />
<input type="hidden" name="id_country" value="#= results.MapsEnable.countryid #" data-field="saveMap" class="postedData" />
<input type="hidden" name="id" value="" class="postedData" data-bind="value: SelectId" />
<input type="hidden" name="zoomLevel" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectZoomLevel" />
<input type="hidden" name="lat" id="lat" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectLat" />
<input type="hidden" name="lng" id="lng" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectLng" />
<input type="hidden" name="geocoderAddress"  id="geocoderAddress" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectGeocoderAddress" />
</div>
</div>
<textarea name="mapsData" id="mapsData" class="hide">#= mcms.objectToString(results.location) #</textarea>
<div class="seperator"></div>
</div>
</div>
</div>
<style type="text/css">
.map img {
     max-width: none;
}
</style>
</script>


<script id="city" type="text/kendo-ui-template" data-script="http://maps.googleapis.com/maps/api/js?sensor=false&callback=loadMaps,/scripts/site/maps/gMaps3.js,/scripts/hotelier/tpls/extendtypehead.js,/scripts/hotelier/tpls/bootstrap-confirm.js,/scripts/hotelier/tpls/location.js"   data-type="text">   
<div id="city">
 <div class="span9">
	<input type="hidden" value="city" name="templateid" id="templateid" />
	# t=app.renderTpl('ToolbarHotelManager'); #
	#= t(results) #
	# t=app.renderTpl('LocationToolbar'); #
	#= t(results) #


<div class="the-form">
	<div class="float-left span3 border-loc" >
			# var k = app.renderTpl("LocationRender"); #				
			#= k({ "table": "geo_cityr", "results": results.location, "countItems": results.countItem }) #
	</div>

<div class="places float-left padding">
<div class="seperator"></div>
<div class="seperator"></div>
<span class="spacer-left spacer-right">Find Country:</span>&nbsp;&nbsp;<input type="text" id="AddressPoint" name="title" data-provide="typeahead" data-field="saveMap" class="spacer-right input-medium search-query typeahead postedData" value="" data-bind="value: SelectGeocoderAddress" autocomplete="off">
<a href="\\#" data-bind="click: findPoint" class="btn btn-primary">Find Point</a>
<a href="\\#" data-action="insert" data-bind="invisible: insertBtn,click: SavePoint" data-id="#= app.getArguments[1] #" class="btn btn-secondary">Add City</a>
<a href="\\#" data-action="update" data-bind="invisible: updateBtn,click: SavePoint" data-id="#= app.getArguments[1] #" class="btn btn-secondary float-right">Update Hotel</a>
<div class="seperator"></div>
<div class="seperator"></div>
<ul id="geocodingResults" class="unstyled"></ul>
<div class="seperator"></div>
<div id="Map" class="float-right map places"></div>
<div class="seperator"></div>
<div id="GetLocation">
<input type="hidden" name="draggable"  value="0" class="loc" />
<input type="hidden" name="id_region" value="#= results.MapsEnable.regionid #" data-field="saveMap" class="postedData" />
<input type="hidden" name="id" value="" class="postedData" data-bind="value: SelectId" />
<input type="hidden" name="zoomLevel" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectZoomLevel" />
<input type="hidden" name="lat" id="lat" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectLat" />
<input type="hidden" name="lng" id="lng" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectLng" />
<input type="hidden" name="geocoderAddress"  id="geocoderAddress" data-field="saveMap" value="" class="loc postedData" data-bind="value: SelectGeocoderAddress" />
</div>
</div>
<textarea name="mapsData" id="mapsData" class="hide">#= mcms.objectToString(results.location) #</textarea>
<div class="seperator"></div>
</div>
</div>
</div>
<style type="text/css">
.map img {
     max-width: none;
}
</style>
</script>
{/if}

<script id="translateLocations" type="text/kendo-ui-template"  data-script="/scripts/hotelier/tpls/translateLocations.js">
<div id="translateLocations">
<textarea name="translateMap" id="translateMap" class="hide">#= mcms.objectToString(results.defaultdata) #</textarea>
<textarea name="translateMapLang" id="translateMapLang" class="hide">#= mcms.objectToString(results.data) #</textarea>
<div class="span9">
<div class="the-form">
<div class="alert alert-success hidden" >
 				Information around the hotel successfully updated.
			</div>
<a href="\\#" data-bind="click: goBack" class="btn SaveTranslations float-left spacer-left spacer-top"><< Back</a>
<button class="btn SaveTranslations float-right spacer-right spacer-top" data-id="#= app.getArguments[1] #" data-bind="click: save">Save</button>
<div class="seperator"></div>
<form class="spacer-left padding-left">
<input type="hidden" name="itemid" id="itemid" value="#= app.getArguments[1] #" class="translations" />
<input type="hidden" name="module" id="moduleItem" value="hotel" class="translations" />
<input type="hidden" name="currentLang" id="currentLang" value="gr" class="translations lang" />
<div class="float-left span6">
<h4 data-bind="text: EditLang"></h4>

<h4><a href="\\#" data-id="extra_fields">Extra Fields</a></h4>
<div id="extra_fields" class="active Properties">
# var j=1; #
# for ( k in results.defaultdata.extra_fields) { # 
			<div class="control-group">
              <label class="control-label" for="">#= results.extraFields[k] #&nbsp;</label>
              <div class="controls">
				<input type="text" id="#= 'extra_fields-'+k #" name="#= 'extra_fields-'+k #" value="" class="translations" data-bind="click: GetLanguage" data-id="#= 'extra_fields-'+k #" />				
              </div>
            </div>
# j++; #
# } #
</div>

</div>

<div class="float-left">
<div class="wrap spacer-bottom">
<h4>{$lang.avaivable_languages}</h4>
<div class="padding">
<ul id="languageTabs" class="idTabs">
# j =0; #
# for (i in results.availableLang) { #
		# if (!j) {  #
			<li><strong><a href="\\#tab1" rel="tab1" data-code="#= i #" data-bind="click: ChangeLang">#= results.availableLang[i] #</a></strong></li>		
		# } else { #
			<li><a href="\\#tab1" rel="tab1" data-code="#= i #"  data-bind="click: ChangeLang">#= results.availableLang[i] #</a></li>
		# } #
	    # j++; #
# } #
</ul>
</div><!-- END PADDING -->
</div><!-- END BOX -->


<div class="wrap spacer-bottom">
<h4>{$lang.defaultTransaltion}</h4>
<div class="sepH_c cf">
<textarea id="defaultTranslationTextArea" rows="15"></textarea>
</div><!-- END PADDING -->
</div><!-- END BOX -->

</div>
</form>
<div class="seperator"></div>
</div>
</div>
</div>
</script>