<script id="preview" type="text/kendo-ui-template" data-script="/scripts/hotelier/tpls/preview.js"  data-type="text">

<div id="preview">
 
 <div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h3>Preview Hotel</h3>
<div class="modal-footer">
<div class="seperator"></div>
<span class="float-left"><strong>By clicking PUBLISH your hotel will be in ‘PENDING MODE’ until our system review your information.</strong></span>
<a href="\\#" class="btn-large btn-primary float-right" data-title="#= results[0].title #" data-id="#= results[0].id #" data-bind="click: publish">Publish Hotel</a>
</div>
</div>
<div class="body">
	<iframe src="/en/preview/hotel/information/#= results[0].id #/#= results[0].permalink #.html" width="1120" height="1150" scrolling="no"></iframe>
</div>
 <div class="modal-footer">
<div class="seperator"></div>
<span class="float-left"><strong>By clicking PUBLISH your hotel will be in ‘PENDING MODE’ until our system review your information.</strong></span>
<a href="\\#" class="btn-large btn-primary float-right" data-title="#= results[0].title #" data-id="#= results[0].id #" data-bind="click: publish">Publish Hotel</a>
</div>
<style type="text/css" >
/* PopUP Preview  */
\\#Popup {
	position: absolute;
	left: 50%;
	top: 50%;
	width: 1120px;
	height: 700px;
    margin: -250px 0 0 -560px;
}
</style>
</div>

</script>

<script id="redirect-dashboard" type="text/kendo-ui-template" data-type="text">
<div id="redirect-dashboard" class="tac spacer-top">
<br/>
<br/>
You are now on the way to be a member of DEKOHO group!<br/>
Your Hotel's data have been received from our system<br/> 
and have already sent  to our Hotels Administrators for approval.<br/> 
The next 24 hours you will be notified. thanks for your patience.
<br/><br/>
WE transfer you to Dashboard!
</div>
</script>