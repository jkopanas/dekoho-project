
{if $USER eq ""}
<div id="header-wrapper">
		 	  		
  <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          
          
          <div class="nav-collapse collapse">
             <ul class="nav">
              <li class="active"><a href="#">{$lang.ab_dekoho}</a></li>
              <li><a href="#">{$lang.terms_of_service}</a></li>
             
            </ul>
            <a href="/{$FRONT_LANG}/register_hotelier.html" class="brand">{$lang.create_account}</a>
           
           
           <ul class="nav "style="float:right;">
         <li class="dropdown">
               {foreach from=$availableLanguages item=a name=b}
						{if $a.code eq $FRONT_LANG} 
                <a data-toggle="dropdown" class="dropdown-toggle" href="{$a.thisURL}"><img src="/images/flags/{$a.code}.png" />&nbsp; &nbsp;{$lang[$a.country]} <b class="caret"></b></a>
                {/if}
                {/foreach}
                <ul class="dropdown-menu">
                {foreach from=$availableLanguages item=a name=b}
						{if $a.code ne $FRONT_LANG}
                  <li><a href="{$a.thisURL}"><img src="/images/flags/{$a.code}.png" />&nbsp; &nbsp;{$lang[$a.country]}</a></li> 
                      {/if}
                {/foreach}     
                </ul>
              </li>
             
            </ul>
           
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

	</div> <!--end header-wrapper -->

<div id="top-wrapper">
	  <div class="content">
		<div id="logo-banner-container">
		<div id="logo" class="float-left">
		<a href="/{$FRONT_LANG}/index.html"><img src="/images/site/logo.jpg" alt="dekoho" title="dekoho"/></a>
		</div>
		<div class="float-right" id="create-acct">
		<a href="/{$FRONT_LANG}/register_hotelier.html" class="btn btn-primary btn-large">{$lang.create_account}</a>
		</div>
		<br class="clear_0"/>
	
		</div> <!-- end logo-banner-container -->
	
	 </div> <!-- end top-content -->
	</div> <!-- end top-wrapper -->


<div id="map-wrapper"> 
	    <div class="content container">
		  <div id="htl-login">
			  <div id="login-msg" class="float-left">
			  <h2>Dekoho</h2>
			  <h5>A New Path for Hoteliers into the World of Online Hotel Reservations </h5>
			  <span>DEKOHO.COM was created to cater for the HOTELIER’s need to deal with the complexities of a dynamic competitive market  and the ever-changing conditions of the International Tourism Industry and the TRAVELER’s demand for Direct Access to Useful Information. 
                DEKOHO Network revolves around those 2 main objectives. Our Internet application aims to facilitate Travelers’ Access to Destination, help Hoteliers increase their Profits and enhance their prospects in the World Tourist Services Market. Our associates’ advanced know-how, commitment and will-power, made this vision possible by developing a model internet application. 
				DEKOHO.COM makes a difference, forging a New Path in the world of Online Hotel Reservations. </span>
			  <h5>The client pays directly to your HOTEL</h5>
			  <span> No hidden costs - No hidden charges.No Commission on Bookings - No Commission on Special Offer Bookings
				Unlike the commission-based model used by other hotel booking engines and tourist agencies, DEKOHO.COM is investing in the development of a Worldwide Partner Network. </span>
			<div class="seperator"></div>
			  </div>
			 <div id="lg-form" class="float-left">
			  <form class="form-signin" id="the_login" method="post" action="">
        <h2 class="form-signin-heading">{$lang.sign_in}</h2>
        {if $smarty.get.action == "enable"}
		Your Account was successfuly activated. You may login with your userid and password.
		{/if}
	
		<label><h5>{$lang.user_id}</h5></label>
        <input type="text" placeholder="{$lang.user_id}" class="input-block-level" name="uname" id="uname">
        <label><h5>{$lang.pass_word}</h5></label>
        <input type="password" placeholder="{$lang.pass_word}" class="input-block-level" name="pass" id="pass">
        <label class="checkbox">
         <a  href="#" class="float-right spacer-right" id="remind_password" >{$lang.remember_password}</a> 
        </label>
        <button type="submit" class="btn btn-large btn-primary">{$lang.sign_in2}</button>
      </form>
			  <div id="show_password" class="hidden">
			  <div id="hidden-msg"></div><div class="seperator"></div>
			  <h2 class="form-signin-heading">{$lang.reset_password}</h2>
			  <label><h5> {$lang.your_email}</h5></label>
		<input id="reset-email" class="input-block-level" type="email" data-email-msg="E-mail format is not valid" required="" for="e-mail" placeholder="{$lang.email}" name="reset-email">
			   <a class="btn btn-large btn-primary" id="password-reminder">{$lang.reset_password}</a><a href="#" id="back-to-login" class="float-right">{$lang.back}</a>
			  </div>
			  </div>
			  <br class="clear_0"/>
		  </div>
		
	   </div> <!-- end map-content -->
	   <div class="seperator"></div> 
   </div> <!-- end map-wrapper -->
   
   

  <div id="footer-wrapper">
	 <div class="content">

	<div id="fotoer-menu" class="float-left">
	          <ul class="bottom-menu float-left">
			  		
			  		<li class="bottom-menu-item"><a href="#">{$lang.ab_dekoho}</a>&nbsp;|</li>
			  		<li class="bottom-menu-item"><a href="#">{$lang.terms_of_service}</a>&nbsp;|</li>
			  		<li class="bottom-menu-item"><a href="/{$FRONT_LANG}/register_hotelier.html">{$lang.create_account}</a></li>
			  		
		  		</ul>
		  		
	</div>
	<div class="float-right spacer-right">
		  <span class="copy spacer-right">&copy; Copyright 2013 Dekoho All Rights Reserved</span>
		  </div>
	</div> <!-- end footer-content -->
	<div class="seperator"></div>
	</div><!--   end footer-wrapper -->
	
	<script type="text/javascript">
	head.js('/scripts/site/registerHotelier.js');
	head.js('/scripts/site/remind_pass.js'); 	
	</script>
	
   {else}
   
 	{include file=$general_top}

    	<div class="content"  id="min-height">
    	<div class="container">
	<div class="span3 bs-docs-sidebar" id="nomarginl">
        <ul class="nav nav-list bs-docs-sidenav affix">
          <li id="dashboardMenu" class="mainMenu"><a href="#dashboard"><i class="icon-chevron-right"></i> <i class="icon-home"></i> &nbsp;{$lang.the_dash}</a></li>
          <li id="profileMenu" class="mainMenu"><a href="#profileManager/account"><i class="icon-chevron-right"></i> <i class="icon-user"></i> &nbsp;{$lang.profile_man}</a></li>
          <li id="hotelMenu" class="mainMenu"><a href="#hotelManager/hotels"><i class="icon-chevron-right"></i><i class="icon-folder-close"></i> &nbsp; {$lang.hotel_man}</a></li>
          <li id="offersMenu" class="mainMenu"><a href="#offers"><i class="icon-chevron-right"></i><i class="icon-bullhorn"></i> &nbsp; {$lang.my_offers}</a></li>
          <li id="ordersMenu" class="mainMenu"><a href="#orders"><i class="icon-chevron-right"></i><i class="icon-list"></i> &nbsp; {$lang.orders_hi}</a></li>
          <li id="notificationsMenu" class="mainMenu"><a href="#notifications"><i class="icon-chevron-right"></i><i class="icon-envelope"></i> &nbsp; {$lang.my_not}</a></li>
          <li class="mainMenu"><a href="#support"><i class="icon-chevron-right"></i><i class="icon-comment"></i> &nbsp; {$lang.support}</a></li>

        </ul>
      </div>
       <input type="hidden" value="{$exchange}" name="exchange" />
	  <input type="hidden" value="{$current_currency.currency}" name="currencyType" />
	  <input type="hidden" value="{$current_currency.currency}" name="currencySymbol" />
      	<div id="ViewHotelier" ></div>
      	
      	  {include file="hooks/hotelier/tpls/helperTpl.tpl"}
      </div>
	</div><!-- end page content -->
	<texarea id="ReturnPost" name="ReturnPost" class="hidden">{$ReturnPost|json_encode}</texarea>
	<div id="Popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
	<div id="confirmDiv" style="display: none;" class="">
	
	</div>
 	{include file=$general_footer}
 
  	<script type="text/javascript">
  		head.js('/scripts/hotelier/router.js');
  		head.js('/scripts/hotelier/validate.js');
  		head.js('/scripts/hotelier/hotelier.js');
  		head.js('/scripts/hotelier/global.js');
  		head.js('/scripts/hotelier/routes.js');
  		head.js('/scripts/ckeditor/ckeditor.js','/scripts/ckeditor/config.js');	
  	</script>

   {/if}
   
   