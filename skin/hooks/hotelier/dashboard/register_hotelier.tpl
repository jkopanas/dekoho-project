<div id="header-wrapper">
		 
		  		
  <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          
          
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="active"><a href="#">{$lang.ab_dekoho}</a></li>
              <li><a href="#">{$lang.terms_of_service}</a></li>
            </ul>
            <a href="/{$FRONT_LANG}/hotelier/index.html" class="brand">{$lang.login}</a>
           
           <ul class="nav" style="float:right;">
               <li class="dropdown">
               {foreach from=$availableLanguages item=a name=b}
						{if $a.code eq $FRONT_LANG} 
                <a data-toggle="dropdown" class="dropdown-toggle" href="{$a.thisURL}"><img src="/images/flags/{$a.code}.png" />&nbsp; &nbsp;{$lang[$a.country]} <b class="caret"></b></a>
                {/if}
                {/foreach}
                <ul class="dropdown-menu">
                {foreach from=$availableLanguages item=a name=b}
						{if $a.code ne $FRONT_LANG}
                  <li><a href="{$a.thisURL}"><img src="/images/flags/{$a.code}.png" />&nbsp; &nbsp;{$lang[$a.country]}</a></li> 
                      {/if}
                {/foreach}     
                </ul>
              </li>
             
            </ul>
           
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
		  		

	</div> <!--end header-wrapper -->

<div id="top-wrapper">
	  <div class="content">
		<div id="logo-banner-container">
		<div id="logo" class="float-left">
		<a href="/{$FRONT_LANG}/index.html"><img src="/images/site/logo.jpg" alt="dekoho" title="dekoho"/></a>
		</div>
		<div class="float-right" id="create-acct">
		<a href="/{$FRONT_LANG}/hotelier/index.html" class="btn btn-primary btn-large">{$lang.login}</a>
		</div>
		<br class="clear_0"/>
	
		</div> <!-- end logo-banner-container -->
	
	 </div> <!-- end top-content -->
	</div> <!-- end top-wrapper -->


<div id="map-wrapper"> 
	    <div class="content container">
		  <div id="htl-login" class="float-left;">
			  <div id="login-msg" class="float-left">
			  <h2>{$lang.create_account}</h2> 
					 <div class="register-img">
					    <img src="/images/site/create_account_1.jpg" class="float-left spacer-right" />
					    <span class="bold">Create & Manage your Hotel Webpage </span><br/><br/>
					    <span>Set up your own Hotel Webpage with description, themes, photos, videos, interactive maps and multilingual webpage support. </span>
					  </div>
					  <div class="register-img">
					    <img src="/images/site/create_account_2.jpg" class="float-left spacer-right" />
					     <span class="bold">Create & Manage Types of Rooms</span><br/><br/>
					     <span>Set up your Hotel’s Rooms with description, types, themes, photos, videos, multilingual webpage support.</span>
					  </div>
					  <div class="register-img">
					    <img src="/images/site/create_account_3.jpg" class="float-left spacer-right" />
					     <span class="bold">Create & Manage SPECIAL OFFERS</span><br/><br/>
					     <span>Set up your Hotel’s Offers! Select time period, add description, types of rooms, themes, photos, videos and multilingual webpage support.</span>
					  </div>
			  </div>
			 <div id="ca-form" class="float-left">
			  <h2 class="form-signin-heading">{$lang.please_type_in}</h2>
			  <form class="form-signin">
         <label><h5>{$lang.your_email}</h5></label>
        <input type="email" name="email" placeholder="{$lang.your_email_place}" id="email" class="input-block-level userData" for="e-mail" required data-email-msg="{$lang.email_not_valid}">
         <label><h5>{$lang.confirm_your_email}</h5></label>
        <input type="email" name="confirm_email" id="confirm_email"  placeholder="{$lang.confirm_your_email_place}" class="input-block-level" for="confirm e-mail" data-verifymails-msg="{$lang.email_not_match}" data-email-msg="{$lang.confirm_email_not_valid}" required>
         <label><h5>{$lang.your_password}</h5></label>
        <input type="password" id="pass" placeholder="{$lang.your_password_place}" name="pass" class="input-block-level userData" for="pass" required>
        <label><h5> {$lang.confirm_pass}</h5></label>
        <input type="password" name="confirm_password" placeholder="{$lang.confirm_pass_place}" class="input-block-level" for="confirm password" data-verifypasswords-msg="{$lang.pass_not_match}" required>
        <label><h5> {$lang.country}</h5></label>
        <select id="user_location" class="userData" name="user_location" for="country" required validationmessage="Please select your country">
        <option value="" name="" id="" class="userData">{$lang.sel_country}...</option>
        {foreach from=$countries item=a name=b}
        <option value="{$a.code}">{$a.country}</option>
        {/foreach}
        </select>
         <br/> <br/>
        <label class="checkbox">
          <input type="checkbox" value="1" name="accept_terms" required validationmessage="{$lang.agree_with_terms}"> {$lang.accept_dekohoterms} <a href="#terms"  data-toggle="modal">{$lang.terms_of_service}</a> 
        </label><br class="clear_0"/>
        <span class="k-widget k-tooltip k-tooltip-validation k-invalid-msg" style="display:none;" data-for="accept_terms"><span class="k-icon k-warning"> </span> </span>
       <br/>
        <button type="button" class="btn btn-large btn-primary float-right" id="create-account">{$lang.create_account}</button>
      </form>
			  
			  </div>
			  <br class="clear_0"/>
		  </div>
		
	   </div> <!-- end map-content -->
	   <div class="seperator"></div>
   </div> <!-- end map-wrapper -->
   
   
   
   
   

  <div id="footer-wrapper">
	 <div class="content">
	
	
	
	<div id="fotoer-menu" class="float-left">
	          <ul class="bottom-menu float-left">
			  		
			  		<li class="bottom-menu-item"><a href="#">{$lang.ab_dekoho}</a>&nbsp;|</li>
			  		<li class="bottom-menu-item"><a href="#">{$lang.terms_of_service}</a>&nbsp;|</li>
			  		<li class="bottom-menu-item"><a href="/{$FRONT_LANG}/hotelier/index.html">{$lang.login}</a></li>
			  		
		  		</ul>
		  		
	</div>
	<div class="float-right spacer-right">
		  <span class="copy spacer-right">&copy; Copyright 2013 Dekoho All Rights Reserved</span>
		  </div>
	</div> <!-- end footer-content --> 
	<div class="seperator"></div>
	</div><!--   end footer-wrapper -->
   
  {include file=$terms}
   {include file=$privacy}
   
   
   <style scoped>

span.k-tooltip {
    border-width: 0px;
    display: inline-block;
    padding: 2px 5px 1px 6px;
    margin-bottom:10px;
    position: static;
    color:red;
    /*background:none;*/
    background:url("/images/site/validation_advice_bg.gif") no-repeat scroll 2px 0 transparent;
}

</style>
   
   
  <script type="text/javascript">

head.js("/scripts/site/registerHotelier.js");

</script> 