<div id="header-wrapper">
		 	  		
  <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          
          
          <div class="nav-collapse collapse">
           <a href="#dashboard" class="brand">{if $USER_NAME eq "" AND $USER_SURNAME eq ""}{$UNAME}{else}{$USER_NAME} {$USER_SURNAME}{/if} | </a><a href="/index.php?logout" class="brand"> {$lang.logout} </a>
			

           		  		<div id="currencyForm">
		  		<div id="currency-language" class="float-right">

			  		<div class="currency float-left" >
			  			<ul class="nav ">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$current_currency.symbol} {$current_currency.currency} <b class="caret"></b></a>
                <ul class="dropdown-menu" style="min-width:140px; height: 200px; overflow-y:auto;">
                 {foreach from=$currencies item=a name=b}
                  <li><a href="#" data-link="{$a.currency}" class="currency" data-bind="click: changeCurrency">{$a.symbol}  {$a.currency}</a></li>      
                 {/foreach}
                </ul>
               
              </li>
            </ul>     
			  		</div>
		</div>
           </div>
           <ul class="nav "style="float:right;">
              <li class="dropdown">
               {foreach from=$availableLanguages item=a name=b}
						{if $a.code eq $FRONT_LANG} 
                <a data-toggle="dropdown" class="dropdown-toggle" href="{$a.thisURL}"><img src="/images/flags/{$a.code}.png" />&nbsp; &nbsp;{$lang[$a.country]} <b class="caret"></b></a>
                {/if}
                {/foreach}
                <ul class="dropdown-menu">
                {foreach from=$availableLanguages item=a name=b}
						{if $a.code ne $FRONT_LANG}
                  <li><a href="{$a.thisURL}"><img src="/images/flags/{$a.code}.png" />&nbsp; &nbsp;{$lang[$a.country]}</a></li> 
                      {/if}
                {/foreach}     
                </ul>
              </li>
             
            </ul>
           
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

	</div> <!--end header-wrapper -->
	
	<div id="top-wrapper">
	  <div class="content">
		<div id="logo-banner-container">
		<div id="logo" class="float-left">
		<a href="/{$FRONT_LANG}/hotelier/index.html#dashboard"><img src="/images/site/logo.jpg" alt="dekoho" title="dekoho"/></a>
		</div>
		<div class="float-right" id="create-acct">
	
		<a href="#hotelManager/hotel" class="btn btn-primary btn-large spacer-right tip" data-placement="top" rel="tooltip" data-original-title="{$lang.add_the_new_hotel}">{$lang.addnew_hotel}</a>
		<a href="#addOffer" class="btn btn-primary btn-large tip" data-placement="top" rel="tooltip" data-original-title="{$lang.add_the_offer}">{$lang.addnew_offer}</a>
		</div>
		<br class="clear_0"/>
	
		</div> <!-- end logo-banner-container -->
	
	 </div> <!-- end top-content -->
	</div> <!-- end top-wrapper -->