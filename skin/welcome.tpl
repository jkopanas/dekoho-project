

   <div id="map-wrapper"> 
	    <div class="content" id="z999">
	    
	    <div class="alert hidden tac" id="wrong_dates_dest">
 				We Could not find the destination or arrival / departure dates are missing
 				<div class="seperator"></div>
			</div>
		    <div id="search">
		    <ul class="search-menu float-left">
			    	<li class="search-item active-search" id="hotel-finder"><a href="#">Hotel Finder</a></li>
			    	<li class="search-item" id="special-offer"><a href="#">Special Offers</a></li>
			    	<li class="search-item" id="roulete"><a href="#">Suprise Me</a></li>
			    </ul>
			    <div class="seperator"></div>
			   <form method="post" name="main_search_form" action="">
			    <div id="map-filters">
			    <div  style="height:70px;">
				    <div class="map-filter-item" id="remove-destination">
				    <span>Destination</span><div class="seperator"></div>
				    <input type="text" name="destination" id="destination" class="grey SearchText" placeholder="city, region" />
				    </div>
				  </div>  
				    <div class="map-filter-item">
				    <span>Arrival</span><div class="seperator"></div>
				    <input type="text" id="arrival" class="grey SearchText-small" autocomplete="off" />
				    </div>
				    <div class="map-filter-item">
				    <span>Departure</span><div class="seperator"></div>
				    
				    <input type="text" id="departure" class="grey SearchText-small" autocomplete="off"/>
				    </div>
				     <div class="map-filter-item">
				    <span>Rooms</span><div class="seperator"></div>
				    <select id="rooms" name="rooms" class="grey selectrooms-persons">
				     <option value="1">1 Room</option>
				     <option value="2">2 Rooms</option>
				     <option value="3">3 Rooms</option>
				     <option value="4">4 Rooms</option>
				     <option value="5">5 Rooms</option>
				     <option value="6">6 Rooms</option>
				     <option value="7">7 Rooms</option>
				    </select>
				    </div>
				    <div class="map-filter-item">
				    <span>Persons</span><div class="seperator"></div>
				    <select name="persons" id="persons" class="grey selectrooms-persons persons-to-submit">
				     <option value="1">1 Person</option>
				     <option value="2" selected="selected">2 Persons</option>
				     <option value="3">3 Persons</option>
				     <option value="4">4 Persons</option>
				     <option value="5">5 Persons</option>
				     <option value="6">6 Persons</option>
				     <option value="7">7 Persons</option>
				    </select>
				    </div>
				    <div id="the-more-rooms" class=""> </div>
				    <br class="clear_0" />
				     <div class="map-filter-item spacer-right">
				    <span>Budget from</span><div class="seperator"></div>
				   
				 
						<div class="control-group">
					      <div class="input-prepend input-append float-left ">
					    		<input type="text" dest="from" integer="" max="10" min="0"  class=" SearchText-mini grey" id="budgetfrom" name="budget_from" autocomplete="off" >
					    		<span class="add-on"> {$current_currency.currency} </span> 
					    	</div>
						</div>
				    </div>
				    <div class="map-filter-item spacer-left">
				    <span>Bugdet to</span><div class="seperator"></div>		    
				    	<div class="control-group">
				      		<div class="input-prepend input-append float-left ">
				    			<input type="text" max="10" min="0"  class=" SearchText-mini grey" id="budgetto" name="budgetto" autocomplete="off">
				    			<span class="add-on"> {$current_currency.currency} </span> 
				    		</div>
						</div>
				    </div>
				    <!-- 
				    <div class="more-criteria">
				    <span><img src="/images/site/more-criteria.jpg" alt="" title=""/><a href="#">More search criteria</a></span>
				    </div> -->
				    <br class="clear_0"/>
				    <div class="find-out">
				 <a class="btn btn-primary btn-large" id="main_search_btn" href="">Find Out Now</a>
				   	</div>	 
				    <div class="seperator"></div><div class="seperator"></div>
			    </div>
			 </form>
		    </div>
		 <!--   <div class="float-left"><img src="/images/site/map.jpg" class="float-left" height="400" width="708"/></div> -->
		 <div class="float-left" id="map-cont"><div id="map" class="float-left" style="width:100%; height:100%;"></div></div>
	   </div> <!-- end map-content -->
	   <div class="seperator"></div>
   </div> <!-- end map-wrapper -->
   
<input type="hidden" id="city_id" value="{$city_id}"/>
<input type="hidden" id="city_name" value="{$city_name}"/>
<input type="hidden" id="city_type" value="{$city_type}"/>
<input type="hidden" id="lat" value="{$lat}"/>
<input type="hidden" id="lng" value="{$lng}"/>

  <div id="page-wrapper">
    <div class="content">
  
  <div class="left-column-main">
  
  <div  id="airtickets"></div><div class="seperator"></div>

  <div id="dekoho2"></div>
  <div class="seperator"></div>
  <a href="http://www.airotel.gr/" target="_blank"><img src="/images/swf/Airotel.jpg" class="float-left"/></a>
<script type="text/javascript">
	var flashvars = {
	xml_path: "/xml/dekoho2.xml"
	};
	var params = {};
	var attributes = {};
	swfobject.embedSWF("/swf/dekoho2.swf", "dekoho2", "300", "250", "9.0.0",
	"expressInstall.swf", flashvars, params, attributes);
</script>
     		<script type="text/javascript">

 				var attributes = {
     				scale:"exactfit"
     				
     			};
		 		var params = {
		 				wmode:"transparent"
     			};

    			swfobject.embedSWF("/swf/airtickets300x250.swf", "airtickets", "300", "250", "9",true,attributes,params);
    			
			</script>
  <!--  <a href="#"><img src="/images/site/left-banner.jpg" alt="" title=""/></a>-->
  
  </div>
	
		  <div class="right-column-main">
			<div id="special-offers">
		    <h2>Popular Destinations</h2> 
			 
		    <ul class="special-offers-menu">
		    <li class="special-item"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#?persons-0=3&room-0=1&roomsNum=1&arrival=20-06-2013&departure=26-06-2013&lang=en&page=1&field=price&dir=ASC&destination=London,%20UK&destinationType=region&destinationID=44&mode=normal" class="">London</a>&nbsp;|</li>
		    <!--  <li class="special-item"><a href="#">Santorini</a>&nbsp;|</li> -->
		    <li class="special-item"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#?persons-0=2&room-0=1&roomsNum=1&arrival=20-05-2013&departure=16-06-2013&lang=en&page=1&field=price&dir=ASC&destination=Crete,%20Greece&destinationType=region&destinationID=129&mode=normal">Crete</a>&nbsp;|</li>
		    <li class="special-item"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#?persons-0=2&room-0=1&roomsNum=1&arrival=20-05-2013&departure=16-06-2013&lang=en&page=1&field=price&dir=ASC&destination=Paris,%20France&destinationType=region&destinationID=53&mode=normal">Paris</a>&nbsp;|</li>
		    <li class="special-item"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#?persons-0=2&room-0=1&roomsNum=1&arrival=20-05-2013&departure=16-06-2013&lang=en&page=1&field=price&dir=ASC&destination=Barcelona,%20Spain&destinationType=region&destinationID=52&mode=normal">Barcelona</a>&nbsp;|</li>
		    <li class="special-item"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#?persons-0=2&room-0=1&roomsNum=1&arrival=20-05-2013&departure=16-06-2013&lang=en&page=1&field=price&dir=ASC&destination=Berlin,%20Germany&destinationType=region&destinationID=92&mode=normal">Berlin</a>&nbsp;|</li>
		    <li class="special-item"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#?persons-0=2&room-0=1&roomsNum=1&arrival=20-05-2013&departure=16-06-2013&lang=en&page=1&field=price&dir=ASC&destination=Madrid,%20Spain&destinationType=region&destinationID=61&mode=normal">Madrid</a>&nbsp;|</li>
		    <li class="special-item"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#?persons-0=2&room-0=1&roomsNum=1&arrival=20-05-2013&departure=16-06-2013&lang=en&page=1&field=price&dir=ASC&destination=Moscow,%20Russia&destinationType=city&destinationID=5&mode=normal">Moscow</a>&nbsp;|</li>
		    <li class="special-item"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#?persons-0=2&room-0=1&roomsNum=1&arrival=20-05-2013&departure=16-06-2013&lang=en&page=1&field=price&dir=ASC&destination=Rome,%20Italy&destinationType=region&destinationID=141&mode=normal">Rome</a>&nbsp;|</li>
		    <li class="special-item"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#?persons-0=2&room-0=1&roomsNum=1&arrival=20-05-2013&departure=16-06-2013&lang=en&page=1&field=price&dir=ASC&destination=Istanbul,%20Turkey&destinationType=region&destinationID=45&mode=normal">Istanbul</a>&nbsp;|</li>
		    <li class="special-item"><a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#?persons-0=2&room-0=1&roomsNum=1&arrival=20-05-2013&departure=16-06-2013&lang=en&page=1&field=price&dir=ASC&destination=Beijing,%20China&destinationType=region&destinationID=2&mode=normal">Beijing</a></li>
		    </ul> 
		    <br class="clear_0"/>
		    <div class="special-hotel">
			    <div class="hotel-img">
			    <img src="/images/site/london.jpg" alt="" title=""/>
			    </div>
			    <div class="hotel-info">
			   <!--   <span>1 Hotel in</span><br/>-->
			    <span>London, UK</span><!-- <span class="hotel-details">&nbsp;&nbsp;<a href="#">offer details</a></span> -->
			    <div class="seperator"></div>
			     <div class="orange">Visit Now London Uk </div><br/>
			    <!-- <span class="orange">Price per room/per night</span>&nbsp;&nbsp;<span class="line-through blue">$40</span><br/>
			    <span class="orange">Special price</span>&nbsp;&nbsp;<span class="blue">$30</span><br/>
			    <span class="orange">Validity</span>&nbsp;&nbsp;<span class="blue">7/3/2013</span> --> 
			    <a href="http://www.dekoho.com/en/search.html#?persons-0=3&room-0=1&roomsNum=1&arrival=20-06-2013&departure=26-06-2013&lang=en&page=1&field=price&dir=ASC&destination=London,%20UK&destinationType=region&destinationID=44&mode=normal" class="btn btn-info btn-midium spacer-right float-right">SEE NOW</a>
			    </div>
			    
		    </div>
		     <div class="special-hotel-last">
			    <div class="hotel-img">
			    <img src="/images/site/barca.jpg" alt="" title=""/>
			    </div>
			    <div class="hotel-info">
			    <!--  <span>5 Hotels in</span><br/>-->
			    <span>Barcelona,Spain</span><!-- <span class="hotel-details">&nbsp;&nbsp;<a href="#">offer details</a></span> -->
			    <div class="seperator"></div>
			     <div class="orange">Visit Now Barcelona ,Spain </div><br/>
			   
			    <a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#?persons-0=2&room-0=1&roomsNum=1&arrival=20-06-2013&departure=26-06-2013&lang=en&page=1&field=price&dir=ASC&destination=Barcelona,%20Spain&destinationType=region&destinationID=52&mode=normal" class="btn btn-info btn-midium spacer-right float-right">SEE NOW</a>
			    </div>  
		    </div>
		    
		    <div class="special-hotel">
			    <div class="hotel-img">
			    <img src="/images/site/new_york.jpg" alt="" title=""/>
			    </div>
			    <div class="hotel-info">
			   <!--   <span>2 Hotels in</span><br/>-->
			    <span>New York, United States</span><!-- <span class="hotel-details">&nbsp;&nbsp;<a href="#">offer details</a></span> -->
			    <div class="seperator"></div>
			     <div class="orange">Visit Now New York United States </div><br/>
			   
			    <a href="http://www.dekoho.com/{$FRONT_LANG}/search.html#?persons-0=2&room-0=1&roomsNum=1&arrival=20-06-2013&departure=26-06-2013&lang=gr&page=1&field=price&dir=ASC&destination=New%20York,%20NY,%20USA&destinationType=region&destinationID=82&mode=normal" class="btn btn-info btn-midium spacer-right float-right">SEE NOW</a>
			    </div> 
		    </div>
		    
		    <div class="special-hotel-last">
			    <div class="hotel-img">
			    <img src="/images/site/athens.jpg" alt="" title=""/>
			    </div>
			    <div class="hotel-info">
			   <!--   <span>8 Hotels in</span><br/>-->
			    <span>Athens,Greece</span><!-- <span class="hotel-details">&nbsp;&nbsp;<a href="#">offer details</a></span> -->
			    <div class="seperator"></div>
			     <div class="orange">Visit Now Athens Greece </div><br/>
			   
			    <a href="/{$FRONT_LANG}/search.html#?persons-0=2&room-0=1&roomsNum=1&arrival=20-06-2013&departure=26-06-2013&lang=en&page=1&field=price&dir=ASC&destination=Attica, Greece&destinationType=region&destinationID=11&mode=normal" class="btn btn-info btn-midium spacer-right float-right">SEE NOW</a>
			    </div>
		    </div>
		    
		    </div>
		  </div><!-- end right-column-min -->
	
  <div class="seperator"></div>
  </div> <!-- end page-content --> 
  
  </div> <!-- end page-wrapper -->
  
  <script src="/scripts/site/maps/gMaps3.js"></script>
<script src="/scripts/site/main_search.js"></script>	
  