<div id="map-wrapper"> 
	    <div class="content">
		    <div id="search">
		    <ul class="search-menu float-left">
			    	<li class="search-item"><a href="#">Hotel Finder</a></li>
			    	<li class="search-item"><a href="#">Special Offers</a></li>
			    	<li class="search-item"><a href="#">Hotel Roulette</a></li>
			    </ul>
			    <div class="seperator"></div>
			    <div id="map-filters">
				    <div class="map-filter-item">
				    <span>Destination</span><div class="seperator"></div>
				    <input type="text" name="destination" id="destination" class="grey SearchText" placeholder="city, region" />
				    </div>
				    <div class="map-filter-item">
				    <span>Arrival</span><div class="seperator"></div>
				    <input type="text" name="destination" id="destination" class="grey SearchText-small" />
				    </div>
				    <div class="map-filter-item">
				    <span>Departure</span><div class="seperator"></div>
				    <input type="text" name="destination" id="destination" class="grey SearchText-small" />
				    </div>
				     <div class="map-filter-item">
				    <span>Rooms</span><div class="seperator"></div>
				    <select id="rooms" name="rooms" class="grey selectrooms-persons">
				     <option value="1">1 Room</option>
				     <option value="2">2 Rooms</option>
				     <option value="3">3 Rooms</option>
				     <option value="4">4 Rooms</option>
				     <option value="5">5 Rooms</option>
				     <option value="6">6 Rooms</option>
				     <option value="7">7 Rooms</option>
				    </select>
				    </div>
				    <div class="map-filter-item">
				    <span>Persons</span><div class="seperator"></div>
				    <select name="persons" id="persons" class="grey selectrooms-persons ">
				     <option value="1">1 Person</option>
				     <option value="2">2 Persons</option>
				     <option value="3">3 Persons</option>
				     <option value="4">4 Persons</option>
				     <option value="5">5 Persons</option>
				     <option value="6">6 Persons</option>
				     <option value="7">7 Persons</option>
				    </select>
				    </div>
				     <div class="map-filter-item">
				    <span>Budget</span><div class="seperator"></div>
				    <input type="text" name="destination" id="destination" class="grey SearchText" />
				    </div>
				    <div class="more-criteria">
				    <span><img src="/images/site/more-criteria.jpg" alt="" title=""/><a href="#">More search criteria</a></span>
				    </div>
				    <a href="#"><img src="/images/site/map-button.jpg" alt="" title=""/></a>
				    <div class="seperator"></div><div class="seperator"></div>
			    </div>
		    </div>
		 <!--   <div class="float-left"><img src="/images/site/map.jpg" class="float-left" height="400" width="708"/></div> -->
	   </div> <!-- end map-content -->
	   <div class="seperator"></div>
   </div> <!-- end map-wrapper -->