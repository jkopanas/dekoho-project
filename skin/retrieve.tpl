<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$SITE_NAME} {if $extraField.Meta_tags.meta_title.value!=""}| {$extraField.Meta_tags.meta_title.value}{/if}</title>
{if $extraField.Meta_tags.meta_description.value !=""}<meta name="description" content="{$extraField.Meta_tags.meta_description.value}" />{/if}
{if $extraField.Meta_tags.meta_keywords.value !=""}<meta name="keywords" content="{$extraField.Meta_tags.meta_keywords.value}" />{/if}
<link href="/scripts/kendo/styles/kendo.common.min.css" rel="stylesheet"/>
<link href="/scripts/kendo/styles/kendo.metro.min.css" rel="stylesheet"/>

<link href="/css/site/retrieve.css" rel="stylesheet" type="text/css" media="all">
<link href="/css/site/grid.css" rel="stylesheet" type="text/css" media="all">

<script type="text/javascript" src="/scripts/head.load.min.js"></script>
<script type="text/javascript">
var mcms;
head.js('/scripts/jquery-1.8.2.min.js','/scripts/mcmsFront.js','/scripts/kendo/js/kendo.core.min.js','/scripts/kendo/js/kendo.validator.min.js','/scripts/kendo/js/kendo.data.min.js','/scripts/kendo/js/kendo.binder.min.js');
head.ready(function(){
mcms =  $(document).mCms({ lang:$('input[name=loaded_language]').val()}).data().mcms;//Initialize
$.lang = mcms.getLang();
mcms.cultures = {};
var cu = jQuery.parseJSON($('textarea[name=mcmsCultures]').val());
mcms.cultures = cu;
});
</script>
</head>
<body>
<input type="hidden" id="currentYear" name="currentYear" value="{$targetYear}" />
<div class="LeftRightMega"></div>

<input type="hidden" value="{$FRONT_LANG}" name="loaded_language" id="loaded_language" />
<textarea class="hidden" name="mcmsCultures">{literal}{"name":"en","numberFormat":{"pattern":["-n"],"decimals":2,",":",",".":".","groupSize":[3],"percent":{"pattern":["-n %","n %"],"decimals":2,",":",",".":".","groupSize":[3],"symbol":"%"},"currency":{"pattern":["($n)","$n"],"decimals":2,",":",",".":".","groupSize":[3],"symbol":"$"}},"calendars":{"standard":{"days":{"names":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"namesAbbr":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"namesShort":["Su","Mo","Tu","We","Th","Fr","Sa"]},"months":{"names":["January","February","March","April","May","June","July","August","September","October","November","December",""],"namesAbbr":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec",""]},"AM":["AM","am","AM"],"PM":["PM","pm","PM"],"patterns":{"d":"M/d/yyyy","D":"dddd, MMMM dd, yyyy","F":"dddd, MMMM dd, yyyy h:mm:ss tt","g":"M/d/yyyy h:mm tt","G":"M/d/yyyy h:mm:ss tt","m":"MMMM dd","M":"MMMM dd","s":"yyyy'-'MM'-'dd'T'HH':'mm':'ss","t":"h:mm tt","T":"h:mm:ss tt","u":"yyyy'-'MM'-'dd HH':'mm':'ss'Z'","y":"MMMM, yyyy","Y":"MMMM, yyyy"},"/":"/",":":":","firstDay":0}}}{/literal}</textarea>
<!-- SET: WRAPPER -->
<div class="wrapperHeader">
<div class="container_14 containerHeader">
        <div id="header">
            <div class="grid_4 languages">
                <span class="text1">
                <label>{$lang.country}:</label>
                <strong>England</strong></span>
                <div class="text2">
                    <label>{$lang.language}:</label>
                   
                    <span><strong><img src="/images/flags/{$FRONT_LANG}.png" id="country-flag" width="20" height="11" alt=""/></strong>
                    <select name="" id="all-countries" class="select-top">
                        {foreach from=$availableLanguages item=a name=b}
                        <option data-url="{$a.thisURL}" value="{$a.code}" {if $a.code eq $FRONT_LANG}selected="selected"{/if}>{$a.country}</option>
                       {/foreach}
                    </select>
                    </span> <span id="select_img"></span></div>
            </div>
            <div class="container_14 menu">
                <h1 class="grid_2 logo">
                    <a href="/{$FRONT_LANG}/index.html"><img src="/images/site/logo.png" width="180" height="100" alt="img"></a>
                </h1>
                <div>
                    <div class="nav">
                    	<ul>
                    		{include file="modules/themes/menu_recursive.tpl" menu=$TopMenu.items level=0 class=""}
                       	</ul>
                    </div>
                    <div id="search" class="search">
                        <form name="searchform" id="searchform" class="searchform" method="post" action="/{$FRONT_LANG}/search/1/">
                        <span class="search-box">
                        <input name="title" id="title" type="text" class="field title-search" value="" placeholder="{$lang.enter_your_search}" /></span>
                        <input type="hidden" id="description" name="description" class="field description-search" value="" />
                        <input type="hidden" name="pageSize" value="10" />
                        <input type="hidden" name="logic" value="or" />
                        <input type="hidden" name="operator" value="LIKE" />
                        <input type="hidden" name="content" value="cruising" />
						<input type="hidden" name="components" value="cruise-excursions/shore-excursion" />
						<input type="hidden" name="ports" value="cruise-destinations/cruise-port" />
						<input type="hidden" name="itinerary" value="cruises/cruise-itinerary" />
                        <a href="#" class="butn Search-btn" name="">Search</a>
                         </form>
                    </div>
                </div>
            </div>
        </div>
 
<div class="wrapper">
<div class="container_12 container">
      
        
        {*foreach item=entry from=$TopMenu.items name=menuid}
       		{include file=$entry.template id=$entry.id}
        {/foreach*}
 
        <!-- END: HEADER -->
            <div class="grid_12 main_content">
            	{include file=$include_file}
            </div>
       	<!-- END: MAIN CONTENT -->
        	 <!-- SET: FOOTER -->
        <div id="footer" class="grid_12">
            <div class="grid_12 footer-content">
                <div class="grid_12 footer-top">              
             
                </div>

                    <span class="copy">&copy; 2001-2012 Louis Hellenic Cruises All Rights Reserved.</span>
                </div>
            </div>
        <!-- END: FOOTER -->
        
        
         
    <!-- END: CONTAINER -->
    
</div>
<!-- END: WRAPPER -->
    
<!--    
     <div class="language-switcher">{include file="langSelect.tpl"}</div>    
        


<div class="nav-container">
    <ul id="nav">
		<li><a href="/{$FRONT_LANG}/index.html"><span>{$lang.home}</span></a></li>

  </ul>
</div>
 
<div class="main-container {if $area eq 'home' OR ($item AND $current_module.name eq "products")}col1-layout{elseif $item AND $current_module.name eq "content"}col2-left-layout{/if}">
{if $nav}{include file="modules/themes/nav_bar.tpl"}{/if}
<div class="main">
  <input type="text" class="autoComplete" name="title" id="title" data-searchBy="title" /> 
 <div class="results" data-for="title"></div>


<div class="seperator"></div> -->

<div id="windowpopup"></div>
</div><!-- END MAIN -->    
<script>
		head.js('/scripts/site/kendoMix.js','/scripts/site/gallery.js','/scripts/site/global.js');
		  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37021045-1']);
  _gaq.push(['_setDomainName', 'marketers.gr']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

     </div>
     </div> 
</body>
</html>
