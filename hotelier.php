<?php
include("init.php");

$current_module = ($_GET['module']? $loaded_modules[$_GET['module']]: $loaded_modules['content']);
$smarty->assign("current_module",$current_module);




if (!$smarty->isCached('home.tpl',$url)) {

	$l = new siteModules();
	$layout = $l->pageBoxes(($_GET['module']? $_GET['module']: 'all'),e_FILE,'admin',array('getBoxes'=>'full','fields'=>'boxes,areas,id,settings','boxFields'=>'id,title,name,settings,fetchType,file,required_modules,template','init'=>1,'boxFilters'=>array('active'=>1),'debug'=>0));

	$smarty->assign("layout",$layout['boxes']);
	
}//END BOXES

$currencies=array();
$sql->q("select * from exchange group by currency;");
$currencies=execute_multi($sql);

//$currencies[]= array("currency"=>"EUR","rate" => "1","symbol"=> "&euro;");

$smarty->assign("currencies",$currencies);

$_SESSION['currency'] = ( !isset($_SESSION['currency']) ) ? $_SESSION['plugins']['dynamicLanguage']['currency'] : $_SESSION['currency'];


foreach($currencies as $key => $value) {

	if  ( $value['currency'] == $_SESSION['currency'] ) {
		$rate = $value['rate'];
		$smarty->assign("current_currency",$currencies[$key]);
		unset($currencies[$key]);
	}
}

$smarty->assign("currencies",$currencies);
$_SESSION['exchange']=$rate;
$smarty->assign("exchange",$rate);
//echo  $_SESSION['plugins']['dynamicLanguage']['marketKey']."marketkey";
$_SESSION['plugins']['dynamicLanguage']['countryName'] =($lang[$_SESSION['plugins']['dynamicLanguage']['countryName']])?$lang[$_SESSION['plugins']['dynamicLanguage']['countryName']]:$_SESSION['plugins']['dynamicLanguage']['countryName'];
$smarty->assign("Cultures",$_SESSION['plugins']['dynamicLanguage']['marketSettings']);
$smarty->assign("TheCountry",$_SESSION['plugins']['dynamicLanguage']['countryName']);
$smarty->assign("MarketKey",$_SESSION['plugins']['dynamicLanguage']['marketKey']);
$smarty->assign("CulturesTpl",json_decode($_SESSION['plugins']['dynamicLanguage']['marketSettings'],true));
$smarty->assign("area","home");//assigned template variable include_file
include(ABSPATH."/memcache.php");
$smarty->caching = USE_SMARTY_CAHCHING;
HookParent::getInstance()->doTriggerHookGlobally("HomePreFetch");
if ($_GET['action'] == "payment") {
	$smarty->assign("ReturnPost",$_POST);
}
$smarty->assign("general_top","hooks/hotelier/dashboard/general_top.tpl");
$smarty->assign("general_footer","hooks/hotelier/dashboard/general_footer.tpl");
$smarty->display("hooks/hotelier/home.tpl",$url);//Display the home.tpl template
?>