var express = require('express');
var app = express();
global.appUrl = app.url =  "http://mcms.home.local";
app.configure(function () {
    app.use(express.static(__dirname + '/public'));
    app.use(express.methodOverride());
    app.use(express.bodyParser());
    app.use(express.cookieParser('secret'));
    app.use(express.cookieSession({secret:'mySecret'}));
    app.use(app.router)
});

/* SOLVES THE CORS PROBLEM */
app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", app.url);
    res.header("Access-Control-Allow-Headers", 'Content-Type, X-Requested-With');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
    res.header('Access-Control-Allow-Credentials', 'true');
    next();
});

// handle OPTIONS requests from the browser
app.options("*", function(req,res,next){res.send(200);});

app.get('/', function(req,res,next){
    res.render('index',
        { title : 'Home' }
    );
});


app.listen(8080);