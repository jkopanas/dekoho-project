/**
 * Author:      Evangelos Pappas
 * description: The module fields class
 *
 */

var Mfields = (function() {
	var __self = {
		token: {},
		db: {},
		mNode: {},
		callback: function(row,errors) { return {}}
	};

	var __sets = {
		filters: {},
		conf: {},
		fields: {}
	};

	var resultset = {};

	/**
	 *
	 * @param args
	 * @return {*}
	 * @constructor
	 */
	function Mfields(args) {
		this.__self = {
			token: args.token,
			db: args.db,
			mNode: args.mNode,
			callback: args.callback
		}
		return this;
	}

	/**
	 *
	 * @param fltrs
	 * @return {*}
	 */
	Mfields.prototype.filters = function(fltrs) {

		return this;
	}

	/**
	 *
	 * @param tbls
	 * @return {*}
	 */
	Mfields.prototype.fields = function(tbls) {
		var __this = this;
		var tblArr = [];
		var count = 0;
		var rowsToken = {};
		// multiple or single requests
		if(typeof tbls === 'string') {
			tblArr .push(tbls);
		}
		else {
			tblArr = tbls;
		}
		var tblArrLength = tblArr.length;
		function tmpCallback(rows, err) {

			// error checking
			if(err === null) {
				rowsToken[tblArr[count].key] = rows;
			}
			else {
				rowsToken[tblArr[count].key] = err;
			}

			// received enough callbacks as the table length requested
			if((++count)>=tblArrLength) {
				__this.resultset = rowsToken;
				__this.__self.callback(rowsToken, null);
			}
		}

		// query for each of table
		for(var i in tblArr) {
			this.__self.db.query(tblArr[i].sql, function(err, rows, fields) {
				if (err) {
					// Local Callback
					tmpCallback([], err);
					return;
				}
				var tmpArr = [];
				// each field is pushed to array Result token
				for(var f in rows) {
					tmpArr.push(rows[f]);
				}
				// Local Callback
				tmpCallback(tmpArr, null);
			});
		}

		return this;
	}

	return Mfields;
})();

exports.Mfields = Mfields;