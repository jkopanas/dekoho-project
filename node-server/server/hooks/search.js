/**
 * Author:      Evangelos Pappas
 * description: The Search class
 *
 */

var Search = (function() {
	var __self = {
		token: {},
		db: {},
		mNode: {},
		callback: function(row,errors) { return {}}
	};

	var __sets = {
		filters: {},
		sql:"",
		conf: {},
		fields: {},
		page:1,
		perPage:10
	};

	/**
	 *
	 * @param args
	 * @return {*}
	 * @constructor
	 */
	function Search(args) {
		this.__self = {
			token: args.token,
			db: args.db,
			mNode: args.mNode,
			callback: args.callback,
			conf: args.conf
		}
		this.__sets = __sets;
		this.__sets.perPage = conf.perPage;
		this.__sets.filters = {
			services:[],
			themeshotels:[],
			facilities:[],
			types:[],
			//rooms:0,
			//persons:0,
			budget:0//,
			//arrival:0,
			//destination:"",
			//departure:0
		};
		this.__sets.filtersSQL = {
			services: " services.id IN ( :args: ) ",
			themeshotels: " themeshotels.id IN ( :args: ) ",
			facilities: " facilities.id IN ( :args: ) ",
			types:" types.id IN ( :args: ) ",
			//rooms:0,
			//persons:0,
			budget: " rooms_hotel.price >= ( :args: ) "//,
			//arrival:0,
			//destination:"",
			//departure:0
		};
		this.__sets.sql = "";
		this.resultset = {};
		this.modQuery = new global.modQuery({
			db: this.__self.db,
			mNode: this.__self.mNode,
			conf: this.__self.conf,
			callback: this.__self.callback
		});
//			.from("hotel")
//			.select(['id','title'])
//			.filterBy("services","id").in([1,2,3,4,5])
//			.filterBy("facilities","id").in([1,2,3,4,5])
//			.groupBy('id')
//			.sortBy(['id','title'])
//			.limit(1,10)
//			.build()
//			.execute();

		return this;
	}

	/**
	 *
	 * @param page
	 * @return {*}
	 */
	Search.prototype.page = function(page) {
		this.__sets.page = page;
		return this;
	}

	/**
	 *
	 * @param limit
	 * @return {*}
	 */
	Search.prototype.perPage = function(limit) {
		this.__sets.perPage = limit;
		return this;
	}

	/**
	 *
	 * @param fltrs
	 * @return {*}
	 */
	Search.prototype.filters = function(fltrs) {
		this.__sets.filters = {
			services: fltrs.services?fltrs.services:[],
			themeshotels: fltrs.themeshotels?fltrs.themeshotels:[],
			facilities: fltrs.facilities?fltrs.facilities:[],
			types: fltrs.types?fltrs.types:[],
			//rooms: fltrs.rooms?parseInt(fltrs.rooms):0,
			//persons: fltrs.persons?parseInt(fltrs.persons):0,
			budget: fltrs.budget?parseInt(fltrs.budget):0//,
			//arrival: fltrs.arrival?fD(fltrs.arrival):0,
			//destination: fltrs.destination?fltrs.destination:"",
			//departure: fltrs.departure?fD(fltrs.departure):0
		};

		//format days
		function fD(str) {
			var strsplit = str.split("-");
			strsplit = [strsplit[1],strsplit[0],strsplit[2]];
			return parseInt((new Date(strsplit.join("/"))).getTime()/1000);
		}

		var sqlToken = [];

		for(var k in this.__sets.filters) {
			if(typeof this.__sets.filters[k] === "object") {
				if(this.__sets.filters[k].length>0) {
					sqlToken.push(this.__sets.filtersSQL[k].replace(":args:",this.__sets.filters[k].join(",")));
				}
			}
			else {
				if(this.__sets.filters[k]) {
					sqlToken.push(this.__sets.filtersSQL[k].replace(":args:",this.__sets.filters[k]));
				}
			}
		}
		var q = sqlToken.join(" AND ");
		this.__sets.sql = " SELECT :fields: " +
			" FROM hotel " +
			//" LEFT JOIN hotel_services ON hotel_services.hotelid = hotel.id " +
			//" LEFT JOIN services ON services.id = hotel_services.itemid " +
			//" LEFT JOIN hotel_facilities ON hotel_facilities.hotelid = hotel.id " +
			//" LEFT JOIN facilities ON facilities.id = hotel_facilities.itemid " +
			//" LEFT JOIN hotel_types ON hotel_types.hotelid = hotel.id " +
			//" LEFT JOIN types ON types.id = hotel_types.itemid " +
			//" LEFT JOIN rooms_hotel ON rooms_hotel.hotel_id = hotel.id " +
			//" LEFT JOIN rooms ON rooms.id = rooms_hotel.room_id " +
			(q.length>0?" WHERE "+q:"");

		return this;
	}

	/**
	 *
	 * @param ids cached ids
	 * @return {*}
	 */
	Search.prototype.getHotels = function(ids) {
		var __this = this;
		var sql = this.__sets.sql.replace(":fields:"," hotel.id, hotel.title, hotel.permalink, hotel.stars ") +
			" GROUP BY hotel.id " +
			" LIMIT "+this.__sets.page+","+this.__sets.perPage;
		this.__self.db.query(sql, function(err, rows, fields) {
			if (err) {
				__this.__self.callback([], err);
				return;
			}
			__this.resultset = rows;
			__this.__self.callback(rows, null);
		});
		return this;
		return this;
	}

	/**
	 *
	 * @param ids cached ids
	 * @return {*}
	 */
	Search.prototype.getHotelsCount = function(ids) {
		var __this = this;
		var sql = this.__sets.sql.replace(":fields:"," COUNT(DISTINCT(hotel.id)) as cnt ") + " ";
		this.__self.db.query(sql, function(err, rows, fields) {
			if (err) {
				__this.__self.callback([], err);
				return;
			}
			__this.resultset = rows;
			__this.__self.callback(rows, null);
		});
		return this;
		return this;
	}

	/**
	 *
	 * @param ids cached ids
	 * @return {*}
	 */
	Search.prototype.getHotelsMapItems = function(ids) {
		var __this = this;
		this.__self.db.query('SELECT `itemid` as id, `lat`, `lng` FROM `maps_items` WHERE itemid IN ('+ids.join(",")+") ", function(err, rows, fields) {
			if (err) {
				__this.__self.callback([], err);
				return;
			}
			__this.resultset = rows;
			__this.__self.callback(rows, null);
		});
		return this;
	}

	/**
	 *
	 * @param req
	 * @param res
	 * @param next
	 * @return {*}
	 */
	Search.prototype.test = function(req, res, next) {
		var __this = this;
		this.__self.db.query('SELECT * FROM hotel LIMIT 10', function(err, rows, fields) {
			if (err) {
				__this.__self.callback([], err);
				return;
			}
			__this.resultset = rows;
			__this.__self.callback(rows, null);
		});
		return this;
	}

	return Search;
})();

exports.search = Search;

/**
 var sql = this.__sets.sql.replace(":fields:"," COUNT(id) as cnt ") +
 " GROUP BY hotel.id " +
 " LIMIT "+this.__sets.page+","+this.__sets.perPage;
 */