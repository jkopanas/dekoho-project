/**
 * Author:      Evangelos Pappas
 * description:
 *
 */


Function.prototype.bind = function(scope) {
	var _function = this;
	return function() {
		return _function.apply(scope, arguments);
	}
}

Object.prototype.__extends = function(d, b) {
	function __() {
		this.constructor = d;
	}
	__.prototype = b.prototype;
	d.prototype = new __();
}

Object.prototype.__getClass = function() {
	var nameRegex = /function (.{1,})\(/;
	try {
		return this.constructor.toString().split(nameRegex)[1];
	} catch(e) {
		return "";
	}
}

Object.prototype.__getPropertiesKeys = function(src, target, itterate) {
	src = (src || this);
	target = (target || []);
	for(var p in src) {
		if(typeof src[p] !== "function") {
			target.push(p);
			if(src[p].constructor == Object && (itterate)) {
				this.__getPropertiesKeys(src[p], target, itterate);
			}
		}
	}
	return target;
}

Object.prototype.__getsKeys = function(src, target, itterate) {
	src = (src || this);
	target = (target || []);
	for(var p in src) {
		target.push(p);
		if(src[p].constructor == Object && (itterate)) {
			this.__getsKeys(src[p], target, itterate);
		}
	}
	return target;
}

Object.prototype.push = function(src) {
	src = (src || []);
	for(var p in src) {
		this[p] = src[p];
	}
	return this;
}

Object.prototype.__cloneProperties = function() {
	clone = {};
	for(var p in this) {
		if(typeof this[p] !== "function") {
			clone[p] = this[p];
		}
	}
	return clone;
}

Object.prototype.__toArray = function(src, target) {
	src = (src || this);
	target = (target || []);
	for(var p in src) {
		if(typeof src[p] !== "function") {
			target.push(src[p]);
		}
	}
	return target;
}

Object.prototype.bind = function(scope, fn) {
	var args = Array.prototype.slice.call(arguments, 2);
	return function () {
		return fn.apply(scope, args.concat(toArray(arguments)));
	};
}

String.prototype.mapReduce = function(arg) {
	var reg = (arg || /\W+/g);
	return this.toLowerCase().
		split(reg).
		reduce(function(mapArr, w) {
			mapArr[w] = (mapArr[w] || 0) + 1;
			return mapArr;
		}, {})
}

var StringBuilder = (function(_super) {
	_super.prototype.__extends(StringBuilder, _super);
	var _array;
	var _index;

	function StringBuilder(arg) {
		_super.call(this, arg);
		this._array = [];
		this._index = 0;
	}

	StringBuilder.prototype.append = function( /* STRING HERE */ ) {
		for(var v in arguments) {
			this._array[this._index] = v;
			this._index++;
		}
	}

	StringBuilder.prototype.toString = function() {
		return this._array.join('');
	}

	return StringBuilder;
})(Object);

var UUID = (function(_super) {
	_super.prototype.__extends(UUID, _super);

	function UUID(arg) {
		_super.call(this, arg);
	}

	UUID.prototype.toString = function() {
		return this.getUuid();
	}

	UUID.prototype.getUuid = function() {
		for(var a,b = a = ''; a++ < 36; b += a * 6.5 ? (a ^ 15 ? 8 ^ Math.random() * (a ^ 20 ? 16 : 4) : 4).toString(16) : '-');
		return b
	}
	return UUID;
})(Object);