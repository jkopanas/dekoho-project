/**
 * Author:      Evangelos Pappas
 * description: A variety of common functions & patterns
 *
 */
exports.mNode = function() {
	return (function() {
		return new require("./core.js").core();
	})();
};
