/**
 * Author:      Evangelos Pappas
 * description:
 *
 */

exports.pipeLine = (function() {
	function PipeLine() {
		this.queue = [];
		return this;
	}

	PipeLine.prototype.then = function(func) {
		this.queue.push(func);
		return this;
	}

	PipeLine.prototype.start = function() {
		this.__next.apply(this, arguments);
		return this;
	}

	PipeLine.prototype.__next = function() {
		var func = this.queue.shift();
		if (func != null) {
			var __this = this;
			(function(args) {
				this.do = function() {
					var args = this.apply(this, arguments);

					__this.__next.apply(__this, [args]);
				}
				this.do(args);
			}).apply(func, arguments);
		}
	}
	return PipeLine;
})();