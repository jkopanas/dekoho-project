/**
 * Author:      Evangelos Pappas
 * description: A variety of common functions & patterns
 *
 */
global.core = (function(args) {

	var jsSuffixRegExp = /\.js$/;
	var currDirRegExp = /^\.\//;
	var contexts = {};

	function Core() {

		return this;
	}

	/**
	 *
	 * @type {Function}
	 * @private
	 */
	Core.prototype.__extends = (function(src, trgt) {
		function __() {
			this.constructor = src;
		}
		__.prototype = trgt.prototype;
		src.prototype = new __();
		return d;
	})

	/**
	 *
	 * @param obj
	 * @return {*}
	 * @private
	 */
	Core.prototype.__getClass = function(obj) {
		var nameRegex = /function (.{1,})\(/;
		try {
			return obj.constructor.toString().split(nameRegex)[1];
		} catch(e) {
			return "";
		}
	}

	/**
	 *
	 * @type {Function}
	 * @private
	 */
	Core.prototype.clone = (function(){
		return function (trgt) {
			Clone.prototype=trgt; return new Clone()
		};
		function Clone(){}
	}());

	/**
	 *
	 * @type {Function}
	 * @private
	 */
	Core.prototype.copy = function(source, destination){
		var __this = this;
		if (!destination) {
			destination = source;
			if (source) {
				if (__this.isArray(source)) {
					destination = __this.copy(source, []);
				} else if (__this.isDate(source)) {
					destination = new Date(source.getTime());
				} else if (__this.isObject(source)) {
					destination = __this.copy(source, {});
				}
			}
		} else {
			if (source === destination) throw Error("Can't copy equivalent objects or arrays");
			if (__this.isArray(source)) {
				destination.length = 0;
				for ( var i = 0; i < source.length; i++) {
					destination.push(__this.copy(source[i]));
				}
			} else {
				__this.each(destination, function(value, key){
					delete destination[key];
				});
				for ( var key in source) {
					destination[key] = __this.copy(source[key]);
				}
			}
		}
		return destination;
	}

	/**
	 * Binds a new Function to an object
	 * @param obj
	 * @param fPTR
	 * @return {Function}
	 */
	Core.prototype.bind = function(obj, fPTR) {
		return function() {
			return fPTR.apply(obj, arguments);
		};
	}

	/**
	 * Merges two objects
	 * @param objTrgt
	 * @param ObjSrc
	 * @return {*}
	 */
	Core.prototype.merge = function(objTrgt, ObjSrc) {
		var __this = this;
		this.each(ObjSrc, function(ptr, index, ptrSrc) {
			if(isObject(ptr)) {
				if(!__this.itOwns(objTrgt[index])) {
					objTrgt[index] = {};
				}
				__this.merge(objTrgt, ptr);
			}
			objTrgt[index] = ptr;
		});
		return objTrgt;
	}

	/**
	 *
	 * @param arg
	 * @return {Object}
	 */
	Core.prototype.mapReduce = function(str,regx) {
		var reg = (regx || /\W+/g);
		return str.toLowerCase().
			split(reg).
			reduce(function(mapArr, w) {
				mapArr[w] = (mapArr[w] || 0) + 1;
				return mapArr;
			}, {})
	}

	/**
	 * a call back iteration in a collection
	 * @param obj
	 * @param fPTR
	 * @return {*}
	 */
	Core.prototype.each = function(obj, fPTR) {
		if(this.isObject(obj)) {
			return this.eachOfObject(obj, fPTR);
		}
		return this.eachOfArray(obj, fPTR);
	}

	/**
	 * Iterates in a Object-source collection
	 * @param obj
	 * @param fPTR
	 * @return {*}
	 */
	Core.prototype.eachOfObject = function(obj, fPTR) {
		for(var k in obj) {
			if(this.itOwns(obj, k) && !this.isFunction(obj[k])) {
				try {
					if(fPTR(obj[k], k, obj)) {
						break;
					}
				} catch(e) {}
			}
		}
		return this;
	}

	/**
	 * Iterates in an array Collection
	 * @param vArr
	 * @param fPTR
	 * @return {*}
	 */
	Core.prototype.eachOfArray = function(vArr, fPTR) {
		for(var i = vArr.length; i--;) {
			try {
				if(vArr[i] && fPTR(vArr[i], i, vArr)) {
					break;
				}
			} catch(e) {}
		}
		return this;
	}

	/**
	 * check if a variable/function name owns to an object
	 * @param obj
	 * @param key
	 * @return {Boolean}
	 */
	Core.prototype.itOwns = function(obj, key) {
		return Object.prototype.hasOwnProperty.call(obj, key);
	}

	/**
	 * Check if argument's a function
	 * @param obj
	 * @return {Boolean}
	 */
	Core.prototype.isFunction = function(obj) {
		return Object.prototype.toString.call(obj) === '[object Function]';
	}

	/**
	 * Check if argument's an Array
	 * @param obj
	 * @return {Boolean}
	 */
	Core.prototype.isArray = function(obj) {
		return Object.prototype.toString.call(obj) === '[object Array]';
	}

	/**
	 *
	 * @param obj
	 * @return {Boolean}
	 */
	Core.prototype.isDate = function isDate(obj) {
		return typeof obj === 'object' && Object.prototype.toString.call(obj) === '[object Date]';
	}

	/**
	 * Check if argument's an object
	 * @param obj
	 * @return {Boolean}
	 */
	Core.prototype.isObject = function(obj) {
		return Object.prototype.toString.call(obj) === '[object Object]';
	}

	/**
	 * A callback-response pattern implementation
	 *
	 * var obj = new core.event(new function() {...});
	 *
	 * @type {*}
	 */
	Core.prototype.event = (function() {
		function Event(func) {
			this.events = [];
			this.callback = func || (function(arg) {});
			return this;
		}
		Event.prototype.emit =  function(event) {
			this.events.push(event);
			return this
		}
		Event.prototype.trigger = function() {
			var args = arguments;
			var __this = this;
			var tmpEvents = this.events.slice(0);
			this.events = [];
			tmpEvents.map(function(ptr) {
				setTimeout(function(){
					__this.callback(ptr, args);
				},0);
			});
			return this;
		}
		return Event;
	})();

	/**
	 * A hook Methodology
	 * @type {*}
	 */
	Core.prototype.hook = (function() {
		var singleton = {
			hookAnchors : {
				global: {}
			},
			hookObservers : {
				global: {}
			},
			hookObservables : {
				global: {}
			},
			resourcePool : {
				global: {}
			},
			errorLog : []
		};

		function HookParent() {
			this.hookAnchors        = singleton.hookAnchors;
			this.hookObservers      = singleton.hookObservers;
			this.hookObservables    = singleton.hookObservables;
			this.resourcePool       = singleton.resourcePool;
			this.errorLog           = singleton.errorLog;
			return this;
		}

		HookParent.prototype.setScope =  function(scope) {
			if(!this.hookAnchors.hasOwnProperty(scope)) {
				this.hookAnchors[scope] = {};
			}
			if(!this.resourcePool.hasOwnProperty(scope)) {
				this.resourcePool[scope] = {};
			}
			return this;
		}

		HookParent.prototype.doCacheResource = function(scope, resourceKey, resource) {
			this.setScope(scope);
			this.resourcePool[scope][resourceKey] = resource;
			return this;
		}

		HookParent.prototype.doCacheResourceGlobally = function(resourceKey, resource) {
			this.doCacheResource("global", resourceKey, resource);
			return this;
		}

		HookParent.prototype.doFetchResource = function(scope, resourceKey) {
			if(this.resourcePool[scope].hasOwnProperty(resourceKey)) {
				return this.resourcePool[scope][resourceKey];
			}
			return null;
		}

		HookParent.prototype.doFetchResourceGlobally = function(resourceKey) {
			return this.doFetchResource('global', resourceKey);
		}

		HookParent.prototype.doUnCacheResource = function(scope, resourceKey) {
			delete this.resourcePool[scope][resourceKey];
			return this;
		}

		HookParent.prototype.doUnCacheResourceGlobally = function(resourceKey) {
			return this.doUnCacheResource('global', resourceKey);
		}

		HookParent.prototype.doBindHook = function(scope, hookKey, hookVal) {
			this.setScope(scope);
			this.hookAnchors[scope][hookKey] = hookVal;
			return this;
		}

		HookParent.prototype.doBindHookGlobally = function(hookKey, hookVal) {
			return this.doBindHook('global', hookKey, hookVal);
		}

		HookParent.prototype.doUnBindHook = function(scope, hookKey) {
			delete this.hookAnchors[scope][hookKey];
			return this;
		}

		HookParent.prototype.doUnBindHookGlobally = function(hookKey) {
			return this.doUnBindHook('global', hookKey);
		}

		HookParent.prototype.doTriggerHook = function(scope, hookKey, args) {
			try {
				return this.hookAnchors[scope][hookKey](args);
			} catch(e) {
				this.errorLog.push(e);
			}
			return null;
		}

		HookParent.prototype.doTriggerHookGlobally = function(hookKey, args) {
			return this.doTriggerHook('global', hookKey, args);
		}

		return HookParent;
	})()

	/**
	 *
	 * @type {Function}
	 */
	Core.prototype.cache = (function() {

		/**
		 *
		 * @param args
		 * @return {*}
		 * @constructor
		 */
		function Cache(args) {
			this.store = args.store;
			return this;
		}

		/**
		 *
		 * @param key
		 * @param def
		 * @return {*}
		 */
		Cache.prototype.get = function(key,callback) {
			var tmp = this.store.get(key, function(rows,err) {
				callback(rows,err);
			});
			return this;
		}

		/**
		 *
		 * @param key
		 * @param val
		 * @param expires
		 * @return {*}
		 */
		Cache.prototype.put = function(key,val,expires,callback) {
			this.store.set(key,val,expires,callback);
			return this;
		}

		/**
		 *
		 * @param key
		 * @return {*}
		 */
		Cache.prototype.free = function(key,callback) {
			this.store.free(key,callback);
			return this;
		}

		/**
		 *
		 * @return {*}
		 */
		Cache.prototype.getMap = function(callback) {
			if(callback) {
				callback(arguments);
			}
			return this;
		}

		/**
		 *
		 * @return {*}
		 */
		Cache.prototype.collectExpired = function(callback) {
			this.store.collectExpired(callback);
			return this;
		}

		return Cache;
	})();


	/**
	 *
	 * @type {*}
	 */
	Core.prototype.memoryStore = (function() {

		/**
		 *
		 * @param conn
		 * @return {*}
		 * @constructor
		 */
		function MemoryStore(map) {
			this.map = map || {};
			this.expirationMap = {};
			this.lastID = "";
			this.expiration = 3600000 // 1hour
			return this;
		}

		/**
		 *
		 * @param key
		 * @param val
		 * @return {*}
		 */
		MemoryStore.prototype.set = function(key,val,expires,callback) {
			this.map[key] = val;
			this.expirationMap[key] = Date.now()+(expires?expires:this.expiration);
			this.lastID = key;
			if(callback) {
				callback(this.lastID,{},arguments);
			}
			return this;
		}

		/**
		 *
		 * @param key
		 * @return {*}
		 */
		MemoryStore.prototype.get = function(key, callback) {
			if(typeof this.map[key] !== "undefined" && !(this.expirationMap[key] > 0 && this.expirationMap[key] < Date.now())) {
				callback(this.map[key],null);
			}
			else {
				callback([],["ERROR: "+key+" is Expired or Not exists"]);
			}
			return this;
		}

		/**
		 *
		 * @param key
		 * @return {*}
		 */
		MemoryStore.prototype.free = function(key,callback) {
			delete this.map[key];
			if(callback) {
				callback(arguments);
			}
			return this;
		}

		/**
		 *
		 * @return {*}
		 */
		MemoryStore.prototype.collectExpired = function(callback) {
			var now = Date.now();
			for(var key in this.map) {
				if(!(this.expirationMap[key] > 0 && this.expirationMap[key] < Date.now())) {
					delete this.map[key];
				}
			}
			if(callback) {
				callback(arguments);
			}
			return this;
		}

		return MemoryStore;
	})();

	/**
	 *
	 * @type {*}
	 */
	Core.prototype.mysqlStore = (function() {

		/**
		 *
		 * @param conn
		 * @return {*}
		 * @constructor
		 */
		function MySQLStore(conn) {
			this.conn = conn;
			this.lastID = 0;
			this.erros = [];
			this.expiration = 3600000 // 1hour
			this.table = "mNode_store";
			return this;
		}

		/**
		 *
		 * @param key
		 * @param val
		 * @return {*}
		 */
		MySQLStore.prototype.insert = function(val, args, callback) {
			var __this = this;
			this.conn.query("INSERT INTO `"+this.table+"` SET ? ", val, function(err, res) {
				if (err) {
					__this.erros.push(err);
					__this.update(val, args, callback);
					return;
				}
				__this.lastID = res.insertId;
				if(callback) {
					callback(__this.lastID,err,arguments);
				}
			});
			return this;
		}

		/**
		 *
		 * @param key
		 * @param val
		 * @return {*}
		 */
		MySQLStore.prototype.update = function(val, args, callback) {
			var __this = this;
			this.conn.query("UPDATE `"+this.table+"` SET ? WHERE "+args+" ",val, function(err, res) {
				if (err) {
					__this.erros.push(err);
					if(callback) {
						callback(0,err,arguments);
					}
					throw err;
				}
				__this.lastID = res.insertId;
				if(callback) {
					callback(__this.lastID,err,arguments);
				}
			});
			return this;
		}

		/**
		 *
		 * @param key
		 * @param val
		 * @return {*}
		 */
		MySQLStore.prototype.set = function(key,val,expires,callback) {
			var __this = this;
			try {
				this.insert({
					key:key,
					scope:"global",
					value:JSON.stringify(val),
					modified:Date.now(),
					expire:Date.now()+(expires?expires:__this.expiration),
					active:1
				}," `key` = '"+key+"' ",callback);
			}
			catch(e) {
				this.update({
					key:key,
					scope:"global",
					value:JSON.stringify(val),
					modified:Date.now(),
					expire:Date.now()+(expires?expires:__this.expiration),
					active:1
				}," `key` = '"+key+"' ",callback);
			}
			return this;
		}

		/**
		 *
		 * @param key
		 * @return {*}
		 */
		MySQLStore.prototype.select = function(args, callback) {
			var locRes = [];
			var __this = this;
			this.conn.query("SELECT * FROM `"+this.table+"` "+(args? " WHERE "+args:""), function(err, rows, fields) {
				if (err) {
					__this.erros.push(err);
					callback([],err);
					return;
				}
				locRes = rows;
				callback(rows,null);
			});

			//while(locRes.length==0);

			return this;
		}

		/**
		 *
		 * @param key
		 * @return {*}
		 */
		MySQLStore.prototype.get = function(key, callback) {
			var args = " `key` = '"+key+"' ";
			return this.select(args, callback);
		}

		/**
		 *
		 * @param key
		 * @return {*}
		 */
		MySQLStore.prototype.delete = function(args,callback) {
			var __this = this;
			this.conn.query('DELETE FROM `'+this.table+'` WHERE '+args, function(err, res) {
				if (err) {
					__this.erros.push(err);
				}
				if(callback) {
					callback(0,err,arguments);
				}
			});
			return this;
		}

		/**
		 *
		 * @param key
		 * @return {*}
		 */
		MySQLStore.prototype.free = function(key,callback) {
			var args = " `key` = '"+key+"' ";
			return this.delete(args,callback);
		}

		/**
		 *
		 * @param key
		 * @return {*}
		 */
		MySQLStore.prototype.inactivate = function(key,callback) {
			var args = " `key` = '"+key+"' ";
			var __this = this;
			this.conn.query('INSERT INTO `'+this.table+'` SET ? WHERE `key` = "'+key+"'", {active: 0}, function(err, res) {
				if (err) {
					__this.erros.push(err);
				}
				__this.lastID = res.insertId;
				if(callback) {
					callback(__this.lastID,err,arguments);
				}
			});
			return this;
		}

		/**
		 *
		 * @return {*}
		 */
		MySQLStore.prototype.collectExpired = function(callback) {
			var args = "( `expire` < "+Date.now()+" AND `expire` != 0 ) OR active = 0 ";
			return this.delete(args,callback);
		}

		return MySQLStore;
	})();

	/**
	 *
	 * @type {*}
	 */
	Core.prototype.stringBuilder = (function() {

		function StringBuilder() {
			this._array = [];
			this._index = 0;
			return this;
		}

		StringBuilder.prototype.append = function( /* STRING HERE */ ) {
			for(var v in arguments) {
				this._array[this._index] = v;
				this._index++;
			}
			return this;
		}

		StringBuilder.prototype.toString = function() {
			return this._array.join('');
		}

		return StringBuilder;
	})();

	/**
	 *
	 * @type {*}
	 */
	Core.prototype.uuid = (function() {

		function UUID(arg) {

			return this;
		}

		UUID.prototype.toString = function() {
			return this.getUuid();
		}

		UUID.prototype.getUuid = function() {
			for(var i = 0,a ,b = a = ''; ++i < 36; b += (a += i) * 6.5 ? (a ^ 15 ? 8 ^ Math.random() * (a ^ 20 ? 16 : 4) : 4).toString(16) : '-');
			return b
		}
		return UUID;
	})();

	return Core;
})((args = {}));

/**
 CREATE TABLE `mNode_store` (
 `id`  int NOT NULL AUTO_INCREMENT ,
 `key`  varchar(50) NULL ,
 `scope`  varchar(10) NULL ,
 `value`  text NULL ,
 `modified`  int(13) NULL ,
 `expire`  int(13) ZEROFILL NULL ,
 `active`  tinyint(1) ZEROFILL NULL ,
 PRIMARY KEY (`id`),
 UNIQUE INDEX `mNode_store_key` (`key`) USING HASH ,
 INDEX `mNode_store_key_scope` (`key`, `scope`) USING HASH
 )
 */