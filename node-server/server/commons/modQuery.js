/**
 * Author:      Evangelos Pappas
 * description:
 *
 */

/**
 *
 * @type {*}
 */
var modQuery = (function() {

	var __self = {
		token: {},
		db: {},
		mNode: {},
		callback: function(row, errors) {
			return {}
		}
	};

	var __qMods = {};

	/**
	 *
	 * @param args
	 * @return {*}
	 * @constructor
	 */

	function ModQuery(args) {
		this.__self = {
			db: args.db,
			mNode: args.mNode,
			callback: args.callback ? args.callback : function() {},
			conf: args.conf
		}
		this.__qMods = this.__self.conf.sql;
		this.isBuilt = false;
		this.sql = "";
		this.wizzard = {
			fields: [],
			target: {},
			targetName: "",
			joins: [],
			filters: [],
			groupBy: [],
			sortBy: [],
			limit: []
		};
		this.resultset = {};
		return this;
	}

	/**
	 *
	 * @return {ModQuery}
	 */
	ModQuery.prototype.newModQuery = function() {
		return new ModQuery(this.__self);
	}

	/**
	 *
	 * @return {*}
	 */
	ModQuery.prototype.getSQL = function() {
		return this.sql;
	}

	/**
	 *
	 * @return {*}
	 */
	ModQuery.prototype.clear = function() {
		this.isBuilt = false;
		this.sql = "";
		this.wizzard = {
			fields: [],
			target: {},
			targetName: "",
			joins: [],
			filters: [],
			groupBy: [],
			sortBy: [],
			limit: []
		};
		this.resultset = {};
		return this;
	}

	/**
	 *
	 * @return {*}
	 */
	ModQuery.prototype.addParallel = function(keepFilters) {
		this.build();
		this.isBuilt = false;
		var tmpFilts = [];
		var tmpJoins = [];
		if(keepFilters) {
			tmpFilts = this.wizzard.filters;
			tmpJoins = this.wizzard.joins;
		}
		this.wizzard = {
			fields: [],
			target: {},
			targetName: "",
			joins: tmpJoins,
			filters: tmpFilts,
			groupBy: [],
			sortBy: [],
			limit: []
		};
		return this;
	}

	/**
	 *
	 * @return {*}
	 */
	ModQuery.prototype.execute = function(func) {
		if(!this.isBuilt) this.build();

		if(typeof func !== "undefined") {
			this.__self.callback = func;
		}

		var __this = this;
		this.__self.db.query(this.sql, function(err, rows, fields) {
			setTimeout(function(sql, err, rows){
				if(err) {
					__this.__self.callback([], err, sql);
					return;
				}
				__this.resultset = rows;
				__this.__self.callback(rows, null, sql);
			},0,this.sql,err,rows);
		});
		return this;
	};

	/**
	 *
	 * @return {*}
	 */
	ModQuery.prototype.executeSQL = function(sql, func) {
		if(typeof func !== "undefined") {
			this.__self.callback = func;
		}

		var __this = this;
		this.__self.db.query(sql, function(err, rows, fields) {
			setTimeout(function(sql, err, rows){
				if(err) {
					__this.__self.callback([], err, sql);
					return;
				}
				__this.resultset = rows;
				__this.__self.callback(rows, null, sql);
			},0,sql,err,rows);
		});
		return this;
	};

	/**
	 *
	 * @return {*}
	 */
	ModQuery.prototype.build = function() {
		if(this.isBuilt) throw "ModQuery is Built";
		this.sql += " SELECT "
			+ (this.wizzard.fields.length > 0 ? this.wizzard.fields.join(", ") : " * ") + " "
			+ " FROM `" + this.wizzard.targetName + "` "
			+ (this.wizzard.joins.length > 0 ? this.wizzard.joins.join(" ") + "" : "")
			+ (this.wizzard.filters.length > 0 ? " WHERE ( " + this.wizzard.filters.join(") AND (") + " )" : "")
			+ (this.wizzard.groupBy.length > 0 ? " GROUP BY " + this.wizzard.groupBy.join(",") + " " : "")
			+ (this.wizzard.sortBy.length > 0 ? " ORDER BY " + this.wizzard.sortBy.join(",") + " " : "")
			+ (this.wizzard.limit.length > 0 ? " LIMIT " + this.wizzard.limit.join(",") + " " : "") + ";";

		this.isBuilt = true;

		return this;
	};

	/**
	 *
	 * @return {*}
	 */
	ModQuery.prototype.from = function(module) {
		if(this.isBuilt) throw "ModQuery is Built";
		if(typeof this.__qMods[module] !== undefined) {
			this.wizzard.target = this.__qMods[module];
			this.wizzard.targetName = module;
		} else {
			this.wizzard.target = module;
			this.wizzard.targetName = module;
		}
		return this;
	};

	/**
	 *
	 * @param fields
	 * @return {*}
	 */
	ModQuery.prototype.select = function(fields) {
		if(this.isBuilt) throw "ModQuery is Built";
		var fArr = [];
		if(typeof fields !== "object") {
			fArr.push(fields);
		} else {
			fArr = fields;
		}

		for(var i in fArr) {
			if(typeof this.wizzard.target !== "undefined" && typeof this.wizzard.target.fields[fArr[i]] !== "undefined") {
				this.wizzard.fields.push(this.wizzard.target.fields[fArr[i]].select);
			} else {
				this.wizzard.fields.push(fArr[i]);
			}
		}
		return this;
	};

	/**
	 *
	 * @param fields
	 * @return {*}
	 */
	ModQuery.prototype.groupBy = function(fields) {
		if(this.isBuilt) throw "ModQuery is Built";
		var fArr = [];
		if(typeof fields !== "object") {
			fArr.push(fields);
		} else {
			fArr = fields;
		}
		this.wizzard.groupBy = this.wizzard.groupBy.concat(fArr);
		return this;
	};

	/**
	 *
	 * @param fields
	 * @return {*}
	 */
	ModQuery.prototype.sortBy = function(fields) {
		if(this.isBuilt) throw "ModQuery is Built";
		var fArr = [];
		if(typeof fields !== "object") {
			fArr.push(fields);
		} else {
			fArr = fields;
		}
		this.wizzard.sortBy = this.wizzard.sortBy.concat(fArr);
		return this;
	};

	/**
	 *
	 * @param fields
	 * @return {*}
	 */
	ModQuery.prototype.limit = function(page, limit) {
		if(this.isBuilt) throw "ModQuery is Built";
		if(typeof limit !== "undefined") {
			this.wizzard.limit = [page, limit];
		} else {
			this.wizzard.limit = [page];
		}
		return this;
	};

	/**
	 *
	 * @param joinSQL
	 * @return {*}
	 */
	ModQuery.prototype.joinSQL = function(jSQL) {
		if(this.isBuilt) throw "ModQuery is Built";
		var __this = this;
		function recurseJoins(mod, wizz) {
			if(typeof wizz.target !== "undefined" && typeof wizz.target.joins[mod] !== "undefined") {
				if(typeof wizz.target.joins[mod].intermediate !== "undefined") {
					recurseJoins(wizz.target.joins[mod].intermediate, wizz);
					wizz.joins.push(wizz.target.joins[mod].sql);
				} else {
					wizz.joins.push(wizz.target.joins[mod].sql);
				}
			}
			else if(typeof mod !== "undefined") {
				wizz.joins.push(mod);
			}
			return __this;
		}

		return recurseJoins(jSQL, this.wizzard);
	};

	/**
	 *
	 * @return {*}
	 */
	ModQuery.prototype.filterBy = function(module, field) {
		if(this.isBuilt) throw "ModQuery is Built";
		var __this = this;

		function recurseJoins(mod, wizz) {
			if(typeof wizz.target !== "undefined" && typeof wizz.target.joins[mod] !== "undefined") {
				if(typeof wizz.target.joins[mod].intermediate !== "undefined") {
					recurseJoins(wizz.target.joins[mod].intermediate, wizz);
					wizz.joins.push(wizz.target.joins[mod].sql);
				} else {
					wizz.joins.push(wizz.target.joins[mod].sql);
				}
			}
			else if(typeof mod !== "undefined" && wizz.targetName !== mod) {
				wizz.joins.push(mod);
			}
			return __this;
		}

		recurseJoins(module, this.wizzard);

		return(new __filter("`" + module + "`.`" + field + "`", false, this, this.wizzard));
	};

	/**
	 *
	 * @return {*}
	 */
	ModQuery.prototype.filterBySQL = function(sql) {
		if(this.isBuilt) throw "ModQuery is Built";
		return(new __filter(sql, true, this, this.wizzard));
	};

	var __filter = (function() {

		/**
		 *
		 * @param f
		 * @return {*}
		 * @constructor
		 */

		function Filter(f, isPureSQL, modQ, wizz) {
			this.__modQ = modQ;
			this.__wizz = wizz;
			if(isPureSQL) {
				this.field = "";
				this.sql = f;
				this.opperator = "";
				this.filtVal = "";
				this.__wizz.filters.push(this.sql);
				return this.__modQ;
			}

			this.field = f;
			this.sql = "";
			this.opperator = "";
			this.filtVal = "";

			return this;
		}

		/**
		 *
		 * @param arg
		 * @return {*}
		 */
		Filter.prototype. in = function(arg) {

			var tmp = [];
			if(typeof arg !== "object") {
				tmp.push(arg);
			} else {
				tmp = arg;
			}

			this.opperator = "IN";
			//this.filtVal = (typeof tmp[0] === "string" ? "('" + tmp.join("','") + "')" : "(" + tmp.join(",") + ")");
			this.filtVal = "(" + tmp.join(",") + ")";
			this.sql = " " + this.field + " " + this.opperator + " " + this.filtVal + " ";
			this.__wizz.filters.push(this.sql);
			return this.__modQ;
		}

		/**
		 *
		 * @param arg
		 * @return {*}
		 */
		Filter.prototype.regex = function(arg) {
			this.opperator = "REGEXP";
			this.filtVal = arg;
			this.sql = " " + this.field + " " + this.opperator + " " + this.filtVal + " ";
			this.__wizz.filters.push(this.sql);

			return this.__modQ;
		}

		/**
		 *
		 * @param arg
		 * @return {*}
		 */
		Filter.prototype.equals = function(arg) {
			this.opperator = "=";
			this.filtVal = (typeof arg === "string" ? "'" + arg + "'" : arg);
			this.sql = " " + this.field + " " + this.opperator + " " + this.filtVal + " ";
			this.__wizz.filters.push(this.sql);

			return this.__modQ;
		}

		/**
		 *
		 * @param arg
		 * @return {*}
		 */
		Filter.prototype.notEquals = function(arg) {
			this.opperator = "!=";
			this.filtVal = (typeof tmp[0] === "string" ? "'" + arg + "'" : arg);
			this.sql = " " + this.field + " " + this.opperator + " " + this.filtVal + " ";
			this.__wizz.filters.push(this.sql);

			return this.__modQ;
		}

		/**
		 *
		 * @param arg
		 * @return {*}
		 */
		Filter.prototype.contains = function(arg) {
			this.opperator = "LIKE";
			this.filtVal = "'%" + arg + "%'";
			this.sql = " " + this.field + " " + this.opperator + " " + this.filtVal + " ";
			this.__wizz.filters.push(this.sql);

			return this.__modQ;
		}

		/**
		 *
		 * @param arg
		 * @return {*}
		 */
		Filter.prototype.startsWith = function(arg) {
			this.opperator = "LIKE";
			this.filtVal = "'%" + arg + "'";
			this.sql = " " + this.field + " " + this.opperator + " " + this.filtVal + " ";
			this.__wizz.filters.push(this.sql);

			return this.__modQ;
		}

		/**
		 *
		 * @param arg
		 * @return {*}
		 */
		Filter.prototype.endsWith = function(arg) {
			this.opperator = "LIKE";
			this.filtVal = "'" + arg + "%'";
			this.sql = " " + this.field + " " + this.opperator + " " + this.filtVal + " ";
			this.__wizz.filters.push(this.sql);

			return this.__modQ;
		}

		/**
		 *
		 * @param arg
		 * @return {*}
		 */
		Filter.prototype.lessThan = function(arg) {
			this.opperator = " < ";
			this.filtVal = arg;
			this.sql = " " + this.field + " " + this.opperator + " " + this.filtVal + " ";
			this.__wizz.filters.push(this.sql);

			return this.__modQ;
		}

		/**
		 *
		 * @param arg
		 * @return {*}
		 */
		Filter.prototype.lessThanEquals = function(arg) {
			this.opperator = " <= ";
			this.filtVal = arg;
			this.sql = " " + this.field + " " + this.opperator + " " + this.filtVal + " ";
			this.__wizz.filters.push(this.sql);

			return this.__modQ;
		}

		/**
		 *
		 * @param arg
		 * @return {*}
		 */
		Filter.prototype.greaterThan = function(arg) {
			this.opperator = " > ";
			this.filtVal = arg;
			this.sql = " " + this.field + " " + this.opperator + " " + this.filtVal + " ";
			this.__wizz.filters.push(this.sql);

			return this.__modQ;
		}

		/**
		 *
		 * @param arg
		 * @return {*}
		 */
		Filter.prototype.greaterThanEquals = function(arg) {
			this.opperator = " >= ";
			this.filtVal = arg;
			this.sql = " " + this.field + " " + this.opperator + " " + this.filtVal + " ";
			this.__wizz.filters.push(this.sql);

			return this.__modQ;
		}

		return Filter;
	})();

	return ModQuery;
})();

global.modQuery = modQuery;