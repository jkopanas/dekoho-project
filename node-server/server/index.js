//debugger;
var events = require('events');
var conf = require("./conf/conf.js").conf();
require("./commons/core.js");
require("./commons/modQuery.js");
var mNode = new global.core();
var PipeLine = require("./commons/pipeLine.js").pipeLine;
var express = require('express');
var app = express();
var mysql = require('mysql');
var mongo = {};
var tmpConf = require("./conf/config.json");
for (var prop in tmpConf) {
	conf[prop] = tmpConf[prop];
}
//require('nodetime').profile({
//	accountKey: 'e884389cc79e95acbba87557f91dc47b9eb8ba92',
//	appName: 'dekoho'
//});
//---Mongo Initiation
//require('mongodb').Db.connect("mongodb://localhost:27017/dekoho", function(err, db) {
//	if(err){
//		console.log(err)
//		return;
//	}
//	global.mongo = mongo = db;
////	mongo.collection('maps_items').find({id:{$in:[1,2]}}).toArray(function(err, items) {
////		if( err || !items){
////			console.log("No results");
////			return;
////		}
////		items.map( function(i) {
////			console.log(i);
////		});
////	});
//	console.log("MongoDB: Connected!");
//});

//---MySQL
global.db = mysql.createConnection(conf.mysql);

global.db.options = {
	MPREFIX: ''
};
global.db.connect(); // Persistent Connection to DB
//----

// Our URL
global.appUrl = app.url = conf.app.url;

//---InMemory & DB Caches
// BD-Store Cache, table->mnode_store
var dbCache = new mNode.cache({
	store: new mNode.mysqlStore(global.db)
});
// InMemory-store Cache
var memoryCache = new mNode.cache({
	store: new mNode.memoryStore()
});
//---

// a map that pairs module types with their intermediate
var intermediateMap = {
	types          : "hotel_types",
	services       : "hotel_services",
	facilities     : "hotel_facilities",
	themeshotels   : "hotel_themeshotels",
	roomsfacilities: "hotel_roomfacilities"
};
var intermediateMapReversed = {
	hotel_types         : "types",
	hotel_services      : "services",
	hotel_facilities    : "facilities",
	hotel_themeshotels  : "themeshotels",
	hotel_roomfacilities: "roomsfacilities"
};
// a map that pairs module types with their intermediate
var inTableMap = {
	stars: "stars",
	title: "title"
};
//---
// in which filters should their count be ignored
var ignoreCounts = ["stars", "title"];
//---

var orderingIds = {
	port : [297],
	train: [299],
	plane: [296]
};


app.configure(function () {
	app.use(express.static(__dirname + '/public'));
	app.use(express.methodOverride());
	app.use(express.bodyParser());
	app.use(express.logger('dev'));
	app.use(express.cookieParser('secret'));
	app.use(express.cookieSession({
		secret: 'mySecret'
	}));
	app.use(express.session({
		maxAge: new Date(Date.now() + 3600000)
	}));
	app.use(app.router)
});

app.listen(conf.port);
__reCacheMFields(null, function () {
	console.log("Listening @" + conf.port);
	console.log("Filters are cached!");
});

/* SOLVES THE CORS PROBLEM */
app.all('/*', function (req, res, next) {
	var env = (conf.app.dev == true) ? conf.app.devUrl : app.url;
	res.header("Access-Control-Allow-Origin", env);
	res.header("Access-Control-Allow-Headers", 'Content-Type, X-Requested-With');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
	res.header('Access-Control-Allow-Credentials', 'true');
	next();
});

/**
 * handle OPTIONS requests from the browser
 */
app.options("*", function (req, res, next) {
	res.send(200);
});

/**
 * An API call to flush the cache of MFields
 */
app.get("/flush", function (req, res, next) {
	__reCacheMFields();
	res.send(200);
	res.end();
});

/**
 * A token Free Call-Handler.
 * Aggregates a parallel query to multiple Tables related with Geo-Suggestion
 */
app.post('/suggest', function (req, res, next) {
	var modQuery = new global.modQuery({
		db      : global.db,
		mNode   : mNode,
		conf    : conf,
		callback: function (rows, err, sql) {
			var token = {
				countries: (rows[0] || []).concat(rows[3]),
				cities   : (rows[1] || []).concat(rows[4]),
				regions  : (rows[2] || []).concat(rows[5])
			};

			res.send(token);
			res.end();
		}
	}).newModQuery() //
		.from("geo_country") //
		.select(['id', 'title', ' "country" as module ']) //
		.filterBy("geo_country", "title") //
		.contains(req.body.text) //
		.addParallel() //
		.from("geo_cityr") //
		.select(['id', 'title', ' "city" as module ']) //
		.filterBy("geo_cityr", "title").contains(req.body.text) //
		.addParallel() //
		.from("geo_region").select(['id', 'title', ' "region" as module ']) //
		.filterBy("geo_region", "title").contains(req.body.text) //
		.addParallel() //
		.from("translations").select([' `itemid` as id ', ' `translation` as title ', ' "country" as module ']) //
		.filterBySQL(" `table` = 'geo_country' AND field = 'title' ") //
		.filterBySQL(" `translation` LIKE '%" + req.body.text + "%' ") //
		.addParallel() //
		.from("translations").select([' `itemid` as id ', ' `translation` as title ', ' "city" as module ']) //
		.filterBySQL(" `table` = 'geo_cityr' AND field = 'title' ") //
		.filterBySQL(" `translation` LIKE '%" + req.body.text + "%' ") //
		.addParallel() //
		.from("translations").select([' `itemid` as id ', ' `translation` as title ', ' "region" as module ']) //
		.filterBySQL(" `table` = 'geo_region' AND field = 'title' ") //
		.filterBySQL(" `translation` LIKE '%" + req.body.text + "%' ") //
		.build() //
		.execute(); //
});

/**
 * A token Free Call-Handler.
 * Randomly selects a destination
 */
app.post('/roulette', function (req, res, next) {
	var tmpArr = [];
	tmpArr.push({module: "geo_cityr", alias: "city", relField: "cityid"});
	tmpArr.push({module: "geo_region", alias: "region", relField: "regionid"});
	var rand = tmpArr[Math.floor(Math.random() * tmpArr.length)];

	var modQuery = new global.modQuery({
		db      : global.db,
		mNode   : mNode,
		conf    : conf,
		callback: function (rows, err, sql) {
			if (err) {
				console.log(sql);
				console.log(err);
			}
			res.send(rows[0]);
			res.end();
		}
	}).newModQuery() //
		.executeSQL("" +
			"SELECT " + rand.module + ".id, COALESCE(translations.translation, " + rand.module + ".title) as destination, '" + rand.alias + "' as destinationType " +
			"FROM " + rand.module + " " +
			"JOIN geo_relations ON " + rand.module + ".id = geo_relations." + rand.relField + " " +
			"LEFT JOIN translations ON translations.itemid = " + rand.module + ".id AND translations.`table` = '" + rand.module + "' AND translations.field = 'title' " +
			"ORDER BY RAND() " +
			"LIMIT 1"); //
});

/**
 * a Call-handler taking place just at the initiation of the Session token
 */
app.post('/boot', function (req, res, next) {
	var filters = {
		lang      : (req.body["lang"] ? req.body["lang"] : conf.defaultLang),
		region    : (req.body["destination"] ? req.body["destination"] : ""),
		regionType: (req.body["destinationType"] ? req.body["destinationType"] : ""),
		regionKey : (req.body["destinationID"] ? req.body["destinationID"] : ""),
		rooms     : [],
		//days: (req.body["arrival"]&&req.body["departure"]?__fD(req.body["departure"])-__fD(req.body["arrival"]):10),
		//budgetMin: (req.body["budget_from"]?req.body["budget_from"]:0),
		//budgetMax: (req.body["budget_to"]?req.body["budget_to"]:10000),
//		budgetDaily: (
//			(req.body["budget_to"] ? req.body["budget_to"] : 1000)
//				- (req.body["budget_from"] ? req.body["budget_from"] : 0))
//			/ (req.body["arrival"] && req.body["departure"] ? __fD(req.body["departure"]) - __fD(req.body["arrival"]) : 10),
		id        : []
	};
	// new tokenKey Generation
	var tokenKey = new mNode.uuid().getUuid();
	// aggregates the rooms so they may be handled and cached
//	if(req.body.roomsNum) {
//		for(var i = 0; i < req.body.roomsNum; ++i) {
//			filters.rooms.push(req.body["persons-" + i]);
//		}
//	}
	// Generating the data that should be cached by this session
	__generateDataSessionToken(tokenKey, filters, function (tk, fltArr, rows, err, sql) {
		if (rows.length > 0) {
			fltArr.zoomLevel = rows[0].zoomLevel;
			fltArr.lat = rows[0].lat;
			fltArr.lng = rows[0].lng;
			for (var i = rows.length; i--;) {
				fltArr.id.push(rows[i].id);
			}
		}
		dbCache.put(tk, fltArr); // new token Generation
	});
	// Get filters cached by the memory
	memoryCache.get("mfields", function (obj, err) {
		if (err && obj.length == 0) {
			__reCacheMFields(filters.lang, function () {
				memoryCache.get("mfields", function (obj, err) {
					doProceed(obj, err);
				});
			});
		}
		else {
			doProceed(obj, err);
		}
		function doProceed(obj, err) {
			var mfTmp = obj[filters.lang] || obj[conf.defaultLang];
			var tmpObj = {
				token  : tokenKey,
				fields : mfTmp,
				mapInfo: {
					lat      : filters.lat,
					lng      : filters.lng,
					zoomLevel: filters.zoomLevel
				},
				map    : conf.tables,
				err    : err
			};
			res.send(tmpObj);
			res.end();
		}
	});
});

app.post('/status', function (req, res, next) {
	res.send({state: "running"});
	res.end();
});
app.get('/status', function (req, res, next) {
	res.send({state: "running"});
	res.end();
});

/**
 * Call-Handler to match geo-locations to the requested IDs
 */
app.post('/map', function (req, res, next) {
	// TODO -> Mongo this, yet it still performs well enough
	var modQuery = new global.modQuery({
		db      : global.db,
		mNode   : mNode,
		conf    : conf,
		callback: function (rows, err, sql) {
			var $mapDetails = {
				lat : [],
				lng : [],
				zoom: []
			};
			for (var a in rows) {
				//rows[a].key = rows[a].id
				//rows[a].id = a+"_"+rows[a].id
				$mapDetails['lat'].push(rows[a].lat);
				$mapDetails['lng'].push(rows[a].lng);
				$mapDetails['zoom'].push(rows[a].zoomLevel);
			}

			var token = {
				items: rows,
				count: rows.length,
				map  : {
					lat : Math.min.apply(Math, $mapDetails['lat']),
					lng : Math.min.apply(Math, $mapDetails['lng']),
					zoom: Math.min.apply(Math, $mapDetails['zoom'])
				}
			};
			res.send(token);
			res.end();
		}
	}).newModQuery() //
		.from("maps_items") //
		.select(['`itemid` as id', 'lat', 'lng', 'zoomLevel', 'MapTypeId']) //
		.filterBy("maps_items", "itemid").in((req.body.ids || [0])) //
		.filterBy("maps_items", "module").equals("hotel") //
		//.sortBy("FIELD(itemid,"+req.body.ids.join(",")+")") //
		.build() //
		.execute(); //
});

/**
 * Call-Handler for the whole Search mechanism
 */
app.post('/search', function (req, res, next) {
	var modQuery = new global.modQuery({
		db      : global.db,
		mNode   : mNode,
		conf    : conf,
		callback: function () {
			console.log(arguments);
		}
	});
	console.log(JSON.stringify(req.body));

	var tokenKey = req.body.token;//"EXPERIMENTAL"; //
	if (tokenKey && tokenKey.length == 0) { // die if not valid
		res.send(404); // Error for no token
		return;
	}

	var token = {
		token       : tokenKey,
		count       : 0,
		data        : [],
		page        : (req.body.filters.page || 1),
		perPage     : (req.body.perPage || conf.perPage),
		dayLength   : 0,
		exchange    : 0,
		budget      : 0,
		fields      : [],
		targetRegion: {}
	};

	var orderedBy = (req.body.order || {});

	var filters = (req.body.filters || {});
	filters.nearby = (filters["nearby"] ? filters["nearby"] : 0);
	filters.lang = (filters["lang"] ? filters["lang"] : conf.defaultLang);
	filters.region = (filters["destination"] ? filters["destination"] : "");
	filters.regionType = (filters["destinationType"] ? filters["destinationType"] : "");
	filters.regionKey = (filters["destinationID"] ? filters["destinationID"] : "");
	filters.rooms = [];
	filters.roomsNum = (filters["roomsNum"] ? filters["roomsNum"] : 0);
	filters.departure = __fD(filters["departure"]);
	filters.arrival = __fD(filters["arrival"]);
	filters.dayLength = (filters["arrival"] && filters["departure"] ? Math.round((filters.departure - filters.arrival) / 86400000) : 1);
	filters.exchange = (filters["exchange"] ? parseFloat(filters["exchange"]) : 1);
	filters.budgetDaily = {
		min: (!!filters["budget_from"] ? Math.round((filters["budget_from"] / filters.exchange) / filters.dayLength) : 0),
		max: (!!filters["budget_to"] ? Math.round((filters["budget_to"] / filters.exchange) / filters.dayLength) : 0)
	};
	filters.orderBy = (orderedBy.field || "price");
	filters.orderDir = (orderedBy.dir == "ASC" ? "ASC" : "DESC");
	filters.discount = (filters.discount || 0);
	filters.id = [];
	filters.nearbies = {};

	if (filters.roomsNum) {
		for (var i = 0; i < filters.roomsNum; ++i) {
			filters.rooms.push(filters["persons-" + i]);
		}
	}
	//console.log(0, Date.now());

	// A pipe-line pattern implementation follows, each pipe-node is executed in asynchronous manner.
	// "bee" is the pipe-line-handler,
	// the executing order should be
	// filterCheck; query; translations; counts; filterCounts; media; nearby; prices; data;

	// Bee as it Collects nectar from one callback
	// to an other before it makes honey :)
	var bee = new mNode.event(function (key, v) {
		switch (key) {
			case "data":
				//console.log(9, Date.now());
				try {
					token.count = (v[0][1][0]["cnt"] || 0);
					token.data = {};

					for (var i = 0; i < v[0][0].length; ++i) {
						//token.data[i+"_"+v[0][0][i].id] = v[0][0][i];
						token.data[v[0][0][i].id] = v[0][0][i];
						var detailed = JSON.parse(token.data[v[0][0][i].id].detailed);
						var priceSum = 0;
						for (var j = 0; j < detailed.length; j++) {
							priceSum += detailed[j];
						}
						token.data[v[0][0][i].id].days = filters.dayLength + 1;
						token.data[v[0][0][i].id].nights = filters.dayLength;
						token.data[v[0][0][i].id].order = i + (token.page * token.perPage);
						token.data[v[0][0][i].id].priceTotal = token.data[v[0][0][i].id].price * 1.0;
						token.data[v[0][0][i].id].price = priceSum * filters.dayLength;
					}
					token.dayLength = filters.dayLength;
					token.exchange = filters.exchange;
					token.budget = filters.budgetDaily;
					token.fields = v[1];
					token.targetRegion = {
						region    : filters.region,
						regionKey : filters.regionKey,
						regionType: filters.regionType
					};

					__modulateRS(token, filters, function (token, filters) {
						res.send(token);
						res.end();
					});

				}
				catch (e) {
					console.error(e);
					res.send(token);
					res.end();
				}
				//console.log(9.1, Date.now());
				//res.send(token);
				//res.end();
				//console.log(9.2, Date.now());
				break;
			case "filterCheck":
				dbCache.get(tokenKey, function (rows, err) {
					if (rows.leng == 0 && err == null) {
						res.send(404); // Error for no token
						return;
					}
					var idsObj = {};
					if (!!rows[0] && !!(idsObj = JSON.parse(rows[0].value)) && filters.region == idsObj.region) {
						//console.log(1, Date.now());
						bee.emit("query").trigger();
					}
					else {
						__generateDataSessionToken(tokenKey, filters, function (tk, fltArr, rows, err, sql) {
							if (rows.length > 0) {
								fltArr.zoomLevel = rows[0].zoomLevel;
								fltArr.lat = rows[0].lat;
								fltArr.lng = rows[0].lng;
								for (var i = rows.length; i--;) {
									fltArr.id.push(rows[i].id);
								}
							}
							// token update
							dbCache.put(tk, fltArr, 0, function () {
								//console.log(1.1, Date.now());
								bee.emit("query").trigger();
							});
						});
					}
				});
				break;
			case "query":
				//console.log(2, Date.now());
				// the flow is changed as a nearby req is handled
				if (filters.nearby) {
					__nearbyGeoSearch(conf.nearby.radius, filters.nearby, modQuery, function (rows, err, sql) {
						var arr = [];
						for (var i = 0; i < rows.length; ++i) {
							filters.nearbies[rows[i].id] = rows[i];
							arr.push(rows[i].id);
						}
						__executeQ(tokenKey, arr);
					});
				}
				else {
					// get cached results
					dbCache.get(tokenKey, function (rows, err) {
						//console.log(2.1, Date.now());
						if (rows.length == 0 && err == null) {
							res.send(404); // Error for no token
							return;
						}
						var idsObj = JSON.parse(rows[0].value);
						__executeQ(tokenKey, (idsObj.id || []));
					});
				}

			function __executeQ(tokenKey, dataSet) {
				// A trick to get all hotels by the existing filters.
				// So all filters are cached and fetched here, so during
				// the creation of the Temp table all the rightfull hotels shall be included
				memoryCache.get("mfieldsIds", function (obj, err) {
					if (err && obj.length == 0) {
						__reCacheMFields(filters.lang, function () {
							memoryCache.get("mfields", function (obj, err) {
								doProceed(obj, err);
							});
						});
					}
					else {
						doProceed(obj, err);
					}

					function doProceed(obj, err) {
						//console.log(2.2, Date.now());
						var mfTmp = obj[filters.lang] || obj[conf.defaultLang];
						// A cross assign trick
						var filtObj = {};
						for (var filt in mfTmp) {
							if (filters[filt]) {
								filtObj[filt] = __clearPrefix(filters[filt]);
							}
						}
						filtObj = (filtObj || mfTmp);
						filtObj.roomsNum = filters.roomsNum;
						filtObj.rooms = filters.rooms;
						filtObj.budgetDaily = filters.budgetDaily;
						filtObj.discount = filters.discount;
						filtObj.orderBy = filters.orderBy;
						filtObj.orderDir = filters.orderDir;

						__buildTempTableByCachedIds(tokenKey, filtObj, mfTmp, (dataSet || []), modQuery,
							function (tokenKey, rows, err, sql) {
								//console.log(2.3, Date.now());
								bee.emit("translations").trigger(tokenKey, rows, err, sql);
							});
					}
				});
			}

				break;
			case "translations":
				//console.log(3, Date.now());
				// v[0] is the tokenKey passed by query
				var mq = modQuery.newModQuery().from(v[0] + "_stage");

				// transliteration support
				if (filters.lang !== conf.defaultLang) {
					mq.select(['hotel.id as id',
							'COALESCE(tr1.translation, hotel.title) as title',
							'COALESCE(tr2.translation, hotel.description) as description',
							'hotel.stars as stars',
							'`' + v[0] + '_stage`.val as detailed',
							'`' + v[0] + '_stage`.price as price',
							'hotel.permalink as permalink'])
						.joinSQL(" JOIN hotel " +
							"ON `" + v[0] + "_stage`.hotelid = hotel.id ")
						.joinSQL(" LEFT JOIN translations as tr1 " +
							"ON tr1.itemid = hotel.id " +
							"AND tr1.`table` = 'hotel' " +
							"AND tr1.field = 'title' " +
							"AND tr1.active = 1 ")
						.joinSQL(" LEFT JOIN translations as tr2 " +
							"ON tr2.itemid = hotel.id " +
							"AND tr2.`table` = 'hotel' " +
							"AND tr2.field = 'description' " +
							"AND tr2.active = 1 ");
				}
				else {
					mq.select([
							'hotel.id as id',
							'hotel.title as title',
							'hotel.description as description',
							'hotel.stars as stars',
							'`' + v[0] + '_stage`.val as detailed',
							'`' + v[0] + '_stage`.price as price',
							'hotel.permalink as permalink'])
						.joinSQL(" JOIN hotel " +
							"ON `" + v[0] + "_stage`.hotelid = hotel.id ");
				}
				mq.groupBy("`" + v[0] + "_stage`.hotelid")
					//.sortBy("`"+[v[0]+"_stage`.hotelid"])
					.sortBy("`" + [v[0] + "_stage`.orderedPrice " + filters.orderDir + " "])
					.limit(((token.page - 1) * (req.body.perPage || conf.perPage)), (req.body.perPage || conf.perPage));
				bee.emit("counts").trigger(v[0], mq);
				break;
			case "counts":
				//console.log(4, Date.now());
				var mq = v[1]; // passed as argument by translations
				mq.addParallel()
					.from(v[0] + "_stage")
					.select("COUNT( DISTINCT hotelid) as cnt")
					//.groupBy("hotelid")
					.build().execute(function (rows, err, sql) {
						//console.log(4.1, Date.now());
						if (err) {
							console.log(sql);
							console.log(err);
						}
						//console.log(sql);
						bee.emit("filterCounts").trigger(rows || [], mq.newModQuery().from(v[0]), v[0], err, sql);
					});
				break;
			case "filterCounts":
				//console.log(5, Date.now());
				// v[2] == tokenKey
				__getFiltersCountByRelatedIds(v[2], filters, v[1], function (tokenKey, rows, err, sql) {
					//console.log(5.1, Date.now());
					bee.emit("media").trigger(v[0], tokenKey, rows, err, sql);
				});
				break;
			case "media":
				//console.log(6, Date.now());
				// fetch the media of the already fetched items
				var idArr = [];
				for (var i = 0; i < v[0][0].length; i++) {
					idArr.push(v[0][0][i].id);
				}
				modQuery.newModQuery()
					.from("item_images")
					.select(['item_images.itemid', 'alt', 'title', '@id:=id',
						"CONCAT( '{', SUBSTRING_INDEX ( group_concat(DISTINCT '\\\"', image_type, '\\\":\\\"', image_url, '\\\"' SEPARATOR ', ' ),', ',6), '}' ) AS thumbs"])
//						"(SELECT image_url FROM item_images_aditional WHERE imageid=@id AND image_type='thumb') as thumb",
//						"(SELECT image_url FROM item_images_aditional WHERE imageid=@id AND image_type='big_thumb') as big_thumb",
//						"(SELECT image_url FROM item_images_aditional WHERE imageid=@id AND image_type='search_thumb') as search_thumb",
//						"(SELECT image_url FROM item_images_aditional WHERE imageid=@id AND image_type='originals') as originals",
//						"(SELECT image_url FROM item_images_aditional WHERE imageid=@id AND image_type='slider') as slider",
//						"(SELECT image_url FROM item_images_aditional WHERE imageid=@id AND image_type='main') as main"])
					.joinSQL(" INNER JOIN item_images_aditional ON (item_images.id=item_images_aditional.imageid) ")
					//.joinSQL(" INNER JOIN `"+v[1]+"_stage` ON `"+v[1]+"_stage`.hotelid = item_images.itemid ")
					.filterBy("item_images", "itemid").in(idArr)
					.filterBy("item_images", "module").equals('hotel')
					.filterBySQL("`item_images_aditional`.`image_type` IN ( 'thumb','big_thumb','search_thumb','originals','slider','main' )")
					.groupBy('itemid')
					.build().execute(function (rows, err, sql) {
						//console.log(6.1, Date.now());
						if (err) {
							//console.log(sql);
							//console.error(err);
						}
						var tmpRowsArr = {};
						for (var i = rows.length; i--;) {
							rows[i].thumbs = JSON.parse(rows[i].thumbs);
							tmpRowsArr[rows[i].itemid] = rows[i];
						}
						for (var i = v[0][0].length; i--;) {
							if (tmpRowsArr[v[0][0][i].id]) {
								v[0][0][i].media = {
									alt         : tmpRowsArr[v[0][0][i].id].alt,
									title       : tmpRowsArr[v[0][0][i].id].title,
									thumb       : tmpRowsArr[v[0][0][i].id].thumbs.thumb || "",
									big_thumb   : tmpRowsArr[v[0][0][i].id].thumbs.big_thumb || "",
									search_thumb: tmpRowsArr[v[0][0][i].id].thumbs.search_thumb || "",
									originals   : tmpRowsArr[v[0][0][i].id].thumbs.originals || "",
									slider      : tmpRowsArr[v[0][0][i].id].thumbs.slider || "",
									main        : tmpRowsArr[v[0][0][i].id].thumbs.main || ""
								};
							}
							else {
								v[0][0][i].media = {}
							}
						}
						bee.emit("nearby").trigger(v[0], v[2], err, sql);
					});
				break;
			case "nearby":
				//console.log(7, Date.now());
				if (filters.nearby) {
					// bind extra fields to the fetched results
					for (var i = 0; i < v[0][0].length; ++i) {
						v[0][0][i].distance = (
							filters.nearbies[v[0][0][i].id].distance > 1 ?
								(filters.nearbies[v[0][0][i].id].distance).toFixed(2) + " Km" :
								(filters.nearbies[v[0][0][i].id].distance * 1000).toFixed() + " m" );
						v[0][0][i].geoDistance = filters.nearbies[v[0][0][i].id].distance;
						v[0][0][i].lat = filters.nearbies[v[0][0][i].id].lat;
						v[0][0][i].lng = filters.nearbies[v[0][0][i].id].lng;
					}
					v[0][0].sort(function (left, right) {
						return left.geoDistance - right.geoDistance
					});
					bee.emit("prices").trigger(v[0], v[1]);
				}
				else {
					bee.emit("prices").trigger(v[0], v[1]);
				}
				break;
			case "prices":
				//console.log(8, Date.now());
				if (filters.rooms.length > 0) {
					// if not results were sorted by nearby
					if (!!!filters.nearby) {
						if (filters.orderDir == "ASC") {
							v[0][0].sort(function (left, right) {
								return left.price - right.price
							});
						}
						else {
							v[0][0].sort(function (left, right) {
								return right.price - left.price
							});
						}
					}
					bee.emit("data").trigger(v[0], v[1]);

//					var idArr = [];
//					for(var i=0; i<v[0][0].length; ++i) {
//						idArr.push(v[0][0][i].id);
//					}
//					modQuery.newModQuery()
//						.from("rooms_hotel")
//						.select(['hotel_id as id',
//							'MIN(price) as price',
//							'MAX(discount) as discount'])
//						.filterBy("rooms_hotel", "hotel_id").in(idArr)
//						.filterBy("rooms_hotel", "room_type").in(filters.rooms)
//						.groupBy('hotel_id')
//						.build().execute(function(rows, err, sql) {
//							if(err) {
//								console.log(sql);
//								console.log(err);
//							}
//							console.log(sql);
//							var tmpObj = {};
//							for(var i=0; i<rows.length; ++i){
//								tmpObj[rows[i].id] = rows[i];
//							}
//							for(var i=0; i<v[0][0].length; ++i) {
//								v[0][0][i].price = tmpObj[v[0][0][i].id].price;
//								v[0][0][i].discount = tmpObj[v[0][0][i].id].discount;
//							}
//
//						});
				}
				else {
					bee.emit("data").trigger(v[0], v[1]);
				}
				break;
			default:
				break;
		}
	});
	bee.emit("filterCheck").trigger();
});

/**
 * Call-Handler to fetch Hotels in offer
 */
app.post('/offer', function (req, res, next) {
	var modQuery = new global.modQuery({
		db      : global.db,
		mNode   : mNode,
		conf    : conf,
		callback: function () {
			console.log(arguments);
		}
	});

	var token = {
		token  : tokenKey,
		count  : 0,
		data   : [],
		page   : (req.body.filters.page || 1),
		perPage: (req.body.perPage || conf.perPage)
	};

	// Bee as it Collects nectar from one callback
	// to an other before it makes honey :)
	var bee = new mNode.event(function (key, v) {
		switch (key) {
			case "data":
				token.count = (v[0][1][0]["cnt"] || 0);
				token.data = v[0][0];
				res.send(token);
				res.end();
				break;
			default:
				break;
		}
	});

	var mq = modQuery
		.newModQuery() //
		.from("hotel") //
		.select(['id', 'title']) //
		.groupBy('id').sortBy("RAND") //
		.limit(1) //
		.build().execute(function (rows, err, sql) {
			bee.emit("data").trigger(rows || rows, err, sql);
		});
});

/**
 * Call-Handler to fetch a Random Hotel
 */
app.post('/rand', function (req, res, next) {
	var modQuery = new global.modQuery({
		db      : global.db,
		mNode   : mNode,
		conf    : conf,
		callback: function () {
			console.log(arguments);
		}
	});

	var token = {
		data: []
	};

	// Bee as it Collects nectar from one callback
	// to an other before it makes honey :)
	var bee = new mNode.event(function (key, v) {
		switch (key) {
			case "data":
				token.data = v[0][0];
				res.send(token);
				res.end();
				break;
			default:
				break;
		}
	});

	var mq = modQuery
		.newModQuery()
		.from("hotel")
		.select(['id', 'title'])
		.groupBy('id').sortBy("RAND")
		.limit(1)
		.build().execute(function (rows, err, sql) {
			bee.emit("data").trigger(rows || rows, err, sql);
		});
});


/**
 * Call-Handler to get One Item's info
 */
app.post('/getItem', function (req, res, next) {
	if (!req.body) {
		res.send(404);
		return;
	}

	var filters = (req.body.filters || {});
	filters.lang = (filters["lang"] ? filters["lang"] : conf.defaultLang);
	//filters.itemid = (filters["itemid"]);

	var modQuery = new global.modQuery({
		db      : global.db,
		mNode   : mNode,
		conf    : conf,
		callback: function (rows, err, sql) {
			if (err) {
				console.log(sql);
				console.log(err);
				res.send(404);
				return;
			}

			var token = rows[0];

			// fetch the media of the already fetched item
			modQuery.newModQuery()
				.from("item_images")
				.select(['item_images.itemid', 'alt', 'title', '@id:=id',
					"(SELECT image_url FROM item_images_aditional WHERE imageid=@id AND image_type='thumb') as thumb",
					"(SELECT image_url FROM item_images_aditional WHERE imageid=@id AND image_type='big_thumb') as big_thumb",
					"(SELECT image_url FROM item_images_aditional WHERE imageid=@id AND image_type='search_thumb') as search_thumb",
					"(SELECT image_url FROM item_images_aditional WHERE imageid=@id AND image_type='originals') as originals",
					"(SELECT image_url FROM item_images_aditional WHERE imageid=@id AND image_type='slider') as slider",
					"(SELECT image_url FROM item_images_aditional WHERE imageid=@id AND image_type='main') as main"])
				.joinSQL(" INNER JOIN item_images_aditional ON (item_images.id=item_images_aditional.imageid) ")
				.filterBy("item_images", "itemid").in([token.id])
				.filterBy("item_images", "module").equal("hotel")
				.groupBy('itemid')
				.build().execute(function (rows, err, sql) {
					if (err) {
						console.log(sql);
						console.log(err);
					}

					if (rows) {
						token.media = {
							alt         : rows[0].alt,
							title       : rows[0].title,
							thumb       : rows[0].thumb,
							big_thumb   : rows[0].big_thumb,
							search_thumb: rows[0].search_thumb,
							originals   : rows[0].originals,
							slider      : rows[0].slider,
							main        : rows[0].main
						};
					}
					else {
						token.media = {}
					}

					res.send(token);
					res.end();
				});
		}
	}).newModQuery().from("hotel"); //

	// transliteration support
	if (filters.lang !== conf.defaultLang) {
		modQuery.select(['hotel.id as id',
				'COALESCE(tr1.translation, hotel.title) as title',
				'COALESCE(tr2.translation, hotel.description) as description',
				'hotel.stars as stars',
				'hotel.permalink as permalink',
				'0 as price',
				'0 as discount',
				'"" as discount_type'])
			.joinSQL(" LEFT JOIN translations as tr1 " +
				"ON tr1.itemid = hotel.id " +
				"AND tr1.`table` = 'hotel' " +
				"AND tr1.field = 'title' " +
				"AND tr1.active = 1 ")
			.joinSQL(" LEFT JOIN translations as tr2 " +
				"ON tr2.itemid = hotel.id " +
				"AND tr2.`table` = 'hotel' " +
				"AND tr2.field = 'description' " +
				"AND tr2.active = 1 ");
	}
	else {
		modQuery.select([
			'hotel.id as id',
			'hotel.title as title',
			'hotel.description as description',
			'hotel.stars as stars',
			'hotel.permalink as permalink',
			'0 as price',
			'0 as discount',
			'"" as discount_type']);
	}
	modQuery
		.filterBy("hotel", "id").in(filters.itemid) //
		.build() //
		.execute(); //
});

/**
 * Call-Handler to flush exchange rates
 */
app.post('/flushExchange', function (req, res, next) {
	if (!req.body) {
		res.send(404);
		return;
	}


	var http = require('http');
	var options = {
		host: 'www.ecb.europa.eu',
		path: '/stats/eurofxref/eurofxref-daily.xml'
	};
	http.request(options,function (response) {
		var str = '';

		response.on('data', function (chunk) {
			str += chunk;
		});
		response.on('end', function () {
			var tmpArr = str.match(/(\w*\s\w*)=\S[A-Z]{3}\S\s\w*=\S(\d*\.\d*)\S/gim);
			var rates = {}
			for (var i = 0; i < tmpArr.length; ++i) {
				var tmp = tmpArr[i].split(/\'/g);
				rates[tmp[1]] = tmp[3];
			}

			var modQuery = new global.modQuery({
				db      : global.db,
				mNode   : mNode,
				conf    : conf,
				callback: function (rows, err, sql) {
					if (err) {
						console.log(sql);
						console.log(err);
					}
				}
			});

			for (var fxKey in rates) {
				modQuery.newModQuery() //
					.executeSQL("INSERT INTO `exchange` (currency,rate) " +
						"VALUES ('" + fxKey + "'," + rates[fxKey] + ") " +
						"ON DUPLICATE KEY UPDATE rate=" + rates[fxKey] + " ");
			}

			res.send(rates);
			//res.send(str);
		});
	}).end();
});


/**
 * Call-Handler to match geo-locations as for nearby
 */
function __nearbyGeoSearch(radius, itemid, modQuery, callback) {

	modQuery.newModQuery()
		.executeSQL(" " +
		" SELECT @lat := lat, @lng := lng " +
		" FROM maps_items " +
		" WHERE itemid = " + itemid + " AND module = 'hotel'; " +
		//" SET @lat = "+obj.lat+"; " +
		//" SET @lng = "+obj.lng+"; " +
		" SET @center = GeomFromText(CONCAT('POINT(',@lat,' ',@lng,')')); " +
		" SET @radius = 0.009*" + radius + "; " +
		" SET @bbox = CONCAT('POLYGON((', " +
		" 	X(@center) - @radius, ' ', Y(@center) - @radius, ',', " +
		" 	X(@center) + @radius, ' ', Y(@center) - @radius, ',', " +
		" 	X(@center) + @radius, ' ', Y(@center) + @radius, ',', " +
		" 	X(@center) - @radius, ' ', Y(@center) + @radius, ',', " +
		" 	X(@center) - @radius, ' ', Y(@center) - @radius, '))' " +
		" ); " +
		" SELECT " +
		" 	(6371*acos(cos(radians(@lat))*cos(radians(lat))*cos(radians(lng)-radians(@lng))+sin(radians(@lat))*sin(radians(lat)))) AS distance, " +
		" 	itemid as id, " +
		"   lat, " +
		"   lng, " +
		" 	geocoderAddress " +
		" FROM maps_items  " +
		" WHERE " +
		" Intersects(maps_items.location, GeomFromText(@bbox)) " +
		" ORDER BY distance ASC; ", function (rows, err, sql) {
			if (err) {
				console.log(sql);
				console.log(err);
			}
			callback((rows[4] && rows[4].length ? rows[4] : [
				{id: 0}
			]), err, sql)
		});
}

/**
 * Gets the count of related Filters by the given relation dataSet
 */
function __getFiltersCountByRelatedIds(tokenKey, filters, modQuery, callback) {
	// Get filters cached by the memory
	memoryCache.get("mfields", function (obj, err) {
		if (err && obj.length == 0) {
			__reCacheMFields(filters.lang, function () {
				memoryCache.get("mfields", function (obj, err) {
					doProceed(obj, err);
				});
			});
		}
		else {
			doProceed(obj, err);
		}

		function doProceed(obj, err) {
			var mfTmp = obj[filters.lang] || obj[conf.defaultLang];
			modQuery.newModQuery().from(tokenKey)
				.select(["itemid as id",
					"tableName as filter",
					"COUNT(hotelid) as count"])
				.groupBy("itemid, tableName")
				.build().execute(function (rows, err, sql) {
					var tmpArr = {};
					for (var i = 0; i < rows.length; ++i) {
						if (typeof tmpArr[intermediateMapReversed[rows[i].filter]] === "undefined") {
							tmpArr[intermediateMapReversed[rows[i].filter]] = [];
						}
						tmpArr[intermediateMapReversed[rows[i].filter]].push({
							id   : rows[i].id,
							count: rows[i].count
						});
					}
					callback(tokenKey, tmpArr, err, sql);
				});
		}
	});
}

function __buildTempTableByCachedIds(tokenKey, filters, allFilts, rows, modQuery, callback) {
	var idArrS = (rows.join(",") || '-1');
	var insertSql = "";
	var inTableSql = "";
	var filterSql = "";
	var filterIds = [];
	var prices = [];

	// create a filter Query that applies to the inTable fields
	for (var fild in inTableMap) {
		if (typeof filters[fild] === "string") {
			inTableSql += " AND ( `" + inTableMap[fild] + "` LIKE '%" + filters[fild] + "%' )";
		}
		else if (typeof filters[fild] === "object" && filters[fild].length > 0) {
			inTableSql += " AND ( `" + inTableMap[fild] + "` IN (" + filters[fild].join(',') + ") )";
		}
	}

	filterSql += "( SELECT id as hotelid FROM `hotel` WHERE id IN (" + idArrS + ") " + inTableSql + " ) AS base ";

	// create an insert Query by filters that generates the target dataSet
	for (var tbl in intermediateMap) {
		if (filters[tbl]) {
			filterIds = filters[tbl];
			for (var i = 0; i < filterIds.length; ++i) {
				filterSql += " JOIN ( " +
					"SELECT hotelid " +
					"FROM `" + intermediateMap[tbl] + "` " +
					"WHERE hotelid IN (" + idArrS + ") " +
					"AND itemid = " + filterIds[i] + " ) " +
					"AS " + intermediateMap[tbl] + "_" + i + " " +
					"ON " + intermediateMap[tbl] + "_" + i + ".hotelid = base.hotelid ";
			}
		}
	}
	// Build the Query that's responsible for rooms & price filtering
	if (filters.roomsNum) {
		filterIds = filters.rooms;
		var tmpField = "price";
		var mscSQL = "";
		if (!!filters.budgetDaily && filters.budgetDaily.max > 0) {
			mscSQL += " AND price BETWEEN " + filters.budgetDaily.min + " AND " + filters.budgetDaily.max + " ";
		}
		if (filters.discount) {
			var d = Math.floor((new Date()).getTime() / 1000);
			mscSQL += " AND discount > 0 ";// +
			//"AND start_discount_date < "+d+" "
			//"AND end_discount_date > "+d+" ";
			tmpField = "discount"
		}
		for (var i = 0; i < filterIds.length; ++i) {
			prices.push("price_" + i);
			filterSql += " JOIN ( " +
				"SELECT hotel_id, " + tmpField + " as price_" + i + ", price as p_" + i + " " +
				"FROM `rooms_hotel` " +
				"WHERE hotel_id IN (" + idArrS + ") " +
				"AND room_type = " + filterIds[i] + " " + mscSQL + " " +
				"GROUP BY hotel_id HAVING price = MIN(" + tmpField + ") ) " +
				"AS rooms_hotel_" + i + " " +
				"ON rooms_hotel_" + i + ".hotel_id = base.hotelid ";
		}
	}

	var orderedPriceSQL = {field: "", join: ""};
	switch (filters.orderBy) {
		case "train":
			orderedPriceSQL.field = "efv.value as orderedPrice";
			orderedPriceSQL.join = "LEFT JOIN extra_field_values efv ON efv.itemid = base.hotelid " +
				"AND efv.fieldid IN (" + orderingIds.train.join(",") + ") ";
			break;
		case "plane":
			orderedPriceSQL.field = "efv.value as orderedPrice";
			orderedPriceSQL.join = "LEFT JOIN extra_field_values efv ON efv.itemid = base.hotelid " +
				"AND efv.fieldid IN (" + orderingIds.plane.join(",") + ") ";
			break;
		case "port":
			orderedPriceSQL.field = "efv.value as orderedPrice";
			orderedPriceSQL.join = "LEFT JOIN extra_field_values efv ON efv.itemid = base.hotelid " +
				"AND efv.fieldid IN (" + orderingIds.port.join(",") + ") ";
			break;
		default:
			orderedPriceSQL.field = "@tmp as orderedPrice";
			orderedPriceSQL.join = " ";
			break;
	}

	// wrap it to complete SQL
	filterSql = " INSERT INTO `" + tokenKey + "_stage` (hotelid, price, orderedPrice, val) " +
		"SELECT " +
		" base.hotelid as hotelid, " +
		" @tmp := ((" + prices.join("+") + ") / " + filters.roomsNum + ") as price, " +
		" " + orderedPriceSQL.field + ", " +
		" CONCAT('['," + prices.join(",',',") + ",']') as val " +
		"FROM (" + filterSql + ") " +
		orderedPriceSQL.join +
		"ORDER BY orderedPrice " + filters.orderDir + " ; ";//, FIELD(rooms_hotel.hotel_id, "+idArrS+"); ";

	// create an insert Query that generates the targeted dataSet
	for (var tbl in intermediateMap) {
		if (allFilts[tbl]) {
			var tmpIds = allFilts[tbl].join(",");

			insertSql += " INSERT INTO `" + tokenKey + "` (hotelid, itemid, tableName) " +
				"SELECT " + intermediateMap[tbl] + ".hotelid, itemid, '" + intermediateMap[tbl] + "' as tableName " +
				"FROM " + intermediateMap[tbl] + " " +
				"INNER JOIN `" + tokenKey + "_stage` ON " + intermediateMap[tbl] + ".hotelid = `" + tokenKey + "_stage`.hotelid " +
				"WHERE itemid IN (" + tmpIds + "); ";
		}
	}

	// Black sorcery stuff, Warranty voids if any change (and you are turned into frog as well)
	modQuery.newModQuery()
		.executeSQL(//"SET @@global.hot_cache.key_buffer_size = 256*1024*1024; " +
		//"CREATE TABLE IF NOT EXISTS `" + tokenKey + "` " +
		"CREATE TEMPORARY TABLE IF NOT EXISTS `" + tokenKey + "` " +
			"(`hotelid` int(11) NOT NULL," +
			"`itemid` int(11) NOT NULL, " +
			"`tableName` varchar(20) DEFAULT NULL, " +
			"KEY `itemid` (`itemid`), " +
			"KEY `hotelid` (`hotelid`), " +
			"KEY `tableName` (`tableName`)," +
			"KEY `combined_hi` (`hotelid`,`itemid`), " +
			"KEY `combined_it` (`itemid`,`tableName`) ) " +
			"ENGINE=MEMORY DEFAULT CHARSET=latin1; " +
			//"CREATE TABLE IF NOT EXISTS `" + tokenKey + "_stage` " +
			"CREATE TEMPORARY TABLE IF NOT EXISTS `" + tokenKey + "_stage` " +
			"(`hotelid` int(11) NOT NULL, " +
			"`price` decimal(11,2) NOT NULL, " +
			"`orderedPrice` int(11) NOT NULL, " +
			"`val` varchar(50) DEFAULT NULL, " +
			"KEY `hotelid` (`hotelid`), " +
			"KEY `price` (`price`), " +
			"KEY `orderedPrice` (`orderedPrice`), " +
			"KEY `combined_hp` (`hotelid`,`price`) ) " +
			"ENGINE=MEMORY DEFAULT CHARSET=latin1; " +
			"TRUNCATE `" + tokenKey + "`; " +
			"TRUNCATE `" + tokenKey + "_stage`; " + // told ya, its an alien matter
			filterSql + insertSql, function (rows, err, sql) {
			if (err) {
				console.log(filters);
				console.log(sql);
				console.log(err);
			}
			//console.log(sql);
			callback(tokenKey, rows, err, sql);
		});
}

/**
 * Flush and reCache MFields
 */
function __reCacheMFields(lang, callback) {
	var modQuery = new global.modQuery({
		db      : global.db,
		mNode   : mNode,
		conf    : conf,
		callback: function (rows, err, sql) {
			if (err) {
				console.log(sql);
				console.log(err);
			}
			var tmpObj = {};
			var tmpIdsObj = {};
			for (var i = rows.length; i--;) {
				for (var j = rows[i].length; j--;) {
					// if No translation, enter default
					if (!!!rows[i][j].code) {
						rows[i][j].code = conf.defaultLang;
					}
					// Check if this Lang was assigned
					if (!!!tmpObj[rows[i][j].code]) {
						tmpObj[rows[i][j].code] = {};
						tmpIdsObj[rows[i][j].code] = {};
					}
					// check if module of this lang was assigned
					if (!!!tmpObj[rows[i][j].code][rows[i][j].module]) {
						tmpObj[rows[i][j].code][rows[i][j].module] = [];
						tmpIdsObj[rows[i][j].code][rows[i][j].module] = [];
					}
					tmpIdsObj[rows[i][j].code][rows[i][j].module].push(rows[i][j].id);
					tmpObj[rows[i][j].code][rows[i][j].module].push({
						id   : rows[i][j].id,
						title: rows[i][j].title,
						count: 0
					});
				}
			}
			memoryCache.put("mfields", tmpObj);
			memoryCache.put("mfieldsIds", tmpIdsObj);
			if (callback) {
				callback(tmpObj, tmpIdsObj, err, sql);
			}
		}
	}).newModQuery()
		.from("services")
		.select(['services.id as id', 'translations.code as code', '"services" as module', 'COALESCE(translations.translation, services.title) as title'])
		.joinSQL(" LEFT JOIN translations " +
			"ON translations.itemid = services.id " +
			"AND `table` = 'services' " +
			"AND field = 'title' " +
			"AND translations.active = 1 ")
		.addParallel()
		.from("themeshotels")
		.select(['themeshotels.id as id', 'translations.code as code', '"themeshotels" as module', 'COALESCE(translations.translation, themeshotels.title) as title'])
		.joinSQL(" LEFT JOIN translations " +
			"ON translations.itemid = themeshotels.id " +
			"AND `table` = 'themeshotels' " +
			"AND field = 'title' " +
			"AND translations.active = 1 ")
		.addParallel()
		.from("facilities")
		.select(['facilities.id as id', 'translations.code as code', '"facilities" as module', 'COALESCE(translations.translation, facilities.title) as title'])
		.joinSQL(" LEFT JOIN translations " +
			"ON translations.itemid = facilities.id " +
			"AND `table` = 'facilities' " +
			"AND field = 'title' " +
			"AND translations.active = 1 ")
		.addParallel()
		.from("types")
		.select(['types.id as id', 'translations.code as code', '"types" as module', 'COALESCE(translations.translation, types.title) as title'])
		.joinSQL(" LEFT JOIN translations " +
			"ON translations.itemid = types.id " +
			"AND `table` = 'types' " +
			"AND field = 'title' " +
			"AND translations.active = 1 ")
		.addParallel()
		.from("roomsfacilities")
		.select(['roomsfacilities.id as id', 'translations.code as code', '"roomsfacilities" as module', 'COALESCE(translations.translation, roomsfacilities.title) as title'])
		.joinSQL(" LEFT JOIN translations " +
			"ON translations.itemid = roomsfacilities.id " +
			"AND `table` = 'roomsfacilities' " +
			"AND field = 'title' " +
			"AND translations.active = 1 ")
		.addParallel()
		.from("hotel")
		.select(['t.id', '"en" as code', '"stars" as module', 't.id as title'])
		.joinSQL("INNER JOIN ( SELECT 1 as id UNION " +
			" SELECT 2 as id UNION " +
			" SELECT 3 as id UNION " +
			" SELECT 4 as id UNION " +
			" SELECT 5 as id ) as t")
		.groupBy('t.id')
		.addParallel()
		.from("hotel")
		.select(['"" as id', '"en" as code', '"title" as module', '"" as title'])
		.groupBy('""')
		.build().execute();
}

/**
 * The mechanism of generating the results by filters
 * that should be used by Session cache
 * @param tokenKey
 * @param filters
 * @param callback
 * @private
 */
function __generateDataSessionToken(tokenKey, filters, callback) {
	// cache Hotel Ids for the specified session search
	var modQuery = new global.modQuery({
		db      : global.db,
		mNode   : mNode,
		conf    : conf,
		callback: function (rows, err, sql) {
			if (err) {
				console.log(sql);
				console.log(err);
			}
			console.log(sql);
			if (callback) {
				callback(tokenKey, filters, rows, err, sql);
			}
		}
	}).newModQuery() //
		.from("geo_relations") //
		.joinSQL(" INNER JOIN hotel " +
			"ON geo_relations.itemid = hotel.id " +
			"AND hotel.enable = 1 " +
			"AND hotel.active = 1 ")
		.select(['`itemid` as `id`']);//,'zoomLevel', 'lat', 'lng']); //

	// Filter TokenCache by the Destination type & relation
	if (!!filters.region && !!filters.regionType) {
		if (!!filters.regionKey) {
			switch (filters.regionType) {
				case "city": // geo_cityr
					modQuery.filterBy("geo_cityr", "id").equals(filters.regionKey) //
					break;
				case "region": // geo_region
					modQuery.filterBy("geo_region", "id").equals(filters.regionKey) //
					break;
				case "country": // geo_country
					modQuery.filterBy("geo_country", "id").equals(filters.regionKey) //
					break;
				default :
					break;
			}
		}
	}
	// execute Query & Cache in CallbackMode
	modQuery
		.build() //
		.execute(); //
}

/**
 *
 * @param arr
 * @return {Array}
 * @private
 */
function __clearPrefix(arr) {
	if (typeof arr !== "object") {
		return arr;
	}

	var tmpArr = [];
	var s = arr.length;

	for (var i = 0; i < s; ++i) {
		tmpArr[i] = (arr[i] || "").split("_")[1];
	}

	return tmpArr;
}

/**
 * Format to a Date count, able to perform subtraction.
 * The Silliest solution ever, yet the fastest to implement & perform
 * @param str
 * @return {Number}
 * @private
 */
function __fD(str) {
	var strSplit = str.split("-");
	strSplit = [strSplit[1], strSplit[0], strSplit[2]];
	var str = strSplit.join("/");
	return ((new Date(str)).getTime());
//	strSplit = [strSplit[1], strSplit[0], strSplit[2]];
//	var str = strSplit.join("/");
//	var isLeap = ((strSplit[2] % 4) == 0 && ((strSplit[2] % 100) != 0 || (strSplit[2] % 400) == 0));
//	var monthDays = [31, (isLeap ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
//	var m = ((new Date(str)).getMonth());
//	return (new Date(str)).getDate() + (monthDays[m] * m) + (365 * ((new Date(str)).getYear()));
}

/**
 * Gets the count of related Filters by the given relation dataSet
 */
function __getFiltersCountByRelatedIds_OLD(tokenKey, filters, modQuery, callback) {
	// Get filters cached by the memory
	memoryCache.get("mfields", function (obj, err) {
		if (err && obj.length == 0) {
			__reCacheMFields(filters.lang, function () {
				memoryCache.get("mfields", function (obj, err) {
					doProceed(obj, err);
				});
			});
		}
		else {
			doProceed(obj, err);
		}

		function doProceed(obj, err) {
			var mfTmp = obj[filters.lang] || obj[conf.defaultLang];
			// a counter that is used to trigger callback
			var count = 0;
			// a map that tracks the order that modules are executed
			var filterPipe = [];
			// The loop to start count each of the modules
			for (var filt in mfTmp) {
				// the count of some filters is useless, ignorance should be applied
				if (ignoreCounts.indexOf(filt) > -1) {
					continue;
				}
				filterPipe.push(filt);
				modQuery.newModQuery().from(tokenKey)//intermediateMap[filt])
					.select([intermediateMap[filt] + ".itemid as id",
						"count(DISTINCT  " + intermediateMap[filt] + ".hotelid) as count"])
					.joinSQL(" INNER JOIN " + intermediateMap[filt] + " " +
						"ON " + tokenKey + ".hotelid = " + intermediateMap[filt] + ".hotelid " +
						"AND tableName = '" + intermediateMap[filt] + "' ")
					.groupBy(intermediateMap[filt] + ".itemid")
					.build().execute(function (rows, err, sql) {
						var tmpArr = {};
						for (var i = 0; i < rows.length; ++i) {
							tmpArr[rows[i].id] = rows[i];
						}
						for (var i = 0; i < mfTmp[filterPipe[count]].length; ++i) {
							if (tmpArr[mfTmp[filterPipe[count]][i].id]) {
								mfTmp[filterPipe[count]][i].count = tmpArr[mfTmp[filterPipe[count]][i].id].count;
							}
							else {
								mfTmp[filterPipe[count]][i].count = 0;
							}
						}

						if ((++count) == filterPipe.length) {
							callback(tokenKey, mfTmp, err, sql);
						}
					});
			}
		}
	});
}

function __modulateRS(token, filter, next) {
	memoryCache.get("mfields", function (obj, err) {
		if (err && obj.length == 0) {
			__reCacheMFields(filter.lang, function () {
				memoryCache.get("mfields", function (obj, err) {
					doProceed(obj, err);
				});
			});
		}
		else {
			doProceed(obj, err);
		}
		function doProceed(obj, err) {
			//token.fields
			var cached = obj[filter.lang] || obj[conf.defaultLang];

			for (var facet in token.fields) {
				token.fields[facet] = (function (filterFacet, globalFacet) {
					var indexFacets = {};
					var __this = this;

					filterFacet.map(function (f) {
						indexFacets[f.id] = f.count;
						return f;
					});
					globalFacet.forEach(function (f) {
						if (indexFacets[f.id]) {
							__this.push({
								id   : f.id,
								title: f.title,
								count: indexFacets[f.id]
							});
							return;
						}
						__this.push(f);
					});
					return this;
				}).call([], token.fields[facet], cached[facet]);
			}
			next(token, filter);
		}
	});
}