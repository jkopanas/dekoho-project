	head.ready(function(){
	
	
	router=new Router();
	router.registerRoute("payment/:page:/:value:/:action:/:transactionid:", controller.payment);
	

	
	router.registerRoute("popup/:page:/:value:/:action:", controller.popup);
	
	router.registerRoute("profileManager/:page:/:action:/:value:", controller.profileroute);


	router.registerRoute("hotelManager/rooms/hotel/:hotelid:", controller.roomsroute);
	
	
	router.registerRoute("hotelManager/rooms/hotel/:hotelid:/new", controller.roomnewroute);
	router.registerRoute("hotelManager/rooms/hotel/:hotelid:/{action}", controller.roomsroute,{ rules: { value: new RegExp('[^new]')}});
	router.registerRoute("hotelManager/rooms/:page:/:roomid:/:action:", controller.roomroute);

	router.registerRoute("hotelManager/rooms/room/{value}/photos/:action:", controller.media, { rules: { value: new RegExp('^[0-9]+$') } });

	router.registerRoute("hotelManager/hotel/{value}/photos/:action:", controller.media, { rules: { value: new RegExp('^[0-9]+$') } });
	router.registerRoute("translations/:page:/:value:/:action:", controller.translate);
	router.registerRoute("hotelManager/:page:/{value}/:action:", controller.hotelroute, { rules: { value: new RegExp('^[0-9]+$') } });
	router.registerRoute("hotelManager/Locations/:page:/{value}/:action:", controller.locationroute, { rules: { value: new RegExp('^[0-9]+$') } });
	router.registerRoute("hotelManager/:page:/{action}", controller.hotelroutenew,	{ rules: { action: new RegExp('^new') } } );
	
	router.registerRoute("hotelManager/:page:/:value:/:action:", controller.hotelroute);
	
	router.registerRoute("addOffer/:page:/:value:/:action:", controller.addOffer);  
	
	router.registerRoute("viewOffer/:page:/:value:/:action:", controller.addOffer);  
	
	router.registerRoute("admin/:page:/:action:/:value:", controller.adminAction);  
	
	router.registerRoute(":page:/:value:/:action:", controller.singleroute);  


	if ("onhashchange" in window) { // event supported?
			window.onhashchange = function() {
				router.applyRoute(window.location.hash.split('#')[1]);
			}
		} else { // event not supported: This would be needed for older IE's
			var storedHash = window.location.hash.split('#')[1];
			window.setInterval(function() {
				if (window.location.hash.split('#')[1] != storedHash) {
					storedHash = window.location.hash.split('#')[1];
					router.applyRoute(window.location.hash.split('#')[1]);
				}
			}, 100);
		}
	 
		if (window.location.hash) {
			window.location.hash=window.location.hash;
	 		router.applyRoute(window.location.hash.split('#')[1]);
		} else {
			window.location.hash="dashboard";
		}
		
		//kendo.bind($("#wrapper"), MasterModel);

	    
});