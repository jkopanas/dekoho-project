head.ready(function() {
	
	var MasterModel="";
	
	(function () {
		var dbMain = new mcms.dataStore(
														{
															method : "POST",
															url : "",
															dataType : "json",
															"async": false,
															trigger : "loadComplete",
															action: ""
														  }
													 );
		
		$(dbMain).bind('loadComplete',function(data) {
			
			var data=dbMain.records;
		
			controller.exchange = data.rate;
			controller.symbol = data.currency;
			location.reload();
			
		});
		
		MasterModel = kendo.observable({
			changeCurrency: function (e) {
				 e.preventDefault();
				 var link=$(e.currentTarget).data('link');
				 
				 dbMain.url = "/ajax/hooks/hotelier/index.php";
				 dbMain.data = {
							"route": { "call": "generalCall" },
							"action": "setCurrency",
							"value": link
					};
					$(dbMain).loadStore(dbMain);

			}
		 });
		
	})();
	 


 
 kendo.bind($("#wrapper"),MasterModel);
 
	
});//END HEAD 
