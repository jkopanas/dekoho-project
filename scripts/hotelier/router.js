/*
Router
 */
var Router="";
var Route="";
(function() {
	Router = function() {
		this.routes = new Array();
	};
	Router.prototype = {
		routes : new Array(),
		registerRoute : function(route, fn, paramObject) {
			var scope = paramObject ? paramObject.scope ? paramObject.scope
					: {} : {};
			var rules = paramObject ? paramObject.rules ? paramObject.rules
					: null : null;
			return this.routes[this.routes.length] = new Route({
				route : route,
				fn : fn,
				scope : scope,
				rules : rules
			})
		},
		addRoute : function(routeConfig) {
			return this.routes[this.routes.length] = new Route(routeConfig);
		},
		applyRoute : function(route) {
			

				$("#page-dark").show();
				$("#loaderPage").show();

			for ( var i = 0, j = this.routes.length; i < j; i++) {
				var sRoute = this.routes[i];
				if (sRoute.matches(route)) {
					sRoute.fn.apply(sRoute.scope, sRoute
							.getArgumentsValues(route));
					break;
				}
			}
			
			
		}
	}

	var Route = function() {
		this.route = arguments[0].route;
		this.fn = arguments[0].fn;
		this.scope = arguments[0].scope ? arguments[0].scope : null;
		this.rules = arguments[0].rules ? arguments[0].rules : {};

		this.routeArguments = Array();
		this.optionalRouteArguments = Array();



		this.routeParts = this.route.split("/");
		for ( var i = 0, j = this.routeParts.length; i < j; i++) {
			var rPart = this.routeParts[i]
			if (rPart.substr(0, 1) == "{"
					&& rPart.substr(rPart.length - 1, 1) == "}") {
				var rKey = rPart.substr(1, rPart.length - 2);
				this.routeArguments.push(rKey);
			}
			if (rPart.substr(0, 1) == ":"
					&& rPart.substr(rPart.length - 1, 1) == ":") {
				var rKey = rPart.substr(1, rPart.length - 2);
				this.optionalRouteArguments.push(rKey);
			}

		}

	}

	Route.prototype.getArgumentsObject = function(route) {
		var rRouteParts = route.split("/");
		var rObject = {};
		for ( var i = 0, j = this.routeParts.length; i < j; i++) {
			var rP = this.routeParts[i];//current routePart
			var cP0 = rP.substr(0, 1); //char at postion 0
			var cPe = rP.substr(rP.length - 1, 1);//char at last postion
			if ((cP0 == "{" || cP0 == ":") && (cPe == "}" || cPe == ":")) {
				var rKey = rP.substr(1, rP.length - 2);
				rObject[rKey] = rRouteParts[i];
			}
		}
		return rObject;
	}

	Route.prototype.getArgumentsValues = function(route) {
		var rRouteParts = route.split("/");
		var rArray = new Array();

		for ( var i = 0, j = this.routeParts.length; i < j; i++) {
			var rP = this.routeParts[i];//current routePart
			var cP0 = rP.substr(0, 1); //char at postion 0
			var cPe = rP.substr(rP.length - 1, 1);//char at last postion
			if ((cP0 == "{" || cP0 == ":") && (cPe == "}" || cPe == ":")) {
				rArray[rArray.length] = rRouteParts[i];
			}
		}
		return rArray;
	}

	Route.prototype.matches = function(route) {

		var incomingRouteParts = route.split("/");
		var routeMatches = true;
		if (incomingRouteParts.length < this.routeParts.length - this.optionalRouteArguments.length) {
			routeMatches
			return;
		} else {
			
			for ( var i = 0, j = incomingRouteParts.length; i < j
					&& routeMatches; i++) {
			
				var iRp = incomingRouteParts[i];//current incoming Route Part
				var rP = this.routeParts[i];//current routePart                     
				if (typeof rP == 'undefined') {
					routeMatches = false;
				} else {
					var cP0 = rP.substr(0, 1); //char at postion 0
					var cPe = rP.substr(rP.length - 1, 1);//char at last postion                   
					if ((cP0 != "{" && cP0 != ":")
							&& (cPe != "}" && cPe != ":")) {
						if (iRp != rP) {
							routeMatches = false;
						}
					} else {
						
						if (this.rules != null) {
							var rKey = rP.substr(1, rP.length - 2);
							
							if (this.rules[rKey] instanceof RegExp) {
								routeMatches = this.rules[rKey].test(iRp);
							}
							if (this.rules[rKey] instanceof Array) {
								if (this.rules[rKey].indexOf(iRp) < 0) {
									routeMatches = false;
								}
							}
						
							if (this.rules[rKey] instanceof Function) {
								routeMatches = this.rules[rKey](iRp, this
										.getArgumentsObject(route), this.scope);
							}
						} else {
							routeMatches = true;
							return;
						}
					}
				}
			}

		}
		return routeMatches;
	}
	
})();