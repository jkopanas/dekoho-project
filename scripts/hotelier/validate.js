
var ValidationObject= {	
		rules: {
	        radio: function(input) {

	            if (input.is(':radio')) { 
	              if (input.filter("[type=radio]") && input.attr("required")) {  
                	return $(".main-selection").find("[name=" + input.attr("name") + "]").is(":checked");
            	  }

	            }
	            
	            return true;
	        }
	    },
	    messages: {
	    	required: function(input){

	    		return "Field "+input.attr("for")+" is required";

	    		
	    	}
	    }
}