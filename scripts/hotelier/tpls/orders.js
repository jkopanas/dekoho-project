var orders={};
head.ready(function() {
(function () {
	var viewModel = kendo.observable({
		init: function (e) {
		    $(".mainMenu").removeClass("active");
		    $("#ordersMenu").addClass("active");
		    
		    kendo.bind($("#orders"), viewModel);
		    orders['viewmodel'] = viewModel;
		},
		print: function (e) {
			e.preventDefault();
			var id=$(e.currentTarget).data("id");
			window.open("/ajax/hooks/hotelier/index.php?file=orders.php&action=printpdf&id="+id, 'Order '+id);
			return false;
		},
		viewPage: function (e) {
			$("#orders").html(controller.templates['orders'](e[0]));
			 kendo.bind($("#orders"), viewModel);
			//$("#row-"+e[0]).remove();
		}
    });

	
	controller.registerCallbackToAction("orders","view","viewPage");
	//console.log
	
	//controller.registerCallbackToAction("orders","view","viewPage");
	
    kendo.bind($("#orders"), viewModel);
    orders['viewmodel'] = viewModel;

    $(".mainMenu").removeClass("active");
    $("#ordersMenu").addClass("active");

})();

});