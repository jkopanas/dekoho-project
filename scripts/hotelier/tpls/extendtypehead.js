head.ready(function() {
	
		$.extend($.fn.typeahead.Constructor.prototype, {    
			render: function (items) {     
		      var that = this
	
		      items = $(items).map(function (i, item) {
		        i = $(that.options.item) 
		        .attr('data-value', item[that.options.display])
                .attr('data-id', item.id)
                .attr("data-lat",item.lat)
                .attr("data-zoomLevel",item.zoomLevel)
                 .attr("data-geocoderAddress",item.geocoderAddress)
		        .attr("data-lng",item.lng);
		        i.find('a').html(that.highlighter(item))
		        return i[0]
		      })
		      items.first().addClass('active')
		      this.$menu.html(items)
		      $(this.$menu).addClass("span4");
		      return this;
		
			},
	        highlighter: function (item) {
	            var query = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&');
	            return item[this.options.display].replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
	                return '<strong>' + match + '</strong>';
	            });
	        },
			select: function () {
		        var val = this.$menu.find('.active').attr('data-value'),
		            id = this.$menu.find('.active').attr('data-id');
		        lat = this.$menu.find('.active').attr('data-lat');
		        lng = this.$menu.find('.active').attr('data-lng');
		        zoomLevel =  this.$menu.find('.active').attr('data-zoomLevel');
		        geocoderAddress = this.$menu.find('.active').attr('data-geocoderAddress');
		        this.$element
		            .val(this.updater(val, id,lat,lng,geocoderAddress,zoomLevel))
		            .change();
		        return this.hide()
		    }
		});


});