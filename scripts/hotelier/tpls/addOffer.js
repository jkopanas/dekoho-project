var addOffer={};
head.ready(function() {
(function () {
	var dataRoom = "";
	var viewModel = kendo.observable({
		hotelid: 0,
		price: "",
		orderType: "",
		ChooseHotel: false,
		ChooseRoom: false,
		init: function (e) {

		    kendo.bind($("#addOffer"), viewModel);
		    addOffer['viewmodel'] = viewModel;
		    
		    controller.dataTpl['addOffer']="";
			viewModel.set("ChooseHotel",false);
			viewModel.set("SelectPayment",false);
		    viewModel.set("CheckedRooms",[]);
		    $("#OffersContent").html("");
		
	    	if ( window['offers'] !== undefined && parseInt(window['offers'].viewmodel.get("hotelid")) ) {
	    			viewModel.get("renderPage")("addOffer/OfferRoomSelect/"+window['offers'].viewmodel.get("hotelid")+"/roomSelect");
	    			viewModel.set("hotelid",window['offers'].viewmodel.get("hotelid"));
	    			window['offers'].viewmodel.set("hotelid","0");
	    	} else {
	    			viewModel.get("renderPage")("addOffer/OfferHotelSelect/"+viewModel.get("hotelid")+"/hotelSelect");
	    	}
	    	
		},
		ShowItemTitle: function (e) {
			return e + " ( Room Offer )";
		},
		ShowRoom: function (e) {
			return (!viewModel.get("ChooseHotel"));
		},
		ShowPreview: function (e) {
			return (!viewModel.get("ChooseRoom"));
		},
		CheckedHotel: function (e) {
			$(".offerAdd").addClass("hide");
			$("#"+$(e.currentTarget).val()).removeClass("hide");
		$(e.currentTarget).attr('checked', true);
			addOffer['viewmodel'].set("hotelid",$(e.currentTarget).val());
			return;
		},
		ItemRooms: [],
		checkRoom: function (e) {
			var id = $(e.currentTarget).val();
			if ( !$(e.currentTarget).is(':checked') ) {
				$("input[name='discount-"+id+"']").val("");
				$("input[name='startdate-"+id+"']").val("");
				$("input[name='enddate-"+id+"']").val("");
				$("#discount-"+id).html("");
				$("input[name='discount-"+id+"']").removeAttr("data-type");
				$("input[name='startdate-"+id+"']").removeAttr("required");
				$("input[name='enddate-"+id+"']").removeAttr("required");		
			} else {			
				$("input[name='discount-"+id+"']").attr("data-type","number");
				$("input[name='startdate-"+id+"']").attr("required","");
				$("input[name='enddate-"+id+"']").attr("required","");
			}
			
		},
		renderPage: function (e) {
			router.applyRoute(e);
		},
		showPage: function (e,data) {
			
			var tpl = controller.renderTpl(e);
			$("#OffersContent > div").hide();
			controller.dataTpl['addOffer'][e]=data;
			data['app']=controller;
			$("#addOffer").find(".OfferBar").removeClass("active");
			$("#addOffer").find("#"+e+"-bar").addClass("active");
			$("#OffersContent").append(tpl(data));
		    kendo.bind($("#addOffer"), addOffer['viewmodel']);
		    
		},
		hotelSelectFn: function (e) {
			viewModel.get('showPage')("OfferHotelSelect",e);
		},
		RoomSelectFn: function (e) {
			
			$("#previewOrder").remove();
			viewModel.get("ItemRooms").splice(0, viewModel.get("ItemRooms").length);
			for (i in e[0]) {
				viewModel.get("ItemRooms").push(e[0][i]);
			}

			viewModel.set('ShowRoom',false);
			viewModel.get('showPage')("OfferRoomSelect",e); 
			$(".dates").kendoDatePicker({
					 min: new Date() 
			});
			$(".priceDiscount").kendoNumericTextBox({
				format: "# "+controller.symbol, //$("input[name='currencySymbol']").val()
				change: function(e) {
					start = parseInt($("#price-"+$(e.sender.element).data('id')).html());
			        var price = start-parseInt($(e.sender.element).val());
			        $("#discount-"+$(e.sender.element).data('id')).html( parseFloat((parseInt(price)*100)/start,0,2) .toFixed(2)+" %");
			    }
			});
			
		},
		ShowPreviewFn: function (e) {

		},
		selectHotel: function (e) {
			e.preventDefault();
			if ( viewModel.get("hotelid")) {
				viewModel.set("ChooseHotel",true);
				viewModel.get("renderPage")("addOffer/OfferRoomSelect/"+viewModel.get("hotelid")+"/roomSelect");
			} else {
				$("#addOffer").find('#messages').html("At least one hotel should be selected");
				$("#addOffer").find('#messages').show().delay(3000).fadeOut('slow');
			}
			
		},
		calculateDiscount: function(e) {
			
		},
		changeDiscount:function (e) {
			e.preventDefault();
			$(e.currentTarget).attr("data-id",$(e.currentTarget).val());
			router.applyRoute("addOffer/previewOrder/"+$(e.currentTarget).val()+"/CheckCoupon");
		},
		CouponFn: function (e) {
			var id;
			for (i in e[0]) {
				$("#"+i).attr("class","");
				$("#"+i).addClass(e[0][i]);
				id=i.split("-");
				if (e[0][i] == "icon-ok") {
					$("#discount-"+id[1]).html("100%");
					$("#amount-"+id[1]).html("0");
					$("#amount-"+id[1]).attr("data-price","0");
				} else {
					$("#discount-"+id[1]).html("");
					$("#amount-"+id[1]).html(viewModel.get("price"));
					$("#amount-"+id[1]).attr("data-price",viewModel.get("price"));
					$("#discount-"+id[1]).html("");
				}
			}
			var pr = 0;
			$(".price").each(function(index,value) {
				pr = pr + parseInt($(value).attr("data-price"));
			});
			$("#totalAmount").html(pr);
		},
		ApplyOffer: function (e) {
			var data = mcms.formData($('#addOffer').find(".postedData"),{ getData:false });
			if ($('input[type=checkbox]:checked').length == 0 ) {
				$("#addOffer").find('#messages').html("At least one room should be selected");
				$("#addOffer").find('#messages').show().delay(3000).fadeOut('slow');
			} else if (controller.validator['addOffer'].validate() ) {
				dataRoom = data;
				viewModel.set("ChooseRoom",true);	
				viewModel.get("renderPage")("addOffer/previewOrder/"+viewModel.get("hotelid")+"/orderPreview");
				$("#messages").hide();
				
			} else {
				
				controller.validator['addOffer']._errorTemplate= function(){};
				if (!controller.validator['addOffer'].validate()) {
					var err=controller.validator['addOffer'].errors();
					$("#addOffer").find('#messages').html("");
					for (i in err ) {
							$("#addOffer").find('#messages').append(err[i]+"<br/>");
							$("#addOffer").find('#messages').show();
					} 
					return;
				}
				
				
			}
		},
		orderPreviewFn: function (e) {
			
			var obj = dataRoom;ar =[];field ="";objs=[];
			for (i in obj) {
			   field= i.split("-");
				if ( field[0] == "id") {
					ar.push(field[1]);
				}
			}
			var start="",end ="";
			for (j in ar) {
				 start= new Date(obj["startdate-"+ar[j]]).getTime();
				 end= new Date(obj["enddate-"+ar[j]]).getTime();
				 objs.push( { "id": ar[j],"discount": obj["discount-"+ar[j]],"startdate": start,"enddate": end,"title": obj["title-"+ar[j]] } );
			}

			e[0]['app'] =  controller;
			e[0]['price'] = viewModel.get("price");
			e[0]['items'] = objs;
			viewModel.get('showPage')("previewOrder",e);

		},
		VisibleHotel: function (e) {
			e.preventDefault();
			$("#addOffer").find(".OfferBar").removeClass("active");
			$("#addOffer").find("#OfferHotelSelect-bar").addClass("active");
			$("#OffersContent > div").hide();
			if ($("#OfferHotelSelect").length == 0 ) {
				viewModel.get("renderPage")("addOffer/OfferHotelSelect/"+viewModel.get("hotelid")+"/hotelSelect");
			} else {
				$("#OffersContent").find("#OfferHotelSelect").show();
			}
			$("#OffersContent").find("#OfferRoomSelect").remove();
			$("#OffersContent").find("#previewOrder").remove();
		},
		VisibleRoom: function (e) {
			e.preventDefault();
			$("#OfferRoomSelect").remove();
			viewModel.get("renderPage")("addOffer/OfferRoomSelect/"+viewModel.get("hotelid")+"/roomSelect");
			return;
		},
		VisiblePayment: function(e) {
			e.preventDefault();
			if ($("#OffersContent").find("#previewOrder").length == 0 ) {
				if ($("#OffersContent").find("#OfferRoomSelect").length == 0 ) {
					viewModel.get("renderPage")("addOffer/OfferRoomSelect/"+viewModel.get("CheckedHotel")+"/roomSelect");
					return;
				}
				 viewModel.get("renderPage")("addOffer/previewOrder/"+viewModel.get("CheckedHotel")+"/orderPreview");
				return;
			}
			$("#addOffer").find(".OfferBar").removeClass("active");
			$("#addOffer").find("#previewOrder-bar").addClass("active");
			$("#OffersContent > div").hide();
			$("#OffersContent").find("#previewOrder").show();
		},
		SelectBilling: 0,
		BillingOption: function (e) {
			if ( parseInt(viewModel.get('SelectBilling')) != 0 && $("#payment option:selected").val() != "ShowPayment" ) {
				viewModel.set("BuyNow",false);
			} else {
				viewModel.set("BuyNow",true);
			}
		},
		SelectOption: function (e) {
			var valueFn = $(e.currentTarget).val();
				viewModel.set("showStartDate",true);
				viewModel.set("credit_card",$("#"+$(e.currentTarget).attr("id")+" option:selected").text());
				viewModel.get(valueFn)();
				//var PaymentData=$.parseJSON($("textarea#PaymentData").html());
				//$("#ShowPay").html(controller.renderTpl(valueFn)(PaymentData));
		},
		ShowPayment: function (e) {
			viewModel.set("BuyNow",true);
		},
		PayPal: function (e) {
				if (parseInt(viewModel.get('SelectBilling'))) {
					viewModel.set("BuyNow",false);
				} else {
					viewModel.set("BuyNow",true);
				}
		},
		jcc: function (e) {
			if (parseInt(viewModel.get('SelectBilling'))) {
				viewModel.set("BuyNow",false);
			} else {
				viewModel.set("BuyNow",true);
			}
		},
		finishPayment: function (e) {
			if ($("#payment option:selected").val() == "PayPal") {
				router.applyRoute("payment/addOffer/StartCheckout/rooms");
			} else if ($("#payment option:selected").val() == "jcc") {
				router.applyRoute("payment/addOffer/startgcc/rooms");
			}
		},	
		checkoutstart: function (data) {

			
			if ($("#payment option:selected").val() == "PayPal" ) {
				window.location=data[0];
			} else if ($("#payment option:selected").val() == "jcc") {
				$("#ShowPay").html(controller.renderTpl("jcc")(data[0].PaymentData));
				$("#paymentForm").submit();
			}
		},
		BuyNow: true,
		showStartDate: true
		
    });
	

	controller.registerCallbackToAction("addOffer","rooms","checkoutstart");
	controller.registerCallbackToAction("addOffer","hotelSelect","hotelSelectFn");
	controller.registerCallbackToAction("addOffer","roomSelect","RoomSelectFn");
	controller.registerCallbackToAction("addOffer","orderPreview","orderPreviewFn");
	controller.registerCallbackToAction("addOffer","CheckCoupon","CouponFn");	
	
	viewModel.set("price",$("input[name='price']").val());
	//viewModel.set("orderType",$("input[name='order_type_id']").val());
	
    kendo.bind($("#addOffer"), viewModel);
    addOffer['viewmodel'] = viewModel;


    	if ( window['offers'] !== undefined && parseInt(window['offers'].viewmodel.get("hotelid"))) {
    		viewModel.get("renderPage")("addOffer/OfferRoomSelect/"+window['offers'].viewmodel.get("hotelid")+"/roomSelect");
    		viewModel.set("hotelid",window['offers'].viewmodel.get("hotelid"));
    		window['offers'].viewmodel.set("hotelid","0");
    	} else {
    		viewModel.get("renderPage")("addOffer/OfferHotelSelect/"+viewModel.get("hotelid")+"/hotelSelect");
    		controller.validator['addOffer']._errorTemplate= function(){};
    	}
})();


});