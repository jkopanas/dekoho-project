var Locations ={};
var country = {};
var region = {};
var city = {};
var x = {};
var CurrentTab = "";
var currentTemplate= $("#templateid").val();
var firstInsert= 0;
var titleView = currentTemplate.charAt(0).toUpperCase() + currentTemplate.slice(1);


head.ready(function() {
	
	(function() {
		
		var arrTable= {"country": "geo_country","region": "geo_region","city": "geo_cityr"};
		var deleteItem=0;
		var markerListeners = [
	                           //    	{ listener : 'dragend',callback : 'hanleMarkerChange' }
	                           {
	                               listener : 'dragend',
	                               callback : 'reverseGeoCodeLocation',
	                               ListenerReverseGeoCoding:true
	                           }	
	                           ];

		var viewModel = kendo.observable({
			insertBtn: true,
			updateBtn: true,
			hotelidTriggered: false,
			newLocation: false,
			hotelid: true,
			SelectId: "",
			SelectTitle: "",
			SelectGeocoderAddress: "",
			SelectZoomLevel: 1,
			SelectLat: 0,
			SelectLng: 0,
			getTable: function () {
				return arrTable[currentTemplate];
			},
			selection: { "extra": [] },
			init: function (e) {

				//console.log(e[0]);
				if ($.isArray(e[0].location)) {
					for (i in e[0].location) {
						if (e[0].location[i].id == e[0].itemid) {
							viewModel.set("SelectGeocoderAddress",e[0].location[i].geocoderAddress);
							viewModel.set("SelectLng",e[0].location[i].lng);
							viewModel.set("SelectLat",e[0].location[i].lat);
							viewModel.set("SelectZoomLevel",parseInt(e[0].location[i].zoomLevel));
						}
					}
				} else {
					viewModel.set("SelectGeocoderAddress",e[0].location.geocoderAddress);
					viewModel.set("SelectLng",e[0].location.lng);
					viewModel.set("SelectLat",e[0].location.lat);
					viewModel.set("SelectZoomLevel",parseInt(e[0].location.zoomLevel));					
				}
				currentTemplate= $("#templateid").val();
				titleView = currentTemplate.charAt(0).toUpperCase() + currentTemplate.slice(1);
				kendo.bind($("#"+currentTemplate), viewModel);
				this[currentTemplate]['viewmodel'] = viewModel;
				controller.registerCallbackToAction(currentTemplate, "update", (currentTemplate == "hotel" ) ? "updateHotel" : "updateForm");
				controller.registerCallbackToAction(currentTemplate, "updateTitle", "updateTitleForm");
				controller.registerCallbackToAction(currentTemplate, "insert", "insertForm");
				controller.registerCallbackToAction(currentTemplate, "delete", "deleteForm");
				controller.registerCallbackToAction(currentTemplate,"enableExtras","enableExtrasReturn");	
				controller.registerCallbackToAction(currentTemplate,"disableExtras","disableExtrasReturn");	
				controller.registerCallbackToAction(currentTemplate, "FirstInsert", (currentTemplate == "hotel" ) ? "insertHotel" : "updateForm");
				
				x={};
				viewModel.get("startMap")();
				
			    $(".SelectHotel").removeClass("active");
			    $("#locationTab").addClass("active");
			    
			} ,
			setActive: function (e) {
				
			},
			Press: function (e) {
				if ( e.charCode === 13 ) {		
					viewModel.set("SelectGeocoderAddress",$(e.currentTarget).val());
					viewModel.get("findPoint")(e);
				}
			},
			displaypopup: function(e) {
				e.preventDefault();
				router.applyRoute($(e.currentTarget).data('route'));
			},
			newposition: function(e) {
				
				var data = mcms.formData($('#GetLocation').find(".loc"),{});
				x.clearBoundingBoxes();
				x.NewPosition(data.lat,data.lng);
				x.getMap().setZoom(parseInt(data.zoomLevel));
				
			},
			findPointByName: function (name) {

			    if (name instanceof Object) {
			    	name.preventDefault();
			    	name=$(name.currentTarget).data('name');
			    }
				var mark = x.Markers();
				mark[0].setMap(null);
				mark.pop();
			    x.geoCode({
			    	markers : {
		        		markerListeners:markerListeners, 
						draggable:  (currentTemplate == "Locations") ? true : false
					},
			        geocoder :{
			            address: name,
			            language:'el',
			            region:'GR'
			        }
			    }
			    ,'returnGeocoderObjectLocation');
			    
			},
			findPoint: function(e) {
				e.preventDefault();
				viewModel.set("hotelid",false);
			    viewModel.get("findPointByName")(viewModel.get("SelectGeocoderAddress"));
			},
			UpdateMap: function (e) {
				e.preventDefault();
				$(".unstyled").find("a").removeClass('bold');
				$(e.currentTarget).addClass("bold");
				 $('#geocodingResults').html('');
				id= $(e.currentTarget).attr("id");
				id=id.split("-");
				viewModel.set("SelectId",id[1]);
				viewModel.set("insertBtn",true);
				viewModel.set("updateBtn",false);
				viewModel.set("SelectId",id[1]);
				viewModel.set("SelectTitle",$(e.currentTarget).html());
				viewModel.set("SelectGeocoderAddress",$(e.currentTarget).data('geocoderaddress'));
				viewModel.set("SelectLng",$(e.currentTarget).data('lng'));
				viewModel.set("SelectLat",$(e.currentTarget).data('lat'));
				viewModel.set("SelectZoomLevel",$(e.currentTarget).data('zoomlevel'));
				viewModel.get("loadMap")();
			},
			deleteForm: function(e) {
			

				$("#itemid-"+deleteItem).parents("li:first").remove();
				$("#itemid-"+idItemSaved).trigger('click');
				viewModel.set("updateBtn",true);
				$('html, body').animate({scrollTop:0}, 'slow');
				$("#messages").html(titleView+" was deleted.");
				$("#messages").show().delay(3000).fadeOut('slow');
				
			},
			updateForm: function(e) {
				
				t=controller.renderTpl('LocationToolbar');
				e[0]['app'] = controller;
				$("#LocationToolbar").replaceWith(t(e[0]));
				k = controller.renderTpl("LocationRender");
				$("#LocationRender").replaceWith(k({ "table": viewModel.get("getTable")(),"results": e[0].location,"countItems": e[0].countItem}));
				kendo.bind($("#"+currentTemplate), viewModel);
				$("#itemid-"+e[0].itemid).trigger('click');
				viewModel.set("updateBtn",true);
				$('html, body').animate({scrollTop:0}, 'slow');
				$("#messages").html("A new "+titleView+" has been assigned to the Hotel.");
				$("#messages").show().delay(3000).fadeOut('slow');	
			},
			updateHotel: function (e) {
				viewModel.set("hotelid",true);
				$('html, body').animate({scrollTop:0}, 'slow');
				$("#messages").html("Hotel's map has been saved.");
				$("#messages").show().delay(3000).fadeOut('slow');	
				$("#geocodingResults").html("");
			},
			insertHotel: function (e) {
				viewModel.set("hotelid",true);
				firstInsert= 0;
				$('html, body').animate({scrollTop:0}, 'slow');
				$("#messages").html("Hotel's map has been saved.");
				$("#messages").show().delay(3000).fadeOut('slow');	
				$("#geocodingResults").html("");
			},
			insertForm: function(e) {
				t=controller.renderTpl('LocationToolbar');
				e[0]['app'] = controller;
				$("#LocationToolbar").replaceWith(t(e[0]));
				k = controller.renderTpl("LocationRender");
				$("#LocationRender").replaceWith(k({ "table": viewModel.get("getTable")(),"results": e[0].location,"countItems": e[0].countItem}));
				kendo.bind($("#"+currentTemplate), viewModel);
				$("#itemid-"+e[0].itemid).trigger('click');
				viewModel.set("updateBtn",true);
				$('html, body').animate({scrollTop:0}, 'slow');
				$("#messages").html("A new "+titleView+" has been assigned to the Hotel.");
				$("#messages").show().delay(3000).fadeOut('slow');	
			},
			updateTitleForm: function(e) {
				
				k = controller.renderTpl("LocationRender");		
				$("#LocationRender").replaceWith(k({ "table": viewModel.get("getTable")(),"results": e[0].location,"countItems": e[0].countItem}));
				kendo.bind($("#"+currentTemplate), viewModel);
				$("#itemid-"+e[0].itemid).trigger('click');
				viewModel.set("updateBtn",true);
				
				$('html, body').animate({scrollTop:0}, 'slow');
				$("#messages").html("The "+titleView+"'s title has been changed");
				$("#messages").show().delay(3000).fadeOut('slow');	
				
				
			},			
			DeleteItem: function (e) {
				e.preventDefault();
				$('#confirmDiv').confirmModal({
					  heading:'Confirm Delete',
					  body:'Are you sure you want to delete this Point.',
					  callback: function() {
							id= $(e.currentTarget).attr("id");
							id=id.split("-");
							deleteItem = id[1];
							router.applyRoute("hotelManager/Locations/"+currentTemplate+"/"+id[1]+"/delete");
					   }
				   });
				
			},
			disableExtrasReturn: function (e) {
				for (i in e[0]) {
				
					$("#extra-"+e[0][i]).removeClass("activate");
					$("#extra-"+e[0][i]).addClass("pending");		
				}
		        
			},
			enableExtrasReturn: function (e) {
				for (i in e[0]) {
				$("#extra-"+e[0][i]).removeClass("pending");
				$("#extra-"+e[0][i]).addClass("activate");
				}
		        
			},
			enableDisableSelected: function (e) {
				 e.preventDefault();
				  ar = viewModel.get("selection.extra");
				  viewModel.set("selected",ar.join());
				  var action = $(e.currentTarget).data("action");
				  router.applyRoute("admin/Locations/"+action+"/"+$(e.currentTarget).data("id"));
			},
			SaveExtraField: function(e) {
				e.preventDefault();
				router.applyRoute("hotelManager/Locations/"+controller.getArguments[1]+"/saveExtra");
				$('html, body').animate({scrollTop:0}, 'slow');
				$("#success-extras").show().delay(3000).fadeOut('slow');	
			},
			SavePoint: function(e) {
				e.preventDefault();	
				var action=$(e.currentTarget).data("action");
				if ( ( currentTemplate == "country" || currentTemplate == "Locations" ) && firstInsert && action == "update" ) {
					action = "FirstInsert";
				} 
				idItemSaved=viewModel.get("SelectId");
				t = ( currentTemplate == "Locations" ) ? "" : currentTemplate+"/";
				router.applyRoute("hotelManager/Locations/"+t+$(e.currentTarget).data("id")+"/"+action);
			},
			UpdateTitle: function (e) {
				
				e.preventDefault();
				router.applyRoute("hotelManager/Locations/"+currentTemplate+"/"+controller.getArguments[1]+"/updateTitle");
				
			},
			loadMap : function(e) {
				
				var data = mcms.formData($('#GetLocation').find(".loc"), {});
				if ( $.isEmptyObject(x) ) {

					$('.map').googleMaps({
						zoom : parseInt(data.zoomLevel),
						clat :  data.lat,
						clng :  data.lng,
						fitbounds: false,
						createMarker : {
							markerListeners:markerListeners, 
							draggable:  (data.draggable == "0") ? false : true
						}
					}).data('Map').init();

					x = $('.map').data('Map');
				} else {
			
					viewModel.get("newposition")();
				}
				
			},
			calculateMeter: function (e) {
				var name=$(e.currentTarget).data("name");
				var km = parseFloat($(e.currentTarget).val(),0,2)/1000;
				$("#calc-"+name).html(km+" km");
			},
			startMap: function (e) {
				
				if(typeof google != 'undefined')
		        {		
					
					if (parseInt(controller.dataTpl[currentTemplate].itemid)) {
						$("#itemid-"+controller.dataTpl[currentTemplate].itemid).trigger('click');
					 	viewModel.set("updateBtn",true);	 	
					 	firstInsert =0;
						idItemSaved = controller.dataTpl[currentTemplate].itemid;
					} else {
						firstInsert =1;
						viewModel.get("loadMap")();
					}
					
				}
			}
		});

		

			kendo.bind($("#"+currentTemplate), viewModel);
			country['viewmodel'] = viewModel;
			region['viewmodel'] = viewModel;
			city['viewmodel'] = viewModel;
			Locations['viewmodel'] = viewModel;

			controller.registerCallbackToAction(currentTemplate, "update", (currentTemplate == "Locations" ) ? "updateHotel" : "updateForm" );
			controller.registerCallbackToAction(currentTemplate, "updateTitle", "updateTitleForm");
			controller.registerCallbackToAction(currentTemplate, "insert", "insertForm");
			controller.registerCallbackToAction(currentTemplate, "delete", "deleteForm");
			controller.registerCallbackToAction(currentTemplate,"enableExtras","enableExtrasReturn");	
			controller.registerCallbackToAction(currentTemplate,"disableExtras","disableExtrasReturn");	
			controller.registerCallbackToAction(currentTemplate, "FirstInsert", (currentTemplate == "Locations" ) ? "insertHotel" : "updateForm");
		
	    $(".SelectHotel").removeClass("active");
	    $("#locationTab").addClass("active");
	    viewModel.get("startMap")();

	    
	})();

});

function reverseGeoCodeLocation(e) {


	
	 var latLng = e.getPosition();
	 Locations['viewmodel'].set("SelectGeocoderAddress",e.results.formatted_address);
	 Locations['viewmodel'].set("SelectLng",latLng.lng());
	 Locations['viewmodel'].set("SelectLat",latLng.lat());
	 Locations['viewmodel'].set("SelectZoomLevel",x.getMap().getZoom());
	 Locations['viewmodel'].set("hotelid",false);

}

function returnGeocoderObjectLocation(results) {
	
	 if (results.length > 0)
	 {
		 var latLng = results[0].marker.getPosition();

		 this[currentTemplate]['viewmodel'].set("SelectId","0");
		 this[currentTemplate]['viewmodel'].set("SelectTitle","");
		 this[currentTemplate]['viewmodel'].set("SelectGeocoderAddress",results[0].formatted_address);
		 this[currentTemplate]['viewmodel'].set("SelectLng",latLng.lng());
		 this[currentTemplate]['viewmodel'].set("SelectLat",latLng.lat());
		 this[currentTemplate]['viewmodel'].set("SelectZoomLevel",x.getMap().getZoom());
		 this[currentTemplate]['viewmodel'].set("updateBtn",true);
		 this[currentTemplate]['viewmodel'].set("insertBtn",false);

		 $('#geocodingResults').html('');
         $.each(results, function(key, val) {
             $('#geocodingResults').append('<li><a href="#" class="results" data-name="'+val.formatted_address+'" data-bind="click: findPointByName" rel="marker-' + key + '">' + val.formatted_address + '</a></li>');
         });
         kendo.bind($("#"+currentTemplate), this[currentTemplate]['viewmodel']);
         
     }
	 
	 


}


function loadMaps() {


	this[currentTemplate]['viewmodel'].get("startMap")();


}