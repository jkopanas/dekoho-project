var notifications={};
var notificationsDetails={};
head.ready(function() {
(function () {
	var selectall=false;
	var viewModel = kendo.observable({
		init: function (e) {
			
		    $(".mainMenu").removeClass("active");
		    $("#notificationsMenu").addClass("active");
		    kendo.bind($("#notifications"), viewModel);
		    kendo.bind($("#notificationsDetails"), viewModel);
		    
		    notifications['viewmodel'] = viewModel;
		    notificationsDetails['viewmodel'] = viewModel;
		},
		allselect: false,
		selectAll: function (e) {
		if (!selectall) { selectall=true; } else { selectall =false; }

			$("#notifications").find(':checkbox:not(:first)').each( function(index,value) {
			
				if ( !$(value).is(":checked") && selectall == true ) {
					$(value).trigger('click');
				}	else if ($(value).is(":checked") && selectall == false) {
					$(value).trigger('click');
				}
			});		
			
		},

		selectedfn: function (e) {	
			if (e === undefined ) {return;}
			ar = viewModel.get("selection.notifications");		
			$(e.currentTarget).parents("tr").find("td,th").toggleClass("highlight");
		},
		selected: "",
		selection: { "notifications": [] },
		deleteSelected: function (e) {
			  e.preventDefault();
			  ar = viewModel.get("selection.notifications");
			  viewModel.set("selected",ar.join());
			 
			  $('#confirmDiv').confirmModal({
				  heading:'Confirm Delete',
				  body:'Are you sure you want to delete the selected Notifications.',
				  callback: function() {
					  router.applyRoute("notifications/0/deleted");		
				   }
			   });
		},
		deleteNotifications: function (e) {
			
			for (i in e[0] ) {
				$("#notifications-"+e[0][i]).remove();
			}
			
		},
		viewPage: function (e) {
			$("#notifications").html(controller.templates['notifications'](e[0]));
			 kendo.bind($("#notifications"), viewModel);
		},
		viewDetails: function(e) {
			$("#notifications").html(controller.templates['notificationsDetails'](e[0]));
			 kendo.bind($("#notifications"), viewModel);
		}
    });

	controller.registerCallbackToAction("notifications","deleted","deleteNotifications");
	controller.registerCallbackToAction("notifications","view","viewPage");
	controller.registerCallbackToAction("notificationsDetails","details","viewDetails");	
	
	
    kendo.bind($("#notifications"), viewModel);
    kendo.bind($("#notificationsDetails"), viewModel);
    
    notifications['viewmodel'] = viewModel;
    notificationsDetails['viewmodel'] = viewModel;

	
    $(".mainMenu").removeClass("active");
    $("#notificationsMenu").addClass("active");
    

})();

});