var billing_profiles ={};
var new_billing= {};
head.ready(function() {
	
(function () {
	
	var viewModel = kendo.observable({     
		init: function(e) {
			  kendo.bind($("#billing_profiles"), viewModel);
			  kendo.bind($("#new_billing"), viewModel);
			 // controller.validator['new_billing']._errorTemplate = kendo.template('<br /><span class="k-widget k-tooltip k-tooltip-validation">#=message#</span>');
		},
		displaypopup: function(e) {
			e.preventDefault();
			router.applyRoute($(e.currentTarget).data('route'));
		},
	    deleteprofile: function(e){
	    	e.preventDefault();
	    	var id = $(e.currentTarget).data('id');
	    	
	    	$('#confirmDiv').confirmModal({
				  heading:'Confirm Delete',
				  body:'Are you sure you want to delete this profile',
				  callback: function() {
					  router.applyRoute("profileManager/billing_profiles/delete/"+id);
				   }
			   });
	    },
	    save: function(e) {
			e.preventDefault();	
			if (controller.validator['new_billing'].validate()) {
				router.applyRoute("profileManager/new_billing/save");
				$("#cl").trigger("click");
			}
		},
		update: function(e) {
			e.preventDefault();
			if (controller.validator['new_billing'].validate()) {
				var id =$(e.currentTarget).data("id");
				router.applyRoute("profileManager/new_billing/save/"+id);
				$("#cl").trigger("click");
			}
		},	
		deleteForm: function (e) {
			$("#billingpr"+e[0]).remove();
	    	$('html, body').animate({scrollTop:0}, 'slow');
			$('#success-billing').show().delay(3000).fadeOut('slow');
		},
		saveForm: function(e) {
	
			var t=controller.renderTpl("update-billing");
			if ( $("#billingpr"+e[0].id).length > 0  ) {
				$("#billingpr"+e[0].id).replaceWith(t({ "a": e[0] }));
			} else {
				$("#billing_profiles > tbody").append(t({ "a": e[0] }));
			}
			kendo.bind($("#billing_profiles"), viewModel);
			
	    }
    });

	
	//controller.validator['new_billing']._errorTemplate = kendo.template('<br/><span class="k-widget k-tooltip k-tooltip-validation">#=message#</span>');
	
	controller.registerCallbackToAction("billing_profiles","delete","deleteForm");
	controller.registerCallbackToAction("new_billing","save","saveForm");
	
	
  kendo.bind($("#billing_profiles"), viewModel);
  kendo.bind($("#new_billing"), viewModel);
  
  billing_profiles['viewmodel']=viewModel;
  new_billing['viewmodel']= viewModel;
})();

});