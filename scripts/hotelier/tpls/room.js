var room={};
head.ready(function() {
(function () {
	var multi="";
	var viewModel = kendo.observable({
		roomsfacilities: true,
		selection: { "filters": [] },
		init: function () {
		
			viewModel.get('multiselect')();
			$('.admin').attr('checked',false);
			kendo.bind($("#room"), viewModel);
			room['viewmodel']=viewModel;
			$(".SelectHotel").removeClass("active");
			$("#roomsTab").addClass("active");
		},
		displaypopup: function(e) {
			router.applyRoute($(e.currentTarget).data('route'));
		},
		enableDisableSelected: function (e) {
			 e.preventDefault();
			 ar = viewModel.get("selection.filters");
			  viewModel.set("selected",ar.join());
			  var action = $(e.currentTarget).data("action");
			  var id = $(e.currentTarget).data("id");
			 if (action == "deleteFilters") {
				 $('#confirmDiv').confirmModal({
					  heading:'Confirm Delete',
					  body:'Are you sure you want to delete the selected '+id,
					  callback: function() {
						  router.applyRoute("admin/room/"+action+"/"+id);
					   }
				   });
			 } else {
			  router.applyRoute("admin/room/"+action+"/"+id);
			 }
		},
		showAdmin: function (e) {	
		},
		disableFiltersReturn: function (e) {
			var item=e[0].id.split(",");
			for (i in item) {
				$("#li-"+e[0].table+"-"+item[i]).find("label").addClass("pending");		
			}
			$("#btn-"+e[0].table).trigger("click");
	
		},
		enableFiltersReturn: function (e) {

			var item=e[0].id.split(",");
			for (i in item) {			
				$("#li-"+e[0].table+"-"+item[i]).find("label").removeClass("pending");		
			}
		
			$("#btn-"+e[0].table).trigger("click");
			
		},
		deleteFiltersReturn: function (e) {
			var item=e[0].id.split(",");
			for (i in item) {
				$("select[name="+e[0].table+"] option[value='"+item[i]+"']").remove();
			}
			multi.multiselect('rebuild');
			multi.multiselect('refresh');
			$("#btn-"+e[0].table).trigger("click");		
		},
		newRoom: function (e) {
			return (controller.dataTpl['room'].room.id == "" ) ? true : false;
		},
		saveForm: function (e) {
	console.log(e[0].room.roomsfacilities);
			var t = controller.renderTpl("selectProperties");
			
			$("select#roomsfacilities").html(t({id: "roomsfacilities", module: "rooms", table: "roomsfacilities", data: e[0].room.roomsfacilities}));
			
			multi.multiselect('rebuild');
			multi.multiselect('refresh');
			kendo.bind($("#room"), viewModel);
		//	controller.executeRoute("dashboard");
		},
		addoption: function(e) {
			e.preventDefault();
			var elem =$(e.currentTarget);
			if ($("."+elem.data('id')).val() == "" )  { return; }		
			$('#'+elem.data('id')).append($('<option value="'+$("."+elem.data('id')).val()+'" selected>').text($("."+elem.data('id')).val())); 
			multi.multiselect('rebuild');
			multi.multiselect('refresh');
			viewModel.get("visible")(e);
		},
		RedirectHotels: function(e) {
			window.location.hash = "hotelManager/rooms/room/"+e[0];
		},
		visible: function (e) {
			e.preventDefault();
			if (viewModel.get("roomsfacilities")) {  viewModel.set("roomsfacilities",false); } else { viewModel.set("roomsfacilities",true); }
		},
		save: function(e) {
			e.preventDefault();
			if (controller.validator['room'].validate()) {	
				var elem =$(e.currentTarget);
				router.applyRoute("hotelManager/rooms/room/"+elem.data('id')+"/save");
				$('html, body').animate({scrollTop:0}, 'slow');
				$('#success-room').show().delay(3000).fadeOut('slow');	
			}	
		},
		add: function(e) {
			e.preventDefault();
			//router.applyRoute("hotelManager/hotel/new");
		},
		create: function (e) {
			e.preventDefault();
			if (controller.validator['room'].validate()) {
			var elem =$(e.currentTarget);
			router.applyRoute("hotelManager/rooms/room/"+elem.data('id')+"/create");
			$('html, body').animate({scrollTop:0}, 'slow');
			$('#success-room-create').show().delay(3000).fadeOut('slow');	
			}
		},
		RejectField: function (e) {
			  e.preventDefault();
			  var action = $(e.currentTarget).data("action");
			  var id = $(e.currentTarget).data("id");
			  router.applyRoute("admin/room/"+action+"/"+id);
		},
		AcceptField: function(e) {
			  e.preventDefault();
			  var action = $(e.currentTarget).data("action");
			  var id = $(e.currentTarget).data("id");
			  router.applyRoute("admin/room/"+action+"/"+id);
		},
		AcceptFieldReturn: function (e) {
		
			var field=Object.keys(e[0]);
			var t= controller.renderTpl("MessageTpl");
			$("#Admin-"+field).remove();
			$("#msg").html(t({"status": "alert-success", "message": "You have accept field: "+field })).show().delay(3000).fadeOut('slow');
		},
		RejectFieldReturn: function (e) {
			
			var field=Object.keys(e[0]);
			var t= controller.renderTpl("MessageTpl");
			$("#Admin-"+field).remove();
			$("#msg").html(t({"status": "alert-success", "message": "You have rejected field: "+ field })).show().delay(3000).fadeOut('slow');
			$("input[name="+field+"]").val(e[0][field]);
		},
		multiselect: function(e) {

			multi = $('.multiselect').multiselect({
			      buttonClass: 'btn input-large',
			      buttonWidth: '500px',
			      maxHeight: 200,
			      width: "100%",
			      onChange: function(element, checked) {
			   
			      },
			      buttonText: function(options) {
			        if (options.length == 0) {
			          return 'None selected ';
			        }
			        else if (options.length > 3) {
				          return options.length + ' selected  <b class="caret"></b>';
				        }
			        else {
			          var selected = '';
			          options.each(function() {
			            selected += $(this).text() + ', ';
			          });
			          return selected.substr(0, selected.length -2);
			        }
			      }
			    });
			
			
		},
		addClassPosteddata: function (e) {
			e.preventDefault();
			$(e.currentTarget).addClass("postedData");
		}
    });
	
	
	controller.registerCallbackToAction("room","RejectField","RejectFieldReturn");	
	controller.registerCallbackToAction("room","AcceptField","AcceptFieldReturn");
	controller.registerCallbackToAction("room","deleteFilters","deleteFiltersReturn");	
	controller.registerCallbackToAction("room","enableFilters","enableFiltersReturn");	
	controller.registerCallbackToAction("room","disableFilters","disableFiltersReturn");	
	controller.registerCallbackToAction("room","save","saveForm");
	controller.registerCallbackToAction("room","create","RedirectHotels");
	
	viewModel.get('init')({});
	

})();

});