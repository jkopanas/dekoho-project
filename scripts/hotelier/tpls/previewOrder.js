
var previewOrder = {};
head.ready(function() {
	
(function () {
	

	
	var viewModel = kendo.observable({     
		init: function (e) {
			  kendo.bind($("#previewOrder"), viewModel);
		},
		displaypopup: function(e) {
			router.applyRoute($(e.currentTarget).data('route'));
		},
		ShowItemTitle: function (e) {
			return e + " ( Publish Hotel )";
		},
		SelectBilling: 0,
		BillingOption: function (e) {
			if ( parseInt(viewModel.get('SelectBilling')) != 0 && $("#payment option:selected").val() != "ShowPayment" ) {
				viewModel.set("BuyNow",false);
			} else {
				viewModel.set("BuyNow",true);
			}
		},
		SelectOption: function (e) {
			var valueFn = $(e.currentTarget).val();
				viewModel.set("showStartDate",true);
				viewModel.set("credit_card",$("#"+$(e.currentTarget).attr("id")+" option:selected").text());
				viewModel.get(valueFn)();
		},
		ShowPayment: function (e) {
			viewModel.set("BuyNow",true);
		},
		PayPal: function (e) {
				if (parseInt(viewModel.get('SelectBilling'))) {
					viewModel.set("BuyNow",false);
				} else {
					viewModel.set("BuyNow",true);
				}
		},
		jcc: function (e) {
			if (parseInt(viewModel.get('SelectBilling'))) {
				viewModel.set("BuyNow",false);
			} else {
				viewModel.set("BuyNow",true);
			}
		},
		finishPayment: function (e) {
			if ($("#payment option:selected").val() == "PayPal") {
				router.applyRoute("payment/previewOrder/StartCheckout/hotel");
			} else if ($("#payment option:selected").val() == "jcc") {
				router.applyRoute("payment/addOffer/startgcc/rooms");
			}
		},
		checkoutstart: function (data) {

			
			if ($("#payment option:selected").val() == "PayPal" ) {
				window.location=data[0];
			} else if ($("#payment option:selected").val() == "jcc") {
				$("#ShowPay").html(controller.renderTpl("jcc")(data[0].PaymentData));
				$("#paymentForm").submit();
			}
		},
		BuyNow: true,
		showStartDate: true
    });
	
	
	
  controller.registerCallbackToAction("previewOrder","hotel","checkoutstart");
	
  kendo.bind($("#previewOrder"), viewModel);
  previewOrder['viewmodel']=viewModel;
  
})();

});