policy={};
var selectall=false;
head.ready(function() {
(function () {
	var multi="";
	var viewModel = kendo.observable({
		init: function (e) {
			
			kendo.bind($("#policy"),  policy['viewmodel']);
			$(".SelectHotel").removeClass("active");
		    $("#policyTab").addClass("active");	    
		    viewModel.get("CheckShowTranslation")();
		
		},
		create: false,
		edit: false,
		ShowTranslate: true,
		selection: { "extra": [] },
		displaypopup: function(e) {
			e.preventDefault();
			router.applyRoute($(e.currentTarget).data('route'));
		},
		CheckShowTranslation: function () {
			var flag = false;
			resultsPolicy=$.parseJSON($("textarea#policyData").html());
			for (i in resultsPolicy.extrafield.policy) {
				if ( resultsPolicy.extrafield.policy[i].value  != "" && resultsPolicy.extrafield.policy[i].type == "area" ) {
					viewModel.set("ShowTranslate",false);
					return;
				} 
			}
			viewModel.set("ShowTranslate",true);
		},
		CreateNew: function (e) {	
			e.preventDefault();	
			if (viewModel.get("create")) { return; } 
			 if (viewModel.get("edit")) { $("#EditPolicy").find(".cancel").trigger("click"); } 
			var id=new Date().getTime();
			resultsPolicy=$.parseJSON($("textarea#policyData").html());
			var tpl = controller.renderTpl('createNewPolicy')({ "id": id,"itemid": resultsPolicy.id  });
			$("#policyFields tbody:last").append("<tr id='policy-"+id+"' >"+$(tpl).html()+"</tr>");
			viewModel.set("create",true);
			viewModel.set("edit",false);
			kendo.bind($("#policy"),  policy['viewmodel']);
		},
		cancel: function (e) {
			e.preventDefault();
			resultsPolicy=$.parseJSON($("textarea#policyData").html());
			var tpl=controller.renderTpl("LoadPolicy");
			var trid= $(e.currentTarget).data("trid");
			var time = $(e.currentTarget).data("time");
			resultsPolicy['trid']=trid;
			resultsPolicy['time']=time;
			var t=tpl({ "results": resultsPolicy });
			$("#EditPolicy").replaceWith("<tr id='policy-"+resultsPolicy.extrafield.policy[trid].fieldid+"' class='"+resultsPolicy.extrafield.policy[trid].extraEnable+"'>"+$(t).html()+"</tr>");
			kendo.bind($("#policy"),  policy['viewmodel']);
			viewModel.set("edit",false);
		},
		deleteSelected: function (e) {
			e.preventDefault();
			var id= $(e.currentTarget).data("itemid").split("-");
			var fieldid=$(e.currentTarget).data("id").split("-");
			
			$('#confirmDiv').confirmModal({
				  heading:'Confirm Delete',
				  body:'Are you sure you want to delete the Policy',
				  callback: function() {
					  router.applyRoute("hotelManager/policy/"+id[1]+"-"+fieldid[1]+"/deleted");
				   }
			   });
		},
		update: function (e) {
			e.preventDefault();
			var id= $(e.currentTarget).data("itemid").split("-");
			 router.applyRoute("hotelManager/policy/"+id[1]+"/update");
		},
		insert: function (e) {
			e.preventDefault();
			var id= $(e.currentTarget).data("itemid").split("-");
		    router.applyRoute("hotelManager/policy/"+id[1]+"/insert");
		},
		cancelInsert: function (e) {
			e.preventDefault();
			viewModel.set("create",false);
			$("#policyFields").find("tr:last").remove();
		},
		EditSelected: function (e) {	  
			   e.preventDefault();
			   if (viewModel.get("edit")) {
				   $("#EditPolicy").find(".cancel").trigger("click");
			   }
			   var trid= $(e.currentTarget).data("trid");
			   var id = $(e.currentTarget).data("id");
			   var time = $(e.currentTarget).data("time");
			   resultsPolicy=$.parseJSON($("textarea#policyData").html()); 
			   var tpl=controller.renderTpl("EditPolicy");
			   resultsPolicy['time'] = time;
			   resultsPolicy['trid'] = trid;
			   $("#"+id).replaceWith(tpl( { "results": resultsPolicy }));
			  
			   if (time) {
				   $("#select-"+resultsPolicy.id).kendoTimePicker({
					    format: "HH:mm tt",
					    value: resultsPolicy.extrafield.policy[trid].value
					});
			   }

			   viewModel.set("edit",true);
			   kendo.bind($("#policy"),  policy['viewmodel']);
		},
		saveForm: function (e) {
			resultsPolicy=$.parseJSON($("textarea#policyData").html()); 
			
			for (i in resultsPolicy.extrafield.policy) {
				if ( resultsPolicy.extrafield.policy[i].fieldid == e[0].id ) {
					resultsPolicy.extrafield.policy[i].enable= e[0].enable;
					resultsPolicy.extrafield.policy[i].extraEnable= e[0].extraEnable;
					resultsPolicy.extrafield.policy[i].value= e[0].value;
					resultsPolicy.extrafield.policy[i].var_name= e[0].var_name;
					if ( e[0].field !== undefined ) {
						resultsPolicy.extrafield.policy[i].field=  e[0].field;
					}
				}
			}
			$("textarea#policyData").html(mcms.objectToString(resultsPolicy));
			$("#EditPolicy").find(".cancel").trigger("click");
			viewModel.get("CheckShowTranslation")();
		},
		insertForm:function (e) {
			lastProperty=-1;
			resultsPolicy=$.parseJSON($("textarea#policyData").html()); 
			for (lastProperty in resultsPolicy.extrafield.policy);
			lastProperty=parseInt(lastProperty)+1;		
			if (lastProperty == 0 ) {
				resultsPolicy.extrafield = {"policy": new Array(e[0])};			
			} else {
				resultsPolicy.extrafield.policy[lastProperty]=e[0];
			}

			var tpl=controller.renderTpl("LoadPolicy");	
			resultsPolicy['trid']=lastProperty;
			resultsPolicy['time']=false;
			var t=tpl({ "results": resultsPolicy });
			$("#policy-"+resultsPolicy.extrafield.policy[lastProperty].policyid).replaceWith("<tr id='policy-"+e[0].fieldid+"' class='pending'>"+$(t).html()+"</tr>");
			viewModel.set("create",false);
			viewModel.set("edit",false);
			kendo.bind($("#policy"),  policy['viewmodel']);
			$("textarea#policyData").html(mcms.objectToString(resultsPolicy));
			
			viewModel.get("CheckShowTranslation")();
			
		},
		deleted: function (e) {
			var id = e[0].id;
			var del = e[0].del;
			var enable = e[0].enable;
			var extraEnable = e[0].extraEnable;
			
			resultsPolicy=$.parseJSON($("textarea#policyData").html()); 
			
			for (i in resultsPolicy.extrafield.policy) {
				if ( resultsPolicy.extrafield.policy[i].fieldid == id ) {				
					if (!del) {
						resultsPolicy.extrafield.policy[i].value="";
						resultsPolicy.extrafield.policy[i].enable= enable;
						resultsPolicy.extrafield.policy[i].extraEnable= extraEnable;
						$("textarea#policyData").html(mcms.objectToString(resultsPolicy));
						$("tr#policy-"+id).find(".edit").trigger("click");
						$("#EditPolicy").find(".cancel").trigger("click");
					} else {
						$("tr#policy-"+id).remove();
						delete resultsPolicy.extrafield.policy[i];
						$("textarea#policyData").html(mcms.objectToString(resultsPolicy));
					}
				}
			}
			viewModel.get("CheckShowTranslation")();
		},
		/*
		deactivated: function (e) {
			  e.preventDefault();
			  ar = viewModel.get("selection.extra");
			  viewModel.set("selected",ar.join());
			  var action = $(e.currentTarget).data("action");
			  router.applyRoute("admin/policy/"+action+"/"+$(e.currentTarget).data("id"));
			  router.applyRoute("hotelManager/hotels/deactivate-"+page);
		},
		activated: function (e) {
			  e.preventDefault();
			  ar = viewModel.get("selection.extra");
			  viewModel.set("selected",ar.join());
			  var action = $(e.currentTarget).data("action");
			  router.applyRoute("admin/policy/"+action+"/"+$(e.currentTarget).data("id"));
			  router.applyRoute("hotelManager/hotels/activate-"+page);
		},
		*/
		disableExtrasReturn: function (e) {
			for (i in e[0]) {
			
				$("#policy-"+e[0][i]).removeClass("");
				$("#policy-"+e[0][i]).addClass("pending");		
			}
	        
		},
		enableExtrasReturn: function (e) {
			
			for (i in e[0]) {
			$("#policy-"+e[0][i]).removeClass("pending");
			$("#policy-"+e[0][i]).addClass("");
			}
	        
		},
		activateExtrasReturn: function (e) {
			
			var item = e[0].split(",");
			for (i in item) {
				if (!$("#policy-"+item[i]).hasClass("pending")) {
					$("#policy-"+item[i]).attr("class","");
				}
				}
		},
		deactivateExtrasReturn: function (e) {
			
			var item = e[0].split(",");
			for (i in item) {
				if (!$("#policy-"+item[i]).hasClass("pending")) {
					$("#policy-"+item[i]).addClass("deactivate");
				}
			}
		},
		enableDisableSelected: function (e) {
			 e.preventDefault();
			  ar = viewModel.get("selection.extra");
			  viewModel.set("selected",ar.join());
			  var action = $(e.currentTarget).data("action");
			  router.applyRoute("admin/policy/"+action+"/"+$(e.currentTarget).data("id"));
		}
    });


	controller.registerCallbackToAction("policy","activateExtras","activateExtrasReturn");	
	controller.registerCallbackToAction("policy","deactivateExtras","deactivateExtrasReturn");	
	controller.registerCallbackToAction("policy","enableExtras","enableExtrasReturn");	
	controller.registerCallbackToAction("policy","disableExtras","disableExtrasReturn");	
	controller.registerCallbackToAction("policy","deleted","deleted");	
	controller.registerCallbackToAction("policy","update","saveForm");
	controller.registerCallbackToAction("policy","insert","insertForm");
	
	
	kendo.bind($("#policy"),  viewModel);
	policy['viewmodel']=viewModel;
	
	$(".SelectHotel").removeClass("active");
    $("#policyTab").addClass("active");
	//viewModel.get("init")();
    
    viewModel.get("CheckShowTranslation")();

    
})();

});