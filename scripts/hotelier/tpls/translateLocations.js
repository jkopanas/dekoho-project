var translatecountry={};
var translateregion={};
var translatecity={};
var translateLocations={};
head.ready(function() {
(function () {

	var defaultTranslate=$.parseJSON($("#translateMap").html());
	var defaultTranslateLang=$.parseJSON($("#translateMapLang").html());	
	
	var viewModel = kendo.observable({ 
		init: function (e) {
			
			defaultTranslateLang=$.parseJSON($("#translateMapLang").html());	
			defaultTranslate=$.parseJSON($("#translateMap").html());
			kendo.bind($("#translate"+currentTemplate), viewModel);

			$("#languageTabs").find("a:first").trigger("click");
		    this["translate"+currentTemplate]['viewmodel']=viewModel;
		    
		},
		goBack: function(e) {
			e.preventDefault();
			history.back();
		},
		GetLanguage: function (e) {
			e.preventDefault();

			var id=$(e.currentTarget).data("id");
			var fields=id.split("-");
			$("#defaultTranslationTextArea").html(defaultTranslate[fields[0]][fields[1]+"-"+fields[2]]);
			
		},
		ChangeLang: function (e) {
			
			e.preventDefault();
			
			viewModel.set("EditLang","Editing "+$(e.currentTarget).html()+" Language");
			var code=$(e.currentTarget).data("code");
			for (i in defaultTranslateLang[code]) {
				$("#"+i).val(defaultTranslateLang[code][i]);
			}
		},
		saveForm: function (e) {
				$('html, body').animate({scrollTop:0}, 'slow');
				$('.alert-success').show().delay(3000).fadeOut('slow');	
			//controller.executeRoute("dashboard");
		},
		save: function(e) {
			e.preventDefault();
		
				router.applyRoute("translations/translate"+currentTemplate+"/"+$(e.currentTarget).data("id")+"/update");
				$('html, body').animate({scrollTop:0}, 'slow');
				$('.alert-success').show().delay(3000).fadeOut('slow');	

		}

    });
	
	controller.registerCallbackToAction("translate"+currentTemplate,"update","saveForm");
	kendo.bind($("#translate"+currentTemplate),  viewModel);
	
	$("#languageTabs").find("a:first").trigger("click");
	
	
	
	translateLocations['viewmodel'] = viewModel;
	translatecountry['viewmodel'] = viewModel;
	translateregion['viewmodel'] = viewModel;
	translatecity['viewmodel'] = viewModel;
	
	
})();

});