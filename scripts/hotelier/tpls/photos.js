var photos={};
head.ready(function() {
(function () {
	var multi="";
	var arr = [];
	var viewModel = kendo.observable({
    init: function(e) {
    	
        kendo.bind($("#photos"), viewModel);
        photos['viewmodel']=viewModel;
   	 	$(".SelectHotel").removeClass("active");
   	 	if ($("#module").val() == "hotel") {
    	 	$("#infoTab").addClass("active");    	
     	} else {
    	 	$("#roomsTab").addClass("active");    	
     	} 	 	
   	 	arr=[];
   	 	viewModel.set("selection",{ photo: [] });
   	 	$('input[type=checkbox]').attr('checked',false);
    },
	displaypopup: function(e) {
		e.preventDefault();
		router.applyRoute($(e.currentTarget).data('route'));
	},
	listener: function(e) {
		var id=$(e.currentTarget).data("id");
       if ( e.type == "mouseleave" ) {
    	   if  ($.inArray(id, arr) === -1) {
    		   $("#caption-"+id).hide();   
    	   }  
       } else {
    	   $("#caption-"+id).show();
       }
    },
    SelectClicked: function (e) {

    	 var checked = $(e.currentTarget).is(":checked");
    	  if (checked) {	
    		  arr.push($(e.currentTarget).data("id"));
    	  } else {
    		  var index = arr.indexOf($(e.currentTarget).data("id"));
    		  arr.splice(index, 1);
    	  }
    	  
    },
	activeFn: function(e) {

		var item=e[0].split(",");
		for (i in item) {
			$("#photo-"+item[i]).removeClass("deactivate");
			$("#photo-"+item[i]).addClass("activate");
        	kendo.bind($("#photos"), viewModel);
		}
	},
	inactiveFn: function(e) {
		var item=e[0].split(",");
		for (i in item) {
		$("#photo-"+item[i]).removeClass("activate");
		$("#photo-"+item[i]).addClass("deactivate");
		}
        kendo.bind($("#photos"), viewModel);
	},
	deleteFn: function(e) {
		var item=e[0].split(",");
		for (i in item) {
			$("#photo-"+item[i]).parents("li:first").remove();
		}
	},
	countSelected: function (e) {
		return viewModel.get("selection.photo").length;
	},
	selection: { "photo": [] },
	deleteSelected: function (e) {
		  e.preventDefault();
		  if ($('input[type=checkbox]:checked').length == 0 ) { return; }
		  ar = viewModel.get("selection.photo");
		  viewModel.set("selected",ar.join());
		  
		  $('#confirmDiv').confirmModal({
			  heading:'Confirm Delete',
			  body:'Are you sure you want to delete the Photo.',
			  callback: function() {
				  router.applyRoute("hotelManager/photos/"+controller.getArguments[0]+"/delete");		
			   }
		   });
		  
	},
	enableDisableSelected: function (e) {
		 e.preventDefault();
		  ar = viewModel.get("selection.photo");
		  viewModel.set("selected",ar.join());
		  var action = $(e.currentTarget).data("action");
		  router.applyRoute("admin/photos/"+action);
	},
	deactivated: function (e) {
		  e.preventDefault();
		  ar = viewModel.get("selection.photo");
		  viewModel.set("selected",ar.join());
		  router.applyRoute("hotelManager/photos/"+controller.getArguments[0]+"/inactive");
	},
	activated: function (e) {
		  e.preventDefault();
		  ar = viewModel.get("selection.photo");
		  viewModel.set("selected",ar.join());
		  router.applyRoute("hotelManager/photos/"+controller.getArguments[0]+"/active");
	},
	disablePhotosReturn: function (e) {

		var item=e[0].split(",");
		for (i in item) {
			$("#photo-"+item[i]).removeClass("activate");
			$("#photo-"+item[i]).removeClass("deactivate");
			$("#photo-"+item[i]).addClass("pending");		
		}
        kendo.bind($("#photos"), viewModel);
        
	},
	enablePhotosReturn: function (e) {

		var item=e[0].split(",");
		for (i in item) {
		$("#photo-"+item[i]).removeClass("pending");
		$("#photo-"+item[i]).addClass("deactivate");
		}
        kendo.bind($("#photos"), viewModel);
        
	},
});

	controller.registerCallbackToAction("photos","enablePhotos","enablePhotosReturn");	
	controller.registerCallbackToAction("photos","disablePhotos","disablePhotosReturn");	
	controller.registerCallbackToAction("photos","active","activeFn");
	controller.registerCallbackToAction("photos","inactive","inactiveFn");
	controller.registerCallbackToAction("photos","delete","deleteFn");	
	
	controller.registerCallbackToAction("photos","save","saveForm");
	controller.registerCallbackToAction("photos","new","RedirectHotels");
	
    kendo.bind($("#photos"), viewModel);
    photos['viewmodel']=viewModel;
    
	 $(".SelectHotel").removeClass("active");
    if ($("#module").val() == "hotel") {
        $("#infoTab").addClass("active");    	
    } else {
         $("#roomsTab").addClass("active");    	
    }


})();

});

function image_uploaded (e) {
	
	var newImages = controller.templates['newImages'];
	var p = $('#thumbs');
	var count = p.children().length;
	count++;
	var getclass =  (count % 3 != 0  ) ? "spacer-photo" : "";
	e.response.uploadedFile['class']=getclass;
	var x = newImages({"results": e.response.uploadedFile});
	p.append(x);
    kendo.bind($("#photos"), photos['viewmodel']);
}