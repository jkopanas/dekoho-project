var hotspots = {};
var x ={};
var resultsMap ={};
var SelectPoint={};
head.ready(function() {
	(function() {
		var markerListeners = [
		                           //{ listener : 'dragend',callback : 'hanleMarkerChange' }
		                           {
		                               listener : 'dragend',
		                               callback : 'reverseGeoCode',
		                               ListenerReverseGeoCoding:true
		                           },
		                           {
		                               listener : 'click',
		                               callback : 'markerClick'
		                           }];
		
		var viewModel = kendo.observable({
			init: function (e) {
			    $(".SelectHotel").removeClass("active");
			    $("#hotspotTab").addClass("active");
			    resultsMap=$.parseJSON($("textarea#mapsData").html());
			    kendo.bind($("#hotspots"), viewModel);
		    
				hotspots['viewmodel'].get("loadMap")(resultsMap);
				viewModel.set("ShowForm",true);
				viewModel.set("EditHotspots",true);
				viewModel.set("CreateHotspots",true);			    
			},
			LabelForm: "",
			hotspotTitle: "",
			hotspotDesc: "",
			EditHotspots: true,
			CreateHotspots: true,
			ShowForm: true,
			displaypopup: function(e) {
				e.preventDefault();
				router.applyRoute($(e.currentTarget).data('route'));
			},
			newposition: function(e) {
				x.NewPosition(e.lat,e.lng);
			},
			SetPointByName: function(e) {
				$("#GetLocation").find("#AddressPoint").val($(e.currentTarget).data('name'))
				viewModel.get("findPointByName")(e);
			},
			findPointByName: function (e) {
				e.preventDefault();
			    x.geoCode({
			    	markers : {
			    		markerListeners:markerListeners, 
			    	 	icon: "/images/site/green.png",
			    	 	draggable: true
			    	},
			        geocoder :{
			            address: $("#GetLocation").find("#AddressPoint").val(),
			            language:'el',
			            region:'GR'
			        }
			    }
			    ,'returnGeocoderObject');
			},
			deleteHotspot: function(e) {
				hotspots['viewmodel'].get("loadMap")(e[0].hotspots);				
			},
			saveForm: function(e) {
				hotspots['viewmodel'].get("loadMap")(e[0].hotspots);
				hotspots['viewmodel'].get("newposition")({ lat: e[0].hotspots[e[0].hotspots.length-1].lat,lng: e[0].hotspots[e[0].hotspots.length-1].lng });
				x.getMap().setZoom(parseInt(e[0].hotspots[e[0].hotspots.length-1].zoomLevel));
				viewModel.set("ShowForm",true);
				viewModel.set("CreateHotspots",true);
			},
			disableHotspotsReturn: function (e) {
		    	$("#enableBtn").removeClass("hide");
		    	$("#disableBtn").addClass("hide");
			},
			enableHotspotsReturn: function (e) {
				$("#disableBtn").removeClass("hide");
		    	$("#enableBtn").addClass("hide");
			},
			enableDisableSelected: function (e) {
				 e.preventDefault();
				  var action = $(e.currentTarget).data("action");
				  router.applyRoute("admin/hotspots/"+action+"/"+$(e.currentTarget).data("id"));
			},
			RejectField: function (e) {
				  e.preventDefault();
				  var action = $(e.currentTarget).data("action");
				  var id = $(e.currentTarget).data("id");
				  router.applyRoute("admin/hotspots/"+action+"/"+id);
			},
			AcceptField: function(e) {
				  e.preventDefault();
				  var action = $(e.currentTarget).data("action");
				  var id = $(e.currentTarget).data("id");
				  router.applyRoute("admin/hotspots/"+action+"/"+id);				  
			},
			AcceptFieldReturn: function (e) {
			
				var field=Object.keys(e[0]);
				var t= controller.renderTpl("MessageTpl");
				$("#Admin-"+field).remove();
				$("#msg").html(t({"status": "alert-success", "message": "You have accept field: "+field })).show().delay(3000).fadeOut('slow');
				resultsMap=$.parseJSON($("textarea#mapsData").html());
				
				var id=$('#hotspots').find("input[name=itemid]").val();

				for (i in resultsMap) { // Update textarea of hotspots with new description - title
					if (resultsMap[i].hotspotid == id) {
						resultsMap[i][field] = e[0][field];
						var index = resultsMap[i]['ReqFields'].indexOf(field);	
						resultsMap[i]['ReqFields'].splice(index, 1);
					}
				}
				
				$("textarea#mapsData").html(mcms.objectToString(resultsMap));
				hotspots['viewmodel'].get("loadMap")(resultsMap);
			},
			RejectFieldReturn: function (e) {
				
				var field=Object.keys(e[0]);
				var t= controller.renderTpl("MessageTpl");
				$("#Admin-"+field).remove();
				$("#msg").html(t({"status": "alert-success", "message": "You have rejected field: "+ field })).show().delay(3000).fadeOut('slow');

					$(":input[name="+field+"]").val(e[0][field]);

			},
			TranslateHotspot: function (e) {
				e.preventDefault();
				if ($('#hotspots').find("input[name=itemid]").val()) {
					window.location.hash="#translations/translatehotspots/"+$('#hotspots').find("input[name=itemid]").val();
				}
			},
			DeletePoint: function (e) {
				e.preventDefault();		
				
				$('#confirmDiv').confirmModal({
					  heading:'Confirm Delete',
					  body:'Are you sure you want to delete this Point.',
					  callback: function() {
							router.applyRoute("hotelManager/hotspots/"+$("#itemid").val()+"/delete");
					   }
				   });
				viewModel.set("ShowForm",true);
				viewModel.set("EditHotspots",true);
				viewModel.set("CreateHotspots",true);
			},
			CreatePoint: function (e) {
				e.preventDefault();
				$('#hotspots').find("input[name=zoomLevel]").val(x.getMap().getZoom());
		
			router.applyRoute("hotelManager/hotspots/"+resultsMap[0].hotel_id+"/save");
			},
			SavePoint: function (e) {
				e.preventDefault();		
					router.applyRoute("hotelManager/hotspots/"+$("#itemid").val()+"/save");
					$('html, body').animate({scrollTop:0}, 'slow');
					$('#success-spot').show().delay(3000).fadeOut('slow');
			},
			Press: function (e) {
				if ( e.charCode === 13 ) {
					
					viewModel.get("findPointByName")(e);
				}
			},
			loadMap : function(e) {
				var n=e.length-1;
				var ec= {};
				if  ( e[0].lat === undefined ) {
					ec.lat = ( $('#hotspots').find("input[name=lat]").val() == "" ) ? 0 : $('#hotspots').find("input[name=lat]").val(); 
					ec.lng = ( $('#hotspots').find("input[name=lng]").val() == "" ) ? 0 : $('#hotspots').find("input[name=lng]").val(); 
					ec.zoomLevel=( $('#hotspots').find("input[name=zoomLevel]").val() == "" ) ? 1 : $('#hotspots').find("input[name=zoomLevel]").val();
					n=0;
					
				} else {
					ec= e[n];
				}
				$('.map').googleMaps({
					zoom : parseInt(ec.zoomLevel),
					clat : ec.lat,
					clng : ec.lng
				}).data("mapHotspots").init();
				x = $('.map').data("mapHotspots");
			
				if  ( e[0].lat !== 0 ) {
					$.each(e,function(index,value){
					
						x.createMarker({
							markerListeners:markerListeners, 
							category: 'moreItems',
							map: x.obj,
							extra: { "ReqFields": value.ReqFields ,"hotspotid": value.hotspotid,"id": value.id,"title": value.title,"description": value.description,"geocoderAddress": value.geocoderAddress,"enable": value.enable },
							icon: "/images/site/green.png",
							draggable: true,
							position: new google.maps.LatLng(value.lat, value.lng)
						});
					 
					});
				}
			},
			startMap: function (e) {

				if(typeof google != 'undefined')
		        {		
				resultsMap=$.parseJSON($("textarea#mapsData").html());
				hotspots['viewmodel'].get("loadMap")(resultsMap);
		        }
			},
			addClassPosteddata: function (e) {
				e.preventDefault();
				$(e.currentTarget).addClass("postedData");
			}
			
		});
		
		kendo.bind($("#hotspots"), viewModel);
		hotspots['viewmodel'] = viewModel;
		
		
		controller.registerCallbackToAction("hotspots","RejectField","RejectFieldReturn");	
		controller.registerCallbackToAction("hotspots","AcceptField","AcceptFieldReturn");
		controller.registerCallbackToAction("hotspots","enableHotspots","enableHotspotsReturn");	
		controller.registerCallbackToAction("hotspots","disableHotspots","disableHotspotsReturn");	
		controller.registerCallbackToAction("hotspots", "delete", "deleteHotspot");
		controller.registerCallbackToAction("hotspots", "save", "saveForm");
		controller.registerCallbackToAction("hotspots", "ajaxAutocomplete","autocompleteReturn");

	    $(".SelectHotel").removeClass("active");
	    $("#hotspotTab").addClass("active");
	    hotspots['viewmodel'].get("startMap")();

	})();

});


function reverseGeoCode(e) {

	 var latLng = e.getPosition();

	$('#hotspots').find("input[name=geocoderAddress]").val(e.results.formatted_address);
	$('#hotspots').find("input[name=lat]").val(latLng.lat());
	
	hotspots['viewmodel'].set("LabelForm","Update Hotspot "+e.results.formatted_address);
    $('#hotspots').find("input[name=lng]").val(latLng.lng());
    $('#hotspots').find("input[name=zoomLevel]").val(x.getMap().getZoom());
  
    
    if (e.extra !== undefined ) {
    	$('#hotspots').find("input[name=itemid]").val(e.extra.hotspotid);
    	router.applyRoute("hotelManager/hotspots/"+$("#itemid").val()+"/update");
    }
}


function markerClick(e) {
	
	if (!e.extra) { return; }
	
	var latLng = e.getPosition();
	var map = e.getMap();
	hotspots['viewmodel'].set("ShowForm",false);
	hotspots['viewmodel'].set("EditHotspots",false);
	hotspots['viewmodel'].set("CreateHotspots",true);

	
	hotspots['viewmodel'].set("LabelForm",e.extra.geocoderAddress);
	$('#hotspots').find("input[name=geocoderAddress]").val(e.extra.geocoderAddress);
    $('#hotspots').find("input[name=lat]").val(latLng.lat());
    $('#hotspots').find("input[name=lng]").val(latLng.lng());
    $('#hotspots').find("input[name=zoomLevel]").val(e.getMap().getZoom());
    $('#hotspots').find("input[name=itemid]").val(e.extra.hotspotid);
    $('#hotspots').find("input[name=hotel_id]").val(resultsMap[0].hotel_id);
    
    $("#hotspot-title").attr("data-tid",e.extra.hotspotid);
    $("#hotspot-description").attr("data-tid",e.extra.hotspotid);
    
    hotspots['viewmodel'].set("hotspotTitle",e.extra.title);
    hotspots['viewmodel'].set("hotspotDesc",e.extra.description);
    var t = controller.renderTpl('EnableDisableFields');
    var f=['title','description'];

    for (i in f) {
    	$("#tpl-"+f[i]).html(t({ "results": { "ReqFields": e.extra.ReqFields },"id": e.extra.hotspotid,"table": "hotspots", "field": f[i] }));
    }
    
    if (parseInt(e.extra.enable)) {
    	$("#disableBtn").removeClass("hide");
    	$("#enableBtn").addClass("hide"); 	
    } else {
    	$("#enableBtn").removeClass("hide");
    	$("#disableBtn").addClass("hide");
    }
    $("#disableBtn").data("id",e.extra.hotspotid);
	$("#enableBtn").data("id",e.extra.hotspotid);
	
	 kendo.bind($("#hotspots"), hotspots['viewmodel']);
    
}


function returnGeocoderObject(results) {

	 if (results.length > 0)
	 {
		 var latLng = results[0].marker.getPosition();
		    hotspots['viewmodel'].set("ShowForm",false);
		 	hotspots['viewmodel'].set("EditHotspots",true);
		 	hotspots['viewmodel'].set("CreateHotspots",false);
		 	hotspots['viewmodel'].set("hotspotTitle","");
		    hotspots['viewmodel'].set("hotspotDesc","");
		    $('#hotspots').find("input[name=hotel_id]").val(resultsMap[0].hotel_id);
		 	hotspots['viewmodel'].set("LabelForm","Create new Hotspot "+results[0].formatted_address);
         
		 $('#hotspots').find("input[name=geocoderAddress]").val(results[0].formatted_address);
		 $('#hotspots').find("input[name=itemid]").val("");
		 $('#hotspots').find("input[name=lat]").val(latLng.lat());
		 $('#hotspots').find("input[name=lng]").val(latLng.lng());
		 
		    
		 $('#geocodingResults').html("");
         $.each(results, function(key, val) {
             $('#geocodingResults').append('<li><a href="#" class="results" data-name="'+val.formatted_address+'" data-bind="click: SetPointByName" rel="marker-' + key + '">' + val.formatted_address + '</a></li>');
         });
     }
	 kendo.bind($("#hotspots"), hotspots['viewmodel']);
}

function loadMaps() {
	hotspots['viewmodel'].get("startMap")();
}
