var account ={};
head.ready(function() {
	
(function () {
	
	var viewModel = kendo.observable({
		init: function(){
		    $(".mainMenu").removeClass("active");
		    $("#profileMenu").addClass("active");
			kendo.bind($("#account"), viewModel);
		},
		save: function(e) {
			e.preventDefault();
			if (controller.validator['account'].validate()) {
				router.applyRoute("profileManager/account/save");
				$('html, body').animate({scrollTop:0}, 'slow');
				 $('#success').show().delay(3000).fadeOut('slow');
			}
		},
		displaypopup: function(e) {
			router.applyRoute($(e.currentTarget).data('route'));
		}
    });
	
    $(".mainMenu").removeClass("active");
    $("#profileMenu").addClass("active");
    
			var ret_mail = true;
			var ret_mail2 = true;

			controller.validator['account'].options.rules['verifyMails'] = function(input){
	              if (input.is("[name=confirm_email]")) {
	                   ret_mail = input.val() === $("#email").val();          
	                }
	                return ret_mail;
	        };  
	        controller.validator['account'].options.rules['verifyMails2']= function(input){	
	              if (input.is("[name=confirm_email2]")) {
	                   ret_mail2 = input.val() === $("#email2").val();          
	                }
	                 return ret_mail2;
	        }



	controller.registerCallbackToAction("account","save","saveForm");
	controller.registerCallbackToAction("account","edit","saverForm");
	
  kendo.bind($("#account"), viewModel);
  account['viewmodel']=viewModel;
  
})();

});