var rooms={};
var selectall=false;
head.ready(function() {
(function () {
	var viewModel = kendo.observable({     
		init: function () {
			kendo.bind($("#rooms"), viewModel);
			viewModel.set("selection", { room: [] });
			viewModel.set("allselect",true);
			selectall=false;
			$("#dashboard").find(':checkbox').attr("checked",false);
			$(".SelectHotel").removeClass("active");
			$("#roomsTab").addClass("active");
		},
		allselect: false,
		selectAll: function (e) {
		if (!selectall) { selectall=true; } else { selectall =false; }

			$("#dashboard").find(':checkbox:not(:first)').each( function(index,value) {
			
				if ( !$(value).is(":checked") && selectall == true ) {
					$(value).trigger('click');
				}	else if ($(value).is(":checked") && selectall == false) {
					$(value).trigger('click');
				}
			});		
			
		},
		selectedfn: function (e) {	
			if (e === undefined ) {return;}		
			$(e.currentTarget).parents("tr").find("td,th").toggleClass("highlight");
		},
		selection: { "room": [] },
		deleteSelected: function (e) {
			  e.preventDefault();
			  if ($('input[type=checkbox]:checked').length == 0 ) { return }
			  var id = $(e.currentTarget).data("id");
			  ar = rooms['viewmodel'].get("selection.room");
			  rooms['viewmodel'].set("selected",ar.join());
			  $('#confirmDiv').confirmModal({
				  heading:'Confirm Delete',
				  body:'Are you sure you want to delete the selected Rooms.',
				  callback: function() {
					  router.applyRoute("hotelManager/rooms/hotel/"+id+"/deleted");
				   }
			   });
		},
		deactivated: function (e) {
			  e.preventDefault();
			  var id = $(e.currentTarget).data("id");
			  ar = rooms['viewmodel'].get("selection.room");
			  rooms['viewmodel'].set("selected",ar.join());
			  router.applyRoute("hotelManager/rooms/hotel/"+id+"/deactivate");
		},
		activated: function (e) {
			  e.preventDefault();		
			  var id = $(e.currentTarget).data("id");
			  ar = rooms['viewmodel'].get("selection.room");
			  rooms['viewmodel'].set("selected",ar.join());
			  router.applyRoute("hotelManager/rooms/hotel/"+id+"/activate");
		},
		displaypopup: function(e) {
			e.preventDefault();
			router.applyRoute($(e.currentTarget).data('route'));
		},
		enableDisableSelected: function (e) {
			 e.preventDefault();
			  ar = viewModel.get("selection.room");
			  viewModel.set("selected",ar.join());
			  var action = $(e.currentTarget).data("action");
			  router.applyRoute("admin/rooms/"+action);
		},
		disableRoomsReturn: function (e) {
			
			var t=controller.renderTpl("StatusRender");
			for (i in e[0]) {
				$("#status-"+e[0][i]).html(t({"status": "0"}));
			}
			
		},
		enableRoomsReturn: function (e) {
			
			var t=controller.renderTpl("StatusRender");
			for (i in e[0]) {
				$("#status-"+e[0][i]).html(t({"status": "1"}));
			}
			
		}
    });

	kendo.bind($("#rooms"),viewModel);
	rooms['viewmodel']=viewModel;
	$(".SelectHotel").removeClass("active");
	$("#roomsTab").addClass("active");

	controller.registerCallbackToAction("rooms","save","saveForm");
	
	
	controller.registerCallbackToAction("rooms","enableRooms","enableRoomsReturn");	
	controller.registerCallbackToAction("rooms","disableRooms","disableRoomsReturn");	
	


})();

});