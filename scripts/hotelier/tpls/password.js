var change_password ={};

head.ready(function() {
	

	
	
(function () {
	
	var viewModel = kendo.observable({     

		saveForm: function(e) {
			 $("#success-pass").show().delay(3000).fadeOut('slow');
		},
		
		updatePassword: function(e) {
			e.preventDefault();
			if (ret_pass==true && $("#confirmnewpass").val()!="" && $("#pass").val()!="" ) {
			router.applyRoute("profileManager/change_password/save");
			} else { $("#wrong_pass_no").show().delay(4000).fadeOut('slow'); }
		},
		showmsg: function(e) {
			if(e[0].result=='false') 
			{
			  $("#wrong_pass").show().delay(4000).fadeOut('slow');
			  $("#pass_update").hide();
			} else 
			{
				$("#wrong_pass").hide();
				$("#pass_update").show();
			}
		}
    });
	
	   var ret_pass = true;
		var user_pass= true;

	        controller.validator['change_password'].options.rules['verifypassword']= function(input){	

	            if (input.is("[name=pass]")) {
	            	var pass = $("#pass").val();
	            	router.applyRoute("profileManager/change_password/passwordcheck");
	    	 	   
	            }
	            return user_pass;
			}
	        
	        controller.validator['change_password'].options.rules['verify2passwords']= function(input){	

	        	if (input.is("[name=confirmnewpass]")) {
	        		
	        		ret_pass = input.val() === $("#newpass").val();
	                }
	        	
	                 return ret_pass;
	              
			}

	controller.registerCallbackToAction("change_password","save","saveForm");
	controller.registerCallbackToAction("change_password","passwordcheck","showmsg");
	
  kendo.bind($("#change_password"), viewModel);
  change_password['viewmodel']=viewModel;
  
})();

});