var support={};
var supportDetails={};
var supportNew={};
head.ready(function() {
(function () {
	var selectall=false;
	var viewModel = kendo.observable({
		init: function (e) {
		    $(".mainMenu").removeClass("active");
		    $("#supportMenu").addClass("active");
		    kendo.bind($("#support"), viewModel);
		    kendo.bind($("#supportDetails"), viewModel);
		    kendo.bind($("#supportNew"), viewModel);
		
		    $("#supportMessage").delay(3000).fadeOut('slow','swing',function() { viewModel.set("message",""); });
		    
		    support['viewmodel'] = viewModel;
		    supportDetails['viewmodel'] = viewModel;
		    supportNew['viewmodel'] = viewModel;
		    
		    if ($("#content").length > 0 ) { viewModel.get('loadEditor')(); }
		    
		},
		allselect: false,
		selectAll: function (e) {
		if (!selectall) { selectall=true; } else { selectall =false; }

			$("#support").find(':checkbox:not(:first)').each( function(index,value) {
			
				if ( !$(value).is(":checked") && selectall == true ) {
					$(value).trigger('click');
				}	else if ($(value).is(":checked") && selectall == false) {
					$(value).trigger('click');
				}
			});		
			
		},
		selectedfn: function (e) {	
			if (e === undefined ) {return;}
			ar = viewModel.get("selection.ticket");		
			$(e.currentTarget).parents("tr").find("td,th").toggleClass("highlight");
		},
		selected: "",
		selection: { "ticket": [] },
		deleteSelected: function (e) {
			  e.preventDefault();
			  ar = viewModel.get("selection.ticket");
			  viewModel.set("selected",ar.join());
			 
			  $('#confirmDiv').confirmModal({
				  heading:'Confirm Delete',
				  body:'Are you sure you want to delete the selected tickets.',
				  callback: function() {
					  router.applyRoute("support/0/deleted");		
				   }
			   });
		},
		CloseTicketSelected: function (e) {
			  e.preventDefault();
			  ar = viewModel.get("selection.ticket");
			  viewModel.set("selected",ar.join());
			 
			  $('#confirmDiv').confirmModal({
				  heading:'Confirm Close Ticket',
				  body:'Are you sure you want to close the selected tickets.',
				  callback: function() {
					  router.applyRoute("admin/support/closed/");
				   }
			   });
		},
		OpenTicketSelected: function (e) {
			  e.preventDefault();
			  ar = viewModel.get("selection.ticket");
			  viewModel.set("selected",ar.join());
			 
			  $('#confirmDiv').confirmModal({
				  heading:'Confirm Re-opened Ticket',
				  body:'Are you sure you want to open the selected tickets.',
				  callback: function() {
					  router.applyRoute("admin/support/opened/");
				   }
			   });
		},
		closeTicket: function (e) {
			for (i in e[0] ) {
				$("#ticket-"+e[0][i]).find("td:last span").removeClass("label-success");
				$("#ticket-"+e[0][i]).find("td:last span").addClass("label-important");
				$("#ticket-"+e[0][i]).find("td:last span").html("Closed");
			}		
		},
		openTicket: function (e) {
			for (i in e[0] ) {
				$("#ticket-"+e[0][i]).find("td:last span").addClass("label-success");
				$("#ticket-"+e[0][i]).find("td:last span").removeClass("label-important");
				$("#ticket-"+e[0][i]).find("td:last span").html("Open");
			}		
		},
		deleteTicket: function (e) {
			for (i in e[0] ) {
				$("#ticket-"+e[0][i]).remove();
			}		
		},
		isInvisible: function () {
			return (viewModel.get('message') == "" ) ? true : false;
		},
		message: "",
		focus: function (e) {
			if ($(e.currentTarget).val() == "Click to Add Title") {
				$(e.currentTarget).val("");
				$(e.currentTarget).css("opacity",1);
			}
		},
		viewPage: function (e) {
			$("#support").html(controller.templates['support'](e[0]));
			 kendo.bind($("#support"), viewModel);
		},
		viewDetails: function(e) {
			$("#support").html(controller.templates['supportDetails'](e[0]));
			 kendo.bind($("#support"), viewModel);
		},
		showDescription: function (e) {
			var id=$(e.currentTarget).attr("id");
			var i=$(e.currentTarget).data("i");
			t=controller.templates[id];
			$(e.currentTarget).replaceWith(t({ 'results': controller.dataTpl.supportDetails.results,'users': controller.dataTpl.supportDetails.users,'i': i }));
			 kendo.bind($("#supportDetails"), viewModel);
		},
		OverOut: function (e) {	
				$(e.currentTarget).toggleClass("deactivatemail");
		},
		Reply: function (e) {
			e.preventDefault();
			window.location.hash="supportNew/"+$(e.currentTarget).data("id")+"/view";
		},
		loadEditor: function (e) {
			if (CKEDITOR.instances['content']) { 
			    delete CKEDITOR.instances['content'] 
			};
			var editorOptions = {	toolbar : [
			                     	            [ 'Bold', 'Italic', '-','Link', '-', 'Undo','Redo','-','PasteText','PasteFromWord','-','SelectAll','RemoveFormat']
			                     	        ],
			                     	       uiColor: "#ffffff",
			                     	language : 'el', ProcessHTMLEntities:false, height:300};
			 var editor = CKEDITOR.replace( 'content',editorOptions );
		},
		ReturnNew: function () {
				viewModel.set("message","Your message has been send successfully");
				window.location.hash="support";
				viewModel.set("reply",false);
		},
		Send: function (e) {
			e.preventDefault();
			for(var instanceName in CKEDITOR.instances)
			    CKEDITOR.instances[instanceName].updateElement();

			router.applyRoute("supportNew/new/save");
		},
		Cancel: function(e) {
			  $('#confirmDiv').confirmModal({
				  heading:'Discard',
				  body:'Are you sure you want to Discard your message? ',
				  callback: function() {
					  window.location.hash="support";
				   }
			   });
		},
		ChangeSubject: function (e) {
			e.preventDefault();
			$("#title").val($(e.currentTarget).html());
		}

    });

	controller.registerCallbackToAction("support","opened","openTicket");
	controller.registerCallbackToAction("support","closed","closeTicket");
	controller.registerCallbackToAction("support","deleted","deleteTicket");
	controller.registerCallbackToAction("support","view","viewPage");
	controller.registerCallbackToAction("supportDetails","details","viewDetails");	
	controller.registerCallbackToAction("supportNew","save","ReturnNew");
	
	
    kendo.bind($("#support"), viewModel);
    kendo.bind($("#supportDetails"), viewModel);
    kendo.bind($("#supportNew"), viewModel);
    
    support['viewmodel'] = viewModel;
    supportDetails['viewmodel'] = viewModel;
    supportNew['viewmodel'] = viewModel;

	
    $(".mainMenu").removeClass("active");
    $("#supportMenu").addClass("active");
    
    if ($("#content").length > 0 ) { viewModel.get('loadEditor')(); }
    
})();

});