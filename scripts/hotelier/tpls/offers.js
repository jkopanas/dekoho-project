var offers={};
head.ready(function() {
(function () {
	var dataRoom = "";
	var viewModel = kendo.observable({
		hotelid: "0",
		init: function (e) {
		    $(".mainMenu").removeClass("active");
		    $("#offersMenu").addClass("active");
		    kendo.bind($("#offers"), viewModel);
		    offers['viewmodel'] = viewModel;
		},
		editable: function (e) {
			e.preventDefault();
			viewModel.set("hotelid",$(e.currentTarget).data("id"));
			location.hash="viewOffer";
		},
		deleteOffer: function (e) {
			e.preventDefault();
			
			$('#confirmDiv').confirmModal({
				  heading:'Confirm Delete',
				  body:'Are you sure you want to cancel the Offer.',
				  callback: function() {
					  router.applyRoute("offers/"+$(e.currentTarget).data("id")+"/deleted");
				   }
			   });
			
		},
		returndelete: function (e) {

			$("#row-"+e[0]).remove();

		},
		displaypopup: function(e) {
			e.preventDefault();
			router.applyRoute($(e.currentTarget).data('route'));
		}
    });

	
	controller.registerCallbackToAction("offers","deleted","returndelete");

    kendo.bind($("#offers"), viewModel);
    offers['viewmodel'] = viewModel;
    
    $(".mainMenu").removeClass("active");
    $("#offersMenu").addClass("active");

})();

});