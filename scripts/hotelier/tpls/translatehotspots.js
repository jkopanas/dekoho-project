var translatehotspots={};
head.ready(function() {
(function () {
	
	var defaultTranslate=$.parseJSON($("#translateMap").html());
	var defaultTranslateLang=$.parseJSON($("#translateMapLang").html());	
	
	var viewModel = kendo.observable({ 
		
		translateTab: true,
		init: function (e) {
			
			defaultTranslateLang=$.parseJSON($("#translateMapLang").html());	
			defaultTranslate=$.parseJSON($("#translateMap").html());
		    kendo.bind($("#translatehotspots"), viewModel);
		    translatehotspots['viewmodel']=viewModel;
			$("#languageTabs").find("a:first").trigger("click");
		    
		},
		goBack: function(e) {
			e.preventDefault();
			history.back();
		},
		GetLanguage: function (e) {
			
			e.preventDefault();
			var id=$(e.currentTarget).data("id");
			var fields=id.split("-");
			$("#defaultTranslationTextArea").html(defaultTranslate[fields[0]][fields[1]+"-"+fields[2]]);
			
		},
		ChangeLang: function (e) {
			
			e.preventDefault();
			
			viewModel.set("EditLang","Editing "+$(e.currentTarget).html()+" Language");
			var code=$(e.currentTarget).data("code");
			console.log(defaultTranslateLang,code);
			for (i in defaultTranslateLang[code]) {
				$("#"+i).val(defaultTranslateLang[code][i]);
			}
		},
		saveForm: function (e) {
		
				$('html, body').animate({scrollTop:0}, 'slow');
				$('.alert-success').show().delay(3000).fadeOut('slow');	
			//controller.executeRoute("dashboard");
		},
		save: function(e) {
			e.preventDefault();
			
		//if (controller.validator['translatehotspots'].validate()) {

				router.applyRoute("translations/translatehotspots/"+$(e.currentTarget).data("id")+"/update");
				
			//}	
		}

    });
	controller.registerCallbackToAction("translatehotspots","update","saveForm");

	kendo.bind($("#translatehotspots"),  viewModel);
	translatehotspots['viewmodel']=viewModel
	
	$("#languageTabs").find("a:first").trigger("click");
	
})();

});