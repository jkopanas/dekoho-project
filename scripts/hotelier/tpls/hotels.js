hotels={};
var selectall=false;
head.ready(function() {
(function () {
	var multi="";
	var viewModel = kendo.observable({

		init: function (e) {
			kendo.bind($("#hotels"),  viewModel);
		    $(".mainMenu").removeClass("active");
		    $("#hotelMenu").addClass("active");
			viewModel.set("selection", { hotel: [] });
			viewModel.set("allselect",true);
		    selectall=false;
			$("#dashboard").find("td,th").removeClass("highlight");
			$("#dashboard").find(':checkbox').attr("checked",false);
		},
		allselect: false,
		selectAll: function (e) {
		if (!selectall) { selectall=true; } else { selectall =false; }

			$("#dashboard").find(':checkbox:not(:first)').each( function(index,value) {
			
				if ( !$(value).is(":checked") && selectall == true ) {
					$(value).trigger('click');
				}	else if ($(value).is(":checked") && selectall == false) {
					$(value).trigger('click');
				}
			});		
			
		},

		selectedfn: function (e) {	
			if (e === undefined ) {return;}	
			$(e.currentTarget).parents("tr").find("td,th").toggleClass("highlight");
		},
		selection: { "hotel": [] },
		deleteSelected: function (e) {
			  e.preventDefault();
			  if ($('input[type=checkbox]:checked').length == 0 ) { return }
			  ar = viewModel.get("selection.hotel");
			  viewModel.set("selected",ar.join());
			  
			  $('#confirmDiv').confirmModal({
				  heading:'Confirm Delete',
				  body:'Are you sure you want to delete the selected hotels.',
				  callback: function() {
					  router.applyRoute("hotelManager/hotels/deleted");		
				   }
			   });
			  
		},
		renew: function (e) {
			  e.preventDefault();
		},
		enableDisableSelected: function (e) {
			 e.preventDefault();
			  ar = viewModel.get("selection.hotel");
			  viewModel.set("selected",ar.join());
			  var action = $(e.currentTarget).data("action");
			  router.applyRoute("admin/hotels/"+action);
		},
		deactivated: function (e) {
			  e.preventDefault();
			  ar = viewModel.get("selection.hotel");
			//  console.log(ar);
			  viewModel.set("selected",ar.join());
			  var p=location.hash.split("/");
			  var page = (p[2] === undefined ) ? "1" :  p[2];
			  router.applyRoute("hotelManager/hotels/deactivate-"+page);
		},
		activated: function (e) {
			  e.preventDefault();
			  ar = viewModel.get("selection.hotel");
			 // console.log(ar);
			  viewModel.set("selected",ar.join());
			  var p=location.hash.split("/");
			  var page = (p[2] === undefined ) ? "1" :  p[2];
			  router.applyRoute("hotelManager/hotels/activate-"+page);
		},
		disableHotelsReturn: function (e) {
	
			var t=controller.renderTpl("StatusRender");
			for (i in e[0]) {
				$("#status-"+e[0][i]).html(t({"status": "0"}));
			}
			
		},
		enableHotelsReturn: function (e) {
			
			var t=controller.renderTpl("StatusRender");
			for (i in e[0]) {
				$("#status-"+e[0][i]).html(t({"status": "1"}));
			}
			
		}
		
    });


	controller.registerCallbackToAction("hotels","save","saveForm");
	controller.registerCallbackToAction("hotels","new","RedirectHotels");
	
	controller.registerCallbackToAction("hotels","enableHotels","enableHotelsReturn");	
	controller.registerCallbackToAction("hotels","disableHotels","disableHotelsReturn");	
	
	
	kendo.bind($("#hotels"),  viewModel);
	hotels['viewmodel']=viewModel
	
	$(".mainMenu").removeClass("active");
    $("#hotelMenu").addClass("active");

    
})();

});