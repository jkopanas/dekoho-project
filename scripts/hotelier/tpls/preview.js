var preview ={};
var previewOrder = {};
head.ready(function() {
	
(function () {
	
	var viewModel = kendo.observable({     
		init: function (e) {
			  kendo.bind($("#preview"), viewModel);
		},
		displaypopup: function(e) {
			router.applyRoute($(e.currentTarget).data('route'));
		},
		publish: function (e) {
			e.preventDefault();	
	//	router.applyRoute("hotelManager/previewOrder/"+$(e.currentTarget).data("id"));
			
	
			var t = controller.renderTpl("wait-loader");
			var d = controller.renderTpl("redirect-dashboard");			
			$("#Popup").height("240px");
			$(window).scrollTop(0);
    		$("#Popup").html(t({}));
   	     	setTimeout(function(){
   	    		$("#Popup").html(d({})); 
   	     			setTimeout(function(){
   	     					location.hash="dashboard";
   	     					$("#Popup").modal('hide');		
   	     			}, 5000);
   	     	}, 10000);


		},


		ShowItemTitle: function (e) {
			return e + " ( Publish Hotel )";
		},
		/*
		SelectOption: function (e) {
			var valueFn = $(e.currentTarget).val();
			viewModel.set("SelectPayment",false);
			viewModel.set("BuyNow",false);
			viewModel.set("showStartDate",true);
			viewModel.set("credit_card",$("#"+$(e.currentTarget).attr("id")+" option:selected").text());
			viewModel.get(valueFn)();	
		},
		ShowPayment: function (e) {
			viewModel.set("SelectPayment",true);
			viewModel.set("BuyNow",true);
		},
		PayPal: function (e) {
			viewModel.set("SelectPayment",true);
		},
		visa: function (e) {
			viewModel.set("SelectPayment",false);
		},
		mastercard: function (e) {
			viewModel.set("showStartDate",false);
		},
		amex: function (e) {
		},

		finishPayment: function (e) {
			router.applyRoute("payment/preview/StartCheckout/hotel");
		},
		checkoutstart: function (data) {
			window.location=data[0];
		},
		BuyNow: true,
		SelectPayment: true,
		showStartDate: true
		*/
    });
	
	

	controller.registerCallbackToAction("preview","save","saveForm");
	controller.registerCallbackToAction("preview","passwordcheck","showmsg");
	controller.registerCallbackToAction("preview","hotel","checkoutstart");
	
  kendo.bind($("#preview"), viewModel);
  preview['viewmodel']=viewModel;
  previewOrder['viewmodel']=viewModel;
  
})();

});




/*
var preview ={};
var previewOrder = {};
head.ready(function() {
	
(function () {
	
	var viewModel = kendo.observable({     
		init: function (e) {
			  kendo.bind($("#preview"), viewModel);
		},
		displaypopup: function(e) {
			router.applyRoute($(e.currentTarget).data('route'));
		},
		publish: function (e) {
			e.preventDefault();	
			router.applyRoute("hotelManager/previewOrder/"+$(e.currentTarget).data("id"));
		
			var t = controller.renderTpl("wait-loader");
			var d = controller.renderTpl("redirect-dashboard");			
			$("#Popup").height("240px");
			$(window).scrollTop(0);
    		$("#Popup").html(t({}));
   	     	setTimeout(function(){
   	    		$("#Popup").html(d({})); 
   	     			setTimeout(function(){
   	     					location.hash="dashboard";
   	     					$("#Popup").modal('hide');		
   	     			}, 5000);
   	     	}, 10000);
			
		
		},
		ShowItemTitle: function (e) {
			return e + " ( Publish Hotel )";
		},
		SelectBilling: 0,
		BillingOption: function (e) {
			if ( parseInt(viewModel.get('SelectBilling')) != 0 && $("#payment option:selected").val() != "ShowPayment" ) {
				viewModel.set("BuyNow",false);
			} else {
				viewModel.set("BuyNow",true);
			}
		},
		SelectOption: function (e) {	
			var valueFn = $(e.currentTarget).val();
			viewModel.set("showStartDate",true);
			viewModel.set("credit_card",$("#"+$(e.currentTarget).attr("id")+" option:selected").text());
			viewModel.get(valueFn)();		
		},
		ShowPayment: function (e) {
			viewModel.set("BuyNow",true);
		},
		PayPal: function (e) {
			if (parseInt(viewModel.get('SelectBilling'))) {
				viewModel.set("BuyNow",false);
			} else {
				viewModel.set("BuyNow",true);
			}
		},
		jcc: function (e) {
			if (parseInt(viewModel.get('SelectBilling'))) {
				viewModel.set("BuyNow",false);
			} else {
				viewModel.set("BuyNow",true);
			}
		},
		finishPayment: function (e) {
			if ($("#payment option:selected").val() == "PayPal") {
				router.applyRoute("payment/preview/StartCheckout/hotel");
			} else if ($("#payment option:selected").val() == "jcc") {
				router.applyRoute("payment/addOffer/startgcc/hotel");
			}	
		},
		checkoutstart: function (data) {
			if ($("#payment option:selected").val() == "PayPal" ) {
				window.location=data[0];
			} else if ($("#payment option:selected").val() == "jcc") {
				$("#ShowPay").html(controller.renderTpl("jcc")(data[0].PaymentData));
				$("#paymentForm").submit();
			}		
		},
		BuyNow: true,
		SelectPayment: true,
		showStartDate: true
    });
	
	

	controller.registerCallbackToAction("preview","save","saveForm");
	controller.registerCallbackToAction("preview","passwordcheck","showmsg");
	controller.registerCallbackToAction("preview","hotel","checkoutstart");
	
  kendo.bind($("#preview"), viewModel);
  preview['viewmodel']=viewModel;
  previewOrder['viewmodel']=viewModel;
  
})();

});
*/