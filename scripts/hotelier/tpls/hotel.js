var hotel={};
head.ready(function() {
(function () {
	var multi="";
	var newhotel=false;
	var viewModel = kendo.observable({ 
		id: "",
		module: "",
		facilities: true,
		roomfacilities: true,
		services: true,
		hoteltypes: true,
		themeshotels: true,
		selection: { "filters": [] },
		init: function (e) {

		    kendo.bind($("#hotel"), viewModel);
		    hotel['viewmodel']=viewModel;
		    hotel['viewmodel'].get('multiselect')();
		    $('.admin').attr('checked',false);
		    kendo.bind($("#hotel"), viewModel);
		    if (newhotel)
		    {
		    	$('html, body').animate({scrollTop:0}, 'slow');
				$('#success-hotel').show().delay(3000).fadeOut('slow');
				newhotel=false;
		    }
		    
		    $(".SelectHotel").removeClass("active");
		    $("#infoTab").addClass("active");
		    
			$(".mainMenu").removeClass("active");
		    $("#hotelMenu").addClass("active");
		    
		},
		newHotel: function (e) {	
			return (controller.dataTpl['hotel'].id == "" ) ? true : false ;
		},
		//displaypopup: function(e) {
		//	router.applyRoute($(e.currentTarget).data('route'));
		//},
		saveForm: function (e) {
		
				$('html, body').animate({scrollTop:0}, 'slow');
				$('#success-hotel').show().delay(3000).fadeOut('slow');	
				var t = controller.renderTpl("selectProperties");
				
				$("select#facilities").html(t({ id: "facilities",table: "facilities", data: e[0].facilities}));
				$("select#themeshotels").html(t({id: "themeshotels",table: "themeshotels",data:e[0].themeshotels}));
				$("select#hoteltypes").html(t({id: "hoteltypes",table: "types",data:e[0].types}));
				$("select#services").html(t({id: "services",table: "services",data:e[0].services}));
				$("select#roomfacilities").html(t({id: "roomfacilities",table: "roomsfacilities", data: e[0].roomsfacilities}));
				
				multi.multiselect('rebuild');
				multi.multiselect('refresh');
				kendo.bind($("#hotel"), viewModel);

				
		},
		save: function(e) {
			e.preventDefault();
			
		if (controller.validator['hotel'].validate()) {
				var elem =$(e.currentTarget);
				router.applyRoute("hotelManager/hotel/"+elem.data('id')+"/save");
			}	
		},
		add: function(e) {
			e.preventDefault();
			if (controller.validator['hotel'].validate()) {
				newhotel = true;
				router.applyRoute("hotelManager/hotel/new");
			}	
		},
		RedirectHotels: function(e) {
			window.location.hash = "hotelManager/hotel/"+e[0];
			$('#success-hotel').show().delay(3000).fadeOut('slow');	
		},
		addoption: function(e) {
			e.preventDefault();
			var elem =$(e.currentTarget);
			if ($("."+elem.data('id')).val() == "" )  { return; }
			$('#'+elem.data('id')).append($('<option value="'+$("."+elem.data('id')).val()+'" selected>').text($("."+elem.data('id')).val())); 
			multi.multiselect('rebuild');
			multi.multiselect('refresh');
			kendo.bind($("#hotel"), viewModel);
			viewModel.get("visible")(e);
		},
		visible: function (e) {
			e.preventDefault();
			var id =$(e.currentTarget).data('id');
			if (viewModel.get(id)) {  viewModel.set(id,false); } else { viewModel.set(id,true); }
		},
		displaypopup: function(e) {
			e.preventDefault();
			router.applyRoute($(e.currentTarget).data('route'));
		},
		enableDisableSelected: function (e) {
			 e.preventDefault();
			 ar = viewModel.get("selection.filters");
			  viewModel.set("selected",ar.join());
			  var action = $(e.currentTarget).data("action");
			  var id = $(e.currentTarget).data("id");
			  
			 if (action == "deleteFilters") {
				 $('#confirmDiv').confirmModal({
					  heading:'Confirm Delete',
					  body:'Are you sure you want to delete the selected '+id,
					  callback: function() {
						  router.applyRoute("admin/hotel/"+action+"/"+id);
					   }
				   });
			 } else {
			  router.applyRoute("admin/hotel/"+action+"/"+id);
			 }
		},
		showAdmin: function (e) {
			$(".filterAdmin").addClass("hide");
			$("."+$(e.currentTarget).attr("id")).removeClass("hide");
		},
		disableHotelsReturn: function (e) {
			$("#disableBtn").hide();
			$("#enableBtn").show();
			
		},
		enableHotelsReturn: function (e) {

			$("#disableBtn").show();
			$("#enableBtn").hide();
			
		},
		disableFiltersReturn: function (e) {

			var item=e[0].id.split(",");
			for (i in item) {
				$("#li-"+e[0].table+"-"+item[i]).find("label").addClass("pending");		
			}
			$("#btn-"+e[0].table).trigger("click");
	
		},
		enableFiltersReturn: function (e) {

			var item=e[0].id.split(",");
			for (i in item) {
				
				$("#li-"+e[0].table+"-"+item[i]).find("label").removeClass("pending");		
			}
			$("#btn-"+e[0].table).trigger("click");
			
		},
		deleteFiltersReturn: function (e) {
			var item=e[0].id.split(",");
			for (i in item) {
				$("select[name="+e[0].table+"] option[value='"+item[i]+"']").remove();
			}
			multi.multiselect('rebuild');
			multi.multiselect('refresh');
			$("#btn-"+e[0].table).trigger("click");		
		},
		RejectField: function (e) {
			  e.preventDefault();
			  var action = $(e.currentTarget).data("action");
			  var id = $(e.currentTarget).data("id");
			  router.applyRoute("admin/hotel/"+action+"/"+id);
		},
		AcceptField: function(e) {
			  e.preventDefault();
			  var action = $(e.currentTarget).data("action");
			  var id = $(e.currentTarget).data("id");
			  router.applyRoute("admin/hotel/"+action+"/"+id);
		},
		AcceptFieldReturn: function (e) {
		
			var field=Object.keys(e[0]);
			var t= controller.renderTpl("MessageTpl");
			$("#Admin-"+field).remove();
			$("#msg").html(t({"status": "alert-success", "message": "You have accept field: "+field })).show().delay(3000).fadeOut('slow');
		},
		RejectFieldReturn: function (e) {
			
			var field=Object.keys(e[0]);
			var t= controller.renderTpl("MessageTpl");
			$("#Admin-"+field).remove();
			$("#msg").html(t({"status": "alert-success", "message": "You have rejected field: "+ field })).show().delay(3000).fadeOut('slow');
			$("input[name="+field+"]").val(e[0][field]);
		},
		multiselect: function(e) {
			
			multi = $('.multiselect').multiselect({
			      buttonClass: 'btn input-large',
			      buttonWidth: '300px',
			      maxHeight: 200,
			      width: "100%",
			      buttonText: function(options) {
			        if (options.length == 0) {
			          return 'None selected <b class="caret"></b>';
			        }
			        else if (options.length >3) {
			          return options.length + ' selected  <b class="caret"></b>';
			        }
			        else {
			          var selected = '';
			          options.each(function() {
			            selected += $(this).text() + ', ';
			          });
			          return selected.substr(0, selected.length -2) + ' <b class="caret"></b>';
			        }
			      }
			    });
		},
		addClassPosteddata: function (e) {
			e.preventDefault();
			$(e.currentTarget).addClass("postedData");
		}

    });
	
	controller.registerCallbackToAction("hotel","RejectField","RejectFieldReturn");	
	controller.registerCallbackToAction("hotel","AcceptField","AcceptFieldReturn");
	controller.registerCallbackToAction("hotel","enableHotels","enableHotelsReturn");	
	controller.registerCallbackToAction("hotel","disableHotels","disableHotelsReturn");
	controller.registerCallbackToAction("hotel","deleteFilters","deleteFiltersReturn");	
	controller.registerCallbackToAction("hotel","enableFilters","enableFiltersReturn");	
	controller.registerCallbackToAction("hotel","disableFilters","disableFiltersReturn");	
	controller.registerCallbackToAction("hotel","save","saveForm");
	controller.registerCallbackToAction("hotel","new","RedirectHotels");

	viewModel.get("init")({});
	
	$(".mainMenu").removeClass("active");
    $("#hotelMenu").addClass("active");
    
})();

});