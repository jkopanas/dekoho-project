var controller = "";
head.ready(function() {

					var TplLoad=function() {
				
						results = controller.db.records.template;
						var templates={},templatesScript={};templatesView={};dataTpl={};templatesLoadedData={};
						var o = $(results).find('script');
						var p = o.length;
						for (p; p--;) {
							templates[$(o[p]).attr('id')] = kendo.template($(o[p]).html());
							templatesScript[$(o[p]).attr('id')] = { "data":$(o[p]).data(),"value":$(o[p]).data('script') ,"force":$(o[p]).data('force')};
							templatesView[$(o[p]).attr('id')] = $(o[p]).data('view');
							templatesLoadedData[$(o[p]).attr('id')] = $(o[p]).data('loadata');
							dataTpl[$(o[p]).attr('id')] = {};
							controller.RegisteredCallback[$(o[p]).attr('id')]=[];
							controller.validator[$(o[p]).attr('id')] = "";
							controller.registerCallbackToAction($(o[p]).attr('id'),"render","requestForm");
							controller.registerCallbackToAction($(o[p]).attr('id'),"popup","requestPopup");
						}
						controller.db.trigger = "loadComplete";
						controller.db.async = true;
						controller.dataTpl = dataTpl;
						controller.templates = templates;
						controller.templatesView = templatesView;
						controller.templatesLoadedData = templatesLoadedData;
						controller.templatesScript = templatesScript;

						//Loaded Data 0 <-- Only cache
						//                    1 <-- Only php
						//                   2 <--  Both
						
						controller.dataTpl['cached'] = {};
				
						/*
						controller.dataTpl.cached['resultsData']= controller.db.records.data.results;
						controller.dataTpl.cached['filters']= controller.db.records.data.filters;
						controller.dataTpl.cached['user']= controller.db.records.data.user;
						controller.dataTpl.cached['total']= controller.db.records.data.total;
						controller.dataTpl.cached['pages']= controller.db.records.data.pages;
						*/
					};

					var Complete=function() { // Render Either a tpl or trigger a callback
						
						$("#loaderPage").hide();
						$("#page-dark").hide();
						
						results=controller.db.records;
						
						/*
						if (controller.templatesLoadedData[controller.db.tpl] || controller.templatesLoadedData[controller.db.tpl] == "2" ) {
							results = $.extend(results,controller.dataTpl.cached);
						}
						*/
						
						console.log(results);
						
						if (results.logout) {
							window.location = results.logout;
						}
						

						var tmp = controller.db.action;

						if (controller.templates[controller.db.tpl]) {
				
						for (i in controller.RegisteredCallback[controller.db.tpl] ) {		
							if (controller.RegisteredCallback[controller.db.tpl][i].hasOwnProperty(controller.db.action) ) {
								
								controller._trigger(controller.RegisteredCallback[controller.db.tpl][i][controller.db.action],results);
								
							}
						} 
						} else {
							var t = controller.renderTpl(results.template);
		        	    	$("#"+controller.view).html(t(results.data));
						}
						controller.db.action = ""; 
						controller.db.async = true;
					};
					
					controller = {
						exchange: $("input[name='exchange']").val(),
						symbol: $("input[name='currencySymbol']").val(),
						view: "ViewHotelier",
						templates : "",
						templatesScript : "",
						templatesView : "",
						templatesLoadedData: "",
						preRenderTpl: "",
						dataTpl: "",
						validator: {},
						LoadedScripts: [],
						RegisteredCallback: {},
						db : new mcms.dataStore({method : "POST",url : "",dataType : "json","async": false,trigger : "loadComplete",action: "" }),
			     	 	_trigger: function( type, data ) {
			    	 		data = data || {}; 	
			    	 
			    	 		if ($.isFunction(this[type])){
			    	 			eval(this[type]).apply(this, Array.prototype.slice.call(arguments, 1));
			    	 		} else {
			    	 				if ( eval(this.db.tpl) !== undefined) {
			    	 					if ( eval(this.db.tpl)['viewmodel'] !== undefined) {
			    	 					eval(this.db.tpl)['viewmodel'].get(type)(Array.prototype.slice.call(arguments, 1));
			    	 					}
			    	 				}
			    	 		}
				       	},
				       	requestPopup: function (data) {
				       		
				       			$("div#Popup").html(this.templates[this.db.tpl](data));
				       			controller.validator[this.db.tpl]=$("div#"+this.db.tpl).kendoValidator(ValidationObject).data("kendoValidator");
								controller.validator[this.db.tpl]._errorTemplate = kendo.template('<br/><span class="k-widget k-tooltip k-tooltip-validation">#=message#</span>');
				       			
				       		if ( (this.templatesScript[this.db.tpl]  && $.inArray(this.templatesScript[this.db.tpl].value, this.LoadedScripts) == -1) || this.templatesScript[this.db.tpl].force  ) {	
				       			this.loadScript(this.templatesScript[this.db.tpl].value);
							} else {	
								this._trigger('init',data);
							}
				       			
				       	},
				        requestForm: function(data) {

				        	    var view = (this.templatesView[this.db.tpl]) ? this.templatesView[this.db.tpl] : this.view;
				        	    data['app']=this;
				        	    this.dataTpl[this.db.tpl] = data;	
				        	   // MasterModel.set("page",this.db.tpl);
				        	    //MasterModel.set("render",this.templates[this.db.tpl](data));
				        	    
			        	    	$("#"+view).html(this.templates[this.db.tpl](data));

								controller.validator[this.db.tpl]=$("div#"+this.db.tpl).kendoValidator(ValidationObject).data("kendoValidator");
								controller.validator[this.db.tpl]._errorTemplate = kendo.template('<br/><span class="k-widget k-tooltip k-tooltip-validation">#=message#</span>');
								var triggerinit = true;
								var scr=this.templatesScript[this.db.tpl].value.split(",");
								if ( scr != "") {
									for (i in scr) {
										if ($.inArray(scr[i], this.LoadedScripts) == -1 ) {	
											this.loadScript(scr[i]);
											triggerinit=false;
										} 							
									}
								}
								
								if (triggerinit) {
									this._trigger('init',data);
								}
	
								/*
								if  ((this.templatesScript[this.db.tpl].value  && $.inArray(this.templatesScript[this.db.tpl].value, this.LoadedScripts) == -1 ) || this.templatesScript[this.db.tpl].force ) {
									
									this.loadScript(this.templatesScript[this.db.tpl].value);
								} else {	
									this._trigger('init',data);
								}
								*/
				        },
				        registerCallbackToAction: function(form,action,func) {
				        	var inj={};
				        	inj[action]=func;
				        	if (!$.isArray(this.RegisteredCallback[form])) {
				        		this.RegisteredCallback[form] =[];
				        	}
				        	this.RegisteredCallback[form].push(inj);
				        },
						loadScript: function(src) {

							
							var oHead = $("#"+this.view);
								var oScript= document.createElement("script");
								oScript.type = "text/javascript";
								oScript.src=src;
								oHead.after(oScript);
								this.LoadedScripts.push(src);
						//	}
						},
						calculateCurrency: function (price) {
							var newprice=parseFloat(this.exchange).toFixed(2)*price;
							return newprice.toFixed(2);
						},
						getArguments: '',
						renderTpl: function(tpl) {
							this.dataTpl[tpl] = {};
							//if ($("div#"+tpl).length != 0) {	
							//	return kendo.template($("#"+tpl).html());
						//	} else {
							if (!controller.templates[tpl]) { return function() { return ""; }; }
								return controller.templates[tpl];
						//	}
						},
						renderPreTpl: function(tpl) {
							if (controller.templates[tpl+"preRender"] === undefined ) { return; }
							var view = (controller.templatesView[tpl]) ? controller.templatesView[tpl] : controller.view;
							$("#"+view).html(controller.templates[tpl+"preRender"]({}));
						},
						executeRoute: function(route) {
							router.applyRoute(route);
							window.location.hash=route;
						},
						returnCurrentObj: function () {
							return eval(this.db.tpl)['viewmodel'];
						},
						translate: function (page,value,action) {
							controller.db.tpl= page;
							controller.getArguments=arguments;
							controller.db.action = (action !== undefined ) ? action : "render";
							var posteddata=page.split("translate");
							if ( action !== undefined ) { posteddata[1] = page; }
							var data = mcms.formData($('#'+posteddata[1]).find(".translations"),{ getData:true });
							controller.db.async = false;
							controller.db.data = { "route": { "call": "translations","page": page, "action": controller.db.action, "value":value }, "data": data };
							controller.db.url = "/ajax/hooks/hotelier/index.php";
							$(controller.db).loadStore(controller.db);
						},
						singleroute: function(page,value,action) {
										
							controller.db.tpl= page;
							controller.db.action = (action && action != "view" && action != "details" ) ? action : "render";
							
							/*
							if (!controller.templatesLoadedData[page]) {
								controller.db.records = controller.dataTpl['cached'];
								Complete();
								return;
							}
							*/
							var data = mcms.formData($('#'+page).find(".postedData"),{ getData:true });
							controller.db.async = false;
							controller.db.data = { "route": {"page": page, "action": action, "value":value }, "data": data };
							controller.db.url = "/ajax/hooks/hotelier/index.php";
							$(controller.db).loadStore(controller.db);
							
						},
						addOffer: function(page,value,action) {
							
							page = (page === undefined ) ? "addOffer" : page;
							controller.db.tpl= "addOffer";
							controller.db.action = (action) ? action : "render";
							var data = mcms.formData($('#'+page).find(".postedData"),{ getData:true });
							controller.db.async = false;
							controller.db.data = { "route": {  "call": "addOffer", "page": page, "action": action, "value":value }, "data": data };
							controller.db.url = "/ajax/hooks/hotelier/index.php";
							$(controller.db).loadStore(controller.db);
	
						},
						media: function (value,action) {
							controller.db.tpl = "photos";
							var page="photos";
							controller.getArguments=arguments;
							controller.db.action = (action !== undefined && action !== "hotel" && action !=="rooms") ? action : "render";
							var data = mcms.formData($('#'+page).find(".postedData"),{ getData:true });
							controller.db.data = { "route": { "call": "hotelManager","page": page, "action": action, "value":value }, "data": data };
							controller.db.url = "/ajax/hooks/hotelier/index.php";
							$(controller.db).loadStore(controller.db);
						},
						roomroute: function (page,roomid,action) {
							controller.db.tpl = page;
							controller.db.action =  (action == "save" || action == "create" ) ? action : "render";
							var data = mcms.formData($('#'+page).find(".postedData"),{ getData:true });		
							
							controller.db.data = { "route": {"page": page, "action": action, "value":roomid }, "data": data };
							controller.db.url = "/ajax/hooks/hotelier/index.php";
							$(controller.db).loadStore(controller.db);
						},
						roomsroute: function (hotelid,action) {
							controller.roomroute("rooms",hotelid,action);
						},
						roomnewroute:function (hotelid) {	
							controller.roomroute("room",hotelid,"new");
						},
						hotelroutenew: function(page,value) {
							controller.hotelroute(page,0,value);
						},
						locationroute: function(page,value,action) {
							controller.db.tpl = page;
							controller.getArguments=arguments;
							controller.db.action = (action !== undefined ) ? action : "render";
							var data = mcms.formData($('#'+page).find(".postedData"),{ getData:true });
							controller.db.data = { "route": { "call": "Locations","page": page, "action": action, "value":value }, "data": data };
							controller.db.url = "/ajax/hooks/hotelier/index.php";
							$(controller.db).loadStore(controller.db);
						},
						hotelroute: function(page,value,action) {
							controller.db.tpl = page;
							controller.getArguments=arguments;
							controller.db.action = (action !== undefined && !isFinite(action)) ? action : "render";
							var data = mcms.formData($('#'+page).find(".postedData"),{ getData:true });
							controller.db.data = { "route": { "call": "hotelManager","page": page, "action": action, "value":value }, "data": data };
							controller.db.url = "/ajax/hooks/hotelier/index.php";
							$(controller.db).loadStore(controller.db);
						},
						profileroute: function(page,action,value) {
							controller.db.tpl= page;
							controller.db.action = (action) ? action : "render";
							var data = mcms.formData($('#'+page).find(".postedData"),{ getData:true });
							controller.db.data = { "route": { "call": "profileManager", "page": page, "action": action, "value":value }, "data": data };
							controller.db.url = "/ajax/hooks/hotelier/index.php";
							$(controller.db).loadStore(controller.db);
						},
						popup: function (page,value,action) {	
							
							controller.db.tpl= page;
							controller.db.action = "popup";
							var data = mcms.formData($('#'+page).find(".postedData"),{ getData:false});
							controller.db.data = { "route": { "page": page,"action": action ,"value":value}, "data": data };
							controller.db.url = "/ajax/hooks/hotelier/index.php";
							//if(controller.validator[page].validate()) {
								$(controller.db).loadStore(controller.db);
							//}
						},
						adminAction: function (page,action,value) {
					
							controller.db.tpl = page;
							controller.getArguments=arguments;
							controller.db.action = action;
							var data = mcms.formData($('#'+page).find(".postedData"),{ getData:true });
							controller.db.data = { "route": { "call": "administratorActions","page": page, "action": action, "value":value }, "data": data };
							controller.db.url = "/ajax/hooks/hotelier/index.php?action="+action+"&value="+value;
							$(controller.db).loadStore(controller.db);
						},
						payment: function (page,value,action,transactionid) {
							
							controller.renderPreTpl(page);
							
							transactionid = (transactionid !== undefined ) ? transactionid.split("?") : [0];
							controller.db.tpl= page;
							controller.db.action = ( value == "StartCheckout" || value == "startgcc"  ) ? action : "render";
							//console.log(controller.db.action,page,"dss");
							var data = mcms.formData($('#'+page).find(".postedData"),{ getData:false });

							controller.db.data = { "data": data, "action": action, "post": $.parseJSON($("#ReturnPost").html()) };
							if (transactionid[1] !== undefined) {
								value = value+"&"+transactionid[1];	
							}
							controller.db.url = "/paypal/index.php?file="+value+"&transactionid="+transactionid[0];
							$(controller.db).loadStore(controller.db);

						}
					};

					
					$(controller.db).bind('loadTpl',TplLoad);
					$(controller.db).bind('loadComplete',Complete);
					controller.db.trigger = "loadTpl";
					controller.db.url = "/ajax/hooks/hotelier/loadTpl.php";
					$(controller.db).loadStore(controller.db);

})();
