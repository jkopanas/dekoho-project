(function ($, undefined) {
	$.tabber = function(obj,options) {
	    var obj = obj, options = options,templates = {},cache={},app={};
	    if (typeof(obj) !== 'object') obj = $(obj)[0];
	    if (typeof(options) !== 'object') options = { };

	    var defaults = {
	    	tabsContainer : '.tabs_content',//all tabs go here
	    	selector : 'rel',//identifies the tab to switch to
	    	tabClass: '.tab',//goes to each tab
	    	tabSelector: '#',//id or class
	    	selectedClass:'current',//class for selected tab
	    	clickSelector: 'a'//what the tab element will be
	    };

	    var options = $.extend(defaults,options);

	    var app = {
	    	options:options,
	        _trigger: function( type, data ) {
	            data = data || {};
	            if ($.isFunction(this.options[type])){
	                eval(this.options[type]).apply(this, Array.prototype.slice.call(arguments, 1));
	            }
	        },
	        hideTabs: function(){
	        	$(options.tabsContainer + ' ' + options.tabClass).hide();
	        },
	        switchTab: function(tab){
	        	this.hideTabs();
	        	$(options.tabSelector + '' + tab.attr(options.selector)).show();
	        	$(obj).find(options.clickSelector).removeClass(options.selectedClass);
	        	tab.addClass(options.selectedClass);
	        	this._trigger( "onTabChange",this,obj,tab );
	        },
	        init: function(){
	        	this.hideTabs();
	        	$(obj).find(options.clickSelector + ':first').addClass(options.selectedClass);
	        	$(options.tabsContainer + ' ' + options.tabClass +':first').show();
	        	this._trigger( "onAppLoaded",this,obj );
	        }
		}//END APP

		app.init();

	    $(obj).on('click',options.clickSelector,function(e){
	        e.preventDefault();
	       app.switchTab($(this));
	    });

    	return app;
    }//END CLASS

$.fn.tabber = function(options) {
if (typeof(options) !== 'object') options = { };

// Iterate over each object, attach Jcrop
this.each(function()
{

if ($(this).data('tabber')){
    return $(this).data('tabber');
}
else {
    $(this).data('tabber',$.tabber($(this),options))
}
});
}//END PLUGIN

})(jQuery);