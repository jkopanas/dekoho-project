/* This is a media galley application for the MCMS. It can be used as a plugin
* It enables upload of media, editing and selection from the global media pool
* It runs autonomously so you can use it in a popup as well
* call it with : $('#mediaGallery').mediaGallery({ a:1});
* Requires : 
* -- Uploaded plugin
*/
(function ($, undefined) {
    
    $.mediaGallery = function(obj,options) {
        var obj = obj, options = options,templates = {},cache={},app={};
    	if (typeof(obj) !== 'object') obj = $(obj)[0];
    	if (typeof(options) !== 'object') options = { };
        
        var defaults = {
            templatesFile : '/skin/common/mediaGallery.tpl',
            messages : {
                errorFileType : 'File type not allowed111',
                btnText: "Select Files"
            },
            modules: [ 
                {id:'addFromUpload',text:'upload from computer'},
                {id:'addFromUrl',text:'from url'},
                {id:'addFromLibrary',text:'from library'},
                {id:'addFromGallery',text:'from gallery'},
                {id:'modifyImage',text:'modify image'}
                
            ],
            dbDefaults : {
                method: "POST",
	            dataType: "json",
                trigger:"loadComplete"
            },
            dbURL : {
                'add' : '/ajax/loader.php?file=items/itemsCategoriesNew.php&action=addFeatured&module=' + mcms.settings.module,
                'read': '/ajax/loader.php?file=items/itemsCategoriesNew.php',
                'delete': '/ajax/loader.php?file=items/itemsCategoriesNew.php&action=deleteFeatured&module=' + mcms.settings.module,
                'update': '/ajax/loader.php?file=items/itemsCategoriesNew.php&action=updateFeaturedOrder&module=' + mcms.settings.module
            }
        };

     var options = $.extend(defaults,options);

     var db = new mcms.dataStore(options.dbDefaults);   
     
        /* read templates */
        if (typeof templates.main == 'undefined') {
           	$.ajax({
        	  url: options.templatesFile,
        	  async:false,
              useController:false,
        	  success: function(data){
        	  	var o = $(data).find('script');
        	  	var p = o.length;
    
        	  	for (p;p--;) {
        	  		templates[$(o[p]).attr('id')] = kendo.template($(o[p]).html());
        	  	}
        	  },
        	  cache:false,
        	  dataType: 'html'
        	});
        }//END TEMPLATES

        /* METHODS AND STUFF */
       var app = {
            options:options,
            templates:templates,
     	 	_trigger: function( type, data ) {
    	 		data = data || {};
    	 		if ($.isFunction(this.options[type])){
    	 			eval(this.options[type]).apply(this, Array.prototype.slice.call(arguments, 1));
    	 		}
	       	},
        _switchTab: function(e) {
    		var current = (typeof e == 'string') ? obj.find('ul.tabsS a[rel='+e +']') : e;
    		obj.find('div.tabs_content .tab').hide();
    		obj.find('div.tabs_content #'+ current.attr('rel')).show();
    		obj.find('ul.tabsS a').removeClass('current');
           
    		current.addClass('current');
            if (options.useController) {
    		  window.location.hash = current.attr('href');
            }
	   }
            
        };//END APP
        
        /* modules */
        /* 
        * Module to upload a file from the computer. Requires a url as well as actions for each step of the process.
        * Requires : 
        * -- uploader methods
        * -- uploader urls
        */
        app.addFromUpload = function(){
            var that = this,allowedFiles='',acceptFiles='';
            //allowedFiles => ['zip',jpg]
            //acceptFiles => ['image/jpeg','image/png']

            if (options.fileType == 'thumb') {
                options.fileType = 'images';
            }

            if (typeof options.fileTypes != 'undefined') {
                var allowedFiles = (typeof options.fileTypes[options.fileType] != 'undefined') ? options.fileTypes[options.fileType].extentions : options.fileTypes;
                var mediaType = (typeof options.fileTypes[options.fileType] != 'undefined') ? options.fileTypes[options.fileType].type  : options.mediaType;
                allowedFiles = allowedFiles.split(';');
            }

        var uploader = new qq.FineUploader($.extend({
            element: obj.find(".uploader")[0],
            autoUpload: true,
            debug:false,
            text:{ uploadButton:  options.messages.btnText},
            request: {
                params:{ },
                endpoint: '/ajax/loader.php?file=items/upload.php&action=xhr&module=' + mcms.settings.module
            },
            callbacks: {
                onComplete: function(id,fileName,obj){

                },
                onSubmit: function(id, fileName){
                    var x = fileName.split('.').pop();
                    uploader.setParams({ ext:x});
                },
                onValidate: function(fileData,isBatch){
                    
                },
                onUpload: function(id,fileName){

                }
            },
            validation: {
                allowedExtensions: allowedFiles,
                acceptFiles:acceptFiles
            }
       },options.uploader)); 

            this.uploader = uploader;
            return uploader;
        };//END MODULE
 
        app.addFromUrl = function(){
            var that = this;
            
        };//END MODULE
        
        app.addFromLibrary = function(){
            var that = this;

        };//END MODULE
        
        app.addFromGallery = function(){
            var that = this;
            
        };//END MODULE  
        
		/*
		* Modifies an image with functionality like crop or scale
		* Should be invoked through method
		* Methods can be overwritten through options.modifyImage
		* Requires :
		* -- image info object that contains full image information from the DB
		* -- Jcrop library
		*/
       app.modifyImage = function(){
            var that = this;
            
            return $.extend({
            	settings:{},
            	init: function(obj){
            		this.obj = obj;
            	},
            	loadImage: function(obj){
            		
            	},
            	saveCrop: function(){
            		
            	}
            },options.modifyImage);
            
        };//END MODULE
         
        /*
        * Shows options after a file has been uploaded
        */
        app.uploadHandler = function(){
            var that = this;

            templates['uploadHandler'](that);
        };//END MODULE  

        //Attach to the container

         obj.append(templates['main'](app));


        /* Load modules */
        for(i=options.modules.length;i--;){
            app[options.modules[i].id]();    
        }
        
       
        /* EVENT BINDS */
        obj.find('.tabsS a').on('click',function(e) {
        	e.preventDefault();
        	app._switchTab($(this).attr('rel'));
        });

        
        app._trigger('onAppLoaded',this);
        return app;
   };//END CLASS


 $.fn.mediaGallery = function(options) {
	if (typeof(options) !== 'object') options = { };

	// Iterate over each object, attach Jcrop
	this.each(function()
	{
    
	    if ($(this).data('mediaGallery')){
			return $(this).data('mediaGallery');
		}
        else {
            $(this).data('mediaGallery',$.mediaGallery($(this),options))
        }
    });
}//END PLUGIN
 


})(jQuery);