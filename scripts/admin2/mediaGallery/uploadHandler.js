head.js('/scripts/uploader/uploader.js');
head.js('/scripts/jcrop/js/jquery.Jcrop.js');
head.js('/scripts/admin2/mediaGallery/index.js');
head.ready(function(){
	var uploaderSettings = {
			multiple:true,
			async: {
					        saveUrl: '/ajax/loader.php?file=items/upload.php&module=content&action=simple',
					        removeUrl: '',
					        removeField: "fileNames[]",
					        autoUpload:false,
							multiple:true
					    },

			upload: function(e){
				
				e.data = $.extend(e.files[0],{ process:false,fileParam:this.element.attr('name')});
			}
	};

	var handler = {
		win : {},
		createWindow: function() {
			var that = this;
			this.win = $('<div id="uploadHandlerWindow" class=""></div>').appendTo('body');
			this.win.kendoWindow({
		        width: "800px",
		        height:'90%',
		        title: $.lang.upload,
		        close: that.destroyWindow(),
		        visible:false,
		        modal:true
	  	  	});

		},
		destroyWindow: function() {
			this.win.remove();
		}
	}//END OBJ

    $(document).on('click','.imageUpload',function(e){//delegate cause live registers the event on every load
		e.preventDefault();
		handler.createWindow();
		var that = $(this);
		uploaderSettings.callbacks = {};
		uploaderSettings.callbacks.onComplete = function(id,fileName,TempObj) {
			$('#'+ that.attr('rel')).val(TempObj.request.path+"/"+fileName);
		}
		$(handler.win).mediaGallery({ uploader:uploaderSettings, modules:[{ id:'addFromUpload',text:'upload from computer'},{ id:'addFromUrl',text:'upload from URL'}], 
			fileType:'thumb',fileTypes:$(this).attr('data-fileTypes'),
			mediaType:$(this).attr('data-mediaType'),
			onUploaderSelect:function(e,b,r){  },onAppLoaded:function(e){ 
		
		}});
		
        handler.win.data("kendoWindow").open();
        handler.win.data("kendoWindow").center();
	});
});