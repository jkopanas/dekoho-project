/*
* Will create an autocomplete thingy
*/
(function ($, undefined) { 
	
$.AutoComplete = function(obj,options) {
	var obj = obj, options = options,templates = {},cache={},app={},resultsCache={};
	if (typeof(obj) !== 'object') obj = $(obj)[0];
	if (typeof(options) !== 'object') options = { };
    
    var defaults = {
        templatesFile : '/skin/common/mediaGallery.tpl',
        template:'',
        delay:500,
        minChars:3,
        dataSource:'',
        separator: ',',
        autoCompleteLength:0,
        resultsPlaceHolder:'AutoCompleteresults',
        matchesClass:'matched',
        dbDefaults : {
            method: "POST",
            dataType: "json",
            trigger:"loadComplete"
        },
        url:''
    };
    
    var options = $.extend(defaults,options);
    var autoCompleteLength = options.minChars; 
    var db = new mcms.dataStore(options.dbDefaults);   
    var placeHolder = ($(options.resultsPlaceHolder).length ==0) ? $('<ul id="'+ options.resultsPlaceHolder + '" style="width:' + obj.width() + 'px; background:white;z-index:999;display:none;" class="'+options.resultsPlaceHolder+'"></ul>').insertAfter(obj) : $(options.resultsPlaceHolder);
	obj.attr('autocomplete','off');
 	var delay = (function(){
	  var timer = 0;
	  return function(callback, ms){
	    clearTimeout (timer);
	    timer = setTimeout(callback, ms);
	  };
	})();   
     
    var app = {
		obj:obj,    	
        options:options,
        templates:templates,
 	 	_trigger: function( type, data ) {
	 		data = data || {};
	 		if ($.isFunction(this.options[type])){
	 			eval(this.options[type]).apply(this, Array.prototype.slice.call(arguments, 1));
	 		}
       	},
       	updateHaystack: function(input, needle) {
       		needle = needle.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");//escape special chars
			var r =  new RegExp(needle, "gi");
			return input.replace(r,function(m) { return '<span class="'+ options.matchesClass + '">' + m + '</span>'});
		},
		createAutoComplete: function(obj,term,records)
		{
			placeHolder.children().remove();
			var list ='';
			var x  =records.length;
			for (j=0;x>j;j++){
				var node = (typeof options.template === 'function') ? options.template(records[j]) : records[j].res;
				list += '<li id="res-' + j +'">' + this.updateHaystack(node, term) + '</li>'
			}
			placeHolder.append(list);
			placeHolder.show();
		},
		hideResults: function(){
			placeHolder.hide();
			placeHolder.empty();
		},
		parse: function(p) {
			var input ='';
			if (p.indexOf(options.separator) != -1){
				var tmp = p.split(options.separator);
				input = tmp[tmp.length-1];
			} else {
				input = p;
				
			}
			return input;
		},
		splitInput: function(string,r){
			var tmp = string.split(options.separator);
			var p = tmp.length;
			tmp[p-1] = r;
			return tmp.join(',');
		},
		selectItem: function(inpt,input){
			if(typeof resultsCache[input] == 'undefined' || placeHolder.find("li.hovered").index() == -1) { return; }		//Nothing selected from the dropdown or no results	
			$(inpt).val(this.splitInput($(inpt).val(),resultsCache[input][placeHolder.find("li.hovered").index()]['res']) + options.separator);
			/* BRING CURSOR TO END AND REFOCUS */
			$(inpt).focus();
			
            // If this function exists...
            var x = (inpt[0]) ? inpt[0]  : inpt;
            if (x.setSelectionRange)
            {
                // ... then use it
                // (Doesn't work in IE)
                // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
                var len = $(inpt).val().length * 2;
                x.setSelectionRange(len, len);
            }
            else
            {
                // ... otherwise replace the contents with itself
                // (Doesn't work in Google Chrome)
                $(inpt).val($(inpt).val());
            }
            // Scroll to the bottom, in case we're in a tall textarea
            // (Necessary for Firefox and Google Chrome)
            this.scrollTop = 999999;
            /* END BRING CURSOR TO END AND REFOCUS */
			app.hideResults();
			
		}
        
    };//END APP
     
    
    /* EVENT BINDS */
	$('html').on('click',function(event) {
		
	    if($(event.target).parents().index(obj) == -1) {
			
	        app.hideResults(); 
	    }        
	});

	$(obj).on('keyup',function(e){
		var code = e.keyCode || e.which;

		var input = app.parse($(this).val());
		
		if (code == 37 || code == 39) //LEFT -- RIGHT
		{
			//Do something
			e.stopPropagation();
			return;
		}
		else if (code === 40) { //Down arrow
			e.preventDefault();
			e.stopPropagation();
			if(placeHolder.find("li.hovered").index()==$("li").length-1) return;
			if (placeHolder.find("li.hovered").length == 0) { //if no li has the hovered class
			    placeHolder.find("li").eq(0).addClass("hovered");
			    
			} else {
			    placeHolder.find("li.hovered").eq(0).removeClass("hovered").next().addClass("hovered");
			}
			
			app._trigger('onDownArrow',this);
			return;
		}
		else if (code === 38) { //Up arrow
			e.preventDefault();
			e.stopPropagation();
			if(placeHolder.find("li.hovered").index()==0) return;
			if (!placeHolder.find("li.hovered")) { //if no li has the hovered class
			    placeHolder.find("li.hovered").eq(0).removeClass("hovered");
			} else {                 
				placeHolder.find("li.hovered").eq(0).removeClass("hovered").prev().addClass("hovered");
			}
			
			app._trigger('onUpArrow',this);
			return;
		}
		else if (code === 13) {//ENTER. BLOCK IT CAUSE WE ARE TRACKING IT ON ANOTHER EVENT
			e.preventDefault();
			e.stopPropagation();
			return;
		}

		if(input.length >= autoCompleteLength) {
//			autoCompleteLength = input.length;
			
			if (input in resultsCache)
			{
				app.createAutoComplete($(this),input,resultsCache[input]);
			}//END CACHED
			else {
				var me = $(this);
				delay(function(){
					db.parent = me;
					db.term = input;
					db.data = {data:input};
					db.url = options.url;
					db.records = {};
					$(db).loadStore(db);
				}, options.delay );
			}//END NON CACHED
		}//END LOOKUP
		else { app.hideResults(); }
	});
	
	
	$(obj).on('keypress',function(e){
		var code = e.keyCode || e.which;
		if (String.fromCharCode(code) == options.separator) {
			app.hideResults();
			return;	
		}
		var input = app.parse($(this).val());
		if (code === 13) {//ENTER
			e.preventDefault();
			e.stopPropagation();
			var selected = placeHolder.find("li.hovered");
			app.selectItem(this,input);
			app._trigger('onPressEnter',this,selected);
			return false;
		}
		else if (code === 38 || code == 40) { //Up DOWN DISABLE
			e.preventDefault();
		}
	});
	
	$(obj).on('keydown',function(e){
		var code = e.keyCode || e.which;

	});
	
	placeHolder.on('click','li',function(e){//delegate
		$(this).addClass('hovered');
		app.selectItem(obj,app.parse(obj.val()));
		app._trigger('onSelect',obj,$(this));
	});
	
	$(db).bind('loadComplete',function(){
		records = db.records;
		app.hideResults();
		if (typeof records !== 'undefined')
		{
			resultsCache[db.term] = records;
			app.createAutoComplete(db,db.term,records);
		}//END RESULTS
	});

    
}//END CLASS
	

	
	
$.fn.AutoComplete = function(options) {
	if (typeof(options) !== 'object') options = { };

	// Iterate over each object, attach Jcrop
	this.each(function()
	{
    
	    if ($(this).data('AutoComplete'))
		{
			return $(this).data('AutoComplete');
		}
        else {
            $(this).data('AutoComplete',$.AutoComplete($(this),options))
        }
    });
}//END PLUGIN
 
})(jQuery);