var cache = {};
var formData = {}, x = {};
var templates = {};
head.js("../scripts/admin2/lib/fancybox/jquery.fancybox-1.3.4.pack.js");
head.ready(function(){
$(".tabsS").flowtabs("div.tabs_content > div");

var lang = ($('input[name=loaded_language]').val() =='gr') ? 'el' : $('input[name=loaded_language]').val();

 userupload = new $.dataStore({	 
	    method: "POST",
	    url:"/ajax/ckeditor/show_files.php",
//		    dataType: "json",
		trigger:"loadComplete"
	});
	
$(userupload).bind('loadComplete',function(){				
	records = userupload.records;
	var nodes;
	$.each(records, function(key, val) {
		nodes += '<li><img src="'+val.thumb+'" class="myimage" rel="'+ val.img + '" /></li>';
	});
	$(nodes).appendTo("#userfiles > ul");
	
});

$(userupload).bind('savePromotion',function(){	
	$('.autoSave').html('last saved at <strong>' + userupload.records.time + '</strong>').css('background','#f0f0e2');
	setTimeout (function() {$('.autoSave').css('background','white');}, 500);
	if (userupload.records.promoID && userupload.records.newItem == true)
	{
		document.location.href.replace(document.location.href + '&promoID='+userupload.records.promoID)
		window.history.pushState("string", document.title, window.location.pathname + window.location.search + '&promoID='+userupload.records.promoID);
		$('input[name=promoID]').val(userupload.records.promoID);
		$('.savePromo').attr('href',$('.savePromo').attr('href').replace('promoID=','promoID='+userupload.records.promoID));
	}
});

$(userupload).bind('savePromotionContinue',function(){	
	if (userupload.records.promoID && userupload.records.newItem == true)
	{
		$('.savePromo').attr('href',$('.savePromo').attr('href').replace('promoID=','promoID='+userupload.records.promoID));
	}
	window.location = userupload.clickedEl.attr('href');
});

$(userupload).bind('schedulePromotion',function(){	
	
});

$('.savePromo').live('click', function(e){
	e.preventDefault();
	userupload.url="../ajax/promotions.php?action=autosave";
	userupload.data = new $.FormData($('.secondary'),{'ReturnType':'array'});//IDS AND STUFF
	userupload.data.data = new $.FormData($('.post'),{'ReturnType':'array'});//ACTUAL FORM
	userupload.dataType = 'json';
	userupload.trigger = $(this).attr('id');
	userupload.clickedEl = $(this);
	$(userupload).loadStore(userupload);	
});

$('textarea.post').ckeditor(function() {},
{
	skin : 'kama',
	height:'400px',
	language : lang
}
);

 var editor = $('textarea.post').ckeditorGet();


userupload.data = {};
userupload.dataType = 'json';
				
$(userupload).loadStore(userupload);

$(userupload).bind('showTemplates',function(){	
	records = userupload.records;
	templates = records;
	var html = '';
	
	$.each(records, function(k,v){
	
	   $('#Tmp').html($('#themesTemplate').html()); 
	   $('#Tmp').render([
	   	{'id': v.template_id,
	     'image_url': v.thumb,
	     'title': v.name
	                        }
	   ]);   
	   html += $('#Tmp').html();
	  
	});//END FOREACH 

	$('#themesContainer').html(html);
});



$(userupload).bind('insertTemplate',function(){	
	//console.log(userupload);
	editor.setData(userupload.records);
});

$(userupload).bind('previewTemplate',function(){	
    $.fancybox({
        'transitionIn'	: 'fade',
        'scrolling'		: 'no',
        'titleShow'		: false,
        'overlayOpacity'	: '0',
        'hideOnOverlayClick': false,
        'type'				:'iframe',
        'href'				: userupload.records
    });
});

$("#showTemplate").live("click",function(e) {
	e.preventDefault();
    $.fancybox({
        'transitionIn'	: 'fade',
        'scrolling'		: 'no',
        'titleShow'		: false,
        'overlayOpacity'	: '0',
        'hideOnOverlayClick': false,
        'type'				:'ajax',
        'href'				: '../ajax/select_template.php?action=previewTemplate&id=' + $(this).attr('rel')
    });

});

$("#insertTemplate").live("click",function() {
	userupload.url = '../ajax/select_template.php?action=previewTemplate&id=' + $(this).attr('rel');
	userupload.dataType = 'html';
	userupload.trigger = 'insertTemplate';
	$(userupload).loadStore(userupload);
});


$("#themes").live("click",function() {

	if (!templates.length){
	userupload.url = '../ajax/select_template.php?action=showTemplates';
	userupload.dataType = 'json';
	userupload.trigger = 'showTemplates';
					
	$(userupload).loadStore(userupload);
	}
});
	
$("#userfiles").find(".myimage").live("dblclick",function() {
 var src = $(this).attr('rel');
 var img= "<img src='"+src+"'/>";
 editor.insertHtml(img);

});


var d = $('#userfiles > ul');
var newFile = {};
var uploader = new qq.FileUploader({
    element: document.getElementById('file-uploader'),
    UploadDropZone : {element : $('#userfiles')[0]},
    action: '/ajax/ckeditor/upload.php?action=uploadfromfilemanager',

    onSubmit: function(id, fileName){
    			$( "#progressbar" ).progressbar({value: 0});
    			uploadID = id;
				newFile = $('<li id="img-'+id+'"><span class="qq-upload-spinner"></span><span class="progress"></span></li>').appendTo(d);
		},
		onProgress: function(id, fileName, loaded, total){		
			var percent = Math.round((loaded/total)*100);
			$( ".ui-progressbar-value" ).text( percent + "%");
			var progressBar = $('#progressbar');
			progressBar.progressbar({value:  percent});
			$( "#progress_val" ).val(  percent + "%" );
			$('#img-'+id+ ' .progress').html('<br />'+ percent + "%");
			
		},
	onComplete: function(id, fileName, responseJSON){
			$('#img-'+id).html('<img src="' + responseJSON.dir + '/thumbs/' + responseJSON.thumb +'" rel="' + responseJSON.dir + '/' + fileName +'" class="myimage" />');
			$("#userfiles").animate({ scrollTop: $("#userfiles").attr("scrollHeight") }, 3000);
		},
    messages:{'buttonMsg':$.lang.upload, 'success':$.lang.success},
    allowedExtensions: ['jpg','png','gif'],
    //params: {'data' :''},
    debug: false
});



formData = new $.FormData($('.post'),{'ReturnType':'array'});
cache = formData;
autoSave(); // Initiate the auto-saving.
});


function autoSave()
{
	
	formData = new $.FormData($('.post'),{'ReturnType':'array'});//IDS AND STUFF
		if (!Object.equals(cache,formData))
		{
			cache = formData;
			userupload.url="../ajax/promotions.php?action=autosave";
			userupload.data = new $.FormData($('.secondary'),{'ReturnType':'array'});//IDS AND STUFF
			userupload.data.data = formData;//ACTUAL FORM
			userupload.trigger = 'savePromotion';
			$(userupload).loadStore(userupload);
		}
	
		setTimeout("autoSave()", 20000); // Autosaves every 20 sec.
}


Object.equals = function( x, y ) {
  if ( x === y ) return true;
    // if both x and y are null or undefined and exactly the same

  if ( ! ( x instanceof Object ) || ! ( y instanceof Object ) ) return false;
    // if they are not strictly equal, they both need to be Objects

  if ( x.constructor !== y.constructor ) return false;
    // they must have the exact same prototype chain, the closest we can do is
    // test there constructor.

  for ( var p in x ) {
    if ( ! x.hasOwnProperty( p ) ) continue;
      // other properties were tested using x.constructor === y.constructor

    if ( ! y.hasOwnProperty( p ) ) return false;
      // allows to compare x[ p ] and y[ p ] when set to undefined

    if ( x[ p ] === y[ p ] ) continue;
      // if they have the same strict value or identity then they are equal

    if ( typeof( x[ p ] ) !== "object" ) return false;
      // Numbers, Strings, Functions, Booleans must be strictly equal

    if ( ! Object.equals( x[ p ],  y[ p ] ) ) return false;
      // Objects and Arrays must be tested recursively
  }

  for ( p in y ) {
    if ( y.hasOwnProperty( p ) && ! x.hasOwnProperty( p ) ) return false;
      // allows x[ p ] to be set to undefined
  }
  return true;
}