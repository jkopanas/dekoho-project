(function ($) {

$.FormFields = function(options) {
	var formFieldTemplates = {};
	var opts = {
		prefix : 'settings_',
		classes : 'toSave',
		extra : ''
	};
	
	$.ajax({
	  url: '/skin/common/formFields/jsFields.tpl',
	  async:false,
	  success: function(data){
	  	var o = $(data).find('script');
	  	var p = o.length;
	
	  	for (p;p--;) {
	  		formFieldTemplates[$(o[p]).attr('data-type')] = $(o[p]).html();
	  	}
	  },
	  cache:false,
	  dataType: 'html'
	});
	var settings = $.extend(opts,options);
	
	return {
		settings : settings,
		renderField : function(field,obj,val) {
			
			var t =  kendo.template(formFieldTemplates[field.type]);
			
			return t({
				field : field, me : obj, settings : settings,val:val
			});
		}
	};

};

})(jQuery);