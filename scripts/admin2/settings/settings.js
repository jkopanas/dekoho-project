var LockEdit= false;
var model = '';
var drag = '';
head.ready(function(){
	
	var SettingsForm = kendo.template($("#OpenEditSettings").html());
    var db = new mcms.dataStore({
        method: "POST",
        url: "",
        dataType: "json",
        trigger: "loadComplete"
    });
    

    	var url = "/ajax/loader.php?file=settings.php&action=getSettings";
    
    	var filter = {};
    
    	var griddatasource = { 
    			url:url,
    			data: {
    				module: $('input[name=current_module]').val()
    			},
    			root: "results" ,
    			receiveData: 'ReturnData',
    			filter: filter
    		};
    		
  		var extraColumns = { 
  				filterable: false,
  				command: "",
  				width: '12%',
  				order: 1000  				
  		};
  		
  		var gridkendo = {
  				editable: {
  					mode: 'inline',
  					confirmation: false
  				},
  				height: '400px',
  				pageable: false,
  		   		detailTemplate: function (e) {
        			t=kendo.template($("#OpenEditSettings").html());
        			return t( { item: e } );
        		},
        		detailInit: function(e) {
        			model = e.data; 
					var detailRow = e.detailRow;
					$('#dataTable tbody.settingcontent').sortable({handle:'td.dragHandle' });
                    detailRow.find(".tabstrip").kendoTabStrip({
                        animation: {
                            open: { effects: "fadeIn" }
                        }
                    }).css('padding','10px');
                   
        		}
  		}
  	
  		var grid ='';

    	grid = $("#gridview").kgrid({ hasFilter: 16, toolbar: $("#template-toolbar-PagesSearch").html() , dataSource: griddatasource, extra: extraColumns, kendo: gridkendo }).data('grid');
    	grid.init();
    	
		
		var viewModel = kendo.observable({
        				inputValue: function (e) {
        					$("#"+e.target.id+$("#"+e.target.id).data('id')).html($("#"+e.target.id).val());
        				},
        				SelectValue: function(e) {
        					$("#"+e.target.id+$("#"+e.target.id).data('id')).html($("#"+e.target.id).val());
        				}
    			});
		
		$(".k-toolbar").delegate(".CreateSet", "click", function(e) {
			
			if (!LockEdit) {
				objG=grid.ReturnGrid();
     			objG.addRow();
     			objG.expandRow($("#gridview").find("tr.k-master-row").first());
     			$(".SettingData:first").focus();	
    			kendo.bind($(".formEl_a"), viewModel);
        		e.preventDefault();
        		LockEdit=true;
        		$(objG.wrapper.find("tbody")).sortable( "option", "disabled", true );
			}
    	})
    	
    	$('.EditSet').live('click', function() {
  			if (!LockEdit) {
				objG=grid.ReturnGrid();
				objG.expandRow($(this).parents('tr'));
				objG.editRow($(this).parents('tr'));
				kendo.bind($(".formEl_a"), viewModel);
				LockEdit=true;
			
				$(objG.wrapper.find("tbody")).sortable( "option", "disabled", true );
  			}
    	});
    	
    	
    	$(".CancelEdit").live('click',function() {
    			
    				objG=grid.ReturnGrid();
    		    	objG.collapseRow($(this).parents('tr').prev());    			
    				
    				objG.cancelChanges();
    				
    				$(this).parents('tr').remove();
    				LockEdit=false;
    				$(objG.wrapper.find("tbody")).sortable( "option", "disabled", drag );
    	 });
    
    	 
        var kendoWindow = $("#delete-confirmation").kendoWindow({
                title: $.lang.menudelete,
                resizable: false,
                visible: false,
                width:"180",
                modal: true
        }).data("kendoWindow");
    	
    	$(".DeleteSet").live('click',function(e){
    		e.preventDefault();
			var id = $(this).data('itemid');
			
			var templates = kendo.template($("#delete-confirmation_menu").html());	    
			
			kendoWindow.content(templates({ item: id }));
			kendoWindow.center().open();
	 	   		
    	});
      			
      			
   		$(".delete-confirm,.delete-cancel").live('click',function() {
   				
   					if ($(this).hasClass("delete-confirm")) {
   						
   						var id=$(this).data('id');
   						db.url = '/ajax/loader.php?file=settings.php&action=delete&id=' + id;
      					db.trigger ='DeleteSettings';
      					$(db).loadStore(db);
      					objG=grid.ReturnGrid();
      					objG.dataSource.read();
                    }
             
                    kendoWindow.close();
                                    
        });
    	
    	
    	
    	$("select#type").live('change', function(e){
    
        	if($(this).val()=="select")
        	{
        	    $("div.multioptions").removeClass("hidden");
        	}
        	else
        	{
        	    $("div.multioptions").addClass("hidden");
        	}
        
    	});
    

        $("#addRow").live("click",function(e){
    		e.preventDefault();
       		var rows = parseInt($("#icounter").val());
       		$("#dataTable tbody.settingcontent").append('<tr class="dataRow" id="row'+rows+'"> ' +
       																				'<td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>' +
       																				'<td><input type="text" name="values[]" class="SettingDataJson inpt_a" value=""></td>' +
       																				'<td><input type="text" name="translation[]" class="SettingDataJson inpt_a" value=""></td>' +
       																				'<td><input type="radio" name="default"  class="SettingData inpt_a" value="0" ></td>' +
       																				'<td><a href="\#" class="removeRow" rel="row'+rows+'">Remove</a></td></tr>')
			rows++;
			$("#icounter").val(rows);
    });

    $(".removeRow").live("click",function(e){
    
    	e.preventDefault();
        var row = $("#"+$(this).attr('rel'));
        row.remove();
        
    });
    
	$("#SaveSetting").live("click",function(e) {
		objG=grid.ReturnGrid();
		var data = new mcms.formData($('.SettingData'),{'ReturnType':'array'});
		var settings = new mcms.formData($('.SettingDataJson'),{'ReturnType':'array'});
		settings = (data.type == "select" ) ? settings : [];
		data.type = (data.type == "select") ? data.select_type : data.type;
		if ($(this).data('id')) {
			
			db.url = '/ajax/loader.php?file=settings.php&action=update&id='+$(this).data('id');
      		db.trigger ='UpdateSetting';
      		db.uid = $(this).parents("tr").prev().data('uid');
      		db.data = { data: data, settings: settings };
      		$(db).loadStore(db);    
      		
		} else {

			db.url = '/ajax/loader.php?file=settings.php&action=create';
      		db.trigger ='CreateSetting';
      		db.uid = $(this).parents("tr").prev().data('uid');
			db.data = { data: data, settings: settings };
      		$(db).loadStore(db);   
			
		}

		
	});    
	
	
	$(db).bind('UpdateSetting',function(){
			
			records=db.records;
			grids = grid.ReturnGrid();
			
    	  	var tr = grids.dataSource.getByUid(db.uid);
      		o = {};
      		$.each(records, function (index, value) {
      				
              		o[index] = value;
      		});
      		for (var attrname in tr) { tr[attrname] = (o[attrname]) ? o[attrname] : tr[attrname]; }

      		grids.dataSource.read();
			LockEdit=false;		
      		$(grids.wrapper.find("tbody")).sortable( "option", "disabled", drag );
      		
	});
	
	$(db).bind('CreateSetting',function(e){
			
			records=db.records;
			grids = grid.ReturnGrid();

			o = {varName: '', title: '', type: '', file: '', options: ''}; 
			for (var attrname in o) {			
				if (model[attrname] !== 'undefined' ) {
				 	model[attrname] = records[attrname];	
				}
			}
	        grids.dataSource.read();
			LockEdit=false;
			$(grids.wrapper.find("tbody")).sortable( "option", "disabled", drag );
			e.preventDefault();

	});
    
    
});
	
	function ReturnData () {
		grid = $("#gridview").kgrid().data('grid');
		
		$('col.k-hierarchy-col').addClass('hidden');
		$('td.k-hierarchy-cell').addClass('hidden');
		$('th.k-hierarchy-cell').addClass('hidden');

		$.each(grid.ReturnGrid().wrapper.find('tbody > tr'),function(index,value) {
				$(value).find("td:eq(1)").addClass("dragHandle");
		});
		
		if (grid.ReturnGrid().dataSource.options.transport.read.data.filter) {
			drag=false;
			$(grid.ReturnGrid().wrapper.find("tbody")).sortable( "option", "disabled", false );
			$(grid.ReturnGrid().wrapper.find("tbody")).sortable({ handle:'td.dragHandle', stop: UpdateOrder, disabled: false });			
		} else {
			drag=true;
			$(grid.ReturnGrid().wrapper.find("tbody")).sortable( "option", "disabled", true );
			$(grid.ReturnGrid().wrapper.find("tbody")).sortable({ handle:'td.dragHandle', stop: UpdateOrder, disabled: true });
		}

	}
	
	function UpdateOrder (e,ui) {

			var tr = $(grid.ReturnGrid().wrapper.find("tbody > tr"));
			var ar = [];
			
			var db = new mcms.dataStore({
        		method: "POST",
        		url: '/ajax/loader.php?file=settings.php&action=order',
        		dataType: "json",
        		trigger: "OrderSetting"
    		});
			
			$.each(tr,function(index,value){
				
				ar.push($(value).find("td:eq(1) > img").data('id'));
				
			});

			db.data = { id: ar };
      		$(db).loadStore(db);   
		
		
	}

	
