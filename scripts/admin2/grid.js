﻿
(function($) {

    $.fn.kgrid = function(options)
    {
        return this.each(function()
        {
            var element = $(this);
			
            // Return early if this element already has a plugin instance
            if (element.data('grid')) return;

            // pass options to plugin constructor
            var myplugin = new grid(this, options);
		           
            // Store plugin object in this element's data
            element.data('grid', myplugin);
        });
    };
	

    var grid = function(element, options)
    {
        var elem = $(element);
        var objGrid = '';
        var AllData = [];
        var filtertpl = '<div class="k-button k-button-icontext Filter float-right">Filter&nbsp;&nbsp;<span class=" k-icon k-filter"></span></div>';
       var GriDdataSource ='';
		var ar=[];
		var fModel={};
		var id = 'id';
    	
		
		
		$(elem).find('tr').append($(".columns-"+elem.attr('id')).html());
		
		$(elem).find('th').each(function(index,value) {
  			if ($(value).data('field')) {
  				if ($("#template-"+$(value).data('field')+"-"+elem.attr('id')).html() != "") {
  					if ($(value).data('key') != null )  { id = $(value).data('field'); }
  					ar.push($(value).data());
  					fModel[$(value).data('field')] = { editable: $(value).data('editable'), type: $(value).data('type')  }
  				}
  			} else {
  				if ($(".id-commands-"+elem.attr('id')).html() != "" ) {
  					gridcommand='';
  					$.each($(".id-commands-"+elem.attr('id')),function(index,value){
  						gridcommand += $(value).html();
  					});
  					options.extra.template = gridcommand;
  					ar.push(options.extra);
  				}
  			}
  			
  		});
  		
  		

  		$(elem).html("");
  			
		ar.sort(function(a, b) { 
			x=a.order - b.order;
    		return  (x==0) ? 1 : a.order - b.order;
		});

  		fModel = (options.dataSource.model == null) ? { id: id, fields: fModel  } : options.dataSource.model;
  		ar = (options.columns == null ) ? ar : options.columns;
        GriDdataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: options.dataSource.url,
                dataType: "json",
                type: "POST",
                data: options.dataSource.data 
            }
        },
        schema: {
        	data: function(data) {
             return data;
            },
            model: fModel,
            total: function(datas) {
            	if (AllData.data) {
            		var total = AllData.data.total;     		
            	} else {
            		var total = 10;
            	}
            	return total;
         	},
         	parse: function(data) {
         		AllData=data;
         		
         		if (options.dataSource.returnData) {
         			mcms.callbackHandler(options.dataSource.returnData,AllData);
         		}
         		
         		if (data[options.dataSource.root]  == null  ) {
         			data[options.dataSource.root] = [];
         		}
         		total = ( AllData[options.dataSource.root].length > 0 ) ? AllData.data.total : 0;
         		var tot = objGrid.wrapper.find(".k-pager-wrap");
         		
         		if (tot.find('.float-right').length == 0) {
						tot.append('<div class="float-right">'+$.lang.total+' : <span>'+total+'</span></div>');
				} else {
					tot.find('.float-right span').html(total)
				}

         		return  data[options.dataSource.root];
         	}
        },
        filter: options.dataSource.filter,
        pageSize: 10,
        serverPaging: true,
        serverFiltering:  true,
        serverSorting: true,
        batch: true
    });


     var extra = $.extend({
     	extra: ''
        }, options || {});

        $.each(ar,function(index,value){
        	if ($("#template-"+value.field+"-"+elem.attr('id')).length>0) {          	
        		ar[index].template=kendo.template($("#template-"+value.field+"-"+elem.attr('id')).html());
        		obj1 = value;
				obj2 =$("#template-"+value.field+"-"+elem.attr('id')).data();
				value = $.extend(obj1, obj2); 
        	}
        });


        
        var griDsettings = $.extend({
            	dataSource: GriDdataSource,
            	pageable: true,
        		filterable: { extra: false },
        		scrollable: true,
        		dataBound: function(e) {
        			if (options.dataSource.receiveData) {
         				mcms.callbackHandler(options.dataSource.receiveData);
         			}
     			},
        		toolbar: [],
				columns: ar
        }, options.kendo || {});

        this.o = { //HOOKS CAN BY APPLIED HERE TO OVERCOME THE SCOPE
            init : function() {	
	         	
            },
            setup : function() {
	         
            }
        }//END OBJECT

	       
        this.init = function() {	   
        	
        	 if (options.hasFilter) {	
        	 	if (options.toolbar) {
  					griDsettings.toolbar.push({ id: elem.attr('id'), name: 'filter', template: options.toolbar });	 
        	 	} else {
        	 		griDsettings.toolbar.push({ id: elem.attr('id'), name: 'filter', template: filtertpl});
        	 	
        	 	}
  				CreateGrid();	
  			} else {
  				
  				if (options.toolbar) {
  					
  					griDsettings.toolbar.push({ id: elem.attr('id'), name: 'toolbar', template: options.toolbar });
  					
        	 	}

  				CreateGrid();		
  			}
        	
  			
  			
        };
        
        var CreateGrid = function()
        {
        	objGrid =  $(elem).kendoGrid(griDsettings).data('kendoGrid');
		};
        
        this.readgrid = function (url,data) {
        	if (data) {		
        		objGrid.dataSource.transport.options.read.data = data;
        	}
        	if (data == 'reset' ) {
        		objGrid.dataSource.transport.options.read.data = '';
        	}
        	objGrid.dataSource.transport.options.read.url = url;
        	objGrid.dataSource.read();
        };
        
        this.ReturnDataSource = function() {
        	return objGrid.dataSource;
        }
        
         this.removeRow = function (o) {
        	 if (Number(o)){
        		
        		 $.each(objGrid.dataSource.view(),function(index,value){
        			 if (value.id==Number(o)){
        				 o=$('tr[data-uid='+value.uid+']');		
        			 }
        		 });
  
        	 }
        	
        	count=objGrid.wrapper.find(".k-pager-wrap").find('.float-right span').html();
			count--;
		    objGrid.wrapper.find(".k-pager-wrap").find('.float-right span').html(count);
        	
         	var obj = objGrid.dataItem(o.closest("tr"));
        	var itemToRemove = objGrid.dataSource.get(obj.id);
        	
			objGrid.dataSource.remove(itemToRemove);
			
        	
			objGrid.refresh();
	
        };
        
		this.addRow = function (obj) {
			count=objGrid.wrapper.find(".k-pager-wrap").find('.float-right span').html();
			count++;
			objGrid.wrapper.find(".k-pager-wrap").find('.float-right span').html(count);
       		objGrid.dataSource.add(obj);
       		objGrid.refresh();
       		
        };
        
        this.ReturnGrid = function() {
        	return objGrid;
        }
        
        
        this.filter = function (obj) {
        	 objGrid.dataSource.filter(obj);
        }
        
        this.FetchDataSource = function () {
       		return AllData;
        }

    	
    	
    }
	  
})(jQuery);
