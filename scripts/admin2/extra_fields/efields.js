var sortable;
var grid ='';
var tpl='';
var dataExtra ='';
var existingGroups = kendo.template($("#existingGroups").html());
var treeviewExtra =''; 
var gridextra='';
var loadGroup = true;
var DeletekendoWindow='';
head.ready(function () {

	
    var db = new mcms.dataStore({
        method: "POST",
        url: "",
        dataType: "json",
        trigger: "loadComplete"
    });
    

    
   		url = '/ajax/loader.php?file=extra_fields/extra_fields.php&module='+$('input[name=current_module_name]').val()+'&action=show';


    	var griddatasource = { 
    			url: url,
    			root: "extrafields",
    			returnData: "loadGroups"
    		};
    		
  		var extraColumns = { 
  				filterable: false,
  				command: "",
  				width: '15%'
  		};
  		
  		
  		
		
    	grid = $("#gridview").kgrid({ toolbar: kendo.template($("#id-toolbar-gridview").html()) , dataSource: griddatasource, extra: extraColumns, kendo:{height: 300 } }).data('grid');
	  	grid.init();
  	
	  		
		$('#total_check').live('click', function(e){
			var checkbox = $(this);
			var el = grid.ReturnGrid();

			var table = el.wrapper.find('.messages');

			if (checkbox.prop('checked') == true)
			{
				el.tbody.find('.check-values').attr('checked',true);
				el.tbody.find('.check-values').parentsUntil('TR').parent().addClass('row_selected');
				table.html(el.tbody.find('.check-values:checked').length);

			}
			else 
			{
				el.tbody.find('.check-values').attr('checked',false);
				el.tbody.find('.check-values').parentsUntil('TR').parent().removeClass('row_selected');
				table.html(el.tbody.find('.check-values:checked').length);
			}
		});
		
		
		$('.check-values').live('click', function(e){
			var checkbox = $(this);
			var el = grid.ReturnGrid();
			var table = el.wrapper.find('.messages');
			var row = checkbox.parentsUntil('TR').parent();
			if (checkbox.prop('checked') == true)
			{
				table.html(el.tbody.find('.check-values:checked').length);
				row.addClass('row_selected');
			}
			else 
			{
				table.html(el.tbody.find('.check-values:checked').length);
				row.removeClass('row_selected');
			}	
		});
		
		
		
	  	
	  	$('#t-groups tbody.content').sortable({ update: SortGroup });
	  
	  	
	  	 $(db).bind('loadGroupid',function(){
	  	 	
	  	 	records = db.records;
	  	 	if (!records.group) {
	  	 	   	records.group = {id: "", title: "", description: ""};
	  	 	}
	  	 	    
	  	 		
	  	 	
	  	 	var validatable = $("#editGroup").kendoValidator().data("kendoValidator");

			if (!records.extrafields) {
				records.extrafields=dataExtra.extrafields;
			}
			
			var groupId = [];
			var itemsExtra= [];
      			if (records.group.items) {
      
	  	 	    	$.each(records.group.items,function(index,value) {
	  	 	    		
	  	 	    		var template=kendo.template("<img class='spacer-right dragHandle' height='16' width='16' src='/images/admin/move-arrow.png'><input type='hidden' class='groupExtraOrder groupData' name='items[]' value='#= id #'/>"+value.field+"<div data-id='#= id #' data-title='#= title #' class='spacer-left pointer k-icon k-delete deleteItem'></div>")
	  	 	    		itemsExtra.push({id: value.fieldid, text: template({ id: value.fieldid, title: value.field }), encoded: false });
	  	 	    		groupId.push(value.fieldid);
	  	 	    		
	  	 	    	});
      			}	  
      			
      			var itemsGroup = [];
      			$.each(records.extrafields,function(index,value) {
      				if ($.inArray(value.fieldid,groupId) == -1 ) {
      					itemsGroup.push(value);
      				}
      			});
      			
      			records.extrafields=itemsGroup;
      			
      			var GroupTpl = kendo.template( $("#newGroups").html() );
      			$("#editGroup").html(GroupTpl(records));
				$(".mSelect").chosen();
	  	 	    
				
	  	 	    treeviewExtra = $("#groupitems").kendoTreeView({
                    dragAndDrop: true,
                    drop: onDrop_f,
                    dragend: DragEnd,
                    dataSource: itemsExtra
                }).data("kendoTreeView");
            
                wnd.title("Group Title: " + records.group.title);
				wnd.center().open();

	  	 });
	  	
		var wnd = $("#editGroup").kendoWindow({
			title : "Order Details",
			modal : true,
			visible : false,
			resizable : true,
			width : 700
		}).data("kendoWindow").center();

	  	
	$('#CreateNewGroup').live('click', function(e){
		
			db.url = '/ajax/loader.php?file=extra_fields/extra_fields.php&module='+$('input[name=current_module_name]').val()+'&action=GroupNew';
      		db.trigger ='loadGroupid';
      		$(db).loadStore(db);
      		
	});
	
	$(".GroupEdit").live('click', function(e){
		
			db.url = '/ajax/loader.php?file=extra_fields/extra_fields.php&module='+$('input[name=current_module_name]').val()+'&action=GroupEdit&id='+$(this).data('id');
      		db.trigger ='loadGroupid';
      		$(db).loadStore(db);
			
	});
	
	DeletekendoWindow = $("#delete-confirmation").kendoWindow({
                title: $.lang.delete+" "+$.lang.group_extra_fields,
                resizable: false,
                visible: false,
                width:"180",
                modal: true
    }).data("kendoWindow");
    
    
    $(".GroupDelete").live('click', function(e){
    		
    		var id = $(this).data('id');
			var templates = kendo.template($("#delete-confirmation_menu").html());	    
			DeletekendoWindow.content(templates({ item: id }));
			DeletekendoWindow.center().open();
			
    }); 
	
	$(".delete-confirm,.delete-cancel").live('click',function() {
   	
   			if ($(this).hasClass("delete-confirm")) {
				db.url = '/ajax/loader.php?file=extra_fields/extra_fields.php&module='+$('input[name=current_module_name]').val()+'&action=Efields&mode=DeleteCheckedGroups';
				db.data = {
					data: { delete: $(this).data('id') } 
				}
      			db.trigger ='DeleteGroups';
      			$(db).loadStore(db);
      		}
                  
            DeletekendoWindow.close();
			
	});
	

	$(db).bind('DeleteGroups',function(){
		
		records=db.records;		
		
		for (var i=0; i<records.length; i++) {
			$('#t-groups > tbody.content').find('tr[data-id='+records[i]+']').remove();
		}
	
	});
	
	
	 function SortGroup() {
	  		data = [];
			
			$.each($(this).find("tr"),function(index,value) {
				data.push($(value).data('id'));
			});
			
    		db.url = '/ajax/loader.php?file=extra_fields/extra_fields.php&module='+$('input[name=current_module_name]').val()+'&action=SaveEfieldsGroupsOrder';
    		db.trigger='testing';
    		db.data = {
    			data: data
    		};
      		$(db).loadStore(db);
      		
	 }
	
	function onDrop_f(e) { 
		
	   	var drop_position=$(e)[0].dropPosition;
	   	if (drop_position=='over') {	
	             e.preventDefault();	
	   	}
			    	
	}
	
	function DragEnd() {
		
		var data = new mcms.formData($('.groupExtraOrder'),{'ReturnType':'array'});
    	db.url = '/ajax/loader.php?file=extra_fields/extra_fields.php&module='+$('input[name=current_module_name]').val()+'&action=SaveGroupExtraOrder&id='+$("#save_cat").data('id');
    	db.trigger='';
    	db.data = {
    		data: data['items']
    	}
      	$(db).loadStore(db);
		
	}
	  	

    $('.AddToItem').live('click',function(e){
    	
    	e.preventDefault();
    	
    	var li = $("#groupitems").find("li:last");
    	
    	var ar =[];
    	$.each($("#gridextrafields input[name='AddField[]']:checked:enabled"),function(index,value){
    		id = $(value).attr('id').split("_")[1];
    		var template=kendo.template("<img class='spacer-right dragHandle' height='16' width='16' src='/images/admin/move-arrow.png'><input type='hidden' class='groupExtraOrder groupData' name='items[]' value='#= id #'/>"+$(value).data('title')+"<div data-id='#= id #' data-title='#= title #' class='spacer-left pointer k-icon k-delete deleteItem'></div>")
    		obj = { id: id, text: template({ id: id, title: $(value).data('title')  }) , encoded: false };
    		ar.push(obj);	
    		$(value).parents("li").remove();
    	});
      
       	if (li.length != 0 ) {
       		treeviewExtra.insertAfter(ar, $(li));
       	} else {
       		ul=$("#groupitems").find("ul");
       		if (!ul.length != 0 ) {
       			$("#groupitems").append(ul);
       			ul=$("#groupitems").find("ul");
       		}
       		treeviewExtra.append(ar, $(ul));
       	}
        
    });
    
    
   $('.deleteItem').live('click',function(){

		$("#gridextrafields").append('<li><input type="checkbox" name="AddField[]"  data-title="'+ $(this).data('title') +'" id="check_"'+$(this).data('id')+'"/><b>#'+$(this).data('id')+'</b>  '+$(this).data('title')+'</li>');
   		treeviewExtra.remove($(this).closest('li'));
   		 grid.ReturnGrid().wrapper.find("th:first").find('input').attr('checked',false);
   });
   
   
   $(".FilterGroup").live('click',function(e){
   
   		e.preventDefault();
   		grid = $("#"+$(this).data('grid')).kgrid().data('grid');
   		
   		grid.filter([{ field: $(this).data('in') ,operator: "in", value: $(this).data('id') }]);	
   		if ($(".ResetFilter").length <= 0 ) {
			grid.ReturnGrid().wrapper.find('.k-grid-toolbar').prepend("<div class='float-left spacer-right ResetFilter'><a class='k-button k-button-icontext'  href='#'><span class='k-icon k-filter'></span>Show All</a></div>");
   		}
   });
   
   
   $(".ResetFilter").live("click",function(e) {
   				e.preventDefault();
   				grid.ReturnGrid().dataSource.transport.options.read.data={};
    			grid.filter({ value: '' });
    			$(this).remove();
   });
       
     $(db).bind('GroupUpdate',function(){
     	  		records = db.records;  	

    				$.each($('#t-groups tbody.content').find('tr=[data-id='+records.id+']').children() ,function(index,value) {
    					if ($(value).data('field')) {
    						var str = (records[$(value).data('field')] != null ) ? records[$(value).data('field')] : '';
    						if ($(value).data('field') != 'title') {
    							$(value).html(str);
    						} else {
    							$(value).find('a').html(str);
    						}
    					}
    				});

    			wnd.close();
     });
     
            
     $(db).bind('GroupInsert',function(){
     	  		records = db.records;  			
     	  		tpl = existingGroups(records);
				$('#t-groups').find('tbody.content').append(tpl);
				 $("#t-groups tr:even").addClass("k-alt");
    			wnd.close();
     });
   
   
    $('.SaveGroupExtra').live('click',function(){
    
    	
    	var data = new mcms.formData($('.groupData'),{'ReturnType':'array'});
    	db.url = '/ajax/loader.php?file=extra_fields/extra_fields.php&module='+$('input[name=current_module_name]').val()+'&action=Efields&mode=SaveEfieldGroup&groupid='+$(this).data('id');
    	if ($(this).data('id')) {
    		db.trigger='GroupUpdate';
    	} else {
    		db.trigger='GroupInsert';
    	}
    	db.data = {
    		data: data    		
    	}
    	if ($("#editGroup").kendoValidator().data("kendoValidator").validate()) {
			$(db).loadStore(db);
    	}
    });
    

    
    


});

    function loadGroups(records) {
			
    	if (loadGroup) {
	 		dataExtra = records;
	 		$.each(records.groups,function(index,value) {
	 			tpl += existingGroups(value);
	 		});
	 		loadGroup = false;
			$('#t-groups').find('tbody.content').append(tpl);
			$('#t-groups tbody.content').sortable({handle:'td.dragHandle' });
			$("#t-groups tr:even").addClass("k-alt");
		
		   
    	}
  	}
  	
  	
