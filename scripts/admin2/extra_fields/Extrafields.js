var grid;
head.ready(function () {
	

    var dbE = new mcms.dataStore({
        method: "POST",
        url: "",
        dataType: "json",
        trigger: "loadComplete"
    });
    

    
 grid = $("#gridview").kgrid().data('grid');  
	  
	  	

	  	
	var wndE = $("#editExtra").kendoWindow({
		title : "",
		modal : true,
		visible : false,
		resizable : true,
		width : 1000
	}).data("kendoWindow").center();

	  	
	$('#CreateNewExtra').live('click', function(e){
		
			dbE.url = '/ajax/loader.php?boxID=31&mode=template&module='+$('input[name=current_module_name]').val()+'&action=ExtraFields';
      		dbE.trigger ='createExtraField';
      		$(dbE).loadStore(dbE);
      		
	});
	
	$(dbE).bind('createExtraField',function(){
			
			records = dbE.records;
		   $("#editExtra").html(records.template);
		    var validatable = $("#GroupForm").kendoValidator().data("kendoValidator");
	  	 	$("#tabStrip").kendoTabStrip().css('border','none');
	  	 	$(".mSelect").chosen();
	  	 	
	  	 	$('#dataTable tbody.extrafieldcontent').sortable({handle:'td.dragHandle' });
        	
			wndE.title("Extra Field: ");
	  	 	wndE.center().open();
		
	});
	
	$(".EditExtra").live('click', function(e){
		
			dbE.url = '/ajax/loader.php?boxID=31&mode=template&module='+$('input[name=current_module_name]').val()+'&action=ExtraFields&fieldid='+$(this).data('id');
      		dbE.trigger ='createExtraField';
      		$(dbE).loadStore(dbE);
			
	});
	
		
	$(".ToolBarActions").live('click', function(e){
			e.preventDefault();
			var BatchAction = new Array;
			el =grid.ReturnGrid();
		
			el.tbody.find('.check-values:checked').each(function(k,v){
				BatchAction.push($(this).val());
				
			});
			
			if (BatchAction.length >= 1 ) {
				
				dbE.url = '/ajax/loader.php?file=extra_fields/extra_fields.php&module='+$('input[name=current_module_name]').val()+'&action=Efields&mode='+$(this).data('action');
				dbE.trigger = $(this).data('trigger');
      			dbE.data = {
      				data: { checkboxes: BatchAction }
      			}
      			$(dbE).loadStore(dbE);	
      			
			}
			
	});
	
	$(".SearchText").live('keyup',function() {
		
    	grid = $("#"+$(this).data('grid')).kgrid().data('grid'); 
    	
    	if (! isNaN ($(this).val()-0)) {
    		if ($(this).val() != "") {
    			grid.filter([{ field: $(this).data('eq') ,operator: "eq", value: $(this).val() }]);	
    		} else {
    			grid.ReturnGrid().dataSource.transport.options.read.data={};
    			grid.filter({ value: '' });
    			$(".ResetFilter").remove();
    		}
    	} else {
    		grid.filter([{field: $(this).data('contains') ,operator: "contains", value: $(this).val()}]);	
    	}	
	});
	
	
	$(dbE).bind('disabled',function(){
		
		records=dbE.records;	
		griditem = grid.ReturnGrid();
		var ar = [];
		$.each(records,function(index,value){
				 item=griditem.dataSource.get(value);
				 item.active="N";
				 item.checked="checked";
				 ar.push(item.uid);
		});	
		
		grid.ReturnGrid().refresh();
		
		$.each(ar,function(index,value) {
			$('tr[data-uid='+value+']').addClass('row_selected');
		});
		
	});
	
		
	$(dbE).bind('activated',function(){
		
		records=dbE.records;	
		griditem = grid.ReturnGrid();
		var ar = [];
		$.each(records,function(index,value){
				 item=griditem.dataSource.get(value);
				 item.active="Y";
				  item.checked="checked";
				 ar.push(item.uid);
		});	
		
		grid.ReturnGrid().refresh();
		
		$.each(ar,function(index,value) {
			$('tr[data-uid='+value+']').addClass('row_selected');
		});
		
	});
	
	$(dbE).bind('Deletechecked',function(){
		
		records=dbE.records;	
		griditem = grid.ReturnGrid();
		
		$.each(records,function(index,value){
				 itemToRemove=griditem.dataSource.get(value);
            	 grid.ReturnDataSource().remove(itemToRemove);
		});	
		
		grid.ReturnDataSource().read();
		
	});
	
	

	
	$("#SaveEfield").live('click', function(e){
		
			var data = new mcms.formData($('.ExtraData'),{'ReturnType':'array'});
			if ($(this).data('id') ) {
				dbE.url = '/ajax/loader.php?file=extra_fields/extra_fields.php&module='+$('input[name=current_module_name]').val()+'&action=Efields&mode=SaveEfield&fieldid='+$(this).data('id');
      			dbE.trigger ='SaveExtraId';
      			dbE.data = {
      				data: data
      			}
			} else {
				dbE.url = '/ajax/loader.php?file=extra_fields/extra_fields.php&module='+$('input[name=current_module_name]').val()+'&action=Efields&mode=SaveEfield';
      			dbE.trigger ='SaveNewExtraId';
      			dbE.data = {
      				data: data
      			}
			}
			
			if ($("#GroupForm").kendoValidator().data("kendoValidator").validate()) {
      			$(dbE).loadStore(dbE);
			}
      		
	});
	
	$(dbE).bind('SaveExtraId',function(){
		
				records = dbE.records;
				grid = $("#gridview").kgrid().data('grid');  
				var DataSource = grid.ReturnDataSource();
				
				var field =DataSource.get(records.fieldid);

				$.each(records,function(index,value) {
					field[index] =value
				});
				
				grid.ReturnGrid().refresh();
				wndE.close();
				
	});
	
	$(dbE).bind('SaveNewExtraId',function(){
				records = dbE.records;
				grid = $("#gridview").kgrid().data('grid');
				var DataSource = grid.ReturnDataSource();
				grid.addRow(records);
				wndE.close();
	});
	

    $(".SlideDown").live('click',function() {
    	$("#categories_list").removeClass('hidden');
    });
    
    
    $(".SlideUp").live('click',function() {	
    	$("#categories_list").addClass('hidden');
    });
    
    $("select#type").live('change', function(e){
    
        if($(this).val()=="multiselect")
        {
            $("div.multioptions").removeClass("hidden");
        }
        else
        {
            $("div.multioptions").addClass("hidden");
            $("#group").addClass("hidden");
            $("#range").addClass("hidden");
        }
    });
    
    $("select#data_type").live('change', function(e){
        
        if($(this).val()=="dropdown")
        {
            //alert("inside drop");
            $("#range").removeClass("hidden");
        }
        else
        {
            $("#range").addClass("hidden");
        }
    });
    
    
    $("#addRow").live("click",function(e){
    	
    	e.preventDefault();
    	var rows = parseInt($("#icounter").val());

       $("#dataTable tbody.extrafieldcontent").append('<tr class="dataRow" id="row'+rows+'">' +
       																				'<td class="dragHandle"><img src="/images/admin/move-arrow.png" width="16" height="16" class="handle"  /></td>' +
       																				'<td><input type="text" name="efieldKey_'+rows+'" class="ExtraData inpt_a"></td>' +
       																				'<td><input type="text" name="efieldVal_'+rows+'" class="ExtraData inpt_a"></td>' +
       																				'<td><a href="\#" class="removeRow" rel="row'+rows+'">Remove</a></td></tr>');
		rows++;
		$("#icounter").val(rows);
		
    });
    
    
    
    $(".removeRow").live("click",function(e){
    	
        e.preventDefault();
        var row = $("#"+$(this).attr('rel'));
        row.remove();
        
    });
    
    
});

  	
  	
