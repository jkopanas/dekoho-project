head.ready(function(){
	 var db = new mcms.dataStore({
         method: "POST",
         url:"",
         //dataType: "json",
         trigger:"loadComplete"
     });
	 				
	   $(db).bind('loadTabs',function(){
	 
		   records = db.records;
		   records = $.parseJSON(records);
		  $.each(records,function(index,value) {  		
				$('#'+value.box.name).html(value.template);	  	
		  });
	 });

	 var ar =[];
	 $.each($(".LoadBox"),function(index,value) {
	 	ar.push("boxID[]="+$(value).data('id'));
	 });
	 

	 db.url = "/ajax/loader.php?"+ar.join("&");
	 db.trigger = 'loadTabs';

	 $(db).loadStore(db);
	 	
	    var wnd = $("#edit_form");
	    var wndf = $("#FilterForm-ShowUsers");
	    
	    
	    $('.k-filter').live('click', function(e) {
   			if (!wndf.data("kendoWindow")) {
            	wndf.show().kendoWindow({
            	        width: $("#FilterForm-ShowUsers").width()+' ',
            	        height: $("#FilterForm-ShowUsers").height()+' ',
           		        title : "Αναζήτηση Χρηστών",
	                    modal : true
    	        });
        	    wndf.data("kendoWindow").center();
    		} else {            
            	wndf.data("kendoWindow").open();
           		wndf.data("kendoWindow").center();   
    		}
    		
      });
	    
$(".k-edit_hoteluser").live( "click", function() {
        	
	    	action = ($(this).attr('id') == undefined ) ? "&action=insert_new_user" : "&action=edit_user";
	    	id=($(this).attr('id') == undefined ) ? "" : "&id=" + $(this).attr('id');
                if (!wnd.data("kendoWindow")) {
                        wnd.kendoWindow({
                                width: "610",
                                height: "420",
                                title : "Προσθήκη / Επεξεργασία  Χρήστη",
                                content : "/ajax/loader.php?file=usersHotel.php"+action+id,
                                visible : true,
                                modal : true
                        });
                        wnd.data("kendoWindow").open();
                        wnd.data("kendoWindow").center();
                } else {
                        wnd.data("kendoWindow").refresh({
                               url : "/ajax/loader.php?file=usersHotel.php"+action+id
                        });
                        wnd.data("kendoWindow").open();
                        wnd.data("kendoWindow").center();
                        
                }
        });  

	    
	    
});// end head

