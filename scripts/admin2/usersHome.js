head.js("../scripts/admin2/lib/datatables/jquery.dataTables.min.js");
head.js("../scripts/admin2/lib/datatables/dataTables.plugins.js");					
head.js("../scripts/admin2/lib/fancybox/jquery.easing-1.3.pack.js");
head.js("../scripts/admin2/lib/gritter/jquery.gritter.min.js");
head.js("../scripts/admin2/lib/fancybox/jquery.fancybox-1.3.4.pack.js");

	
head.ready(function(){
    $(".tabsS").flowtabs("div.tabs_content > div");

    $("#filterReuslts").fancybox({
        'transitionIn'	: 'fade',
        'scrolling'		: 'no',
        'titleShow'		: false,
        'overlayOpacity'	: '0',
        'hideOnOverlayClick': false,
        'onClosed'		: function() {
            $("#login_error").hide();
        }

    });
			
    $('.quickEdit').live('click', function(e){
        e.preventDefault();
        var me = $(this);
        $.fancybox({
            'transitionIn'	: 'fade',
            'scrolling'		: 'no',
            'titleShow'		: false,
            'overlayOpacity'	: '0',
            'hideOnOverlayClick': false,
            'type'				:'ajax',
            'href'				: me.attr('href')
        });
				 
    });
			 


    $('.chSel_all').live('click', function() {
        $(this).closest('table').find('input.check').attr('checked', this.checked);
    });
	
	
    $.fn.dataTableExt.oApi.fnMultiFilter = function( oSettings, oData ) {
        var extraFilters = {};
        var tmp = new Array;
        for ( var key in oData )
        {
		
            if ( oData.hasOwnProperty(key) )
            {
                for ( var i=0, iLen=oSettings.aoColumns.length ; i<iLen ; i++ )
                {
				
                    if( oSettings.aoColumns[i].mDataProp == key )
                    {
                        /* Add single column filter */
                        oSettings.aoPreSearchCols[ i ].sSearch = oData[key];
                        oSettings.aoPreSearchCols[ i ].k = key;
                        break;
                    }
                    else {
                        extraFilters[key] = oData[key]  ;
                    }
                }
            }
        }

        $.each(extraFilters, function(index, value) { 
            if (value)
            {
                tmp.push(index + ':::' + value);
                oData[index] = value;
            }
        });

        oSettings.aoPreSearchCols[0]['sSearch'] = tmp.join('###');

        this.oApi._fnDraw( oSettings );
    }
	
    var oTable = $('#data_table').dataTable({
        "bFilter": true,
        "bStateSave": false,
        "bProcessing": true,
        "bServerSide": true,
        "bAutoWidth": false,
        "sAjaxSource": "../ajax/ajaxRequests.php?&action=loadUsers",
        "aoColumns": [
        {
            "mDataProp": "checkID"
        },
{
            "mDataProp": "id"
        },
{
            "mDataProp": "uname"
        },
{
            "mDataProp": "user_join"
        },
{
            "mDataProp": "email"
        },
{
            "mDataProp": "user_login"
        },
{
            "mDataProp": "user_class"
        }
	
        ],

        "aoColumnDefs": [ 
	
        {
            "fnRender": function ( oObj ) {
                oObj.aData.origID = oObj.aData.id;
                return '<input type="checkbox" value="'+oObj.aData.id+'" class="check" name="id" />';
            },
            "bSortable": false,
            "sWidth": "1%",
            "aTargets": [ 0 ]
        },
        {

            "bSortable": true,
            "sWidth": "1%",
            "aTargets": [1 ]
        },	
        {
            "fnRender": function ( oObj ) {
                $(this).parent().addClass('test');
                var ret = '<strong><a href="#" class="objTitle" rel="' + oObj.aData.origID +'">'+ oObj.aData.uname + '</a></strong>';
                ret += '<br>';
                ret += '<div class="row-actions hidden"><span class="edit"><a href="editItem.php?id='+oObj.aData.origID+'&module='+$('input[name=current_module_name]').val()+'" title="Edit this item">Edit</a> | </span><span class="inline hide-if-no-js"><a href="quickEditItem.php?id='+oObj.aData.origID+'&module='+$('input[name=current_module_name]').val()+'" class="quickEdit" title="Edit this item inline">Quick&nbsp;Edit</a> | </span><span class="trash"><a class="submitdelete" title="Move this item to the Trash" href="http://testing.home.local/wordpress/wp-admin/post.php?post=1&amp;action=trash&amp;_wpnonce=d30c721212">Trash</a> | </span><span class="view"><a href="http://testing.home.local/wordpress/?p=1" title="View “Hello world!”" rel="permalink">View</a></span></div>';
                return (ret) ? ret : '';
            },
            "sWidth": "80%",
            "aTargets": [ 2]
        },
        {
            "fnRender": function ( oObj ) {
                var a = parseInt(oObj.aData.user_join);
                var date = new Date(a*1000);
                var formattedTime = date.getDate() + '/' + (parseInt(date.getMonth()) + 1) + '/' + date.getFullYear() + ' @ ' + date.getHours() + ':' + date.getMinutes();
                return  (formattedTime) ? formattedTime : '';
            },
            "bSortable": true,
            "sWidth": "12%",
            "aTargets": [ 3 ]
        },
        {

            "bSortable": false,
            "aTargets": [ 4 ]
        },
        {

            "bSortable": false,
            "sWidth": "1%",
            "aTargets": [ 5 ]
        },	
        {

            "bSortable": false,
            "sWidth": "1%",
            "aTargets": [ 6 ]
        },	
        {
            "fnRender": function ( oObj ) {
                switch (oObj.aData.user_login)
                {
                    case '1':
                        return '<span class="fr notification ok_bg">'+$.lang.yes+'</span>';
                        break;
                    case '0':
                        return '<span class="fr notification error_bg">' +$.lang.no +'</span>';
                        break;
                }
            //return (oObj.aData.active == 1) ? '<span class="fr notification ok_bg">'+$.lang.yes+'</span>' : '<span class="fr notification error_bg">' +$.lang.no +'</span>' ;
            },
            "bSortable": false,
            "sWidth": "2%",
            "aTargets": [ 5 ]
        },
        {
            "sClass": "center", 
            "aTargets": [ 5,6 ]
        },
{
            "sClass": "titleCol", 
            "aTargets": [ 2 ]
        }
        ]
    });
	
    $(".titleCol").live({
        mouseenter: function() { 
            $(this).find('.row-actions').removeClass('hidden');
        },
        mouseleave: function () {
            $(this).find('.row-actions').addClass('hidden');
        }
    });
	
	


    $('#searchTable').click( function(e) { 
        //oTable.aoData.push({ "name": "dateStart", "value": 'sdsd' });
        e.preventDefault();
        var a = {  };
        var b;
        var dates = {};
        $(this).closest('form').find('.filter').each(function(index) {
	
            b = $(this).attr('name').split('filter-');

            if (b[1] == 'date_from' && $(this).val() && $(this).val() !='undefined')
            {
                dates.from = 'from#@#'+$(this).val();
            }
            if (b[1] == 'date_to' && $(this).val() && $(this).val() !='undefined')
            {
                dates.to = 'to#@#'+$(this).val();
            }
            a[b[1]] = $(this).val();
        });

        if (dates.from && dates.to)
        {
            a.date_added = dates.join('###');
        }
        else { 
            a.date_added = (dates.from) ? dates.from : dates.to;
        }

        oTable.fnMultiFilter( a );
    } );


});//END JQ