head.ready(function(){
	
	 var db = new mcms.dataStore({
         method: "POST",
         url:"",
         //dataType: "json",
         trigger:"loadComplete"
     });
	 				
	 $(".OverTitle").live('mouseover',function(){
	 	$('.toolbar').hide();
	 	$(this).find('.toolbar').show();
	 });
	 

	 $(db).bind('loadDash',function(){
		   records = db.records;
		   records = $.parseJSON(records);
			if (records) {
				$('#'+records.name).find('.box_c_content').prepend(records.template);		
			}
			
	 });

	$('.wToogle').live('click', function(e){
		$(this).parents('.box_c').find('.box_c_content > div').toggle('fast');
	});
	
	$('.wRemove').live('click', function(e){
		$(this).parents('.box_c').remove();
	});

	options = {
			connectWith: ".column",
			handle: ".box_c_heading",
			tolerance: "pointer",
			opacity: 0.6,
			helper: 'clone',
			revert: true
	}
	
//	options = {};
	
	$(".column").sortable(options);
	
	$( ".box_c" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".box_c_heading" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon '></span>")
				.end()
			.find( ".box_c_content" );

		$( ".box_c_heading .ui-icon" ).click(function() {
			$( this ).parents( ".box_c:first" ).find( ".box_c_content" ).toggle();
		});

		$( ".column" ).disableSelection();
	
	
	
	 $.each($(".box_c"),function(index,value) {
	 		db.url = "/ajax/loader.php?boxID="+$(value).data('id');
	    	db.trigger = 'loadDash';
	    	$(db).loadStore(db);
	 });
	        	    
	$('.ModuleFilter').live('click',function() {
		
			$('.ModuleFilter').removeClass('k-state-selected');
			grid = $("#gridview").kgrid().data('grid');
			grid.readgrid('/ajax/loader.php?file=itemsSearch.php&module='+$(this).data('id')+'&action=pagesFilter',false);
			$(this).addClass('k-state-selected');

	});
	
		$(".SearchText").live('keyup',function() {
		
    	grid = $("#"+$(this).data('grid')).kgrid().data('grid'); 
    	
    	if (! isNaN ($(this).val()-0)) {
    		if ($(this).val() != "") {
    			grid.filter([{ field: $(this).data('eq') ,operator: "eq", value: $(this).val() }]);	
    		} else {
    			grid.ReturnGrid().dataSource.transport.options.read.data={};
    			grid.filter({ value: '' });
    		}
    	} else {
    		grid.filter([{field: $(this).data('contains') ,operator: "contains", value: $(this).val()}]);	
    	}	
	});


});// end head
