head.ready(function () {
   
	
    dbLanguageBox = new mcms.dataStore({
        method: "POST",
        url: "",
        dataType: "json",
        trigger: "loadLangBox"
    });
	
	
	$(".saveSingleTranslation").live('click',function(index,value) {
		
		data =  new mcms.formData($('.SaveTranslate'),{'getData':'true'});
		var table = $(this).data('table');

		$.each(data,function(index,value) {
			data[index]={ field: value.field, code: value.code, value: value.value, table: table }
		})

		dbLanguageBox.data = {
			data: data
		};
	
		dbLanguageBox.trigger='returnlang';
		dbLanguageBox.url = "/ajax/loader.php?file=Languages.php&action=saveContentTranslations&id="+$(this).data('id');
		$(dbLanguageBox).loadStore(dbLanguageBox);

	});


});
