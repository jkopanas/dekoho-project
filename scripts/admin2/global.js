head.ready(function(){
	var editorOptions = {	toolbar : [
            [ 'Format','Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Image','Link','SpecialChar', '-', 'Undo','Redo','-','PasteText','PasteFromWord','-','Preview','Source','Maximize','-','Table','Find','Replace','-','SelectAll','RemoveFormat','CMSImages']
        ],
        
language : mcms.currentLang = (mcms.mcmsSettings.currentLang == 'gr') ? 'el' : mcms.currentLang, ProcessHTMLEntities:false, height:300};

$('.ckEditor').live('click', function(e){
	e.preventDefault();
	if ($(this).attr('rel') in mcms.editors) {
		mcms.editors[$(this).attr('rel')].destroy();
		delete(mcms.editors[$(this).attr('rel')]);
	} else {
		var a = mcms.callInternalMethod('listAttributes',$(this))
	mcms.editors[$(this).attr('rel')] = $('#'+$(this).attr('rel')).ckeditor($.extend(editorOptions,mcms.callInternalMethod('listAttributes',$(this),{prefix:'data-',returnType:'keyVal',stripPrefix:true}))).ckeditorGet();
	
	}
});
$("#CPMenu").kendoMenu();

	    var wnd = $("#edit_form");
        $(".k-edit_user").live( "click", function() {
        	
	    	action= ($(this).attr('id') == undefined ) ? "&action=insert_new_user" : "&action=edit_user";
	    	id=($(this).attr('id') == undefined ) ? "" : "&id=" + $(this).attr('id');
                if (!wnd.data("kendoWindow")) {
                        wnd.kendoWindow({
                                width: "610",
                                height: "420",
                                title : "Προσθήκη / Επεξεργασία  Χρήστη",
                                content : "/ajax/loader.php?file=user_functions.php"+action+id,
                                visible : true,
                                modal : true
                        });
                        wnd.data("kendoWindow").open();
                        wnd.data("kendoWindow").center();
                } else {
                        wnd.data("kendoWindow").refresh({
                               url : "/ajax/loader.php?file=user_functions.php"+action+id
                        });
                        wnd.data("kendoWindow").open();
                        wnd.data("kendoWindow").center();
                        
                }
        });  


$("#mainMenu").kendoMenu();

$('.msg_close').live('click', function(e) {
	$(this).parent().hide('slow');
});


var s = {
    data : function(data) {
    	allData = data;
    	if (data.results.length == 0) {
    		return [];
    	}
		return data.results;
	}
};
var mod = $('input[name=searchModule]:checked').val();
var ds = {serverFiltering: true,
serverPaging: true,schema:s,pageSize:10,transport: {cache: "inmemory",read : {dataType:'json',url :"/ajax/loader.php?file=itemsSearch.php&module=" + mod + "&action=pagesFilter",type:'post',data:{}}}};

var onSelect = function(e) {
	if (e.item != null) {
		 var dataItem = this.dataItem(e.item.index());
   		 window.location.href = $(e.item).find('.foundItem').attr('href');
	} 
};


var auto = $("#quickSearch").kendoAutoComplete({
select:onSelect,
dataSource: ds,
dataTextField:'q',
filter: "contains",
//minLength:3,
template:  kendo.template($('#autoCompleteTemplate').html())
}).data("kendoAutoComplete");

$('.moreBtn').live('click', function(e){
$(this).parents('div').find('.options').show();
});

$('input[name=searchModule]').live('click', function(e){
auto.dataSource.options.transport.read.url = "/ajax/loader.php?file=itemsSearch.php&module=" + $('input[name=searchModule]:checked').val() + "&action=pagesFilter";
});

$('body').click(function(e) {
	if ($(e.target).parents('.options').length != 1){
		 $('.quickSearchContainer .options').hide();
	}
});

var checkUpdater = new mcms.dataStore({
     method: "POST",
     async:true,
     url: '/ajax/loader.php?file=updater/versionCheck.php&action=versionCheck',
     dataType: "json",
     trigger:"checkComplete"
 });
 
 $(checkUpdater).bind('checkComplete',function(){
 	if (checkUpdater.records.status == true) {
 		//SET MESSAGES
 		if (parseInt(checkUpdater.records.updater.total) > 0) {
 			$('#systemMessages .updatesNo').html(checkUpdater.records.updater.total);
 			$('#systemMessages').show();
 		}
 	}
 	else {
 		checkUpdater.url = mcms.system.version_check_url+'&version='+mcms.system.version_number;
 		checkUpdater.dataType = 'jsonp';
 		checkUpdater.type = 'POST';
 		checkUpdater.trigger = 'versionSet';
 		
 		$(checkUpdater).loadStore(checkUpdater);
 	}
 });
 
  $(checkUpdater).bind('versionSet',function(){
		checkUpdater.data = checkUpdater.records;
		checkUpdater.url = '/ajax/loader.php?file=updater/versionCheck.php&action=versionCheck';
		checkUpdater.dataType = 'json';
		checkUpdater.trigger = 'checkComplete';
		$(checkUpdater).loadStore(checkUpdater);
  });
 
 $(checkUpdater).loadStore(checkUpdater);
});//END HEAD

function saved(msg,className) {
	var k = (msg) ? msg : $.lang.saved;
	var v = (className) ? className : 'msg_ok';
	$('.debug').html('<div class="msg_box '+v+'">'+ k +'<img src="/images/fancybox/blank.gif" class="msg_close" alt=""></div>').show();
	setTimeout(function() {$('.debug').html('').hide()}, 4000); // Autosaves every 20 sec.
}