$(document).ready(function() {
	
	 var db = new mcms.dataStore({
         method: "POST",
         url:"",
         //dataType: "json",
         trigger:"loadComplete"
     });
	 
	 var wnd = $("#edit_form");
	 var ret = true;
	 var ret_user = true;
	 var user_mail = true;
	 var  button_id =$('.add_edit_user').attr('id');
	 var user_id= (button_id == 'saveUserDetails' ) ? "&user_id="+$("#cur_userid").val() : "";
	 
  var validatable = $("#user_validate").kendoValidator({
	 rules: {
        verifyPasswords: function(input){
           
              if (input.is("[name=confirm_password]")) {
                   ret = input.val() === $("#pass").val();
                }
                 return ret;
        },	
		checkusername: function(input){
			
            if (input.is("[name=uname]")) {
            	var username = $("#uname").val();
            	db.url="/ajax/loader.php?file=user_functions.php&action=checkUser"+user_id;
    	 		db.trigger ='check_username';
    	 	    db.data = {uname:username} 
    	 	    $(db).loadStore(db);
    	 	    
            }
            return ret_user;
		},
		checkemail: function(input){

            if (input.is("[name=email]")) {
            	var email = $("#email").val();
            	db.url="/ajax/loader.php?file=user_functions.php&action=checkEmail"+user_id;
    	 		db.trigger ='check_mail';
    	 	    db.data = {email:email} 
    	 	    $(db).loadStore(db);
    	 	   
            }
            return user_mail;
		}
		
    },
    messages: {
    	checkemail: function(input){
    		if (input.is("[name=email]")) {
    		  return "Tο e-mail χρησιμοποιείται ήδη";
    		} 
    	},
    	checkusername: function(input){
    		if (input.is("[name=uname]")) {
    		  return "Tο όνομα χρήστη χρησιμοποιείται ήδη";
    		} 
    	},
    	required: function(input){
    		return "Το πεδίο "+input.attr("for")+" είναι υποχρεωτικό";
    	}
    }
}).data("kendoValidator");


$(".add_edit_user").click(function() {
	
       if (validatable.validate()) {
    	  if (button_id != 'saveUserDetails') {  
    	    var total_users=parseInt($("#total_users").val());
  			$("#tot_users").html(parseInt(total_users+1));
			$("#total_users").val(parseInt(total_users+1));
    	  }	
		    action= (button_id == 'saveUserDetails' ) ? "&action=saveUserDetails" : "&action=new_user_entry";
	 	  	db.url="/ajax/loader.php?file=usersFront.php"+action;
	 		db.trigger ='add_edit_user';
	 	    db.data = {data:mcms.formData('.userData',{ getData:false})} 
	    	$(db).loadStore(db); 
       } 
    });

$(db).bind('add_edit_user',function(){
	records = db.records;
	records = $.parseJSON(records);
	 wnd.data("kendoWindow").close(); 

	   grid = $("#gridview").kgrid().data('grid');
	   
	  if (button_id != 'saveUserDetails') {    
		  grid.addRow(records.results);
	  } else {	
	  
		  var DataSource = grid.ReturnDataSource();
       
          var field =DataSource.get(records.id);
          $.each(records,function(index,value) {
               field[index] =value
       	  });
       	  grid.ReturnGrid().refresh();	
	  }	
			
	
});


$(db).bind('check_username',function(){
	records = db.records;
	
	 if (records == 'false') {
		 ret_user=false;
	} else { 
		ret_user=true;
	  }
	 
});

$(db).bind('check_mail',function(){
	records = db.records;
	
	 if (records == 'false') {
		 user_mail=false;
	} else { 
		user_mail=true;
	  }	 
	 
});

/*
function sleep(milliseconds) {
	  var start = new Date().getTime();
	  for (var i = 0; i < 1e7; i++) {
	    if ((new Date().getTime() - start) > milliseconds){
	      break;
	    }
	  }
	}
*/
 });// end document ready

