head.ready(function(){
	
	 var filterdb = new mcms.dataStore({
         method: "POST",
         url:"",
         //dataType: "json",
         trigger:"loadComplete"
     });
	
     var args =[];
     
     
 	$(".grid_posted").each(function(index,value) {
     	if ( $(value).data('grid') == "gridview_cat" || $(value).data('grid') == null  ) {
     		var str = $(value).attr('name') + "=" + $(value).val();		
     		args.push(str);  
     	} 
     });
     
         
		var str = "&"+args.join('&');
		var url = "/ajax/loader.php?file=items/itemsCategories.php&module="+$('input[name=current_module_name]').val()+str;

 		
    	var griddatasource = { url: url , root: "results" };

  		
  		var extraColumns = { 
  				filterable: false,
  				command: "",
  				title: "",
  				width: '10%'
  		};
  		
		var grid ='';
    	grid = $("#gridview_cat").kgrid({dataSource: griddatasource, toolbar: $("#template-toolbar-gridview_cat").html() , extra: extraColumns}).data('grid');
		
	  	grid.init();
    
});
	