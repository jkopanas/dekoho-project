(function ($, undefined) {
$.itemsRelated = function(obj,options) {
    var obj = obj, options = options,templates = {},cache={},app={};
    if (typeof(obj) !== 'object') obj = $(obj)[0];
    if (typeof(options) !== 'object') options = { };

    var delay = (function(){
      var timer = 0;
      return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
      };
    })();

    var defaults = {
        templates : '#templates',
        messages : {
            errorFileType : 'File type not allowed'
        },
        sortableDefualts:{},
        settings:{},
        results_per_page:50,
        modules:['pages','categories'],
        areas:{
            filters:'.filters',
            pages:'.pages',
            categories: '.categories',
            sortable: '.sortable',
            saveBtn: '.save',
            filterField:'.filter'
        },
        sortItems:true,
        module:'',
        itemid:'',
        moduleSource:'content',
        pages:[],
        categories:[],
        postData:{},
        type:'normal',
        dbDefaults : {
            method: "POST",
            dataType: "json",
            trigger:"loadComplete",
            async:true
        },
        dbURL : {
            read: '/ajax/loader.php?file=items/itemsQuickSearch.php&action=fetchData',
            filter:'/ajax/loader.php?file=itemsSearch.php&action=pagesFilter',
            order: '/ajax/loader.php?file=items/itemsQuickSearch.php&action=orderRelated',
            add:'/ajax/loader.php?file=items/itemsQuickSearch.php&action=addRelated',
            remove:'/ajax/loader.php?file=items/itemsQuickSearch.php&action=deleteRelated'
        }   
    };
    
    var options = $.extend(defaults,options);
    options.module = options.moduleSource;//Default


    var dbDefaults = {    
        method: "POST",
        dataType: "json",
        trigger:"loadComplete",
        async:true
    };
    
    var appDB = new mcms.dataStore(dbDefaults);

    /* read templates */
    if (typeof templates.main == 'undefined') {

        var o = $(options.templates).find('script');
        var p = o.length;

        for (p;p--;) {
            templates[$(o[p]).attr('id')] = kendo.template($(o[p]).html());
        }
    }//END TEMPLATES


    var app = {
        itemsFlat:{},
        existingItems:{},
        target:obj,
        templates:templates,
        data:{},
        options:options,
        _setupCache: function(word){
            if (typeof cache[options.module] == 'undefined') { cache[options.module] = {};}
           // if (typeof cache[options.module][word] == 'undefined') {cache[options.module][word] = {};}
        },
        _callInternal: function(type,data){
            data = data || {};
            if ($.isFunction(this[type])){
                eval(this[type]).apply(this, Array.prototype.slice.call(arguments, 1));
            }
        },
        _trigger: function( type, data ) {
            data = data || {};
            if ($.isFunction(this.options[type])){
                eval(this.options[type]).apply(this, Array.prototype.slice.call(arguments, 1));
            }
        },
        fetchData: function(data,items) {
            appDB.url = options.dbURL.read + '&module='+ options.module;
            if (typeof items !== 'undefined'){
                appDB.url = appDB.url  + '&itemid='+options.itemid;
            }
            appDB.data = $.extend(options.postData,data);
            appDB.dataType = 'JSON';
            appDB.trigger = 'fetchData';
            $(appDB).loadStore(appDB);

        },
        renderRecursive: function(data,level,tpl) {//tpl should be a kendo template
            var render ='';
            
            level = (level == null) ? 0 : level;
            for (i in data) {
                data[i]['level'] = level;
                render += (typeof tpl === 'object') ? templates[tpl.template]({d:data[i],t:tpl,me:this}) : templates[tpl.template](data[i]);//this is used to do advanced template operations like check with existing elements
                if (typeof data[i]['subs'] == 'object') {
                    render += tpl.encStart + this.renderRecursive(data[i]['subs'],level+1,tpl) + tpl.encEnd;
                }
            }
            return render;
        },
        renderPages: function(data) {
            return templates['pages'](data);
        },
        renderCategories: function(data){
            //console.log(obj)
        },
        filterPages: function(data) {
            var source = data.parents('.mAccordion:first').find('.itemsResults');
            this._setupCache(data.val());

            if (data.val() in cache[options.module]) {
                this.drawResults(source,{allPages:cache[data.attr('data-module')][data.val()],itemsFlat:this.itemsFlat});
                return;
            }
            
            var me = data;
            var filter = {};
            var filters = new Array;
            var v = mcms.formData(me.parents('.mAccordion').find('.SearchData'),{ getData:true});
            if (v.substring) {
                filter.filter = {};
                filter['filter']['filters'] = [{field:'q',operator:'contains','logic':'and',value:v.substring['value']}];
            }
            if (v.categoryid) {
                filter.categoryid =  v.categoryid['value'];
                filter.search_in_subcategories =  1;
            }
            appDB.source = source;
            appDB.query = data.val();
            appDB.me = me;
            appDB.module = data.attr('data-module');
            appDB.trigger = 'loadComplete';
            filter.pageSize = options.results_per_page;
            appDB.url = options.dbURL.filter + '&module='+options.module;
            appDB.data = filter;
            $(appDB).loadStore(appDB);
            
        },
        updateOrder: function(e,ui){
            appDB.url = options.dbURL.order;
            appDB.me = $(this);
            appDB.data = { categoryid :app.settings.currentCategory, items:this.sortable.sortable('toArray') };
            appDB.trigger = 'updateOrder';
            $(appDB).loadStore(appDB);

            this._trigger( "onBeforeUpdateOrder",this,appDB,ui );
        },
        addToList: function(data){
            this._trigger( "onBeforeAddtoList",this,appDB );
        },
        deleteFromList: function(data) {

        },
        drawResults: function(target,data) {
            target.empty().append(this.templates['pages'](data));
        },
        init: function(){
            this._trigger('onBeforeAppLoaded',this);
            this.fetchData({fetch:options.modules},true);  
            this._trigger('onAppLoaded',this);
        },
        setupSearchForm: function(data){
            var tmp = templates['searchItems']({obj:data,me:this});
            $(obj).find(options.areas.filters).empty().append(tmp);
        },
        drawResults: function(target,data) {
            target.empty().append(this.templates['pages']({d:data,me:this}));
        },
        updateState:function(el){
            var order = $(obj).find(options.areas.sortable).sortable('toArray');
            var o = order.length,q = new Array;

            for (p=0;o>p;p++){
                q.push(mcms.callInternalMethod('listAttributes',$(obj).find(options.areas.sortable).find('#'+order[p]),{prefix:'data-',returnType:'keyVal',stripPrefix:true}));
            }

            
            appDB.url = options.dbURL.order +  '&module='+options.module;
            appDB.data = {data:q}
            appDB.trigger = 'itemsUpdated';
            $(appDB).loadStore(appDB);

            this._trigger( "onAfterUpdateOrder",this,appDB,el );
        },
        drawItems:function(data){
             $(obj).find(options.areas.sortable).empty().append(templates['items']({d:data,me:this}));
             $(obj).find(options.areas.sortable).sortable(options.sortableDefualts);
        },
        deleteFromList: function(o){
            var r=confirm($.lang.u_sure);

            if (r === true) {
                appDB.url = options.dbURL.remove +  '&module='+options.module;
                appDB.me = o;
                appDB.data = { sourceID :options.itemid,sourceModule:options.moduleSource, destModule:o.parent().attr('data-module'), destID:o.parent().attr('rel'),type:o.parent().attr('data-type') };
                appDB.trigger = 'itemsDeleted';
                $('#'+ o.parent().attr('data-type') + '-' + o.parent().attr('rel')).removeClass('hidden');
                $('#'+ o.parent().attr('data-type') + '-' + o.parent().attr('rel')).parents('li:first').removeClass('inUse');

                var p = this.itemsFlat[o.parent().attr('data-module')][o.parent().attr('data-type')].length;
                for(p;p--;) {
                    if (this.itemsFlat[o.parent().attr('data-module')][o.parent().attr('data-type')][p] == parseInt(o.parent().attr('rel'))) {
                        this.itemsFlat[o.parent().attr('data-module')][o.parent().attr('data-type')].splice(p, 1);
                    }
                }
                $(appDB).loadStore(appDB);
            }
            this._trigger( "onBeforeDelete",this,appDB );
        },
        flatenItems:function(data){

            for(p in data){
                if (typeof this.itemsFlat[data[p]['destModule']] == 'undefined') {this.itemsFlat[data[p]['destModule']] = {};}
                if (typeof this.itemsFlat[data[p]['destModule']][data[p]['type']] == 'undefined') {this.itemsFlat[data[p]['destModule']][data[p]['type']] = new Array;}
                this.itemsFlat[data[p]['destModule']][data[p]['type']].push(data[p]['destID']);
            }

        }
    }//END APP

    app.init();

    $(appDB).bind('fetchData',function(){
        app.filters = appDB.records;

        if (typeof appDB.records.items !== 'undefined' && appDB.records.items != null){
            app.existing = app.flatenItems(appDB.records.items);

            app.drawItems(appDB.records.items);
        }
        app.setupSearchForm(app.filters);
    });

    $(obj).on('click','.mAccordion h4',function(e){//ACCORDION
        e.preventDefault();
        $(this).parents('.mAccordion').find('.sub_section').hide();
        $(this).parents('.micro').find('.sub_section').show();
    });

    $(obj).on('keyup',options.areas.filterField,function(e){
        var that = $(this);
      delay(function(){
        app.filterPages(that);
      }, 400 );         
    });

    $(obj).on('click','.quickFilter',function(e){
        e.preventDefault();
        app.filterPages($(this).parents('.mAccordion').find('.filter'));
    });

    $(obj).on('change','select[name=module]',function(e){
        options.module = $(this).val();
        app.fetchData({});
    });

    $(obj).on('click','.addItems',function(e){
        e.preventDefault();
        appDB.url = options.dbURL.add + '&module='+options.moduleSource;
        var tmp = mcms.callInternalMethod('listAttributes',$(this),{prefix:'data-',returnType:'keyVal',stripPrefix:true});
        tmp.lastOrderby = $(options.areas.sortable+':first').children('li').length;
        
        appDB.data = {itemid:options.itemid,data:mcms.formData($(this).parents('.micro').find('input[type=checkbox]:checked'),{ getData:true})};
        appDB.trigger = 'itemsAdded';

        $(appDB).loadStore(appDB);
    });

    $(appDB).bind('itemsAdded',function(){
        var r = '',p = new Array;
        for(j in appDB.data.data) {
            var el = $('#'+appDB.data.data[j]['el']);
            var d = {};
            d['itemid'] = appDB.data.data[j]['value'];
            d['item'] = {};
            d['item']['title'] = el.parents('li:first').find('span').text();
            d['type'] = appDB.data.data[j]['type'];
            d['module'] = appDB.data.data[j]['module'];
            p.push(d);
            if (typeof app.itemsFlat[d['type']] == 'undefined') {
                app.itemsFlat[d['type']] = new Array;
            }

            if (typeof app.itemsFlat[d['module']] == 'undefined') {app.itemsFlat[d['module']] ={}; }
            if (typeof app.itemsFlat[d['module']][d['type']] == 'undefined') {app.itemsFlat[d['module']][d['type']] = new Array;}
            app.itemsFlat[d['module']][d['type']].push(d['itemid']);
            el.parents('label:first').addClass('inUse');
            el.addClass('hidden');
            el.attr('checked',false);//reset
        }
        $(obj).find(options.areas.sortable).append(app.templates['items']({d:p,me:app}));
        if (options.sortItems) {
            app.sortable = $(obj).find(options.areas.sortable).sortable();//reinitialize sortables
        }
        saved();
    });


    $(obj).on('click','img.wRemove',function(e){//delegate cause live registers the event on every load
        e.preventDefault();
        app.deleteFromList($(this));
    });

    $(appDB).bind('loadComplete',function(){
        var res = appDB.records;
        if (appDB.query.length > 0) {

            cache[appDB.module][appDB.query] = res.results;
        }

        app.drawResults(appDB.source,{allPages:res.results,itemsFlat:app.itemsFlat});
    });

    $(appDB).bind('itemsDeleted',function(){
        app._trigger( "onAfterDelete",this,appDB );
        $(appDB.me).parents('li:first').remove();
        saved();
    });

    return app;

}//END CLASS

$.fn.itemsRelated = function(options) {
if (typeof(options) !== 'object') options = { };

// Iterate over each object, attach Jcrop
this.each(function()
{

if ($(this).data('itemsRelated')){
    return $(this).data('itemsRelated');
}
else {
    $(this).data('itemsRelated',$.itemsRelated($(this),options))
}
});
}//END PLUGIN



})(jQuery);