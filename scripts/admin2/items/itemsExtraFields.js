head.js('/scripts/mcms.tabs.js');
head.ready(function(){
	var itemid = $('input[name=itemID]').val();
	var currentModule = $('input[name=module]').val();
	$(".tabber").live("click", function(e) {
		e.preventDefault();
		$('.tabcontent').hide();
		$(".tabber").removeClass('selected');
		$(this).addClass('selected');
		$('#'+$(this).attr('rel')).show();
		$(this).parents('div:first').find('.saveEfields').attr('data-code',$(this).attr('data-code'));
	});

	$('.languageTranslationBox').focus(function(event){
		var thisName = $(this).attr('name').split('-');//GET THE NAME
		thisName = thisName[0];
		$('#defaultTranslationTextArea').val($('#original-content').find('#'+thisName).val());
	}).blur(function(event){
		$('#defaultTranslationTextArea').val('');
	});
	$('.saveEfields').live('click', function(e) {
		var action ='';
		if ($(this).attr('data-code')){
			action = 'translateEfieldValues';
			db.data = {data : mcms.formData($('#tab-'+$(this).attr('data-code')).find('.efields'),{ getData:true}) };	
		}
		else{
			action = 'saveEfields';
			db.data = {data : mcms.formData($(this).parents('.tab:first').find('.efields'),{ getData:true}) };	
		}
		
		db.url = '/ajax/loader.php?action='+ action + '&file=items/itemsActions.php&id='+itemid+'&module='+currentModule;
		db.trigger = 'efieldsSaved';
		
		$(db).loadStore(db);
	});
	
	

	$(db).bind('efieldsSaved',function(){
		saved();		
	});
	
	
	$('.tabsS').tabber();

});//END HEAD


