head.ready(function(){
var itemid = $('input[name=itemID]').val();
var currentModule = $('input[name=module]').val();
	$(".tabber").live("click", function(e) {
		e.preventDefault();
		$('.tabcontent').hide();
		$(".tabber").removeClass('selected');
		$(this).addClass('selected');
		$('#'+$(this).attr('rel')).show();
	});

	$('.languageTranslationBox').focus(function(event){
		var thisName = $(this).attr('name').split('-');//GET THE NAME
		thisName = thisName[0];
		$('#defaultTranslationTextArea').val($('#original-content').find('#'+thisName).val());
	}).blur(function(event){
		$('#defaultTranslationTextArea').val('');
	});

	$('.SaveTranslations').live('click', function(e) {
		db.url = '/ajax/Languages.php?action=saveContentTranslations&id='+itemid+'&module='+currentModule;
		db.trigger = 'translationSaved';
		db.data = {data : mcms.formData($('.languageTranslationBox'),{ getData:true}) };	
		$(db).loadStore(db);
	});

	$(db).bind('translationSaved',function(){
		saved();		
	});
	
});//END HEAD