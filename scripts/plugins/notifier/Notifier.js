var notifierAjaxUrl = "/ajax/loader.php?file=plugins/notifier/admin.php";
head.ready(function() {

	var dlDataSource = new kendo.data.DataSource({
		//batch: true,
		serverPaging: true,
		serverSorting: true,
		serverFiltering: true,
		serverAggregates: true,
		pageSize: 40,
		transport: {
			read: {
				url: notifierAjaxUrl,
				// dataType: "jsonp"
			},
			update: {
				url: notifierAjaxUrl,
				// dataType: "jsonp"
			},
			destroy: {
				url: notifierAjaxUrl,
				// dataType: "jsonp"
			},
			create: {
				url: notifierAjaxUrl,
				// dataType: "jsonp"
			},
			parameterMap: function(data, option) {
				if(option == "read") {
					return data;
				}
				if(option == "update") {
					var tmpArr = data;
					tmpArr['action'] = "modify";
					return tmpArr;
				}
				if(option == "create") {
					var tmpArr = data;
					// delete tmpArr.settings;
					tmpArr['action'] = "addnew";
					return tmpArr;
				}
				if(option == "destroy") {
					var tmpArr = data;
					tmpArr['action'] = "delete";
					return tmpArr;
				}
				return data;
			}
		},
		schema: {
			total: function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = obj.count;
				return objVal;
			},
			model: {
				id: "id",
				fields: {
					key: {
						text: "ID",
						type: "string",
						editable: false,
						nullable: false
					},
					date_added: {
						text: "Date",
						type: "string",
						editable: false,
						nullable: true
					},
					module: {
						text: "Enntity",
						type: "string",
						editable: false,
						nullable: true
					},
					description: {
						text: "Text",
						type: "string",
						editable: false,
						nullable: true
					},
					permalink: {
						text: "Text",
						type: "string",
						editable: false,
						nullable: true
					},
					status: {
						text: "status",
						type: "string",
						editable: true,
						nullable: true
					},
					active: {
						text: "active",
						type: "string",
						editable: true,
						nullable: true
					},
				}
			},
			data: function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = obj.data;
				return objVal;
			},
			parse: function(data) {
				return data;
			}
		},
		requestStart: function(e) {},
		change: function(e) {}
	});

	var notifierGrid = $("#notifierGrid").kendoGrid({
		dataSource: dlDataSource,
		pageSize: 40,
		navigatable: true,
		pageable: true,
		//selectable: 'row',
		selectable: false,
		editable: "inline",
		scrollable: true,
		scrollable: {
			virtual: false
		},
		height: 450,
		filterable: {
			extra: false
		},
		toolbar: [{
			text: "",
			template: kendo.template($("#id-toolbar-lang").html())
		}],
		columns: [{
			field: "key",
			title: "ID",
			width: "10%"
		}, {
			field: "date_added",
			title: "Date & Time",
			width: "15%",
			filterable: false,
			template: function(data) {
				return dateFormater(data.date_added);
			}
		}, {
			field: "module",
			title: "Entity",
			width: "25%",
		}, {
			field: "description",
			title: "Text",
			width: "35%",
			filterable: false,
		}, {
			field: "active",
			title: "Active",
			filterable: false,
			editor: booleanDropDownEditor,
			width: "10%",
			template: function(data) {
				return data.active == 1 ? "<span class=\"fl notification ok_bg\">Ναι</span>" : "<span class=\"fl notification error_bg\">Οχι</span>"
			},
			scrollable: false,
			//
		}, {
			command: [{
				text: "",
				template: function(data) {
					return "<span class='editBtn k-icon k-edit pointer'></span>"
				}
			}, {
				text: "",
				template: function(data) {
					return "<span data-id='${uid}' data-name='${name}' class='deleteBtn k-delete k-icon pointer'></span>";
				}
			}, {
				text: "",
				template: function(data) {
					return '<a href="${permalink}"><img src="/images/admin/content.png" width="14" class="spacer-left" title="Διαχείριση Περιεχομένου"></a>';
				}
			}],
			filterable: false,
			title: " ",
			width: "7%"
		}]
	}).delegate(".editBtn", "click", function(e) {
		e.preventDefault();
		notifierGrid.editRow($(this).closest("tr"));
		$(this).removeClass("editBtn");
		$(this).removeClass("k-edit");
		$(this).addClass("k-update");
		$(this).addClass("updateBtn");
		$(this).parent("tr").children("deleteBtn").removeClass("hidden");
	}).delegate(".deleteBtn", "click", function(e) {
		e.preventDefault();
		notifierGrid.cancelRow();
		$(this).removeClass("editBtn");
		$(this).removeClass("k-edit");
		$(this).addClass("k-update");
		$(this).addClass("updateBtn");
		$(this).parent("tr").children("deleteBtn").removeClass("hidden");
		var targetRow = $(this).closest("tr");
		notifierGrid.removeRow(targetRow);
	}).delegate(".updateBtn", "click", function(e) {
		e.preventDefault();
		notifierGrid.saveRow();
	}).delegate(".addnew-button", "click", function(e) {
		e.preventDefault();
		notifierGrid.addRow();
		$("#notifierGrid").find(".k-grid-edit-row .editBtn").removeClass("editBtn").removeClass("k-edit").addClass("k-update").addClass("updateBtn");
	}).delegate(".langcode", "click", function(e) {
		dlDataSource.filter({
			field: "language",
			operator: "eq",
			value: $(this).attr("data-code")
		});
	}).delegate(".clear-button", "click", function(e) {
		dlDataSource.filter({});
		var val = $(this).parent("div").find(".active-button").attr("data-code");
		if(val > 0) {
			$(this).parent("div").find(".active-button").attr("data-code", (val > 0 ? 0 : 1));
			var tmp = $(this).parent("div").find(".active-button").children(".textToShow").html();
			$(this).parent("div").find(".active-button").children(".textToShow").html(
			$(this).parent("div").find(".active-button").children(".textToHide").html());
			$(this).parent("div").find(".active-button").children(".textToHide").html(tmp);
			if($(this).parent("div").find(".active-button").children(".k-icon").hasClass("k-disable")) {
				$(this).parent("div").find(".active-button").children(".k-icon").removeClass("k-disable");
				$(this).parent("div").find(".active-button").children(".k-icon").addClass("k-enable");
			} else {
				$(this).parent("div").find(".active-button").children(".k-icon").removeClass("k-enable");
				$(this).parent("div").find(".active-button").children(".k-icon").addClass("k-disable");
			}
		}
	}).delegate(".active-button", "click", function(e) {
		var val = $(this).attr("data-code");
		dlDataSource.filter({
			field: "active",
			operator: "eq",
			value: (val > 0 ? 1 : 0)
		});
		$(this).attr("data-code", (val > 0 ? 0 : 1));
		var tmp = $(this).children(".textToShow").html();
		$(this).children(".textToShow").html($(this).children(".textToHide").html());
		$(this).children(".textToHide").html(tmp);
		if($(this).children(".k-icon").hasClass("k-disable")) {
			$(this).children(".k-icon").removeClass("k-disable");
			$(this).children(".k-icon").addClass("k-enable");
		} else {
			$(this).children(".k-icon").removeClass("k-enable");
			$(this).children(".k-icon").addClass("k-disable");
		}
	}).bind("edit", function(e) {

	}).data('kendoGrid');

	function dateFormater(timeStamp)
	{
		var date = new Date(parseInt(timeStamp)*1000);
		var str = "";
		str += date.getHours()+":"+date.getMinutes()+", ";
		str += date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear();
		return str;
	}

	function editor(container, options) {
		$('<textarea id="value-en"	name="value-en" rows="5" cols="10"></textarea><br><a class="ckEditor" rel="value-en" data-width="150" data-height="150" href="#">HTML edition</a>').appendTo(container);
		$(".k-edit-field").addClass("spacer-bottom");
	}

	function booleanDropDownEditor(container, options) {
		$('<input	/>').appendTo(container).kendoDropDownList({
			dataTextField: "text",
			dataValueField: "value",
			autoBind: true,
			dataSource: [{
				text: "Yes",
				value: 1
			}, {
				text: "No",
				value: 0
			}]
		});
	}
});