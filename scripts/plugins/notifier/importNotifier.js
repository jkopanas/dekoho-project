(function($, undefined) {
	jQuery.fn.center = function(parent) {

		if(parent) {
			parent = this.parent();
		} else {
			parent = window;
		}
		this.css({
			"position": "absolute",
			"top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
			"left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
		});
		return this;
	}

	var delay = (function() {
		var timer = 0;
		return function(callback, ms) {
			clearTimeout(timer);
			timer = setTimeout(callback, ms);
		};
	})();

	$(".filter[data-type='date']").kendoDatePicker({
		format: "dd/MM/yyyy",
		change: function(e) {
			//this.value().getTime();
		}
	});

	$.notifier = function(obj, options) {
		var obj = obj,
			options = options,
			templates = {},
			cache = {},
			app = {};
		if(typeof(obj) !== 'object') obj = $(obj)[0];
		if(typeof(options) !== 'object') options = {};

		var showsActiveItems = true;
		var baseUrl = '/ajax/loader.php';
		var ajaxUrl = '/ajax/loader.php?file=plugins/notifier/admin.php&action=review';
		var totalRows = 0;
		var defaults = {
			templates: '#templates',
			modalName: 'modal-back',
			fetchUrl: ajaxUrl,
			forms: {
				components: 'item',
				ships: 'item',
				promotions: 'item',
				//'promotions',
				shipareas: 'item',
				cabins: 'item',
				brands: 'item',
				//'brands',
				cruises: 'item',
				decks: 'item',
				//'decks',
				itinerary: 'item',
				ports: 'item',
				regions: 'item',
				//regions'
			}
		};
		var options = $.extend(defaults, options);
		var filters = {};
		var req = {
			file: 'plugins/notifier/admin.php',
			results_per_page: 10,
			skip: 0,
			page: 1,
			pageSize: 10,
			active: (showsActiveItems + 0) // a trick to trip
		};
		var url = createRequestUrl(req);

		var dbDefaults = {
			method: "POST",
			dataType: "json",
			trigger: "loadComplete",
			async: true
		};
		var appDB = new mcms.dataStore(dbDefaults);

		/* read templates */
		if(typeof templates.main == 'undefined') {

			var o = $(options.templates).find('script');
			var p = o.length;

			for(p; p--;) {
				templates[$(o[p]).attr('id')] = kendo.template($(o[p]).html());
			}
		} //END TEMPLATES
		var app = {
			options: options,
			templates: templates,
			_trigger: function(type, data) {
				data = data || {};
				if($.isFunction(this.settings[type])) {
					eval(this.settings[type]).apply(this, Array.prototype.slice.call(arguments, 1));
				}
			},
			init: function() {
				appDB.url = url;
				$(appDB).loadStore(appDB);
			},
			renderRows: function(html) {
				$(obj).find('tbody').empty().html(html);
			},
			addLoader: function() {
				$(obj).find('tbody').empty().html(templates['loader']({}) + "");
			},
			fetchRecord: function(o) {
				appDB.url = options.fetchUrl + '&id=' + o.id + '&module=' + o.module;
				appDB.trigger = 'fetchRecord';
				appDB.lastO = o;
				$(appDB).loadStore(appDB);
			},
			addModal: function(o) {
				this.fetchRecord(o);
				$('body').append('<div id="' + options.modalName + '"></div><div id="modal"><div class="close"></div><div class="inner"></div></div>');
				$('#modal').center();
			},
			filterPages: function(target, additional) {
				if(target) {
					filters[target.prop('name')] = target.val();
				}
				if(additional) {
					additional = "&"+additional;
				}
				var tmpArr = [];
				for(tmpFilter in filters) {
					tmpArr.push("filter[filters][" + tmpFilter + "]=" + filters[tmpFilter]);
				}
				appDB.trigger = "loadComplete";
				//appDB.url = createRequestUrl(req) + '&value=' + target.val() + '&field=' + target.prop('name');
				appDB.url = createRequestUrl(req) + "&"+additional + '&' + tmpArr.join('&');
				$(appDB).loadStore(appDB);
			},
			removeModal: function() {
				$('#' + options.modalName).remove();
				$('#modal').remove();
			}
		}; //END APP
		$(appDB).bind('loadComplete', function() {
			var l = appDB.records.data.length;
			var html = '';
			totalRows = appDB.records.count;
			for(var i = 0; l > i; i++) {
				html += templates['row'](appDB.records.data[i]);
			}
			app.renderRows(html);
			try {
				var num = (req.results_per_page * (req.page-1));
				$("#firstRowIDPTR").html( (num>0? num : 0)+1 );
				$("#lastRowIDPTR").html( (num>0? num : 0)+l );
				//$("#lastRowIDPTR").html(appDB.records.data[l - 1]['id'] + "");
				$("#totalRowsPTR").html(appDB.records.count);
			} finally {

			}
			if(totalRows <= req.results_per_page && req.page === 1) {
				$('#data_table_next').removeClass('paginate_enabled_next');
				$('#data_table_next').addClass('paginate_disabled_next');
			} else if(req.page === 1) {
				$('#data_table_next').removeClass('paginate_disabled_next');
				$('#data_table_next').addClass('paginate_enabled_next');
			}
		});

		$(appDB).bind('fetchRecord', function() {
			var msg = templates['form']({
				records: appDB.records,
				me: app,
				a: appDB.lastO
			});
			$('#modal').find('.inner').html(msg);
		});

		$(obj).on('click', '.editMe', function(e) {
			e.preventDefault();
			app.addModal({
				id: $(this).attr('data-id'),
				module: $(this).attr('data-module')
			});
		});

		$('body').on('click', '#modal .close', function(e) {
			app.removeModal();
		});

		$('body').on('change', '.dataTables_length select', function(e) {
			app.addLoader();
			req.results_per_page = $(this).val();
			req.page = 1;
			app.filterPages();
			//appDB.trigger = "loadComplete";
			//appDB.url = createRequestUrl(req);
			//$(appDB).loadStore(appDB);

			//$('#data_table_next').removeClass('paginate_disabled_next');
			//$('#data_table_next').addClass('paginate_enabled_next');
			//$('#data_table_previous').removeClass('paginate_enabled_previous');
			//$('#data_table_previous').addClass('paginate_disabled_previous');
		});

		$('body').on('click', '.statusChangeBtn', function(e) {
			app.addLoader();
			showsActiveItems = !showsActiveItems;
			req.active = (showsActiveItems + 0);
			app.filterPages();
			//appDB.trigger = "loadComplete";
			//appDB.url = createRequestUrl(req);
			//$(appDB).loadStore(appDB);
			if(showsActiveItems) {
				$('#disableItemsBtn').show();
				$('#enableItemsBtn').hide();
			} else {
				$('#enableItemsBtn').show();
				$('#disableItemsBtn').hide();
			}
		});

		$('body').on('click', '#data_table_next.paginate_enabled_next', function(e) {
			app.addLoader();
			if((req.results_per_page * req.page) < totalRows) {
				req.page++;
				app.filterPages();
				$('#data_table_previous').removeClass('paginate_disabled_previous');
				$('#data_table_previous').addClass('paginate_enabled_previous');
				if((req.results_per_page * req.page) > totalRows) {
					$('#data_table_next').removeClass('paginate_enabled_next');
					$('#data_table_next').addClass('paginate_disabled_next');
				}
			}
		});

		$('body').on('click', '#data_table_previous.paginate_enabled_previous', function(e) {
			app.addLoader();
			if(req.page > 1) {
				req.page--;
				app.filterPages();
				$('#data_table_next').removeClass('paginate_disabled_next');
				$('#data_table_next').addClass('paginate_enabled_next');
				if(req.page <= 1) {
					req.page = 1;
					$('#data_table_previous').removeClass('paginate_enabled_previous');
					$('#data_table_previous').addClass('paginate_disabled_previous');
				}
			}
		});

		$('body').on('click', '#deleteItemsBtn', function(e) {
			var tmpArr = [];
			$('td.chb_col .inpt_c1[type="checkbox"]:checked').each(function(i, tmpObj) {
				tmpArr.push($(tmpObj).attr('data-id'));
			});
			if(showsActiveItems) {
				appDB.url = createRequestUrl(req) + "&action=inactivate&ids=" + tmpArr.join(",");
			} else {
				appDB.url = createRequestUrl(req) + "&action=delete&ids=" + tmpArr.join(",");
			}
			app.addLoader();
			appDB.trigger = "loadComplete";
			$(appDB).loadStore(appDB);
		});

		$('body').on('click', '.chSel_all', function(e) {
			if(this.checked) {
				$('.inpt_c1[type="checkbox"]').prop('checked', true);
			} else {
				$('.inpt_c1[type="checkbox"]').prop('checked', false);
			}
		});

		$('body').on('click', '.clear-button', function(e) {
			filters = {};
			req.page = 1;
			$(".th_wrapp .filter").val('');
			app.addLoader();
			appDB.trigger = "loadComplete";
			appDB.url = createRequestUrl(req);
			$(appDB).loadStore(appDB);
		});

		$(obj).on('keyup', '.th_wrapp input', function(e) {
			var that = $(this);
			delay(function() {
				app.addLoader();
				if(that.val().length > 0) {
					app.filterPages(that);
				} else {
					delete filters[that.prop('name')];
					app.filterPages();
				}
			}, 400);
		});

		$(obj).on('change', '.th_wrapp select', function(e) {
			var that = $(this);
			app.addLoader();
			if(that.val().length > 0) {
				app.filterPages(that);
			} else {
				delete filters[that.prop('name')];
				app.filterPages();
			}
		});

		$(obj).on('change', '.th_wrapp input[data-type="date"]', function(e) {
			var that = $(this);
			app.addLoader();
			if(that.val().length > 0) {
				app.filterPages(that);
			} else {
				delete filters[that.prop('name')];
				app.filterPages();
			}
		});

		$('body').on('click', '.formEl_a.current div.save-btn', function(e) {
			var data = new mcms.formData($(this).parents('.formEl_a:first').find('.toSave'), {
				'ReturnType': 'array'
			});
			var tmpArr = [];
			for(var tmpObj in data) {
				tmpArr.push("data[" + tmpObj + "]=" + data[tmpObj]);
			}
			var targetKey = $(this).attr('data-value');
			$(appDB).bind('saveComplete', function() {
				var tmp = "";
				if(showsActiveItems) {
					tmp = "action=inactivate&id=" + targetKey;
					//appDB.url = createRequestUrl(req) + "&action=inactivate&id=" + targetKey;
				} else {
					tmp = "action=delete&id=" + targetKey;
					//appDB.url = createRequestUrl(req) + "&action=delete&id=" + targetKey;
				}
				app.filterPages(false, tmp);
				//appDB.trigger = "loadComplete";
				//$(appDB).loadStore(appDB);
			});
			appDB.trigger = "saveComplete";
			appDB.url = createRequestUrl(req) + '&action=commit&id=' + $(this).attr('data-value') + '&module=' + $('input[id="target-module"]').val() + '&' + tmpArr.join('&');
			$(appDB).loadStore(appDB);
			app.removeModal();
		});

		$('body').on('click', '.formEl_a.revision div.transact-btn', function(e) {
			var field = $(this).attr('data-name');
			$('.formEl_a.current #' + field).val($('.formEl_a.revision #' + field).val());
		});

		$('body').on('click', '.formEl_a div.transactall-btn', function(e) {
			$('.formEl_a.revision .toSave').each(function(i, target) {
				$('.formEl_a.current #' + $(target).attr('id')).val($(target).val());
			});
		});

		window.onresize = function() {
			$('#modal').center();
		};

		function createRequestUrl(url) {
			var tmp = new Array;
			for(j in url) {
				tmp.push(j + '=' + url[j]);
			}
			return baseUrl + '?' + tmp.join('&');
		}
		app.init();
		return app;

	} //END CLASS
	$.fn.notifier = function(options) {
		if(typeof(options) !== 'object') options = {};

		// Iterate over each object, attach Jcrop
		this.each(function() {

			if($(this).data('notifier')) {
				return $(this).data('notifier');
			} else {
				$(this).data('notifier', $.notifier($(this), options))
			}
		});
	} //END PLUGIN
})(jQuery);