var ajaxUrl = "/ajax/loader.php?file=plugins/advancedSearch/index.php";
head.ready(function() {
	var asDataSource = new kendo.data.DataSource({
		//batch: true,
		serverPaging: true,
		serverSorting: true,
		serverFiltering: true,
		serverAggregates: true,
		pageSize: 40,
		transport: {
			read: {
				url: ajaxUrl,
				// dataType: "jsonp"
			},
			update: {
				url: ajaxUrl,
				// dataType: "jsonp"
			},
			destroy: {
				url: ajaxUrl,
				// dataType: "jsonp"
			},
			create: {
				url: ajaxUrl,
				// dataType: "jsonp"
			},
			parameterMap: function(data, option) {
				if(option == "read") {
					return data;
				}
				if(option == "update") {
					var tmpArr = data;
					tmpArr['action'] = "modify";
					return tmpArr;
				}
				if(option == "create") {
					var tmpArr = data;
					// delete tmpArr.settings;
					tmpArr['action'] = "addnew";
					return tmpArr;
				}
				if(option == "destroy") {
					var tmpArr = data;
					tmpArr['action'] = "delete";
					return tmpArr;
				}
				return data;
			}
		},
		schema: {
			total: function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = obj.count;
				return objVal;
			},
			model: {
				id: "id",
				fields: {
					id: {
						text: "id",
						type: "string",
						editable: false,
						nullable: true
					},
					key: {
						text: "key",
						type: "string",
						editable: false,
						nullable: true
					},
					shipID: {
						text: "shipID",
						type: "string",
						editable: false,
						nullable: true
					},
					shipKey: {
						text: "shipKey",
						type: "string",
						editable: false,
						nullable: true
					},
					shipName: {
						text: "shipName",
						type: "string",
						editable: false,
						nullable: true
					},
					title: {
						text: "title",
						type: "string",
						editable: false,
						nullable: true
					},
					description: {
						text: "description",
						type: "string",
						editable: false,
						nullable: true
					},
					sailingPortID: {
						text: "sailingPortID",
						type: "string",
						editable: false,
						nullable: true
					},
					sailingPortKey: {
						text: "sailingPortKey",
						type: "string",
						editable: false,
						nullable: true
					},
					sailingDateTime: {
						text: "sailingDateTime",
						type: "string",
						editable: false,
						nullable: true
					},
					returnDateTime: {
						text: "returnDateTime",
						type: "string",
						editable: false,
						nullable: true
					},
					sailingDays: {
						text: "sailingDays",
						type: "string",
						editable: false,
						nullable: true
					},
					sailingNights: {
						text: "sailingNights",
						type: "string",
						editable: false,
						nullable: true
					},
					checkinTime: {
						text: "checkinTime",
						type: "string",
						editable: false,
						nullable: true
					},
					boardingTime: {
						text: "boardingTime",
						type: "string",
						editable: false,
						nullable: true
					},
					brandID: {
						text: "brandID",
						type: "string",
						editable: false,
						nullable: true
					},
					brandKey: {
						text: "brandKey",
						type: "string",
						editable: false,
						nullable: true
					},
					regionID: {
						text: "regionID",
						type: "string",
						editable: false,
						nullable: true
					},
					regionKey: {
						text: "regionKey",
						type: "string",
						editable: false,
						nullable: true
					},
					programID: {
						text: "programID",
						type: "string",
						editable: false,
						nullable: true
					},
					programKey: {
						text: "programKey",
						type: "string",
						editable: false,
						nullable: true
					},
					themeID: {
						text: "themeID",
						type: "string",
						editable: false,
						nullable: true
					},
					themeKey: {
						text: "themeKey",
						type: "string",
						editable: false,
						nullable: true
					},
					isB2B: {
						text: "isB2B",
						type: "string",
						editable: false,
						nullable: true
					},
					active: {
						text: "active",
						type: "string",
						editable: false,
						nullable: true
					},
					settings: {
						text: "settings",
						type: "string",
						editable: false,
						nullable: true
					},
					permaling: {
						text: "permaling",
						type: "string",
						editable: false,
						nullable: true
					},
					date_added: {
						text: "date_added",
						type: "string",
						editable: false,
						nullable: true
					},
					date_modified: {
						text: "date_modified",
						type: "string",
						editable: false,
						nullable: true
					},
					comments_count: {
						text: "comments_count",
						type: "string",
						editable: false,
						nullable: true
					},
					orderby: {
						text: "orderby",
						type: "string",
						editable: false,
						nullable: true
					},
					programName: {
						text: "programName",
						type: "string",
						editable: false,
						nullable: true
					},
					regionName: {
						text: "regionName",
						type: "string",
						editable: false,
						nullable: true
					},
					brandName: {
						text: "brandName",
						type: "string",
						editable: false,
						nullable: true
					},
					portName: {
						text: "portName",
						type: "string",
						editable: false,
						nullable: true
					},
					portCC: {
						text: "portCC",
						type: "string",
						editable: false,
						nullable: true
					},
					portCN: {
						text: "portCN",
						type: "string",
						editable: false,
						nullable: true
					},
					marketpricesKey: {
						text: "marketpricesKey",
						type: "string",
						editable: false,
						nullable: true
					},
					marketID: {
						text: "marketID",
						type: "string",
						editable: false,
						nullable: true
					},
					marketKey: {
						text: "marketKey",
						type: "string",
						editable: false,
						nullable: true
					},
					marketpricesMin: {
						text: "marketpricesMin",
						type: "string",
						editable: false,
						nullable: true
					},
					marketpricesMax: {
						text: "marketpricesMax",
						type: "string",
						editable: false,
						nullable: true
					},
					marketpricesPromoMin: {
						text: "marketpricesPromoMin",
						type: "string",
						editable: false,
						nullable: true
					},
					marketpricesPromoMax: {
						text: "marketpricesPromoMax",
						type: "string",
						editable: false,
						nullable: true
					},
					availabilityTag: {
						text: "availabilityTag",
						type: "string",
						editable: false,
						nullable: true
					},
					cabinsArr: {
						text: "cabinsArr",
						type: "string",
						editable: false,
						nullable: true
					},
					surchargesArr: {
						text: "surchargesArr",
						type: "string",
						editable: false,
						nullable: true
					},
					componentsArr: {
						text: "componentsArr",
						type: "string",
						editable: false,
						nullable: true
					},
				}
			},
			data: function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = obj.data;
				return objVal;
			},
			parse: function(data) {
				return data;
			}
		},
		requestStart: function(e) {},
		change: function(e) {}
	});

	var listView = $("#resultsList").kendoListView({
        dataSource: asDataSource,
        template: kendo.template($("#listTemplate").html()),
        navigatable: true,
        selectable: true,
        editable: false
    }).data("kendoListView");
});