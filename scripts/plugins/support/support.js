//var filter = { table: "users_request", field: "status" ,operator: "eq", value: "0" };
head.ready(function(){
	 var db = new mcms.dataStore({
         method: "POST",
         url:"",
         //dataType: "json",
         trigger:"loadComplete"
     });
	 				
	   $(db).bind('loadTabs',function(){
	 
		   records = db.records;
		   records = $.parseJSON(records);
		  $.each(records,function(index,value) {  		
				$('#'+value.box.name).html(value.template);	  	
		  });
		  $('input[type=checkbox]').attr('checked',false);
	 });
	 
	   
	   $(db).bind('loadSupport',function(){
		   records = db.records;
		   records = $.parseJSON(records);
		   window.location=records.results;
		  
	 });

	 var ar =[];
	 $.each($(".LoadBox"),function(index,value) {
	 	ar.push("boxID[]="+$(value).data('id'));
	 });

	 db.url = "/ajax/loader.php?"+ar.join("&");
	 db.trigger = 'loadTabs';

	 $(db).loadStore(db);
	 
	 	
	 $(".calendar").kendoCalendar({
		 	change:function(e) {
				var str = kendo.toString(this.value(), 'yyyy-MM-dd');
				var d = str.match(/\d+/g);
				$('#'+e.sender.element.attr('data-trigger')).val(+new Date(d[0], d[1] - 1, d[2])/1000);
		 	}
		 });
	 
	    var wndf = $("#FilterSupport");
	    
	    
	    $('.k-filter').live('click', function(e) {
	    	 e.preventDefault();
   			if (!wndf.data("kendoWindow")) {
            	wndf.show().kendoWindow({
            	        width: '900',
            	        height: $("#FilterSupport").height()+' ',
           		        title : "Αναζήτηση",
	                    modal : true
    	        });
        	    wndf.data("kendoWindow").center();
    		} else {            
            	wndf.data("kendoWindow").open();
           		wndf.data("kendoWindow").center();   
    		}
    		
      });
	    
	    $('.ShowSupport').live('click', function(e) {
	    	 e.preventDefault();
	    	 db.url = "/ajax/loader.php?file=plugins/support/admin.php&action=GotoSupportMsg&uid="+$(this).data('uid')+"&admin="+$(this).data('admin')+"&id="+$(this).data('conversationid');
	    	 db.trigger = 'loadSupport';

	    	 $(db).loadStore(db);
   		
     });

	    
	    $('.filterTable').live('click', function(e) {
	    	  e.preventDefault();
	    	$('.filterTable').removeClass("k-state-selected");
	    	$(this).addClass("k-state-selected");
		  		if (grid.ReturnDataSource().transport.options.read.data.filter !== undefined ) {
	    			var ar = grid.ReturnDataSource().transport.options.read.data.filter.filters;
	    		} else {
	    			var ar = [];
	    		}
	    		var flag=false;
	    		for (i in ar) {
	    			if ( $(this).data('table') == "all" && ar[i].field == "`table`" ) {
	    				ar.splice(i, 1);
	    				flag=true;
	    			} else if ( ar[i].field == "`table`" ) {
	    				ar[i].value=$(this).data('table');
	    				flag=true;
	    			} 
	    		}
	    		if (!flag) {
	    			ar.push({ table: "users_request", field: "`table`" ,operator: "eq", value: $(this).data('table') });
	    		}
	    		grid.filter(ar);

	    });
	    
		   $("#SearchSupports").live("click", function(e){
			   e.preventDefault();
			   var data= {};  
			   var ar=[];
			   	data= mcms.formData('.filterdata',{ getData:true});
			   
			    grid.ReturnDataSource().transport.options.read.data.filter = {};
			    
		   	    for (i in data) {
		   			   	ar.push({ table: data[i].table, field: i ,operator: data[i].mode, value: data[i].value });
		   	    }
		  //grid.ReturnDataSource().transport.options.read.data['new']=(data['new'] !== undefined) ? true: false;
		   	 grid.filter(ar);
		   	 wndf.data("kendoWindow").close();
			   
			}); 
	    
	    
});// end head