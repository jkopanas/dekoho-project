$.fn.Controller = function(dataSrc) {
	
	mcms.fbController = $.extend(true,kendo.observable({
		mcmsds: new mcms.dataStore({ method: "POST", url:"/ajax/loader.php?file=plugins/facebook/facebook.php&action=getTemplate", trigger:"loadComplete"}),
		ds : new kendo.data.DataSource(dataSrc),
		doActions : function(e) {		
			e.preventDefault();
			var func = $(e.currentTarget).attr('data-method');
			this.callbackHandler(func,$(e.target));
			return this;
		},
		actionUrl: function(e) {

			this.ds.transport.options.read.url=$(e).data('href');
			this.ds.fetch(this.renderResults);
			return this;
		},
		AllData: "",
		setAction: function (e) {
   			this.ds.transport.options.read.url=e;
			this.ds.fetch(this.renderResults);
   		},
		renderResults: function(e) {
			$("#load_template").html(mcms.fbController.AllData.data.template);
			var t = kendo.template($("#resultsTemplate").html());
			$("#results").html(t(this.data()));
			return this;
		}
	}),mcms.app);
	
	kendo.bind('body', mcms.fbController);
	
	
	return mcms.fbController;
};
