var facebook = '';
head.ready(function() {

	facebook = function(options) {

		var MyApp = '';
		var CurrentUser = '';
		var Status = 0;

		db = new mcms.dataStore({
					method : "POST",
					url : '/ajax/loader.php?file=plugins/facebook/facebook.php',
					dataType : "json",
					trigger : "loadComplete"
				});
				
				
		$(db).bind('InsertFbUser',function(){
			records = db.records;
			console.log(records);
			console.log('insert user');
			control.setAction('/facebook/ajax/');
		});	
	
		$(db).bind('GetUser',function(){
			records = db.records;
			CurrentUser = records;	
			console.log('connected');
			console.log(CurrentUser);
		});

		var settings = $.extend({
					appid : ''
				}, options);

		this.init = function() {

					FB.init({
						appId : settings.appid,
						channelUrl : settings.channelurl,
						status : false, // check login status
						cookie : true, // enable cookies to allow the server to
						xfbml : true, // parse XFBML
						oauth : true
					});
					
					FB.Event.subscribe('edge.create',like);
					FB.Event.subscribe('edge.remove',unlike);
					FB.Event.subscribe('comment.create',comment);
					FB.Event.subscribe('comment.remove',uncomment);
					FB.getLoginStatus(updateStatus);
		}
		
		

		var login = function() {

			FB.login(function(response) {
						if (response.authResponse) {
							FB.api('/me', function(info) {
										console.log(' insert user');
										CurrentUser = info;
										
										db.data = {
											action : "insert",
											data : CurrentUser
										}
										db.trigger = "InsertFbUser";
										$(db).loadStore(db);
									});
						} else {
							console.log('rejected login');
							control.setAction('/facebook/ajax/');
						}
					}, {
						scope : 'email,user_birthday,status_update,publish_stream,user_about_me,read_stream,user_likes,friends_likes'
					});

		}

		var updateStatus = function(response) {

			if (response.status == "connected") {
				db.data = {
					action : "get",
					id : response.authResponse.userID
				}
				db.trigger = "GetUser";
				$(db).loadStore(db);
				
				console.log('connected');
			} else if (response.status == "not_authorized") {
				control.setAction('/facebook/ajax/');
				console.log('not uthorized yet');
				login();	
			} else {
				control.setAction('/facebook/ajax/');
			}
			
		}

		this.logout = function() {

			FB.logout(function(response) {
						console.log("logout");
						logout(response);
						
					});

		}

		this.PublishPost = function(name, description, hrefTitle, hrefLink,userPrompt) {
			
			FB.ui({
						method : 'stream.publish',
						message : '',
						attachment : {
							name : name,
							caption : '',
							description : (description),
							href : hrefLink
						},
						action_links : [{
									text : hrefTitle,
									href : hrefLink
								}],
						user_prompt_message : userPrompt
					}, function(response) {
						showLoader(false);
					});

		}

		this.Share = function() {

			var share = {
				method : 'stream.share',
				u : 'http://thinkdiff.net/'
			};

			FB.ui(share, function(response) {
						console.log(response);
					});

		}

		this.GraphStreamPublish = function() {

			FB.api('/me/feed', 'post', {
				message : "I love thinkdiff.net for facebook app development tutorials",
				link : 'http://ithinkdiff.net',
				picture : 'http://thinkdiff.net/iphone/lucky7_ios.jpg',
				name : 'iOS Apps & Games',
				description : 'Checkout iOS apps and games from iThinkdiff.net. I found some of them are just awesome!'

			}, function(response) {

				if (!response || response.error) {
					alert('Error occured');
				} else {
					alert('Post ID: ' + response.id);
				}

			});
		};

		this.Query = function() {

			FB.api('/me', function(response) {

				// http://developers.facebook.com/docs/reference/fql/user/
				var query = FB.Data
						.query(
								'select name, profile_url, sex, pic_small from user where uid={0}',
								response.id);
				query.wait(function(rows) {

						});
			});
		};

		this.getUser = function() {
			return (CurrentUser == '') ? false : CurrentUser;
		}

	};

	// Load the SDK Asynchronously
	(function(d) {
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement('script');
		js.id = id;
		js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		ref.parentNode.insertBefore(js, ref);
	}(document));

});
