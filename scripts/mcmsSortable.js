/*
$('.mcmsSortable').mcmsSortable();
$('.mcmsSortable').mcmsSortable().bind('sortupdate', function() {
});
$('.mcmsSortable').mcmsSortable({
    items: ':not(.disabled)'
});
$('.mcmsSortable').mcmsSortable({
    handle: 'h2'
});
$('#mcmsSortable1, #mcmsSortable2').mcmsSortable({
    connectWith: '.connected'
});
$('.mcmsSortable').mcmsSortable('destroy');
$('.mcmsSortable').mcmsSortable('disable');
$('.mcmsSortable').mcmsSortable('enable');
EVENTS : sortupdate - dragStart - drop - overItem

		.connected, .sortable, .exclude, .handles {
			margin: auto;
			padding: 0;
			width: 310px;
			-webkit-touch-callout: none;
			-webkit-user-select: none;
			-khtml-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}
		.sortable.grid {
			overflow: hidden;
		}
		.connected li, .sortable li, .exclude li, .handles li {
			list-style: none;
			border: 1px solid #CCC;
			background: #F6F6F6;
			font-family: "Tahoma";
			color: #1C94C4;
			margin: 5px;
			padding: 5px;
			height: 22px;
		}
		.handles span {
			cursor: move;
		}
		li.disabled {
			opacity: 0.5;
		}
		.sortable.grid li {
			line-height: 80px;
			float: left;
			width: 80px;
			height: 80px;
			text-align: center;
		}
		li.highlight {
			background: #FEE25F;
		}
		#connected {
			width: 440px;
			overflow: hidden;
			margin: auto;
		}
		.connected {
			float: left;
			width: 200px;
		}
		.connected.no2 {
			float: right;
		}
		li.sortable-placeholder {
			border: 1px dashed #CCC;
			background: none;
		}
*/

var dragging, placeholders = $();
$.fn.mcmsSortable = function(options) {
options = $.extend({//Defaults
}, options);//END EXTEND 
	return this.each(function() {
		if (/^enable|disable|destroy$/.test(options)) {
			var items = $(this).children($(this).data('items')).attr('draggable', options == 'enable');
			options == 'destroy' &&	items.add(this)
				.removeData('connectWith').removeData('items')
				.unbind('dragstart.h5s dragend.h5s selectstart.h5s dragover.h5s dragenter.h5s drop.h5s');
			return;
		}
		var index, items = $(this).children(options.items), connectWith = options.connectWith || false;
		var placeholder = $('<' + items[0].tagName + ' class="mcmsSortable-placeholder">');
		var handle = options.handle, isHandle;
		items.find(handle).mousedown(function() {
			isHandle = true;
		}).mouseup(function() {
			isHandle = false;
		});
		$(this).data('items', options.items)
		placeholders = placeholders.add(placeholder);
		if (connectWith) {
			$(connectWith).add(this).data('connectWith', connectWith);
		}
		var dragging = $();
		items.attr('draggable', 'true').bind('dragstart.h5s', function(e) {
			if (handle && !isHandle) {
				return false;
			}
			isHandle = false;
			var dt = e.originalEvent.dataTransfer;
			dt.effectAllowed = 'move';
			dt.setData('Text', 'dummy');
			dragging = $(this).addClass('mcmsSortable-dragging');
			index = dragging.index();
			if (index == dragging.index()) {
				items.parent().trigger('dragStart',e,this);
			}
		}).bind('dragend.h5s', function() {
			
			dragging.removeClass('mcmsSortable-dragging').fadeIn();
			
			placeholders.detach();
			if (index != dragging.index()) {
				items.parent().trigger('sortupdate',this);
			}
			dragging = null;
		}).not('a[href], img').bind('selectstart.h5s', function() {
			this.dragDrop && this.dragDrop();
			return false;
		}).end().add([this, placeholder]).bind('dragover.h5s dragenter.h5s drop.h5s', function(e) {
			if (!items.is(dragging) && connectWith !== $(dragging).parent().data('connectWith')) {
				return true;
			}
			if (e.type == 'drop') {
				e.stopPropagation();
				items.parent().trigger('droped');
				placeholders.filter(':visible').after(dragging);
				return false;
			}
			if (e.type == 'dragover') {
				e.stopPropagation();
				items.parent().trigger('overItem',this);
			}

			e.preventDefault();
			e.originalEvent.dataTransfer.dropEffect = 'move';
			if (items.is(this)) {
				dragging.hide();
				$(this)[placeholder.index() < $(this).index() ? 'after' : 'before'](placeholder);
				placeholders.not(placeholder).detach();
			}
			return false;
		});
	});
};
