var plugin="";
head.ready(function(){


		 var dbcontrols = new mcms.dataStore({
	         method: "POST",
	         url:"",
	         //dataType: "json",
	         trigger:"loadCompletes"
	     });
		 
		 
		//// start booking plugin /////
	
		$("#myDiv").booking({"validate":ValidationObject,"modalName": 'modal-back'});
		plugin = $("#myDiv").data("booking");
		  
		///////////////////
		  
		$(".next").live("click",function(e) {
			
			e.preventDefault();
			
			var next_template = $(this).data("template");
			
			
			if($(this).data("function"))
			{
				plugin.ExecFunc=$(this).data("function");
			}	


			plugin.RenderPage(next_template,"myDiv");
				

		}); //end next
		
		
		$(".previous").live("click",function(e) {
			
			e.preventDefault();
			
			var previous_template = $(this).data("template_previous");	
			
			plugin.RenderPagePrevious(previous_template,"myDiv");	
			
			$("#BookForm").hide();
		
		});// end previous
		
		
		$('#NoChildren').change(function() {
			
			var input_ages= $('#NoChildren :selected').val();
			
			$('.ages').remove();
			
			for (i=0; i<input_ages; i++)
			{
				counter=i+1;
				$('<div class="ages"><span class="blue-age">Age of child '+counter+':</span> <input type="numeric" name="ChildAge'+i+'" id="ChildAge'+i+'" min="0" max="16" class="childrenage UserData blue" required data-required-msg="Age of child '+counter+' is required"/></div>').appendTo('.inputs');
			}
			 $(".childrenage").kendoNumericTextBox({format: "#"});

		}); //end change function

	
    $(".RoomSelection").live("click",function(e) {
    	
    	e.preventDefault();
    	
    	var type = $(this).attr("id");
    	
    	$("#maxprice").val($(this).data("maxprice"));
    	
    	plugin.CurrentData.AvailableCategories.AvailableCategory.MAXprice=$(this).data("maxprice");
		plugin.Redraw("cabininfo",{"searchdata":plugin.CurrentData.AvailableCategories.AvailableCategory,"search":type,"field":"CategoryCode"});
		
		$(".category-item").find("a").html("SELECT");
		$(".category-item").find("a").css("background-color","#4B8CF4");
		$(this).html("SELECTED");
		$(this).css("background-color","#113A5E");
		$("#cabininfo").find("h2").html($(this).data("type")+" Category Stateroom");

    });// end category selection

 
    $(".CabinSelection").live("click",function(e) {
    
	    	e.preventDefault();
	    	
	    	var id=$(this).data("id");
			
	    	var value=$(this).data("value");
			
	    	$("#windowpopup").empty();
	    	
	    	$("#CategoryCode").val(value);
	
	    	var Elements = NextMaxPrice({"id": value,"price":$(this).data("price")});
	    	
	    	if ($(this).data("max")==0)
	    	{
				delete plugin.clientdata.DtsCruiseCabinAvailabilityRequest;
	    	    plugin.RenderWindow("upgrade",Elements);
	       	}
	    	else
	    	{ 
	    		delete plugin.clientdata.DtsCruiseCabinAvailabilityRequest;
	    	    plugin.RenderPage("DtsCruiseCabinAvailabilityRequest","cabininfo");
	      	}  
    	
    }); //end cabin selection
    
    $(".UpgradeCategory").live("click",function() {
        
    	var id=$(this).attr("id");
    
    	$("#hiddencat").val(id);

    	plugin.RenderPage("DtsCruiseCabinAvailabilityRequest","cabininfo");
       
    }); //end cabin selection
    
    	
  function NextMaxPrice(obj) {
    	
    	var type = obj.id.charAt(0);
    	var max = obj.price;
    	var arr = {};
    	var ar = [];
    	var nexttotalcabinprice = "";
    
    	$.each(plugin.CurrentData.AvailableCategories.AvailableCategory,function(k,a) {
    	 
    	if(a.CategoryCode==obj.id)
    	  {
    		  arr.CurrentElement=a;
    	  }	
    		if(type==a.CategoryCode.charAt(0)) 
    		{
    		  if(max<a.TotalCabinPrice )
    		  {
    			  nexttotalcabinprice=a.TotalCabinPrice;
    			  ar.push(a);
    			 
    		  }
    		}
    	});
    	var min = 0;
    	$.each(ar,function(k,a) {
    		if (a.TotalCabinPrice < min || min==0) {min=a.TotalCabinPrice;arr.NextElement=a;}
    	});
    	
    	arr.ExtraPriceN = (nexttotalcabinprice-max)/$("#SailingLengthDays").val();
    	arr.ExtraPrice = Math.round (arr.ExtraPriceN*100) / 100;
      	return arr;
      	
    } // end function
  
  
    
    $(".CruisePromotionCodeClick").live("click",function(){
    	
    	plugin.validator.validate();
    	
    });

    $(".SelectCabin").live('click',function(e){
    	e.preventDefault();
    	
    	$("#CabinNo").val($(this).data('value'));
    	
    	$("div.main").hide();
    	
    	$("#myDiv").append(plugin.templates["BookForm"]({"clientdata":plugin.clientdata}));

    	$(".UpdateHeader").hide();
               $(".blueHeader span").css("color","#1B4F77")
        	   $("#HeaderBrBookForm").show();
               $("#HeaderBrBookForm").css("color","#fff");
               $("#HeaderBrBookForm").parents("span:first").css("color","#fff");
               
    }); // emd SelectCabin

/*
    $(".fill_up_form").live("click",function() {
    
    	$("div.main").hide();
    	
    	$("#myDiv").append(plugin.templates["BookForm"]({"clientdata":plugin.clientdata}));

    	
    	
    	$(".UpdateHeader").hide();
               $(".blueHeader span").css("color","#1B4F77")
        	   $("#HeaderBrBookForm").show();
               $("#HeaderBrBookForm").css("color","#fff");
               $("#HeaderBrBookForm").parents("span:first").css("color","#fff");
    	
    }); //end cabin selection
  */  
    
    
    $("#proceed-checkout").live("click",function(e) {
    		
    	e.preventDefault();
    	
    	deposite=$("#MinDeposit").val();
        
        if ($("#TxtAmount").val() >=deposite && $("#TxtAmount").val()<=$("#MaxDeposit").val()) 
        {
        	$('#checkout').submit();
        } 
        else 
        {
        	$("#hidden-msg-proceed").html("The value Must be between "+deposite+" and "+$("#MaxDeposit").val()).show().delay(3000).fadeOut('slow');

            return false;
        } 
    
    }); // end proceed-checkout


    $(plugin.db).bind('loadPrices',function(){

    		var res = plugin.db.records;
    		results=$.parseJSON(res);
    		results.clientdata = plugin.clientdata;
    		
    		plugin.drawResults(plugin.db.templid,results);
            plugin.removeModal();
    	
        }); // end bind
    
    

}); // end head ready



	function GetCurrentYear() {
	   
		var startDate = new Date();
	    
	    var currentyear = startDate.getFullYear();
	    
	    return currentyear;
	    
   } // end function GetCurrentYear
 

   	 function SendRequest(data) {
   		 	
   	 		
			plugin.ExecFunc="";
   	 		plugin.CurrentTemplate = "DtsCruisePricingAvailabilityRequest";
        	plugin.CurrentSource = "myDiv";
        	plugin.db.templid = "DtsCruisePricingAvailabilityRequest";
 
         
        	  
        	  var obj = mcms.formData('.UserData',{ getData:false});
        	   obj['ComponentID'] = data.CruiseProducts.CruiseSailing.ComponentInfo.ComponentID;
        	   plugin.clientdata['DtsCruisePricingAvailabilityRequest']= obj;
     
        	plugin.db.data=plugin.clientdata['DtsCruisePricingAvailabilityRequest'];
            plugin.addModal(plugin.messages['DtsCruisePricingAvailabilityRequest'],"loadingimg");
            plugin.db.trigger="loadPrices";
            plugin.delay(function(){
            	plugin.db.url="/ajax/loader.php?file=booking/send_request.php&template_id=DtsCruisePricingAvailabilityRequest";
                $(plugin.db).loadStore(plugin.db);        
            }, 5  );

   	 } //end function SendRequest

	function ProcessData(data){
		
			var persons = parseInt(data.clientdata.DtsCruisePricingAvailabilityRequest.NoAdults)+parseInt(data.clientdata.DtsCruisePricingAvailabilityRequest.NoChildren);	
			var SuitePrices = [];
			var InteriorPrices = [];
			var ExteriorPrices = [];
			var SuiteImage = "";
			var InteriorImage = "";
			var ExteriorImage = "";
			var totalcategories =0;
			
		  if(data.AvailableCategories) {
			$.each(data.AvailableCategories.AvailableCategory,function(k,a) {
			 if (persons<= parseInt(a.MaximumOccupancy)) {  
				if("S"==a.CategoryCode.charAt(0)) 
			    {	
					 SuiteImage=a.CategoryGraphicsUrl;
					 SuitePrices.push(a.TotalCabinPrice);
					 
			    }
				else if("I"==a.CategoryCode.charAt(0)) 
				{
					InteriorImage=a.CategoryGraphicsUrl;
					InteriorPrices.push(a.TotalCabinPrice);
				}
				else if("X"==a.CategoryCode.charAt(0)) 
				{
					ExteriorImage=a.CategoryGraphicsUrl;
					ExteriorPrices.push(a.TotalCabinPrice);
				}
			 }	
			});	
		 }		
		  
		  if(SuitePrices.length>0) {
			  totalcategories = totalcategories +1;
		  }
		  
		  if(InteriorPrices.length>0) {
			  totalcategories = totalcategories +1;
		  }
		  
		  if(ExteriorPrices.length>0) {
			  totalcategories = totalcategories +1;
		  }
		  
		  
				var maxSuite = Math.max.apply(Math, SuitePrices);
				var minSuite = Math.min.apply(Math, SuitePrices);
				var maxInterior = Math.max.apply(Math, InteriorPrices);
			    var minInterior = Math.min.apply(Math, InteriorPrices);
			    var maxExterior = Math.max.apply(Math, ExteriorPrices);
			    var minExterior = Math.min.apply(Math, ExteriorPrices);	
			    totalcategories = 
				
				
				data.ProcessedData = {"maxSuite":maxSuite,"minSuite":minSuite,"maxInterior":maxInterior,"minInterior":minInterior,
									  "maxExterior":maxExterior,"minExterior":minExterior,"SuiteImage":SuiteImage,
									  "InteriorImage":InteriorImage,"ExteriorImage":ExteriorImage,"count":totalcategories}
				
				return data;
				
	} // end function ProcessData


