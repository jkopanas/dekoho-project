head.ready(function(){
	
	var delay = (function(){
      var timer = 0;
      return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
      };
    })();

    jQuery.fn.center = function(parent) {
        
    	if (parent) {
	        parent = this.parent();
    	} else {
        	parent = window;
    	}
    	this.css({
        	"position": "absolute",
        	"top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
        	"left": "150px"
    	});
		return this;
	}

	var db = new mcms.dataStore({
        method: "POST",
        url:"",
        //dataType: "json",
        trigger:"loadCompletes"
    });
	
	var now = new Date();
	
	var year = $("#currentYear").val();
	var selectedMonths = [];
	var selectedPorts = [];
	
	$(".when").attr('checked',false);
	$(".destination").attr('checked',false);
	
	$(".CruiseNow").live('click',function(e) {		
		e.preventDefault();
		port = (selectedPorts.length > 0) ? "cruise-to/"+selectedPorts.join(",")+"/" : "";
		date = (selectedMonths.length > 0) ? "cruise-on/"+selectedMonths.join(",")+"/" : "";
		top.location.href= "http://"+document.domain+"/"+$("#loaded_language").val()+"/cruises/cruise-"+year+"/index.html#!"+port+date;
		
	});
	
	$(".when").click(function(e) {
		
		year = ( year == "" ) ? $(this).data('year') : year;
		if ( year != $(this).data('year')) {
			year = $(this).data('year');
			selectedMonths = [];
			$('.when').not(this).attr( 'checked', false);
		}

		if (!(key=$.inArray($(this).val(),selectedMonths))) {
			selectedMonths.splice (key, 1);
		} else {
			selectedMonths.push($(this).val());
		}
		
	});
	
	$(".destination").click(function() {
			
		if (!(key=$.inArray($(this).val(),selectedMonths))) {
			selectedPorts.splice (key, 1);
		} else {
			selectedPorts.push($(this).val());
		}
		
	});

	
	/// retrieve booking //////
	
	 $(db).bind('retrieve-user-data',function(){		
		var result = db.records;
		$("#retrieve-info").html(result);
		result = JSON.parse(result);
		var component_id = result.CruiseBookings.CruiseSailing.ComponentInfo.ComponentID;
		var session_id = result.SessionInfo.SessionID;
		var children = 0;
		var adults = 0;
		$.each(result.ParticipantList.ParticipantData,function(k,a) {
			if(a.PersonType == "A") {
				adults = adults + 1;
			} else {
				children = children + 1;
			}
		});
		db.url="/ajax/loader.php?file=booking/get_components.php&component_id="+component_id+"&session_id="+session_id+"&adults="+adults+"&children="+children;
 		db.trigger ='Submit-Form';
 	    db.data = {} 
    	$(db).loadStore(db); 
		
	});
	 
	 $(db).bind('Submit-Form',function(){	
	
		var result = db.records;
		$("#get-compo").html(result);
		$('#retrieve-booking').submit();
		
	});
	
		$("#ret-bkg").click(function(e) {
			e.preventDefault();
			
            $('#modal').center();
            $("#modal-back").show();$("#modal").show();
			db.url="/ajax/loader.php?file=booking/retrieve_booking.php";
		 	db.trigger ='retrieve-user-data';
		 	db.data = {};
		 	delay(function(){
		    	$(db).loadStore(db);
		    }, 5 );
		});
	//// end retrieve booking //////
	
				
		function validEmail(e) {
		    var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
		    return String(e).search (filter) != -1;
		}
		
		$("#signupfornews").click(function(e) {
			e.preventDefault();
			var email = $("#user-news-mail").val();
			
			if (validEmail(email)==true) {
			
			db.url="/ajax/loader.php?file=site/newsletter.php";
	 		db.trigger ='newsletter-signup';
	 	    db.data = {"email":email} 
	    	$(db).loadStore(db); 
			} else {
				
				$("#hidden-msg-signup").html("Not a valid e-mail").css("color","red").show().delay(3000).fadeOut('slow');
				 	
			}
		});
		
		 $(db).bind('newsletter-signup',function(){	
			 $("#user-news-mail").val("your email ...");
			 var result = db.records;
			 if (result=="register") {
				 $("#hidden-msg-signup").html("Your e-mail was registered. Thank you..").css("color","#3772B4").show().delay(3000).fadeOut('slow');
				 
			 } else {
				 $("#hidden-msg-signup").html("An error occured.Plese try again...").css("color","red").show().delay(3000).fadeOut('slow');
			}
		 });
		
});