'use strict';
/* plugin to insert a table with item results. gets options from parameters */
angular.module('dataTables', []).
directive('itemsTable', function ($compile,$http,$routeParams,$resource) {
  return {
  	replace:false,
    templateUrl : 'partials/dataTables.html', //(optional) the contents of this template can be downloaded and constructed into the element
	restrict:'E',
    link : function($scope, $element, attributes) { //this is where your magic happens
     	$scope.vars = attributes;
        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
     	var options={};
        var delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })();

		var defaults = {
			orderby : 'date_added',
			way: 'desc',
			results_per_page: 10,
			page:1,
            delay:400
		};
		var attrs = {};
		for (var a in attributes.$attr){
			attrs[attributes.$attr[a]] = attributes[a];
		}

		options = angular.extend({}, defaults, attrs);//override with incoming params
        options = angular.extend({}, options, $scope.routeParams);
		//Init tables
        options.init = true;
        getItemData($scope,options,$http);//fetch from ajax

        $scope.allSelected = false;

        $scope.toggleAllSelected = function () {

            angular.forEach($scope.items.results, function (item) {
                item.selected = $scope.allSelected;
            });
        };

		$scope.sortItemResults = function(){
			options.orderby = this.orderProp;
            options.way = this.orderWay;
            options.results_per_page = this.results_per_page;
			$scope.routeParams.page =1;
            $routeParams.page = 1;
            options = angular.extend({}, options, $routeParams);
			getItemData($scope,options,$http);//re-fetch as something changed
		}

        $scope.quickFilter = function(){
            delay(function(){
               options.filters = $scope.filters;
               delete options.init;
               getItemData($scope,options,$http);//re-fetch as something changed
            }, options.delay );
        }
    }
  };
});

function getItemData($scope,options,$http){//call to ajax to get items

		$scope.module = $scope.routeParams.moduleName;
		var page = options.page;
		$scope.orderProp = options.orderby;
		$scope.orderWay = options.way;
		$scope.results_per_page = options.results_per_page;
		$scope.options = options;

    var g = {orderby:options.orderby,way:options.way,results_per_page:options.results_per_page,filters:options.filters,init:options.init};
    $http.post(urls.loader+'?file=itemsSearch.php&module='+$scope.routeParams.moduleName+'&action=pagesFilter&page='+page, $.param(g)).
        success(function(data, status) {
            $scope.items = data;

            if (typeof data.init != 'undefined'){
                $scope.config = data.init;

            }
//            $scope.pagination = paginate(data.count,data.data.results_per_page,data.data.page);

            $scope.pagination = data.pagination;

        });
}