(function ($, undefined) {
$.mcmsModal = function(obj,options) {
    var obj = obj, options = options,templates = {},cache={},app={};
	if (typeof(obj) !== 'object') obj = $(obj)[0];
	if (typeof(options) !== 'object') options = { };

	$overlay = $('<div id="overlay"></div>');
	$modal = $('<div id="modal"></div>');
	$content = $('<div id="content"></div>');
	$close = $('<a id="close" href="#">close</a>');

	$modal.hide();
	$overlay.hide();
	$modal.append($content, $close);

	var app = {
	 	_trigger: function( type, data ) {
	 		data = data || {};
	 		if ($.isFunction(this.options[type])){
	 			eval(this.options[type]).apply(this, Array.prototype.slice.call(arguments, 1));
	 		}
       	},
		center: function () {
		    var top, left;

		    top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
		    left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;

		    $modal.css({
		        top:top + $(window).scrollTop(), 
		        left:left + $(window).scrollLeft()
		    });
		}
	};//END APP

app._trigger('onAppLoaded',this);
return app;
};//END CLASS


$.extend($.mcmsModal.prototype, {
a : function(){}
    });

 $.fn.mcmsModal = function(options) {
	if (typeof(options) !== 'object') options = { };

	// Iterate over each object, attach Jcrop
	this.each(function()
	{
    
	    if ($(this).data('mcmsModal')){
			return $(this).data('mcmsModal');
		}
        else {
            $(this).data('mcmsModal',$.mcmsModal($(this),options))
        }
    });
}//END PLUGIN

})(jQuery);