var x = {};
var y = {};
head.ready(function() {

	var delay = (function() {
		var timer = 0;
		return function(callback, ms) {
			clearTimeout(timer);
			timer = setTimeout(callback, ms);
		};
	})();

	var markerTemplate = kendo.template($("#markerTemplate").html());
	var obj = $.parseJSON($('textarea[name=fomatedItemsRaw]').val());
	var db = new mcms.dataStore({
		method: "POST",
		url: "",
		//dataType: "json",
		trigger: "loadComplete"
	});
	var page = 1,
		items_per_page = 3,
		dbURLs = '/async/shorexBox/portKey/'+$("#PortKey").val()+'/index.html',
		prevUri = '';
	//'/material/itinerary.json'
	$('#searchResultsDestination').renderCruiseResults({
		dbURL: dbURLs,
		resultsPlaceholder: $('#hiddenDiv'),
		resultsPaginator: '.paginators',
		boxWidth: 240,
		filtersForm: $('.filters'),
		templates: '#templatePackage',
		filtersClass: '.SearchData',
		modal: false,
		onDataRendered: function(fetchedResults, f, x) {

			var renderPackageTitlesBool = false;
			try {
				if(fetchedResults.data[0].related) {
					renderPackageTitlesBool = true;
				}
			} catch(e) {}
			if(renderPackageTitlesBool) {
				$(".sorexBox").show();
				var tmpTemplate = kendo.template($("#packageTitleList").html());
				$(".packageListArea").html(tmpTemplate({
					obj: fetchedResults,
					me: this
				}));
			}
			paginate(page);
		}
	});

	if(obj) {
		$('#map').googleMaps({
			zoom: parseInt(obj.map.zoomLevel),
			clat: obj.map.lat,
			clng: obj.map.lng,
			marker_icon: "/images/site/booking-dot.png",
			createMarker: {
						infoWindow: {
							content: markerTemplate( obj.items[0] )
						}
			}
		}).data('map').init();
		x = $('#map').data('map');
		
	}


	var item = $.parseJSON($('textarea[name=item]').val())
	
	
	 $('.sidebar-accordion .accordion').cruiseSearch({onDataRendered:function(v,e,obj){$(obj).find('.cruiseAccordionContent:first').show()},
		 dbURL:'/async/cruisesAccordion/action/simple.html',
		 postData:{cruiseTo:item.key}, type:'byItn'
		 }
		 );


	$(document).delegate('.shipInfo', 'click', function(e) {
		e.preventDefault();
	}).delegate('.sorexBox', 'click', function(e) {
		//e.preventDefault();
	}).delegate('.sorexBox .nextResults', 'click', function(e) {
		e.preventDefault();
		$('#searchResultsDestination .placeHolder').empty();
		$('#searchResultsDestination .placeHolder').addClass('loading');
		paginate(page + 1, 'inc');
		page++;
	}).delegate('.sorexBox .prevResults', 'click', function(e) {
		e.preventDefault();
		$('#searchResultsDestination .placeHolder').empty();
		$('#searchResultsDestination .placeHolder').addClass('loading');
		paginate(page - 1, 'inc');
		page--;
	}).delegate('.sorexBox .portAnchor', 'click', function(e, a) {
		e.preventDefault();
		$('#searchResultsDestination .placeHolder').empty();
		$('#searchResultsDestination .placeHolder').addClass('loading');
		$('#searchResultsDestination').data('renderCruiseResults').options.dbURL = '/async/shorexBox/portKey/'+$("#PortKey").val()+'/index.html';
		$('#searchResultsDestination').data('renderCruiseResults').fetchData();
		page = 1;
		paginate(page, 'inc');
	}).delegate('.sorexBox .packageAnchor', 'click', function(e) {
		e.preventDefault();
		$('#searchResultsDestination .placeHolder').empty();
		$('#searchResultsDestination .placeHolder').addClass('loading');
		$('#searchResultsDestination').data('renderCruiseResults').options.dbURL = '/async/shorexBox/portKey/'+$("#PortKey").val()+'/index.html';
		$('#searchResultsDestination').data('renderCruiseResults').fetchData();
		page = 1;
		paginate(page, 'inc');
	});

	function paginate(page, opt) {
		var start = (page == 1) ? 0 : (page * items_per_page) - items_per_page;
		var pages = Math.ceil($('#hiddenDiv .col').length / items_per_page);
		var new_content = $('#hiddenDiv .col').slice(start, (start + items_per_page)).clone();
		$('#searchResultsDestination .placeHolder').removeClass('loading');
		$('#searchResultsDestination .placeHolder').empty().append(new_content);
		$('#searchResultsDestination .placeHolder').find('.col').animate({
			'opacity': 0
		}, 100).animate({
			'opacity': 1
		}, 900);

		if(page + 1 > pages && pages > 1) {
			$('.nextResults').hide();
			$('.prevResults').show();
		} else if(page - 1 == 0 && pages > 1) {
			$('.nextResults').show();
			$('.prevResults').hide();
		} else {
			if(pages > 1) {
				$('.nextResults').show();
				$('.prevResults').show();
			}
		}
	}


}); // END HEAD


