
$(document).ready(function(){
	 
	var sum = 0;
	var price = 0;
	
	$(".grey option:selected").each(function(index, value) { 
		if($(value).val()!=0) {
			price = parseFloat($(value).val()*$(this).data("price"));
			sum = parseFloat(sum) + price;
			
			$(value).parent().css("background-color","#7BD17B");
			
		}
	});
	TotalSum = $("#nights").val()*sum;
	TotalSum1 = TotalSum.toFixed(2);
	$("#total-pirce").html(TotalSum1);
	$("#charge_total").val(TotalSum1);
	
	
	$('.grey').change(function() {
		sum = 0 ;
		price = 0;
		$(".grey option:selected").each(function(index, value) { 
			if($(value).val()!=0) {
				price = parseFloat($(value).val()*$(this).data("price"));
				sum = parseFloat(sum) + price;
				
				$(value).parent().css("background-color","#7BD17B");
			}
		});
		TotalSum = $("#nights").val()*sum;
		TotalSum1 = TotalSum.toFixed(2);
		$("#total-pirce").html(TotalSum1);
		$("#charge_total").val(TotalSum1);
	});
	

	$("#make-reservation").click(function(){
		
		$("#reservation-form").submit();
		
	});
	
	

	$('a[rel=popover]').popover({
		  html: true,
		  trigger: 'hover',
		  placement: 'left',
		  content: function(){
		
			  return '<img src="'+$(this).data('img') + '" />';

			  }
		});
		
	
	
	
	
});
