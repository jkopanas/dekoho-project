head.ready(function(){
	
	
 var db = new mcms.dataStore({
         method: "POST",
         url:"",
         //dataType: "json",
         trigger:"loadComplete"
 });
	
	
	 var ret = true;
	 var ret_user = true;
	 var user_mail = true;
	 var ret_mail = true;
	 var ret_mail1 = true;
	 
	 
	 var validatable = $("#htl-login").kendoValidator({
		   rules: {
	       
			 verifyPasswords: function(input){
	           
	              if (input.is("[name=confirm_password]")) {
	                   ret = input.val() === $("#pass").val();
	                }
	                 return ret;
	        },	
	        
	        verifyMails: function(input){
		           
	              if (input.is("[name=confirm_email]")) {
	                   ret_mail = input.val() === $("#email").val();
	                }
	                 return ret_mail;
	        },
	        
	        
	        /*
	        checkbox: function(input) {
	            if (input.filter("[name=accept_terms]").length) {             
	               return input.parent().find("[name=accept_terms]:checked").length >= 1;
	            }
	            return true;
	          },
	       */

			
			checkemail: function(input){

	            if (input.is("[name=email]")) {
	            	var email = $("#email").val();
	            	db.url="/ajax/loader.php?file=user_functions.php&action=checkEmail";
	    	 		db.trigger ='check_mail';
	    	 	    db.data = {email:email} 
	    	 	    $(db).loadStore(db);
	    	 	   
	            }
	            return user_mail;
			}
			
	    },
	    messages: {
	    	checkemail: function(input){
	    		if (input.is("[name=email]")) {
	    		  return "This email is already used";
	    		} 
	    	},
	    	
	    	required: function(input){
	    		//return $.lang.the_field+" "+input.attr("for")+" "+$.lang.is_required;
	    		return "Field "+input.attr("for")+" is required";
	    	}
	    }
	}).data("kendoValidator");
	 
	 
 $("#create-account").click(function(e) {
	
		e.preventDefault();
	
		if (validatable.validate()) 
		
		{ 	 
		    db.url="/ajax/loader.php?file=usersFront.php&action=saveHotelier";
	 		db.trigger ='add_hotelier';
	 	    db.data = {data:mcms.formData('.userData',{ getData:false})} 
	    	$(db).loadStore(db);
	 	   $("#ca-form").find(".form-signin").hide();
			$("#ca-form").append("<br/><br/>Our system sent you an email confirmation.Please, confirm and complete the “Create Account” process.<br/><br/>In case there is no confirmation email in your inbox, please, check your spam folder.<br/><br/> Thank you,<br/> <img src='/images/site/logo.jpg' width='150'/>");
	   }		
 });
	
	
 $(db).bind('add_hotelier',function(){
		records = db.records;
		records = $.parseJSON(records);	
		
  });	
	
	
	
	
	$(db).bind('check_mail',function(){
		
		records = db.records;
		
		 if (records == 'false') 
		 {
			 user_mail=false; 
		 } else 
		 { 
			user_mail=true;
		 }	 
		 
	});

	
	
	
	$("#remind_password").click(function(e){
		e.preventDefault();

		$("#the_login").hide();
		
		$("#show_password").show();
		
		
	}); 
	
	
	
	 $("#back-to-login").click(function(){
		 
		 $("#the_login").show();
			
			$("#show_password").hide();
		 
	 });
	
	
	$("#password-reminder").click(function(e){
	
		e.preventDefault();
		
	});
	
	
}); // end headready