var sliders = {};
var x = {};
head.ready(function(){
var frontLang = $('input[name=loaded_language]').val();
points = new $.dataStore({
    method: "POST",
    url:"/getItemMarkers.html",
    dataType: "json",
	trigger:"loadComplete"
});

$(points).bind('loadComplete',function(){
	records = points.records;
	$.each(records.items, function(key, val) {
		 x.createMarker({ category:'moreItems', icon:'/images/icons/pin1_trans.png',map:x.obj,position : new google.maps.LatLng(val.lat,val.lng),infoWindow: { content : '<a href="/'+ frontLang +'/listing/'+val.item.id+'/'+val.item.permalink+'.html">' + val.item.title
		 + '<br> ' + val.distance + 'km'}} );
	});
	
});
$(points).bind('requestSaved',function(){
	$("#requestForm").hide('fast');
	$("#thankYouMsg").show();
	setTimeout (function() {$.fancybox.close();}, 5000);
});

var validatable = $("#requestForm").kendoValidator().data("kendoValidator");

      $("#sendRequest").click(function() {
         if (validatable.validate()) {
         	
            points.data = {'var' : new $.FormData($('.filter'),{'ReturnType':'array'}) };
            points.url = '/index.html';
            points.trigger = 'requestSaved';
            points.dataType = '';
            $(points).loadStore(points);
         }
      });
            function startChange() {
                        var startDate = start.value();

                        if (startDate) {
                            startDate = new Date(startDate);
                            startDate.setDate(startDate.getDate() + 1);
                            end.min(startDate);
                        }
                    }

                    function endChange() {
                        var endDate = end.value();
                        var startDate = start.value();

                        if (endDate) {
                            endDate = new Date(endDate);
                            startDate = new Date(startDate);
                            endDate.setDate(endDate.getDate() - 1);
                            start.max(endDate);
                        }
                    }
var start = $("#startDate").kendoDatePicker({
                        change: startChange,
                        format: "dd/MM/yyyy"
                    }).data("kendoDatePicker");
         var end = $("#endDate").kendoDatePicker({
                        change: endChange,
                         format: "dd/MM/yyyy"
                    }).data("kendoDatePicker");
$('#contactForm').fancybox({		
		width		: '800px',
		fitToView	: false,
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'});
	
	var obj = $.parseJSON($('#mapItemEncoded').val());
	
$('#mapItem').googleMaps({ zoom:parseInt(obj.zoomLevel),clat:obj.lat,clng:obj.lng,mapTypeId:obj.MapTypeId}).data('mapItem').init();	
 x = $('#mapItem').data('mapItem'); 
x.createMarker({ map:x.obj,position : new google.maps.LatLng(parseFloat(obj.lat),parseFloat(obj.lng)),icon:'/images/icons/pin2_trans.png'} );
points.data = {'module':'listings','data':$('#moreItems').val(),'lat':$('input[name=lat]').val(),'lng':$('input[name=lng]').val()};
$(points).loadStore(points);

	x.refreshMap();
$('#tabStrip').tabber({'galleryControl':'.tabber','map':x});

});//END HEAD

$.fn.tabber = function(options ) {  
	var tB = function(element, options) {
		var settings = $.extend( {
			'tabsContainer' : '#tabsContainer',
			'galleryControl' : '',
			'tabsClass' : '.tab'
		}, options);	
		
		var positionArrow = function(el) { 
			$('#activeArrow').css({'left':el.width()/2 + el.position().left});
		}
		
		
		var tC = $(settings.tabsContainer);
		var first = $(element).find('li > a:first');
		first.addClass('active');
		positionArrow(first);
		tC.find(settings.tabsClass+':first').show();
		
		if (settings.galleryControl){
			$('#'+ first.attr('rel')).parent().show();
			if (first.attr('data-type') == 'slider') {
				startSlides($('#'+ first.attr('data-target')));
			}
		}
		
		$(element).find('li > a').click(function(e) {
			e.preventDefault();
			$(settings.tabsClass).hide();

			$(this).parentsUntil('ul').parent().find('a').removeClass('active');
			$(this).addClass('active');
			positionArrow($(this));
			$(tC).find('#'+$(this).attr('rel')).show();
			pauseSliders();
			if (settings.galleryControl){
				$(settings.galleryControl).hide();
				$('#'+$(this).attr('rel')).show();
				if ($(this).attr('data-type') == 'slider'){
					$('#'+$(this).attr('data-target')).show();
					startSlides($('#'+$(this).attr('data-target')));
				}
				else if ($(this).attr('data-type') == 'map'){
					$('#'+$(this).attr('data-target')).show();
					settings.map.refreshMap();
				}
			}
			
		});

		var callbackHandler = function(func)
		{
		if (this[func])
		{
			window[func].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else { console.log(func + ' is not registered'); }
		}
	    
	};//END CLASS
    return this.each(function() {   

    var element = $(this);
       // Return early if this element already has a plugin instance
   if (element.data($(this).attr('id'))) return;
       
	var c = new tB(this,options);	    
	element.data($(this).attr('id'), c);  
});

};

function pauseSliders() {
	for (var i in sliders) {
		jQuery(sliders[i]).cameraPause();
	}
}

function startSlides(el) {
if (el.attr('id') in sliders) { 

	jQuery(sliders[el.attr('id')]).cameraResume();
	return;
}
if (el.length){
sliders[el.attr('id')] = el;
jQuery(el).camera({
	thumbnails: true,
	height:'325px',
	time: 3000
});
}

}