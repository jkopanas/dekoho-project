$(document).ready(function(){
	
	var title = $("#title").val().split("%");
	var description = $("#description").val().split("%");
	var lat = $("#lat").val().split("%");
	var lng = $("#lng").val().split("%");	
	var image = $("#image").val().split("%");

    if($.isArray(lat)) {
			$('#map').googleMaps({
				zoom: parseInt($("#hotelzoomLevel").val()),
				clat: $("#hotellat").val(),
				clng: $("#hotellng").val(),
				marker_icon: "/images/icons/pin1_trans.png",
				createMarker: {
							infoWindow: {
								content: '<span style="color:#09394E">'+$("#hotelgeocoderAddress").val()+'</span>'
							}
				}
			}).data('map').init();
			
			x = $('#map').data('map');
		
		    $.each(lat,function(index,value){
		        x.createMarker({
		            category: 'moreItems',
		            map: x.obj,
		            icon: image[index],
		            position: new google.maps.LatLng(lat[index], lng[index]),
		           // title: value.item.title,
		            infoWindow: {
		                content: '<span class="loc-title">'+title[index]+'</span><div class="seperator-small"></div><span class="loc-desc">'+description[index]+'</span>'
		            }
		        });
		    });
    }		
	
	
});