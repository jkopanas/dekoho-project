var markerIcons = {
    selected : '/images/icons/pin2_trans.png',
    'default' : '/images/icons/pin1_trans.png'
};

mcms.directive("googleMap",function ($log, $timeout,$filter,gMaps,$rootScope) {
    return {
        restrict: "EC",
        priority: 100,
        transclude: true,
        template: "<div class='angular-google-map' ng-transclude></div>",
        replace: false,
    /*        scope: {
                showItem: "=showItem", // required
                center: "=center", // required
                markers: "=markers", // optional
                latitude: "=latitude", // required
                longitude: "=longitude", // required
                zoom: "=zoom", // optional, default 8
                refresh: "&refresh", // optional
                windows: "=windows" // optional"
            },*/
        controller: function ($scope, $element) {
            $scope._m = $scope.map;

            self.addInfoWindow = function (lat, lng, content) {
                $scope._m.addInfoWindow(lat, lng, content);
            };
        },
        link: function (scope, element, attrs, ctrl) {

            angular.element(element).addClass("angular-google-map");
            scope._m = {};
            scope.$watch('center', function (value) {
                if (value){
                    scope._m = new gMaps({
                        container: element[0],
                        center: new google.maps.LatLng(scope.center.lat,scope.center.lng),
                        draggable: attrs.draggable == "true",
                        zoom: scope.zoom,
                        settings:attrs
                    });

                    // Done, draw the map
                    scope._m.draw();

                    // Put the map into the scope
                    scope.map = scope._m;

                    scope._m.on("zoom_changed", function () {
                        if (scope.zoom != scope._m.zoom) {

                            $timeout(function () {
                                scope.zoom = scope._m.zoom;
//                                console.log(scope._m.getBounds());
                            });
                        }
                    });//END ZOOM EVENT

                    scope._m.on("bounds_changed", function () {
                        $timeout(function () {
                            if (!scope._m.dragging) {
//                                console.log(scope._m.getBounds());
                            }

                        },200);
                    });

                }//END VALUE
            });//END WATCH



            scope.$watch("refresh()", function (newValue, oldValue) {
                if (newValue) {
                    scope._m.draw();
                }
            });

            $rootScope.$watch("infoWindowClosed", function (value) {
                if (value) {
                    scope.highlightMarker();
                    scope.infoWindowClosed = false;
                }
            });

            $rootScope.highlightMarker = function(item){
                if (typeof scope.selectedMarker != 'undefined' && scope.selectedMarker == item){
                    return;
                }
                else if (typeof scope.selectedMarker == 'undefined' || !scope.selectedMarker){
                    scope.selectedMarker = item;
                    scope.items.data[item].marker.setIcon(markerIcons.selected);
                }
                else if (typeof item == 'undefined'){
                    scope.items.data[scope.selectedMarker].marker.setIcon(markerIcons['default']);
                    scope.selectedMarker = false;
                }
                else {
                    scope.items.data[scope.selectedMarker].marker.setIcon(markerIcons['default']);
                    scope.selectedMarker = item;
                    scope.items.data[item].marker.setIcon(markerIcons.selected);
                }

            };


            scope.$watch("markers", function (newValue, oldValue) {
                if (newValue.length >0){

                    if ($rootScope.clearMarkers){
                        scope._m.clearMarkers();
                        $rootScope.clearMarkers = false;
                    }
                   for (var a in newValue){

                      var marker = scope._m.addMarker(newValue[a].lat,newValue[a].lng,{
                           extra:{
                               itemid:newValue[a].id
                           },
                          icon: markerIcons['default'],
                           listeners:{
                               click:function(){
                                   //GET SINGLE HOTEL
                                   scope.showItem(this.extra.itemid);
                                   $rootScope.highlightMarker(this.extra.itemid);
                               }
                           }
                       });
                       scope.items.data[newValue[a].id]['marker'] = marker;
                       scope.items.data[newValue[a].id]['markerInfo'] = newValue[a];
                   }
                    scope._m.mc.setMaxZoom(16);
                    scope._m.mc.addMarkers(scope._m.getMarkerInstances());

                    if (attrs.fit == "true" && newValue.length > 1) {
                        scope._m.fit();
                    }
                }
            });


        }//END LINK
    }
});

mcms.directive('showMiniMap', function($timeout,$filter,gMaps,$rootScope) {//input:checkbox, input:radio, input:file
    return {
        restrict: "EC",
        priority: 100,
        transclude: true,
        template: "<div class='angular-google-map' ng-transclude></div>",
        link: function(scope, element, iAttrs) {
            function drawMap(){

                var zoom = (typeof scope.items.data[iAttrs.id].markerInfo == 'undefined') ? 14 : scope.items.data[iAttrs.id].markerInfo.zoomLevel;
                var map = new gMaps({
                    container: element[0],
                    draggable: iAttrs.draggable == "true",
                    center: new google.maps.LatLng(scope.items.data[iAttrs.id].markerInfo.lat,scope.items.data[iAttrs.id].markerInfo.lng),
                    zoom: zoom,
                    settings:iAttrs
                });


                // Done, draw the map
                $timeout(function(){

                    map.draw();
                    var marker = map.addMarker(scope.items.data[iAttrs.id].markerInfo.lat,scope.items.data[iAttrs.id].markerInfo.lng,{
                        extra:{
                            itemid:iAttrs.id
                        },
                        icon: markerIcons['default']
                    });
                    map.refresh();
                });
            }

                scope.$watch("selectedItem", function (value) {
                element.html('');
                if (value == iAttrs.id) {

                    if (typeof scope.items.data[iAttrs.id].markerInfo == 'undefined'){

                        scope.fetchMapData().then(function(res){
                             $timeout(function(){
                                drawMap();
                            });

                        });
                    }
                    else {
                        drawMap();
                    }
                }
            });
        }
    }
});


mcms.directive('selectPin', function($timeout) {//input:checkbox, input:radio, input:file
    return function(scope, iElement, iAttrs) {
        iElement.on('click',function(e){
            e.preventDefault();
            if (!scope.mapVisible){//in case it is closed, wait for the controller
                scope.showMap().then(function(){
                   $timeout(function(){
                       select();
                   });

                });
            }
            else {
                select();

            }
            function select(){
                scope._m.refresh();
                scope._m.mc.setMaxZoom(8);
                var zoom = (typeof scope.items.data[iAttrs.selectPin].markerInfo.zoomLevel == 'undefined') ? 8 : scope.items.data[iAttrs.selectPin].markerInfo.zoomLevel;
                scope._m.getInstance().setZoom(zoom);
                scope._m.getInstance().panTo(scope.items.data[iAttrs.selectPin].marker.getPosition());
                google.maps.event.trigger(scope.items.data[iAttrs.selectPin].marker, 'click');
                $('html, body').animate({scrollTop:0}, 'fast');

            }

        })

    };
});


mcms.directive('replaceInput', function($timeout) {//input:checkbox, input:radio, input:file
    return function(scope, iElement, iAttrs) {
        iElement.uniform();
    };
});

var regionsLang = {
    city:'Cities',
    region: 'Regions',
    country: 'Countries'
    },resultCache ={}, processResults = function(data){
    var arr = [],u=0;
    for (var a in data){
        if (data[a].length>0){
            var t = data[a].length;
            for (var p=0;t>p;p++){
                var icon = (p==0) ? true : false;
                var first = (p==0 && u ==0) ? true : false;
                arr.push({
                    label: data[a][p].title,
                    value: data[a][p].title,
                    module:data[a][p].module,
                    first:first,
                    icon:icon,
                    id:data[a][p].id
                })
            }

            u++;
        }
    }
    console.log(arr);
    return arr;
};

mcms.directive('suggestCity', function($timeout,$http) {//input:checkbox, input:radio, input:file
    return function(scope, iElement, iAttrs) {


        iElement.autocomplete({
            source: function(request, response) {
                var q = iElement.val();
                if (q in resultCache){
                    return response(resultCache[q]);
                }
                $http.post(urls.suggestCity, $.param({text:q})).
                    success(function(data, status) {
                        var res = processResults(data);
                        resultCache[q] = res;
                        response(res);

                    });
            },
            minLength: 2,
            select: function( event, ui ) {
                scope.filters.destination = ui.item.value;
                scope.filters.destinationType = ui.item.module;
                scope.filters.destinationID = ui.item.id;

            },
            focus: function(){return false;}
        }).data( "uiAutocomplete" )._renderItem = function( ul, item ) {
            var f  =(item.icon && !item.first) ? '<li style="border-top: 1px solid #CCC;"></li>' : '<li></li>';
            var b  =(item.icon) ? "<a><span class='autocomplete_header icon_"+item.module+"'>"+regionsLang[item.module]+"</span>" + item.label  + "</a>" : "<a>"  + item.label+ "</a>";
            return $( f)
                .data( "item.autocomplete", item )
                .append(b)
                .appendTo( ul );
        };
    };
});

var chkDates = function(arrivalObj,departureObj,scope){
    var valid = false,ret ={},arrival = scope.filters.arrival,departure = scope.filters.departure;
    if (arrival == '' || departure == ''){//no dates. Set them
        valid = false;
    }

    var arrivalParts = arrival.split("-"),departureParts = departure.split("-"), now = new Date();
    var arrivalDate = new Date(arrivalParts[2], (arrivalParts[1] - 1), arrivalParts[0]),
        departureDate = new Date(departureParts[2], (departureParts[1] - 1), departureParts[0]);

    if (arrivalDate > departureDate){
        valid = false;
    }
    else if (arrivalDate > now){
        valid = true;
    }
    else {
        arrivalDate = now;
        var month = (arrivalDate.getMonth() < 10) ? '0'+String(arrivalDate.getMonth()+1) : String(arrivalDate.getMonth()+1);
        var day = (arrivalDate.getDate() < 10) ? '0'+String(arrivalDate.getDate()) : String(arrivalDate.getDate());
        arrival = day+'-'+month+'-'+arrivalDate.getFullYear();
        valid = false;
    }

    if (departureDate > now && arrivalDate < departureDate){
        valid = true;
    } else {
        valid = false;
    }

    if (valid == true){
        scope.filters.arrival = arrival;
        scope.filters.departure = departure;
        return;
    }

    //invalid

    var dD = departureObj.datepicker("getDate");
    var aD = arrivalObj.datepicker("getDate");
    var minDepartureDate = new Date();

     if (arrivalDate > departureDate){
         minDepartureDate = new Date(arrivalDate.getFullYear(),arrivalDate.getMonth(),arrivalDate.getDate()+2);
         departureObj.datepicker('option', 'minDate', minDepartureDate);
         arrivalObj.datepicker('option', 'minDate', arrivalDate);
    }
    else {
        dD = new Date();
        dD.setDate(dD.getDate());
        minDepartureDate.setDate(minDepartureDate.getDate() + 1);
    }

    var d = (arrivalDate.getDate() < 10) ? '0'+String(arrivalDate.getDate()) : String(arrivalDate.getDate());
    var m = (arrivalDate.getMonth() < 10) ? '0'+String(arrivalDate.getMonth()+1) : String(arrivalDate.getMonth()+1);
    var y = arrivalDate.getFullYear();
    var de =  d+'-'+m+'-'+y;


    var d = (minDepartureDate.getDate() < 10) ? '0'+String(minDepartureDate.getDate()) : String(minDepartureDate.getDate());
    var m = (minDepartureDate.getMonth() < 10) ? '0'+String(minDepartureDate.getMonth()+1) : String(minDepartureDate.getMonth()+1);
    var y = minDepartureDate.getFullYear();
    var a =  d+'-'+m+'-'+y;

    scope.filters.arrival = de;
    scope.filters.departure = a;


}

function checkdate(input){
    var validformat=/^\d{2}-\d{2}-\d{4}$/; //Basic check for format validity
    var returnval=false,
        now = new Date();

    if (!validformat.test(input)){
        returnval=false;
    }
    else{ //Detailed check for valid date ranges
        var monthfield=input.split("-")[1]
        var dayfield=input.split("-")[0]
        var yearfield=input.split("-")[2]
        var dayobj = new Date(yearfield, monthfield-1, dayfield)
        if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
            returnval=false;
        else {
            returnval=true;
        }
    }
    return returnval;
}

var date_objs ={},budget_objs={};
mcms.directive('datePicker', function($timeout) {//input:checkbox, input:radio, input:file
    return {
        require: 'ngModel',
        link: function(scope, iElement, iAttrs, ctrl) {
            date_objs[iAttrs.dest] = iElement;
            scope.$watch('appStarted', function (value) {//ensure render
                if (value){

//                    DatePicked(date_objs['arrival'],date_objs['departure'],'',scope);
                    chkDates($(date_objs['arrival']),$(date_objs['departure']),scope);
                    iElement.datepicker({
                        autoSize: true,
                        constrainInput: true,
                        dateFormat: 'dd-mm-yy',
                        minDate: 0,
                        maxDate: '+90D',
                        onSelect: function(dateText, inst) {
                            scope.filters[iAttrs.dest] = iElement.val();
                            chkDates($(date_objs['arrival']),$(date_objs['departure']),scope);
                            scope.$apply();
                        }
                    });


                    ctrl.$parsers.unshift(function(viewValue) {
                        if(checkdate(viewValue)) {
                            var arrivalParts = scope.filters.arrival.split("-"),departureParts = scope.filters.departure.split("-"), now = new Date();
                            var arrivalDate = new Date(arrivalParts[2], (arrivalParts[1] - 1), arrivalParts[0]),
                                departureDate = new Date(departureParts[2], (departureParts[1] - 1), departureParts[0]);
                            if (arrivalDate < departureDate){
                                ctrl.$setValidity('datePicker', true);
                                scope.errors.departSmall =false;
                                return viewValue;
                            }
                            else {
                                scope.errors.departSmall =true;
                            }
                        }
                        else {
                            ctrl.$setValidity('datePicker', false);
                            return '';
                        }
                    });
                }
            });
        }
    }
});

var INTEGER_REGEXP = /^\-?\d*$/;

mcms.directive('integer', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            budget_objs[attrs.dest] = elm;
            ctrl.$parsers.unshift(function(viewValue) {
                if (INTEGER_REGEXP.test(viewValue)) {

                    if (attrs.dest == 'to' && parseInt(elm.val()) < parseInt(budget_objs['from'].val())){
                        ctrl.$setValidity('integer', false);
                        return undefined;
                    }
                    ctrl.$setValidity('integer', true);
                    return Math.abs(viewValue);
                } else {
                    ctrl.$setValidity('integer', false);
                    return undefined;
                }
            });
        }
    };
});

mcms.directive('loadingMessage', function() {
    return {
        restrict: 'A',
        transclude: true,
        priority: 1e3,
        template: '<div>\n  <div class="overlay-container" ng-show="message">\n    <div class="overlay-text" ng-bind="message"></div>\n  </div>\n  <div ng-transclude></div>\n</div>',
        link: function ($scope, $el, attrs) {
            $scope.$watch('loading', function (value) {//ensure render

                if (value){

                    $scope.message = $scope.loadingMessage || 'Searching...';
                } else {
                    $scope.message = '';
                }
            });
        }
    };
});

mcms.directive('ngIf', function() {
    return {
        priority: 500,
        restrict: 'A',
        transclude: 'element',
        compile: function(element, attrs, transcludeFn) {
            var watchExpr = attrs.ngIf;

            return function(scope, element) {
                var selectedElement,
                    selectedScope;

                scope.$watch(watchExpr, function(value) {
                    if (selectedElement) {
                        selectedScope.$destroy();
                        selectedElement.remove();
                        selectedElement = selectedScope = null;
                    }

                    if (value && transcludeFn) {
                        selectedScope = scope.$new();
                        transcludeFn(selectedScope, function(transcludeElement) {
                            selectedElement = transcludeElement;
                            element.after(transcludeElement);
                        });
                    };
                });
            };
        }
    };
});