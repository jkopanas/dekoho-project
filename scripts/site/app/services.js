'use strict';
/* Services */

angular.module('mcmsServices', []).
    factory('xhr', ['$rootScope','$http','$q', function($rootScope,$http,$q){
        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        return {
            boot:function(params){
                var items = [];
                params.lang = $rootScope.lang;

                var defer = $q.defer();
                params = (typeof params != 'undefined') ? params : {};
                $http.post(urls.boot,$.param(params)).
                    success(function(data, status) {
                        defer.resolve(data);
                        $rootScope.init = data;
                    });
                return defer.promise;
            },
            query: function(filters,order){
                var defer = $q.defer();
                order = (typeof order != 'undefined') ? order : {};
                $http.post(urls.query, $.param({token:$rootScope.init.token,lang:$rootScope.lang,filters:filters,order:order,time:$rootScope.time})).
                    success(function(data, status) {
                        defer.resolve(data);
                    });
                return defer.promise;

            },
            mapItems: function(filters,params){
                var defer = $q.defer();
                params = (typeof params != 'undefined') ? params : {};
                $http.post(urls.mapItems, $.param({token:$rootScope.init.token,lang:$rootScope.lang,ids:filters,params:params})).
                    success(function(data, status) {
                        defer.resolve(data);
                    });
                return defer.promise;
            },
            roulette: function(filters,params){
                var defer = $q.defer();

                $http.post(urls.roulette, {}).
                    success(function(data, status) {
                        defer.resolve(data);
                    });
                return defer.promise;
            }
        }
    }]).factory('parseUrl', ['$rootScope','$location', function($rootScope,$location){
        return {
            parseAction : function(tag,ret) {
                var p = tag.match(/[#!/]+([a-zA-Z]+)/i,'');
                if (p != null) {
                    if (ret) { return p[1]; }
                    return p;//JUST THE ACTION
                }
            },
            parseParams : function(string,map) {
                var e = this.parseAction(string,1);
                var params = string.match(/[^/]+/g),prev ='';
                if (params != null) {
                    var x = {},p = params.length;
                    for (var k=0;p>k;k++){
                          if (k%2 ==0) {
                            x[params[k]] = params[k];
                            prev = params[k];
                        }
                        else {
                              if (typeof map[x[prev]] != 'undefined' && map[x[prev]].type == 'multiple'){
                                  var tmp = params[k].split(',');
                                  x[prev] =  (tmp.length > 0) ? tmp : [params[k]];
                              }
                              else {
                                  x[prev] = params[k];
                              }
                        }

                        if (x[prev].indexOf(',') != -1){
                            x[prev] = x[prev].split(',');//ARRAY
                        }
                    }
                    return x;
                }
            },

            parse : function(tag,options) {
//                var settings = $.extend(this.options,options);
                var settings = {};
                var ret = {};
                var o = this.parseAction(tag);
                if (o != null) {
                    ret['action'] = o[1];
                    ret['params'] = this.parseParams(tag.replace(o[0],''),options);
                }
                if (settings.callBack) {
                    this.callbackHandler(settings.callBack,ret);
                }

                return ret;
            },
            callbackHandler : function (func)
            {
                if (this[func])
                {
                    this[func].apply(this, Array.prototype.slice.call(arguments, 1));
                }
                else { console.log(func + ' is not registered'); }
            },
            is_numeric:function(mixed_var){
                return (typeof(mixed_var) === 'number' || typeof(mixed_var) === 'string') && mixed_var !== '' && !isNaN(mixed_var);
            },
            rebuildUrl: function(src,dest){

            }
        }

    }]);

function currencyTrans(exchange,price) {
	var newprice=parseFloat(exchange)*price;
	return newprice.toFixed(2);
}



function paginate(count,results_per_page,page){
    var regFinal = results_per_page * page;
    var regInicial = regFinal - results_per_page;
    var tmp = {};
    tmp.page = page;
    tmp.total = Math.ceil(count/results_per_page);
    tmp.next = parseInt(page) + 1;
    tmp.prev = parseInt(page) - 1;
    if (regInicial == 0){
        regInicial++;
    }
    if (page == tmp.total){
        regFinal = count;
    }
    if (page > 1){
        regInicial++;
    }
    tmp.from = regInicial;
    tmp.to = regFinal;

    var totalRecordsControl = count;
    if ((totalRecordsControl%results_per_page!=0)){
        while(totalRecordsControl%results_per_page!=0){
            totalRecordsControl++;
        }
    }

    var ultimo = String(page).substr(-1),begin=0,end=0,pageInicial=0;
    tmp.pages = new Array;
    if (ultimo == 0){
        begin = (page-9);
        pageInicial = (page - ultimo);
        end = page;
    }
    else{
        pageInicial = (page - ultimo);
        begin = (page-ultimo)+1;
        end = pageInicial+10;
    }
    var num = tmp.total;
    if (end>num){
        end = num;
    }
    for (var a = begin; a <= end ; a++){
        tmp.pages.push(a);
    }
    return tmp;
}