/*
Used with angular.
Just the core code to be invoked within a directive

 */
"use strict";
angular.module('google-maps', []).
    factory('gMaps', function($rootScope){

    var MapModel = (function () {

        var _defaults = {
            zoom: 8,
            draggable: false,
            container: null
        };

        function PrivateMapModel(opts) {

            var _instance = null,
                _markers = [],	// caches the instances of google.maps.Marker
                _handlers = [], // event handlers
                _windows = [],  // InfoWindow objects
                o = angular.extend({}, _defaults, opts),
                that = this,
                _mc ={};

            this.center = opts.center;
            this.zoom = o.zoom;
            this.draggable = o.draggable;
            this.dragging = false;
            this.selector = o.container;
            this.markers = [];

            this.draw = function () {

                if (that.center == null) {
                    // TODO log error
                    return;
                }

                if (_instance != null) {

                    // Refresh the existing instance
                    _instance.setCenter(that.center);
                    _instance.setZoom(that.zoom);
                    google.maps.event.trigger(_instance, "resize");
                } else {

                    // Create a new map instance

                    _instance = new google.maps.Map(that.selector, {
                        center:that.center,
                        zoom:that.zoom,
                        draggable:that.draggable,
                        mapTypeId:google.maps.MapTypeId.ROADMAP
                    });

                    if (typeof MarkerClusterer != 'undefined' && o.settings.usecluster) {
                        that.mc = new MarkerClusterer(_instance, []);
                    }

                    google.maps.event.addListener(_instance, "dragstart",

                        function () {
                            that.dragging = true;
                        }
                    );

                    google.maps.event.addListener(_instance, "dragend",

                        function () {
//                            that.dragging = false;
                        }
                    );

                    google.maps.event.addListener(_instance, "idle",

                        function () {
                            that.dragging = false;
                        }
                    );

                    google.maps.event.addListener(_instance, "drag",

                        function () {
                            that.dragging = true;
                        }
                    );

                    google.maps.event.addListener(_instance, "zoom_changed",
                        function () {
                            that.zoom = _instance.getZoom();
                        }
                    );

                    google.maps.event.addListener(_instance, "center_changed",

                        function () {
                            that.center = _instance.getCenter();
                        }
                    );
                    // Attach additional event listeners if needed
                    if (_handlers.length) {
                        angular.forEach(_handlers, function (h, i) {

                            google.maps.event.addListener(_instance,
                                h.on, h.handler);
                        });
                    }
                }

                // TODO is this really needed to open each info windows?
                angular.forEach(_windows, function (w, i) {
                    w.open(_instance);
                });
            };

            this.fit = function () {
                if (_instance && _markers.length) {
                    var c = _instance.getCenter();
                    google.maps.event.trigger(_instance,'resize');

                    var bounds = new google.maps.LatLngBounds();

                    angular.forEach(_markers, function (m, i) {
                        bounds.extend(m.getPosition());
                    });

                    _instance.setZoom( _instance.getZoom() );

                    _instance.setCenter(c);
                    _instance.fitBounds(bounds);

                }
            };

            this.refresh = function(){
                var c = _instance.getCenter();
                google.maps.event.trigger(_instance,'resize');
                _instance.setCenter(c);
            }

            this.on = function(event, handler) {
                _handlers.push({
                    "on": event,
                    "handler": handler
                });
                google.maps.event.addListener(_instance, event, handler);
            };

            this.addListeners = function(){

            }

            this.addMarker = function (lat, lng, options) {

                if (that.findMarker(lat, lng) != null) {
                    return;
                }

                var marker = new google.maps.Marker(angular.extend({}, {
                    position: new google.maps.LatLng(lat, lng),
                    map: _instance
                }, options));

                // Cache marker
                _markers.unshift(marker);

                // Cache instance of our marker for scope purposes
                that.markers.unshift({
                    "lat": lat,
                    "lng": lng,
                    "draggable": false,
                    "label": options.label,
                    "url": options.url,
                    "thumbnail": options.thumbnail
                });
                if (options.listeners){
                    for (var a in options.listeners){
                        google.maps.event.addListener(marker, a, options.listeners[a]);
                    }
                }

                // Return marker instance
                return marker;
            };

            this.findMarker = function (lat, lng) {
                for (var i = 0; i < _markers.length; i++) {
                    var pos = _markers[i].getPosition();

                    if (pos.lat() == lat && pos.lng() == lng) {
                        return _markers[i];
                    }
                }

                return null;
            };

            this.findMarkerIndex = function (lat, lng) {
                for (var i = 0; i < _markers.length; i++) {
                    var pos = _markers[i].getPosition();

                    if (pos.lat() == lat && pos.lng() == lng) {
                        return i;
                    }
                }

                return -1;
            };

            this.addInfoWindow = function (lat, lng, html) {
                var win = new google.maps.InfoWindow({
                    content: html,
                    position: new google.maps.LatLng(lat, lng)
                });

                _windows.push(win);

                return win;
            };

            this.hasMarker = function (lat, lng) {
                return that.findMarker(lat, lng) !== null;
            };

            this.getMarkerInstances = function () {
                return _markers;
            };

            this.clearMarkers = function () {
                _markers = [];
                that.markers =[];
                that.mc.clearMarkers();
            }
            this.removeMarkers = function (markerInstances) {

                var s = this;

                angular.forEach(markerInstances, function (v, i) {
                    var pos = v.getPosition(),
                        lat = pos.lat(),
                        lng = pos.lng(),
                        index = s.findMarkerIndex(lat, lng);

                    // Remove from local arrays
                    _markers.splice(index, 1);
                    s.markers.splice(index, 1);

                    // Remove from map
                    v.setMap(null);
                });
            };

            this.getBounds = function(){
                return _instance.getBounds();
            }

            this.getInstance = function(){
                return _instance;
            }
        }

        // Done
        return PrivateMapModel;
    }());

        return MapModel;
});//END FACTORY