'use strict';
var baseUrl = '/ajax/search.php';
var urls = {
    boot : baseUrl+'?action=boot',
    query : baseUrl+'?action=search',
    mapItems:baseUrl+'?action=map',
    suggestCity:baseUrl+'?action=suggest',
    nearby:baseUrl+'?action=nearby',
    roulette:baseUrl+'?action=roulette'
},appData={};

var mcms = angular.module('mcms', ['mcmsServices','google-maps']).run( function($rootScope, $location,$http) {
    $rootScope.lang = $('input[name=loaded_language]').val();
    $rootScope.exchange = $('input[name=exchange]').val();
});