'use strict';
var globalSettings ={},cache={};
/* Controllers */
mcms.controller('mainCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {


}]);

mcms.controller('searchCtrl', ['$scope','$http','$location', '$rootScope','xhr','parseUrl','$q','$timeout', function($scope, $http,$location,$rootScope,xhr,parseUrl,$q,$timeout) {
    $scope.errors = {},$scope.filters={};
    $scope.orderBy = {field:'price',dir:'ASC'};
    $scope.markers = [];
    $scope.appliedFilters = [];
    $scope.params = $location.search();
    var tmp = $location.absUrl().match(/([a-zA-Z]{2})\/search/i,'');
    $scope.code = tmp[1];
    if (typeof $scope.params.destination == 'undefined'){
        $scope.error = 'noDestination';
        $rootScope.$broadcast('noDestination');
        window.location.href = '/index.html';
        return;
    }

    if (typeof $scope.params.field != 'undefined'){
        $scope.orderBy.field = $scope.params.field;
    }
    if (typeof $scope.params.dir != 'undefined'){
        $scope.orderBy.dir = $scope.params.dir;
    }



    if (typeof $rootScope.init == 'undefined'){

        $scope.predicate = '';
        var g = xhr.boot($scope.params).then(function(res){
            $scope.results_per_page = 10;
            $scope.token = res.token;
            $scope.defaultRooms = {room:'1',persons:2};
            //check for rooms
            var count = 0;
            $scope.roomsNum = [];
            for (var a in $scope.params){

            //	if (a.indexOf('budget_from') != -1 || a.indexOf('budget_to') != -1 ) {
            //		$scope.params[a] = ( $scope.params[a] != "") ? currencyTrans($rootScope.exchange,$scope.params[a]) : "" ;
           // 	}

                if (a.indexOf('room-') != -1){
                    var o = $scope.defaultRooms;

                    if (typeof $scope.params['persons-'+a.replace('room-','')] != 'undefined'){
                        o.persons = $scope.params['persons-'+a.replace('room-','')];
                        $scope.filters['persons-'+a.replace('room-','')] = o.persons;
                    }

                    $scope.roomsNum.push(o);
                    count++;
                }
            }

            $scope.params.roomsNum = count;
            if (count ==0){
                $scope.roomsNum.push($scope.defaultRooms);
            }


            $scope.stars = [1,2,3,4,5].reverse();
            res.map.push({
                name:'stars',
                prefix:'stars_',
                type:'multiple',
                fields:['num']
            });
            $scope.createModel('stars');

            $scope.fields = res.fields;
            $scope.fieldMapTmp = res.map;

            $scope.fieldMap = {},$scope.fieldMapValue={};
            for (var o in $scope.fieldMapTmp){
                $scope.fieldMap[$scope.fieldMapTmp[o].name] = $scope.fieldMapTmp[o];
            }

            for (var a in $scope.fields){
                $scope.createModel(a);
            }

            $scope.filters = $.extend($scope.filters,$scope.params);

            for (var t in $scope.filters){
                if (typeof $scope.fieldMap[t] != 'undefined' && $scope.fieldMap[t].type == 'multiple'){

                    if ($scope.filters[t].length > 0){
                        $scope.filters[t] = (String($scope.filters[t]).indexOf(",") != -1) ? $scope.filters[t].split(',') : [$scope.filters[t]];
                    }
                }
            }

            $scope.appStarted = true;

            var time = Math.round(new Date().getTime() / 1000);
            var init =false;
            $scope.filters.page = (typeof $scope.params.page == 'undefined') ? 1 : $scope.params.page;
            if ($scope.params != '' && typeof $scope.params.page != 'undefined'){
                init = true;
            }
            else {
                init = false;
            }

            if (typeof $scope.params.mode == 'undefined'){
                $scope.filters.mode = 'normal';
            }

            $scope.firstBoot = true;
            $scope.calcPersons();

            $scope.filter(init);



        });
    }

    $scope.calcPersons = function(){
        var it = 1;
        $scope.personsTotal =0;
        for (var i=0;$scope.filters.roomsNum>i;i++){
            $scope.personsTotal = $scope.personsTotal + parseInt($scope.filters['persons-'+i]);
        }
    }

    $scope.moreRooms = function(el){
        var current = $scope.roomsNum.length;

        if ($scope.filters.roomsNum > current) {//add rooms
            var diff = $scope.filters.roomsNum - current;
            for (var i=0;diff>i;i++){
                $scope.filters['persons-'+(current)] = $scope.defaultRooms.persons;
                $scope.roomsNum.push($scope.defaultRooms);
                current = current + 1;
            }
        }
        else {
            var diff = current - $scope.filters.roomsNum;
            for (var i=0;diff>i;i++){
                delete $scope.filters['room-'+(current-1)];
                delete $scope.filters['persons-'+(current-1)];
                $scope.roomsNum.splice($scope.defaultRooms,1);
                current = current-1;
            }
        }

    }

    $scope.resetFilters = function(){
        for (var i=0;$scope.fieldMapTmp.length > i;i++){
            $scope.filters[$scope.fieldMapTmp[i]['name']] = [];
        }
        delete $scope.filters.nearby;
        $scope.filter();
    }

    $scope.formUrl = function(){
        var tmp = {},tmp1 =[];
        for (var t in $scope.filters){
            if ($scope.filters[t].length > 0){
                tmp[t] = $scope.filters[t];
                tmp1.push(t+'='+$scope.filters[t]);
            }
            else if (typeof $scope.filters[t] == 'number'){
                tmp[t] = $scope.filters[t];
                tmp1.push(t+'='+$scope.filters[t]);
            }
        }
        tmp.field =$scope.orderBy.field;
        tmp.dir =$scope.orderBy.dir;
        tmp1.push('field='+$scope.orderBy.field);
        tmp1.push('dir='+$scope.orderBy.dir);
        $scope.hotelUrl = tmp1.join('&');
        $location.search(tmp);
    }

    $scope.order = function(filter){
        if (filter == $scope.orderBy.field){
            $scope.orderBy.dir = ($scope.orderBy.dir =='ASC') ? 'DESC' :'ASC';
        }
        $scope.orderBy.field = filter;

        $scope.filter();
    }

    $scope.filter = function(init,mode){

        $scope.formUrl();

        $scope.loading = true;
        $scope.calcPersons();

        if (typeof init == 'undefined'){
            $scope.filters.page = 1;
            $rootScope.clearMarkers = true;
        }

        $scope.filters.exchange=parseFloat($rootScope.exchange).toFixed(2);

        var p = xhr.query($scope.filters,$scope.orderBy).then(function(res){

            if (mode == 'append'){
                for (var a in res.data){
                    $scope.items.data[a] = res.data[a];
                }
            }
            else {
                $scope.res = [];
                $scope.items = res;
            }
            for (var a in res.data) {
            	res.data[a].price=currencyTrans($rootScope.exchange,res.data[a].price);
                $scope.res.push(res.data[a]);
            }



            if (typeof $scope.fetchComplete == 'undefined'){
                $scope.firstLoad = true;
            }
            else {
                $scope.firstLoad = false;
            }

            for (var a in res.fields){
                for (var b in res.fields[a]){
                    $scope.fields[a][b].count = res.fields[a][b].count;
                }
            }
            $scope.loading = false;
            var page = ($scope.filters.page) ? $scope.filters.page : 1;
            $scope.pagination = paginate(res.count,$scope.results_per_page,page);


            $scope.fetchComplete = new Date();
            if ($scope.mapVisible){
                clearMapSelection();
                $scope.addMarkers(res.data);
            }
            if ($scope.firstBoot){
                if ($scope.params.showMap) {
                    $scope.showMap();
                }
                $scope.firstBoot = false;
            }
            if (typeof _gaq !== "undefined" && _gaq !== null) {
                _gaq.push(['_trackPageview', location.pathname+location.search+location.hash]);
            }
        });
    }


    $scope.loadMore = function() {
       if ($scope.filters.page < $scope.pagination.total){
           $scope.filters.page++;
           $scope.filter(true,'append');
       }
    };

    $scope.mode = function(mode){
        $scope.filters.mode = mode;
        if (mode == 'roulete'){
            xhr.roulette().then(function(res){
               $scope.filters.destination = res.destination;
               $scope.filters.destinationType = res.destinationType;
               $scope.filters.destinationID = res.id;
               delete $scope.filters.discount;
               $scope.filter();
            });
        }
        else if (mode == 'offers'){
            $scope.filters.discount = true;
            $scope.filter();
        }
        else {
            delete $scope.filters.discount;
            $scope.filter();
        }
    }

    $scope.nearBy = function(itemid){
        if (itemid == $scope.filters.nearby){
           delete $scope.filters.nearby;
           $scope.predicate = '';
        }
        else {
            $scope.predicate = '-order';
            $scope.filters.nearby = itemid;
        }

        $scope.filter();
    }

    $scope.showItem = function(item){

        $scope.itemShown = true;
        $scope.item = $scope.items.data[item];
        $scope.selectedItem = item;
        $scope.$apply();
    }

    $scope.closeInfoWindow = function(){
        $scope.itemShown = false;
        $scope.selectedItem = false;
        $rootScope.highlightMarker()
    }

    function clearMapSelection(){
        $scope.markers =[];
        $scope.selectedItem = false;
        $scope.selectedMarker = false;
        $rootScope.clearMarkers = true;
    }

    $scope.showMap = function(state){
        if (state){//hide the map
            $scope.mapVisible = false;
            clearMapSelection();
            delete $scope.filters.showMap;
            $scope.formUrl();
            return;
        }

        var defer = $q.defer();//defer it so that the directive can wait for completion

        if (!$scope.center){
            console.log($scope.init);
            $scope.center = {
                lat: $scope.init.mapInfo['lat'],
                lng: $scope.init.mapInfo['lng']
            };
            $scope.zoom = $scope.init.mapInfo.zoomLevel;
            $scope.geolocationAvailable = navigator.geolocation ? true : false;
        }

        $scope.mapVisible = true;
        $scope.addMarkers($scope.items.data);
        $scope.filters.showMap = 1;
        $scope.formUrl();
        defer.resolve(true);
        return defer.promise;
    }

    $scope.fetchMapData = function(){
        var defer = $q.defer();//defer it so that the directive can wait for completion
        var items = [];

        for (var a in $scope.res){
            items.push($scope.res[a].id);
        }
        xhr.mapItems(items).then(function(res){
            for (var a in res.items){
                $scope.items.data[res.items[a].id]['markerInfo'] = res.items[a];
            }
            defer.resolve(res.items);
        });

        return defer.promise;
    }


    $scope.addMarkers = function(m){
        var items = [];
//console.log(m)
        for (var a in m){
            items.push(m[a]['id']);
        }

        var p = xhr.mapItems(items).then(function(res){
            $scope.markers =[];
            $scope.markers = res.items;
        });

    }

    $scope.changePage = function(page){
        $scope.filters.page = page;
        $rootScope.clearMarkers = true;
        $scope.filter(true);
    }

    $scope.isChecked = function(item,model){

        if ($scope.filters[model].indexOf(item) !== -1){
            return true;
        }

        return false;
    };

    $scope.chkModel = function(me,model){
        $scope.appliedFilters.push(model);
        if ($scope.filters[model].indexOf(me) == -1){
            $scope.filters[model].push(me);
        }
        else {
            $scope.filters[model].splice($scope.filters[model].indexOf(me),1);
        }
    }

    $scope.unsetFilter = function(item,model){
        $scope.appliedFilters.splice($scope.appliedFilters.indexOf(model),1);
        $scope.filters[model].splice($scope.filters[model].indexOf(item),1);
        $scope.filter();
    }

    $scope.miniMap = function(item){
        $scope.selectedItem = item;
    }

    $scope.createModel = function(name){

        if (typeof $scope.filters == 'undefined') {
            $scope.filters = {};
        }
        $scope.filters[name] = new Array;
 /*       if (typeof $scope.fields[name] != 'undefined'){
            for (var p=0;$scope.fields[name].length>p;p++){
                $scope.filters[name].push($scope.fields[name][p].id);
            }
        }*/

    }
}]);
