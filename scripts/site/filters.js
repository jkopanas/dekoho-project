var x = {};
var lists = {};
head.ready(function(){
var frontLang = $('input[name=loaded_language]').val();
var lang = $('input[name=loaded_language]').val();
db = new $.dataStore({	 
    method: "POST",
    url:"/"+ lang + '/applyFilter.html',
//	dataType: "json",
	trigger:"loadComplete"
});
var minPrice = parseInt($('input[name=minPrice]').val());
var maxPrice = parseInt($('input[name=maxPrice]').val());

updatePriceRange(minPrice,maxPrice);

var rangeSlider = $("#priceRange").kendoRangeSlider({
	min: minPrice,
	max:maxPrice,
	selectionStart :minPrice,
	selectionEnd :maxPrice,
 	smallStep: 1,
    largeStep: 100,
    change: sliderOnChange,
    tooltip:{'enabled':true},
     change: function(e) {
     	var state = (e.values[0] != minPrice || e.values[1] != maxPrice) ? 'true' : 'false';
     	$('input[name=minPrice]').val(e.values[0]).attr('data-enabled',state);
     	$('input[name=maxPrice]').val(e.values[1]).attr('data-enabled',state);
     	updatePriceRange(e.values[0],e.values[1]);
     	submitFields();	
     }
}).data("kendoRangeSlider");

$('.drillDown').drillDownList({'module':$('input[name=module]').val()});

$("#panelBar").kendoPanelBar();

		$('.mSelect').kendoDropDownList().data("kendoDropDownList");


$(db).bind('loadComplete',function(){				
	records = db.records;
	
  	$('#loadingMessage').hide();
  	$('#items').show();	
  	drawData(records);
});

	var obj = $.parseJSON($('textarea[name=fomatedItemsRaw]').val());
	$('#map').googleMaps({ zoom:parseInt(obj.map.zoomLevel),clat:obj.map.lat,clng:obj.map.lng}).data('map').init();
	x = $('#map').data('map');

$(x).bind('populateMap',function(){
	if (!x.items){
		var obj = $.parseJSON($('textarea[name=fomatedItemsRaw]').val());
	}
	else {
		var obj = x.items;
		x.destroyMarkers();
		
	}
	if (obj.items) {
		$.each(obj.items, function(key, val) {
			 x.createMarker({ category:'moreItems', icon:'/images/icons/pin1_trans.png',map:x.obj,position : new google.maps.LatLng(val.lat,val.lng),infoWindow: { content : 
			 '<a href="/'+ frontLang +'/listing/'+val.item.id+'/'+val.item.permalink+'.html">' + val.item.title +'</a><br><a href="#item'+ val.itemid+ '" class="goToItem">'+ $.lang.more +'</a>'}} );
		});
	}
	x.refreshMap();
});
	

$(".goToItem").live('click', function(e){  
e.preventDefault();
$('html,body').animate({scrollTop:$($(this).attr('href')).offset().top}, 500);
});

$("input.efieldFilter").live('click', function(e){  
submitFields();	
});

$("select.efieldFilter, select.mSelect").live('change', function(e){  
submitFields();	
});

$(".paginateFilter").live('click', function(e){  
submitFields($(this).attr('rel'));	
});




$(".closeMap").live('click', function(e){  
$(this).parent().hide();
$('.gMap').parent().show();
});

$(".gMap").live('click', function(e){  



$('#map').parent().show();
$(x).trigger('populateMap');
$(this).parent().hide();
});

$(".multiSelect").live('click', function(e){  
$('#multiSelect-methods-content-'+$(this).attr('data-id')).show();
});


	$('html').click(function(event) {
	    if($(event.target).parentsUntil('div').parent().hasClass('multiSelectContent') == false) {
	       $('.multiSelectContent').hide();
	    }        

	});

$(".commonFilter, .breadCrumb,.commonAll").live('click', function(e){  	
	$('input[name=locationFilter]').val($(this).attr('data-val'));
	submitFields();	
});



});//END HEAD


function submitFields(page) {
	var el = $(this).parent();
	var p = (page) ? page : 1;
	$('#items').hide();
	$('#loadingMessage').show();
	db.data = {'page':p,'sort':new $.FormData($('.sort'),{'ReturnType':'array'}),'data' : new $.FormData($('input.post'),{'ReturnType':'array'}),'price' : new $.FormData($('.price[data-enabled=true]'),{'ReturnType':'array'}),
	'filter': $('input[name=locationFilter]').val(),'categoryid': $('select[name=categoryid]').val(), 'efields' : new $.FormData($('.efieldFilter'),{'ReturnType':'array'})};

	$(db).loadStore(db);
	
}

function drawData(data)
{
//	$('.debugArea').html(data);
var u =   new Array;
var a = {};

	var obj = jQuery.parseJSON(data);
	
//	console.log(obj)
	$('.efieldFilter').each(function() {
		u.push($(this).attr('id'));
	});
	var d = {};
	
if(obj.efields){
	$.each(obj.efields, function(key, value) { 
		$.each(value.data, function(k, v) {
			if (v.type == 'radio'){
				$('#total-'+v.id).html(v.values[0].total);
				a[v.id] = k;
				d[v.id] = k;
			}
			else {
				$.each(v.values, function(b, n) {
					$('#total-'+n.id).html(n.value + '('+n.total+')');
					a[n.id] = k;
					d[n.id] = k;
				});
			}

//				console.log('found' + v.id);

		});

//		$('#'+value.data.id)
	});
}

	var i=0;
	for (i=0;u.length > i;i++){
		if (u[i] in d) {
			$('#'+u[i]).parentsUntil('li').parent().find('input').attr('disabled',false);
		}
		else{
			$('#'+u[i]).parentsUntil('li').parent().find('input').attr('disabled','disabled');
			$('#total-'+u[i]).html('0');
		}
			
	}
			
	if (obj.map) {
		var markers ='';
		if (obj.map.items){
			$.each(obj.map.items, function(key, v) { 
				markers += '&markers=icon:http://simplecrete.com/images/icons/pin1_trans.png|label:S|'+v.lat+','+v.lng;
			})
		}
		$('#gMap').attr('src','http://maps.googleapis.com/maps/api/staticmap?center='+obj.map.lat+','+obj.map.lng+'&zoom=6&size=215x215&sensor=false'+markers)
	}
	
	$('#filtersList').find('li').remove();
	
/*	if (obj.filters != 'null')
	{
		var filters = jQuery.parseJSON(obj.filters);
		$.each(filters, function(key, value) { 
			if (value.type == 'efield')
			{
				var t = (value.translation) ? value.translation : value.value;
				$('#filtersList').append('<li><div>'+ value.field +' : ' + t + '<span><a href="#" class="removeFilter efield" rel="'+ key + '">'+ $.lang.remove + '</a></span></div></li>');
			}
			else {
				$('#filtersList').append('<li><div>'+ $.lang.location +' : ' + value.field + '<span><a href="#" class="removeFilter link" rel="'+ key + '">'+ $.lang.remove + '</a></span></div></li>');
			}
		});//END FOREACH
	}*/


//	$('#extraFieldsFilters > ul#filters').html(obj.efields);

/*	if (obj.commonCategories) {
		$.each(obj.commonCategories, function(k, v) { //UPDATE THE SELECT WITH CHILDREN VALUES
			console.log($('select[data-tableID=' +k+'] option[value!=""]'))
			$('select[data-tableID=' +k+'] option[value!=""]').remove();
			var tmp ='';
			$.each(v, function(key, val) {
				tmp += '<option value ="'+val.val+'">'+ val.item.category +'</option>';
			});
			$('select[data-tableID=' +k+']').append(tmp);
			lists[$('select[data-tableID=' +k+']').attr('id')].refresh();
		});
	}*/


if(obj.itemsCount > 0) {
	$('#items').html(obj.items);
	obj.from = obj.list.from;
	obj.to = obj.list.to;
}
else{ 
	$('#items').html($('#noItems').html());
	obj.from = 0;
	obj.to = 0;
}

if (obj.commonTree) {
	$.each(obj.commonTree, function(key, val) {
		var me = $('div.drillDown[data-table='+key+']');
		var n =  me.drillDownList().data(me.attr('id'));
		$('ul[data-table='+key+']').children().remove();	
		var m = n.drawList(val.items);
		$(m).appendTo($('ul[data-table='+key+']'));
		n.drawBreadCrumb(val.nav);
	});
}


$('.itemsCount').html(obj.itemsCount);
$('.itemsFrom').html(obj.from);
$('.itemsTo').html(obj.to); 

//	$('#locations').html(obj.locations);
//	$('#appliedFilters').show();
	x.items = obj.map;
	$(x).trigger('populateMap');
}

function sliderOnChange(e) {
	updatePriceRange(e.values[0],e.values[1]);
}

function updatePriceRange(min,max) {
	$('#priceRanges span.min').html(min);
	$('#priceRanges span.max').html(max);
}


$.fn.drillDownList = function(options) {
	var dD = function(element, options) {
		var settings = $.extend( {
		'module' : 'listings',
		'clickCallback' : '',
		'obj' : {}
		}, options);
		
	var view = $(element).find('ul.view');
	var breadCrumb = $(element).find('.breadCrumbs');
	
	
    var callbackHandler = function(func)
	{
		if (this[func])
		{
			window[func].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else { console.log(func + ' is not registered'); }
	}
	

	

	this.drawList = function(obj) {
		var l ='';
		$.each(obj, function(k, v) {
			l += '<li><a href="#" data-val="'+v.link+'" class="commonFilter">'+v.category+' (<span class="totals">'+v.num_items+'</span>)</a>'; 
			if (v.num_sub > 0) {
				l += '<a href="#" data-val="'+v.link+'" data-text="'+v.category+'" class="subs"> <img src="/images/breadcrumbs_arrow.png"  /></a>';
			}
			l += '</li>';	
		});

		return l;
	}//END DRAW
	
	this.drawBreadCrumb = function(nav) {
		breadCrumb.children().find('.breadCrumb').parent().remove();
		var c = '';
		if (nav) {
			$.each(nav, function(k, v) {
				c += '<li><span class="breadCrumb"><img src="/images/breadcrumbs_arrow.png"  /></span></li>';
				c += '<li><a href="#" data-val="'+v.link+'" class="breadCrumb">'+v.category+'</a>';
			});
			$(c).appendTo(breadCrumb);
		}
	}
	
	
	};//END CLASS

return this.each(function() {

	var element = $(this);
	// Return early if this element already has a plugin instance
	if (element.data($(this).attr('id'))) return;
	var c = new dD(this,options);
	element.data($(this).attr('id'), c);
});
};