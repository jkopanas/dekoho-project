head.ready(function(){

$('.products-grid .item').hover(function(){
$(this).find('.product-price').parent().show();
}, function(){
$(this).find('.product-price').parent().hide();

});
var cartDialogTemplate = kendo.template($("#cartDialogTemplate").text());

var cartDialog = $("#cartDialog");
if (!cartDialog.data("kendoWindow")) {
    cartDialog.kendoWindow({
        width: "490px",
        title: 'Added to cart',
        visible:false,
        modal:true
    });
}

eshop = new mcms.dataStore({
    method: "POST",
    url:"/userCart.html",
    dataType: "json",
	trigger:"loadComplete"
});
	
$(eshop).bind('addToCart',function(){
	records = eshop.records;
	updateCart(records);
	cartDialog.html(cartDialogTemplate(records));
	console.log(cartDialogTemplate(records));
    cartDialog.data("kendoWindow").open();
    cartDialog.data("kendoWindow").center();
});
	
$(".addToCart").click(function(e){
	e.preventDefault();
	eshop.trigger = 'addToCart';
	eshop.data = { id :  $(this).attr('rel'), action : 'addToCart',module:'products'}
	$(eshop).loadStore(eshop);
});//END FUNCTION
	

$(".closeModal").live("click", function() {
	cartDialog.data("kendoWindow").close();
});


$(".remove-item").click(function(){
  if (confirm($.lang.removeFromCart)) {
				var itemid = $(this).attr('rel');
				var placer = new Array();
				placer[0] = '.total-price';
				placer[1] = '#total-'+itemid;
				placer[2] = '.total-products';
				placer[3] = '.total-price-no-vat';
				placer[4] = '.vat-price';
				save_quantity('/userCart.html',{'mode':'removeItem','itemid':itemid},placer);
				$(this).parent().parent().remove();
  }
});//END EMPTY CART

$(".empty_cart").click(function(){
location.href = '/empty_cart.html';					
});//END EMPTY CART

$(".payment_method").live("click", function() {
	$('.payment_method_div').hide();
	$('.fields').hide();
	$(".shipping_method").attr('checked','');
	$('#payment_method_div'+$(this).val()).show();
});


$(".shipping_method").live("click", function() {
	$('.fields').hide();
	$('#fields-'+$(this).val()).show();
});

$("#full-form").live("submit", function(e) {
	e.preventDefault();
	console.log(mcms.formData($('.save'),{ getData:true}))
});

});//END HEAD



function updateCart(obj) {
	if ($('.cartItems').hasClass('hidden')) { $('.cartItems').removeClass('hidden'); }
	$('.total-items').html(obj.total_quantities);
	$('.total-price').html(obj.formatedPrice);
}