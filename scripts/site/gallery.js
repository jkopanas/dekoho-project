(function ($, undefined) {
      var delay = (function(){
      var timer = 0;
      return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
      };
    })();
$.imgGallery = function(obj,options) {
    var obj = obj, options = options,templates = {},cache={},app={};
	if (typeof(obj) !== 'object') obj = $(obj)[0];
	if (typeof(options) !== 'object') options = { };

	var mainImg = $(obj).find('.mainImg');
        var app = {
	options:options,
 	_trigger: function( type, data ) {
		data = data || {};
		if ($.isFunction(this.options[type])){
			eval(this.options[type]).apply(this, Array.prototype.slice.call(arguments, 1));
		}
	}
}//END APP

$(obj).on('click','.thumbs a.img',function(e){
	e.preventDefault();
	$(mainImg).find('.loader').show();
	$(mainImg).find('.img').animate({'opacity':0},100);
	var img = new Image();
    img.onload = function() {
    	$(mainImg).find('.loader').hide();
    	$(mainImg).find('.img').attr('src',img.src).animate({'opacity':1},200);
    };
    img.src = $(this).attr('href');
	
        if ($(this).attr('data-title') == "" ) {
            $(mainImg).find('p').hide();
        } else {
            $(mainImg).find('p').show();
        }
        $(mainImg).find('.descriptionText').text($(this).attr('data-title'));
});


/*
$(obj).find('.thumbs').on({

 mouseenter: function () {
 	$(this).find('div').addClass('hovered');
 	//$(this).css('height',$(this).find('div').height()+'px')
//stuff to do on mouseover

}, //notice the comma..

  mouseleave: function () {
//$(this).css('height','').addClass('origHeight');
$(this).find('div').removeClass('hovered');
  //stuff to do on mouseleave
	
}

  });
*/

app._trigger('onAppLoaded',this);
return app;
};//END CLASS

$.fn.imgGallery = function(options) {
if (typeof(options) !== 'object') options = { };

// Iterate over each object, attach Jcrop
this.each(function()
{

    if ($(this).data('imgGallery')){
		return $(this).data('imgGallery');
	}
    else {
        $(this).data('imgGallery',$.imgGallery($(this),options))
    }
});
}//END PLUGIN
})(jQuery);


$(document).ready(function(){
	 
	$('.imgGallery').imgGallery();
	
});
