head.ready(function(){
var validatable = $("#contactForm").kendoValidator().data("kendoValidator");
contactForm = new mcms.dataStore({
    method: "POST",
    url:"/index.html",
    dataType: "",
	trigger:"submitContact"
});
var obj = $.parseJSON($('#mapItemEncoded').val());
$('#contactMap').googleMaps({ zoom:parseInt(obj.zoomLevel),clat:obj.lat,clng:obj.lng,mapTypeId:obj.MapTypeId}).data('contactMap').init();	
 x = $('#contactMap').data('contactMap'); 
x.createMarker({ map:x.obj,position : new google.maps.LatLng(parseFloat(obj.lat),parseFloat(obj.lng)),icon:'/images/icons/pin2_trans.png',infoWindow: { content : obj.geocoderAddress} });

$(contactForm).bind('submitContact',function(){
	$("#saveNewsLetter").hide('fast');
	$("#emailSaved").show();
});

$(".submitContact").click(function() {
 if (validatable.validate()) {
 	
    contactForm.data = {'var' : new $.FormData($('.contactField'),{'ReturnType':'array'}) };
    $(contactForm).loadStore(contactForm);
 }
});
});