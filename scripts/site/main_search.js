var x;
$(document).ready(function(){
		
	var regionsLang = {
		    city:'Cities',
		    region: 'Regions',
		    country: 'Countries'
		    };
	
	var destination="";
	var destinationType="";
	var destinationID="";
	

	  $( "#destination" ).autocomplete({
			 source: function( request, response ) {
		            $.ajax({
		                url: "/ajax/search.php?action=suggest",
		                type:"POST",
		                dataType:"json",
		                data: {"text": $("#destination").val()},
		                  success: function(data) {
		                	  var res = processResults(data);
		                     
		                        response(res);
		                    
		                  }
		            });
			 },select: function( event, ui ) {
				    destination = ui.item.value;
				    destinationType = ui.item.module;
				    destinationID = ui.item.id;

	            }
			
		}).data( "uiAutocomplete" )._renderItem = function( ul, item ) {
            var f  =(item.icon && !item.first) ? '<li style="border-top: 1px solid #CCC;"></li>' : '<li></li>';
            var b  =(item.icon) ? "<a><span class='autocomplete_header icon_"+item.module+"'>"+regionsLang[item.module]+"</span>" + item.label  + "</a>" : "<a>"  + item.label+ "</a>";
            return $( f)
                .data( "item.autocomplete", item )
                .append(b)
                .appendTo( ul );
        };
	

	  $(function() {
		  $( "#arrival" ).datepicker({
		      dateFormat:"dd-mm-yy",
		      changeMonth: true,
		      minDate: new Date(),
		      onClose: function( TheDate ) {
		        $( "#departure" ).datepicker( "option", "minDate", TheDate );
		       
		      }
		    });
		    $( "#departure" ).datepicker({
		      dateFormat:"dd-mm-yy",
		      changeMonth: true,
		      onClose: function( TheDate ) {
		        $( "#arrival" ).datepicker( "option", "maxDate", TheDate );
		        $( "#arrival" ).datepicker( "option", "minDate", new Date() );
		      }
		    });
	  });
	  
	  var lat = "";
	  var lng = "";
	  var zoomlevel=2;
	  
	  
	  if($("#lat").val()!="" && $("#lng").val()!="" ) {
		  lat = $("#lat").val();
		  lng = $("#lng").val();
		  zoomlevel = 6;
	  }
	  
	  
	  $('#map').googleMaps({
		   zoom: zoomlevel,
		   clat: lat,
		   clng: lng,
		   marker_icon: "/images/icons/box-info.png"
		  
		}).data('map').init();
		x = $('#map').data('map');
	  
		 
		  x.createMarker({

              map: x.getMap(),
              infoWindow: {
            	  content:'<span style="color:#09394E">Where would you like to travel?</span>'
				},
              icon: "/images/icons/box-info.png",

              position: new google.maps.LatLng(lat,lng)
      });
		
		
		
	  
	  $('#rooms').change(function() {
		  var rooms = $('#rooms :selected').val();
		  
		 var counter = 1;
		 $(".all-gen-rooms").remove();
		 
		 for (i=1; i<rooms; i++)
			{
				counter=i+1;
				$('<div class="all-gen-rooms"><div class="map-filter-item" style="width:133px;"><span class="hidden-rooms">Room '+counter+'</span></div><div class="map-filter-item"><span>Persons</span><div class="seperator"></div><select name="persons" id="persons" class="grey selectrooms-persons persons-to-submit"><option value="1">1 Person</option><option value="2">2 Persons</option><option value="3">3 Persons</option><option value="4">4 Persons</option><option value="5">5 Persons</option><option value="6">6 Persons</option><option value="7">7 Persons</option></select></div><div class="seperator"></div></div>').appendTo('#the-more-rooms');
			}
		 //x.refreshMap();
	  });
	  
	  
		  $("#main_search_btn").live("click",function(){  
			  
				  var arr = [];
				  
			     var exchange=parseFloat($('input[name="exchange"]').val()).toFixed(2);

				  var arrival = $("#arrival").val();
				  var departure = $("#departure").val();
				  var  budget_from=$("#budgetfrom").val();
				  var  budget_to=$("#budgetto").val();
				  var roomsNum = 0;
				  
				  $(".persons-to-submit option:selected").each(function(index, value) { 
					  
					 arr.push($(value).val());
					 roomsNum +=1;
					 
				  });
				  
				  var persons="";
				  var counter_r=0;
				  
				  $(arr).each(function(index, value) {
				  
					  persons=persons+"room-"+counter_r+"=1&persons-"+counter_r+"="+value+"&";
					  
					  counter_r+=1;
				  });
				  
				  
				  
				
				  var budgetfrom ="";
				  var budgetto = "";
	
				  if(budget_from!="" && budget_to!="") {
					  
					  var budgetfrom =  "&budget_from="+budget_from;
					  var budgetto =  "&budget_to="+budget_to;
				  }
		
					  var mode="";
					 
					  if($("#special-offer").hasClass("active-search"))
					  {
						  var mode = "&discount=true&mode=offers";

						  
					  } else if($("#roulete").hasClass("active-search"))
					  {
						  var mode = "&mode=roulete";
					  }
				  
						if(mode=="&mode=roulete") {	
							
							destination=$("#city_name").val();
							 destinationType=$("#city_type").val();
							 destinationID=$("#city_id").val();
						}
					  
			

				if (destination!="" && arrival!="" && departure!="") {  
					  var url = "/"+$('#loaded_language').val()+"/search.html#?"+persons+"roomsNum="+roomsNum+"&arrival="+arrival+"&departure="+departure+"&lang=en&page=1&field=price&dir=ASC&destination="+destination+"&destinationType="+destinationType+"&destinationID="+destinationID+budgetfrom+budgetto+mode;
					  window.location = url;
			   } else {
				 $("#wrong_dates_dest").show().delay(3000).fadeOut('slow');
				   
				  
			   }
		 
	  });
	  
		  
		  $(".search-item").live("click",function(){
				
			  $(".search-menu").find("li").removeClass("active-search");
			  $(this).addClass("active-search"); 
			  
			  if($(this).attr("id")=="roulete"){
				  $("#remove-destination").hide();
				  
			  } else { $("#remove-destination").show();}
			  
		  });
		  
		

		  $('#destination').blur(function() {
			  
	  if(destinationID!="") {  
			  $.ajax({
	                url: "/ajax/main_search.php",
	                type:"POST",
	                dataType:"json",
	                data: {"destinationType": destinationType,"destinationID":destinationID},
	                  success: function(value) {
	                	  x.destroyMarkers();
	                	  x.getMap().setCenter(new google.maps.LatLng(value.lat, value.lng));
	                	 ///******//// 
		                	  if(destinationType=="country") {
		                		  var zoom = 3;
		                	  } else var zoom=10;
	                	  ///*****/////
	                	  x.getMap().setZoom(zoom),
	                	  x.createMarker({
                              map: x.getMap(),
                              infoWindow: {
              					content:'<span style="color:#09394E">Your travel location</span>'
              				},
                              icon: "/images/icons/box-info.png",
             
                              position: new google.maps.LatLng(value.lat, value.lng)
                      });
                         // x.getMap().createMarker.infoWindow.content ="will search for hotel in "+destination+ "("+destinationType+")" ;
                          
	                  }
                          
                          
                         
	              
	            });
			  }
			});
		 
		  
	
});
/*
infowindow = new google.maps.InfoWindow({content:"oti na nai"});

function setText(e){
	console.log(e,x);
	infowindow.open(x,this);
	
}
*/

function processResults(data){
    var arr = [],u=0;
    for (var a in data){
        if (data[a].length>0){
            var t = data[a].length;
            
            for (var p=0;t>p;p++){
                var icon = (p==0) ? true : false;
                var first = (p==0 && u ==0) ? true : false;
                arr.push({
                    label: data[a][p].title,
                    value: data[a][p].title,
                    module:data[a][p].module,
                    first:first,
                    icon:icon,
                    id:data[a][p].id
                })
            }

            u++;
        }
    }
    return arr;
}
