head.ready(function(){
/*  

 var autoComplete = $("#autoComplete").kendoAutoComplete({
 	dataTextField: "name",

  dataSource: {
  	  	type:'json',
   pageSize: 10, // limits result set
   transport: {
    read: 
 {
            url: "/ajax/extjs.php?action=autocomplete",
            dataType: "json",
            type:'POST'
        }
    
   }
  }
 }).data("kendoAutoComplete");
 
 autoComplete.popup.element.bind("click", function(e) {
   var index = $(e.currentTarget).index();
   var dataItem = autoComplete.dataSource.view()[index];
    
   console.log(dataItem);
});
	
      $("#grid").kendoGrid({
      	height: 250,
        sortable: true,
        pageable: true,

          columns:[
              {
                  field: "id",
                  title: "#ID"
              },
              {
              	field:'uname',
              	title:'Username',
              	template : '<a href="\\#">${ uname }</a>'
              },
              {
              	field:'user_name',
              	title:'Full name',
              	template : '<a href="\\#">${ user_name }</a>'
              }
              ],
               schema: {
             data: "data",
             total: "count",
            model: {
            fields: {
                id: { type: "number" },
                uname: { type: "string" },
            }
        }
         },
      dataSource: {
          transport: {
        read: {
            // the remote service url
            url: "/ajax/extjs.php?action=allUsers",

            // JSONP is required for cross-domain AJAX
            dataType: "json",
			type : 'post',
            // additional parameters sent to the remote service

            data: {
                code: "en"
            }
        }
    }	
      }
      });
*/

var validatable = $("#saveNewsLetter").kendoValidator().data("kendoValidator");
newsLetter = new $.dataStore({
    method: "POST",
    url:"/index.html",
    dataType: "json",
	trigger:"loadComplete"
});

$(newsLetter).bind('saveNewsLetter',function(){
	$("#saveNewsLetter").hide('fast');
	$("#emailSaved").show();
});

$("#saveEmail").click(function() {
 if (validatable.validate()) {
 	
    newsLetter.data = {'var' : new $.FormData($('.toPost'),{'ReturnType':'array'}) };
    newsLetter.url = '/index.html';
    newsLetter.trigger = 'saveNewsLetter';
    newsLetter.dataType = '';
    $(newsLetter).loadStore(newsLetter);
 }
});
});