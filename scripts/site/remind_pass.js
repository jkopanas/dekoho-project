head.ready(function() {

	var db = new mcms.dataStore({
        method: "POST",
        url:"",
        async:true,
        //dataType: "json",
        trigger:"loadCompletes"
    });
	


$("#password-reminder").click(function(e) {
	e.preventDefault();
	
	var email = $("#reset-email").val();
	
	if (validEmail(email)==true) {
	
	db.url="/ajax/loader.php?file=user_functions.php&action=remindPassowrd&email="+email;
		db.trigger ='remind-the-pass';
	    db.data = {"email":email} 
	$(db).loadStore(db); 
	} else {
		
		$("#hidden-msg").html("Not a valid e-mail.<div class='seperator'></div>").css("color","red").show().delay(3000).fadeOut('slow');
		 	
	}
});



$(db).bind('remind-the-pass',function(){	
	
	 var result = db.records;

	 if (result=="send") {
		 $("#hidden-msg").html("An e-mail has been sent to you with instruction on how to change your passowrd.<div class='seperator'></div>").css("color","#006ACC").show().delay(3000).fadeOut('slow');
		 
	 } else {
		 $("#hidden-msg").html("E-mail wasn't found in our Database.<div class='seperator'></div>").css("color","red").show().delay(3000).fadeOut('slow');
	}
});


function validEmail(e) {
    var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
    return String(e).search (filter) != -1;
}


});