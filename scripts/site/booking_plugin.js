(function ($, undefined) {
    
    $.Hotelier = function(obj,options) {
    	
        
        var defaults = {
            templatesFile : '/skin/modules/booking/booking_templates.tpl',
            
            dbDefaults : {
                method: "POST",
	         //   dataType: "json",
                trigger:"loadComplete"
            },
            dbURL : {
              
            },
            map : {
              
            	
            }
            
        };
     var options = $.extend(defaults,options);
     
    var db = new mcms.dataStore(options.dbDefaults);   

        /* read templates */
        if (typeof templates.main == 'undefined') {
           	$.ajax({
        	  url: options.templatesFile,
        	  async:false,
        	  success: function(data){
        	  	var o = $(data).find('script');
        	  	var p = o.length;
        	  	
        	  	for (p;p--;) {
        	  		templates[$(o[p]).attr('id')] = kendo.template($(o[p]).html());
        	  		messages[$(o[p]).attr('id')] = $(o[p]).data('msg');
                    templatesData[$(o[p]).attr('id')] = $(o[p]).data();
        	  	}
        	  },
        	  cache:false,
        	  dataType: 'html'
        	});
        }//END TEMPLATES
        /* METHODS AND STUFF */
        app = {
            db: db,
        	ExecFunc:"",
        	wnd: $("#windowpopup").kendoWindow({width: "619",height: "388",content: "",title: false,visible: false,modal:true}).data("kendoWindow"),
        	validator:$("#myDiv").kendoValidator(options.validate).data("kendoValidator"),
        	clientdata: {},	
        	ActiveTemplates:  new Array(),
        	CurrentTemplate: "",
        	CurrentSource: "",
        	CurrentData: "",
            options:options,
            delay: delay,
            templatesData: templatesData,
            messages:messages,
            templates:templates,
     	 	_trigger: function( type, data ) {
    	 		data = data || {};
    	 		
    	 		if ($.isFunction(this.options[type])){
    	 			eval(this.options[type]).apply(this, Array.prototype.slice.call(arguments, 1));
    	 		}
	       	},
        _switchTab: function(e) {
    		
	   },
       addModal: function(mssg,template){
            var msg = (mssg == null) ? options.messages.modalMessage : mssg;

            $('#modal').find(".inner").html(this.templates[template]({"message": mssg}));
            $('#modal').center();
            $('#modal-back,#modal').show();
        },
        removeModal: function(){
            delay(function(){
                $('#'+options.modalName+ ',#modal').hide();
             }, 300  );
           
        }        
        };//END APP
      
        app.drawResults = function (templID,data) {

            if (!app.templatesData[templID].skip) {
                if ($("#HeaderBr"+db.templid).length) {      
                    $(".UpdateHeader").hide();
                    $(".blueHeader span").css("color","#1B4F77")
        	        $("#HeaderBr"+db.templid).show();
                    $("#HeaderBr"+db.templid).css("color","#fff");
                    $("#HeaderBr"+db.templid).parents("span:first").css("color","#fff");
                }
        	$("#"+this.CurrentSource).find("div.main").hide();
    		$("#"+this.CurrentSource).append(app.templates[templID]({"clientdata":this.clientdata,"app": app,"data":data}));

        	}
        } //end function drawResults
        
        
        app.ExecuteFunction = function(res) {
        	
        	var res=window[this.ExecFunc](res);
        	 this.ExecFunc="";
        	 return res;
        	 
        } //end function ExecuteFunction
        
        
        
        app.Redraw = function (templID,obj) {

        	data=this.Filter(obj);
        	
        	
        	$("#"+templID).find("div.main").show();
        	
        	$("#"+templID).html(app.templates[templID]({"clientdata":this.clientdata,"app": app,"data":data}));
        	
        } // end function Redraw
        
        
        $(db).bind('loadComplete',function(){
        	
    		var res = db.records;
    		results=$.parseJSON(res);
    		results.clientdata = app.clientdata;
    		
            if (!plugin.templatesData[db.templid].skip) {
    		if(window[app.ExecFunc]) 
    		{	
    			var results = app.ExecuteFunction(results);
    		}
    		    app.drawResults(db.templid,results);
                app.removeModal();
                app.wnd.close();
            } else {
                app.ExecuteFunction(results);
            }
    	
        }); // end bind 
        
        
        app.Filter = function(obj) {
        	
        	var NewData=[];
        	
        	$.each(obj.searchdata,function(k,a) {
        		
        	    if(obj.search==a[obj.field].charAt(0)) 
        	    {
        	       a.MAXprice=obj.searchdata.MAXprice;
        	       NewData.push(a);  
        	    }
        	
        	});

          return NewData;
        
        } // end function Filter
        
       
       app.RenderWindow = function(template,obj,enable) {

    	   $("#windowpopup").html(this.templates[template](obj));
           this.wnd.open().center();
    	  // this.wnd.wrapper.find(".k-header").remove();

            $("#windowpopup").show();
            
       } // end function RenderWindow

       
        app.RenderPage = function(templ,source){

        	app.CurrentTemplate = templ;
        	app.CurrentSource = source;
        	db.templid = templ;
        	

            if(app.validator.validate()) {
                if(!this.clientdata[templ])
        	    {
        	   	   this.clientdata[templ]=mcms.formData('.UserData',{ getData:false});
        	    }
        	    db.data=this.clientdata[templ];
                db.trigger="loadComplete";
                if (this.messages[this.CurrentTemplate]) {
                    app.addModal(this.messages[this.CurrentTemplate],"loadingimg");
                }

                    delay(function(){
                        db.url="/ajax/loader.php?file=booking/send_request.php&template_id="+templ;
                        $(db).loadStore(db);        
                    }, 5  );
        	}	

        } // end function RenderPage
        
        app.SetCurrentData = function(data)       
        {	
        	this.CurrentData = data;
        	
        } // end function SetCurrentData
        
         
        app.RenderPagePrevious = function(templ,source){
        
        	$("#"+source).find("div#"+app.CurrentTemplate).remove();
        	app.CurrentTemplate = templ;
        	$("#"+source).find("div#"+templ).show();
        	
        } // end function RenderPagePrevious
    	
		
          
          //Attach to the container
         // obj.append(templates['test'](app));
        //app.RenderPage("test","myDiv");
       // app._trigger('onAppLoaded',this);
        
        return app;
   };//END CLASS
 
 
 $.fn.booking = function(options) {
	
	 if (typeof(options) !== 'object') options = { };

	this.each(function()
	{
    
	    if ($(this).data('booking'))
		{ 
			return $(this).data('booking');
		}
        else {

             $(this).data('booking',$.booking($(this),options))
        }
	    
    });
}//END PLUGIN
 
})(jQuery);

