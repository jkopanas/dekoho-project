
	
head.ready(function(){
	
	
	var colors = ['#1E90FF', '#FF1493', '#32CD32', '#FF8C00', '#4B0082'];
	var selectedColor;
	var colorButtons = {};
	var m =[];

	
	
	$.maps = {};//WILL HOLD A LIST WITH ALL THE MAPS
	var res = {};
	var mycolor = "#ff0066";
	var mycolor2 = "#966E7E";
	var mybg_color = "#000000";
	 
	var cluster_styles = [{
	    url: 'images/m3.png',
	    height: 30,
	    width: 30,
	    opt_textSize: 14,
	    anchor: [3, 0],
	    textColor: '#222222'
	}, {
	    url: 'images/m4.png',
	    height: 40,
	    width: 40,
	    opt_textSize: 17,
	    opt_anchor: [6, 0],
	    opt_textColor: '#222222'
	}, {
	    url: 'images/m5.png',
	    width: 50,
	    height: 50,
	    opt_textSize: 21,
	    opt_anchor: [8, 0],
	    opt_textColor: '#222222'
	}, {
	    url: 'images/m5.png',
	    width: 50,
	    height: 50,
	    opt_textSize: 21,
	    opt_anchor: [8, 0],
	    opt_textColor: '#222222'
	}];
 
	var my_cat_style ={
		cat1:    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=R|cc0000|FFFFFF'},
		cat2:  { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=G|00cc00|333333'},
		cat3:   { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=B|2222cc|FFFFFF'}, 
		cat4:   { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|00cccc|333333'}
    };
	
    var markerListeners = [
//    	{listener : 'dragend',callback : 'hanleMarkerChange' }
    	{listener : 'dragend',callback : 'reverseGeoCode',ListenerReverseGeoCoding:true },
    	{listener : 'drag',callback : 'reverseGeoCode' },
	];
    var mapListeners = [
    	{listener : 'zoom_changed',callback : 'hanleMapChange' },
    	{listener : 'maptypeid_changed',callback : 'hanleMapChange' },
    	{listener : 'tilt_changed',callback : 'hanleMapChange' }
	];
var mapDefaults = {
cluster_styles : { styles : cluster_styles }, cat_style: my_cat_style
};
if ($('input[name=mapItem]').val())
{
var obj = jQuery.parseJSON($('input[name=mapItem]').val());
var mapOpt = $.extend({}, {zoom:parseInt(obj.zoomLevel), mapTypeId:obj.MapTypeId}, {});
mapDefaults = $.extend({map_opt: mapOpt,clat:obj.lat,clng:obj.lng,addListeners:mapListeners}, {}, mapDefaults);
}

$("#lookupdeal").live('click',function() {
 $.maps['map_1'].geoCode({geocoder :{address:$('#mapaddress').val(),language:'el',region:'GR'}, addListeners:mapListeners});
});



	
		$('#map_1').googleMaps(mapDefaults);
	


if (obj)
{
var infoWindowContent = '<h2>' + obj.item.title + '</h2>';
infoWindowContent += '<div>' + obj.geocoderAddress + '</div>';
	$.maps['map_1'].createMarker({createMarker:{
	position:new google.maps.LatLng(obj.lat,obj.lng),
	draggable:true,
	infoWindow: {content:infoWindowContent},
	infoWindowEvents : {setZoom:parseInt(obj.zoomLevel),MapTypeId:obj.MapTypeId},
	listeners:markerListeners,
	geocoder :{address:$('input[name=address]').val(),language:'el',region:'GR'},
	infoWindowListener: 1}}
	);	
}//END CREATE INITIAL MARKER


$("#loadMarkers").click(function (e) {

$.getJSON("/material/citiesJson.json", function(data) {
$('#markerCategories').show();
$.each(data, function(key, val) {

	$.maps['map_1'].createMarker({createMarker:{
		position:new google.maps.LatLng(val.lat,val.lng),
		draggable:false,
		id:val.id,
		category:val.category,
		infoWindow: {content:val.name},
		infoWindowListener: 1}}
		);
});//END LOOP

//console.log($.maps['map_1'].gMap.getMapTypeId())
});

$(".categories").click(function (e) {
	var me = $(this);
	$.each($.maps['map_1'].markerCategories[me.val()], function(key, val) {
	if (me.is(':checked'))//SHOW MARKERS
	{
		$.maps['map_1'].markersList[val].setVisible(true);
		$.maps['map_1'].markerCluster.addMarker($.maps['map_1'].markersList[val]);
	}//END SHOW
	else {//HIDE MARKERS
		$.maps['map_1'].markersList[val].setVisible(false);
		$.maps['map_1'].markerCluster.removeMarker($.maps['map_1'].markersList[val]);
	}//END HIDE
	});//END LOOP
});

    $("#addressForm").live('submit',function (e) {
       e.preventDefault();
       $('#savePlaceOnMap').show();
       $(res).trigger('loaded');
       $.maps['map_1'].geoCode({geocoder :{address:$('#mapaddress').val(),language:'el',region:'GR'}, markers : {draggable : true,listeners:markerListeners,ListenerReverseGeoCoding:true,geocoder:{language:'el',region:'GR'} },addListeners:mapListeners}
        ,'returnGeocoderObject');
    });

$(".results").live('click', function(e){
	e.preventDefault();
	      $.maps['map_1'].geoCode({geocoder :{address:$(this).html()}, markers : {draggable : true,listeners:markerListeners,ListenerReverseGeoCoding:true,geocoder:{language:'el',region:'GR'} },addListeners:mapListeners}
        ,'geocodedResults');
});

$("#savePlaceOnMap").live('click', function(e){
	var d  = new $.FormData($(this).closest('form').find('.get'),{'ReturnType':'array'});
	$.ajax({
		url: "/ajax/mapActions.php?action=placeItemOnMap",
		async:false,
		type:'POST',
		data:{ 'data':d }, 
		cache: false,
		success: function(data){
				$('.debugArea').html('<span class="red">' + $.lang.changesSaved + '</span>').show();
				setTimeout( "jQuery('.debugArea').hide();",3000 );
				parent.$('body').trigger('loadMap',[d]);
		}
	});//END AJAX 
});

});

	
	function createcircle () {
	//	console.log($("#lat").val());
	//	if ($("#lat").val() == "[]" )  {
		var lat = jQuery.parseJSON( $("#lat").val() );
		var lng = jQuery.parseJSON( $("#lng").val() );
		var radious = jQuery.parseJSON( $("#radious").val() );
		
		index=lat.length + 1;

		for (i=0; i<lat.length;i++) {
			
		 var myLatlng = new google.maps.LatLng(lat[i].lat,lng[i].lng);
		 	
		 radius=radious[i].radious;
		 
		    circle[lat[i].id] = new google.maps.Circle({
	              map: $.maps['map_1'].gMap,
	              radius: Number(radius),
	              clickable:true,
	              strokeColor: "#1E90FF",
	              fillColor: "#1E90FF",
	              fillOpacity: 0.3,
	              center:  myLatlng
	        });
		   
		   circle[lat[i].id].id=i+1;

		   google.maps.event.addListener(circle[lat[i].id], 'click', function() {
			   setSelection(this);
	        });  
		   
		   google.maps.event.addListener(circle[lat[i].id], 'center_changed', function () {
			   setSelection(this);
			   changecirclepoints(this.id);
	        });
		   
		     google.maps.event.addListener(circle[lat[i].id], 'radius_changed', function () {
	        	 setSelection(circle[this.id]);
	        	 changecirclepoints(this.id);
	        });
		     
		}
//	}
	}
	

function initcircle(country) {
	
    var polyOptions = {
      strokeWeight: 0,
      fillOpacity: 0.45,
      editable: true
    };
    // Creates a drawing manager attached to the map that allows the user to draw
    // markers, lines, and shapes

    drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.CIRCLE,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_LEFT,
        drawingModes: [google.maps.drawing.OverlayType.CIRCLE]
      },
      //drawingModes: "circle",
      markerOptions: {
        draggable: true
      },
      polylineOptions: {
        editable: true
      },
      rectangleOptions: polyOptions,
      circleOptions: polyOptions,
      polygonOptions: polyOptions,
      map: $.maps['map_1'].gMap
    });
  
    
    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {
        if (e.type != google.maps.drawing.OverlayType.MARKER) {
        // Switch back to non-drawing mode after drawing a shape.
        drawingManager.setDrawingMode(null);
        // Add an event listener that selects the newly-drawn shape when the user
        // mouses down on it.
        e.overlay.id=index;
        newShape[index]= e.overlay;
        newShape[index].type = e.type;
  
        google.maps.event.addListener(newShape[index], 'click', function() {
            setSelection(newShape[this.id]);
        });  
        
        google.maps.event.addListener(newShape[index], 'center_changed', function () {
        	   setSelection(newShape[this.id]);
        	   changecirclepoints(this.id);
        });
        
        google.maps.event.addListener(newShape[index], 'radius_changed', function () {
        	 setSelection(newShape[this.id]);
        	  changecirclepoints(this.id);
        });
        setSelection(newShape[index]);
        if (country) {
        	var latlng = new google.maps.LatLng(newShape[index].getCenter().lat(), newShape[index].getCenter().lng());
        	$.maps['map_1'].geoCode({focus:false,notfound:'geocodenoreturn',geocoder :{'latLng': latlng,language:'el',region:'GR'}},'returnGeocoderObjects');
        }
        addcirclepoints(index);
        index++;
      }
    });
   }
	
	function addcirclepoints (id) {
		lat=$("#lat").val();
		lng=$("#lng").val();
		radious=$("#radious").val();
		var strlat = lat.substring(0, lat.length - 1);
		if (strlat != "[" ) { strlat=strlat+","; }
		strlat = strlat+'{"id":'+id+',"lat": '+newShape[id].getCenter().lat()+'}]';
		var strlng = lng.substring(0, lng.length - 1);
		if (strlng != "[" ) { strlng=strlng+","; }
		strlng = strlng+'{"id":'+id+',"lng": '+newShape[id].getCenter().lng()+'}]';
		var strradious = radious.substring(0, radious.length - 1);
		if (strradious != "[" ) { strradious=strradious+","; }
		strradious = strradious+'{"id":'+id+',"radious": '+newShape[id].getRadius()+'}]';
		$("#lat").val(strlat);
		$("#lng").val(strlng);
		$("#radious").val(strradious);
	}

	function changecirclepoints (id) {
		var lat=$("#lat").val();
	    var lng=$("#lng").val();
	    var radious=$("#radious").val();
	    var myReg = new RegExp('(\{"id":'+id+'.*?\})','i');
	    var replacelat= '{"id":'+id+', "lat": '+selectedShape.getCenter().lat()+'}';
	    var replacelng= '{"id":'+id+', "lng": '+selectedShape.getCenter().lng()+'}';
	    var replaceradious= '{"id":'+id+', "radious": '+selectedShape.getRadius()+'}';
	    lat=lat.replace(myReg,replacelat);
	    lng=lng.replace(myReg,replacelng);
	    radious=radious.replace(myReg,replaceradious);
	    $("#lat").val(lat);
	    $("#lng").val(lng);
	    $("#radious").val(radious);
	    if (country == true ) {
			var latlng = new google.maps.LatLng(selectedShape.getCenter().lat(), selectedShape.getCenter().lng());
    		$.maps['map_1'].geoCode({focus:false,notfound:'geocodenoreturn',geocoder :{'latLng': latlng,language:'el',region:'GR'}},'returnGeocoderObjectschange');
		}
	}

function clearSelection() {
  if (selectedShape) {
    selectedShape.setEditable(false);
    selectedShape = null;
  }
}

function setSelection(shape) {
  clearSelection();
  selectedShape = shape;
  shape.setEditable(true);
  selectColor(shape.get('fillColor') || shape.get('strokeColor'));
}

function deletefields(reg,str,field) {
	str=str.replace(reg,",");
    str=str.replace(/^\[,/, '[');
    str=str.replace(/,\]$/, ']');
    $("#"+field).val(str);
}

function deleteSelectedShape() {
  if (selectedShape) {
	id=selectedShape.id;
    selectedShape.setMap(null);
    if(m[id] != undefined) { m[id].setMap(null); }
    var lat=$("#lat").val();
    var lng=$("#lng").val();
    var radious=$("#radious").val();
    var country=$("#countrymap").val();
    var myReg = new RegExp('(,?)(\{"id":'+id+'.*?\})(,?)','i');
    deletefields (myReg,lat,"lat");
    deletefields (myReg,lng,"lng");
    deletefields (myReg,radious,"radious");
    deletefields (myReg,country,"countrymap");
  }
}

function selectColor(color) {
  selectedColor = color;
  for (var i = 0; i < colors.length; ++i) {
    var currColor = colors[i];
    colorButtons[currColor].style.border = currColor == color ? '2px solid #789' : '2px solid #fff';
  }

  // Retrieves the current options from the drawing manager and replaces the
  // stroke or fill color as appropriate.
  var polylineOptions = drawingManager.get('polylineOptions');
  polylineOptions.strokeColor = color;
  drawingManager.set('polylineOptions', polylineOptions);

  var rectangleOptions = drawingManager.get('rectangleOptions');
  rectangleOptions.fillColor = color;
  drawingManager.set('rectangleOptions', rectangleOptions);

  var circleOptions = drawingManager.get('circleOptions');
  circleOptions.fillColor = color;
  drawingManager.set('circleOptions', circleOptions);

  var polygonOptions = drawingManager.get('polygonOptions');
  polygonOptions.fillColor = color;
  drawingManager.set('polygonOptions', polygonOptions);
}

function setSelectedShapeColor(color) {
  if (selectedShape) {
    if (selectedShape.type == google.maps.drawing.OverlayType.POLYLINE) {
      selectedShape.set('strokeColor', color);
    } else {
      selectedShape.set('fillColor', color);
    }
  }
}

function makeColorButton(color) {
  var button = document.createElement('span');
  button.className = 'color-button';
  button.style.backgroundColor = color;
  google.maps.event.addDomListener(button, 'click', function() {
    selectColor(color);
    setSelectedShapeColor(color);
  });

  return button;
}

 function buildColorPalette() {
   var colorPalette = document.getElementById('color-palette');
  
   for (var i = 0; i < colors.length; ++i) {
     var currColor = colors[i];
     var colorButton = makeColorButton(currColor);
     colorPalette.appendChild(colorButton);
     colorButtons[currColor] = colorButton;
   }
   selectColor(colors[0]);
 }


function hanleMapChange(map)
{
	if (map.event == 'maptypeid_changed')
	{
		$('input[name=MapTypeId]').val(map.getMapTypeId());
	}
	else if (map.event == 'zoom_changed')
	{
		$('input[name=zoomLevel]').val(map.getZoom());
	}
}//END FUNCTION

function hanleMarkerChange(marker)
{
	console.log(marker.map.getZoom())
}//END FUNCTION

function reverseGeoCode(results)
{
	var latLng = results.getPosition();		

	if (results.event == 'dragend')
	{
		$('#geocodingDataNow .address span').html(results.results.formatted_address);
		populateHiddenFields(results.results.formatted_address,latLng.lat(),latLng.lng(),$.maps['map_1'].gMap);
	}
	else if (results.event == 'drag')
	{
		$('#geocodingDataNow .currentPosition span').html(latLng.lat() + ' , ' + latLng.lng());
	}
}

function populateHiddenFields(address,lat,lng,map)
{
	$('input[name=geocoderAddress]').val(address);
	$('input[name=lat]').val(lat);
	$('input[name=lng]').val(lng);
	$('input[name=zoomLevel]').val(map.getZoom());
	$('input[name=MapTypeId]').val(map.getMapTypeId());
}

function geocodedResults(results)
{
	var latLng = results[0].marker.getPosition();
	populateHiddenFields(results[0].formatted_address,latLng.lat(),latLng.lng(),$.maps['map_1'].gMap);
	$('#geocodingDataNow .address span').html(results[0].formatted_address);
}


function returnGeocoderObject(results) {

	$('#geocodingResults').children().remove();
	var latLng = results[0].marker.getPosition();

	populateHiddenFields(results[0].formatted_address,latLng.lat(),latLng.lng(),$.maps['map_1'].gMap);
	$('#geocodingDataNow .currentPosition span').html(latLng.lat() + ' , ' + latLng.lng());
	$('#geocodingDataNow .address span').html(results[0].formatted_address);
	if (results.length > 0)
	{
		$.each(results, function(key, val) {
			$('#geocodingResults').append('<li><a href="#" class="results" rel="marker-' + key + '">' + val.formatted_address + '</a></li>');
		});
	}
}

});//END JQ



function returnGeocoderObjectschange(results) {
	
	ids=selectedShape.id;
	if (typeof document.getElementById('countrymap') != undefined ) {
		c=document.getElementById('countrymap').value;
		var myReg = new RegExp('(\{"id":'+ids+'.*?\})','i');
		
		for (var i = 0; i < results[0].address_components.length; i++)
    	{
			var addr = results[0].address_components[i];
			var getCountry;
			if (addr.types[0] == 'country') 
			getCountry = addr.short_name;
    	}
    	code=getCountry.toLowerCase();
    	var replacecountry= '{"id":'+ids+', "country": '+code+'}';
    	c = c.replace(myReg,replacecountry);
		document.getElementById('countrymap').value=c;
	}
}


function geocodenoreturn() {
	
		
		ids=selectedShape.id;
		var country=document.getElementById('countrymap').value;
		var myReg = new RegExp('(\{"id":'+ids+'.*?\})','i');
		country=country.replace(myReg,'{"id":'+ids+',"country": nocountry }');
	    
		if (country == '{"id":'+ids+',"country": "nocountry"}' ) { 
			document.getElementById('countrymap').value=country;
		} else {
			var country = country.substring(0, country.length - 1);
			if (country != "[" ) { country=country+","; }
			document.getElementById('countrymap').value=country+'{"id":'+ids+',"country": nocountry }';
		}
		
}

function returnGeocoderObjects(results) {
	if (typeof document.getElementById('countrymap') != undefined ) {
	var country=document.getElementById('countrymap').value;
	var country = country.substring(0, country.length - 1);
	if (country != "[" ) { country=country+","; }
	ids=index-1;
	
	for (var i = 0; i < results[0].address_components.length; i++)
    {
      var addr = results[0].address_components[i];
      var getCountry;
      if (addr.types[0] == 'country') 
        getCountry = addr.short_name;
    }
	
    code=getCountry.toLowerCase();
    country = country+'{"id":'+ids+',"country": "'+code+'"}]';
	document.getElementById('countrymap').value=country;
	} else {
		console.log("Place a country map hidden field ");
	}
}
