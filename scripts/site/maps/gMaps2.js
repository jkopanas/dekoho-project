jQuery.fn.googleMaps = function(options) {
	
	return this.each(function() {
		
	$.maps[this.id] = new $.googleMaps;
	// Fill default values where not set by instantiation code
	var opts = $.extend({}, $.maps[this.id].defaults, options);
		// Create Map
	var settings = $.extend({}, $.maps[this.id].defaults, options);
	settings['map_opt'] = $.extend({}, $.maps[this.id].baseconf,options.map_opt);
	settings['infobox_s'] = $.extend({}, $.maps[this.id].s_infobox, options.infobox_s);
	$.maps[this.id].defaults = $.extend({}, $.maps[this.id].defaults,settings);
    $.maps[this.id].center = new google.maps.LatLng(settings.clat, settings.clng);
    $.maps[this.id].gMap = new google.maps.Map(this, settings.map_opt);
    $.maps[this.id].bounds = new google.maps.LatLngBounds();
    $.maps[this.id].markerCluster = new MarkerClusterer($.maps[this.id].gMap, null, settings.cluster_styles);
    $.maps[this.id].gMap.setCenter($.maps[this.id].center);
	if (options.addListeners)
	{
		var me = $.maps[this.id];
		$.each(options.addListeners, function(key, val) {
			google.maps.event.addListener(me.gMap, val.listener, function() {
			callbackHandler(val.callback,$.extend({}, me.gMap,{ event:val.listener}))
			});
		});
	}//END LISTENERS
	if (options.createMarker)
	{
		$.maps[this.id].createMarker(options);
	}
	
	});
};


 $.googleMaps = function (options)
{
	 
   return new $.extend({
baseconf : {
    zoom: 8,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    zoomControl: true,
    zoomControlOptions : {style: google.maps.ZoomControlStyle.DEFAULT}
},
mapsConfiguration: function(opts) {
	
},

s_infobox : {
	 content: "",
	 disableAutoPan: false,
	 maxWidth: 0,
	 pixelOffset: new google.maps.Size(-110, 5),
	 zIndex: null,
	 boxStyle: { 
	 background: "url('/scripts/maps/images/infobox_top.png') top center no-repeat",
	 opacity: 1,
	 color:'#000',
	 padding: '0',
	 width: "220px"
	 },
	 closeBoxMargin: "16px 4px",
	 closeBoxURL: "images/infobox_close.png",
	 infoBoxClearance: new google.maps.Size(1, 1),
	 isHidden: false,
	 pane: "floatPane",
	 enableEventPropagation: false
},

defaults : {

    elements: '#list .maplocation', //links selector
    map_opt: {},          // custom map options object
    clat: 35.01107294249947,                // set the lat default map center
    clng: 33.1880304,                // set the lng default map center
    mapstyle_name: '',              // custom map style label and id
    mapstyle: '',                   // mapstyle object
    cluster_styles: {},             // custom cluster icons object
    marker_icon: '',                // custom marker icon url
    infowindows: true,              // shows infoWindows grabing html from the .infobox element
    infobox: false,                 // enable custom infoWindows using infobox class
    infobox_s: {},           // default color scheme for custom infobox container
    trigger: 'map_open',            // you can set a event trigger for each link/marker
    clickedzoom: 15,                // set the zoom level when you click the single marker
    timeout: 100,                   // delay between click and zoom on the single marker
    mode: 'latlng',                 // switch mode
    wait: 500,                      // timeout between geocode requests
    maxtry:10,                      // limit of time to bypass query overlimit
    cat_style: {},                  // costum icons and click zoom level
    fitbounds: true,                // on|off fit bounds
    defzoom : 20,                    // default zoom level if fitbounds is off
    showzoom: false,                // bind current map zoom level event
    before: function () {},         // before create map callback
    after: function () {},          // after create map callback 
    afterUpdate: function () {}     // after update map callback

    
},
init : function() { 
	this.defaults['map_opt'] = this.baseconf;
	this.defaults['infobox_s'] = this.s_infobox;
},
GeocoderStatusDescription : {
  "OK": "The request did not encounter any errors",
  "UNKNOWN_ERROR": "A geocoding or directions request could not be successfully processed, yet the exact reason for the failure is not known",
  "OVER_QUERY_LIMIT": "The webpage has gone over the requests limit in too short a period of time",
  "REQUEST_DENIED": "The webpage is not allowed to use the geocoder for some reason",
  "INVALID_REQUEST": "This request was invalid",
  "ZERO_RESULTS": "The request did not encounter any errors but returns zero results",
  "ERROR": "There was a problem contacting the Google servers"
},
GeocoderLocationTypeDescription : {
  "ROOFTOP": "The returned result reflects a precise geocode.",
  "RANGE_INTERPOLATED": "The returned result reflects an approximation (usually on a road) interpolated between two precise points (such as intersections). Interpolated results are generally returned when rooftop geocodes are unavilable for a street address.",
  "GEOMETRIC_CENTER": "The returned result is the geometric center of a result such a line (e.g. street) or polygon (region).",
  "APPROXIMATE": "The returned result is approximate."
},
directions: {},
latitude: '',
longitude: '',
latlong: {},
maps: {},
center: {},
bounds: {},
marker: {},
markerCategories: {},
markersList: {},
markerCluster: {},
geocoderResults: {},
gMap: {},
boundingBoxes: new Array(),
getLatitude: function() {
	return this.latitude;
},
getLongitude: function() {
	return this.longitude;
},
getBounds: function() {
	return this.gMap.getBounds();
},
getZoom: function() {
	return this.gMap.getZoom();
},
mapLatLong: function(latitude, longitude) {
	// Returns Latitude & Longitude Center Point
	return new google.maps.LatLng(latitude, longitude);
},
clearMarkers : function(marker)
{
	if (marker)
	{
		this.markerCluster.removeMarker(marker);
	}//ALL
	else {
		var me = this;
		$.each(this.markersList, function(key, val) {
		  me.markerCluster.removeMarker(me.markersList[key]);
		  delete me.markersList[val];
		});
	}
},
clearAllMarkers : function()
{
	this.markerCluster.clearMarkers();
},
createMarker : function(options){
	
	var opts = $.extend({}, {		draggable:false,
		map:this.gMap,
		position: this.center}, options.createMarker);
	var settings = $.extend({}, this.defaults, opts);
   if (opts.category){
   	if (!this.markerCategories[opts.category])
   	{
   		this.markerCategories[opts.category] = new Array;
   	}
   	this.markerCategories[opts.category].push(opts.id);
   	if (settings.cat_style)
   	{
   		var c_icon = settings.cat_style[opts.category]['icon'];
    	if (c_icon){ settings['icon'] =  c_icon; }
   	}
   }
	var marker = new google.maps.Marker(settings);
	var me = this;
	var totalMarkers = (me.markersList.length) ? me.markersList.length + 1 : 1;
	var id = (opts.id) ? opts.id : totalMarkers;

	me.markersList[id] = marker;
	if (opts.id)
	{
		marker['id'] = opts.id;
	}
	this.bounds.extend(options.createMarker.position);
	if (settings.listeners)
	{
		$.each(settings.listeners, function(key, val) {
			google.maps.event.addListener(marker, val.listener, function() {
				if (val.ListenerReverseGeoCoding)//REVERSE GEOCODING LISTENER
				{
					me.ListenerReverseGeoCoding(val.listener,$.extend({}, {marker : marker }, settings),val.callback);
				}//END REVERSE GEOCODING
				else {
					callbackHandler(val.callback,$.extend({}, marker,{ event:val.listener,map:this.gMap}));
				}
			});
		});
	}//END LISTENERS
	if (settings.infoWindow)
	{
		var infowindow = this.infoWindow(settings.infoWindow);
		if (settings.infoWindowListener)
		{
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(me.gMap,marker);
				if (settings.infoWindowEvents.setZoom)
				{
						me.gMap.setZoom(settings.infoWindowEvents.setZoom)
				}
				if (settings.infoWindowEvents.MapTypeId)
				{
						me.gMap.setMapTypeId(settings.infoWindowEvents.MapTypeId)
				}
				
				me.gMap.setCenter(marker.getPosition());
				
			});
			google.maps.event.addListener(me.gMap, 'click', function() {
			  infowindow.close();
			});
		}
	}//END INFO WINDOWS

	this.markerCluster.addMarker(marker);
	return marker;
},//END CREATE MARKER
infoWindow: function(options)
{
	var infowindow = new google.maps.InfoWindow({
	    content: options.content
	});
	return infowindow;
},
geoCoderCallback : function(func) { 
	this[func].apply(this, Array.prototype.slice.call(arguments, 1));
},
geoCode: function(options,callback) {

	var opts = $.extend({}, {language:'en',
		region:'US',
		address: ''}, options.geocoder);
	
	if ( typeof options.focus  == 'undefined' ) { options.focus=true;}
	var me = this;
	 geocoder = new google.maps.Geocoder();
	this.clearMarkers();
	
if (geocoder) {
  geocoder.geocode( opts, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
    	
    	if (options.focus) {
    me.gMap.setCenter(results[0].geometry.location);
	me.DrawBoundingBox(results[0],options);
    //FOCUS ON THE FIRST
    $.results = results;
	var markerOpts = $.extend({}, {position: results[0].geometry.location,
			draggable:false,
			infoWindow: {content: results[0].formatted_address},
			infoWindowListener: 1}, options.markers);
	
      var marker = me.createMarker({createMarker:markerOpts});
	//Clear old markers
	var id = (me.markersList.length) ? me.markersList.length + 1 : 1;
	me.markersList[id] = marker;
	results[0]['marker'] = marker;
    }
/*      	if (options.SaveAliasData)//THIS WILL SAVE THE ALIAS MARKER
		{
			$(options.SaveAliasData).val('lat:::'+results[0].geometry.location.lat()+'###lng:::'+results[0].geometry.location.lng())
		}*/
      if(options.ListenerReverseGeoCoding)
      {
      	me.ListenerReverseGeoCoding({marker:marker,settings:options})
      }//END REVERSE GEOCODING LISTENER
      
/*      if (options.addListeners)
      {
      	me.MapListeners({settings:options,point:marker})
      }*/
		if (options.addListeners)
		{
			$.each(options.addListeners, function(key, val) {
				google.maps.event.addListener(me.gMap, val.listener, function() {
				callbackHandler(val.callback,$.extend({}, me.gMap,{ event:val.listener}))
				});
			});
	}//END LISTENERS
		if (callback)
		{
			callbackHandler(callback,results);
		}
    } else {
      callbackHandler(options.notfound,results);
      alert("Geocode was not successful for the following reason: " + status);
    }
  });
}

},//END FUNCTION
test : function() { return console.log(this) },
DrawBoundingBox: function(res,options)
{
	 var ne = res.geometry.viewport.getNorthEast();
     var sw = res.geometry.viewport.getSouthWest();
//          console.log(ne + ' ' + sw);
      this.gMap.fitBounds(res.geometry.viewport); 
      var boundingBoxPoints = [
        ne, new google.maps.LatLng(ne.lat(), sw.lng()),
        sw, new google.maps.LatLng(sw.lat(), ne.lng()), ne
     ];
     var boundingBox = new google.maps.Polyline({//This will draw the geocoder results box
        path: boundingBoxPoints,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
     });
	//clear old ones
	this.clearBoundingBoxes();
	this.boundingBoxes.push(boundingBox);
     boundingBox.setMap(this.gMap);
     
      if (options.SaveZipData)// THIS WII SAVE THE BOX INFO OF A ZIP CODE
      {
      	$(options.SaveZipData).val('ne_lat:::'+ne.lat()+'###sw_lng:::'+sw.lng()+'###sw_lat:::'+sw.lat()+'###ne_lng:::'+ne.lng()+'###lat:::'+res.geometry.location.lat()+'###lng:::'+res.geometry.location.lng())
      }//END SAVE ZIP GEOCODE INFO
	return boundingBoxPoints;
},
clearBoundingBoxes : function()
{
  for (i in this.boundingBoxes) {
      this.boundingBoxes[i].setMap(null);
    }
},
MapListeners: function(options)
{
	var point = options.point;
    google.maps.event.addListener(this.gMap, 'zoom_changed', function() {
			callbackHandler(options.settings.addListeners,this);
		  }); 
	 google.maps.event.addListener(this.gMap, 'maptypeid_changed', function() {
          	callbackHandler(options.settings.addListeners,this);
	          	
	 }); 
	 google.maps.event.addListener(this.gMap, 'tilt_changed', function() {
          	callbackHandler(options.settings.addListeners,this);
	          	
	 }); 
},//END LISTENERS
ListenerReverseGeoCoding: function(event,options,callback) {
//Add listener to marker for reverse geocoding
//console.log(options)
var point = options.marker;
var me = this;
var latlng = point.getPosition();
options.geocoder.address =''; //RESET ADDRESS AS THIS IS REVERSE GEOCODING
var opts = $.extend({}, {'latLng': new google.maps.LatLng(latlng.lat(), latlng.lng())}, options.geocoder);
if (!this.geocoder)
{
	geocoder = new google.maps.Geocoder();
}

geocoder.geocode( opts, function(results, status) {
  if (status == google.maps.GeocoderStatus.OK) {
    if (results[0]) {
    	var ret = $.extend({}, point,{ event:event,map:me.gMap});
		callbackHandler(callback,$.extend({}, {results : results[0]},ret))
    }
  }
});

}//END REVERSE GEOCODING
},options);
};//END CLASS

function callbackHandler(func)
{
	this[func].apply(this, Array.prototype.slice.call(arguments, 1));
}
