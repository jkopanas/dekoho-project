    var markerItems = {};
    var h;
    var res = {};
    var mapOpts ={};
    var markerListeners = {};
    var mapListeners = {};

    
head.ready(function(){

    savePoint = new mcms.dataStore({
        method: "POST",
        url:"/ajax/mapActions.php?action=placeItemOnMap&module="+currentModule,
        dataType: "json",
        trigger:"loadComplete"
    });	
    
    $(savePoint).bind('loadComplete',function(){
        $('#debugArea span').html($.lang.changesSaved).show();
        setTimeout( "jQuery('#debugArea').hide();",3000 );	
    });
    
    markerListeners = [
    //    	{ listener : 'dragend',callback : 'hanleMarkerChange' }
    {
        listener : 'dragend',
        callback : 'reverseGeoCode',
        ListenerReverseGeoCoding:true
    },
    {
        listener : 'drag',
        callback : 'reverseGeoCode'
    },

    {
        listener : 'rightclick',
        callback : 'markerRightClick'
    }
		
    ];
    mapListeners = [
    {
        listener : 'zoom_changed',
        callback : 'hanleMapChange'
    },
    {
        listener : 'maptypeid_changed',
        callback : 'hanleMapChange'
    },
    {
        listener : 'tilt_changed',
        callback : 'hanleMapChange'
    }
    //    	{ listener : 'click',callback : 'hanleMapChange' }
    	
    ];
	
    	points = new mcms.dataStore({
            method: "POST",
            url:"/ajax/mapActions.php?action=debug&module="+currentModule,
            dataType: "json",
            trigger:"loadComplete"
        });	
    
 //   $("#maps a").live('click',function() { 


       // $(points).bind('loadComplete',function(){
        if ($('input[name=lat]').val() !== "" ) {
            mapOpts = {
                zoom: parseInt($('input[name=zoomLevel]').val()),
                clat: $('input[name=lat]').val(),
                clng: $('input[name=lng]').val(),
                mapTypeId: 'roadmap',
                createMarker: true
            };
        } else {
            mapOpts = {
            		 mapTypeId: 'roadmap'
            }
        }

        mapOpts.mapListeners = mapListeners;

        $('#mapHotel').googleMaps(mapOpts).data('mapHotel').init();
        h = $('#mapHotel').data('mapHotel'); 	

        /*
        if (markerItems.hasOwnProperty('items')) {

            $.each(markerItems.items, function(key, val) {
                h.createMarker({
                    markerListeners:markerListeners, 
                    category:val.itemid,
                    id: val.id,
                    draggable:true,
                    map:h.obj,
                    position : new google.maps.LatLng(val.lat,val.lng),
                    infoWindow: {
                        content : '#' + val.itemid + ' - ' + val.id + '<br /> ' + val.geocoderAddress
                        }
                    } );
                fillFields(val);
            });					
        }


        points.records = {};
        points.url = '/ajax/loader.php?file=hotels/mapActions.php&action=getSingleItem&module='+currentModule+'&itemid='+itemid+"&table=maps_items";
        $(points).loadStore(points);
*/

        $(points).bind('ChangeMap',function(){
        	results=points.records;
    	$("#MapChange").html(results.template);
    });
    
        
        $("#MapOption").change(function(e){
        	points.trigger="ChangeMap";
        	points.url = '/ajax/loader.php?module=hotel&boxID='+$(this).val()+'&id='+itemid;
            $(points).loadStore(points);
        });
        

 //   });
 
        $("#savePlaceOnMap1").click(function(e){
        	e.preventDefault();
        	$("#savePlaceOnMap").trigger('click');
        });
        
 $("#savePlaceOnMap").click(function(e){
	 e.preventDefault();
	var d  = new mcms.formData($(this).closest('form').find('.get'),{'ReturnType':'array'});


	$.ajax({
		url: "/ajax/mapActions.php?action=placeItemOnMap&module=hotel",
		async:false,
		type:'POST',
		data:{ 'data':d }, 
		cache: false,
		success: function(data){
			//$('.debugArea').html('<span class="red">' + $.lang.changesSaved + '</span>').show();
			//	setTimeout( "jQuery('.debugArea').hide();",3000 );
			points.trigger="ChangeMap";
    		points.url = '/ajax/loader.php?module=hotel&boxID=46&id='+itemid;
        	$(points).loadStore(points);
saved($.lang.changesSaved);
				parent.$('body').trigger('loadMap',[d]);
		}
	});//END AJAX 

});

 
$("#addressSearch").live('click',function (e) {
    e.preventDefault();
    h.geoCode({
        geocoder :{
            address:$('#mapaddress').val(),
            language:'el',
            region:'GR'
        }, 
        markers : {
            draggable : true,
            markerListeners:markerListeners,
            ListenerReverseGeoCoding:true,
            onComplete:function(marker){
                return;
            }
        },
    addListeners:mapListeners
    }
    ,'returnGeocoderObject');
});


$(".results").live('click', function(e){
    e.preventDefault();

    h.geoCode({
        geocoder :{
            address:$(this).html(),
            language:'el',
            region:'GR'
        }, 
        markers : {
            draggable : true,
            markerListeners:markerListeners,
            ListenerReverseGeoCoding:true
        },
        addListeners:mapListeners
    }
    ,'geocodedResults');
});	 

    $("#addressForm").live('submit',function (e) {
       e.preventDefault();
       $('#savePlaceOnMap').show();
  
    h.geoCode({
        geocoder :{
            address:$('input[name=address]').val(),
            language:'el',
            region:'GR'
        }, 
        markers : {
            draggable : true,
            markerListeners:markerListeners,
            ListenerReverseGeoCoding:true,
            onComplete:function(marker){
                return;
            }
        },
    addListeners:mapListeners
    }
    ,'returnGeocoderObject');
    });

function removeHTMLTags(text) {
    var strInputCode = text;
    /*
			This line is optional, it replaces escaped brackets with real ones,
			i.e. < is replaced with < and > is replaced with >
			strInputCode = strInputCode.replace(/&(lt|gt);/g, function (strMatch, p1) {
				return (p1 == "lt")? ""<"" : "">"";
			});
		 */
    var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");
    //alert(”Output text:\n” + strTagStrippedText);
    // Use the alert below if you want to show the input and the output text
    // alert(”Input code:\n” + strInputCode + “\n\nOutput text:\n” + strTagStrippedText);
			
    return strTagStrippedText;
}

});//END JQ

function hanleMapChange(map)
{
    if (map.event == 'maptypeid_changed')
    {
        $('input[name=MapTypeId]').val(map.getMapTypeId());
    }
    else if (map.event == 'zoom_changed')
    {
        $('input[name=zoomLevel]').val(map.getZoom());
    }
}//END FUNCTION


function reverseGeoCode(results)
{
    var latLng = results.getPosition();		
    if (results.event == 'dragend')
    {
        $('#geocodingDataNow .address span').html(results.results.formatted_address);
        populateHiddenFields(results.results.formatted_address,latLng.lat(),latLng.lng(),results.id,h.getMap());
        save_point();
       // save_multiple_point();
    }
    else if (results.event == 'drag')
    {
        $('#geocodingDataNow .currentPosition span').html(latLng.lat() + ' , ' + latLng.lng());
    }
}//END FUNCTION

function geocodedResults(results)
{
    var latLng = results[0].marker.getPosition();
    populateHiddenFields(results[0].formatted_address,latLng.lat(),latLng.lng(),results.id,h.getMap());
    save_point();
    $('#geocodingDataNow .address span').html(results[0].formatted_address);
}

function returnGeocoderObject(results) {

    $('#geocodingResults').children().remove();
    var latLng = results[0].marker.getPosition();
	
    populateHiddenFields(results[0].formatted_address,latLng.lat(),latLng.lng(),0,h.getMap());
    save_point();
	
    $('#geocodingDataNow .currentPosition span').html(latLng.lat() + ' , ' + latLng.lng());
    $('#geocodingDataNow .address span').html(results[0].formatted_address);
    if (results.length > 0)
    {
        $.each(results, function(key, val) {
            $('#geocodingResults').append('<li><a href="#" class="results" rel="marker-' + key + '">' + val.formatted_address + '</a></li>');
        });
    }
}

function populateHiddenFields(address,lat,lng,id,map)
{
    $('input[name=geocoderAddress]').val(address);
    $('input[name=lat]').val(lat);
    $('input[name=lng]').val(lng);
    $('input[name=zoomLevel]').val(map.getZoom());
    $('input[name=MapTypeId]').val(map.getMapTypeId());
    $('input[name=id]').val(id);
}

function fillFields(p) {
    $('input[name=geocoderAddress]').val(p.geocoderAddress);
    $('input[name=lat]').val(p.lat);
    $('input[name=lng]').val(p.lng);
    $('input[name=zoomLevel]').val(p.zoomLevel);
    $('input[name=MapTypeId]').val(p.MapTypeId);
    $('input[name=id]').val(p.id);
}


function save_point()
{
    var d  = new mcms.formData($('.mapHotel'),{
        'ReturnType':'array'
    });
    savePoint.data = {
        'data':d
    }
    //$(savePoint).loadStore(savePoint);
}

function save_multiple_point()
{
	var d  = new mcms.formData(('form .get'),{'ReturnType':'array'});
	$.ajax({
		url: "/ajax/mapActions.php?action=placeItemOnMap&module="+currentModule,
		async:false,
		type:'POST',
		data:{ 'data':d }, 
		cache: false,
		success: function(data){
//				$('.debugArea').html('<span class="red">' + $.lang.changesSaved + '</span>').show();
//				setTimeout( "jQuery('.debugArea').hide();",3000 );
				saved($.lang.changesSaved);
				parent.$('body').trigger('loadMap',[d]);
		}
	});//END AJAX 
}

function markerRightClick(results) {
	
	var id=results.id;

	$.ajax({
		url: "/ajax/mapActions.php?action=DeleteItemOnMap&id="+id+"&module="+currentModule,
		async:false,
		type:'POST',
		cache: false,
		success: function(data){
				obj=h.Markers();
				count=obj.length;
				for( i=0; i<=count;i++) {
					if (obj[i].id == id ) {
						obj[i].setMap(null);
					}
				}
				
		}
	});//END AJAX 

	
}