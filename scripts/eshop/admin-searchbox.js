
function clickEvent(data) {
	console.log(data.res.data.id);
}

var ordersUrl = "/ajax/loader.php?boxID=orders";
var plainDataSource = {};
var detailedOrderWindow = {};
var comboBoxDataSource = {};

head.ready(function() {
	comboBoxDataSource = new kendo.data.DataSource({
        data: [ ]
	});
	plainDataSource = new kendo.data.DataSource({
		type : "json",
		serverPaging : true,
		serverSorting : true,
		serverFiltering : true,
		serverAggregates : true,
		pageSize : 40,
		transport : {
			read : {
				url : ordersUrl
			},
			parameterMap : function(data, option) {
				if (option == "read") {
					return data;
				}
				return data;
			}
		},
		aggregate : [
		    { field : "id", aggregate : "count" },
			{ field : "amount", aggregate : "sum" },
		],
		schema : {
			aggregates : function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = jQuery.parseJSON(obj.data.values);
				return {
					id : {
						count : obj.data.count,
					},
					amount : {
						sum : obj.data.sum,
					}
				};
			},
			total : function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = jQuery.parseJSON(obj.data.values);
				return obj.data.count;
			},
			model : {
				id : "id",
				fields : {
					id : {
						type : "Number",
						nullable : true
					},
					email : {
						type : "string",
						nullable : true
					},
					status : {
						nullable : true
					},
					amount : {
						type : "Number",
						nullable : true
					},
					payment_method : {
						nullable : true
					},
					shipping_method : {
						nullable : true
					},
					notes : {
						type : "string",
						nullable : true
					},
					archive : {
						nullable : true
					},
				},
			},
			data : function(data) {
				var obj = jQuery.parseJSON(data);
				var objVal = jQuery.parseJSON(obj.data.values);
				try {
					for (i in objVal) {
						objVal[i].text = "";
						objVal[i].text += objVal[i].id + " "
								+ objVal[i].email + " "
								+ objVal[i].amount;
						
					}
				} catch (e) {
					// console.log(e);
				}
				try {
					if (obj.data.payment_methods) {
						var tmpArr = [];
						for (i in obj.data.shipping_methods) {
							var tmpObj = obj.data.shipping_methods[i];
							tmpArr[i] = {};
							tmpArr[i].id = tmpObj.id;
							tmpArr[i].title = tmpObj.shipping_time;
						}
						comboBoxDataSource
								.data(obj.data.payment_methods
										.concat(tmpArr));
						comboBoxDataSource.fetch(function(data) {
							// console.log(data);
						});
					}
				} catch (e) {
					// console.log(e);
				}
				// console.log(objVal);
				return objVal;
			},
			parse : function(data) {
				return data;
			}
		},
		requestStart : function(e) {
		},
		change : function(e) {
		}
	});
	
	/*******************************/
	$("#searchBox").kendoAutoComplete({
		minLength : 4,
		autoBind: false,
		template: '${ data.text } &euro;',
		dataTextField : "text",
		dataValueField: "id",
		placeholder : "Enter Order ID, User name or email...",
		separator : ", ",
		filter : 'contains', // startswith, endswith
		// suggest: true,
		// template: kendo.template(" "),
		dataSource : plainDataSource,
		delay : 500,
		height : 500,
		select : function(e) {
			if(parseInt(this.value())>0)
			{
				e.preventDefault();
				var orderInfo = new mcms.dataStore({
			    	method: "POST",
			    	url:"/ajax/loader.php?boxID=orders",
			    	dataType: "json",
			 		trigger:"loadComplete",
			 		data: {
			 			id : parseInt(this.value()),
			 		},
				});
				
				$(orderInfo).bind('loadComplete',function(){
					drawOrdersDetailsModal($.parseJSON(orderInfo.records.data.values));
					
				});
				$(orderInfo).loadStore(orderInfo);
			}
		},
		change : function(a) {
			var thisValue = this.value().split(" ");
			plainDataSource.filter({
                field: "text",
                //operator: "eq",
                value:  thisValue[0]
            });
		},
		close : function() {
		},
		open : function() {
		}
	});
	
	detailedOrderWindow = $("#details").kendoWindow({
		title : "Order Details",
		modal : true,
		visible : false,
		resizable : true,
        width: "85%",
        height: "85%",
	}).data("kendoWindow");
});

function drawOrdersDetailsModal(dataItem)
{
	var detailsTemplate = kendo.template( $("#modalTemplate").html() );
	detailedOrderWindow.content(detailsTemplate(dataItem));
	$("#tabStrip").kendoTabStrip().css('border','none');; 
	detailedOrderWindow.center().open();
	
	$("#orderStatusList").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        autoBind: true,
        dataSource: [
            { text: "Έναρξη παραγγελίας", value: "1" },
            { text: "Σε αναμονή", value: "2" },
            { text: "Αναμονή από κάρτα", value: "3" },
            { text: "Επιβεβαίωση παραγγελίας", value: "4" },
            { text: "Προετοιμασία", value: "5" },
            { text: "Αποστολή", value: "6" },
            { text: "Ολοκληρώθηκε", value: "7" },
            { text: "Ακυρώθηκε", value: "8" },
        ],
        change: function(e) {
        	$("#orderStatusList").val()
        	var orderStatusReq = new mcms.dataStore({
            	method: "POST",
            	url:"/ajax/loader.php?boxID=orders",
            	dataType: "json",
         		trigger:"loadComplete",
         		data: {
         			action : "update",
         			id : $("#orderArchiveBtn #orderArchiveVal").html(),
         			status: $("#orderStatusList").val()
         		},
        	});
        	$(orderStatusReq).bind('loadComplete',function(){
        		plainDataSource.fetch (function(data) {
        			//console.log(data);
        		});
        		detailedOrderWindow.close();
        	});
        	$(orderStatusReq).loadStore(orderStatusReq);
        }
    });
}

//orderStatusCodes is initiated at index page - inlineTempalte
function orderStatusConverter(id)
{
	return (orderStatusCodes[id]? orderStatusCodes[id] : "Αγνωστο");
}



function optionsDropDownEditor(container, options) {
    $('<input data-text-field="active" data-value-field="value" data-bind="value:' + options.field + '"/>')
                    .appendTo(container)
                    .kendoDropDownList({
                        autoBind: false,
                        dataSource: [{ id: 1, active: "Yes", value: 1 }, { id: 2, active: "No", value: 0}]
                    });
}

function fetchOrderInfo(orderId)
{
	var orderInfo = new mcms.dataStore({
    	method: "POST",
    	url:"/ajax/loader.php?boxID=orders",
    	dataType: "json",
 		trigger:"loadComplete",
 		data: {
 			id : orderId,
 		},
	});
	
	$(".orderDetailsContainer").html(" ");
	$(".orderDetailsContainer").addClass("ac_loading");
	$(orderInfo).bind('loadComplete',function(){
		//console.log(orderInfo.records);
		$(".orderDetailsContainer").removeClass("ac_loading");
		var tmpTempl = kendo.template($("#extraDetails").html());
		//console.log($.parseJSON(orderInfo.records.data.values).details);
		$(".orderDetailsContainer").html(tmpTempl($.parseJSON(orderInfo.records.data.values)));
	});
	
	$(orderInfo).loadStore(orderInfo);
}

// orderStatusCodes is initiated at index page - inlineTempalte
function orderStatusConverter(id)
{
	return (orderStatusCodes[id]? orderStatusCodes[id] : "Αγνωστο");
}


/******************************* /
$("#searchCombo").kendoComboBox({
	autobind : false,
	filter : "startswith",
	minLength : 3,
	delay : 500,
	placeholder : "Filter by Category...",
	dataTextField : "email",
	dataValueField: "id",
	dataSource : $(document).mCms.eshopCatLocalDataSource,
	change : function(event) {
		if (this.current() !== null) {
			eshopCatLocalDataSource.filter({
				field : "Id",
				operator : "eq",
				value : this.value()
			});
		}
	}
});
/*******************************/
/******************************* /
$("#resultsGrid").kendoGrid({
	dataSource : plainDataSource,
	detailTemplate : kendo.template($("#gridTemplate").html()),
	height : 400,
	sortable : true,
	navigatable : true,
	selectable : 'row',
	scrollable : {
		virtual : true
	},
	columns : gridColumns,
	// function() {return gridColumns; },
	change : function(e) {
		var currTR = this.current().closest('tr');
		// Close other TRs
		this.collapseRow(currTR.siblings());
		// Check if next tr already is a detail-row (KendoUI hides
		// an existing detail-row on collapseRow)
		if (currTR.next('tr').hasClass('k-detail-row')) {
			var detailTR = currTR.next('tr');
			if (detailTR.is(':visible')) {
				this.collapseRow(currTR);
			} else {
				this.expandRow(currTR);
			}
		} else {
			this.expandRow(currTR);
		}
	}
});
/*******************************/