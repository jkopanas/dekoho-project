head.ready(function() {
	$("#searchBox").kendoAutoComplete({
		minLength : 3,
		dataTextField : "from_user",
		//dataValueField: "id_str",
		placeholder: "Enter Category, ID or Name",
		// separator: ", ",
		//filter: 'contains', // startswith, endswith
		//suggest: true,
		// template: kendo.template(" "),
		dataSource : {
			type : "json",
			serverFiltering : false,//true,
			//serverPaging : true,
			//pageSize : 20,
			transport : {
				read: {
                    url: "http://search.twitter.com/search.json",
                    dataType: "jsonp", 
                    data: {
                        q: function() {
                            return $("#searchBox").val();
                        }
                    }
                }
			}
		},
		animation: {
            open: {
                effects: "fadeIn",
                duration: 300,
                show: true
            },
            close: {
                effects: "fadeOut",
                duration: 300,
                hide: true,
                show: false
            }
		},
		delay: 500,
		height: 500,
		schema: { data: "results", type: "jsonp" },
		select : function(e) {
			var dataItem = this.dataItem(e.item.index());
			console.log("event :: select (" + dataItem + ")");
		},
		change : function(a) {
			console.log("event :: change");
			console.log(a);
		},
		close : function() {
			console.log("event :: close");
		},
		open : function() {
			console.log("event :: open");
		}

	});
});
