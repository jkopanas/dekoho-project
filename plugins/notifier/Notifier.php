<?php

class Notifier
{
	/**
	 * Constructor of the class
	 */
	function __construct()
	{

	}

	/**
	 * Selects All Notifications, Can be paginated & filtered
	 * @param unknown_type $settings
	 * @return Ambigous <number, unknown, string, mixed>
	 */
	function getNotifications($settings)
	{
		global $sql;
		$q="";
		if(isset($settings['filters']['searchfilters']))
		{
			$tmpArr = array();
			foreach ($settings['filters']['searchfilters'] as $k=>$v)
			{
				$tmpArr[] = " notifier.`".$k."` ".(is_numeric($v)? ( round($v/2147483647) >0? ' >= '.$v : ' = '.$v ) : " LIKE '".$v."'");
			}
			$q .= " WHERE ";
			$q .= implode(" AND ",$tmpArr);
		}

		$sort_direction = ($settings['filters']['sort_direction'])? $settings['filters']['sort_direction'] :'DESC';
		$sort_field = ($settings['filters']['sort_field']) ? $settings['filters']['sort_field'] : 'id';
		$q .= " ORDER BY $sort_field $sort_direction ";

		$current_page = ($settings['filters']['page']) ? $settings['filters']['page'] : 1;
		$results_per_page =  ($settings['filters']['limit']) ? $settings['filters']['limit'] : 40;
		if (isset($settings['filters']['start']))
		{
			$start = $settings['filters']['start'];
		}
		else
		{
			$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
		}
		$q .= " LIMIT $start,".$results_per_page;

		$sql->db_Select("notifier",
			" notifier.* ",
			$q,
			false
		);
		return execute_multi($sql);
	}

	function getFieldChanges($settings)
	{
		global $sql;
		$q="";
		$sql->q("EXPLAIN components");
		$tmpArr = execute_multi($sql);
		$fieldArr = array();
		foreach($tmpArr as &$vArr)
		{
			$fieldArr[$vArr['Field']] = $vArr['Field'];
		}
	}

	/**
	 * Returns count query
	 * @param unknown_type $settings
	 */
	function getNotificationCount($settings)
	{
		global $sql;
		$q="";
		if(isset($settings['filters']['searchfilters']))
		{
			$tmpArr = array();
			foreach ($settings['filters']['searchfilters'] as $k=>$v)
			{
				$tmpArr[] = " notifier.`".$k."` ".(is_numeric($v)? ( round($v/PHP_INT_MAX) >0? ' >= '.$v : ' = '.$v ) : " LIKE '".$v."'");
			}
			$q .= implode(" AND ",$tmpArr);
		}

		$sql->db_Select("notifier", " count(id) as countN ", $q );
		$tmp = execute_single($sql);
		return $tmp['countN'];
	}

	/**
	 * Select Market country by Key
	 * @param unknown_type $key
	 * @return Ambigous <number, unknown, string, mixed>
	 */
	function getNotificationByKey($key)
	{
		global $sql;
		$sql->db_Select("notifier","*"," `key` = ".$key." ");
		return execute_single($sql);
	}

	/**
	 * Deletes Notification countries either by ID or Key
	 * @param unknown_type $argArr
	 */
	function deleteNotification($argArr)
	{
		global $sql;
		return $sql->db_Delete("notifier", (is_numeric($argArr['id'])?" `id` = ".$argArr['id']:" `key` = '".$argArr['id']."'"));
	}

	/**
	 * Updates Notifications either by ID or Key
	 * @param unknown_type $argArr
	 */
	function updateNotification($argArr)
	{
		global $sql;
		$id = $argArr['id'];
		unset($argArr['id']);

		foreach ($argArr as $key => $val)
		{
			$sql->db_Update("notifier",
				"`".$key."` = ".(is_numeric($val)?$val:"'".$val."'")." WHERE "
				.(is_numeric($id)?" `id` = ".$id:" `key` = '".$id."'")
				);
		}
		return $id;
	}

	/**
	 * Insert a new Notification
	 * @param unknown_type $argArr
	 */
	function insertNotification($argArr)
	{
		global $sql;
		$sql->db_Insert("notifier",
			"'','".$argArr['key']
		."','".$argArr['date_added']
		."','".$argArr['module']
		."','".$argArr['description']
		."','".$argArr['status']
		."','".$argArr['permalink']
		."','".$argArr['active']
		."','".$argArr['lang']
		."','".$argArr['settings']."'"
		);
		return $sql->last_insert_id;
	}
}
?>