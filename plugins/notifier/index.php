<?php

include_once(ABSPATH."/plugins/notifier/Notifier.php");
HookParent::getInstance()->doBindHookGlobally("notifier","notifierInitiate");


/**
 * Gloal initiation of Plugin
 */
function notifierInitiate()
{
	global $smarty,$loaded_modules;
	$notifierAsModule = array(
			'id' 				=> 98,
			"name" 				=> "notifier",
			"comment" 			=> "Notifier",
			"folder" 			=> "plugins/notifier/",
			"active" 			=> 1,
			"startup" 			=> 1,
			"main_file" 		=> "plugloader.php?fetch=notifier",
			"menu_title_admin" 	=> "notifications",
			"orderby" 			=> 99,
			"is_item" 			=> false,
			"on_menu" 			=> true,
			"insatnce"			=> new Notifier(),
	);
	$loaded_modules['notifier'] = $notifierAsModule;
	$smarty->assign("loaded_modules",$loaded_modules);
}

?>