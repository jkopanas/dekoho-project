<?php

class DynamicLanguage
{
	public $countryCode		= "";
	public $languageCode	= "";
	public $countryName		= "";
	public $sourceIPAddr	= "";
	public $sqlDriver		= "";

	/**
	 * Constructor of the class
	 */
	function __construct()
	{
		global $sql;
		$this->sqlDriver 		= &$sql;
		$this->countryCode		= "";
		$this->countryName		= "";
		$this->languageCode		= "";
		$this->sourceIPAddr		= $_SERVER['REMOTE_ADDR'];
	}
	
	/**
	 * fetches the country of visitor by seeking into a IP list
	 * @param String $ip
	 * @return Ambigous <number, unknown, string, mixed>
	 */
	function getCountryByIPAddr($ip=NULL)
	{
		$ip = ($ip? $ip : $this->sourceIPAddr);
		$ip = explode('.',$ip);
		$ip = ($ip[3] + $ip[2]*256 + $ip[1]*65536 + $ip[0]*16777216);
		$this->sqlDriver->db_Select("geo_ip","*"," `start` <= ".$ip." ORDER BY `start` DESC LIMIT 1");
		$tmpArr = execute_single($this->sqlDriver);
		$this->countryCode = $tmpArr['cc'];
		$this->countryName = $tmpArr['cn'];
		return $tmpArr;
	}
	
	/**
	 * A function that bridges the market countries with the languages 
	 * by accepting the IP addr argument
	 * @param unknown_type $ip
	 * @return Ambigous <number, unknown, string, mixed>
	 */
	function getLanguageByIPAddr($ip=NULL)
	{
		$ip = ($ip? $ip : $this->sourceIPAddr);
		$ip = explode('.',$ip);
		$ip = ($ip[3] + $ip[2]*256 + $ip[1]*65536 + $ip[0]*16777216);
		
		$this->sqlDriver->db_Select("countries","*",
			" `active` = 'Y' AND `code` = ( "
				." SELECT mcountr.`language` "
				." FROM marketcountries AS mcountr "
				." WHERE "
					." mcountr.`key` =  ( "
							." SELECT geoip.cc "
							." FROM geo_ip AS geoip "
							." WHERE "
								." geoip.`start` <= ".$ip
							." ORDER BY geoip.`start` DESC "
							." LIMIT 1 "
					." ) "
			." ) "
		);
		$tmpObj = execute_single($this->sqlDriver);
		$this->languageCode = $tmpObj['code'];
		return $tmpObj;
	}
	
	/**
	 * Get all activate Languages
	 * @param array $actives
	 * @return Ambigous <number, unknown, string, mixed>
	 */
	function getLanguages($actives=true)
	{
		$this->sqlDriver->db_Select("countries","*",($actives?" active = 'Y' ":""));
		return execute_multi($this->sqlDriver);
	}
	
	/**
	 * Selects All Market Countries, Can be paginated & filtered
	 * @param unknown_type $settings
	 * @return Ambigous <number, unknown, string, mixed>
	 */
	function getMarketCountries($settings)
	{
		$q="";
		if(isset($settings['filters']))
		{
			$tmpArr = array();
			foreach ($settings['filters'] as $k=>$v)
			{
				$tmpArr[] = " marketcountries.`".$k."` = ".(is_numeric($v)? $v : "'".$v."'");
			}
			$q .= " WHERE ";
			$q .= implode(" AND ",$tmpArr);
		}
		
		$sort_direction = ($settings['sort_direction'])? $settings['sort_direction'] :'DESC';
		$sort_field = ($settings['sort_field']) ? $settings['sort_field'] : 'id';
		$q .= " ORDER BY $sort_field $sort_direction ";
		
		$current_page = ($settings['page']) ? $settings['page'] : 1;
		$results_per_page =  ($settings['limit']) ? $settings['limit'] : 40;
		if (isset($settings['start']))
		{
			$start = $settings['start'];
		}
		else 
		{
			$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
		}
		$q .= " LIMIT $start,".$results_per_page;
		
		$this->sqlDriver->db_Select("marketcountries",
			" marketcountries.*, markets.`name` as marketName,  countries.country as languageName ",
			" JOIN markets ON markets.id = marketcountries.marketID "
			." JOIN countries ON countries.`code` = marketcountries.`language` ".$q,
			false
		);
		return execute_multi($this->sqlDriver);
	}
	
	/**
	* Returns count query
	* @param unknown_type $settings
	*/
	function getMarketsCountriesCount($settings)
	{
		$q="";
		if(isset($settings['filters']))
		{
			$tmpArr = array();
			foreach ($settings['filters'] as $k=>$v)
			{
				$tmpArr[] = " marketcountries.`".$k."` = ".(is_numeric($v)? $v : "'".$v."'");
			}
			$q .= " WHERE ";
			$q .= implode(" AND ",$tmpArr);
		}
	
		$this->sqlDriver->db_Select("marketcountries",
			" count(marketcountries.id) as countN ",
			$q,
			false 
		);
		$tmp = execute_single($this->sqlDriver);
		return $tmp['countN'];
	}
	
	/**
	* Selects All Market, Can be paginated & filtered
	* @param unknown_type $settings
	* @return Ambigous <number, unknown, string, mixed>
	*/
	function getMarkets($settings)
	{
		$q="";
		if(isset($settings['filters']))
		{
			$tmpArr = array();
			foreach ($settings['filters'] as $k=>$v)
			{
				$tmpArr[] = "`".$k."` = ".(is_numeric($v)? $v : "'".$v."'");
			}
			$q .= implode(" AND ",$tmpArr);
		}
	
		$sort_direction = ($settings['sort_direction'])? $settings['sort_direction'] :'DESC';
		$sort_field = ($settings['sort_field']) ? $settings['sort_field'] : 'id';
		$q .= " ORDER BY $sort_field $sort_direction ";
	
		$current_page = ($settings['page']) ? $settings['page'] : 1;
		$results_per_page = ($settings['limit']) ? $settings['limit'] : 40;
		if (isset($settings['start']))
		{
			$start = $settings['start'];
		}
		else
		{
			$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
		}
		$q .= " LIMIT $start,".$results_per_page;
		
		$this->sqlDriver->db_Select("markets", " markets.*", $q );
		return execute_multi($this->sqlDriver);
	}
	
	/**
	 * Returns count query
	 * @param unknown_type $settings
	 */
	function getMarketsCount($settings)
	{
		$q="";
		if(isset($settings['filters']))
		{
			$tmpArr = array();
			foreach ($settings['filters'] as $k=>$v)
			{
				$tmpArr[] = "`".$k."` = ".(is_numeric($v)? $v : "'".$v."'");
			}
			$q .= implode(" AND ",$tmpArr);
		}
		
		$this->sqlDriver->db_Select("markets", " count(id) as countN ", $q );
		$tmp = execute_single($this->sqlDriver);
		return $tmp['countN'];
	}
	
	/**
	 * Select Market by Key
	 * @param unknown_type $key
	 * @return Ambigous <number, unknown, string, mixed>
	 */
	function getMarketByKey($key)
	{
		$this->sqlDriver->db_Select("markets","*"," `key` = '".$key."' ");
		return execute_single($this->sqlDriver);
	}
	
	/**
	* Select Market country by Key
	* @param unknown_type $key
	* @return Ambigous <number, unknown, string, mixed>
	*/
	function getMarketCountryByKey($key)
	{
		$this->sqlDriver->db_Select("marketcountries","*"," `key` = '".$key."' ");
		return execute_single($this->sqlDriver);
	}
	
	/**
	 * Updates Market countries either by ID or Key
	 * @param unknown_type $argArr
	 */
	function updateMarketCountry($argArr)
	{
		$id = $argArr['id'];
		unset($argArr['id']);
		$marketTmp = $this->getMarketByKey($argArr['marketKey']);
		$argArr['marketID'] = $marketTmp['id'];
		foreach ($argArr as $key => $val)
		{
			$this->sqlDriver->db_Update("marketcountries",
				"`".$key."` = ".(is_numeric($val)?$val:"'".$val."'")." WHERE "
					.(is_numeric($id)?" `id` = ".$id:" `key` = '".$id."'")
			);
		}
		return $id;
	}
	
	/**
	 * Updates Market countries either by ID or Key
	 * @param unknown_type $argArr
	 */
	function deleteMarket($argArr)
	{
		return $this->sqlDriver->db_Delete("markets", (is_numeric($argArr['id'])?" id = ".$argArr['id']:" key = '".$argArr['id']."'"));
	}
	
	/**
	* Updates Market countries either by ID or Key
	* @param unknown_type $argArr
	*/
	function deleteMarketCountry($argArr)
	{
		return $this->sqlDriver->db_Delete("marketcountries", (is_numeric($argArr['id'])?" `id` = ".$argArr['id']:" `key` = '".$argArr['id']."'"));
	}
	
	/**
	* Updates Markets either by ID or Key
	* @param unknown_type $argArr
	*/
	function updateMarket($argArr)
	{
		$id = $argArr['id'];
		unset($argArr['id']);
	
		foreach ($argArr as $key => $val)
		{
			$this->sqlDriver->db_Update("markets",
				"`".$key."` = ".(is_numeric($val)?$val:"'".$val."'")." WHERE "
					.(is_numeric($id)?" `id` = ".$id:" `key` = '".$id."'")
			);
		}
		return $id;
	}
	
	/**
	 * Insert a new Market Country
	 * @param unknown_type $argArr
	 */
	function insertMarketCountry($argArr)
	{
		$marketTmp = $this->getMarketByKey($argArr['marketKey']);
		
		$this->sqlDriver->db_Insert("marketcountries",
			"'','".$argArr['key']
			."','".$marketTmp['id']
			."','".$argArr['marketKey']
			."','".$argArr['name']
			."','".$argArr['description']
			."','".$argArr['currency']
			."','".$argArr['active']
			."','".$argArr['language']."'"
		);
		return $this->sqlDriver->last_insert_id;
	}
	
	/**
	* Insert a new Market
	* @param unknown_type $argArr
	*/
	function insertMarket($argArr)
	{
		$this->sqlDriver->db_Insert("markets",
			"'','".$argArr['key']
			."','".$argArr['name']
			."','".$argArr['active']
			."','".$argArr['default']."'"
		);
		return $this->sqlDriver->last_insert_id;
	}
}
?>