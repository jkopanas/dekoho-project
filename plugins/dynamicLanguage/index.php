<?php

include_once(ABSPATH."/plugins/dynamicLanguage/DynamicLanguage.php");
HookParent::getInstance()->doBindHookGlobally("beforeDefineContentLanguage","dynamicLanguageInject");
HookParent::getInstance()->doBindHookGlobally("dynamicLanguage","dynamicLanguageInitiate");
//HookParent::getInstance()->doBindHookGlobally("afterSystemLoad","dynamicLanguageAfterSystemLoad");
HookParent::getInstance()->doBindHookGlobally("beforePlugLoaddynamicLanguage","beforePlugLoadDL");


/**
 * Inject Hack for Languages of MCMS
 */
function dynamicLanguageInject()
{
	global $smarty,$sql;
	if(empty($_GET['lang']) && empty($_SESSION['plugins']['dynamicLanguage']['langCode']))
	{
		$tmp 					= new DynamicLanguage();
		//$tmpMarketCountryObj 	= $tmp->getCountryByIPAddr("72.233.69.6"); // Random US IP
		//$tmpLangObj 			= $tmp->getLanguageByIPAddr("72.233.69.6");
		//$tmpMarketCountryObj 	= $tmp->getCountryByIPAddr("188.4.206.159"); //Random GR IP
		//$tmpLangObj 			= $tmp->getLanguageByIPAddr("188.4.206.159");
		//$tmpMarketCountryObj 	= $tmp->getCountryByIPAddr("192.168.2.5"); // Random US IP
		//$tmpLangObj 			= $tmp->getLanguageByIPAddr("192.168.2.5");
		$tmpMarketCountryObj 	= $tmp->getCountryByIPAddr();
		$tmpLangObj 			= $tmp->getLanguageByIPAddr();
		$_SESSION['plugins']['dynamicLanguage']['countryCode'] 	= $tmpMarketCountryObj['cc'];
		$_SESSION['plugins']['dynamicLanguage']['countryName'] 	= $tmpMarketCountryObj['cn'];

		$tmpMarketCountryObj 	= $tmp->getMarketCountryByKey($tmpMarketCountryObj['cc']);
		$_SESSION['plugins']['dynamicLanguage']['marketKey'] 	= $tmpMarketCountryObj['marketKey'];
		$_SESSION['plugins']['dynamicLanguage']['langCode'] 	= $tmpLangObj['code'];
		$_SESSION['plugins']['dynamicLanguage']['agencyID'] 	= $tmpMarketCountryObj['agencyID'];
		$_SESSION['plugins']['dynamicLanguage']['currency'] 	= $tmpMarketCountryObj['currency'];
		
		$sql->db_Select("exchange","rate, symbol","currency = '".$tmpMarketCountryObj['currency']."'");
		$tmpRate = execute_single($sql);
        $_SESSION['plugins']['dynamicLanguage']['exchange'] 	= $tmpRate['rate'];
        $_SESSION['plugins']['dynamicLanguage']['symbol'] 	    = $tmpRate['symbol'];
		
		$tmpMarketCountryObj 	= $tmp->getMarketByKey($tmpMarketCountryObj['marketKey']);
		$_SESSION['plugins']['dynamicLanguage']['marketName'] 	= $tmpMarketCountryObj['name'];
		$_SESSION['plugins']['dynamicLanguage']['marketSettings'] = $tmpMarketCountryObj['settings'];
	}
	elseif(!empty($_GET['lang']))
	{
		$tmp 					= new DynamicLanguage();
		//$tmpMarketCountryObj 	= $tmp->getCountryByIPAddr();
		//$tmpMarketCountryObj 	= $tmp->getCountryByIPAddr("72.233.69.6"); // Random US IP
		//$tmpLangObj 			= $tmp->getLanguageByIPAddr("72.233.69.6");
		//$tmpMarketCountryObj 	= $tmp->getCountryByIPAddr("188.4.206.159"); //Random GR IP
		//$tmpLangObj 			= $tmp->getLanguageByIPAddr("188.4.206.159");
		$tmpMarketCountryObj 	= $tmp->getCountryByIPAddr();
		$tmpLangObj 			= $tmp->getLanguageByIPAddr();
		$_SESSION['plugins']['dynamicLanguage']['countryCode'] 	= $tmpMarketCountryObj['cc'];
		$_SESSION['plugins']['dynamicLanguage']['countryName'] 	= $tmpMarketCountryObj['cn'];

		$tmpMarketCountryObj 	= $tmp->getMarketCountryByKey($tmpMarketCountryObj['cc']);
		$_SESSION['plugins']['dynamicLanguage']['marketKey'] 	= $tmpMarketCountryObj['marketKey'];
		$_SESSION['plugins']['dynamicLanguage']['langCode'] 	= $_GET['lang'];
		$_SESSION['plugins']['dynamicLanguage']['agencyID'] 	= $tmpMarketCountryObj['agencyID'];
		$_SESSION['plugins']['dynamicLanguage']['currency'] 	= $tmpMarketCountryObj['currency'];
		
		$sql->db_Select("exchange","rate","currency = '".$tmpMarketCountryObj['currency']."'");
		$tmpRate = execute_single($sql);
		$_SESSION['plugins']['dynamicLanguage']['exchange'] 	= $tmpRate['rate'];
		
		$tmpMarketCountryObj 	= $tmp->getMarketByKey($tmpMarketCountryObj['marketKey']);
		$_SESSION['plugins']['dynamicLanguage']['marketName'] 	= $tmpMarketCountryObj['name'];
		$_SESSION['plugins']['dynamicLanguage']['marketSettings'] = $tmpMarketCountryObj['settings'];
		unset($_GET['lang']);
	}
	$langObj = new Languages();
	$langObj->setLanguage($_SESSION['plugins']['dynamicLanguage']['langCode'], $cookie);
	
	define("FRONT_LANG",$_SESSION['plugins']['dynamicLanguage']['langCode']);
	define("LANG",$_SESSION['plugins']['dynamicLanguage']['langCode']);
	define("COUNTRY_CODE",$_SESSION['plugins']['dynamicLanguage']['countryCode']);
	define("COUNTRY_NAME",$_SESSION['plugins']['dynamicLanguage']['countryName']);
	define("MARKET_KEY",$_SESSION['plugins']['dynamicLanguage']['marketKey']);
	global $front_language;
	$front_language = $_SESSION['plugins']['dynamicLanguage']['langCode'];
	$smarty->assign("front_language",$_SESSION['plugins']['dynamicLanguage']['langCode']);
	//$smarty->assign("lang",$langObj->LoadLanguage($_SESSION['plugins']['dynamicLanguage']['langCode']));
	$smarty->assign("FRONT_LANG",$_SESSION['plugins']['dynamicLanguage']['langCode']);
	$smarty->assign("LANG",$_SESSION['plugins']['dynamicLanguage']['langCode']);
	$smarty->assign("COUNTRY_CODE",$_SESSION['plugins']['dynamicLanguage']['countryCode']);
	$smarty->assign("COUNTRY_NAME",$_SESSION['plugins']['dynamicLanguage']['countryName']);
	$smarty->assign("MARKET_KEY",$_SESSION['plugins']['dynamicLanguage']['marketKey']);
	$country = $langObj->get_country($_SESSION['plugins']['dynamicLanguage']['langCode']);
	setlocale (LC_ALL, $country['locale']);
	$smarty->assign("LanguageDetails",$country);

	if(FRONT_LANG != DEFAULT_LANG) 
	{
		$_GET['lang'] = FRONT_LANG;
		$_GET['sl'] = FRONT_LANG;
		define("TRANSLATE",$_SESSION['plugins']['dynamicLanguage']['langCode']);
		$smarty->assign("TRANSLATE",$_SESSION['plugins']['dynamicLanguage']['langCode']);
	}
}

/**
 * Gloal initiation of Plugin
 */
function dynamicLanguageInitiate()
{
	global $smarty,$loaded_modules;
	$dynamicLanguageAsModule = array(
			'id' 				=> 99,
			"name" 				=> "dynamicLanguage",
			"comment" 			=> "Dynamic Languages",
			"folder" 			=> "plugins/dynamicLanguage/",
			"active" 			=> 1,
			"startup" 			=> 1,
			"main_file" 		=> "plugloader.php?fetch=dynamicLanguage",
			"menu_title_admin" 	=> "languages",
			"orderby" 			=> 0,
			"is_item" 			=> false,
			"on_menu" 			=> false,
			"insatnce"			=> new DynamicLanguage(),
			"langCode"			=> LANG,
			"countryCode"		=> COUNTRY_CODE,
			"countryName"		=> COUNTRY_NAME
		
	);
	$loaded_modules['dynamicLanguage'] = $dynamicLanguageAsModule;
	$smarty->assign("loaded_modules",$loaded_modules);
}

/**
 * Manipulate before fetch Plugin's Admin Page
 */
function beforePlugLoadDL()
{
	global $smarty,$loaded_modules;
	$tmp = new DynamicLanguage();
	$smarty->assign("active_languages",$tmp->getLanguages());
}
?>