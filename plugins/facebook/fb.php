<?php
class fb extends user {


	/**
	 * The users profile
	 *
	 * @param int $id
	 * @param array $settings
	 * @return array
	 */

	public $facebook_uid = 0;
	public $like_app = 0;
	public $facebook_profile = 0;
	
	public function GetRegisterUser($uid=0) {
		
		require_once ABSPATH.'/plugins/facebook/facebook_sdk3/facebook.php';
	
		if (empty($_SESSION['facebook_profile'])) {
			
			if (!$uid) {
				$facebook = new Facebook(array(
  					'appId'  => '188422644620707',
  					'secret' => 'eafd0993b73d9a7125456bb94bf1cd08',
					'cookie' => true
				));
		
				$params = array(
        			'method' => 'fql.query',
        			'query' => "SELECT uid,is_app_user FROM user WHERE uid= me()",
    			);
    			$result = $facebook->api($params);
			} else {
				$result['0']['uid'] =$uid;
			}
			$this->facebook_profile = ($result) ? $this->userProfile(0,array("getUser" => 1,"searchUser" => array("facebook_uid" => $result[0]['uid'] ))) : false;
			$this->SetSessionFacebook($this->facebook_profile);
			
		} else {
			
			$this->facebook_profile=$_SESSION['facebook_profile'];
			$this->like_app = $_SESSION['facebook_profile']['settings']['like_app'];
			$this->facebook_uid = $_SESSION['facebook_profile']['facebook_uid'];
			
		}
		
	}
	
	public function SetSessionFacebook($ar) {
		
			$this->facebook_uid = $ar['facebook_uid'];
			$settings = json_decode($ar['settings'],true);
			$this->like_app = $settings['like_app'];
			$this->facebook_profile['settings'] = $settings;
			$_SESSION['facebook_profile'] = $ar;
			
	}

	
	public function userProfile($id = 0,$settings=0)
	{
		global $sql;

		$fields = ($settings['field']) ? $settings['field'] : "*";
		
		if ($id) {
			$q[] = "id =$id";
		}
		
		if ($settings['searchUser']) {			
			foreach ($settings['searchUser'] as $key => $value ) {
				$q[] = $key. "='".$value."'";
			}	
		}
	
		if (is_array($q)) {
			$query = implode(" AND ",$q);
		}
		
		$sql->db_Select($this->tables['profile']['table'],$fields,$query);
		if ($sql->db_Rows() > 0) {
			$u=execute_single($sql);
			if ($settings['getUser']) {
				 $user=$this->searchUsers(array( "return" =>"single","debug" =>0,"searchFields" => array("id" =>$u['id'])));	
				 $user['profile'] = $u;
				 return $user;
			}
		} else {
			return array();
		}
		
	}//END FUNCTION
	
	
	public function addUserProfile($data,$settings=array()) {
		global $sql;
		foreach ($data as $k => $v)
		{ 	 				 	       
			$keys[] = $k;
			$values[] = "'$v'";
		}
		
		$sql->db_Insert($this->tables['profile']['table']." (".implode(",",$keys).")",implode(",",$values));
		if ($settings['debug']) {
			echo "INSERT INTO ".$this->tables['profile']['table']." (".implode(",",$keys).") VALUES (".implode(",",$values).")";
		}
		
		
		
	}



}