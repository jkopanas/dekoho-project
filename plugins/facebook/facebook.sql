-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 13, 2012 at 03:08 PM
-- Server version: 5.5.22
-- PHP Version: 5.3.10-1ubuntu3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `baloons`
--

-- --------------------------------------------------------

--
-- Table structure for table `users_comments`
--

CREATE TABLE IF NOT EXISTS `users_comments` (
  `itemid` int(11) DEFAULT NULL,
  `module` varchar(20) DEFAULT NULL,
  `comment` text,
  `uid` int(11) DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  KEY `itemid` (`itemid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `users_items_votes`
--

CREATE TABLE IF NOT EXISTS `users_items_votes` (
  `itemid` int(11) NOT NULL,
  `module` varchar(20) NOT NULL,
  `votes_plus` int(11) DEFAULT NULL,
  `votes_minus` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_points`
--

CREATE TABLE IF NOT EXISTS `users_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `itemid` int(11) DEFAULT NULL,
  `module` varchar(20) DEFAULT NULL,
  `action` varchar(20) DEFAULT NULL,
  `points` tinyint(4) DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `uid` (`uid`),
  KEY `itemid` (`itemid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_points_list`
--

CREATE TABLE IF NOT EXISTS `users_points_list` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `var` varchar(20) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` text,
  `value` tinyint(4) DEFAULT NULL,
  `unique_per_page` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_profile`
--

CREATE TABLE IF NOT EXISTS `users_profile` (
  `id` int(11) DEFAULT NULL,
  `facebook_uid` varchar(50) DEFAULT NULL,
  `birthday` varchar(20) DEFAULT NULL,
  `gender` int(1) DEFAULT NULL,
  `settings` text,
  `points` int(11) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


ALTER TABLE `users_profile` ADD `facebook_uid` varchar(50) DEFAULT NULL;
ALTER TABLE `users_profile` ADD `birthday` varchar(20) DEFAULT NULL;
ALTER TABLE `users_profile` ADD `gender` int(1) DEFAULT NULL;
ALTER TABLE `users_profile` ADD `settings` text;
ALTER TABLE `users_profile` ADD `points` int(11) DEFAULT NULL;

-- --------------------------------------------------------

--
-- Table structure for table `users_votes`
--

CREATE TABLE IF NOT EXISTS `users_votes` (
  `itemid` int(11) DEFAULT NULL,
  `rating` tinyint(4) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `module` varchar(20) DEFAULT NULL,
  KEY `itemid` (`itemid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
