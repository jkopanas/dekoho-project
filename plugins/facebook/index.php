<?php

	if ($_GET['action'] != "facebook") { return; }
	include ABSPATH.'/plugins/facebook/fb.php';
	
	
	HookParent::getInstance()->doBindHookGlobally("HomePreFetch","StartFacebook");
	HookParent::getInstance()->doBindHook('content', "ContentFetch","ContentFacebook");
	
	HookParent::getInstance()->doBindHookGlobally("BeforeLoadTemplate","LoadUserFilterTemplate");
	HookParent::getInstance()->doBindHookGlobally("AfterInitUser","UserExtendFb");
	HookParent::getInstance()->doBindHookGlobally("AfterManageUserTemplate","AddGridCommands");

	
	
	function ContentFacebook ($module) {
		
		global $smarty,$menu,$items;
		
		$items['data']['template']=$smarty->fetch($smarty->getTemplateVars('include_file'));		
		$item['data']=$items['data'];
	
		
		$item['results']['item'] = $items['results'];
		$item['results']['featured']=$smarty->getTemplateVars('featured');

		if( $signedRequest['page']['liked'] == 1 ) {
			$smarty->$smarty->fetch("include_file", "plugins/facebook/facebook_not_like.tpl");
		}
		
		if ($_GET['ajax']) { echo json_encode($item); exit; }
		

		$smarty->display("plugins/facebook/home.tpl");
		

	}
	
	function StartFacebook () {
		
		global $smarty,$menu,$Content,$user_fb;
		$featured['featured'] = $Content->FeaturedContent(27,array('get_results'=>1,'active'=>1,'debug'=>0,'thumb'=>1,'images'=>1,'orderby'=>'orderby','cat_details'=>1));
		

		if($_SESSION['facebook_profile']['settings']['like_app']) {
			$data['template']=$smarty->fetch("plugins/facebook/facebook_like.tpl");
			#echo "1like";
		} else {
			#echo "not like";
			$data['template']=$smarty->fetch("plugins/facebook/facebook_not_like.tpl");
		}
		
		if ($_GET['ajax']) { echo json_encode(array("data" => $data, "results" => $featured )); exit; }
		$smarty->display("plugins/facebook/home.tpl");
		exit;
	}

	function AddGridCommands ($module) {
		global $smarty;
		$smarty->assign("plugin_file", "plugins/facebook/admin/GridCommands.tpl");
	}

	function LoadUserFilterTemplate ($ar) {
		global $smarty;
		$smarty->append("boxFilter",array("title" => "Facebook Search","file" => "plugins/facebook/admin/FilterTpl.tpl"));
	}


	function UserExtendFb () {
		global $user;
		include ABSPATH.'/plugins/facebook/fb.php';
		$user = new fb();
	}


?>
