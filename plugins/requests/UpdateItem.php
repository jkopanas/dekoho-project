<?php
class UpdateItem {
	
	
	public $table;
	public $uid;
	
	function __construct($settings =array()) {
		
		
		$this->table=($settings['table']) ? $settings['table'] : "users_request";

		$this->uid = (!is_numeric(ID)) ? $settings['uid'] :  ID;
	
		
	}
	
	
	public function InsertRequest($data) {
		
		global $sql;
		$t = new Parse();
		
	//	print_ar($data);
		foreach ($data as $k=>$v) {
			$insertQ[] = "'".$t->toDB($v)."'";
			$fields[] = "`".$k."`";
			$updateQ[] = "`".$k."` = '".$t->toDB($v)."'";
		}
		//
		$sql->db_Select("users_request","id","uid=".$this->uid." and `table`='".$this->table."' and `field`='".$data['field']."' and hotelid=".$data['hotelid']."  and itemid=".$data['itemid']." and status=0");
	
		if ($sql->mySQLrows > 0) {
			$row=execute_single($sql);
	
			$query = implode(",",$updateQ)." WHERE id =".$row['id'];
			$sql->db_Update("users_request",$query);

		} else {
			$insertQ[] = "'".time()."'";
			$insertQ[] = "'".ID."'";
			$insertQ[] = "'".$this->table."'";
			$fields[] = 'date_added';
			$fields[] = 'uid';
			$fields[] = '`table`';
				
			$sql->db_Insert("users_request (id,status,".implode(',',$fields).")","'',0,".implode(",",$insertQ));
			$id = $sql->last_insert_id;

			
		}
		
	}
	
	
	public function GetRequest($itemid,$settings =array()) {
		
		global $sql;
		

		$q = (is_array($itemid)) ? "itemid in (".implode(",",$itemid).")" : "itemid=".$itemid;

		if ($settings['fields']) {
			$q = $q." and `field`='".$settings['fields']."'";
		}

		$q = ($settings['status']) ? $q." and status in (".$settings['status'].")" : $q." and status=0";

		$sql->db_Select("users_request","*",$q." and `table`='".$this->table."' and uid=".$this->uid);
	//	print_ar($sql);
	//echo "users_request","*",$q." and `table`='".$this->table."' and uid=".$this->uid;
		$res = execute_multi($sql);
		 $ar=array();
		 $fieldar = array();
	//	print_ar($res);
 
		 foreach((array)$res as $key => $value) {		 	
		 	$ar[$value['itemid']][$value['field']]= ($settings['getFields']) ? $value['value'] : $value;
		 	$fieldar[$value['itemid']][]=$value['field'];
		 }
		 if ($settings['getFields']) {
		 	return array($ar,$fieldar);
		 }
		 
		 return $ar;
	}
	
	public function GetSingleRequest($hotelid) {
		global $sql;
		
		$sql->db_Select("users_request","*","status =0 and hotelid=".$hotelid);
		$res = execute_multi($sql);
		
		return $res;
	}
	
	public function UpdateRequest($settings =array(),$status) {

		$res=$this->GetRequest($settings['id'],array("fields" => $settings['field'],"status" => ($settings['status']) ? $settings['status'] : 0));
		if ($res[$settings['id']][$settings['field']]['id']) {
			$this->ChangeReqStatus($res[$settings['id']][$settings['field']]['id'],$status);
		}
	}
	
	public function DeleteRequest($settings =array()) {
		
		$res=$this->GetRequest($settings['id'],array("fields" => $settings['field'],"status" => "0,1"));
		if ($res[$settings['id']][$settings['field']]['id']) { 
			$this->delete($res[$settings['id']][$settings['field']]['id']);
		}
	}

	public function AcceptRequest($settings =array()) {
		
		global $sql;
		
		$res=$this->GetRequest($settings['id'],array("fields" => $settings['field']));
		$res = $res[$settings['id']][$settings['field']];
		
		$res['value'] = ($settings['value']) ? $settings['value'] : $res['value'];
		
		$sql->db_Update($this->table, "`".$res['field']."`='".$res['value']."' where `".$res['key']."`=".$res['itemid']);
		
		$this->ChangeReqStatus($res['id'], "1");
		$sql->db_Select($this->table,"`".$res['field']."`",$res['key']."=".$res['itemid']);

		return execute_single($sql);

	}
	
	public function DenyRequest($settings) {
		
		global $sql;
		$res=$this->GetRequest($settings['id'],array("fields" => $settings['field']));
		$res = $res[$settings['id']][$settings['field']];
		
		$this->ChangeReqStatus($res['id'], "2");		
		$sql->db_Select($this->table,$res['field'],$res['key']."=".$res['itemid']);
		
		return execute_single($sql);
	}
	
	
	private function ChangeReqStatus ($id,$status) {
		global $sql;
		
		if ($status) {
			$sql->q("INSERT INTO users_request_history SELECT id,uid,field,value,`table`,itemid,hotelid,`key`,date_added,1 FROM users_request WHERE id = ".$id);
			$this->delete($id);
		} else {
			$sql->db_Update("users_request", "status=".$status." where id=".$id);
		}
		
		return true;
		
	}
	
	private function delete ($id) {
		global $sql;
		$sql->db_Delete("users_request", "id=".$id);
		//echo "users_request", "status = 0 and id=".$id;
		return true;
	
	}
	
	public function MoveRequestToArchive() {
	
	
	}
	
	public function searchRequests ($posted_data,$settings,$page) {
		
		global $sql;
		$fields= ($posted_data['fields']) ? $posted_data['fields'] : "*";
		$posted_data['sort_direction'] = ($posted_data['sort_direction']) ? $posted_data['sort_direction'] : "asc";
		$posted_data['results_per_page'] = ($posted_data['results_per_page']) ? $posted_data['results_per_page'] : "10";
		
		
		
		if ($posted_data['searchFields']) {
			foreach ($posted_data['searchFields'] as $key=>$value) {
			foreach($value as $k =>$v) {
				if ($v['type'] == 'LIKE') {
					$condition[] = $v["table"].".".$key." LIKE '%".$v["val"]."%'";
				}
				elseif ($v['type'] == 'NOTLIKE') {
					$condition[] = $v["table"].".".$key." NOT LIKE '%".$v["val"]."%'";
				}
				elseif ($v['type'] == 'EQ') {
					$condition[] = $v["table"].".".$key." = '".$v["val"]."'";
				}
				elseif ($v['type'] == 'NE') {
					$condition[] = $v["table"].".".$key." != '".$v["val"]."'";
				}
				elseif ($v['type'] == 'GT') {
					$condition[] = $v["table"].".".$key." > ".$v["val"]."";
				}
				elseif ($v['type'] == 'LT') {
					$condition[] = $v["table"].".".$key." < ".$v["val"]."";
				}
				elseif ($v['type'] == 'IN') {
					$condition[] = $v["table"].".".$key." IN (".$v['val'].")";
				}
				else {
					$condition[] = $v["table"].".".$key.$v['type']." ".$v["val"]."";
				}
			}
		}
		if ($condition) {
			$search_condition[] = implode(" ".$posted_data['SearchLogic']." ", $condition);
		}
		}
		
		$search_tables[] = "users_request";
		$search_tables[] = "LEFT JOIN hotel ON (hotel.id = users_request.hotelid)";
		$search_tables[] = "LEFT JOIN users ON (users_request.uid = users.id)";
		
		$search_tables[] = "LEFT JOIN geo_relations ON (geo_relations.itemid = users_request.hotelid)";

		if ($posted_data['new'] == "true" ) {
			$search_condition[] = "hotel.enable=0";
		}
		
		$selected = ($posted_data['sort']) ? $posted_data['sort'] : $this->table.".date_added" ;
		$direction = ($posted_data['sort_direction']) ? $posted_data['sort_direction'] : $settings['settings']['default_sort_direction'];
		
		$search_tables = implode(" ",(array)$search_tables);
		$search_condition = implode(" AND ",(array)$search_condition);
		$extras = " GROUP BY users_request.hotelid having MIN(users_request.date_added) ORDER BY $selected $direction";
		
		$current_page = ($posted_data['page']) ? $posted_data['page'] : 1;
		$results_per_page = ($posted_data['results_per_page']) ? $posted_data['results_per_page'] : $settings['settings']['items_per_page'];
		if (isset($posted_data['start']))
		{
			$start = $posted_data['start'];
		}
		else {
			$start = ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
		}
		$mode = ($search_condition) ? 'default' : 'no_where';
		$sql->q("Select COUNT(hotelid) as total from ( Select hotelid from ".$search_tables." where ".$search_condition." ".$extras.") as tables");
		
		$tot = execute_single($sql);
	//	echo $search_tables,"COUNT(*) as total",$search_condition,$mode;
		$total = $tot['total'];
		if ($total)
		{
			$results['data']['total'] = $total;
			$sql->db_Select($search_tables,$fields,"$search_condition $extras LIMIT $start,".$results_per_page,$mode);
			$search_results = execute_multi($sql,1);
		//	echo $search_tables,$fields,"$search_condition $extras LIMIT $start,".$results_per_page,$mode;
			paginate_results($current_page,$results_per_page,$total,$posted_data['suffix']);
		}
		if (!empty($search_results)) {
			foreach ($search_results as $key => &$value ) {

				$req=new UpdateItem(array("table"=> "hotel","uid"=>$value['uid']));
				list($reqRes,$REqField)=$req->GetRequest($value['hotelid'],array("getFields" => 1));
				$value = array_merge ($value,(array) $reqRes[$value['hotelid']]);
				
			}
		}
		
		
		return array($search_results,$results);
		
	}
	
	
	
}

?>