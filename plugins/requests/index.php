<?php
include_once(ABSPATH."/plugins/requests/UpdateItem.php");
HookParent::getInstance()->doBindHookGlobally("requests","requestsInitiate");

function requestsInitiate()
{
 global $sql,$smarty,$loaded_modules;
 $requestsAsModule = array(
   'id'     => 99,
   "name"     => "requests",
   "comment"    => "Requests",
   "folder"    => "plugins/requests/",
   "active"    => 1,
   "startup"    => 1,
   "main_file"   => "plugloader.php?fetch=requests",
   "menu_title_admin"  => "requests",
   "orderby"    => 0,
   "is_item"    => false,
   "on_menu"    => true,  
 );
 $loaded_modules['requests'] = $requestsAsModule ;
 $smarty->assign("loaded_modules",$loaded_modules);

 
 $sql->db_Select("geo_country","id,title");
 $tmp=execute_multi($sql);
 
 
 foreach($tmp as $key => $value) {
 	$res[$value['id']]=$value['title'];
 }
 
 $smarty->assign("Countries",$res);
}
?>