<?php
global $plugins;
$plugins = array(
	'shortcodes'=> array('name' => 'shortCodes','path'=>'shortcodes/index.php','area' => array("F") ),
	'categoriesMaintanance'=> array('name' => 'categoriesMaintanance','path'=>'categoriesMaintanance/index.php','area' => array("A",'X') ),
	//'facebook'=> array('name' => 'facebook','path'=>'facebook/index.php', 'area' => array("F","A","X")),
	'dynamicLanguage' =>array('name' => 'dynamicLanguage','path'=>'dynamicLanguage/index.php', 'tpl'=> 'dynamicLanguage/home.tpl', 'area' => array("F","A","X")),
	'advancedSearch' =>array('name' => 'advancedSearch','path'=>'advancedSearch/index.php', 'tpl'=> 'advancedSearch/home.tpl', 'area' => array("F","A","X")),
	'dbrevision' =>array('name' => 'dbrevision','path'=>'dbrevision/index.php', 'tpl'=> 'notifier/home.tpl', 'area' => array("A","X")),
	//'notifier' =>array('name' => 'notifier','path'=>'notifier/index.php', 'tpl'=> 'notifier/home.tpl', 'area' => array("A","X")),
	'requests' =>array('name' => 'requests','path'=>'requests/index.php', 'tpl'=> 'requests/home.tpl', 'area' => array("F","A","X")),
	'support' =>array('name' => 'support','path'=>'support/index.php', 'tpl'=> 'support/home.tpl', 'area' => array("F","A","X"))
);

?>