<?php

include_once(ABSPATH."/plugins/dbrevision/DBRevision.php");
HookParent::getInstance()->doBindHookGlobally("dbrevision","dbrevisionInitiate");


/**
 * Gloal initiation of Plugin
 */
function dbrevisionInitiate()
{
	global $smarty,$loaded_modules;
	$dbrevisionAsModule = array(
			'id' 				=> 98,
			"name" 				=> "dbrevision",
			"comment" 			=> "DBRevision",
			"folder" 			=> "plugins/notifier/",
			"active" 			=> 1,
			"startup" 			=> 1,
			"main_file" 		=> "plugloader.php?fetch=dbrevision",
			"menu_title_admin" 	=> "DBRevision",
			"orderby" 			=> 99,
			"is_item" 			=> false,
			"on_menu" 			=> false,
			"insatnce"			=> new DBRevision(),
	);
	$loaded_modules['dbrevision'] = $dbrevisionAsModule;
	$smarty->assign("loaded_modules",$loaded_modules);
}

?>