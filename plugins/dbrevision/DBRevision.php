<?php

class DBRevision
{
	/**
	 * Constructor of the class
	 */
	function __construct()
	{
		
	}

	/**
	 * Selects All Fields of a Revision, Can be paginated & filtered
	 * @param unknown_type $settings
	 * @return Ambigous <number, unknown, string, mixed>
	 */
	function getRevisionByKey($settings)
	{
		global $sql;
		$q="";
		
		$fieldArr = $this->getRevisionFields($settings['filters']['module']);
		$entityArr = array();
		
		$settings['filters']['searchfilters']['key'] 	= $settings['filters']['key'];
		$settings['filters']['searchfilters']['module'] = $settings['filters']['module'];
		
		$sort_direction 	= 'DESC';
		$sort_field 		= "`key`, field, date";
		$current_page 		= ($settings['filters']['page']) ? $settings['filters']['page'] : 1;
		$results_per_page 	=  1;
		$start 				= ($current_page) ? ($current_page*$results_per_page)-$results_per_page : 0;
			
		foreach ($fieldArr as $k => $v)
		{
			$tmpArr = array();
			$q="";
			$settings['filters']['searchfilters']['field'] = $k;
			foreach ($settings['filters']['searchfilters'] as $k2=>$v2)
			{
				$tmpArr[] = " notifier_fields.`".$k2."` = ".(is_numeric($v2)? $v2 : "'".$v2."'");
			}
			$q .= " WHERE ";
			$q .= implode(" AND ",$tmpArr);
			
			$q .= " ORDER BY $sort_field $sort_direction ";
			$q .= " LIMIT $start,".$results_per_page;
			
			$sql->db_Select("notifier_fields",
				" date,value,status,lang,settings ",
				$q,
				false
			);
			$entityArr[$k] = execute_single($sql);
		}
		
		return $entityArr;
	}
	
	/**
	 * fetch the fields of a module
	 * @param $module
	 */
	function getRevisionFields($module)
	{
		global $sql;
		$q="";
		$sql->q("EXPLAIN ".$module);
		$tmpArr = execute_multi($sql);
		$fieldArr = array();
		foreach($tmpArr as &$vArr)
		{
			$fieldArr[$vArr['Field']] = $vArr['Field'];
		}
		return $fieldArr;
	}

	/**
	 * Deletes Revision either by ID or Key
	 * @param unknown_type $argArr
	 */
	function deleteRevision($argArr)
	{
		global $sql;
		return $sql->db_Delete("notifier_fields", (is_numeric($argArr['id'])?" `id` = ".$argArr['id']:" `key` = '".$argArr['id']."'"));
	}

	/**
	 * Updates Entity by a Revision
	 * @param unknown_type $settings
	 */
	function updateByRevision($settings)
	{
		global $sql;		
		$id = $settings['filters']['id'];
		foreach ($settings['filters']['values'] as $key => $val)
		{
			$sql->db_Update($settings['filters']['module'],
				"`".$key."` = ".(is_numeric($val)?$val:"'".$val."'")." WHERE "
				.(is_numeric($id)?" `id` = ".$id:" `key` = '".$id."'")
			);
		}
		return $id;
	}

	/**
	 * Insert a new Revision
	 * @param unknown_type $argArr
	 */
	function insertRevision($argArr)
	{
		global $sql;
		$sql->db_Insert("notifier_fields",
			"'','".$argArr['key']
			."','".$argArr['module']
			."','".$argArr['field']
			."','".$argArr['date']
			."','".$argArr['value']
			."','".$argArr['status']
			."','".$argArr['active']
			."','".$argArr['lang']
			."','".$argArr['settings']."'"
		);
		return $sql->last_insert_id;
	}
}
?>