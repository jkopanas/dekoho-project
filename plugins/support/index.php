<?php
include_once(ABSPATH."/plugins/advancedSearch/dao/Support.php");
HookParent::getInstance()->doBindHookGlobally("support","supportInitiate");

function supportInitiate()
{
 global $smarty,$loaded_modules;
 $requestsAsModule = array(
   'id'     => 99,
   "name"     => "support",
   "comment"    => "Supports",
   "folder"    => "plugins/support/",
   "active"    => 1,
   "startup"    => 1,
   "main_file"   => "plugloader.php?fetch=support",
   "menu_title_admin"  => "support",
   "orderby"    => 0,
   "is_item"    => false,
   "on_menu"    => true,  
 );
 $loaded_modules['support'] = $requestsAsModule ;
 $smarty->assign("loaded_modules",$loaded_modules);
}
?>