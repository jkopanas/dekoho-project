<?php

/**
 * The Data Acces Model of hotel
 * @author Evalon-Acer
 * Flags status 1 --> unread
 *		     status 2 --> read
 *          state 0 --> closed
 *			 state 1 --> closed
 */ 
class Support extends AbstractDAO
{
	/**
	 *
	 * @var AdvancedSearch
	 */
	private $__moduleName;

	function __construct()
	{
		
		$this->__moduleName = "users_tickets";
			
		$this->addFieldToStructure("id","users_tickets.`id` as id");
		$this->addFieldToStructure("status","users_tickets.`status` as status");
		$this->addFieldToStructure("state","users_tickets.`state` as state");
		$this->addFieldToStructure("settings","users_tickets.`settings` as settings");
		$this->addFieldToStructure("date_added","users_tickets.`date_added` as date_added");
		$this->addFieldToStructure("title","users_tickets.`title` as title");
		$this->addFieldToStructure("description","users_tickets.`description` as description");
		$this->addFieldToStructure("orderby","orders.`id` as id");
		
	}

	
	function getSupport($settings)
	{
		
		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$settings['filters']['sort_field']="users_tickets.id";
		$settings['filters']['perPage'] = 10;
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		
		$tmpArr['jointables'] = array_merge(array (
			//array('name'=>"orders_types",	"idKey"=>"orders_types.id",		"idRel"=>"orders.typeid",	"joinType"=>"INNER JOIN"),
		),((array)$tmpArr['filters']['jointables']));
		
		$tmpArr['jointables'] = (isset($settings['overwriteJoin'])) ? $settings['overwriteJoin'] : $tmpArr['jointables'];
		$tmpArr['filters']['execute'] = "searchBySQL";

		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
		
		
		return $resultArr;
		
	}
	
	function NewTicket ($data) {
		global $sql;
		//print_ar($data);
		
		$cur=explode(".",microtime(true));
		$user_id = ($data['user_id']) ? $data['user_id'] : ID;

		if (!isset($data['reply'])) {
			if (!isset($data['title'])) { return;}
			$sql->q("select conversationid from users_tickets order by conversationid desc limit 1");
			$res=execute_single($sql);
			$id=($res) ? $res['id']+1 : 1;
			$sql->db_Insert("users_tickets (id,user_id,uid,senderid,conversationid,title,description,date_add,status,state)"," '',".$user_id.",".ID.",1,".$id.",'".$data['title']."','".$data['description']."',".$cur[0].",1,0");
		} else {
			$sql->q("select title,senderid from users_tickets where conversationid=".$data['reply']." and senderid != ".ID." order by id desc limit 1");
			$res=execute_single($sql);

			if ($data['sender_id']){
				$sql->db_Update("users_tickets", "senderid=".$data['user_id']." where conversationid=".$data['reply']." and senderid=1");
				//echo "users_tickets", "senderid=".$data['user_id']." where conversationid=".$data['reply']." and senderid=1";
				$sender_id=$data['sender_id'];
			} else {
				$sender_id=$res['senderid'];
			}
			$sql->db_Insert("users_tickets (id,user_id,uid,senderid,conversationid,title,description,date_add,status,0)"," '',".$user_id.",".ID.",".$sender_id.",".$data['reply'].",'".$res['title']."','".$data['description']."',".$cur[0].",1,0");
		//	echo "users_tickets (id,user_id,uid,senderid,conversationid,title,description,date_add,status)"," '',".$user_id.",".ID.",".$sender_id.",".$data['reply'].",'".$res['title']."','".$data['description']."',".$cur[0].",1";
		}
		return $sql->last_insert_id;
	}
	
	function getSupportDetails($settings)
	{
	
		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$settings['filters']['sort_field']="users_tickets.id";
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
	
		$tmpArr['jointables'] = array_merge(array (
				//array('name'=>"orders_types",	"idKey"=>"orders_types.id",		"idRel"=>"orders.typeid",	"joinType"=>"INNER JOIN"),
		),((array)$tmpArr['filters']['jointables']));
		$tmpArr['jointables'] = (isset($settings['overwriteJoin'])) ? $settings['overwriteJoin'] : $tmpArr['jointables'];
	
		$tmpArr['filters']['execute'] = "searchBySQL";
	
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		
		$resultArr['results'] = (array) $resultArr['results'];
	
		return $resultArr;
	
	}
	
	
	function DeleteSupport($data) {
		
		global $sql;
		$sql->db_Delete("users_tickets","user_id=".ID." AND id in (".$data.")");
		
		return explode(",",$data);
		
	}
	
	function ConversationMsgStatus($data) {
	
		global $sql;

		$sql->db_Update("users_tickets","status=".$data['status']." where conversationid=".$data['id']);

		return;
	
	}
	
	function ConversationMsgState($data) {
	
		global $sql;
		
		$sql->db_Select("users_tickets","conversationid","id=".$data['id']);
		$row=execute_single($sql);
		$sql->db_Update("users_tickets","state=".$data['state']." where conversationid=".$row['conversationid']);
	
		return;
	
	}
	
	
}

?>