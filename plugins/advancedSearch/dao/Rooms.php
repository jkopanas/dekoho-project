<?php

/**
 * The Data Acces Model of hotel
 * @author Evalon-Acer
 *
 */
class Rooms extends AbstractDAO
{
	/**
	 *
	 * @var AdvancedSearch
	 */
	protected $__moduleName;
	public $__joins;

	function __construct()
	{
		
		$this->exchange = $_SESSION['exchange'];
		
		$this->__moduleName = "rooms";
		
		$this->addFieldToStructure("id","rooms.`id` as id");
		$this->addFieldToStructure("hotelKey","rooms.`hotelKey` as `key`");
		$this->addFieldToStructure("title","rooms.`title` as title",1,"");
		$this->addFieldToStructure("description","rooms.`description` as description",1,"");
		$this->addFieldToStructure("active","rooms.`active` as active");
		$this->addFieldToStructure("settings","rooms.`settings` as settings");
		$this->addFieldToStructure("permalink","rooms.`permalink` as permalink",1);
		$this->addFieldToStructure("date_added","rooms.`date_added` as date_added");
		$this->addFieldToStructure("date_modified","rooms.`date_modified` as date_modified");
		$this->addFieldToStructure("comments_count","rooms.`comments_count` as comments_count");
		$this->addFieldToStructure("orderby","rooms.`id` as id");
		$this->addFieldToStructure("description_long","rooms.`description_long` as description_long",1);
		$this->addFieldToStructure("lang","rooms.`lang` as lang");
		
	//	$this->addRelations("roomsfacilities", array("table" => "rooms_facilities", "key"=>"roomid"));
	
		$this->__joins["roomsfacilities"]= array("table" => "rooms_facilities", "key"=>"roomid" , "relId"=>"itemid");
		
		$this->extraTable = array (
				array("table" => "rooms_hotel" , "field"=> "room_id"),
				array("table" => "users_request" , "field"=> "itemid","where" => array("`table`"=> "rooms")),
				array("table" => "rooms_facilities" , "field"=> "roomid")
		);

		
	}

	/**
	 * Get hotel by settings
	 * <pre>
	 * $settings['filters']         = array(
	 *     'id'                => $id,
	 *     'inDiscount'        => 0, // to fetch Hotels in Discount only
	 *     'priceRange'        => array(MIN,MAX),
	 *     'pricePromoRange'   => array(MIN,MAX),
	 *     'categoryid'        => $categoryid, // to fetch Category Path
	 *     'return'            => 'paginated', //or null
	 *     'perPage'           => ($perPage? $perPage : 40),
	 *     'page'              => ($page ? $page : 1 ),
	 *     'archive'           => ($archived? 1 : 0 ),
	 *     'lang'              => ($lang? $lang : DEFAULT_LANG),
	 *     'date-from'         => $dateFrom,
	 *     'date-to'           => $dateTo,
	 *     'sort_field'        => $sortField,
	 *     'sort_direction'    => $sortDir,
	 *     'providerid'        => $providerid,
	 *     'user'              => $user,
	 *     'efield'            => $efield,
	 *     'evalue'            => $evalue,
	 *     'exclude'           => 'id,id,id',//to excludde
	 *     'categoryies'       => array( // To Join content by categories
	 *         array(
	 *             'categoryid'    => $categoryid
	 *         )
	 *     ),
	 * );
	 * $settings['searchfilters']     = array(
	 *     array(
	 *         'item'     => 'id',
	 *         'type'     => 'eq',
	 *         'val'      => 0,
	 *     )
	 * );
	 * </pre>
	 * @param array $settings
	 * @return array array('total'=>$total,'results'=>$search_results,'data'=>$posted_data);
	 */
	function getRoom($settings)
	{
		
		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$settings['filters']['sort_field']="rooms.id";
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		
		$tmpArr['jointables'] = array_merge(array (
			array('name'=>"rooms_hotel",	"idKey"=>"rooms_hotel.room_id",		"idRel"=>"rooms.id","joinType"=>"LEFT JOIN"),
		),((array)$tmpArr['filters']['jointables']));
		$tmpArr['jointables'] = (isset($settings['overwriteJoin'])) ? $settings['overwriteJoin'] : $tmpArr['jointables'];
	
		$tmpArr['filters']['execute'] = ($tmpArr['filters']['execute']? $tmpArr['filters']['execute']: "searchItem");
	    //$tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		
		$resultArr['results'] = (array) $resultArr['results'];
		
		/* Should be delete i fixed with roomProperitesFront 
		$settings['filters']['roomProperties'] = (!$settings['filters']['roomProperties']) ? false : true;
		if ($settings['filters']['roomProperties']) {
			if (isset($resultArr['results'][0]['id'])) {
				foreach( $this->__joins as $key => $value ) {
					
					$resultArr['results'][0][$key]=$this->getRoomProperties($key,$value,$resultArr['results'][0]['id']);
				}
			}
		}
	*/
		// Parse JSON Values
		if ($resultArr['results'][0]) {
			foreach ($resultArr['results'] as &$vArr)
			{
				$vArr['price'] = $this->CalculateCurrency($vArr['price']);
				$vArr['discount'] = $this->CalculateCurrency($vArr['discount']);
				$tmpID[]=$vArr['id'];
				@isJSON($vArr['settings'],$vArr['settings'],true);
			}
		}
		
		return $resultArr;
		
	}
	
	
	public function getPropertiesRoomsFront($settings) {
		



		$tmpArr ['debug'] = 0;
		foreach ( $this->__joins as $key => $value ) {


			$tmpArr ['filters'] ['sort_field'] = $value ['table'] . ".id";
			$tmpArr ['filters'] ['table'] = $value ['table'];
			$tmpArr ['searchfilters'] = $settings ['searchfilters'];
			$tmpArr ['filters'] ['fields'] = array (
					$value ['table'] . '.id' ,$key.".title",$key.".description",$value ['table'] . ".roomid"
			);
				
			$tmpArr ['jointables'] = array_merge ( array ( array (
					'name' => $key,
					"idKey" => $key . ".id",
					"idRel" => $value ['table'] . ".itemid",
					"joinType" => "INNER JOIN" 
			)), (( array ) $tmpArr ['filters'] ['jointables']) );
			
			
			if(LANG != DEFAULT_LANG)
			{
				$tmpArr ['filters'] ['fields'][]="translations.translation as transvalue";
				$tmpArr ['jointables'] = array_merge ( array ( 
						array (
								'name' => $key,
								"idKey" => $key . ".id",
								"idRel" => $value ['table'] . ".itemid",
								"joinType" => "INNER JOIN"
						),	
						array (
								'name' => "translations",
								"idKey" => "translations.itemid",
								"idRel" => $key . ".id and translations.code='".LANG."'",
								"joinType" => "LEFT JOIN"
						)
						), (( array ) $tmpArr ['filters'] ['jointables']) );
			}
			
			$tmpArr ['filters'] ['execute'] = "searchBySQL";
			$resultArr = array ();
			$resultArr = AdvancedSearch::getInstance ( $this->__moduleName )->{$tmpArr ['filters'] ['execute']} ( $tmpArr );
			


			if ( is_array($resultArr ['results'][0]) && LANG != DEFAULT_LANG ) {
				foreach($resultArr ['results'] as $k => &$v) {
					if (!empty($v['transvalue'])) {
					$v['title'] = $v['transvalue'];
					} 
				}
			}

			$ar[$key] = (array) $resultArr ['results'];
			
		}
		return $ar;
	}
	
	
	public function getRoomProperties ($table,$settings,$id) {
	
		$settings['searchfilters'][] = array(
				'item'     => "1",
				'type'     => "eq",
				'val'      => 1,
		);
	
		$tmpArr['debug']=0;
		$tmpArr['filters']['sort_direction'] ="ASC";
		$tmpArr['filters']['sort_field'] =$table.".title";
		$tmpArr['filters']['table'] = $table;
		$tmpArr['filters']['jointables'] = $settings['jointables'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['fields'] = (isset($settings['filters']['fields'])) ?  $settings['filters']['fields'] : array($table.'.id','@ids := id as ids','enable','title','(select roomid from rooms_facilities where itemid=@ids and roomid='.$id.') as roomid');
		$tmpArr['jointables'] = array_merge(array (),((array)$tmpArr['filters']['jointables']));
		$tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
		return $resultArr['results'];
	
	}
	
	
	public function getRoom_Hotel ($settings) {
		
		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$settings['filters']['sort_field']="room_id";
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['table'] = "rooms_hotel";
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		
		$tmpArr['jointables'] = array_merge(array (),((array)$tmpArr['filters']['jointables']));
		$tmpArr['jointables'] = (isset($settings['overwriteJoin'])) ? $settings['overwriteJoin'] : $tmpArr['jointables'];
		//$tmpArr['filters']['execute'] = ($tmpArr['filters']['execute']? $tmpArr['filters']['execute']: "searchItem");
		$tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
		
		return $resultArr;
		
	}

	public function getRoomsByUser ($settings) {
		
		
		$settings['searchfilters'][] = array(
				'item'     => "rooms.uid",
				'type'     => "eq",
				'val'      => ID,
		);
		
		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$settings['filters']['groupBy'] = "rooms.id";
		$settings['filters']['sort_field']="rooms.id";
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		
		$tmpArr['jointables'] = array_merge(array (
				array('name'=>"users",	"idKey"=>"users.id","idRel"=>"rooms.uid",	"joinType"=>"INNER JOIN"),
				array('name'=>"orders","idKey"=>"orders.itemid","idRel"=>"rooms.id and orders.module='".$this->__moduleName."'" ,"joinType"=>"left JOIN"),
		),((array)$tmpArr['filters']['jointables']));
		
		$tmpArr['jointables'] = (isset($settings['overwriteJoin'])) ? $settings['overwriteJoin'] : $tmpArr['jointables'];
		
	//	print_ar($tmpArr['filters']['fields']);
		//$tmpArr['filters']['execute'] = ($tmpArr['filters']['execute']? $tmpArr['filters']['execute']: "searchItem");
		$tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
		
		// Parse JSON Values
	    //	print_ar($resultArr);
	  
		foreach ($resultArr['results'] as &$vArr)
		{
			if (!isJSON($vArr['settings'],$vArr['settings'])) {
				$vArr = @json_decode(base64_decode($vArr['settings']),true);
			}
		}
		return $resultArr;
		
	}
	

	
	public function getHotelRooms($settings) {
		
		$settings['searchfilters'][] = array(
				'item'     => "rooms.uid",
				'type'     => "eq",
				'val'      => ID,
		);
		
		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$settings['filters']['groupBy'] = "room.id";
		$settings['filters']['sort_field']="room.id";
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		
		$tmpArr['jointables'] = array_merge(array (
				array('name'=>"users",	"idKey"=>"users.id","idRel"=>"rooms.uid",	"joinType"=>"INNER JOIN"),
				array('name'=>"orders_item",	"idKey"=>"orders_item.itemid","idRel"=>"rooms.id and orders_item.module='".$this->__moduleName."'" ,"joinType"=>"LEFT JOIN"),
				array('name'=>"orders",	"idKey"=>"orders.id","idRel"=>"orders_item.id" ,"joinType"=>"left JOIN"),
		),((array)$tmpArr['filters']['jointables']));
		
		//$tmpArr['filters']['execute'] = ($tmpArr['filters']['execute']? $tmpArr['filters']['execute']: "searchItem");
		$tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
		
		// Parse JSON Values
		//	print_ar($resultArr);
		 
		foreach ($resultArr['results'] as &$vArr)
		{
			if (!isJSON($vArr['settings'],$vArr['settings'])) {
				$vArr = @json_decode(base64_decode($vArr['settings']),true);
			}
		}
		return $resultArr;
		
	}
	

	public function DashboardRoom($data) {
	
		global $sql;
		
		$sql->db_Select($this->__moduleName,"id,enable","id in (".implode(",",$data['data']).")");
		if ( $sql->mySQLrows == count($data['data']) ) {
			if ($data['mode'] =="deleted" ) {
				$this->DeleteRoom($data['data']);
			} else if ($data['mode'] =="activate" || $data['mode'] =="deactivate") {
				
				$row=execute_multi($sql);
				$data['data']=array();
				foreach($row as $key => $value) {
					if ($value['enable']) {
						$data['data'][]=$value['id'];
					}
				}			
				$this->ModuleActions($data,array("table" => "rooms_hotel","key" => "room_id","extraTables" => $this->extraTable));
			}
		}
		
	}
	
	public function DeleteRoom($data) {
		
		global $sql;
		
		$sql->db_Select("rooms_facilities","itemid","roomid in (".implode(",",$data).")");
		$row=execute_multi($sql);
		
		foreach($row as $key => $value) {
			$itemid[]=$value['itemid'];
		}
		
		$this->DeleteImage($data);
		$sql->db_Delete("users_request","itemid in (".implode(",",$itemid).") and `table`='roomsfacilities'");
		$sql->db_Delete("users_request","itemid in (".implode(",",$itemid).") and `table`='rooms'");
		$sql->db_Delete("rooms","id in (".implode(",",$data).")");
		$sql->db_Delete("rooms_hotel","room_id in (".implode(",",$data).")");
		$sql->db_Delete("rooms_facilities","roomid in (".implode(",",$data).")");
		
		/*
		echo "users_request","itemid in (".implode(",",$facilitiesid).") and `table`='roomsfacilities'"."</br>";
		echo "rooms","id in (".implode(",",$data).")"."</br>";
		echo "rooms_hotel","room_id in (".implode(",",$data).")"."</br>";
		echo "rooms_facilities","roomid in (".implode(",",$data).")"."</br>";
		*/
		/*  TODO Add translations delete */
		
		
	}
	
	
	public function NewRoom($data) {
		
		global $sql;
		
		$tmp=$data['room'];
		$ar= array();
	
		foreach ($tmp as $key => $value) {
			if ($this->__forceAdminCheck[$key] && USER_CLASS != "A" ) {
				unset($tmp[$key]);
				$ar[$key] = $value;
			}
		}

		$tmp['enable'] = array("value" => 0);
		$tmp['uid'] = array("value" => ID);
		

		$roomid = AdvancedSearch::getInstance($this->__moduleName)->ItemAdd($tmp);

		$req=new UpdateItem(array("table"=> $this->__moduleName));
		
		
		foreach ((array)$ar as $key => $value) {
			$tmpArr['field']=$key;
			$tmpArr['value']=$value['value'];
			$tmpArr['itemid']=$roomid;
			$tmpArr['key']="id";
			$tmpArr['hotelid']=$tmp['hotelKey']['value'];
			$req->InsertRequest($tmpArr);
		}
		
		if (is_numeric($roomid)) {
		
		foreach($this->__joins as $key => $value ) {

			if (isset($data['roomProperties'][$key])) {
				foreach($data['roomProperties'][$key] as $k => $v ) {		
					if (!is_numeric($v)) {
						$sql->db_Insert($key." (title,description,enable)","'".$v."','',0");
						$sql->db_Insert($value['table']." (roomid,itemid)","$roomid,$sql->last_insert_id");
						
						$req=new UpdateItem(array("table"=> $key));
						$tmpArr['field']="title";
						$tmpArr['value']=$v;
						$tmpArr['itemid']=$sql->last_insert_id;
						$tmpArr['hotelid']=$tmp['hotelKey']['value'];
						$tmpArr['key']="id";
						$req->InsertRequest($tmpArr);
						
					} else {
						$sql->db_Insert($value['table']." (roomid,itemid)","$roomid,$v");
					}
				}
			}
		}

		
		$number = $data['roomsecondary']['price'] / number_format( $this->exchange, 2, '.', '');
		$data['roomsecondary']['price'] =  number_format($number, 2, '.', '');
		$data['roomsecondary']['room_id'] =$roomid;
		$data['roomsecondary']['hotel_id'] =$data['room']['hotelKey']['value'];

		$sql->db_Insert("rooms_hotel (".implode(",",array_keys($data['roomsecondary'])).")", implode(",",array_values($data['roomsecondary'])) );
	

	}
	return $roomid;
	}
	
	public function UpdateRoom($data,$id) {
	
		global $sql;
		

		$tmp=$data['roommain'];
		$ar = array();
		
		foreach ((array)$tmp as $key => $value) {
			if ($this->__forceAdminCheck[$key]  && USER_CLASS != "A" ) {
				$ar[$key] = $value;
				unset($tmp[$key]);
			}
		}
		
		if (!empty($tmp)) {
		$roomid=AdvancedSearch::getInstance($this->__moduleName)->ItemModify($tmp,$id);
		}
	
		$req=new UpdateItem(array("table"=> $this->__moduleName));
		
		
		foreach ($ar as $key => $value) {
			$tmpArr['field']=$key;
			$tmpArr['value']=$value['value'];
			$tmpArr['itemid']=$id;
			$tmpArr['key']="id";
			$req->InsertRequest($tmpArr);
		}
		
		if ($id) {

			foreach($this->__joins as $key => $value ) {
				
				if ($data['roomProperties'][$key]) {
					$sql->db_Delete($value['table'],"roomid=".$id);
					foreach($data['roomProperties'][$key] as $k => $v ) {
						if (!is_numeric($v)) {

							$sql->db_Insert($key." (title,enable,description)","'".$v."',0,''");
							$itemid =$sql->last_insert_id;
							$sql->db_Insert($value['table']." (roomid,itemid)", $id.",".$itemid);				
							
							$req=new UpdateItem(array("table"=> $key));
							$tmpArr['field']="title";
							$tmpArr['value']=$v;
							$tmpArr['itemid']=$itemid;
							$tmpArr['hotelid']=$data['hotelKey']['hotelid'];
							$tmpArr['key']="id";
							$req->InsertRequest($tmpArr);
							
						} else {
							$sql->db_Insert($value['table']." (roomid,itemid)",$id.",".$v);
						}
			
					}
				}
			}
			
			foreach($data['roomsecondary'] as $key => $value ) {
				
				if ($key == "price") {
					$number = $value / number_format( $this->exchange, 2, '.', '');
					$value =  number_format($number, 2, '.', '');
				}
				
				$args[] = $key."='".$value."'";
			}
			
			$sql->db_Update("rooms_hotel", implode(",",$args)."where room_id=".$id);
		}

		

	}
	
	
	
	
}

?>