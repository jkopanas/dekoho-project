<?php

/**
 * The Data Acces Model of hotel
 * @author Evalon-Acer
 *
 */
class Location extends AbstractDAO
{
	/**
	 *
	 * @var AdvancedSearch
	 */
	private $__moduleName;
	public $__joins;
	public $Location;

	function __construct($settings)
	{
		
		$this->__joins['geoCountry'] = array("table" => "geo_country", "key"=>"id" ,"relKey" => "countryid","refId" => "id_country");
		$this->__joins['geoRegion'] = array("table" => "geo_region", "key"=>"id", "relKey" => "regionid","refId" => "id_region" );
		$this->__joins['geoCity'] = array("table" => "geo_cityr", "key"=>"id", "relKey" => "cityid","refId" => "id_city");
		$this->__joins['geoHotel'] = array("table" => "maps_items", "key"=>"id", "relKey" => "itemid");
		

		$this->__moduleName = $settings['module'];

		$this->addFieldToStructure("id",$this->__moduleName.".`id` as id");
		$this->addFieldToStructure("title",$this->__moduleName.".`title` as title");
		$this->addFieldToStructure("	geocoderAddress",$this->__moduleName.".`	geocoderAddress` as 	geocoderAddress");

	}
	
		
	public function GetPlacesAutocomplete($settings) {
		

		$settings['searchfilters'][] = array(
				'item'     => $this->__joins[$_POST['data']['table']['value']]['table'].".title",
				'type'     => "like",
				'val'      => $_POST['route']['value'],
		);
		

		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$tmpArr['lang'] = $settings['lang'];
		$settings['filters']['groupBy'] = $this->__moduleName.".id";
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		$tmpArr['jointables'] = array_merge(array (),((array)$tmpArr['filters']['jointables']));
		$tmpArr['jointables'] = (isset($settings['overwriteJoin'])) ? $settings['overwriteJoin'] : $tmpArr['jointables'];  
	    $tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr = (array) $resultArr['results'];
		$Location =array();
		foreach($resultArr as $key =>$value) {
 			array_push($Location, array("zoomLevel"=>$value['zoomLevel'],"geocoderAddress"=>$value['geocoderAddress'],"lat"=>$value['lat'],"lng"=>$value['lng'],"title"=>$value['title'], "id" => $value['id']));
		}
		return $Location;
		 
	} 	
	
	public function addPoint ($fields,$settings) {
		
		global $sql;	
		
		$fields['date_added'] = time();

		if ( $settings['action'] == "update" ) {

			foreach ($fields as $key => $value ) {
				$q[]=$key."='".$value."'";
			}
			
			$sql->db_Update($this->__moduleName,implode(",",$q)." where ".$settings['id']."=".$settings['value']);
			return $settings['value'];
			
		} else if ( $settings['action'] == "insert" ) {

			$sql->db_Insert($this->__moduleName." (".implode(",",array_keys($fields)).",location)", "'".implode("','",array_values($fields))."',GeomFromText(CONCAT('POINT(',".$fields['lat'].",' ',".$fields['lng'].",')'))" );
			$id=$sql->last_insert_id;
				
			//echo $this->__moduleName." (".implode(",",array_keys($fields)).",location)", "'".implode("','",array_values($fields))."',GeomFromText(CONCAT('POINT(',".$fields['lat'].",' ',".$fields['lng'].",')'))";
			
			//print_ar($sql);
			return $id;
			
		}
		
	}
	
	public function addPointRelation($ar,$settings) {
		global $sql;

		$sql->db_Select("geo_relations","itemid","itemid=".$settings['id']);
		$row=execute_single($sql);
		if ( $settings['action'] == "update" || $row['itemid'] != "" ) {
			
			foreach($ar as $key => $value ) {
				$q[]=$key."='".$value."'";
			}
			$sql->db_Update("geo_relations", implode(",",$q)." where itemid=".$settings['id']);
			//echo "geo_relations", implode(",",$q)." where itemid=".$settings['id'];
		} else if ($settings['action'] == "insert" ) {//new entry
			
			$sql->db_Insert('geo_relations (itemid,countryid,regionid,cityid) ', $settings['id'].",'".$ar['countryid']."','".$ar['regionid']."','".$ar['cityid']."'");	
		//	echo 'geo_relations (itemid,countryid,regionid,cityid) ', $settings['id'].",'".$ar['countryid']."','".$ar['regionid']."','".$ar['cityid']."'";
		//		print_ar($sql);
		}
		
		return true;
		
	}
	
	public function delete($id) {
		
		global $sql;
		
		$sql->db_Delete($this->__moduleName, "id=".$id);
		return true;
		
	}
	
	public function GetLocation ($settings) {
		
		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$settings['filters']['sort_field']=$settings['filters']['table'].".id";
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['withoutJoin'] = $settings['filters']['withoutJoin'];
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		
		
		$tmpArr['jointables'] = array_merge(array (),((array)$tmpArr['filters']['jointables']));

		$tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);

		$resultArr['results'] = (array) $resultArr['results'];
		
		
		return $resultArr;		
		
		
	}
	
	
	public function getHotelLocations($settings) {
	
		
		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$settings['filters']['sort_field']="geo_relations.id";
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['table'] = "geo_relations";
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
	
		$tmpArr['jointables'] = array_merge(array (
				array ('name' => "geo_country","idKey" => "geo_country.id","idRel" => "geo_relations.countryid","joinType" => " LEFT JOIN" ),
				array ('name' => "geo_region","idKey" => "geo_region.id","idRel" => "geo_relations.regionid","joinType" => " LEFT JOIN" ),
				array ('name' => "geo_cityr","idKey" => "geo_cityr.id","idRel" => "geo_relations.cityid","joinType" => " LEFT JOIN" )
		),((array)$tmpArr['filters']['jointables']));
	
		$tmpArr['jointables'] = (isset($settings['overwriteJoin'])) ? $settings['overwriteJoin'] : $tmpArr['jointables'];
		//$tmpArr['filters']['execute'] = ($tmpArr['filters']['execute']? $tmpArr['filters']['execute']: "searchItem");
		$tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		
		if (!$resultArr['results']) {
			$resultArr['results']=array();
		}
		$resultArr['results'] = (array) $resultArr['results'];

		
		return $resultArr;
	
	}
	
	


}
?>