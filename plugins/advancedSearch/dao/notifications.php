<?php

class Notifications extends AbstractDAO
{
	/**
	 *
	 * @var AdvancedSearch
	 */
	private $__moduleName;

	function __construct()
	{
		
		$this->__moduleName = "users_notification";
			
		$this->addFieldToStructure("id","users_notification.`id` as id");
		$this->addFieldToStructure("status","users_notification.`status` as status");
		$this->addFieldToStructure("settings","users_notification.`settings` as settings");
		$this->addFieldToStructure("date_added","users_notification.`date_added` as date_added");
		$this->addFieldToStructure("title","users_notification.`title` as title");
		$this->addFieldToStructure("description","users_notification.`description` as description");
		$this->addFieldToStructure("orderby","orders.`id` as id");
		
		
	}

	
	function getNotifications($settings)
	{
		
		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$settings['filters']['sort_field']="users_notification.id";
		$settings['filters']['perPage'] = 10;
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		
		$tmpArr['jointables'] = array_merge(array (
			//array('name'=>"orders_types",	"idKey"=>"orders_types.id",		"idRel"=>"orders.typeid",	"joinType"=>"INNER JOIN"),
		),((array)$tmpArr['filters']['jointables']));
		$tmpArr['jointables'] = (isset($settings['overwriteJoin'])) ? $settings['overwriteJoin'] : $tmpArr['jointables'];
	
		$tmpArr['filters']['execute'] = "searchBySQL";

		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
		
		return $resultArr;
		
	}
	
	function getNotificationsDetails($settings)
	{
	
		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$settings['filters']['sort_field']="users_notification.id";
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
	
		$tmpArr['jointables'] = array_merge(array (
				//array('name'=>"orders_types",	"idKey"=>"orders_types.id",		"idRel"=>"orders.typeid",	"joinType"=>"INNER JOIN"),
		),((array)$tmpArr['filters']['jointables']));
		$tmpArr['jointables'] = (isset($settings['overwriteJoin'])) ? $settings['overwriteJoin'] : $tmpArr['jointables'];
	
		$tmpArr['filters']['execute'] = "searchBySQL";
	
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
	
		return $resultArr;
	
	}
	
	
	function DeleteNotifications($data) {
	
		global $sql;
		$sql->db_Delete("users_notification","uid=".ID." AND id in (".$data.")");	
		return explode(",",$data);
	
	}
	
	function SendNotifications($data) {
	
		global $sql;
		
		$t = new Parse();
		$date_added = time();
		$uid=($data['uid']) ? $data['uid']: ID;
		
		$sql->db_Insert("users_notification (id,uid,title,description,date_added,status)"," '',".$uid.",'".$t->toDB($data['title'])."','".$t->toDB($data['description'])."',".$date_added.",0");
		return json_encode(array("id"=> $sql->last_insert_id),true);
	
	}	
	
}

?>