<?php

/**
 * The Data Acces Model of hotel
 * @author Giannis Kopanas
 *    
 *    0 started
 *    1 completed
 *    2 pending
 *    3 deleted
 *    4 refund
 */
class Orders extends AbstractDAO
{
	/**
	 *
	 * @var AdvancedSearch
	 */
	private $__moduleName;

	function __construct()
	{
		
		$this->__moduleName = "orders";
			
		$this->addFieldToStructure("id","orders.`id` as id");
		$this->addFieldToStructure("billing_profile_id","orders.`billing_profile_id` as billing_profile_id");
		$this->addFieldToStructure("status","orders.`status` as status");
		$this->addFieldToStructure("archive","orders.`archive` as archive");
		$this->addFieldToStructure("settings","orders.`settings` as settings");
		$this->addFieldToStructure("date_added","orders.`date_added` as date_added");
		$this->addFieldToStructure("date_modified","orders.`date_modified` as date_modified");
		$this->addFieldToStructure("orderby","orders.`id` as id");
		
		
	}

	
	function getOrders($settings)
	{
		
		
		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$settings['filters']['sort_field']="orders.id";
		$settings['filters']['perPage'] = 10;
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		
		
		$tmpArr['jointables'] = array_merge(array (
			array('name'=>"orders_types",	"idKey"=>"orders_types.id",		"idRel"=>"orders.typeid",	"joinType"=>"INNER JOIN"),
		),((array)$tmpArr['filters']['jointables']));
		
		$tmpArr['jointables'] = (isset($settings['overwriteJoin'])) ? $settings['overwriteJoin'] : $tmpArr['jointables'];
	
		$tmpArr['filters']['execute'] = ($tmpArr['filters']['execute']? $tmpArr['filters']['execute']: "searchBySQL");

		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
		
		// Parse JSON Values
		foreach ($resultArr['results'] as &$vArr)
		{
			$tmpID[]=$vArr['id'];
			@isJSON($vArr['settings'],$vArr['settings'],true);
		}
		
		return $resultArr;
		
	}
	
	
	function getOfferOrders($settings) {
		
		$settings['searchfilters'][] = array(
				'item'     => "module",
				'type'     => "eq",
				'val'      => "rooms",
		);
		$settings['searchfilters'][] = array(
				'item'     => "uid",
				'type'     => "eq",
				'val'      => ID,
		);
		$tmpArr = array();
		
		$tmpArr['debug'] = 0;
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['withoutJoin'] = $settings['filters']['withoutJoin'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		
		$tmpArr['jointables'] = array_merge(array (
				array('name'=>"rooms_hotel",	"idKey"=>"rooms_hotel.room_id",		"idRel"=>"orders.itemid",			"joinType"=>"JOIN"),
		),((array)$tmpArr['filters']['jointables']));

		
		//$tmpArr['filters']['execute'] = ($tmpArr['filters']['execute']? $tmpArr['filters']['execute']: "searchItem");
		$tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
		
		// Parse JSON Values
		
		foreach ($resultArr['results'] as &$vArr)
		{
			$tmpID[]=$vArr['id'];
			@isJSON($vArr['settings'],$vArr['settings'],true);
		}
		
		return $resultArr;
	}
	
	
}

?>