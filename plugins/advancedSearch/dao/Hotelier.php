<?php

/**
 * The Data Acces Model of hotel
 * @author Evalon-Acer
 *
 */
class Hotelier extends user
{
//changed to public by chris ?? private//

	public $fields = array();
	public $billing_fields = array();
	
	public function __construct() {
		
		$this->tables['payment'] = array('table'=>'users_payment_method','key'=>'id');
		$this->tables['billing'] = array('table'=>'users_profile_billing','key'=>'id');
		
		$this->fields = array (
				"profile" => array (
						"facebook_uid" => "",
						"twitter_uid" => "",
						"gender" => "",
						"company_name" => "",
						"user_telephone" => "",
						"city" => "",
						"state" => "",
						"zip_code" => "",
						"settings" => array (
								"email2" => "",
								"fax" => "",
								"address" => "" 
						) 
				)
		);

/**
 * 
 * fields in billing profile  chris
 *
 */
		$this->billing_fields = array (
				"billing_profiles" => array (
						"operating_company"=>"",
						"activity" => "",
						"first_name" => "",
						"last_name" => "",
						"city" => "",
						"telephone" => "",
						"invoice_email" => "",
						"settings" => array (
							"address"=>"",
							"commercial_re_no" => "",
							"email_notification" => "",
							"fax" => "",
							"state" => "",
							"tax_no" => "",
							"tax_office" => "",
							"zip_code" => "",
						)
				)
		
		);
		
		$this->user();
	}
		
	public function GetHotelier($settings) {
		
		$set=array();
		foreach($settings['search'] as $key => $value) {
			foreach($value as $k => $v) {
				$set['searchFields'][$k] = $v;
			}
		}
	
		$set['JoinInner'] = $settings['JoinInner'];
		$set['return'] = 'multi';
		$set['profile']= ($settings['profile']) ? $settings['profile'] : 0;
		
		$set['fields'] = (is_array($settings['fields']))? implode(",",$settings['fields']) : "users.*";
		$results = $this->searchUsers($set);	

		if ($results[0]["details"]['profile']) {
			$results[0]["details"]['profile'] = $results[0]["details"]['profile'] + $this->fields['profile'];
			$results[0]["details"]['profile']['settings'] = $results[0]["details"]['profile']['settings'] + $this->fields['profile']['settings'];
		}
		return $results;
		
	} 	
	
	
	public function GetHotelierBillingProfile($settings) {
		$set=array();
		foreach((array)$settings['search'] as $key => $value) {
			foreach((array)$value as $k => $v) {
				$set['searchFields'][$k] = $v;
			}
		}
		
		$set['JoinInner'] = $settings['JoinInner'];
		$set['return'] = 'multi';
		$set['profile'] = ($settings['billing']) ? $settings['billing'] : 0;
		$set['debug'] = ($settings['debug']) ? $settings['debug'] : 0; 
		$set['fields'] = (is_array($settings['fields'])) ? implode(",",$settings['fields']) : "users.*";
		$results = $this->searchUsers($set);
	    
	    if (is_array($results[0])) {
                 foreach ($results as &$vArr)
                 {
                 	if (!isJSON($vArr['settings'],$vArr['settings'])) {
                    	$vArr = @json_decode(base64_decode($vArr['settings']),true);
        	        }
                 }
        } else {
               	return array();
        }

		return $results;
	}
	
	public function UpdateAccount($data) {
		
		$tmpArr['field']=$data;
		//$tmpArr['field']['id']=ID;
		$this->ModifyUserProfile(ID,array("users_profile"=>$tmpArr));
		
	}
	
	
	public function UpdatePayment() {

		$this->ModifyUserProfile(0,"users_payment_method",$data);
		
	}
	

	public function UpdateBillingProfile($data) {
		$tmpArr['field']=$data['billing'];
		if (is_numeric($data['id'])) {
			$id=0;
			$tmpArr['field']['id']=$data['id'];
		} else {
			$id=ID;
		}

		return $this->ModifyUserProfile($id,array("users_profile_billing"=>$tmpArr));
		
	}
	

}
?>