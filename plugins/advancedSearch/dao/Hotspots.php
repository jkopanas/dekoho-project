<?php

/**
 * The Data Acces Model of hotel
 * @author Evalon-Acer
 *
 */
class Hotspots extends AbstractDAO
{
	/**
	 *
	 * @var AdvancedSearch
	 */
	private $__moduleName;
	private $__joins;
	private $checkadmin=false;

	function __construct()
	{
		$this->__moduleName = "hotspots";
		
		$this->addFieldToStructure("id","hotspots.`id` as id");
		$this->addFieldToStructure("key","hotspots.`key` as `key`");
		$this->addFieldToStructure("title","hotspots.`title` as title",1);
		$this->addFieldToStructure("description","hotspots.`description` as description",1);
		$this->addFieldToStructure("active","hotspots.`active` as active");
		$this->addFieldToStructure("settings","hotspots.`settings` as settings",0);
		$this->addFieldToStructure("permalink","hotspots.`permalink` as permalink","1");
		$this->addFieldToStructure("date_added","hotspots.`date_added` as date_added");
		$this->addFieldToStructure("date_modified","hotspots.`date_modified` as date_modified");
		$this->addFieldToStructure("comments_count","hotspots.`comments_count` as comments_count");
		$this->addFieldToStructure("orderby","hotspots.`id` as id");
		$this->addFieldToStructure("description_long","hotspots.`description_long` as description_long");
		$this->addFieldToStructure("lang","hotspots.`lang` as lang");

	}
	
	public function getHotspots($settings) {
		
		$settings['searchfilters'][] = array(
				'item'     => "maps_items.module",
				'type'     => "eq",
				'val'      => "hotspots",
		);
		$tmpArr = array();
		
		$tmpArr['debug'] = $settings['debug'];
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		
		$tmpArr['jointables'] = array_merge(array (
				array('name'=>"maps_items",	"idKey"=>"maps_items.itemid",		"idRel"=>"hotspots.id",			"joinType"=>"LEFT JOIN"),
		),((array)$tmpArr['filters']['jointables']));
		
		
		$tmpArr['filters']['execute'] = ($tmpArr['filters']['execute']? $tmpArr['filters']['execute']: "searchItem");
		$tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
		// Parse JSON Values
		if (is_array($resultArr['results'][0])) {
		foreach ($resultArr['results'] as &$vArr)
		{
			$tmpID[]=$vArr['id'];
			@isJSON($vArr['settings'],$vArr['settings'],true);
		}
		} else { return array("results"=>array()); }
		
		return $resultArr;
	}
	
	public function UpdateMapHotspots($data) {
		
		global $sql;
		$posted_data=$data['data'];

		$fields['id'] = "'".$posted_data['id']."'";
		$fields['itemid'] = "'".$posted_data['itemid']."'";
		$fields['module'] = "'".$posted_data['module']."'";
		$toUpdate['title'] = $fields['title'] = "'".$posted_data['title']."'";
		$toUpdate['content'] = $fields['content'] = "'".$posted_data['content']."'";
		$toUpdate['geocoderAddress'] = $fields['geocoderAddress'] = "'".$posted_data['geocoderAddress']."'";
		$toUpdate['lat'] = $fields['lat'] = "'".$posted_data['lat']."'";
		$toUpdate['lng'] = $fields['lng'] = "'".$posted_data['lng']."'";
		$toUpdate['location'] = $fields['location'] = "GeomFromText(CONCAT('POINT(',".$posted_data['lat'].",' ',".$posted_data['lng'].",')'))";
		$toUpdate['zoomLevel'] = $fields['zoomLevel'] = "'".$posted_data['zoomLevel']."'";
		$toUpdate['MapTypeId'] = $fields['MapTypeId'] = "'".$posted_data['MapTypeId']."'";
		$toUpdate['settings'] = $fields['settings'] = "'".$posted_data['settings']."'";
		$fields['date_added'] = "'".time()."'";
		$posted_data['itemid'] = ($posted_data['itemid']) ? $posted_data['itemid'] : 0;
		

		$sql->db_Select("maps_items","id","itemid = ".$posted_data['itemid']." AND  module = '".$posted_data['module']."'");
		//echo "maps_items","id","itemid = ".$posted_data['itemid']." AND  module = '".$posted_data['module']."'";
		if ($sql->db_Rows()) { //UPDATE
			$r = execute_single($sql);
			foreach ($toUpdate as $k=>$v)
			{
				$q[] = "$k = $v";
			}
			$sql->db_Update("maps_items",implode(" , ",$q)." WHERE id = ".$r['id']);
			//echo "maps_items",implode(" , ",$q)." WHERE id = ".$r['id'];
			//print_ar($sql);
		} //END UPDATE
		else {//NEW - INSERT
			unset($fields['id']);
			foreach ($fields as $k=>$v){
				$keys[] = $k;
			}
			$fields = implode(",",$fields);
			$keys = implode(',',$keys);
			$sql->db_Insert("maps_items ($keys)",$fields);
					//echo "INSERT INTO maps_items ($keys) VALUES ($fields)";
		}//END NEW
		
		return;
		
	}
	
	
	public function DeleteHotspots($data) {
		
		global $sql;
		
		$sql->db_Delete("hotspots","id IN (".implode(",",$data).")");
		$sql->db_Delete("maps_items","itemid in (".implode(",",$data).") and module='hotspots'");
		
		///// Delete My Request ////////
		$req = new UpdateItem(array("table"=>"hotspots"));
		foreach ($data as $key => $value) {
			$req->DeleteRequest(array("id"=> $value ));
		}
		///// Delete My Request ////////
		
	}
	
	public function UpdateHotspot($data,$id) {

		
			global $sql;
			$tmp=$data['hotspot'];
			$ar = array();
			foreach ($tmp as $key => $value) {
				if ($this->__forceAdminCheck[$key] && USER_CLASS != "A") {
					$ar[$key] = $value;
					unset($tmp[$key]);
				}
			}
			if (!empty($tmp)) {
			$hotspotsid=AdvancedSearch::getInstance($this->__moduleName)->ItemModify($tmp,$id);
			}
			
			$req=new UpdateItem(array("table"=> $this->__moduleName));
			
			
			foreach ($ar as $key => $value) {
				$tmpArr['field']=$key;
				$tmpArr['value']=$value['value'];
				$tmpArr['itemid']=$id;
				$tmpArr['key']="id";
				$req->InsertRequest($tmpArr);
			}

			return $id;
	}

	
	public function NewHotspot($data) {

		$tmp=$data['hotspot'];
		$ar= array();
		foreach ($tmp as $key => $value) {
			if ($this->__forceAdminCheck[$key] && USER_CLASS != "A" ) {
				unset($tmp[$key]);
				$ar[$key] = $value;
			}
		}
	
		$tmp['uid']['value']=ID;
		$hotspotid = AdvancedSearch::getInstance($this->__moduleName)->ItemAdd($tmp);
		
		
		$req=new UpdateItem(array("table"=> $this->__moduleName));
		
		
		foreach ($ar as $key => $value) {
			$tmpArr['field']=$key;
			$tmpArr['value']=$value['value'];
			$tmpArr['itemid']=$hotspotid;
			$tmpArr['key']="id";
			$tmpArr['hotelid']=$tmp['hotel_id']['value'];
			$req->InsertRequest($tmpArr);
		}
		
		return $hotspotid;
		
	}

	
	public function GetExtraFields ($id) {
	
		global $loaded_modules;
		$current_module = $loaded_modules["hotel"];
		$Efields = new ExtraFields(array('module'=>$current_module));
	
		$groups = $Efields->searchGroups(array('debug'=>0,'searchFields'=>array('module'=>$current_module['name'])));
	
		foreach ((array) $groups as $key => $value) {
			$AllExtraFields[$value['title']] = $Efields->GetExtraFields(array('debug'=>0,'match_values'=>1,'itemid' => $id,'groupid' => $value['id'],'page'=> 1,'results_per_page'=>1000,'way'=>'ASC'));
		}
	
		return $AllExtraFields;
	
	}
	
}

?>