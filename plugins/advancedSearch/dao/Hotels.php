<?php

/**
 * The Data Acces Model of hotel
 * @author Evalon-Acer
 *
 */
class Hotels extends AbstractDAO
{
	/**
	 *
	 * @var AdvancedSearch
	 */
	protected $__moduleName;
	public $__joins;
	private $checkadmin=false;
	public $extraTable = array();

	function __construct()
	{
		$this->exchange = $_SESSION['exchange'];
		
		$this->__moduleName = "hotel";
		
		$this->addFieldToStructure("id","hotel.`id` as id");
		$this->addFieldToStructure("key","hotel.`key` as `key`");
		$this->addFieldToStructure("title","hotel.`title` as title",1);
		$this->addFieldToStructure("description","hotel.`description` as description",1);
		$this->addFieldToStructure("active","hotel.`active` as active");
		$this->addFieldToStructure("settings","hotel.`settings` as settings",0,array("totalRooms"=>"","chain" => "","hotelcontactperson"=>"","position"=>"","hoteltelephone2"=>"","hoteltelephone"=>"","hotelfax"=>"","paypalmail"=>"","hotelmail"=>"","hotelaccountowner"=>"","hotelaccountaddress"=>"","iban"=>"","bankname"=>"","bankaddress"=>"","bic"=>"","hotelmobile"=>""));
		$this->addFieldToStructure("permalink","hotel.`permalink` as permalink",1);
		$this->addFieldToStructure("date_added","hotel.`date_added` as date_added");
		$this->addFieldToStructure("date_modified","hotel.`date_modified` as date_modified");
		$this->addFieldToStructure("comments_count","hotel.`comments_count` as comments_count");
		$this->addFieldToStructure("orderby","hotel.`id` as id");
		$this->addFieldToStructure("description_long","hotel.`description_long` as description_long");
		$this->addFieldToStructure("lang","hotel.`lang` as lang");
		$this->addExtraFieldToStructure("enable","hotel.`enable` as `enable`");
		$this->addExtraFieldToStructure("stars","hotel.`stars` as `stars`");
		$this->addExtraFieldToStructure("pets","hotel.`pets` as `pets`");
		
		
		$this->__joins["facilities"]= array("table" => "hotel_facilities", "key"=>"hotelid", "field"=>"hotelid" , "relId"=>"itemid");
		$this->__joins["services"]=array("table" => "hotel_services", "key"=>"hotelid", "field"=>"hotelid" ,"relId"=>"itemid" );
		$this->__joins["themeshotels"]= array("table" => "hotel_themeshotels", "key"=>"hotelid", "field"=>"hotelid" ,"relId"=>"itemid");
		$this->__joins["types"]= array("table" => "hotel_types", "key"=>"hotelid","field"=>"hotelid" , "relId"=>"itemid");
		$this->__joins["roomsfacilities"]= array("table" => "hotel_roomfacilities", "key"=>"hotelid","field"=>"hotelid" , "relId"=>"itemid");
		
		$this->extraTable = array ( 
				array("table" => "rooms" , "field"=> "hotelKey"),
				array("table" => "hotspots" , "field"=> "hotel_id"),
				array("table" => "rooms_hotel" , "field"=> "hotel_id"),
				array("table" => "geo_relations" , "field"=> "itemid"),
				array("table" => "users_request" , "field"=> "hotelid")
		);
		
		foreach ($this->__joins as $key => $value) {
				$this->extraTable = array_merge($this->extraTable,array($value));
		}
		
	}

	/**
	 * Get hotel by settings
	 * <pre>
	 * $settings['filters']         = array(
	 *     'id'                => $id,
	 *     'inDiscount'        => 0, // to fetch Hotels in Discount only
	 *     'priceRange'        => array(MIN,MAX),
	 *     'pricePromoRange'   => array(MIN,MAX),
	 *     'categoryid'        => $categoryid, // to fetch Category Path
	 *     'return'            => 'paginated', //or null
	 *     'perPage'           => ($perPage? $perPage : 40),
	 *     'page'              => ($page ? $page : 1 ),
	 *     'archive'           => ($archived? 1 : 0 ),
	 *     'lang'              => ($lang? $lang : DEFAULT_LANG),
	 *     'date-from'         => $dateFrom,
	 *     'date-to'           => $dateTo,
	 *     'sort_field'        => $sortField,
	 *     'sort_direction'    => $sortDir,
	 *     'providerid'        => $providerid,
	 *     'user'              => $user,
	 *     'efield'            => $efield,
	 *     'evalue'            => $evalue,
	 *     'exclude'           => 'id,id,id',//to excludde
	 *     'categoryies'       => array( // To Join content by categories
	 *         array(
	 *             'categoryid'    => $categoryid
	 *         )
	 *     ),
	 * );
	 * $settings['searchfilters']     = array(
	 *     array(
	 *         'item'     => 'id',
	 *         'type'     => 'eq',
	 *         'val'      => 0,
	 *     )
	 * );
	 * </pre>
	 * @param array $settings
	 * @return array array('total'=>$total,'results'=>$search_results,'data'=>$posted_data);
	 */
	function getHotel($settings)
	{
		
		$settings['searchfilters'][] = array(
				'item'     => "uid",
				'type'     => "eq",
				'val'      => ID,
		);
		
		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$settings['filters']['sort_field']="hotel.id";
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		$tmpArr['jointables'] = array_merge(array (),((array)$tmpArr['filters']['jointables']));
		$tmpArr['jointables'] = (isset($settings['overwriteJoin'])) ? $settings['overwriteJoin'] : $tmpArr['jointables'];  
	
		$tmpArr['filters']['execute'] = ($tmpArr['filters']['execute'] ? $tmpArr['filters']['execute']: "searchItem");
	    //$tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
		
		// Parse JSON Values		
		$settings['filters']['hotelProperties'] = (!$settings['filters']['hotelProperties']) ? false : true;
		if ($settings['filters']['hotelProperties']) {
			foreach( $this->__joins as $key => $value ) {
				$resultArr['results'][0][$key]=$this->getHotelProperties($key,$value,$resultArr['results'][0]['id']);
			}
		}

	 if (is_array($resultArr['results'][0])) {
		foreach ($resultArr['results'] as &$vArr)
		{
			$vArr['price'] = $this->CalculateCurrency($vArr['price']);
			$vArr['discount'] = $this->CalculateCurrency($vArr['discount']);
			if (!@isJSON($vArr['settings'],$vArr['settings'],true)) {
				$vArr['settings'] = json_decode(base64_decode($vArr['settings']),true);
            }
		 }

		return $resultArr;
		
	} else {
		return array( "results" => "" );
	}
}	
	public function getPropertiesHotelFront($settings) {
		


		$tmpArr ['debug'] = 0;
		foreach ( $this->__joins as $key => $value ) {


			$tmpArr ['filters'] ['sort_field'] = $value ['table'] . ".id";
			$tmpArr ['filters'] ['table'] = $value ['table'];
			$tmpArr ['searchfilters'] = $settings ['searchfilters'];
			$tmpArr ['filters'] ['fields'] = array (
					$value ['table'] . '.id' ,$key.".title",$key.".description"
			);
				
			$tmpArr ['jointables'] = array_merge ( array ( array (
					'name' => $key,
					"idKey" => $key . ".id",
					"idRel" => $value ['table'] . ".itemid",
					"joinType" => "INNER JOIN" 
			)), (( array ) $tmpArr ['filters'] ['jointables']) );
			
			
			if(LANG != DEFAULT_LANG)
			{
				$tmpArr ['filters'] ['fields'][]="translations.translation as transvalue";
				$tmpArr ['jointables'] = array_merge ( array ( 
						array (
								'name' => $key,
								"idKey" => $key . ".id",
								"idRel" => $value ['table'] . ".itemid",
								"joinType" => "INNER JOIN"
						),	
						array (
								'name' => "translations",
								"idKey" => "translations.itemid",
								"idRel" => $key . ".id and translations.code='".LANG."'",
								"joinType" => "LEFT JOIN"
						)
						), (( array ) $tmpArr ['filters'] ['jointables']) );
			}
			
			$tmpArr ['filters'] ['execute'] = "searchBySQL";
			$resultArr = array ();
			$resultArr = AdvancedSearch::getInstance ( $this->__moduleName )->{$tmpArr ['filters'] ['execute']} ( $tmpArr );
			


			if ( is_array($resultArr ['results'][0]) && LANG != DEFAULT_LANG ) {
				foreach($resultArr ['results'] as $k => &$v) {
					if (!empty($v['transvalue'])) {
					$v['title'] = $v['transvalue'];
					} 
				}
			}

			$ar[$key] = (array) $resultArr ['results'];
			
		}
		return $ar;
	}
	
	public function getHotelProperties ($table,$settings,$id) {
		
		$settings['searchfilters'][] = array(
				'item'     => "1",
				'type'     => "eq",
				'val'      => 1,
		);
	
		$tmpArr['debug']=0;	
		$tmpArr['filters']['sort_direction']="ASC";
		$tmpArr['filters']['sort_field'] =$table.".title";
		$tmpArr['filters']['table'] = $table;
		$tmpArr['filters']['jointables'] = $settings['jointables'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		//$tmpArr['filters']['fields'] = array($table.'.id','@ids := id as ids','title','enable','(select hotelid from '.$this->__joins[$table]['table'].' where itemid=@ids and hotelid='.$id.') as hotelid');
		$tmpArr['filters']['fields'] = (isset($settings['filters']['fields'])) ?  $settings['filters']['fields'] : array($table.'.id','@ids := id as ids','title','enable','(select hotelid from '.$this->__joins[$table]['table'].' where itemid=@ids and hotelid='.$id.') as hotelid');
		
		$tmpArr['jointables'] = array_merge(array(),((array)$tmpArr['filters']['jointables']));
		
		$tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
		return $resultArr['results'];
		
	}

	public function getHotelByUser ($settings) {
		
		
		$settings['searchfilters'][] = array(
				'item'     => "hotel.uid",
				'type'     => "eq",
				'val'      => ID,
		);
		
		$tmpArr = array();
		$tmpArr['debug'] = $settings['debug'];
		$settings['filters']['groupBy'] = "hotel.id";
		$settings['filters']['sort_field']="hotel.id";
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['withoutJoin'] = $settings['filters']['withoutJoin'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		
		$tmpArr['jointables'] = array_merge(array (
				/*array('name'=>"users",	"idKey"=>"users.id","idRel"=>"hotel.uid",	"joinType"=>"INNER JOIN"),*/
				array('name'=>"maps_items","idKey"=>"maps_items.itemid","idRel"=>"hotel.id and maps_items.module='hotel'","joinType"=>"LEFT JOIN")
		),((array)$tmpArr['filters']['jointables']));
		
		//$tmpArr['filters']['execute'] = ($tmpArr['filters']['execute']? $tmpArr['filters']['execute']: "searchItem");
		$tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
		
		
		if (is_array($resultArr['results'][0])) {
			
			foreach ($resultArr['results'] as &$vArr)
			{	
				if (!@isJSON($vArr['settings'],$vArr['settings'],true)) {
					$vArr['settings'] = json_decode(base64_decode($vArr['settings']),true);
				}
			}
		} else { return array("results"=>array()); }
		return $resultArr;
		
	}
	
	public function getHotelHotspots($settings) {
		
		$settings['searchfilters'][] = array(
				'item'     => "maps_items.module",
				'type'     => "eq",
				'val'      => "hotspots",
		);
		$tmpArr = array();
		
		$tmpArr['debug'] = $settings['debug'];
		$tmpArr['lang'] = $settings['lang'];
		$tmpArr['filters'] = $settings['filters'];
		$tmpArr['searchfilters'] = $settings['searchfilters'];
		$tmpArr['filters']['withoutJoin'] = $settings['filters']['withoutJoin'];
		$tmpArr['filters']['module'] = $this->__moduleName;
		$tmpArr['filters']['fields'] = (is_array($tmpArr['filters']['fields'])? $tmpArr['filters']['fields'] : array());
		foreach ($tmpArr['filters']['fields'] as &$vArr)
		{
			$vArr = $this->getFiledSQLByKey($vArr);
		}
		
		$tmpArr['jointables'] = array_merge(array (
				array('name'=>"hotspots",	"idKey"=>"hotspots.hotel_id",		"idRel"=>"hotel.id",			"joinType"=>"JOIN"),
		),((array)$tmpArr['filters']['jointables']));
		
		
		//$tmpArr['filters']['execute'] = ($tmpArr['filters']['execute']? $tmpArr['filters']['execute']: "searchItem");
		$tmpArr['filters']['execute'] = "searchBySQL";
		$resultArr = array();
		$resultArr = AdvancedSearch::getInstance($this->__moduleName)->{$tmpArr['filters']['execute']}($tmpArr);
		$resultArr['results'] = (array) $resultArr['results'];
		
		// Parse JSON Values
		
		foreach ($resultArr['results'] as &$vArr)
		{
			$tmpID[]=$vArr['id'];
			@isJSON($vArr['settings'],$vArr['settings'],true);
		}
		
		return $resultArr;
	}
	
	public function getHotelByRoom($settings) {
	
	
		$tmpObj = new Rooms();
		$tmpArr =$tmpObj->getRoom($settings);
		
		return $tmpArr;
	}	
	
	
	public function getHotelRooms($settings) {


		$tmpObj = new Rooms();
		$tmpArr =$tmpObj->getRoom($settings);
		return $tmpArr;
	}
	
	
public function getHotelSpecialOffers($settings) {
		
		$allHotel =array("");
		$allHotel=$this->getHotel($settings);
		
		if (empty($allHotel['results'])) { return array("results" => array());	}

		$settings=array();
		$settings['searchfilters'][] = array(
				'item'     => "orders.uid",
				'type'     => "eq",
				'val'      => ID,
		);
		$settings['debug']=0;
		$settings['filters'] = array (
				'groupBy' => "rooms.hotelKey",
				'fields'		=> array(
						'rooms.id as rid',
						'hotelKey',
						'status',
						'SUM(IF(status = "1", 1,0))  as active',
						'SUM(IF(status = "2", 1,0))  as inactive'
				),
				'jointables' => array(
						array(
									'name'=>"rooms",
									"idKey"=>"rooms.id",
									"idRel"=>"orders.itemid",
									"joinType"=>"INNER JOIN"
						)
				)
		);
		
		$tmpObj=new Orders();
		$tmpArr=$tmpObj->getOrders($settings);
		foreach((array)$tmpArr['results'] as $key => $value) {
			$tmpRoom[$value['hotelKey']]=$value;
		}
		

		$req=new UpdateItem(array("table"=> "hotel"));
		
		if (is_array($allHotel['results'][0])) {
			foreach($allHotel['results'] as $key => &$value) {
				$value['activeOffer']=$tmpRoom[$value['id']]['active'];
				$value['inactiveOffer']=$tmpRoom[$value['id']]['inactive'];
				list($reqRes,$REqField)=$req->GetRequest($value['id'],array("getFields" => 1));
				$value = array_merge ($value,(array) $reqRes[$value['id']]);
			}

		}
	
		return $allHotel;

	  }
		
	public function GetRel ($table) {
		
		if ($this->__moduleName == $table ) {
			
			return array("table" => $table,"field"=>"id");
		} else {
			
			return array( "table" => $table,"field"=> "" );
			
		}
		
		
	}
	
	public function NewHotel($data) {
		
		global $sql;
	
		$tmp=$data['hotel'];
		$ar= array();
	    foreach ($tmp as $key => $value) {
			if ($this->__forceAdminCheck[$key] && USER_CLASS != "A" ) {  
				unset($tmp[$key]); 
				$ar[$key] = $value;
			}
		}
		
		$tmp['active'] = array("value" => 1);
		$tmp['enable'] = array("value" => 0);
		$tmp['uid'] = array("value" => ID);
		$hotelid = AdvancedSearch::getInstance($this->__moduleName)->ItemAdd($tmp);
		
		$req=new UpdateItem(array("table"=> $this->__moduleName));
		
		
		foreach ($ar as $key => $value) {
			$tmpArr['field']=$key;
			$tmpArr['value']=$value['value'];
			$tmpArr['itemid']=$hotelid;
			$tmpArr['hotelid']=$hotelid;
			$tmpArr['key']="id";
			$req->InsertRequest($tmpArr);
		}
		
		if (is_numeric($hotelid)) {
		
		foreach($this->__joins as $key => $value ) {

			if (isset($data['hotelProperties'][$key])) {
				foreach($data['hotelProperties'][$key] as $k => $v ) {		
					if (!is_numeric($v)) {
						$sql->db_Insert($key." (title,enable,description)","'".$v."',0,''");
						$sql->db_Insert($value['table']." (hotelid,itemid)","$hotelid,$sql->last_insert_id");
						
						$req=new UpdateItem(array("table"=> $key));
						$tmpArr['field']="title";
						$tmpArr['value']=$v;
						$tmpArr['itemid']=$sql->last_insert_id;
						$tmpArr['hotelid']=$hotelid;
						$tmpArr['key']="id";
						$req->InsertRequest($tmpArr);
						
					} else {
						$sql->db_Insert($value['table']." (hotelid,itemid)","$hotelid,$v");
					}
				}
			}
		}

	}
	return $hotelid;
}	

	public function DashboardHotel($data) {
		global $sql;
		
		$sql->db_Select($this->__moduleName,"id","id in (".implode(",",$data['data']).")");
		if ( $sql->mySQLrows == count($data['data']) ) {
			if ($data['mode'] =="deleted") {
				$this->DeleteHotel($data['data']);
			} else if ($data['mode'] =="activate" || $data['mode'] =="deactivate") {		
				$this->ModuleActions($data,array("extraTables" => $this->extraTable));
			}
		}
	}
	
	public function DeleteHotel($data) {
	
		global $sql;
	
		
		//geo_relations
		$sql->db_Delete("geo_relations","itemid in (".implode(",",$data).")");
		//filters
	$sql->db_Delete("hotel_roomfacilities","hotelid in (".implode(",",$data).")");
		$sql->db_Delete("hotel_services","hotelid in (".implode(",",$data).")");
		$sql->db_Delete("hotel_themeshotels","hotelid in (".implode(",",$data).")");
		$sql->db_Delete("hotel_types","hotelid in (".implode(",",$data).")");
		$sql->db_Delete("hotel_facilities","hotelid in (".implode(",",$data).")");
		
		// main deletes
		$sql->db_Delete("hotel","id in (".implode(",",$data).")");
		$this->DeleteImage($data);
		$sql->db_Delete("maps_items","itemid in (".implode(",",$data).") and module='".$this->__moduleName."'");
		$sql->db_Delete("extra_field_values","itemid in (".implode(",",$data).")");
		$sql->db_Delete("users_request","hotelid in (".implode(",",$data).")");
		
		
		$sql->db_Select("hotspots","id","hotel_id in (".implode(",",$data).")");
		$row=execute_multi($sql);
		
		foreach($row as $key => $value) {
			$hotspotsid[]=$value['id'];
		}
		
		$rm = new Hotspots();
		$rm->DeleteHotspots($hotspotsid);
		
		$sql->db_Select("rooms_hotel","room_id","hotel_id in (".implode(",",$data).")");
		$row=execute_multi($sql);
		
		foreach($row as $key => $value) {
			$roomid[]=$value['room_id'];
		}
		
		$rm = new Rooms();
		$rm->DeleteRoom($roomid);
		/*  TODO Add translations delete */
	
	
	}


	public function UpdateHotel($data,$id) {
	
		global $sql;
		
		$tmp=$data['hotelmain'];
		$ar = array();
		foreach ($tmp as $key => $value) {
			if ($this->__forceAdminCheck[$key] && USER_CLASS != "A" ) {
				$ar[$key] = $value;
				unset($tmp[$key]);
			} 
		}
		
		$hotelid=AdvancedSearch::getInstance($this->__moduleName)->ItemModify($tmp,$id);
		
		
		$req=new UpdateItem(array("table"=> $this->__moduleName));
		
		
		foreach ($ar as $key => $value) {
			$tmpArr['field']=$key;
			$tmpArr['value']=$value['value'];
			$tmpArr['itemid']=$hotelid;
			$tmpArr['hotelid']=$hotelid;
			$tmpArr['key']="id";
			$req->InsertRequest($tmpArr);
		}
		$hotelfilters=array();
		if ($hotelid) {
			foreach($this->__joins as $key => $value ) {
				if ($data['hotelProperties'][$key]) {
					$sql->db_Delete($value['table'],"hotelid=".$id);
					foreach($data['hotelProperties'][$key] as $k => $v ) {	
						if (!is_numeric($v)) {
							
							$sql->db_Insert($key." (title,enable,description)","'".$v."',0,''");
							$itemid=$sql->last_insert_id;
							
							$sql->db_Insert($value['table']." (hotelid,itemid)", $id.",".$itemid);
							
							$req=new UpdateItem(array("table"=> $key));
							$tmpArr['field']="title";
							$tmpArr['value']=$v;
							$tmpArr['itemid']=$itemid;
							$tmpArr['hotelid']=$id;
							$tmpArr['key']="id";
							$req->InsertRequest($tmpArr);
							
						} else {
							$sql->db_Insert($value['table']." (hotelid,itemid)",$id.",".$v);
						}
						
					}
				}
			}	
			
		}
		return $hotelfilters;
		
	}
	
	public function GetExtraFields ($id,$extraKind = array(),$searchField = array(),$page = 1) {
		
		global $loaded_modules,$sql;
		$current_module = $loaded_modules["hotel"];
		$Efields = new ExtraFields(array('module'=>$current_module));
		
		$groups = $Efields->searchGroups(array('debug'=>0,'searchFields'=>array('module'=>$current_module['name'])));
		
		if (empty($extraKind)) {
			foreach ((array) $groups as $key => $value) {
				$AllExtraFields[$value['title']] = $Efields->GetExtraFields(array('debug'=>0,'groupid' => $value['id'],'page'=> $page,'searchFields' => $searchField, 'results_per_page'=>1000,'way'=>'ASC'));
			}
		} else {
			$AllExtraFields[$extraKind['title']] = $Efields->GetExtraFields(array('debug'=>0,'groupid' => $extraKind['id'],'page'=> $page,'searchFields' => $searchField, 'results_per_page'=>1000,'way'=>'ASC'));
		}
		
		
		if ($extraKind['title'] == "locations") {
			
			$AllExtraFields=$this->GetExtraFieldsLocation($id,$AllExtraFields);
			
		} else if ($extraKind['title'] == "policy") {
			
			$AllExtraFields=$this->GetExtraFieldsPolicy($id,$AllExtraFields);
			
		}
		
		
		return $AllExtraFields;

	}
	
	
	
	public function GetExtraFieldsPolicy ($id,$AllExtraFields) {
	
		global $sql;
		foreach($AllExtraFields['policy'] as $key => $value) {
			$fieldsid[]=$value['fieldid'];
		}
		
		$sql->db_Select("extra_field_values","itemid,value,fieldid,enable","fieldid in (".implode(",",$fieldsid).") and enable = 1  or ( fieldid in (".implode(",",$fieldsid).") and itemid=".$id." and enable = 0) ");
		$value_extra=execute_multi($sql);
	//	print_ar($sql);
		foreach((array)$value_extra as $key => $value) {
			$tmp_value_extra[$value['fieldid']]= array("value" => ($value['itemid'] == $id) ? $value['value']: "","enable" => $value['enable']);
		}
		
		foreach($AllExtraFields['policy'] as $key => $value) {
			if ($tmp_value_extra[$value['fieldid']]) {
				$AllExtraFields['policy'][$key] = array_merge($AllExtraFields['policy'][$key],$tmp_value_extra[$value['fieldid']]);
			} else {
				unset($AllExtraFields['policy'][$key]);
			}
		}
		
		return $AllExtraFields;
	
	}
	
	
	
	public function GetExtraFieldsLocation ($id,$AllExtraFields) {
	
		global $sql;
		foreach($AllExtraFields["locations"] as $key => $value) {
			$fieldsid[]=$value['fieldid'];
		}
	
		$sql->db_Select("extra_field_values","value,fieldid,enable","fieldid in (".implode(",",$fieldsid).") and  itemid=".$id);
		$value_extra=execute_multi($sql);
	
		foreach((array)$value_extra as $key => $value) {
			$tmp_value_extra[$value['fieldid']]=  array( "value" => $value['value'],"enable"=> $value['enable']);
		}
	
		foreach($AllExtraFields["locations"] as $key => $value) {
			if ($tmp_value_extra[$value['fieldid']]) {
				$AllExtraFields["locations"][$key] =array_merge($AllExtraFields["locations"][$key],$tmp_value_extra[$value['fieldid']]);
			}
		}
	
	
		return $AllExtraFields;
	
	}
	
	
	
	
	
	
	
	
	
}

?>