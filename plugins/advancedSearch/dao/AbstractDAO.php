<?php

/**
 * The Data Acces Model of cruises
 * @author Evalon-Acer
 *
 */
class AbstractDAO
{
	/**
	 *
	 * @var AdvancedSearch
	 */
	protected $exchange;
	private $__fields;
	private $__extrafields;
	public  $fields;
	protected $__forceAdminCheck;
	function __construct()
	{
		$this->__fields = array(
			'id' => "`".$this->__module."`.`id` as id",
		);
		
	}
	
	
	
	/**
	 * Adds Related Tables Structure in order to be accessed in a DOA class
	 * @param $key
	 * @param $filedQ
	 * @return AbstractDAO
	 */
	function addRelations($key,$filedQ)
	{
		$this->__joins[$key] = $filedQ;
		return $this;
	}
	
	/**
	 * Adds a Filed Structure in order to be accessed in a DOA class
	 * @param $key
	 * @param $filedQ
	 * @return AbstractDAO
	 */
	function addFieldToStructure($key,$filedQ,$check = 0,$value="")
	{
		$this->__forceAdminCheck[$key] = $check;
		$this->__fields[$key] = $filedQ;
		$this->fields[$key] = $value;
		return $this;
	}
	
	/**
	 * get the sql by the requesting field key
	 * @param sql $key
	 */
	function getFiledSQLByKey($key)
	{
		return (isset($this->__fields[$key])? $this->__fields[$key] : $key);
	}
	
	/**
	 * Adds a  Extra Filed Structure in order to be accessed in a DOA class
	 * @param $key
	 * @param $filedQ
	 * @return AbstractDAO
	 */
	function addExtraFieldToStructure($key,$filedQ)
	{
		$this->__extrafields[$key] = $filedQ;
		return $this;
	}
	
	/**
	 * get the sql by the requesting field key
	 * @param sql $key
	 */
	function getExtraFiledSQLByKey($key)
	{
		return (isset($this->__extrafields[$key])? $this->__extrafields[$key] : $this->__extrafields );
	}
	
	
	public function CalculateCurrency($price) {
		
		$number = number_format((float)$this->exchange, 2, '.', '')*$price;
		return number_format((float)$number, 2, '.', '');
		
	}
	
	public function ModuleActions($data,$settings) {
		global $sql;
	//	print_ar($data);
	$table = (isset($settings['table'])) ? $settings['table'] : $this->__moduleName;
	$key = (isset($settings['key'])) ? $settings['key'] : "id";
	
		if (is_array ( $data['data'] )) {	
			if ($data['mode'] == 'activate') {
				$sql->db_Update ( $table , "active = 1 WHERE ".$key." in (" . implode ( ",", $data['data'] ) . ")" );
			//	echo  $this->__moduleName , "active = 1 WHERE id IN (" . implode ( ",", $data['data'] ) . ")" ;
			} elseif ($data ['mode'] == 'deactivate') {
				$sql->db_Update ( $table , "active = 0 WHERE ".$key." in (" . implode ( ",", $data ['data'] ) . ")" );
			}
			// elseif ($data ['mode'] == 'deleted') {	
				//AdvancedSearch::getInstance($this->__moduleName)->DeleteItem($data['data'],array("extraTables"=> array ($settings['extraTables']) ));
			//}
			// GET ALL THE CONTENT BACK TO DISPLAY IT AGAIN
		} // END IDS
	}
	
	public function DeleteImage($itemid) {
		global $sql;
	
		$sql->db_Select("item_images","id","itemid in (".implode(",",$itemid).") and module='".$this->__moduleName."'");
		
		if ($sql->mySQLrows > 0 ) {
			$row=execute_multi($sql);
			foreach((array)$row as $key => $value) {
				$imageid[] = $value['id'];
			}
			
			$sql->db_Delete("item_images","id in (".implode(",",$imageid).") ");
			$sql->db_Delete("item_images_additional","id in (".implode(",",$imageid).") ");
			$sql->db_Delete("users_request","itemid in (".implode(",",$imageid).") and `table`='item_images'");
			
			//echo "item_images","id in (".implode(",",$imageid).") "."</br>";
			//echo "item_images_additional","id in (".implode(",",$imageid).") "."</br>";
			//echo "users_request","itemid in (".implode(",",$imageid).") and `table`='item_images'"."</br>";
		}

	}
	
	
}

?>