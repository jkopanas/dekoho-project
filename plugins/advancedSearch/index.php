<?php
include_once(ABSPATH."/plugins/advancedSearch/AdvancedSearch.php");
foreach (glob(ABSPATH."/plugins/advancedSearch/dao/*.php") as $file)
{
	include_once($file);
}
HookParent::getInstance()->doBindHookGlobally("advancedSearch","advancedSearchInitiate");

/**
 * Plugin initiation
 */
function advancedSearchInitiate() 
{
	global $smarty,$loaded_modules;
	$advancedSearchAsModule = array(
		'id' 				=> 99,
		"name" 				=> "advancedSearch",
		"comment" 			=> "Advance Search",
		"folder" 			=> "plugins/advancedSearch/",
		"active" 			=> 1,
		"startup" 			=> 1,
		"main_file" 		=> "plugloader.php?fetch=advancedSearch",
		"menu_title_admin" 	=> "search",
		"orderby" 			=> 0,
		"is_item" 			=> false,
		"on_menu" 			=> false,
		"num_subs"			=> 0,
		"instance"			=> new AdvancedSearch()
	);
	
	$loaded_modules['advancedSearch'] = $advancedSearchAsModule;
	$smarty->assign("loaded_modules",$loaded_modules);
	// Mocking tests
// 	$mockObj = new Cruises();
// 	print_ar($mockObj->getCruises(array(
// 		"filters" => array(
// 			'return'            => 'paginated', //or null
// 			'perPage'           => 40,
// 			'page'              => 1,
// 			'active'			=> 1
// 		)
// 	)));
// 	echo(json_encode($mockObj->getCruises(array(
// 		"filters" => array(
// 			'return'            => 'paginated', //or null
// 			'perPage'           => 40,
// 			'page'              => 1,
// 			'active'			=> 1
// 		)
// 	))));
// 	$tmpArr2 = $mockObj->getCruises(array(
// 		"filters" => array(
// 			'return'            => 'paginated', //or null
// 			'perPage'           => 40,
// 			'page'              => 1,
// 			'active'			=> 1
// 		)
// 	));
// 	foreach ($tmpArr2['results'][0] as $k => $v)
// 	{
// 		echo "\n".$k .": {"
// 			."\n\ttext: \"".$k."\","
// 			."\n\ttype: \"string\","
// 			."\n\teditable: false,"
// 			."\n\tnullable: true"
// 			."\n},";
// 		echo "<span id=\"cruise_".$k."\">\${".$k."}</span>\n";
// 	}
// 	exit();
}

?>