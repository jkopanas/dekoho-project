<?php

/**
 * AdvancedSearch DB Model
 * @author Evalon-Acer
 *
 */
class AdvancedSearch
{
	private static $_INSTANCE = NULL;
	private $__items;
	private $__module;

	function __construct($module)
	{
		$this->init($module);
	}

	function init($module)
	{
		// Singleton Pattern
		if(self::$_INSTANCE === null || ($this->__module != $module && $module != null))
		{
			$this->__module = $module;
			$settings = array();
			$settings['module']['name'] = $this->__module;
			$this->__items = new Items($settings);
			self::$_INSTANCE = &$this;
		}
	}

	/**
	 * Singleton Pattern:: Get the only instance
	 * @return AdvancedSearch
	 */
	public static function getInstance($module=null)
	{
		if(self::$_INSTANCE === null || $module != null)
		{
			// Force Singleton's initialization
			self::$_INSTANCE = new AdvancedSearch($module);
		}
		return self::$_INSTANCE;
	}

	/**
	 * A smart Items.class wrapper
	 * <pre>
	 * $settings['filters']         = array(
	 *     'module'            => ($module? $module: $this->__module),
	 *     'id'                => $id,
	 *     'categoryid'        => $categoryid, // to fetch Category Path
	 *     'return'            => 'paginated', //or null
	 *     'perPage'           => ($perPage? $perPage : 40),
	 *     'page'              => ($page ? $page : 1 ),
	 *     'archive'           => ($archived? 1 : 0 ),
	 *     'lang'              => ($lang? $lang : DEFAULT_LANG),
	 *     'date-from'         => $dateFrom,
	 *     'date-to'           => $dateTo,
	 *     'sort_field'        => $sortField,
	 *     'sort_direction'    => $sortDir,
	 *     'providerid'        => $providerid,
	 *     'user'              => $user,
	 *     'efield'            => $efield,
	 *     'evalue'            => $evalue,
	 *     'exclude'           => 'id,id,id',//to excludde
	 *     'fields'            => array(
	 *         "table.field as f1",
	 *         "table2.field2 as f2"
	 *     ),
	 *     'categoryies'       => array( // To Join content by categories
	 *         array(
	 *             'categoryid'    => $categoryid
	 *         )
	 *     ),
	 * );
	 * $settings['jointables']        = array(
	 *     array(
	 *         "name"     => "brand",
	 *         "idKey"    => "brand.id",
	 *         "idRel"    => "cruise.brandID",
	 *         "joinType" => "LEFT JOIN"
	 *     )
	 * );
	 * $settings['searchfilters']     = array(
	 *     array(
	 *         'item'     => 'id',
	 *         'type'     => 'eq',
	 *         'val'      => 0,
	 *     )
	 * );
	 * </pre>
	 * @param array $settings
	 * @return array('total'=>$total,'results'=>$search_results,'data'=>$posted_data);
	 */
	function searchItem($settings)
	{
		// initiate Items settings
		$settings['debug'] = ($settings['debug']) ? $settings['debug'] : 0; 
		$settings['filters']['itemid'] 			= $settings['filters']['id'] 		? $settings['filters']['id']		: "";
		$settings['filters']['module']['name'] 	= $settings['filters']['module'] 	? $settings['filters']['module']	: $this->__module;
		$settings['fieldsHooks']				= $settings['filters']['fields'] 	? $settings['filters']['fields'] 	: array($this->__module.'.*');
		$settings['hooks'][]					= "1=1";
		// Initiate token Array
		$tmpArr = array(
			'return_ids'		=> true,
			'id'				=> $settings['filters']['id'] 				? $settings['filters']['id'] 				: "",
			'categoryid'		=> $settings['filters']['categoryid'] 		? $settings['filters']['categoryid'] 		: "", // to fetch Category Path
			'return'			=> $settings['filters']['return'] 			? $settings['filters']['return'] 			: "", //'paginated', //or null
			'results_per_page'	=> $settings['filters']['perPage'] 			? $settings['filters']['perPage'] 			: 40,
			'page' 				=> $settings['filters']['page'] 			? $settings['filters']['page'] 				: 1,
			'archive' 			=> $settings['filters']['archive'] 			? $settings['filters']['archive'] 			: 0,
			//'availability'		=> $settings['filters']['active'] 			? $settings['filters']['active'] 			: 0, --> Buggy
			'lang'				=> $settings['filters']['lang'] 			? $settings['filters']['lang'] 				: "",
			'date-from'			=> $settings['filters']['date-from'] 		? $settings['filters']['date-from'] 		: 0,
			'date-to'			=> $settings['filters']['date-to'] 			? $settings['filters']['date-to'] 			: 0,
			'sort' 				=> $settings['filters']['sort_field'] 		? $settings['filters']['sort_field'] 		: 'id',
			'sort_direction' 	=> $settings['filters']['sort_direction'] 	? $settings['filters']['sort_direction'] 	: 'DESC',
			'providerid'		=> $settings['filters']['providerid'] 		? $settings['filters']['providerid'] 		: '',
			'user'				=> $settings['filters']['user'] 			? $settings['filters']['user'] 				: '',
			'efield'			=> $settings['filters']['efield'] 			? $settings['filters']['efield'] 			: '',
			'evalue'			=> $settings['filters']['evalue'] 			? $settings['filters']['evalue'] 			: '',
			'exclude'			=> $settings['filters']['exclude'] 			? $settings['filters']['exclude'] 			: '',
			'fieldsHooks'		=> $settings['filters']['fields'] 			? $settings['filters']['fields'] 			: array('*'),
			'thumb'				=> $settings['filters']['thumb'] 			? $settings['filters']['thumb'] 			: '',
			'images'			=> $settings['filters']['images'] 			? $settings['filters']['images'] 			: '',
			'main'				=> $settings['filters']['main'] 			? $settings['filters']['main'] 				: '',
			'categoryies'		=> array( // To Join content by categories
				array(
					'categoryid'	=> $categoryid
				)
			),
		);
		
		// Initiate Join Tables
		foreach ($settings['jointables'] as $vArr) 
		{
			$tmpArr['included_table'][$vArr['name']] = 
				" ".$vArr['joinType']." ".$vArr['name']
				." ON ".$vArr['idKey']."=".$vArr['idRel'];
		}
		
		// Initiate Search Filters
		//print_ar($settings['searchfilters']);
		
		foreach ($settings['searchfilters'] as $k => $v) 
		{
			$tmpArr['searchFields'][$v['item']]['item'] = $v['item'];
			$tmpArr['searchFields'][$v['item']]['val'] = $v['val'];
			switch (strtolower($v['type'])) 
			{
				case '==':
				case 'eq':
				case 'equals':
				case 'equal':
				case 'equalto':
				case 'isequalto':
					$tmpArr['searchFields'][$v['item']]['type'] = 'EQ';
					break;
					//-----
				case 'contains':
				case 'substringof':
				case 'like':
				case '%':
					$tmpArr['searchFields'][$v['item']]['type'] = 'LIKE';
					break;
					//-----
				case 'neq':
				case '!=':
				case 'isnotequalto':
				case 'notequals':
				case 'notequalto':
				case 'notequalto':
				case 'ne':
					$tmpArr['searchFields'][$v['item']]['type'] = 'NE';
					break;
					//-----
				case 'lt':
				case '<':
				case 'islessthan':
				case 'lessthan':
				case 'less':
					//-----
				case 'lte':
				case 'lessthanequal':
				case 'le':
					$tmpArr['searchFields'][$v['item']]['type'] = 'LT';
					break;
					//-----
				case 'gt':
				case 'isgreaterthan':
				case '>':
				case 'greaterthan':
				case 'greater':
					//-----
				case 'gte':
				case 'isgreaterthanorequalto':
				case '>=':
				case 'greaterthanequal':
				case 'ge':
					$tmpArr['searchFields'][$v['item']]['type'] = 'GT';
					break;
					//-----
				case 'startswith':
					//-----
				case 'endswith':
					//-----
				default:
					$tmpArr['searchFields'][$v['item']]['type'] = $v['type'];
					break;
			}
		}
		//print_ar($tmpArr);
		// Execute itemSearch
		return $this->__items->ItemSearch($tmpArr, $settings,$page=0,$settings['debug']);
	}

	/**
	 * Generates a Smart Search Query
	 * <pre>
	 * $settings['filters']['id']                ? $settings['filters']['id']               : "", // Value of id/key eg 0 or QWERTY
	 * $settings['filters']['idKey']             ? $settings['filters']['idKey']            : "id", // id, key
	 * $settings['filters']['table']             ? $settings['filters']['table']            : $this->__module, // tableName OR tableName as table1
	 * $settings['filters']['filterlogic']       ? $settings['filters']['filterlogic']      : 'AND', // AND/OR
	 * $settings['filters']['return']            ? $settings['filters']['return']           : "", //'paginated', //or null
	 * $settings['filters']['perPage']           ? $settings['filters']['perPage']          : 40,
	 * $settings['filters']['page']              ? $settings['filters']['page']             : 1,
	 * $settings['filters']['sort_field']        ? $settings['filters']['sort_field']       : 'id',
	 * $settings['filters']['sort_direction']    ? $settings['filters']['sort_direction']   : 'DESC', // ASC/DESC
	 * $settings['filters']['groupBy']           ? $settings['filters']['groupBy']          : 'id',
	 * $settings['filters']['fields']            = array(
	 *     "table.field as f1",
	 *     "table2.field2 as f2"
	 * )
	 * $settings['jointables']        = array(
	 *     array(
	 *         "name"     => "brand",
	 *         "idKey"    => "brand.id",
	 *         "idRel"    => "cruise.brandID",
	 *         "joinType" => "LEFT JOIN"
	 *     )
	 * );
	 * $settings['searchfilters']     = array(
	 *     array(
	 *         'item'     => 'id',
	 *         'type'     => 'eq', // or even regex for REGEXP
	 *         'val'      => 0,
	 *     )
	 * );
	 * </pre>
	 * @param array $settings
	 * @return array <number, unknown, string, mixed>
	 */
	function searchBySQL($settings)
	{
		global $sql;
		$settings['debug'] = ($settings['debug']) ? $settings['debug'] : 0;
		$searchFilter 	= array();
		$joinTables		= array();
		$qArg			= "";

		// Initiate token Array
		$tmpArr = array(
			'id'				=> $settings['filters']['id'] 				? $settings['filters']['id'] 				: "", // Value of id/key eg 0 or QWERTY
			'idKey'				=> $settings['filters']['idKey'] 			? $settings['filters']['idKey'] 			: "id", // id, key
			'table'				=> $settings['filters']['table'] 			? $settings['filters']['table'] 			: $this->__module, // tableName OR tableName as table1
			'filterlogic'		=> $settings['filters']['filterlogic']		? $settings['filters']['filterlogic']		: 'AND', // AND/OR
			'return'			=> $settings['filters']['return'] 			? $settings['filters']['return'] 			: "", //'paginated', //or null
			'perPage'			=> $settings['filters']['perPage'] 			? $settings['filters']['perPage'] 			: 40,
			'page' 				=> $settings['filters']['page'] 			? $settings['filters']['page'] 				: 1,
			'sort' 				=> $settings['filters']['sort_field'] 		? $settings['filters']['sort_field'] 		: 'id',
			'sort_direction' 	=> $settings['filters']['sort_direction'] 	? $settings['filters']['sort_direction'] 	: 'DESC', // ASC/DESC
			'groupBy'			=> $settings['filters']['groupBy']			? $settings['filters']['groupBy']			: '',
			'fields'			=> $settings['filters']['fields']			? $settings['filters']['fields']			: array()		
		);
		// Initiate Query Argument
		$qArg .= ($tmpArr['groupBy']?" GROUP BY ".$tmpArr['groupBy']:"");
		$qArg .= " ORDER BY ".$tmpArr['sort']." ".$tmpArr['sort_direction'];
		$qArg .= (($tmpArr['return']=="paginated")?" LIMIT ".(($tmpArr['page']*$tmpArr['perPage'])-$tmpArr['perPage']).",".$tmpArr['perPage']:"");
		// Initiate Join Tables
		foreach ($settings['jointables'] as $vArr)
		{
			$joinTables[] = " ".$vArr['joinType']." ".$vArr['name']." ON ".$vArr['idKey']."=".$vArr['idRel'];
		}
		
		// Initiate Search Filters
		foreach ($settings['searchfilters'] as $k => $v)
		{
			switch (strtolower($v['type']))
			{
				case '==':
				case 'eq':
				case 'equals':
				case 'equal':
				case 'equalto':
				case 'isequalto':
					$searchFilter[] = " ".$v['item']." ".(is_numeric($v['val'])?"= ".$v['val']:"= '".$v['val']."'");
					break;
					//-----
				case 'contains':
				case 'substringof':
				case 'like':
				case '%':
					$searchFilter[] = " ".$v['item']." LIKE '%".$v['val']."%'";
					break;
					//-----
				case 'neq':
				case '!=':
				case 'isnotequalto':
				case 'notequals':
				case 'notequalto':
				case 'notequalto':
				case 'ne':
					$searchFilter[] = " ".$v['item']." ".(is_numeric($v['val'])?"!= ".$v['val']:"!= '".$v['val']."'");
					break;
					//-----
				case 'lt':
				case '<':
				case 'islessthan':
				case 'lessthan':
				case 'less':
					$searchFilter[] = " ".$v['item']." < ".$v['val']."";
					break;
					//-----
				case 'lte':
				case '<=':
				case 'lessthanequal':
				case 'le':
					$searchFilter[] = " ".$v['item']." <= ".$v['val']."";
					break;
					//-----
				case 'gt':
				case 'isgreaterthan':
				case '>':
				case 'greaterthan':
				case 'greater':
					$searchFilter[] = " ".$v['item']." > ".$v['val']."";
					break;
					//-----
				case 'gte':
				case 'isgreaterthanorequalto':
				case '>=':
				case 'greaterthanequal':
				case 'ge':
					$searchFilter[] = " ".$v['item']." >= ".$v['val']."";
					break;
					//-----
				case 'startswith':
					$searchFilter[] = " ".$v['item']." LIKE '".$v['val']."%'";
					break;
					//-----
				case 'endswith':
					$searchFilter[] = " ".$v['item']." LIKE '%".$v['val']."'";
					break;
					//-----
				case 'regex':
					$searchFilter[] = " ".$v['item']." REGEXP '".$v['val']."'";
					break;
					//-----
				default:
					$searchFilter[] = " ".$v['item']." ".$v['type']." ".$v['val'];
					break;
			}
		}
		
		// append id SearchFilter
		if(strlen($tmpArr['id'])>0)
		{
			$searchFilter[] = " ".$tmpArr['idKey']." ".(is_numeric($tmpArr['id'])?"= ".$tmpArr['id']:"= '".$tmpArr['id']."'");
		}
		
		$out = array();
		$out['data'] = $tmpArr;
		
//		$sql->db_Select(
//			$tmpArr['table']." ".implode(" ", $joinTables),
//			"COUNT(*) as total",
//			implode(" ".$tmpArr['filterlogic']." ", $searchFilter)." ".$qArg
//		);
		
		// Execute SQL
		if ( $tmpArr['return']=="total" ) {
			$sql->q("select count(*) as total from (SELECT ". (count($tmpArr['fields'])>0? implode(" , ", $tmpArr['fields']) : "*")." FROM ".$tmpArr['table']." ".implode(" ", $joinTables)." WHERE ".implode(" ".$tmpArr['filterlogic']." ", $searchFilter)." ".$qArg.") as total");
		} else {	
			$sql->db_Select(
				$tmpArr['table']." ".implode(" ", $joinTables),
				(count($tmpArr['fields'])>0? implode(" , ", $tmpArr['fields']) : "*"),
				implode(" ".$tmpArr['filterlogic']." ", $searchFilter)." ".$qArg
			);
		}
		if ($settings['debug']) {
			echo "SELECT ". (count($tmpArr['fields'])>0? implode(" , ", $tmpArr['fields']) : "*")." FROM ".$tmpArr['table']." ".implode(" ", $joinTables)." WHERE ".implode(" ".$tmpArr['filterlogic']." ", $searchFilter)." ".$qArg;
		}
		
		if(strlen($tmpArr['id'])>0)
		{
			$out['results'] = execute_single($sql);
			return $out;
		}
		$out['results'] = execute_multi($sql);
		return $out;
	}
	
	
	//public function relate
	public function DeleteItem ($data,$settings) {
	
		return $this->__items->DeleteContent($data,$settings);
	}
	
	
	
	public function ItemAdd ($data) {
		
		
		return $this->__items->saveItem($data,0,array('complexFields'=>1,'debug'=>0));
	}
	
	public function ItemModify ($data,$id) {
		
		return $this->__items->SaveItem((array)$data,$id,array('complexFields'=>1,'debug'=>0));
	}
	
	
}

?>