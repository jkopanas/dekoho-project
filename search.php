<?php 
include_once('init.php');


$sql->q("select * from exchange group by currency;");
$currencies=execute_multi($sql);
$currencies[]= array("currency"=>"EUR","rate" => "1","symbol"=>"&euro;");

$smarty->assign("currencies",$currencies);

$_SESSION['currency'] = (isset($_POST['currency'])) ? $_POST['currency']  : ( ( !isset($_SESSION['currency']) ) ? $_SESSION['plugins']['dynamicLanguage']['currency'] : $_SESSION['currency']);


foreach($currencies as $key => $value) {
	
	if  ( $value['currency'] == $_SESSION['currency'] ) {
		$rate = $value['rate'];
		$smarty->assign("current_currency",$currencies[$key]);
		unset($currencies[$key]);
	}
	
}


$_SESSION['exchange']=$rate;
$smarty->assign("currencies",$currencies);
$smarty->assign("exchange",$rate);

$_SESSION['plugins']['dynamicLanguage']['countryName'] =($lang[$_SESSION['plugins']['dynamicLanguage']['countryName']])?$lang[$_SESSION['plugins']['dynamicLanguage']['countryName']]:$_SESSION['plugins']['dynamicLanguage']['countryName'];
$smarty->assign("Cultures",$_SESSION['plugins']['dynamicLanguage']['marketSettings']);
$smarty->assign("TheCountry",$_SESSION['plugins']['dynamicLanguage']['countryName']);
$smarty->assign("CulturesTpl",json_decode($_SESSION['plugins']['dynamicLanguage']['marketSettings'],true));

$urls= array (
			  "content" => "cruising",
		      "components"=>"cruise-excursions/shore-excursion",
		      "ports"=>"cruise-destinations/cruise-port",
		      "itinerary"=>"cruises/cruise-itinerary"
		);

$uriArr = explode('/',$_GET['uri']);
$reqToken = array();
$prev = null;
foreach ($uriArr as $k=>$v) {
	if (($k+1)%2 != 0) {
		$reqToken[$v] = $v;
		$prev = $v;
	}
	else {
		$reqToken[$prev] = $v;
	}
}
####################### PAGINATION DATA ##################################################
if ($_GET["page"]!=""):		$page	= $_GET["page"];	else:	$page	= 1;endif;
define ('CURRENT_PAGE',$page);
$smarty->assign("PAGE",$page);//assigned template variable PAGE
###################### END OF PAGINATION DATA ###########################################

if ($_GET['module'] != "search") {
	 $m = explode("-",$_GET['module']);
	 $ArrModule[]= $m[1];
} else {
	$ArrModule = array('content','itinerary','ports','components');
}
$numItems = count($ArrModule);
foreach ($ArrModule as $key => $module) {
$current_module = $loaded_modules[$module];

	$c = new Items(array('module'=>$current_module));
	$posted_data = $_POST;
	//$posted_data = array('categoryid'=>$cat,'availability'=>1,'thumb'=>1,'main'=>1);
	unset($current_module['active']);
	//$posted_data = array('availability'=>1);
	//$posted_data['page'] = ($posted_data['page']) ? $posted_data['page'] : 1;
	$posted_data['cat_details'] = 1;
	$posted_data['sort'] = ($posted_data['sort']) ? $posted_data['sort'] : $current_module['settings']['default_sort'];
	$posted_data['sort_direction'] = ($posted_data['sort_direction']) ? $posted_data['sort_direction'] : $current_module['settings']['default_sort_direction'];
	$posted_data['results_per_page'] = ($posted_data['pageSize']) ? $posted_data['pageSize'] : 10;	
	if ($_POST['sort']) {
		if (!strstr($_POST['sort'][0]['field'],'.')){
			$posted_data['sort'] = $_POST['sort'][0]['field'];
			$posted_data['sort_direction'] = $_POST['sort'][0]['dir'];
		}
		else {
			$posted_data['sort'] = '';
			$posted_data['sort_direction'] = '';
		}
	}

	$op = ($_POST['operator']) ? $_POST['operator'] : "LIKE";  
		if (is_array($reqToken)) { //KENDO GRID FILTERS
				foreach ($reqToken as $key => $value) {
					$posted_data['searchFields'][$key]= array('type'=>$op,'val'=>$value);
				}		
		}
   
	$posted_data['SearchLogic'] = ($_POST['logic']) ? $_POST['logic'] : "or";
	$posted_data['page'] = $page;
	$res[$module] = $c->ItemSearch($posted_data,$current_module,$page,0);
	
	$totals[]=$res[$module]['data']['total'];

	$res[$module]['data']['url']=$urls[$module];

}
$pg = new pagination($page,"10");
$total=max($totals);
$pg->setTotalRecords($total);
//if( $total > 0 ) { $total_pages = ceil($total/$limit); } else { $total_pages = 0; } 

$smarty->assign("needle",$reqToken['title']);
$smarty->assign("total_pages",$total_pages);
$smarty->assign("list",array("total" => array_sum($totals)));
$smarty->assign("navigation",$pg->getNavigation());
$smarty->assign("num_links",$pg->getCurrentPages());
$smarty->assign("PAGE",$page);

$smarty->assign("items",$res);
$smarty->assign("include_file",'search_results.tpl');//assigned template variable include_file
include(ABSPATH."/memcache.php");
$smarty->caching = USE_SMARTY_CAHCHING;
$smarty->display("home.tpl",$url);//Display the home.tpl template

?>
