<?php
include_once("eshop/admin/init.php");//load from eshop/admin!!!!
define("NAME_OF_INSTANCE","index");
$jsonOutput['name'] = NAME_OF_INSTANCE;

if ($_GET['action']=='orders')
{
	global $sql;
	$period = "";
	$limit = (is_numeric($_REQUEST['limit']) ? "limit ". $_REQUEST['limit'] : "");
	
	switch((isset($_REQUEST['period']) ? $_REQUEST['period'] : 'daily'))
	{
		case 'monthly':
			$period = '%b';
			break;
		case 'yearly':
			$period = '%Y';
			break;
		case 'hourly':
			$period = '%k';
			break;
		case 'daily':
		default:
			$period = '%a';
			break;
	}
	
	$sql->q("SELECT count( id ) as value , FROM_UNIXTIME( `date_added` , '".$period."' ) as xAxis
			FROM eshop_orders
			GROUP BY xAxis
			ORDER BY date_added"
			.$limit);
	
	//$items = array();
	$items = execute_multi($sql);
	$graph = array();
	$headers = array();
	//print_r($items);
	foreach ($items as $key=>$value) 
	{
		if (is_array($value)  && strlen($value['value'])>0 && strlen($value['xAxis'])>0 )
		{
			$graph[] = $value['value'];
			$headers[] = $value['xAxis'];
		}
	}
	
	print(json_encode(array('values'=>$graph,'headers'=>$headers)));
	exit();
}


if ($_GET['action']=='pmethods')
{
	global $sql;
	$sql->q("SELECT count( es.id ) as value , 
					pm.title as xAxis
			FROM eshop_orders as es
			LEFT JOIN eshop_payment_methods as pm
				on pm.id = es.payment_method
			GROUP BY xAxis
			ORDER BY xAxis");

	//$items = array();
	$items = execute_multi($sql);
	$graph = array();
	$headers = array();
	//print_r($items);
	foreach ($items as $key=>$value)
	{
		if (is_array($value)  && strlen($value['value'])>0 && strlen($value['xAxis'])>0 )
		{
			$graph[] = $value['value'];
			$headers[] = $value['xAxis'];
		}
	}

	print(json_encode(array('values'=>$graph,'headers'=>$headers)));
	exit();
}

if ($_GET['action']=='shipmethods')
{
	global $sql;
	$sql->q("SELECT count( es.id ) as value , 
					shipm.shipping as xAxis
			FROM eshop_orders as es
			LEFT JOIN eshop_shipping as shipm
				on shipm.id = es.shipping_method
			GROUP BY xAxis
			ORDER BY xAxis");

	//$items = array();
	$items = execute_multi($sql);
	$graph = array();
	$headers = array();
	//print_r($items);
	foreach ($items as $key=>$value)
	{
		if (is_array($value)  && strlen($value['value'])>0 && strlen($value['xAxis'])>0 )
		{
			$graph[] = $value['value'];
			$headers[] = $value['xAxis'];
		}
	}

	print(json_encode(array('values'=>$graph,'headers'=>$headers)));
	exit();
}



/********************************************************************** /
$orders = $eshop->searchOrders(array(
		"debug"=>0,
		'results_per_page'=>40,
		'return'=>'paginated',
		'page'=>1,
		'getTotalAmount'=>1,
		'searchFields'=>$searchFields,
		'missingFields'=>$missing_data,
		'getUserDetails'=>1,
		'searchFieldsGT'=>$searchFieldsGT,
		'searchFieldBETWEEN'=>$searchFieldsBetween,
		'searchFieldsLike'=>$searchFieldsLike,
		'orderby'=>'amount',
		'way'=>'ASC',
		'filters'=>$filters
));

if(count($orders)>0)
{
	$jsonOutput[DATA_KEY] = array(
					"payment_methods" 	=> json_encode($eshop->paymentMethods(0,array('fields'=>'id,title'))),
					"results_per_page" 	=> ($_REQUEST['results_per_page'] AND $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 40,
					"status_codes"		=> json_encode($eshop->orderStatusCodes(array('simplify'=>1))),
					"ordersSum"			=> $eshop->ordersTotal,
					"values" 			=> json_encode($orders),
					"headers" 			=> json_encode(array_keys($orders[0]))
	);
	$jsonOutput[STATUS_KEY] = SUCCESS;
}
else
{
	$jsonOutput[STATUS_KEY] = FAILURE;
}
print(json_encode($jsonOutput));
exit();
/**********************************************************************/
?>