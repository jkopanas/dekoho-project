<?php
$tmpArr = array(
	'http' => array(
		'method' => 'POST',
		'content' => http_build_query($_POST),
		'header'=>
			"Accept:*/*\r\n"
			+ "Accept-Charset:UTF-8,*;q=0.5\r\n"
			+ "Accept-Language:el,en-GB;q=0.8\r\n"
			+ "Accept-Encoding:gzip,deflate,sdch\r\n"
			+ "Connection:keep-alive\r\n"
			+ "Content-type:application/x-www-form-urlencoded\r\n"
			+ "Origin:http://127.0.0.1\r\n"
			+ "Host:127.0.0.1:3000\r\n",
	)
);

$fp = fopen("http://127.0.0.1:3002/".$_GET['action'], "rb", false, stream_context_create($tmpArr));
if($fp)
{
    while (!feof($fp))
    {
    	echo fgets($fp, 1024);
    }
}
fclose($fp);
?>