<?php
include($_SERVER['DOCUMENT_ROOT']."/manage/init.php");//load from manage!!!!
if ($_POST['var']) { $posted_data = form_settings_array($_POST['var']);}
$current_module = $loaded_modules['recipes'];
$Recipe = new Recipes(array('module'=>$current_module,'debug'=>0));

if ($_GET['action'] == 'getRecipeIngredients') {
$id = $_POST['itemid'];
$item_settings = array('fields'=>'*','thumb'=>0,'CatNav'=>0,'debug'=>0,'main'=>0,'GetRecipeDetails'=>1,'GetCommonCategories'=>0,'returnSimple'=>1,'full-recipe'=>1);
$item = $Recipe->GetItem($id,$item_settings);
$smarty->assign("item",$item);
$smarty->display("modules/recipes/admin/recipes_ingredients.tpl");
}//END GET SINGLE RECIPE INGREDIENTS

if ($_GET['action'] == 'copyRecipeIngredients') {
$posted_data = $_POST['data'];
foreach ($posted_data as $k=>$v) {
	if (strstr($k,'id-')) {
		list($field,$ingid)=split("-",$k);
		$ids[] = $ingid;
	}//END IF
}//END FOREACH
$ingredients_list = $Recipe->GetRecipeIngredients(array('id'=>$posted_data['itemid'],'ids'=>$ids,'full-recipe'=>1));
echo json_encode($ingredients_list);
}//END copyRecipeIngredients
?>