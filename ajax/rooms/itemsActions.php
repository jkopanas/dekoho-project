<?php
$module = $loaded_modules[$_GET['module']];
$c = new Items(array('module'=>$module));
$hotels = new Hotel_Items();
if ($_GET['action'] == 'saveItem') {
	$mode = ($_GET['id']) ? 'modify' : 'add';
	$save = $c->saveItem($_POST['main'],$_GET['id'],array('complexFields'=>1,'debug'=>1));

	if ($save) {
		
		$tmpObj = new Rooms();
	
		foreach($tmpObj->__joins as $key => $value ) {
			if ($_POST[$key]) {
				$sql->db_Delete($value['table'],"roomid=".$_GET['id']);
				foreach($_POST[$key]['value'] as $k => $v ) {
					if (!$v) {
						$sql->db_Insert($key." (title,enable,description)","'".$k."',0,''");
						$sql->db_Insert($value['table']." (roomid,itemid)",$_GET['id'].",".$sql->last_insert_id);
					} else {
						$sql->db_Insert($value['table']." (roomid,itemid)",$_GET['id'].",".$v);
						echo $value['table']." (roomid,itemid)",$_GET['id'].",".$v;
					}
				}
			}
		}

		foreach($_POST['sec'] as $key => $value ) {
			$args[] = $key."='".$value['value']."'";
		}
		$sql->db_Update("rooms_hotel", implode(",",$args)."where room_id=".$_GET['id']);
	}
	
	if ($_GET['returnItem']) {
		$id = ($_GET['id']) ? $_GET['id'] : $save;
		$item = $c->GetItem($id, array('fields'=>'*','thumb'=>0,'CatNav'=>0,'debug'=>0,'main'=>0,'cat_details'=>1,'parse'=>'toForm','GetCommonCategoriesTree'=>1,'returnSimple'=>1));
		$moreItems = $c->LatestItems(array('results_per_page'=>10,'active'=>2,'get_provider'=>0,'get_user'=>0,'debug'=>0,'just_results'=>1,'location'=>0,'GetCommonCategoriesTree'=>0
		,'categoryid'=>$item['main_category']['categoryid']));
	}

	echo json_encode(array('id'=>$save,'debug'=>'','mode'=>$mode,'item'=>$item,'moreItems'=>$moreItems));
	exit();
}//END SAVE ITEM

if ($_GET['action'] == 'saveImage') {
	$current_module = $module;
	$media = (!$_GET['media']) ? 'image' : $_GET['media'];
    $MediaFilesSettings = MediaFiles(array('single'=>1,'type'=>$media)); 

    $targetPath = $_SERVER['DOCUMENT_ROOT'] . $MediaFilesSettings['folder']."/".$current_module['name']."_".$_GET['id'];
    if (!is_dir($targetPath)) {
    mkdir($targetPath,0755);
    }//END IF

 	$fileParam = ($_GET['fileParam']) ? $_GET['fileParam'] : $_POST['fileParam'];
    $files = $_FILES[$fileParam];
    if (isset($files['name']))
    {
        $error = $files['error'];

        if ($error == UPLOAD_ERR_OK) {
            $targetFile = $targetPath."/".basename($files["name"]);
            $uploadedFile = $files["tmp_name"];
            if (is_uploaded_file($uploadedFile)) {
                if (!move_uploaded_file($uploadedFile, $targetFile)) {
                    echo "Error moving uploaded file";
                }
                else {
					include($_SERVER['DOCUMENT_ROOT']."/modules/common_functions.php");
						$a = new mediaFiles(array('module'=>$module));
						$fileType = (!$_GET['fileType']) ? 0 : $_GET['fileType'];
						if ($media == 'image') {
							$imageCategory = ($_GET['imageCategory']) ? $_GET['imageCategory'] : $_POST['imageCategory'];
							$imageCategory = (!$imageCategory) ? 0 : $imageCategory;
							$up = $a->handleImageUpload($_GET['id'],basename($files["name"]),$targetPath,$targetPath,array('fileType'=>$fileType,'imageCategory'=>$imageCategory,'keepOriginal'=>$module['settings']['keep_original_image']));
							if ($module['settings']['keep_original_image']) {

							}//END ORIGINALS
						}//END IMAGES
						elseif ($media == 'video') {
							$up = $a->SaveVideo($_GET['id'],basename($files["name"]),$a->CheckDocument($targetFile,$targetPath,basename($files["name"])),$_POST);
						}//END VIDEO
						else {
							$up = $a->SaveDocument($_GET['id'],basename($files["name"]),$a->CheckDocument($targetFile,$targetPath,basename($files["name"])));
						}//END OTHER TYPES
				}//END MOVE
                }//END ALL IS WELL
                

            }
        } else {
            // See http://php.net/manual/en/features.file-upload.errors.php
            echo "Error code " . $error;
        }
	
	header('Content-Type: text/plain;');
 
	$data = array('foo' => $_GET, 'status' => 'ok','uploadedFile'=>$up);
	 
	echo json_encode($data);
	exit();
}
if ($_GET['action'] == 'MediaActions') {
	$module = $_GET['module'];
	switch ($_GET['media'])
	{
		case 'document': $table = 'item_documents';
		break;
		case 'video': $table = 'item_videos';
		break;
		default: $table = 'item_images';
		break;
	}
/* NEED TO RUN CLEAN UP OF THE ACTUAL FILES */
if ($_GET['mode'] == 'enableMedia') {
	$sql->db_Update($table,"available = 1 WHERE id IN (".implode(",",$_POST['ids']).") AND module = '$module'");
//	echo "UPDATE $table SET available = 1 WHERE id IN (".implode(",",$_POST['ids']).") AND module = '$module'";
}

elseif ($_GET['mode'] == 'disableMedia') {
	$sql->db_Update($table,"available = 0 WHERE id IN (".implode(",",$_POST['ids']).") AND module = '$module'");
}
elseif ($_GET['mode'] == 'deleteMedia') {
	$sql->db_Delete($table,"id IN (".implode(",",$_POST['ids']).") AND module = '$module'");
}
elseif ($_GET['mode'] == 'updateMedia') {
	foreach ($_POST as $k=>$v)
	{
		if (strstr($k,'-'))
		{
			list($field,$id)=split("-",$k);
				$updates[$id][$field] = $v;
		}
	}
	foreach ($updates as $key=>$val) {
		$r = array();
		foreach ($val as $k=>$v) {
			$r[] = "$k = '$v'";
		}
//		$q[] = implode(",",$r). " WHERE id = $key";
//echo "UPDATE $table SET ".implode(",",$r). " WHERE id = $key\n";
		$sql->db_Update($table,implode(",",$r). " WHERE id = $key");
	}
	
}
elseif ($_GET['mode'] == 'sortOrder') {
	foreach ($_POST['ids'] as $k=>$v) {
		$sql->db_Update($table,"orderby = $k WHERE id = ".str_replace('Copies','',$v)." AND module = '".$_GET['module']."'");
//		echo "UPDATE $table SET orderby = $k WHERE id = ".str_replace('Copies','',$v)." AND module = '".$_GET['module']."'\n";
	}
	echo json_encode($_POST);
}
}//END MEDIA


if ($_GET['action'] == 'saveEfields') {
	$c->UpdateExtraFieldsValues($_POST['data'],$_GET['id']);
}

if ($_GET['action'] == 'saveMediaTranslation') 
{
	foreach (((array)$_POST['data']) as $id => $vIDArr)
	{
		$q = "";
		foreach ($vIDArr as $code => $vCodeArr)
		{
			foreach ($vCodeArr as $field => $vField)
			{
				$sql->db_Select(
					"translations",
					"*",
					"itemid = ".$id
					." AND `table` = 'item_images' "
					." AND `module` = '".$_REQUEST['module']."' "
					." AND `code` = '".$code."' "
					." AND `field` = '".$field."' "
				);
				//$tmpArr = execute_single($sql);
				if(execute_single($sql))
				{
					$sql->db_Update(
						"translations",
						"translation = '".$vField."', date_modified = " . microtime()
						." WHERE itemid = ".$id
						." AND `table` = 'item_images' "
						." AND `module` = '".$_REQUEST['module']."' "
						." AND `code` = '".$code."' "
						." AND `field` = '".$field."' "
					);
				}
				else
				{
					$sql->db_Insert(
						'translations (`code`,`itemid`,`table`,`field`,`module`,`translation`,`date_added`,`date_modified`,`active`,`settings`) ', 
						" '$code' , $id, 'item_images', '$field', '".$_REQUEST['module']."', '$vField' , ".time().", ".time().", 1, '{}'"
					);
				}
			}
		}
	}
	
	echo json_encode(array("status"=>"SUCCESS"));
	exit();
}

if ($_GET['action'] == 'fetchMediaTranslation') 
{
	$sql->db_Select("translations","*","itemid = ".$_REQUEST['id']." AND `table` = 'item_images' AND module = '".$_REQUEST['module']."' ");
	$tmpArr = (array) execute_multi($sql);
	$resultArr = array();
	
	foreach ($tmpArr as $vArr)
	{
		$resultArr[$vArr['itemid']][$vArr['code']][$vArr['field']] = $vArr['translation'];
	}
	
	$sql->db_Select("item_images","*","id = ".$_REQUEST['id']." AND module = '".$_REQUEST['module']."' ");
	
	echo json_encode(array(
		'single'=>(array) execute_single($sql),
		'data'=>$resultArr,
		'langs'=>$Languages->AvailableLanguages(),
		'active'=>LANG
	));
	exit();
}

if ($_GET['action'] == 'translateEfieldValues') {
	/*
	* To my future me. I'm so so sorry about this, but i was in a hurry and it will just work
	* Optimize it by creating a query for all fields and chery pick the ones that need to update
	* 
	* Dev2: Dear past & future of yours, I'm not at all a time instance of yours. 
	* I know, shit happens & your apology is accepted, Cheers!
	*/
	foreach ($_POST['data'] as $k=>$v){
		$q ="AND (settings LIKE '%\"field\":\"itemid\"%' AND (settings LIKE '%\"value\":".$_GET['id']."%' OR settings LIKE '%\"value\":\"".$_GET['id']."\"%'))";
		
		$sql->db_Select("translations","id,field,code","itemid = ".$v['id']." AND translations.table = 'extra_fields'  AND module = '".$_GET['module']."' $q AND code = '".$v['code']."'");
//		echo "itemid = ".$v['id']." AND translations.table = 'extra_field_values'  AND module = '".$_GET['module']."' $q";
		$existing = execute_multi($sql);
		$tables[$k] = $v['extra_fields'];
		$new[$v['code']][$k] = $v['value'];

	
		if ($existing) {
			foreach ($existing as $v)//BEGIN UPDATING EXISTING
			{
				if ($new[$v['code']][$v['field']]) {
	//				echo "UPDATE translations SET translation = '".$new[$v['code']][$v['field']]."' WHERE id = ".$v['id']."<br>";
					$sql->db_Update("translations","translation = '".$new[$v['code']][$v['field']]."' WHERE id = ".$v['id']);	
					unset($new[$v['code']][$v['field']]);
				}
				else {//EMPTY FIELDS. NEVER SUBMITED THUS THE USER WANTS TO CLEAN THEM
					$cleanUp[] = $v['id'];
				}
			}
			if ($cleanUp) {
				$sql->db_Delete("translations","id IN (".implode($cleanUp).")");
			}
		}
		}//END LOOP FIELDS
		
		if ($new) {	
			foreach ($new as $k=>$v)//BEGIN INSERTING NEW
			{
				if ($v AND $_GET['id']) {
					$date_added = time();
					foreach ($v as $key=>$value) {
					$table = $tables[$key];
						$settings = json_encode(array('extraField'=>array('field'=>'itemid','value'=>$_GET['id'])));
					$inserts[] = "'$k','".$_POST['data'][$key]['id']."','extra_fields','value','".$_GET['module']."','$value','$date_added','$date_added','1','$settings'";
					}//END LOOP FIELDS
				}//END IF VALUES
			}//END  LOOP LANGUAGES
		}
		if ($inserts) {
            $sql->db_Insert('translations (translations.code,itemid,translations.table,field,module,translation,date_added,date_modified,active,settings) ', implode("),(",$inserts) );
			//mysql_query("INSERT INTO translations VALUES ".implode(",",$inserts));
		}
	
}//END ACTION

function saveEfieldTranslations(){

		$posted_data = $_POST['data'];
		if ($_GET['itemid'] AND is_numeric($_GET['itemid'])) {
			$q = "AND (settings LIKE '%\"field\":\"itemid\"%' AND settings LIKE CONCAT('%\"value\":',".$_GET['itemid'].",'%'))";
		}
		$sql->db_Select("translations","id,field,code","itemid = ".$_GET['id']." AND module = '".$_GET['module']."' $q");
		$existing = execute_multi($sql);
		

		foreach ($posted_data as $k=>$v)
		{
			list($field,$code)=split("-",$k);
			$tmp = explode('@',$field);
			$tables[$field] = $v['table'];
			$new[$code][$field] = $v['value'];
		}

		if ($existing) {
			foreach ($existing as $v)//BEGIN UPDATING EXISTING
			{
				if ($new[$v['code']][$v['field']]) {
	//				echo "UPDATE translations SET translation = '".$new[$v['code']][$v['field']]."' WHERE id = ".$v['id']."<br>";
					$sql->db_Update("translations","translation = '".$new[$v['code']][$v['field']]."' WHERE id = ".$v['id']);	
					unset($new[$v['code']][$v['field']]);
				}
				else {//EMPTY FIELDS. NEVER SUBMITED THUS THE USER WANTS TO CLEAN THEM
					$cleanUp[] = $v['id'];
				}
			}
			if ($cleanUp) {
				$sql->db_Delete("translations","id IN (".implode($cleanUp).")");
			}
		}

		foreach ($new as $k=>$v)//BEGIN INSERTING NEW
		{
			if ($v AND $_GET['id']) {
				$date_added = time();
				foreach ($v as $key=>$value) {
				$table = $tables[$key];
				if ($_GET['itemid']) {
					$settings = json_encode(array('extraField'=>array('field'=>'itemid','value'=>$_GET['itemid'])));
				}
				$inserts[] = "'$k','".$_GET['id']."','$table','$key','".$_GET['module']."','$value','$date_added','$date_added','1','$settings'";
				}//END LOOP FIELDS
			}//END IF VALUES
		}//END  LOOP LANGUAGES

		if ($inserts) {
            $sql->db_Insert('translations (translations.code,itemid,translations.table,field,module,translation,date_added,date_modified,active,settings) ', implode("),(",$inserts) );
			//mysql_query("INSERT INTO translations VALUES ".implode(",",$inserts));
		}
}
?>