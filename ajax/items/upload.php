<?php
$current_module = $loaded_modules[$_GET['module']];
if (!call_user_func($_GET['action'],$_GET,$_POST)) {
	exit();
}

function xhr($getData,$postData=0) {
	global $sql,$loaded_modules,$current_module;
	$MediaFilesSettings = detectMediaType($postData['extension']);
	$sizeLimit = 2 * 1024 * 1024;
	$now = strftime('%m-%Y',time());
	$targetPath = ABSPATH . "/media_files/images/".$now;//Generic upload
	$uploader = new qqFileUploader(array(), $sizeLimit);
	$result = $uploader->handleUpload($targetPath);
	$_REQUEST['path']="/media_files/images/".$now;
	echo json_encode(array('success'=>'true','request'=>$_REQUEST));
}

function simple($getData,$postData=0) {
	global $sql,$loaded_modules,$current_module;
	
	$MediaFilesSettings = detectMediaType($postData['extension']);
	if (!$postData['id']) {
		$now = strftime('%m-%Y',time());
		$targetPath = ABSPATH . $MediaFilesSettings['folder']."/".$now;//Generic upload
	}
	else { 
    	$targetPath = ABSPATH . $MediaFilesSettings['folder']."/".$current_module['name']."_".$postData['id'];
	}
    if (!is_dir($targetPath)) {
    	mkdir($targetPath,0755);
    }//END IF
    
 	$fileParam = ($getData['fileParam']) ? $getData['fileParam'] : $postData['fileParam'];
    $files = $_FILES[$fileParam];
    $a = new mediaHandler();
    if (isset($files['name']))
    {
    	$error = $files['error'];
        if ($error == UPLOAD_ERR_OK) {
        	$targetFile = $targetPath."/".basename($files["name"]);
            $uploadedFile = $files["tmp_name"];
            if (is_uploaded_file($uploadedFile)) {
                if (!move_uploaded_file($uploadedFile, $targetFile)) {
                    echo "Error moving uploaded file";
                }//END ERROR
                else {//HANDLE UPLOADS HERE
                	$arr['uploadedFile'] = str_replace(ABSPATH,'',$targetFile);
					//DO SOME PROCESSING
                	if ($postData['process']) {
	                	/* 
	                	* This will process the uploaded file as an items media.
	                	*/
                		if ($postData['isItem']) {
                			
                		}
                		else {//Process non items
                			/*
                			* For images we need the size to be resized to,The type of the resize (scale - crop - box), could be array to create multiple copies - requires prefix. 
                			* We also need somewhere to save it. Like a JSON Gallery or some DB field.
                			*/
                			if ($MediaFilesSettings['type'] == 'image') {
                				
                			}//END IMAGES
                			elseif ($MediaFilesSettings['type'] == 'video') {
                				
                			}
                			else {//JUST SAVE IT
                				
                			}
                		}//END NON ITEMS
                		
                		
                	}//END PROCESSING

				}//END MOVE
                }//END ALL IS WELL
                

            }
    	 
    	 echo json_encode($arr);
    	 
    }//END FILE
    else {
        // See http://php.net/manual/en/features.file-upload.errors.php
        echo "Error code " . $error;
   }
}

class mediaHandler {
	
	
	
   public function CreateImageSet($file,$path,$destinationPath,$settings=0)
   {
		global $sql;
   	    if ($settings['CustomImageSet'] AND is_array($settings['ImageSet'])) {//CUSTOM SET. GET THE IMAGE TYPES DATA
   			$ImageTypes = get_image_types($settings['module']['id'],array('ids'=>implode(",",$settings['ImageSet']),'debug'=>0));	
   		}
   		elseif ($settings['CustomImageSet'] AND !is_array($settings['ImageSet'])) {//FAILSAFE
   			$ImageTypes = get_image_types($settings['module']['id'],array('required'=>1));
   		}
   		elseif (!$settings['CustomImageSet'] AND !is_array($settings['ImageSet'])) {//DEFAULT ACTION, CREATE ALL COPIES
   			$ImageTypes = get_image_types($settings['module']['id']);	
   		}
   		elseif (!$settings['CustomImageSet'] AND is_array($settings['ImageSet'])) {//PROBABLY CAME FROM A READY MADE ARRAY
   			$ImageTypes = $settings['ImageSet'];	
   		}
		
   		foreach ($ImageTypes as $v)
   		{
   			$this->CreateDirs($destinationPath."/".$v['dir']);//CREATE DIRECTORY IF NOT THERE
   			if ($v['settings']['resize']) {
   				$dest = ($v['dir']) ? $destinationPath."/".$v['dir'] : $destinationPath;
   				$Image[$v['title']] = $this->CreateImge($itemid,$v,$file,$path,$dest);//Create Resampled image
   				
   			}//END IF REIZE
   		}

		return $Image;
   }//END METHOD
   
   
   public function CreateDirs($dir)
   {
		if ($dir AND !is_dir($dir)) {
				mkdir($dir,0755);
		}
   }//END public function
   
}//END CLASS


/* 
* input the extention, go to DB, create an array ext=>type, locate type, return array with the folder and other data. Defaults to document
*/
function detectMediaType($ext) {
	global $sql;
	$sql->db_Select('media_files','*');
	$a = execute_multi($sql);

	$exts = array();
	foreach ($a as $v){
		$tmp = explode(';',$v['extentions']);
		foreach ($tmp as $j) {
			if ($j == $ext){
				$exts = $v;
			}
		}
	}
	return $exts;
}


/**
 * Handle file uploads via XMLHttpRequest
 */
class qqUploadedFileXhr {
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    public function save($path) {    
        $input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);
        
        if ($realSize != $this->getSize()){            
            return false;
        }
        
        $target = fopen($path, "w");        
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);
        
        return true;
    }

    /**
     * Get the original filename
     * @return string filename
     */
    public function getName() {
        return $_GET['qqfile'];
    }
    
    /**
     * Get the file size
     * @return integer file-size in byte
     */
    public function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])){
            return (int)$_SERVER["CONTENT_LENGTH"];            
        } else {
            throw new Exception('Getting content length is not supported.');
        }      
    }   
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class qqUploadedFileForm {
	  
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    public function save($path) {
        return move_uploaded_file($_FILES['qqfile']['tmp_name'], $path);
    }
    
    /**
     * Get the original filename
     * @return string filename
     */
    public function getName() {
        return $_FILES['qqfile']['name'];
    }
    
    /**
     * Get the file size
     * @return integer file-size in byte
     */
    public function getSize() {
        return $_FILES['qqfile']['size'];
    }
}

/**
 * Class that encapsulates the file-upload internals
 */
class qqFileUploader {
    private $allowedExtensions;
    private $sizeLimit;
    private $file;
	private $uploadName;

	/**
	 * @param array $allowedExtensions; defaults to an empty array
	 * @param int $sizeLimit; defaults to the server's upload_max_filesize setting
	 */
    function __construct(array $allowedExtensions = null, $sizeLimit = null){
    	if($allowedExtensions===null) {
    		$allowedExtensions = array();
    	}
    	if($sizeLimit===null) {
    		$sizeLimit = $this->toBytes(ini_get('upload_max_filesize'));
    	}
    	        
        $allowedExtensions = array_map("strtolower", $allowedExtensions);
            
        $this->allowedExtensions = $allowedExtensions;        
        $this->sizeLimit = $sizeLimit;
        
        $this->checkServerSettings();       

        if (strpos(strtolower($_SERVER['CONTENT_TYPE']), 'multipart/') === 0) {
            $this->file = new qqUploadedFileForm();
        } else {
            $this->file = new qqUploadedFileXhr();
        }
    }
    
    /**
     * Get the name of the uploaded file
     * @return string
     */
	public function getUploadName(){
		if( isset( $this->uploadName ) )
			return $this->uploadName;
	}
	
	/**
	 * Get the original filename
	 * @return string filename
	 */
	public function getName(){
		if ($this->file)
			return $this->file->getName();
	}
    
	/**
	 * Internal function that checks if server's may sizes match the
	 * object's maximum size for uploads
	 */
    private function checkServerSettings(){        
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));        
        
        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';             
            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");    
        }        
    }
    
    /**
     * Convert a given size with units to bytes
     * @param string $str
     */
    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;        
        }
        return $val;
    }
    
	/**
	 * Handle the uploaded file
	 * @param string $uploadDirectory
	 * @param string $replaceOldFile=true
	 * @returns array('success'=>true) or array('error'=>'error message')
	 */
    function handleUpload($uploadDirectory, $replaceOldFile = FALSE){
        if (!is_writable($uploadDirectory)){
            return array('error' => "Server error. Upload directory isn't writable.");
        }
        
        if (!$this->file){
            return array('error' => 'No files were uploaded.');
        }
        
        $size = $this->file->getSize();
        
        if ($size == 0) {
            return array('error' => 'File is empty');
        }
        
        if ($size > $this->sizeLimit) {
            return array('error' => 'File is too large');
        }
        
        $pathinfo = pathinfo($this->file->getName());
        $filename = $pathinfo['filename'];
        //$filename = md5(uniqid());
        $ext = @$pathinfo['extension'];		// hide notices if extension is empty

        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
        }
        
        $ext = ($ext == '') ? $ext : '.' . $ext;
        
        if(!$replaceOldFile){
            /// don't overwrite previous files that were uploaded
            while (file_exists($uploadDirectory . DIRECTORY_SEPARATOR . $filename . $ext)) {
                $filename .= rand(10, 99);
            }
        }
        
		$this->uploadName = $filename . $ext;
		
        if ($this->file->save($uploadDirectory . DIRECTORY_SEPARATOR . $filename . $ext)){
            return array('success'=>true);
        } else {
            return array('error'=> 'Could not save uploaded file.' .
                'The upload was cancelled, or server error encountered');
        }
        
    }    
}
?>