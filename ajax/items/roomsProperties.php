<?php
$current_module = $loaded_modules[$_GET['module']];
$c = new Items(array('module'=>$current_module));
if (!$box) {
	$sql->db_Select("modules_boxes",'id,name,template,settings,file',"file = '".$_GET['file']."'");
	$box = execute_single($sql);
	$box['settings'] = json_decode($box['settings'],true);
	$smarty->assign("quickEdit",$_GET['trigger']);
}
$item_settings = array('fields'=>'*','thumb'=>1,'CatNav'=>1,'debug'=>0,'main'=>0,'parse'=>'toForm','GetCommonCategoriesTree'=>1,'returnSimple'=>1);

$item = $c->GetItem($_GET['id'],$item_settings);
$smarty->assign("item",$item);
$allCategories = $c->AllItemsTreeCategories();

$sql->q("SELECT * from types");
$types = execute_multi($sql);
$sql->q("SELECT * from facilities");
$facilities = execute_multi($sql);
$sql->q("SELECT * from themeshotels");
$themes = execute_multi($sql);
$sql->q("SELECT * from services");
$services = execute_multi($sql);


$sql->q("SELECT itemid from hotel_types where hotelid=".$_GET['id']);
$hotel_types = execute_multi($sql);
foreach((array)$hotel_types as $key=>$value) {
	$hotel_types_simple[$value['itemid']] = $value['itemid'];
}
$sql->q("SELECT itemid from hotel_themeshotels where hotelid=".$_GET['id']);
$hotel_themes = execute_multi($sql);
foreach((array)$hotel_themes as $key=>$value) {
	$hotel_themes_simple[$value['itemid']] = $value['itemid'];
}
$sql->q("SELECT itemid from hotel_facilities where hotelid=".$_GET['id']);
$hotel_facilities = execute_multi($sql);
foreach((array)$hotel_facilities as $key=>$value) {
	$hotel_facilities_simple[$value['itemid']] = $value['itemid'];
}

$sql->q("SELECT itemid from hotel_services where hotelid=".$_GET['id']);
$hotel_services = execute_multi($sql);
foreach((array)$hotel_services as $key=>$value) {
	$hotel_services_simple[$value['itemid']] = $value['itemid'];
}




if (is_array($item['category'])) {
foreach ($item['category'] as $v)
{
	$simplified_categories[$v['catid']] = $v['main'];
}	
}
$p = new SettingsManager();
$tags = new tags(array('module'=>$current_module,'debug'=>0,'itemid'=>$_GET['id'],'orderby'=>'orderby'));
$smarty->assign("current_module",$current_module);
$smarty->assign("tags",$tags->getAllTags(array('debug'=>0)));
$smarty->assign("itemTags",json_encode($tags->GetTagItemsByItemid($_GET['id'])));
$smarty->assign("image_types",get_image_types($current_module['id']));
$smarty->assign("simple_categories",$simplified_categories);


$smarty->assign("types",$types);
$smarty->assign("facilities",$facilities);
$smarty->assign("themes",$themes);
$smarty->assign("services",$services);

$smarty->assign("hotel_types",$hotel_types_simple);
$smarty->assign("hotel_themes",$hotel_themes_simple);
$smarty->assign("hotel_facilities",$hotel_facilities_simple);
$smarty->assign("hotel_services",$hotel_services_simple);




$smarty->assign("allcategories",$allCategories);
$smarty->assign("pageSettings",$p->getSettings($current_module['name'],basename($box['file']),array('debug'=>0)));

$smarty->assign("box",$box);
$t = ($box['template']) ? $box['template'] : 'admin2/ajax/item/itemsGeneralForm.tpl';
$template = $smarty->fetch($t);
$output = array('template'=>$template,'box'=>$box,'a'=>$_GET);
if($_GET['simple']) { echo $template; exit(); }
if (!$buffer) {
	echo json_encode($output);
	exit();
}


?>