<?php 

if (!call_user_func($_POST['route']['page'],$_POST)) {
	exit();
}


function account ($posted_data) {
	global $sql;
	$tmpObj = new Hotelier();
	if ($posted_data['route']['action'] == "save" ) {

		foreach($posted_data['data'] as $key=>$value) {
			if (!strstr($key, 'settings-')) {
				$tmpArr[$value['field']][$key] =$value['value'];
			} else {
				list($field,$field_name)=split("settings-",$key);
				$tmpArr[$value['field']]['settings'][$field_name] =$value['value'];
			}
		}
		
		$tmpArr['usermain']['id']=ID;
		$settings=array("action"=>"modifydUser");
		$tmp = $tmpObj->addUser($tmpArr['usermain'],$settings);	
		$tmp = $tmpObj->UpdateAccount($tmpArr['usersecondary']);
		
	} else {
		
		$settings['search'][] = array( "users.id" => ID );
		$settings['profile'] =1;
		$tmpArr = $tmpObj->GetHotelier($settings);
		
	}
	//$sql->db_Select('country_codes',"code,country","lang='".FRONT_LANG."'");
	$sql->db_Select('country_codes',"code,country","lang='en'");
	$tmpArr[0]['countrycodes']=execute_multi($sql);
	
	$sql->q("SELECT id,country_codes.code,countries_ph_codes.country,prefix from countries_ph_codes INNER JOIN  country_codes ON country_codes.code=countries_ph_codes.code");
	$tmpArr[0]['countryphonecodes']=execute_multi($sql);
	echo json_encode($tmpArr[0],true);
}

function payment ($posted_data) {
	
	$tmpObj = new Hotelier();
	if ($posted['route']['action'] == "save" ) {
		$tmpArr = $tmpObj->UpdatePayment();
	} else {	
		$settings['search'][] = array( "users.id" => ID );
		$settings['fields']= array("users_payment_method.*");
		$settings['profile'] =0;
		$settings['JoinInner'][] = array("name"=>"users_payment_method","idKey"=>"users_payment_method.uid","idRel"=>"users.id","joinType"=>"INNER JOIN");
		$tmpArr = $tmpObj->GetHotelier($settings);	
	}
	
}


function billing_profiles ($posted_data) {
	global $sql;
	if ($posted_data['route']['action']=='delete') {
	//	$sql->db_Delete("users_profile_billing","id=".$posted_data['route']['value']);
		echo json_encode($posted_data['route']['value'],true);
		exit;
	}
	
	$tmpObj = new Hotelier();
	
		$settings['search'][] = array( "users.id" => ID );
		$settings['fields']= array("users_profile_billing.*");
		$settings['profile'] =0;
		$settings['JoinInner'][] = array("name"=>"users_profile_billing","idKey"=>"users_profile_billing.uid","idRel"=>"users.id","joinType"=>"INNER JOIN");
		$tmpArr = $tmpObj->GetHotelierBillingProfile($settings);
		
		echo json_encode($tmpArr,true);
	
}

function email_notification() {
	
}

function new_billing ($posted_data) {
	global $sql;
	$tmpObj = new Hotelier();

	
	if ($posted_data['route']['action'] == "save" ) {	
	
		foreach($posted_data['data'] as $key=>$value) {
			if (!strstr($key, 'settings-')) {
				$tmpArr[$value['field']][$key] =$value['value'];
			} else {
				list($field,$field_name)=split("settings-",$key);
				$tmpArr[$value['field']]['settings'][$field_name] =$value['value'];
			}
		}

		$tmpArr['id']=$posted_data['route']['value'];
		$tmpArr = $tmpObj->UpdateBillingProfile($tmpArr);
		$posted_data['route']['value'] = ($posted_data['route']['value']) ? $posted_data['route']['value'] : $tmpArr;
	}
		
		$tmpObj = new Hotelier();
		
		
		$settings['search'][] = array( "users.id" => ID,"users_profile_billing.id" => $posted_data['route']['value'] );
		$settings['fields']= array("users_profile_billing.*");
		$settings['profile'] =0;
		$settings['JoinInner'][] = array("name"=>"users_profile_billing","idKey"=>"users_profile_billing.uid","idRel"=>"users.id","joinType"=>"INNER JOIN");
		$tmpArr = $tmpObj->GetHotelierBillingProfile($settings);
		
		echo json_encode($tmpArr[0],true);

	
}


function change_password ($posted_data) {
	global $sql;
	if ($posted_data['route']['action'] == "passwordcheck" ) {
		
		$sql->q("SELECT pass from users where id=".ID);
		$user = execute_single($sql);
		$user_pass = text_decrypt($user['pass']);
		
		  if($user_pass==$posted_data['data']['pass']['value']) 
		  {
		    echo json_encode(array("result"=>"true"),true);
		    exit;
		  } 
		  else 
		  {
		  	echo json_encode(array("result"=>"false"),true);
		  	exit;
		  }
		
		
	} else {
	if($posted_data['data']['newpass']['value']!="") {	
		$pass= text_crypt($posted_data['data']['newpass']['value']);
		
		$sql->q("UPDATE users SET pass='".$pass."' where id=".ID);
		echo json_encode(array(true));
	  }
	}
}

exit;

?>