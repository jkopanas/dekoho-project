<?php

if (! call_user_func ( $_POST ['route'] ['page'], $_POST )) {
	exit ();
}

function addOffer ($posted_data) {
	global $sql;
	$sql->db_Select("orders_types","*","module='rooms'");
	$res=execute_single($sql);
	$res['settings'] = (!empty($res['settings'])) ? json_decode($res['settings'],true) : array();

	echo json_encode($res,true);
	exit();
}


function OfferHotelSelect($posted_data) {
	
	global $loaded_modules;
$settings = array();
$settings['debug']=0;
$settings['filters'] = array(
		'images' => 1,
		'execute' => "searchBySQL",
		"groupBy" => "id",
		'sort_field' 	=> 'id',
		'sort_direction'=> 'DESC',
		"hotelProperties" => false,
		'fields'		=> array (
				"maps_items.title",
				"maps_items.geocoderAddress",
				"hotel.*",
				"count(rooms_hotel.hotel_id) as roomCount"
		)
);

$settings['overwriteJoin'] = array(
		array(
				'name'=>"maps_items",
				"idKey"=>"maps_items.itemid",
				"idRel"=>"hotel.id",
				"joinType"=>"LEFT JOIN"
		),
		array(
				'name'=>"rooms_hotel",
				"idKey"=>"rooms_hotel.hotel_id",	
				"idRel"=>"hotel.id",
				"joinType"=>"LEFT JOIN"
		)		
);

$settings['searchfilters'][] = array(
		'item'     => "enable",
		'type'     => "eq",
		'val'      => "1",
);

$tmpObj = new Hotels();
$tmpArr=$tmpObj->getHotel($settings);

if (is_array($tmpArr['results'][0])) {
	foreach($tmpArr['results'] as $key => $value) {
		$tmpArr['results'][$key]['checked'] = ($value['id'] == $posted_data['route']['value']) ? "checked" : "";

		$current_module = $loaded_modules['hotel'];
		$h = new Items(array('module'=>$current_module));
		$item_settings = array('fields'=>'id','thumb'=>1,'CatNav'=>1,'debug'=>0,'cat_details'=>1,'main'=>1,'images'=>1,'grouped'=> 1, 'getbyField' => "title",'efields'=>1,'parse'=>1);
		$req=new UpdateItem(array("table"=> "hotel"));
		list($reqRes,$REqField)=$req->GetRequest($value['id'],array("getFields" => 1));
		$tmpArr['results'][$key] = array_merge ($tmpArr['results'][$key],$h->GetItem($value['id'],$item_settings),(array) $reqRes[$value['id']]);
		
	}
} else {
	$tmpArr['results'] = array();
}
echo json_encode($tmpArr['results'],true);
exit;

}

function previewOrder ($posted_data) {
	if ( $posted_data['route']['action'] == "orderPreview" ) {
	$tmpObj = new Hotelier();
	$tmpArr = array();
	
	$settings['search'][] = array( "users.id" => ID );
	$settings['fields']= array("users_payment_method.*");
	$settings['profile'] =0;
	$settings['JoinInner'][] = array("name"=>"users_payment_method","idKey"=>"users_payment_method.uid","idRel"=>"users.id","joinType"=>"INNER JOIN");
	$tmpArr['payment'] = $tmpObj->GetHotelier($settings);

	$settings['fields']= array("users_profile_billing.*");
	$settings['JoinInner'][0] = array("name"=>"users_profile_billing","idKey"=>"users_profile_billing.uid","idRel"=>"users.id","joinType"=>"INNER JOIN");
	$tmpArr['billing'] = $tmpObj->GetHotelierBillingProfile($settings);
	
	echo json_encode($tmpArr,true);
	exit;
	
	} else if ($posted_data['route']['action'] == "CheckCoupon")  {
		
		global $sql;
		
		foreach($posted_data['data'] as $key=>$value) {
			$tmpArr[$value['field']][$key] =$value['value'];
		}

		$sql->db_Select("orders_types","*","module='rooms'");
		$res=execute_single($sql);
		$price=json_decode($res['settings'],true);
		$ar = array();
		$coup = array();

		$cur = explode(".",microtime(true));
		foreach($tmpArr['coupon'] as $key => $value) {
		
			$ar[$value][]=$price['price'];
			$p = array_sum($ar[$value]);
			$sql->db_Select("coupons","id","uid=".ID." and code in ('".$value."') AND start_date < ".$cur[0]." and end_date > ".$cur[0]." and price >= ".$p);
			$res=execute_single($sql);
			
			if ($res['id'] ) {
				$coup[$key] = "icon-ok";	
			} else {
				$coup[$key] = "icon-remove";
			}	
			
		}
		echo json_encode($coup,true);
		exit;
		
	}
}

function OfferRoomSelect ($posted_data) {
	
	

	
	$settings=array();
	$settings['searchfilters'][] = array(
			'item'     => "archive",
			'type'     => "eq",
			'val'      => "0",
	);
	$settings['filters']['sort_field'] ="orders.id";
	$tmpObj = new Orders();
	$resOffers=$tmpObj->getOfferOrders($settings);
	
	foreach($resOffers['results'] as $key => $value) {
		$offerRes[$value['itemid']]	=$value;
	}
	
	$settings = array();
	
	$settings['debug'] = 0;
	$settings['filters'] = array(
			'execute'=>'searchBySQL',
			"roomProperties"=> false,
			'fields' => array(
					"rooms.title",
					"rooms.id",
					"rooms_hotel.room_id as room_id",
					"rooms_hotel.availability as roomCount",
					"rooms_hotel.room_type as RoomType",
					"rooms_hotel.price as price",
					"rooms_hotel.availability as quantity",
					"rooms_hotel.discount",
					"rooms_hotel.start_discount_date as startdate",
					"rooms_hotel.end_discount_date as  enddate"
			)
	);
	
	$settings['overwriteJoin'] = array(
			array('name'=>"rooms_hotel",	"idKey"=>"rooms_hotel.room_id",		"idRel"=>"rooms.id",			"joinType"=>"INNER JOIN")
	);
	
	$settings['searchfilters'][] = array(
			'item'     => "rooms_hotel.hotel_id",
			'type'     => "eq",
			'val'      => $posted_data['route']['value'],
	);
	
	$settings['searchfilters'][] = array(
			'item'     => "enable",
			'type'     => "eq",
			'val'      => "1",
	);

	$tmpObj = new Rooms();
	
	$tmp=$tmpObj->getRoom($settings);
	$cur=explode(".",microtime(true));	
	if ($tmp['results']['0']) {
		foreach($tmp['results'] as $key => $value) {
			$tmp['results'][$key]['enabled'] = (substr($value['enddate'],0,10) >= $cur[0] ) ? "disabled"  : "" ;
			if ( $value['enddate'] != 0 ) {
				$tmp['results'][$key]['enddate']= date('m/d/Y', substr($value['enddate'],0,10));
				$tmp['results'][$key]['startdate']= date('m/d/Y', substr($value['startdate'],0,10));
			}
			
			if ( $tmp['results'][$key]['enabled'] == "" && isset($offerRes[$value['room_id']]) ) {	
				$tmp['results'][$key]['discount'] = $offerRes[$value['room_id']]['settings']['discount'];
				$tmp['results'][$key]['enddate'] = date('m/d/Y', substr($offerRes[$value['room_id']]['settings']['end_date'],0,10)); 
				$tmp['results'][$key]['startdate'] = date('m/d/Y', substr($offerRes[$value['room_id']]['settings']['start_date'],0,10));
			}
			$req=new UpdateItem(array("table"=> "rooms"));
			list($reqRes,$REqField)=$req->GetRequest($value['id'],array("getFields" => 1));
			$tmp['results'][$key]= array_merge ($tmp['results'][$key],(array) $reqRes[$value['id']]);
			
		}
	} else {
		$tmp['results']=array();
	}
	

	
	echo json_encode(  $tmp['results'] ,true);
	exit;
	
}


function OfferCouponCheck ($posted_data) {
	global $sql;
	
	
}

?>