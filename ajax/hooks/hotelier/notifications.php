<?php

if (! call_user_func ( $_POST ['route'] ['page'], $_POST )) {
	exit ();
}

function notifications($posted_data) {

	
	if ($posted_data['route']['action']=="deleted") {
		$tmpObj = new Notifications();
		$notifications=$tmpObj->DeleteNotifications($posted_data['data']['selected']['value']);
		echo json_encode($notifications,true);
		exit;
	}
	
	
$tmpObj = new Notifications();
$settings=array();
$settings['searchfilters'][] = array(
		'item'     => "uid",
		'type'     => "eq",
		'val'      => ID,
);

$settings['overwriteJoin']=array();
$settings['debug']=0;
$settings['filters']['return']="total";
$settings['filters']['groupBy']="users_notification.id";
$settings['filters']['sort_field'] ="users_notification.id";

$settings['filters']['page'] = ($posted_data['route']['value']) ? $posted_data['route']['value'] : 1;
$settings['filters']['fields'] = array("users_notification.id");
$total=$tmpObj->getNotifications($settings);

$settings['debug']=0;
$settings['filters']['return']="paginated";
$settings['filters']['fields'] = array("*");
$res=$tmpObj->getNotifications($settings);

echo json_encode(array("results" => $res['results'], "pages"=> $settings['filters']['page'], "totals"=> $total['results'][0]['total']),true);
exit;

}



function sendNotification ($posted_data) {
	
	$tmpObj = new Notifications();
	$posted_data['data']['uid']=$posted_data['uid'];
	return $tmpObj->SendNotifications($posted_data['data']);
	
}



?>