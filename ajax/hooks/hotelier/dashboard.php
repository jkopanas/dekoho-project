<?php

$settings = array();
$settings['debug']=0;
$settings['filters'] = array(
		'perPage' => "10",
		'page' => $_POST['route']['value'],
		'images' => 1,
		'sort_field' 	=> 'id',
		'sort_direction'=> 'DESC',
		'fields' => array(
				"maps_items.title",
				"maps_items.geocoderAddress",
				"hotel.*"
		)
);

$settings['overwriteJoin'] = array(
		array(
				'name'=>"maps_items",
				"idKey"=>"maps_items.itemid",
				"idRel"=>"hotel.id and maps_items.module='hotel'",
				"joinType"=>"LEFT JOIN"
		)
);
$tmpObj = new Hotels();
$tmpArr=$tmpObj->getHotelSpecialOffers($settings);




foreach((array)$tmpArr['results'] as $key => $value) {
	$current_module = $loaded_modules['hotel'];
	$h = new Items(array('module'=>$current_module));
	$item_settings = array('fields'=>'id','thumb'=>1,'CatNav'=>1,'debug'=>0,'cat_details'=>1,'main'=>1,'images'=>1,'grouped'=> 1, 'getbyField' => "title",'efields'=>1,'parse'=>1);
	$tmpArr['results'][$key] = array_merge($h->GetItem($value['id'],$item_settings),$value);
}

$tmpArr['userdetails'] =array("username"=>UNAME,"name"=>USER_NAME,"surname"=>USER_SURNAME);
$tmpArr['pages'] = (isset($_POST['route']['value']))? $_POST['route']['value'] : 1;
echo json_encode($tmpArr,true);
exit;