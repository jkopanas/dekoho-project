<?php
//print_ar($_POST);


	if (! call_user_func ( (isset($_POST ['route'] ['page'])) ? $_POST ['route'] ['page']: $_GET['action'] , (!empty($_POST)) ? $_POST : $_GET )) {
		exit ();
	}


function printpdf($posted_data) {
	
	global $sql,$smarty;

	$content = $smarty->fetch("../orders/order-".ID."-".$posted_data['id'].".tpl");
	###### PDF #####
	require_once($_SERVER['DOCUMENT_ROOT'].'/includes/tcpdf/config/lang/eng.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/includes/tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->SetHeaderData('logo.jpg', PDF_HEADER_LOGO_WIDTH, SITE_NAME, '#'.$posted_data['id']);
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	$pdf->setLanguageArray($lang);
	$pdf->SetFont('freeserif', '', 10);
	$pdf->AddPage();
	$pdf->writeHTML($content, true, 0, true, 0);
	$pdf->lastPage();
	$js .= 'print(true);';
	$pdf->IncludeJS($js);
	$pdf->Output($order['id'].'.pdf', 'D');
	exit();
	
}

function orders($posted_data) {

		
		$tmpObj = new Orders();
		$settings=array();
		$settings['searchfilters'][] = array(
				'item'     => "uid",
				'type'     => "eq",
				'val'      => ID,
		);
		
		$settings['overwriteJoin']=array();
		$settings['debug']=0;
		$settings['filters']['return']="total";
		$settings['filters']['groupBy']="orders.txn_id";
		$settings['filters']['sort_field'] ="orders.id";
		
		$settings['filters']['page'] = ($posted_data['route']['value']) ? $posted_data['route']['value'] : 1;
		$settings['filters']['fields'] = array("orders.id");
		$total=$tmpObj->getOrders($settings);
		$settings['filters']['return']="paginated";
		$settings['filters']['fields'] = array("sum(price) as total","orders.id","orders.transaction_id","orders.txn_id","orders.date_added","orders.payment_method","orders.status");
		$res=$tmpObj->getOrders($settings);
		
		echo json_encode(array("results" => $res['results'], "pages"=> $settings['filters']['page'], "totals"=> $total['results'][0]['total']),true);
		exit;


}



?>