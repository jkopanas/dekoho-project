<?php
if ($_GET['action'] == "enableExtraField" ) {
	
	$sql->db_Update("extra_field_values","enable = 1 WHERE itemid=".$_GET['id']." AND fieldid =".$_GET['fieldid']);
	echo json_encode(array("id" =>$_GET['var'], "enable" => true),true);
	
}

 /// DEprecated remove after update /////
if ($_GET['action'] == "MediaActions" ) {
	if ($_GET['mode'] == "activateMedia" ) {
		foreach ($_POST['ids'] as $key => $value ) {
			$sql->db_Update("item_images","enable = 1 WHERE id=".$value);
		}
		echo json_encode(array("id" =>$_POST['ids'], "enable" => true),true);
	}
}

if ($_GET['action'] == 'closed') { //support ticket
	$posted_data=$_POST;
	
	$tmpObj = new Support();
	$ar=explode(",",$posted_data['data']['selected']['value']);

	foreach($ar as $key => $value) {
		$tmpObj->ConversationMsgState(array("id" => $value,"state" => "1"));
	}
	
	echo json_encode($ar,true);
	exit;
	
}

if ($_GET['action'] == 'opened') { //support ticket
	$posted_data=$_POST;

	$tmpObj = new Support();
	$ar=explode(",",$posted_data['data']['selected']['value']);

	foreach($ar as $key => $value) {
		$tmpObj->ConversationMsgState(array("id" => $value,"state" => "0"));
	}

	echo json_encode($ar,true);
	exit;

}



if ($_GET['action'] == 'saveImage') {
	$module = $loaded_modules[$_GET['module']];
	$current_module = $module;
	$media = (!$_GET['media']) ? 'image' : $_GET['media'];
	$MediaFilesSettings = MediaFiles(array('single'=>1,'type'=>$media));

	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $MediaFilesSettings['folder']."/".$current_module['name']."_".$_GET['id'];
	if (!is_dir($targetPath)) {
		mkdir($targetPath,0755);
	}//END IF

	$fileParam = ($_GET['fileParam']) ? $_GET['fileParam'] : $_POST['fileParam'];
	$files = $_FILES[$fileParam];
	if (isset($files['name']))
	{
		$error = $files['error'];

		if ($error == UPLOAD_ERR_OK) {
			$targetFile = $targetPath."/".basename($files["name"]);
			$uploadedFile = $files["tmp_name"];
			if (is_uploaded_file($uploadedFile)) {
				if (!move_uploaded_file($uploadedFile, $targetFile)) {
					echo "Error moving uploaded file";
				}
				else {
					include($_SERVER['DOCUMENT_ROOT']."/modules/common_functions.php");
					$a = new mediaFiles(array('module'=>$module));
					$fileType = (!$_GET['fileType']) ? 0 : $_GET['fileType'];
					if ($media == 'image') {
						$imageCategory = ($_GET['imageCategory']) ? $_GET['imageCategory'] : $_POST['imageCategory'];
						$imageCategory = (!$imageCategory) ? 0 : $imageCategory;
						if ($imageCategory == 0 ) {	
							$sql->db_Select("item_images","id","itemid=".$_GET['id']." and module='".$_GET['module']."' and type=0");
							$id=execute_single($sql);
							$sql->db_Delete("users_request","`table`='item_images' and uid=".ID." and status=0 and itemid=".$id['id']);
						}				
						$up = $a->handleImageUpload($_GET['id'],basename($files["name"]),$targetPath,$targetPath,array('fileType'=>$fileType,'imageCategory'=>$imageCategory,'keepOriginal'=>$module['settings']['keep_original_image']));
						if ($module['settings']['keep_original_image']) {

						}//END ORIGINALS
					}//END IMAGES
					elseif ($media == 'video') {
						$up = $a->SaveVideo($_GET['id'],basename($files["name"]),$a->CheckDocument($targetFile,$targetPath,basename($files["name"])),$_POST);
					}//END VIDEO
					else {
						$up = $a->SaveDocument($_GET['id'],basename($files["name"]),$a->CheckDocument($targetFile,$targetPath,basename($files["name"])));
					}//END OTHER TYPES
						

					$req=new UpdateItem(array("table"=> "item_images"));
					$data = array($tmpArr['saveExtra']);
					foreach ($data as $key => $value) {
						$tmp['field']="original_file";
						$tmp['value']=$files["name"];
						$tmp['itemid']=$up[key($up)]['imageid'];
						$tmp['key']="id";
						$tmp['hotelid']=$_GET['hotelid'];
						$req->InsertRequest($tmp);
					}
						
				}//END MOVE
			}//END ALL IS WELL


		}
	} else {
		// See http://php.net/manual/en/features.file-upload.errors.php
		echo "Error code " . $error;
	}

	header('Content-Type: text/plain;');

	$data = array('foo' => $_GET, 'status' => 'ok','uploadedFile'=>$up);

	echo json_encode($data);
	exit();
}

if ($_GET['action'] == "enablePhotos" ) {
	$posted_data=$_POST;
	
	$req = new UpdateItem(array("table"=>"item_images"));
	foreach (explode(",",$posted_data['data']['ids']['value']) as $key => $value) {
		$req->UpdateRequest(array("id"=> $value ,"field" => "original_file","status" => 0 ),1);
	}
	$sql->db_Update("item_images","enable = 1 WHERE id in (".$posted_data['data']['ids']['value'].")");
	echo json_encode($posted_data['data']['ids']['value'],true);
	exit();
}

if ($_GET['action'] == "disablePhotos" ) {
	$posted_data=$_POST;
	
	$req = new UpdateItem(array("table"=>"item_images"));
	foreach (explode(",",$posted_data['data']['ids']['value']) as $key => $value) {
		$req->UpdateRequest(array("id"=> $value ,"field" => "original_file","status" => 0 ),0);
	}	
	
	$sql->db_Update("item_images","enable = 0 WHERE id in (".$posted_data['data']['ids']['value'].")");
	echo json_encode($posted_data['data']['ids']['value'],true);
	exit();
}


if ($_GET['action'] == "enableRooms" ) {

	$posted_data=$_POST;
	$ar = (empty($posted_data['data']['selected']['value'])) ? array ($posted_data['route']['value']) : explode(",",$posted_data['data']['selected']['value']);

	foreach ($ar as $key => $value ) {
		$sql->db_Update("rooms","enable = 1 WHERE id=".$value);
		$sql->db_Update("rooms_hotel","active = 1 WHERE room_id=".$value);
	}
	
	echo json_encode($ar,true);
	exit();
	
}


if ($_GET['action'] == "disableRooms" ) {

	$posted_data=$_POST;
	$ar = (empty($posted_data['data']['selected']['value'])) ? array ($posted_data['route']['value']) : explode(",",$posted_data['data']['selected']['value']);

	foreach ($ar as $key => $value ) {
		$sql->db_Update("rooms","enable = 0 WHERE id=".$value);
		$sql->db_Update("rooms_hotel","active = 0 WHERE room_id=".$value);
	}
	echo json_encode($ar,true);
}


if ($_GET['action'] == "enableExtras" ) {

	$posted_data=$_POST;
	$ar = (empty($posted_data['data']['ids']['value'])) ? array ($posted_data['route']['value']) : explode(",",$posted_data['data']['ids']['value']);
	$req = new UpdateItem(array("table"=>"extra_field_values"));	
	foreach ($ar as $key => $value ) {
		$req->UpdateRequest(array("id"=> $value ,"field" => "value","status" => 0 ),1);
		$sql->db_Update("extra_field_values","enable = 1 WHERE itemid=".$_GET['value']." and fieldid=".$value);
	}

	echo json_encode($ar,true);
	exit();

}


if ($_GET['action'] == "disableExtras" ) {

	$posted_data=$_POST;
	$ar = (empty($posted_data['data']['ids']['value'])) ? array ($posted_data['route']['value']) : explode(",",$posted_data['data']['ids']['value']);
	$req = new UpdateItem(array("table"=>"extra_field_values"));
	foreach ($ar as $key => $value ) {
		$req->UpdateRequest(array("id"=> $value ,"field" => "value","status" => 0 ),0);
		$sql->db_Update("extra_field_values","enable = 0 WHERE  itemid=".$_GET['value']." and fieldid=".$value);
	}
	echo json_encode($ar,true);
}

if ($_GET['action'] == "activateExtras" ) {

	global $sql;
	$posted_data=$_POST;
	$ar = (empty($posted_data['data']['ids']['value'])) ? $posted_data['route']['value'] : $posted_data['data']['ids']['value'];
	$sql->db_Update("extra_fields", "active='Y' where fieldid in (".$ar.")");
	echo json_encode($ar,true);
}


if ($_GET['action'] == "deactivateExtras" ) {
	
	global $sql;
	$posted_data=$_POST;
	$ar = (empty($posted_data['data']['ids']['value'])) ? $posted_data['route']['value'] : $posted_data['data']['ids']['value'];
	$sql->db_Update("extra_fields", "active='N' where fieldid in (".$ar.")");
	echo json_encode($ar,true);
}


if ($_GET['action'] == "enableFilters" ) {
	
	$posted_data=$_POST;
	$sql->db_Update($_GET['value'],"enable = 1 WHERE id  in (".$posted_data['data']['ids']['value'].")");
	$req = new UpdateItem(array("table"=>$_GET['value']));
	
	foreach (explode(",",$posted_data['data']['ids']['value']) as $key => $value) {
		$req->UpdateRequest(array("id"=> $value ,"field" => "title","status" => 0 ),1);
	}
	echo json_encode(array("table"=> $_GET['value'],"id"=>$posted_data['data']['ids']['value']),true);
	
}

if ($_GET['action'] == "disableFilters" ) {
	
		$posted_data=$_POST;
		$sql->db_Update($_GET['value'],"enable = 0 WHERE id  in (".$posted_data['data']['ids']['value'].")");
		$req = new UpdateItem(array("table"=>$_GET['value']));
		foreach (explode(",",$posted_data['data']['ids']['value']) as $key => $value) {
			$req->UpdateRequest(array("id"=> $value ,"field" => "title","status" => 0 ),0);
		}
		
		echo json_encode(array("table"=> $_GET['value'],"id"=>$posted_data['data']['ids']['value']),true);
		
}

if ($_GET['action'] == "deleteFilters" ) {
	$posted_data=$_POST;
	$sql->db_Delete($_GET['value']," id  in (".$posted_data['data']['ids']['value'].")");
	//echo $_GET['value']," id  in (".$posted_data['data']['ids']['value'].")";
	$req = new UpdateItem(array("table"=>$_GET['value']));
	foreach (explode(",",$posted_data['data']['ids']['value']) as $key => $value) {
			$req->DeleteRequest(array("id"=> $value ,"field" => "title" ));
	}
	echo json_encode(array("table"=> $_GET['value'],"id"=>$posted_data['data']['ids']['value']),true);
}


if ($_GET['action'] == "enableHotels" ) {

	$posted_data=$_POST;
	$ar = (empty($posted_data['data']['selected']['value'])) ? array ($posted_data['route']['value']) : explode(",",$posted_data['data']['selected']['value']);

	foreach ($ar as $key => $value ) {
		$sql->db_Update("hotel","enable = 1, active = 1 WHERE id=".$value);
	}
	
	echo json_encode($ar,true);
}


if ($_GET['action'] == "disableHotels" ) {
	
	$posted_data=$_POST;
	$ar = (empty($posted_data['data']['selected']['value'])) ? array ($posted_data['route']['value']) : explode(",",$posted_data['data']['selected']['value']);
	
	foreach ($ar as $key => $value ) {	
		$sql->db_Update("hotel","enable = 0,active=0 WHERE id=".$value);		
	}
	echo json_encode($ar,true);
}


if ($_GET['action'] == "RejectField" ) {

	$posted_data=$_POST;	
	list($field,$id)=explode("-",$posted_data['route']['value']);
	$req = new UpdateItem(array("table"=> $posted_data['data']['table-'.$id]['value']));
	$res=$req->DenyRequest(array("id"=> $id ,"field" => $field ));

	echo json_encode($res,true);
	exit();
}

if ($_GET['action'] == "AcceptField" ) {

	$posted_data=$_POST;
	list($field,$id)=explode("-",$posted_data['route']['value']);
	$req = new UpdateItem(array("table"=> $posted_data['data']['table-'.$id]['value']));
	$value= (isset($posted_data['data'][$field]['value'])) ? $posted_data['data'][$field]['value'] : 0;
	$res=$req->AcceptRequest(array("id"=> $id ,"field" => $field,"value" =>$value ));
	
	echo json_encode($res,true);
	exit();

}


if ($_GET['action'] == "enableHotspots" ) {

	$posted_data=$_POST;
	$sql->db_Update("hotspots","enable = 1 WHERE id=".$_GET['value']);
	echo json_encode($posted_data['route']['value'],true);
		
}


if ($_GET['action'] == "disableHotspots" ) {
	$posted_data=$_POST;
	$sql->db_Update("hotspots","enable = 1 WHERE id=".$_GET['value']);
	echo json_encode($posted_data['route']['value'],true);
}

exit;
?>