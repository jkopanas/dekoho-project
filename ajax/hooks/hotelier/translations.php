<?php 	

$data=$_POST;

if ($_POST['route']['action'] == 'update'){
	
	$t = new Parse();
	$ar=array();
	foreach($data['data'] as $key => $value) {
		if (strstr($key,"-")) {
		
			$field=explode("-",$key);
			$ar[$field[0]][$field[1]][]=$field[2];
			$arid[$key] =$value['value'];
		}
	}
	
	foreach($ar as $key => $value) {
		
		foreach($value as $k => $v) {		
			if ($data['data']['itemid']['value'] AND is_numeric($data['data']['itemid']['value'])) {
					$q ="AND (settings LIKE '%\"field\":\"itemid\"%' AND (settings LIKE '%\"value\":".$data['data']['itemid']['value']."%' OR settings LIKE '%\"value\":\"".$data['data']['itemid']['value']."\"%'))";
			}
			$sql->db_Select("translations","id,field,code,itemid","itemid in ('".implode("','",$v)."') AND code='".$data['data']['currentLang']['value']."' AND field ='".$k."' AND `table` = '".$key."' $q");
			$existing[$key][$k] = execute_multi($sql);
		}
		
	}

	
		foreach ((array)$existing as $key => $value)//BEGIN UPDATING EXISTING
		{

				foreach ($value as $ke => $va)//BEGIN UPDATING EXISTING
				{		
					if (is_array($va)) {	
						foreach ($va as $k => $v)//BEGIN UPDATING EXISTING
						{
							$sql->db_Update("translations","translation = '".$t->toDB($arid[$key."-".$ke."-".$v['itemid']])."' WHERE id = ".$v['id']);
							unset($arid[$key."-".$ke."-".$v['itemid']]);
						}
					}
				}	
			
		}

		foreach ((array)$existing as $key => $value)//BEGIN UPDATING EXISTING
		{		
			$inserts=array();
				foreach ($arid as $k => $v)//BEGIN UPDATING EXISTING
				{
					list($table,$field,$id)=explode("-",$k);
					if ($table == $key ) {
						$valueTrans = $t->toDB($v);			
						$date_added = time();
						$module=$table;
						$settings = json_encode(array());
						if ($data['data']['itemid']['value']) {
							$settings = json_encode(array('extraField'=>array('field'=>'itemid','value'=>$data['data']['itemid']['value'])));
							$module = $data['data']['module']['value'];
						}
						$inserts[] = "'".$data['data']['currentLang']['value']."','".$id."','$table','$field','".$module."','$valueTrans','$date_added','$date_added','1','$settings'";
					}
				}
				if (!empty($inserts)) {
					$sql->db_Insert('translations (translations.code,itemid,translations.table,field,module,translation,date_added,date_modified,active,settings) ', implode("),(",$inserts) );
				}
		}
	echo true;
	exit;
	
} else {
	
	$langHotelier=$Languages->AvailableLanguages(array('active'=>'Y','findDefault'=>'default_lang','replaceURL'=>e_REQUEST_URI));
	
	
	foreach((array) $data['data'] as $key => $value) {
		
		$tmpArr[$value['ttable']][$value['tkey']][$value['tid']][] = $value['tfield']; 
		
		$defaultTranslate[$value['ttable']][$value['tfield']."-".$value['tid']]=$value['value'];
		
		if ($value['ttable'] == "extra_fields") {
			
			$sql->db_Select($value['ttable'],"field","fieldid=".$value['tid']);
			$extrarow=execute_single($sql);
			$extraFields[$value['tfield']."-".$value['tid']]=$extrarow['field'];
		
		}
		
	}
	

	$translations = array();

	foreach ((array)$tmpArr as $key => $value) {	
		
			$fkey = array_keys($value);
			$tid=array_keys($value[$fkey[0]]);
			$tfield =array();
			foreach($value[$fkey[0]] as $ke => $va) {
				$tfield[]=array_values($va);				
			} 
			
			$tfield = array_unique($tfield);
			
			if ($key == "extra_fields") {
				$sql->db_Select("translations","*", "`table`='".$key."' and itemid in ('".implode("','",$tid)."') and  (settings LIKE '%\"field\":\"itemid\"%' AND (settings LIKE '%\"value\":".$data['route']['value']."%' OR settings LIKE '%\"value\":\"".$data['route']['value']."\"%'))");
				$row=execute_multi($sql);
			} else {
				$sql->db_Select("translations","*", "`table`='".$key."' and field in ('".implode("','",$tfield[0])."') and itemid in ('".implode("','",$tid)."')");
				$row=execute_multi($sql);
			}
			if (!empty($row)) {
		
				foreach ($row as $k => $v) {
				
					$translations[$v['code']][$v['table']."-".$v['field']."-".$v['itemid']]=$v['translation'];
				
				}
				
			}

	}
	
	
	foreach($langHotelier as $k => $v) {
		if ( DEFAULT_LANG != $v['code'] ) {
	
			$availableLang[$v['code']]=$v['country'];
		}
	}

	//print_ar($extraFields);

	echo json_encode(array("defaultdata"=>$defaultTranslate,"extraFields" =>$extraFields,"data" => $translations,"availableLang"=> $availableLang,"default" => DEFAULT_LANG),true);
	exit;
}
	?>