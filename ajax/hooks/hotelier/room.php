<?php




foreach((array)$_POST['data'] as $key=>$value) {
	if ($value['field'] == "roommain") {
		$tmpArr[$value['field']][$key] =array("value" => $value['value']);
	} else {
		$tmpArr[$value['field']][$key] =$value['value'];
	}
}



if ($_POST['route']['action'] == "save") {
	
	$tmpObj = new Rooms();
	$tmpObj->UpdateRoom($tmpArr,$_POST ['route'] ['value']);
	
	//echo json_encode(array("room"=>$tmpArr),true);
	//exit;
		
} else if ($_POST['route']['action'] == "create") {
	
	
	$tmpObj = new Rooms ();
	
	$tmpArr['roommain']['hotelKey']['value'] = $_POST['route']['value'];
	$tmpArr = $tmpObj->NewRoom(array("room" => $tmpArr['roommain'],"roomsecondary" => $tmpArr['roomsecondary'],"roomProperties" => $tmpArr['roomProperties']));
	
	echo json_encode(array($tmpArr),true);
	exit;
	
	
} else if ($_POST['route']['action'] == "new") {
	
	$tmpObj = new Rooms();
	$tmpArr=$tmpObj->fields;
		foreach( $tmpObj->__joins as $key => $value ) {
			
			$value['filters']['fields'] = array($key.".id",'title','enable','roomid');
			$value['jointables'] = array(
					array(
							'name'=>$value['table'],
							"idKey"=>$key.".id",
							"idRel"=> $value['table'].".itemid and roomid=0",
							"joinType"=>"LEFT JOIN"
					)
			);
			
			$tmpArr[$key]=$tmpObj->getRoomProperties($key,$value,0);
			
		}
		
		$tmpArr['price']=0;
		$tmpArr['availability']=0;
		
		$settings = array ();
		$settings ['debug'] = 0;
		$settings ['filters'] = array (
				'return' => 'paginated',
				'sort_field' => 'id',
				'sort_direction' => 'DESC',
				'fields' => array (
						'id',
						'title',
						'description',
						'active',
						'stars',
						'pets',
						'enable',
						'hotel.settings'
				)
		);
		$settings['searchfilters'][] = array(
				'item'     => "id",
				'type'     => "eq",
				'val'      => $_POST['route']['value']
		);
		
		$tmpObj = new Hotels ();
		$hotel = $tmpObj->getHotel ( $settings );
		echo json_encode (array("room" => $tmpArr,"title"=>$hotel['results'][0]['title'],"id"=>$hotel['results'][0]['id']),true);
		exit;
	
}
	
	global $loaded_modules;
	$settings = array();
	$settings['debug'] =0;
	$settings['filters'] = array(
			'sort_field' 	=> 'id',
			'sort_direction'=> 'DESC',
			'execute' => "searchBySQL",
            'roomProperties'=>false,
	'fields' => array(
					"rooms.id",
					"rooms.title",
					"rooms.description",
					"rooms.hotelKey",
					"rooms.enable",
					"rooms.date_modified",
					"rooms_hotel.price",
					"rooms_hotel.room_type",
					"rooms_hotel.availability",
					"rooms_hotel.active as active"
			)
	);
	
	$settings['searchfilters'][] = array(
			'item'     => "rooms.id",
			'type'     => "eq",
			'val'      => $_POST['route']['value'],
	);
	
	$tmpObj = new Rooms();
	$tmpArr=$tmpObj->getRoom($settings);
	
	if (isset($tmpArr['results'][0]['id'])) {
		foreach( $tmpObj->__joins as $key => $value ) {		

			$value['filters']['fields'] = array($key.".id",'title','enable','roomid');
			$value['jointables'] = array(
					array(
							'name'=>$value['table'],
							"idKey"=>$key.".id",
							"idRel"=> $value['table'].".itemid and roomid=".$_POST['route']['value'],
							"joinType"=>"LEFT JOIN"
					)
			);

			$tmpArr['results'][0][$key]=$tmpObj->getRoomProperties($key,$value,$tmpArr['results'][0]['id']);
		}
	}
	
	foreach ($tmpArr['results'][0]['roomsfacilities'] as $k => $v) {
		if ($v['enable'] == 0 && $v['hotelid'] == "" && USER_CLASS != "A" ) {
			unset($tmpArr['results'][0]['roomsfaciliities'][$key][$k]);
		}
	}

	$current_module = $loaded_modules['rooms'];
	$r = new Items(array('module'=>$current_module));
	$item_settings = array('fields'=>'id','thumb'=>1,'CatNav'=>1,'debug'=>0,"available"=>1,'cat_details'=>1,'main'=>1,'images'=>1,'grouped'=> 1, 'getbyField' => "title",'efields'=>1,'parse'=>1);
	$res = $r->GetItem($_POST['route']['value'],$item_settings);
	
	//*********** Get Users Request if Exist *********** //
	$req=new UpdateItem(array("table"=> "rooms"));
	list($reqRes,$REqField)=$req->GetRequest($_POST['route']['value'],array("getFields" => 1));
	$res = array_merge($res,$tmpArr['results'][0],(array)$reqRes[$_POST['route']['value']]);
	///////////////////////////////////////////////////

	$res['ReqFields'] = $REqField[$_POST['route']['value']];
	
	 $settings = array ();
	 $settings ['debug'] = 0;
	 $settings ['filters'] = array (
	 		'return' => 'paginated',
	 		'sort_field' => 'id',
	 		'sort_direction' => 'DESC',
	 		'fields' => array (
	 				'id',
	 				'title',
	 				'description',
	 				'active',
	 				'stars',
	 				'pets',
	 				'enable',
	 				'hotel.settings'
	 		)
	 );
	 $settings['searchfilters'][] = array(
	 		'item'     => "id",
	 		'type'     => "eq",
	 		'val'      => $tmpArr['results'][0]['hotelKey']
	);
	 
	 $tmpObj = new Hotels ();
	 $hotel = $tmpObj->getHotel ( $settings );
	 
	 $req=new UpdateItem(array("table"=> "hotel"));
	 list($reqRes,$REqField)=$req->GetRequest($tmpArr['results'][0]['hotelKey'],array("getFields" => 1));
	 $hotel['results'][0]['title'] = (isset($reqRes[$tmpArr['results'][0]['hotelKey']]['title'])) ? $reqRes[$tmpArr['results'][0]['hotelKey']]['title']  : $hotel['results'][0]['title'];
	 
	 
	 
	echo json_encode(array( "room" => $res,"title"=>$hotel['results'][0]['title'],"id"=>$hotel['results'][0]['id']),true);
	exit ();

