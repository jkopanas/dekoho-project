<?php
if (! call_user_func ( $_POST ['route'] ['page'], $_POST )) {
	exit ();
}


function previewOrder ($posted_data) {
	
	global $sql,$loaded_modules;
	$current_module = $loaded_modules['hotel'];
	$h = new Items(array('module'=>$current_module));
	$item_settings = array('fields'=>'*','thumb'=>1,'CatNav'=>1,'debug'=>0,"available"=>1,'cat_details'=>1,'main'=>1,'images'=>1,'grouped'=> 1, 'getbyField' => "title",'efields'=>1,'parse'=>1);
	
	$item = $h->GetItem($posted_data ['route'] ['value'],$item_settings);
	
	
	$sql->db_Select("orders_types","*","module='hotel'");
	$res=execute_single($sql);
	$res['settings'] = (!empty($res['settings'])) ? json_decode($res['settings'],true) : array();
	
	$tmpObj = new Hotelier();
	$tmpArr = array();

	$settings['search'][] = array( "users.id" => ID );
	$settings['fields']= array("users_payment_method.*");
	$settings['profile'] =0;
	$settings['JoinInner'][] = array("name"=>"users_payment_method","idKey"=>"users_payment_method.uid","idRel"=>"users.id","joinType"=>"INNER JOIN");
	$tmpArr['payment'] = $tmpObj->GetHotelier($settings);

	$settings['fields']= array("users_profile_billing.*");
	$settings['JoinInner'][0] = array("name"=>"users_profile_billing","idKey"=>"users_profile_billing.uid","idRel"=>"users.id","joinType"=>"INNER JOIN");
	$tmpArr['billing'] = $tmpObj->GetHotelierBillingProfile($settings);
	$tmpArr['price'] = $res['settings']['price'];
	$tmpArr['items'][0] = $item;
	$tmpArr["title"]=$item['title'];
	 $tmpArr["id"] =$item['id'];
	echo json_encode($tmpArr,true);
}



function hotel($posted_data) {
	
	//print_ar($posted_data);
	if ($posted_data ['route'] ['action'] == "save") {
		foreach($posted_data['data'] as $key=>$value) {
			if ($value['field'] == "hotelmain") {
				$tmpArr[$value['field']][$key] =array("value" => $value['value']);
			} else {
				$tmpArr[$value['field']][$key] =$value['value'];
			}
		}
		
		$tmpObj = new Hotels ();
		
		$tmpArr=$tmpObj->UpdateHotel($tmpArr,$posted_data ['route'] ['value']);

	}
	
	if ($posted_data ['route'] ['action'] == "new") {

		foreach($posted_data['data'] as $key=>$value) {
			if ($value['field'] == "hotelmain") {
				$tmpArr[$value['field']][$key] =array("value" => $value['value']);
			} else {
				$tmpArr[$value['field']][$key] =$value['value'];
			}
		}
		$tmpObj = new Hotels ();
		$tmpArr = $tmpObj->NewHotel(array("hotel" => $tmpArr['hotelmain'],"hotelProperties" => $tmpArr['hotelProperties']));
		echo json_encode($tmpArr,true);
		exit;
	} else if (!isset($posted_data ['route'] ['value'])) {
		
		$tmpObj = new Hotels ();
		$tmpArr=$tmpObj->fields;
		
		foreach( $tmpObj->__joins as $key => $value ) {
			
			$value['filters']['fields'] = array($key.".id",'title','enable','hotelid');
			$value['jointables'] = array(
					array(
							'name'=>$value['table'],
							"idKey"=>$key.".id",
							"idRel"=> $value['table'].".itemid and hotelid=0",
							"joinType"=>"LEFT JOIN"
					)
			);
			$tmpArr[$key]=$tmpObj->getHotelProperties($key,$value,0);
		
		}
		
		//print_ar($tmpArr);
		echo json_encode ($tmpArr,true);
		exit;
		
	} else {
		
		global $loaded_modules;

/*
		 $time = microtime();
		$time = explode(" ", $time);
		$time = $time[1] + $time[0];
		$start = $time;
*/
		
		$current_module = $loaded_modules['hotel'];
		$h = new Items(array('module'=>$current_module));
		$item_settings = array('fields'=>'*','thumb'=>1,'CatNav'=>1,'debug'=>0,"available"=>1,'cat_details'=>1,'main'=>1,'images'=>1,'grouped'=> 1, 'getbyField' => "title",'efields'=>1,'parse'=>1);
		$res = $h->GetItem($posted_data ['route'] ['value'],$item_settings);
		
		if ($res['uid'] != ID) { return; }
		
		$res['settings']['hoteltelephone2'] = (isset($res['settings']['hoteltelephone2'])) ? $res['settings']['hoteltelephone2'] : "";
		$res['settings']['bic'] = (isset($res['settings']['bic'])) ? $res['settings']['bic'] : "";
		$res['settings']['bankaddress'] = (isset($res['settings']['bankaddress'])) ? $res['settings']['bankaddress'] : "";
		$res['settings']['bankname'] = (isset($res['settings']['bankname'])) ? $res['settings']['bankname'] : "";
		$res['settings']['iban'] = (isset($res['settings']['iban'])) ? $res['settings']['iban'] : "";
		$res['settings']['hotelmobile'] = (isset($res['settings']['hotelmobile'])) ? $res['settings']['hotelmobile'] : "";
		$res['settings']['paypalmail'] = (isset($res['settings']['paypalmail'])) ? $res['settings']['paypalmail'] : "";
		$res['settings']['hotelcontactperson'] = (isset($res['settings']['hotelcontactperson'])) ? $res['settings']['hotelcontactperson'] : "";
		$res['settings']['position'] = (isset($res['settings']['position'])) ? $res['settings']['position'] : "";
		$res['settings']['hotelaccountaddress'] = (isset($res['settings']['hotelaccountaddress'])) ? $res['settings']['hotelaccountaddress'] : "";
		$res['settings']['hotelaccountowner'] = (isset($res['settings']['hotelaccountowner'])) ? $res['settings']['hotelaccountowner'] : "";
		
		
		//*********** Get Users Request if Exist *********** //
		$req=new UpdateItem(array("table"=> "hotel"));
		list($reqRes,$REqField)=$req->GetRequest($posted_data ['route'] ['value'],array("getFields" => 1));
		$res = array_merge($res,(array)$reqRes[$posted_data ['route'] ['value']]);
		///////////////////////////////////////////////////
		$res['ReqFields'] = $REqField[$posted_data ['route'] ['value']];


		/*
		 $time = microtime();
		$time = explode(" ", $time);
		$time = $time[1] + $time[0];
		$finish = $time;
		$totaltime = round(($finish - $start), 4);
		$res['time'] = $totaltime;
*/
		
		$tmpObj = new Hotels ();

		
		foreach( $tmpObj->__joins as $key => $value ) {
				
			$value['filters']['fields'] = array($key.".id",'title','enable','hotelid');
			$value['jointables'] = array(
					array(
							'name'=>$value['table'],
							"idKey"=>$key.".id",
							"idRel"=> $value['table'].".itemid and hotelid=".$posted_data ['route'] ['value'],
							"joinType"=>"LEFT JOIN"
					)
			);
			$res[$key]=$tmpObj->getHotelProperties($key,$value,$res['id']);
			foreach ($res[$key] as $k => $v) {
				if ($v['enable'] == 0 && $v['hotelid'] == "" && USER_CLASS != "A" ) {
					unset($res[$key][$k]);
				}
			}
		}
					


		
		echo json_encode ( $res, true );
		exit ();
	}
}

function hotels($posted_data) {
	global $loaded_modules;
	
	$action = explode("-",$posted_data['route']['value']);
	$page = (is_numeric($posted_data['route']['value'])) ? $posted_data['route']['value'] : 1; 
 
	if (is_numeric($action[1])) { $page=$action[1]; }
	
	$ar['data'] = explode(",",$posted_data['data']['selected']['value']);
	$ar['mode'] = $action[0];
	$tmpObj = new Hotels();
	$tmpObj->DashboardHotel($ar);

	$settings = array ();
	$settings ['debug'] = 0;
	$settings ['filters'] = array (
			'return' => 'paginated',
			'page' => $page,
			'perPage' => "10",
			'sort_field' => 'id',
			'sort_direction' => 'DESC',
			'fields' => array (
					"maps_items.title",
					"maps_items.geocoderAddress",
					'id',
					'title',
					'date_added',
					'active',
					'enable',
					'hotel.settings'
			) 
	);
 	
 	$tmpArr=$tmpObj->getHotelByUser($settings);
 	$settings ['filters']['return'] = "total";
 	$settings ['filters']['fields'] =array("hotel.title");
 	$settings ['debug'] = 0;
 	$tmpArrTotal=$tmpObj->getHotelByUser($settings);
 //	print_ar($tmpArr);
 	
 	foreach ($tmpArr['results'] as $key => $value ) {
 		$current_module = $loaded_modules['hotel'];
 		$h = new Items(array('module'=>$current_module));
 		$item_settings = array('fields'=>'id','thumb'=>1,'CatNav'=>1,'debug'=>0,'cat_details'=>1,'main'=>1,'images'=>1,'grouped'=> 1, 'getbyField' => "title",'efields'=>1,'parse'=>1);
 		
 		$req=new UpdateItem(array("table"=> "hotel"));
 		list($reqRes,$REqField)=$req->GetRequest($value['id'],array("getFields" => 1));
 		$tmpArr['results'][$key] = array_merge ($tmpArr['results'][$key],$h->GetItem($value['id'],$item_settings),(array) $reqRes[$value['id']]);
 		
 	}
 	
 	
 	echo json_encode( array ( "results" => $tmpArr['results'],"pages" => $page,"total" =>$tmpArrTotal['results'][0]['total'] ) ,true);
	exit ();
}

function photos ($posted_data) {
	

	foreach((array) $posted_data['data'] as $key=>$value) {
			
		$tmpArr[$value['field']][$key] =$value['value'];
			
	}
	if ( $posted_data['route']['action'] =='delete' ) {
		global $sql;
		
		$sql->db_Select("item_images  inner join ".$tmpArr['PhotoModule']['module']." on ".$tmpArr['PhotoModule']['module'].".id=item_images.itemid","item_images.id","uid=".ID." and item_images.id in (".$posted_data['data']['ids']['value'].")");
		if ($sql->mySQLrows == count(explode(",",$posted_data['data']['ids']['value'])) ) {
			$sql->db_Delete("item_images","id IN (".$posted_data['data']['ids']['value'].") AND module = '".$tmpArr['PhotoModule']['module']."'");
			$sql->db_Delete("item_images_aditional","imageid IN (".$posted_data['data']['ids']['value'].")");

			   ///// Delete My Request ////////
				$req = new UpdateItem(array("table"=>"item_images"));
				foreach (explode(",",$posted_data['data']['ids']['value']) as $key => $value) {
					$req->DeleteRequest(array("id"=> $value ,"field" => "original_file" ));
				}
				///// Delete My Request ////////

		}
		
		echo json_encode($posted_data['data']['ids']['value'],true);
		exit();
	} else if ( $posted_data['route']['action']=='active' ) {
		global $sql;
		$sql->db_Update("item_images","available = 1 WHERE id IN (".$posted_data['data']['ids']['value'].") AND enable=1  AND module = 'hotel'");
		echo json_encode($posted_data['data']['ids']['value'],true);
		exit();
	} else if ($posted_data['route']['action']=='inactive') {
		global $sql;
		$sql->db_Update("item_images","available = 0 WHERE id IN (".$posted_data['data']['ids']['value'].") AND enable=1 AND module = 'hotel'");
		echo json_encode($posted_data['data']['ids']['value'],true);
		exit();
	} else {
		
	global $loaded_modules;
	
	$current_module = $loaded_modules[$posted_data['route']['action']];
	$c = new Items(array('module'=>$current_module));
	$i = new ItemImages(array('module'=>$current_module));
	$id = $posted_data['route']['value'];
	$images = $i->GetItemDetailedImages($id,null);

	foreach ((array) $images['images'] as $key => $value) {
		$images['images'][$key]['imageActive'] = ($value['enable']) ? ($value['available']) ? "activate" : "deactivate" : "pending";
	}

	if (USER_CLASS == "A" && $images['thumbs'] ) {
		$images['thumbs']['imageActive'] = ($images['thumbs']['enable']) ? ($images['thumbs']['available']) ? "activate" : "deactivate" : "pending";
		$images['images'][]=$images['thumbs'];
	}
	
	if (!$images)  {$images = array("images"=>array());}
	$settings = array();
	$settings['debug'] = 0;
	$settings ['filters'] = array (
			'return' => 'paginated',
			'sort_field' => 'id',
			'sort_direction' => 'DESC',
			'fields' => array ( "hotel.id as idhotel","hotel.title as hotelTitle" )
	);
	
	$settings['searchfilters'][] = array(
			'item'     => "id",
			'type'     => "eq",
			'val'      => $posted_data ['route'] ['value'],
	);
	
	$tmpObj = new Hotels ();
	
	if ($posted_data ['route'] ['action'] =="rooms") {
		$settings['overwriteJoin'] = array(
				array(
						'name'=>"hotel",
						"idKey"=>"hotel.id",
						"idRel"=>"rooms.hotelKey",
						"joinType"=>"INNER JOIN"
				)
		);
		$hotel = $tmpObj->getHotelByRoom( $settings );
	} else {
		$hotel = $tmpObj->getHotel ( $settings );
	}
	
	$req=new UpdateItem(array("table"=> "hotel"));
	list($reqRes,$REqField)=$req->GetRequest($hotel['results'][0]['idhotel'],array("getFields" => 1));
	$hotel['results'][0]['hotelTitle'] = (isset($reqRes[$hotel['results'][0]['idhotel']]['title'])) ? $reqRes[$hotel['results'][0]['idhotel']]['title']  : $hotel['results'][0]['hotelTitle'];

	echo json_encode(array ("itemid" => $id,"module" => $posted_data['route']['action'],"title"=>$hotel['results'][0]['hotelTitle'], "id" =>$hotel['results'][0]['idhotel'], "image" => $images),true);	
	}
	exit;
}

function video ($posted_data) {



}

function policy ($posted_data) {
	
	foreach((array) $posted_data['data'] as $key=>$value) {
			
		$tmpArr[$value['field']][$key]=$value['value'];
			
	}
	
	if ($posted_data ['route'] ['action'] =="update") {
		global $loaded_modules,$sql;
		$current_module = $loaded_modules["hotel"];
		$Efields = new ExtraFields(array('module'=>$current_module));
		
		$Efields->UpdateExtraFieldsValues(array($tmpArr['saveExtra']),$posted_data ['route'] ['value']);	
		
		$req=new UpdateItem(array("table"=> "extra_field_values"));
		$data = array($tmpArr['saveExtra']);
		if ($posted_data['data']['value']['type'] != "notresponse" ) {
		foreach ($data as $key => $value) {
		
			$tmp['field']="value";
			$tmp['value']=$value['value'];
			$tmp['itemid']=$value['id'];
			$tmp['key']="fieldid";
			$tmp['hotelid']=$posted_data ['route'] ['value'];
			$req->InsertRequest($tmp);
				
		}
		
			if ( USER_CLASS == "A" ) {
				$sql->db_Update("extra_fields","field= '".$posted_data['data']['field']['value']."', var_name='".$posted_data['data']['var_name']['value']."' where fieldid=".$posted_data['data']['id']['value']." and module='hotel'");
				$tmpArr['saveExtra']['field']=$posted_data['data']['field']['value'];
				$tmpArr['saveExtra']['var_name']=$posted_data['data']['var_name']['value'];
				$tmpArr['saveExtra']['enable']=1;
			} else {
				$tmpArr['saveExtra']['enable']=0;
				$tmpArr['saveExtra']['extraEnable']="pending";
			}
		}
		
		echo json_encode($tmpArr['saveExtra'],true);
		exit;
	} else if ($posted_data ['route'] ['action'] =="deleted") {
		global $sql;
		list($itemid,$fieldid) = explode("-",$posted_data ['route'] ['value']);
		$sql->db_Select("hotel","id","uid=".ID." and id=".$itemid);
		if ($sql->mySQLrows > 0) {
		$sql->db_Select("extra_fields","type","fieldid=".$fieldid);
		$r=execute_single($sql);
		
		if ($r['type'] != "text") {
			
			$sql->db_Delete("extra_field_values","itemid=".$itemid." and fieldid=".$fieldid);
			$sql->db_Select("extra_field_values","fieldid","fieldid=".$fieldid);
			$delete=false;
			if ($sql->mySQLrows == 1) {
				
				$sql->db_Delete("extra_fields","fieldid=".$fieldid);
				//echo "extra_fields","fieldid=".$fieldid;
				$delete= true;
			}
			
		//// Delete my Requests ////
			$req = new UpdateItem(array("table"=>"extra_field_values"));
			$req->DeleteRequest(array("id"=> $fieldid ,"field" => "value" ));
		//// Delete my Requests ////
			} else { return; }
			
		}
		echo json_encode(array("id"=>$fieldid,"del" => $delete),true);
		
		exit;
	} else if ($posted_data ['route'] ['action'] =="insert") {
		global $loaded_modules;
		$current_module = $loaded_modules["hotel"];
		$Efields = new ExtraFields(array('module'=>$current_module));
		$ar['type'] ="area";
		$ar['active'] ="N";
		$ar['var_name']= time(true)."-".ID; //$tmpArr['saveExtra']['field'];
		$ar['field']= $tmpArr['saveExtra']['field'];
		$ar['group_list'] = array("83");
		$res=$Efields->AddExtraField($ar,array('debug'=>0));
		$tmpArr['saveExtra']['id']=$res['fieldid'];
		$Efields->UpdateExtraFieldsValues(array($tmpArr['saveExtra']),$posted_data ['route'] ['value']);
		
		$req=new UpdateItem(array("table"=> "extra_field_values"));
		$data = array($tmpArr['saveExtra']);
		
		foreach ($data as $key => $value) {
		
			$tmp['field']="value";
			$tmp['value']=$value['value'];
			$tmp['itemid']=$value['id'];
			$tmp['key']="fieldid";
			$tmp['hotelid']=$posted_data ['route'] ['value'];
			$req->InsertRequest($tmp);
		
		}
		unset($res['action']);
		$res['policyid']=$tmpArr['saveExtra']['policyid'];
		$res['value'] = $tmpArr['saveExtra']['value'];
		echo json_encode($res,true);
		exit;
	}
	
	$settings = array ();
	$settings ['debug'] = 0;
	$settings ['filters'] = array (
			'return' => 'paginated',
			'sort_field' => 'id',
			'sort_direction' => 'DESC',
			'fields' => array (
					'id',
					'title',
					'description',
					'active',
					'stars',
					'pets',
					'enable',
					'hotel.settings'
			)
	);
	$settings['searchfilters'][] = array(
			'item'     => "id",
			'type'     => "eq",
			'val'      => $posted_data ['route'] ['value'],
	);
	
	$tmpObj = new Hotels ();
	$hotel = $tmpObj->getHotel ( $settings );
	
	$req=new UpdateItem(array("table"=> "hotel"));
	list($reqRes,$REqField)=$req->GetRequest($posted_data ['route'] ['value'],array("getFields" => 1));
	$hotel['results'][0]['title'] = (isset($reqRes[$posted_data ['route'] ['value']]['title'])) ? $reqRes[$posted_data ['route'] ['value']]['title']  : $hotel['results'][0]['title'];
	
	
	$extrafield =$tmpObj->GetExtraFields($posted_data ['route'] ['value'],array("title"=>"policy","id"=>83),array(),$posted_data ['route'] ['action']);
	
	$count= $extrafield['countPolicy'];

		foreach ((array) $extrafield['policy'] as $key => $value ) {
			
				$extrafield['policy'][$key]['extraEnable'] =($extrafield['policy'][$key]['enable']) ? ""  : "pending";
		}




	
	echo json_encode ( array ( "page" => $posted_data ['route'] ['action'] ,"extrafield" => $extrafield,"enable" =>$hotel['results'][0]['enable'],"title"=>$hotel['results'][0]['title'],"id"=>$hotel['results'][0]['id']), true );
	exit ();
	
}


function hotspots ($posted_data) {
	
	global $loaded_modules;
	if ($posted_data ['route'] ['action'] == "save" || $posted_data ['route'] ['action'] =="update") {

		foreach($posted_data['data'] as $key=>$value) {
			if ($value['field'] == "dataItem") {
				$tmpArr[$value['field']][$key] =array("value" => $value['value']);
			} else {
				$tmpArr[$value['field']][$key] =$value['value'];
			}
		}
		
		$tmpArr['data']['settings'] = array();
		
		
		$tmpObj = new Hotspots();

	    if ($tmpArr['data']['itemid']) {
			$tmpObj->UpdateHotspot(array ("hotspot" => $tmpArr['dataItem']),$posted_data['route']['value']);
			$tmpObj->UpdateMapHotspots($tmpArr);
			$posted_data ['route'] ['value'] = $tmpArr['dataItem']['hotel_id']['value'];
		} else {	
			unset($tmpArr['dataItem']['id']);
			$tmpArr['dataItem']['enable']['value'] = 0;
			$tmpArr['dataItem']['active']['value'] = 1;
			$tmpArr['data']['itemid']=$tmpObj->NewHotspot(array("hotspot" =>$tmpArr['dataItem']));
			$tmpObj->UpdateMapHotspots($tmpArr);
		}
		
		$posted_data ['route'] ['value']= $tmpArr['dataItem']['hotel_id']['value'];
	} else if ($posted_data ['route'] ['action'] == "delete")  {
		
		global $sql;
		foreach($posted_data['data'] as $key=>$value) {
			$tmpArr[$value['field']][$key] =$value['value'];
		}
		
		$sql->db_Select("hotspots","id","uid=".ID." and id IN (".$posted_data ['route'] ['value'].")");
		if ($sql->mySQLrows == count(explode(",",$posted_data ['route'] ['value'])) ) {
			
			
			$TmpHotspot = new Hotspots();
			$TmpHotspot->DeleteHotspots(explode(",",$posted_data ['route'] ['value']));
			
		}

		$posted_data ['route'] ['value']= $tmpArr['dataItem']['hotel_id'];

	}
	

	$settings ['debug'] = 0;
	$settings['filters']['sort_field'] = "hotspots.id";
	$settings['filters']['fields'] = array(
			"maps_items.geocoderAddress",
			"maps_items.lat",
			"maps_items.lng",
			"maps_items.zoomLevel",
			"maps_items.id",
			"maps_items.title as map_title",
			"maps_items.content as map_content",
			"hotspots.id as hotspotid",
			"hotspots.title as title",
			"hotspots.hotel_id as hotel_id",
			"hotspots.description as description",
			"hotspots.enable as enable"
			);
	$settings['searchfilters'][] = array(
			'item'     => "hotel_id",
			'type'     => "eq",
			'val'      => $posted_data ['route'] ['value'],
	);
	$tmpObj = new Hotspots();
	$tmpArr = $tmpObj->getHotspots($settings);
	
	if (!empty($tmpArr['results'])) {
		
			foreach ($tmpArr['results'] as $key => &$value) {
				$req=new UpdateItem(array("table"=> "hotspots"));
	 			list($reqRes,$REqField)=$req->GetRequest($value['hotspotid'],array("getFields" => 1));
	 			$value = array_merge ($value, (array) $reqRes[$value['hotspotid']]);
				//*********** Get Users Request if Exist *********** //
				///////////////////////////////////////////////////
				$value['ReqFields'] = $REqField[$value['hotspotid']];
			}
		
	} else  {
		$tmpArr['results'][0] = array ( 'hotel_id' => $posted_data ['route'] ['value']);
	}

	$settings = array ();
	$settings ['debug'] = 0;
	$settings ['filters'] = array (
			'sort_field' => 'id',
			'sort_direction' => 'DESC',
			'fields' => array (
					'id',
					'title',
					"maps_items.zoomLevel as zoomLevel",
					"maps_items.lat as lat",
					"maps_items.lng as lng"
			)
	);
	$settings['searchfilters'][] = array(
			'item'     => "id",
			'type'     => "eq",
			'val'      => $posted_data ['route'] ['value'],
	);
	
	$settings['filters']['jointables'] =array (
			array ('name' => "maps_items","idKey" => "maps_items.itemid","idRel" => "hotel.id and maps_items.module='hotel'","joinType" => " LEFT JOIN" )
	);
	$tmpObj = new Hotels ();
	$hotel = $tmpObj->getHotel ( $settings );
	
	$req=new UpdateItem(array("table"=> "hotel"));
	list($reqRes,$REqField)=$req->GetRequest($posted_data ['route'] ['value'],array("getFields" => 1));
	$hotel['results'][0]['title'] = (isset($reqRes[$posted_data ['route'] ['value']]['title'])) ? $reqRes[$posted_data ['route'] ['value']]['title']  : $hotel['results'][0]['title'];
	

	echo json_encode ( array ("lat"=>$hotel['results'][0]['lat'],"lng"=>$hotel['results'][0]['lng'],"zoomLevel"=>$hotel['results'][0]['zoomLevel'],"title"=>$hotel['results'][0]['title'], "id" =>$posted_data ['route'] ['value'], "hotspots" =>$tmpArr['results']), true );
	exit ();

	
}

function Locations($posted_data) {

	if ( $posted_data ['route'] ['action'] == "saveExtra" ) {
		
		$data = array();
		foreach ( $posted_data ['data'] as $key => $value ) {	
				if ($value['field'] == "ExtraItem" ) { 
					list($field,$table) = explode("-",$key);
					if ($field != "id") {
						$data[$key] = array("type"=>$value['type'],"id" => $posted_data ['data']["id-".$key]['value']  ,"value" => $value['value'] );
					} else {
						if (!isset($posted_data ['data'][$table])) { 
							$data[$table] = array("type"=>$value['type'],"id" => $value['value']  ,"value" => " " );
						}
					
					}
				}
		}

		$module = $loaded_modules[$_GET['module']];
		$c = new Items(array('module'=>$module));
		$c->UpdateExtraFieldsValues($data,$posted_data ['route'] ['value'] );
		
		$req=new UpdateItem(array("table"=> "extra_field_values"));

		foreach ($data as $key => $value) {
			if ($value['type'] != "notresponse") {
				$tmpArr['field']="value";
				$tmpArr['value']=$value['value'];
				$tmpArr['itemid']=$value['id'];
				$tmpArr['key']="fieldid";
				$tmpArr['hotelid']=$posted_data ['route'] ['value'];
				$req->InsertRequest($tmpArr);
			}
			
		}
		
	} else if ( $posted_data ['route'] ['action'] == "FirstInsert" ) {
		
		$tmpObj = new Location ( array (
				"module" => "maps_items"
		) );
		foreach ( $posted_data ['data'] as $key => $value ) {
		    if ($value ['field'] == "saveMap") {
					$ar[$key]=$value['value'];
			}
		}

		$ar['module'] = "hotel";
		$ar['MapTypeId'] ="ROADMAP";
		$ar['itemid'] = $posted_data ['route']['value'];

		$id=$tmpObj->addPoint($ar,array("action" => "insert","id" => $posted_data ['data']['id'] ));
	

	} else if ( $posted_data ['route'] ['action'] == "update" ) {
		
		$tmpObj = new Location ( array (
				"module" => "maps_items"
		) );
		
		foreach ( $posted_data ['data'] as $key => $value ) {
			if ($value ['field'] == "saveMap") {
				$ar[$key]=$value['value'];
			}
		}
		
		
		$id=$tmpObj->addPoint($ar,array("action" => "update","id" => "module='hotel' and itemid","value" => $posted_data ['route']['value'] ));
		
		
	} else {

		
		$settings = array ();
		$settings ['debug'] = 0;
		$settings ['filters'] = array (
				'perPage' => "1",
				"execute" => "searchBySQL",
				'sort_direction' => 'DESC',
				'fields' => array (
						'id',
						'title',
						'description',
						'active',
						'stars',
						'pets',
						'enable',
						'hotel.settings',
						"IFNULL(zoomLevel,1) as zoomLevel ",
						"IFNULL(lat, 0) as  lat",
						"IFNULL(lng,0) as lng",
						"geocoderAddress",
						"IFNULL(maps_items.id,0) as mapid"
				)
		);
		$settings['searchfilters'][] = array(
				'item'     => "hotel.id",
				'type'     => "eq",
				'val'      => $posted_data ['route'] ['value'],
		);
		
		
		$settings['filters']['jointables'] =array (
		array ('name' => "maps_items","idKey" => "maps_items.itemid","idRel" => "hotel.id and maps_items.module='hotel'","joinType" => " LEFT JOIN" )
		);
		$tmpObj = new Hotels ();
		$hotel = $tmpObj->getHotel ( $settings );
		
		

		
		$req=new UpdateItem(array("table"=> "hotel"));
		list($reqRes,$REqField)=$req->GetRequest($posted_data ['route'] ['value'],array("getFields" => 1));
		$hotel['results'][0]['title'] = (isset($reqRes[$posted_data ['route'] ['value']]['title'])) ? $reqRes[$posted_data ['route'] ['value']]['title']  : $hotel['results'][0]['title'];
		
		
		


		$extrafield =$tmpObj->GetExtraFields($posted_data ['route'] ['value'],array("title" =>"locations","id"=>81));
		

		foreach ($extrafield as $k => $v ) {
			foreach ((array)$v as $key => $value ) {
				$extrafield[$k][$key]['value'] = (isset($extrafield[$k][$key]['value'])) ? $value['value'] : " ";
				$extrafield[$k][$key]['extraEnable'] =($extrafield[$k][$key]['enable']) ? "activate"  : "pending";		
			}
		}

		
		
		$settings = array ();

		$tmpObj = new Location( array (
				"module" => ""
		) );
		$settings['searchfilters']=array();
		$settings['searchfilters'][] = array(
				'item'     => "itemid",
				'type'     => "eq",
				'val'      => $posted_data ['route'] ['value'],
		);
		
		$settings ['debug'] = 0;
		$settings['filters']['fields'] = array("countryid,regionid,itemid as id,cityid");
		$settings ['filters']['table'] ='geo_relations';
		$settings ['filters']['sort_field']='id';
		$settings ['filters']['sort_direction']='DESC';
		$settings ['overwriteJoin'] = array();
		$currentHotel=$tmpObj->getHotelLocations($settings);
		if (empty($currentHotel['results'])) {
			$currentHotel['results'][0] = array("countryid"=>"","regionid"=>"","id"=> "","cityid" => "");
		}
		
		echo json_encode ( array ("active" => array("region"=>"","country"=>"","city"=>"","hotel" => "active"),"MapsEnable"=>$currentHotel['results'][0] ,"itemid"=>$hotel['results'][0]['mapid'],"location"=> $hotel['results'][0], "extrafield" => $extrafield,"enable" =>$hotel['results'][0]['enable'],"title"=>$hotel['results'][0]['title'],"id"=>$hotel['results'][0]['id']), true );
		exit ();
		
	}
	
}

function preview($posted_data) {
	//include("../../../preview.php");
	
	$settings = array();
	$settings ['filters'] = array (
			'return' => 'paginated',
			'sort_field' => 'id',
			'sort_direction' => 'DESC',
			'fields' => array ( "id","title" ) 
			);
	$settings['searchfilters'][] = array(
			'item'     => "id",
			'type'     => "eq",
			'val'      => $posted_data ['route'] ['value'],
	);
	
	$tmpObj = new Hotels ();
	$hotel = $tmpObj->getHotel ( $settings );
	echo json_encode ( array ("title"=>$hotel['results'][0]['title'], "id" =>$posted_data ['route'] ['value']), true );
	exit ();
	
}

?>