<?php

include "../../initAjax.php";


$file = (isset($_POST['route']['call'])) ? $_POST['route']['call'] : $_POST['route']['page'];
if( file_exists( ABSPATH."/ajax/hooks/hotelier/".$file.".php") ) {
	include $file.".php";
} else {
	include_once $_GET['file'];
	json_encode(array("false"),true);
	exit;
}