<?php

/*
$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$start = $time;
*/

$ar['data'] = explode(",",$_POST['data']['selected']['value']);
$ar['mode'] = $_POST['route']['action'];

$tmpObj = new Rooms();

if ($ar['data'][0]) {
	$tmpObj->DashboardRoom($ar);
}

$settings = array();
$settings['debug'] = 0;
$settings['filters'] = array(
		'sort_field' 	=> 'hotel.id',
		'sort_direction'=> 'DESC',
		'execute'=>'searchBySQL',
		'fields' => array(
				"rooms_hotel.room_id as room_id",
				"rooms_hotel.availability as roomCount",
				"rooms_hotel.room_type as RoomType",
				"rooms_hotel.price as price",
				"rooms_hotel.active as active",
				"hotel.title as title",
				"hotel.id as idhotel"
		)
);

$settings['searchfilters'][] = array(
		'item'     => "hotel.id",
		'type'     => "eq",
		'val'      => $_POST['route']['value'],
);

$settings['overwriteJoin'] = array(
		array(
				'name'=>"rooms_hotel",
				"idKey"=>"rooms_hotel.hotel_id",
				"idRel"=>"hotel.id",
				"joinType"=>"LEFT JOIN"
		)
);


$tmpObj = new Hotels();
$tmp=$tmpObj->getHotel($settings);

if (empty($tmp['results'])) { echo json_encode(array ("logout"=>"/en/hotelier/index.html#dashboard"),true); exit; }

$req=new UpdateItem(array("table"=> "hotel"));
list($reqRes,$REqField)=$req->GetRequest($_POST['route']['value'],array("getFields" => 1));

$tmp['results'][0]['title'] = (isset($reqRes[$_POST['route']['value']]['title'])) ? $reqRes[$_POST['route']['value']]['title']  : $tmp['results'][0]['title'];



foreach($tmp['results'] as $key =>$value) {
	$tmpArr[$value['room_id']]=$value;
}

$current_module = $loaded_modules['rooms'];
$Rooms = new Items(array('module'=>$loaded_modules['rooms'],'debug'=>0));
$item_settings = array('fields'=>'id,title,description,date_added,enable','thumb'=>1,'CatNav'=>1,'debug'=>0,'cat_details'=>1,'main'=>1,'images'=>1,'grouped'=> 1, 'getbyField' => "title",'efields'=>1,'parse'=>1);
$posted_data['searchFields']["hotelKey	"]= array('type'=>"EQ",'val'=>$_POST['route']['value']);
//$posted_data['settings']=$item_settings;
$current_module =array_merge($current_module,$item_settings);
$posted_data['results_per_page'] = 10;
$posted_data['sort'] ="id";
$posted_data['sort_direction'] = "DESC";
unset($current_module['active']);

$items = $Rooms->ItemSearch($posted_data,$current_module,$page,0);

if (!empty($items['results'])) {
	foreach( $items['results'] as $key => &$value ) {
		$value['extra']=($tmpArr[$value['id']]) ? $tmpArr[$value['id']] : "";
		$value['active'] = $tmpArr[$value['id']]['active'];
		$req=new UpdateItem(array("table"=> "rooms"));
		list($reqRes,$REqField)=$req->GetRequest($value['id'],array("getFields" => 1));
		$value = array_merge ($value,(array) $reqRes[$value['id']]);
	}
} else { $items['results']=array(); }

/*
$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$finish = $time;
$totaltime = round(($finish - $start), 4);
*/
echo json_encode( array ("results" => $items['results'],"id" =>$tmp['results'][0]['idhotel'], "title" =>  $tmp['results'][0]['title'] ),true);
exit;