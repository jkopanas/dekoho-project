<?php
if (! call_user_func ( $_POST ['route'] ['page'], $_POST )) {
	exit ();
}

function supportNew($posted_data) {

	
		if ($posted_data['route']['action']=="save") {
			
			foreach($posted_data['data'] as $key=>$value) {
				$tmpArr[$value['field']][$key] =$value['value'];
			}
			$tmpObj = new Support();
			if (USER_CLASS == "A") {
				$tmpArr['openTicket']['user_id'] =ADMINID;
				$tmpArr['openTicket']['sender_id'] =ID;
			}
			return json_encode(array($tmpObj->NewTicket($tmpArr['openTicket'])),true);
			
		} else if ($posted_data['route']['action']=="view" ) {
			
			$tmpObj = new Support();
			$settings=array();
			
			$settings['searchfilters'][] = array(
					'item'     => "conversationid",
					'type'     => "eq",
					'val'      => $posted_data['route']['value'],
			);
			
			$settings['overwriteJoin']=array();
			$settings['debug']=0;
			$settings['filters']['groupBy']="users_tickets.id";
			$settings['filters']['sort_field'] ="users_tickets.date_add";
			$settings['filters']['fields'] = array("*");
			$res=$tmpObj->getSupportDetails($settings);
			
			foreach($res['results'] as $key => $value) {
			
				if ($value['uid'] != ID && $value['senderid'] !=ID ) {
					echo json_encode(array("results" => array()),true);
					exit;
				}
				$res['results'][$key]['date_add']= date('Y-m-d H:i:s',$value['date_add']);
				$userid[]=$value['user_id'];
			}
			
			$tmpObj->ConversationMsgStatus(array("status" => 2,"id"=>$posted_data['route']['value']));
			
			$tmpObj = new Hotelier();
			foreach ($userid as $key => $value) {
				$settings=array();
				$settings['search'][] = array( "users.id" => $value );
				$tmpArr[$value] = $tmpObj->GetHotelier($settings);
			}
			
			echo json_encode(array("reply" => 1, "users"=>$tmpArr, "results" => $res['results']),true);
			exit;
			
		}

		echo json_encode(array("results" => array()),true);
		exit;

}


?>