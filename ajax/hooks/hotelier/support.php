<?php

if (! call_user_func ( $_POST ['route'] ['page'], $_POST )) {
	exit ();
}

function support($posted_data) {

	if ($posted_data['route']['action']=="deleted") {
		$tmpObj = new Support();
		$support=$tmpObj->DeleteSupport($posted_data['data']['selected']['value']);
		echo json_encode($support,true);
		exit;
	}
	
$tmpObj = new Support();
$settings=array();

$settings['searchfilters'][] = array(
		'item'     => "uid",
		'type'     => "eq",
		'val'      => ID
);
$settings['debug']=0;
$settings['filters']['return']="total";
$settings['filters']['groupBy']="users_tickets.id";
$settings['filters']['sort_field'] ="users_tickets.id";
$settings['filters']['jointables'][]= array( 
		'name'=>"(SELECT MAX(t1.date_add) as time, t1.id, t1.title, t1.description FROM users_tickets t1 GROUP BY conversationid ) recent",	
		"idKey"=>"recent.time",
		"idRel"=>"users_tickets.date_add",
		"joinType"=>"INNER JOIN"
	);

$settings['filters']['page'] = ($posted_data['route']['value']) ? $posted_data['route']['value'] : 1;
$settings['filters']['fields'] = array("users_tickets.id");
$total=$tmpObj->getSupport($settings);

$settings['debug']=0;
$settings['filters']['return']="paginated";
$settings['filters']['fields'] = array("users_tickets.*");
$res=$tmpObj->getSupport($settings);

$user= (USER_CLASS == "A")  ? ADMINID : ID;

if (is_array($res['results'][0])) {	
	foreach($res['results'] as $key => $value) {
		$res['results'][$key]['date_add'] = date('Y-m-d H:i:s',$value['date_add']);
		$res['results'][$key]['currentuser'] = $user;
		$res['results'][$key]['userclass'] = USER_CLASS;
	}
} else {
	$res['results']=array();
}


echo json_encode(array("results" => $res['results'], "pages"=> $settings['filters']['page'], "totals"=> $total['results'][0]['total']),true);
exit;

}



?>