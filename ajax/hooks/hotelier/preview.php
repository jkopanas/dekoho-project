<?php

	
$settings = array ();
$settings ['debug'] = 0;
$settings ['filters'] = array (
		'return' => 'paginated',
		'sort_field' => 'id',
		'sort_direction' => 'DESC',
		'fields' => array (
				'id',
				'title',
				'description',
				'active',
				'stars',
				'pets',
				'enable',
				'hotel.settings'
		)
);
$settings['searchfilters'][] = array(
		'item'     => "id",
		'type'     => "eq",
		'val'      => $_POST ['route'] ['value'],
);

$tmpObj = new Hotels ();
$hotel = $tmpObj->getHotel ( $settings );
echo json_encode($hotel['results'],true);
exit;