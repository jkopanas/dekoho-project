<?php
if (! call_user_func ( $_POST ['route'] ['page'], $_POST )) {
	exit ();
}

function notificationsDetails($posted_data) {

	$tmpObj = new Notifications();
	$settings=array();
	
	$settings['searchfilters'][] = array(
			'item'     => "id",
			'type'     => "eq",
			'val'      => $posted_data['route']['value'],
	);
	
	$settings['searchfilters'][] = array(
			'item'     => "uid",
			'type'     => "eq",
			'val'      => ID,
	);
	$settings['overwriteJoin']=array();
	$settings['debug']=0;
	$settings['filters']['groupBy']="users_notification.id";
	$settings['filters']['sort_field'] ="users_notification.id";

	$settings['filters']['fields'] = array("*");
	$res=$tmpObj->getNotificationsDetails($settings);

	echo json_encode(array("results" => $res['results'][0]),true);
	exit;

}


?>