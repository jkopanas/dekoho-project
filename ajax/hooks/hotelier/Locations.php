<?php

if ( USER_CLASS == "U") {
	echo json_encode( array( "template" => "EmptyRender","data" => array( "message" => "You are not Allowed to view this Page") ) ,true);
	exit ();
}
if (! call_user_func ( $_POST ['route'] ['page'], $_POST )) {
	exit ();
}

function country ($posted_data) {
	
	
	if ( $posted_data['route']['action'] == "delete" ) {
		
		$tmpObj = new Location ( array (
				"module" => "geo_country"
		) );
		
		$tmpObj->delete($posted_data ['route'] ['value']);
		echo json_encode(array(true),true);
		exit;
	} else if ($posted_data ['route'] ['action'] == "updateTitle") {
		
		
		$tmpObj = new Location ( array (
				"module" => "geo_country"
		) );

		$id=$tmpObj->addPoint(array("title" => $posted_data['data']['UpdateTitle']['value']),array("action" => "update","id" => "id","value" => $posted_data ['data']['id']['value'] ));
		
	} else if ( $posted_data ['route'] ['action'] == "update"  || $posted_data ['route'] ['action'] == "FirstInsert" ) {

		$posted_data ['route'] ['action'] = ( $posted_data ['route'] ['action'] == "FirstInsert" ) ? "insert"  : $posted_data ['route'] ['action'];
		$tmpObj = new Location ( array (
				"module" => "geo_country"
		) );

		$tmpObj->addPointRelation(array("cityid"=> 0 ,"regionid"=> 0 ,"countryid" => $posted_data ['data']['id']['value'] ),array("id"=> $posted_data ['route']['value'], "action" => $posted_data ['route'] ['action']));
		
		
	} else if ($posted_data ['route'] ['action'] == "insert") {
		

		foreach ( $posted_data ['data'] as $key => $value ) {
			if ($value ['field'] == "saveMap") {
				$ar[$key]=$value['value'];
			}
		}
		
		$tmpObj = new Location ( array (
				"module" => "geo_country"
		) );
		
		$ar['MapTypeId'] = "roadmap";
		$id=$tmpObj->addPoint($ar,array("action" => "insert","id" => $posted_data ['data']['id'] ));

		$tmpObj->addPointRelation(array("cityid"=> 0 ,"regionid"=> 0 ,"countryid" => $id ),array("id" => $posted_data ['route']['value'], "action" => $posted_data ['route'] ['action']));
		
	
	}
	
	$settings = array ();
		$settings ['debug'] = 0;
		$settings ['filters'] = array (
				'sort_field' => 'id',
				'sort_direction' => 'DESC',
				'fields' => array (
						'id',
						'title',
						'description',
						'active',
						'enable',
						'hotel.settings',
				)
		);
		$settings['searchfilters'][] = array(
				'item'     => "id",
				'type'     => "eq",
				'val'      => $posted_data ['route'] ['value'],
		);
		
		$tmpObj = new Hotels ();
		$hotel = $tmpObj->getHotel ( $settings );

		

		
		$settings = array ();
		$settings ['debug'] = 0;
		$settings ['filters'] = array (
				'sort_field' => 'id',
				'sort_direction' => 'DESC',
				'table' => "geo_country"
		);
		
		$tmpObj = new Location( array (
				"module" => ""
		) );
		
		$settings['searchfilters'][] = array(
				'item'     => "1",
				'type'     => "eq",
				'val'      => 1,
		);
		
		$tmpArr = $tmpObj->GetLocation($settings);
		
		$settings['searchfilters'][] = array(
				'item'     => "itemid",
				'type'     => "eq",
				'val'      => $posted_data ['route'] ['value'],
		);

		$settings ['debug'] = 0;
		$settings['filters']['fields'] = array("countryid as id,regionid,cityid,countryid");
		$settings ['filters']['table'] ='geo_relations';
		$currentHotel=$tmpObj->getHotelLocations($settings);
		

		if (empty($currentHotel['results'])) {
			$currentHotel['results'][0]=array("countryid" => 0,"cityid"=>0,"regionid"=>0);
		}
		
		$settings['searchfilters'] =array(array(
				'item'     => "1",
				'type'     => "eq",
				'val'      => 1,
		));
		$settings['filters']['fields'] = array("count(countryid) as numItems,countryid");
		$settings['filters']['groupBy'] = "geo_relations.countryid";
		$numItems=$tmpObj->getHotelLocations($settings);
		$numItemsFormatted =array();
		foreach ( $numItems['results'] as $key => $value ) {
			$numItemsFormatted[$value['countryid']]=$value['numItems'];
		}

		echo json_encode ( array ("active" => array("region"=>"","country"=>"active","city"=>""),"MapsEnable"=>$currentHotel['results'][0],"countItem" => $numItemsFormatted ,"itemid"=>$currentHotel['results'][0]['id'],"location"=> $tmpArr['results'],"title"=>$hotel['results'][0]['title'],"id"=>$hotel['results'][0]['id']), true );
		exit ();
}

function region ($posted_data) {


	if ($posted_data['route']['action'] == "delete") {
	
		$tmpObj = new Location ( array (
				"module" => "geo_region"
		) );
	
		$tmpObj->delete($posted_data ['route'] ['value']);
		echo json_encode(array(true),true);
		exit;
	
	} else if ($posted_data ['route'] ['action'] == "updateTitle") {
	
	
		$tmpObj = new Location ( array (
				"module" => "geo_region"
		) );
	
		$id=$tmpObj->addPoint(array("title" => $posted_data['data']['UpdateTitle']['value']),array("action" => "update","id" => "id","value" => $posted_data ['data']['id']['value'] ));
	
	} else if ($posted_data ['route'] ['action'] == "update") {
	
		$tmpObj = new Location ( array (
				"module" => "geo_region"
		) );
	
		$tmpObj->addPointRelation(array("regionid" => $posted_data ['data']['id']['value'],"cityid" => 0 ),array("id"=> $posted_data ['route']['value'], "action" => $posted_data ['route'] ['action']));
	
	
	} else if ($posted_data ['route'] ['action'] == "insert") {
	
	
		foreach ( $posted_data ['data'] as $key => $value ) {
			if ($value ['field'] == "saveMap") {
				$ar[$key]=$value['value'];
			}
		}
	
		$tmpObj = new Location ( array (
				"module" => "geo_region"
		) );
	
		$id=$tmpObj->addPoint($ar,array("action" => "insert","id" => $posted_data ['data']['id'] ));
	
		$tmpObj->addPointRelation(array("regionid" => $id ),array("id" => $posted_data ['route']['value'], "action" => "update"));
	
	
	}
	
	$settings = array ();
	$settings ['debug'] = 0;
	$settings ['filters'] = array (
			'sort_field' => 'id',
			'sort_direction' => 'DESC',
			'fields' => array (
					'id',
					'title',
					'description',
					'active',
					'enable',
					'hotel.settings',
			)
	);
	$settings['searchfilters'][] = array(
			'item'     => "id",
			'type'     => "eq",
			'val'      => $posted_data ['route'] ['value'],
	);
	
	
	$tmpObj = new Hotels ();
	$hotel = $tmpObj->getHotel ( $settings );

	
	$tmpObj = new Location( array (
			"module" => ""
	) );
	$settings['searchfilters']=array();
	$settings['searchfilters'][] = array(
			'item'     => "itemid",
			'type'     => "eq",
			'val'      => $posted_data ['route'] ['value'],
	);
	
	$settings ['debug'] = 0;
	$settings['filters']['fields'] = array("countryid,regionid as id,cityid,regionid");
	$settings ['filters']['table'] ='geo_relations';
	$settings ['filters']['sort_field']='id';
	$settings ['filters']['sort_direction']='DESC';
	$settings ['overwriteJoin'] = array();
	$currentHotel=$tmpObj->getHotelLocations($settings);
	
	$settings = array ();
	$settings ['debug'] = 0;
	$settings ['filters'] = array (
			'sort_field' => 'id',
			'sort_direction' => 'DESC',
			'table' => "geo_region"
	);
	$settings['searchfilters'][] = array(
			'item'     => "geo_region.id_country",
			'type'     => "eq",
			'val'      => $currentHotel['results'][0]['countryid'],
	);
	$tmpArr = $tmpObj->GetLocation($settings);
	
	if ($tmpArr['results'][0] == 0 ) {
		$tmpArr['results'] = array();
	}
	
	$settings['searchfilters'] = array(array(
			'item'     => "1",
			'type'     => "eq",
			'val'      => 1,
	));
	$settings['filters']['fields'] = array("count(regionid) as numItems,regionid");
	$settings['filters']['groupBy'] = "geo_relations.regionid";
	$numItems=$tmpObj->getHotelLocations($settings);
	foreach ( $numItems['results'] as $key => $value ) {
		$numItemsFormatted[$value['regionid']]=$value['numItems'];
	}
	
	echo json_encode ( array ("active" =>array("region"=>"active","country"=>"","city"=>""),"MapsEnable"=>$currentHotel['results'][0],"countItem" => $numItemsFormatted ,"itemid"=>$currentHotel['results'][0]['id'],"location"=> $tmpArr['results'],"title"=>$hotel['results'][0]['title'],"id"=>$hotel['results'][0]['id']), true );
	exit ();
}


function city ($posted_data) {
	

	if ($posted_data['route']['action'] == "delete") {
	
		$tmpObj = new Location ( array (
				"module" => "geo_cityr"
		) );
	
		$tmpObj->delete($posted_data ['route'] ['value']);
		echo json_encode(array(true),true);
		exit;
	
	} else if ($posted_data ['route'] ['action'] == "updateTitle") {
	
	
		$tmpObj = new Location ( array (
				"module" => "geo_cityr"
		) );
	
		$id=$tmpObj->addPoint(array("title" => $posted_data['data']['UpdateTitle']['value']),array("action" => "update","id" => "id","value" => $posted_data ['data']['id']['value'] ));
	
	} else if ($posted_data ['route'] ['action'] == "update") {
	

		$tmpObj = new Location ( array (
				"module" => "geo_cityr"
		) );
	
		$tmpObj->addPointRelation(array("cityid" => $posted_data ['data']['id']['value'] ),array("id"=> $posted_data ['route']['value'], "action" => $posted_data ['route'] ['action']));
	
	
	} else if ($posted_data ['route'] ['action'] == "insert") {
	
	
		foreach ( $posted_data ['data'] as $key => $value ) {
			if ($value ['field'] == "saveMap") {
				$ar[$key]=$value['value'];
			}
		}
	
		$tmpObj = new Location ( array (
				"module" => "geo_cityr"
		) );

		$id=$tmpObj->addPoint($ar,array("action" => "insert","id" => $posted_data ['data']['id'] ));
	
		$tmpObj->addPointRelation(array("cityid" => $id ),array("id" => $posted_data ['route']['value'], "action" => "update"));
	
	
	}
	
	$settings = array ();
	$settings ['debug'] = 0;
	$settings ['filters'] = array (
			'sort_field' => 'id',
			'sort_direction' => 'DESC',
			'fields' => array (
					'id',
					'title',
					'description',
					'active',
					'enable',
					'hotel.settings',
			)
	);
	$settings['searchfilters'][] = array(
			'item'     => "id",
			'type'     => "eq",
			'val'      => $posted_data ['route'] ['value'],
	);
	
	
	$tmpObj = new Hotels ();
	$hotel = $tmpObj->getHotel ( $settings );
	
	
	$tmpObj = new Location( array (
			"module" => ""
	) );
	$settings['searchfilters']=array();
	$settings['searchfilters'][] = array(
			'item'     => "itemid",
			'type'     => "eq",
			'val'      => $posted_data ['route'] ['value'],
	);
	
	$settings ['debug'] = 0;
	$settings['filters']['fields'] = array("countryid,regionid,cityid as id,cityid");
	$settings ['filters']['table'] ='geo_relations';
	$settings ['filters']['sort_field']='id';
	$settings ['filters']['sort_direction']='DESC';
	$settings ['overwriteJoin'] = array();
	$currentHotel=$tmpObj->getHotelLocations($settings);
	
	$settings = array ();
	$settings ['debug'] = 0;
	$settings ['filters'] = array (
			'sort_field' => 'id',
			'sort_direction' => 'DESC',
			'table' => "geo_cityr"
	);
	$settings['searchfilters'][] = array(
			'item'     => "geo_cityr.id_region",
			'type'     => "eq",
			'val'      => $currentHotel['results'][0]['regionid'],
	);
	$tmpArr = $tmpObj->GetLocation($settings);
	
	if ($tmpArr['results'][0] == 0 ) {
		$tmpArr['results'] = array();
	}
	
	$settings['searchfilters'] = array(array(
			'item'     => "1",
			'type'     => "eq",
			'val'      => 1,
	));
	$settings['filters']['fields'] = array("count(cityid) as numItems,cityid");
	$settings['filters']['groupBy'] = "geo_relations.cityid";
	$numItems=$tmpObj->getHotelLocations($settings);
	foreach ( $numItems['results'] as $key => $value ) {
		$numItemsFormatted[$value['cityid']]=$value['numItems'];
	}
	
	echo json_encode ( array ("active" => array("region"=>"","country"=>"","city"=>"active"),"MapsEnable"=>$currentHotel['results'][0],"countItem" => $numItemsFormatted ,"itemid"=>$currentHotel['results'][0]['id'],"location"=> $tmpArr['results'],"title"=>$hotel['results'][0]['title'],"id"=>$hotel['results'][0]['id']), true );
	exit ();
	

}



exit;