<?php
global $sql;


$sql->q("select maps_items.zoomLevel as hotelid_zoomLevel,maps_items.lat as hotelid_lat,maps_items.lng as hotelid_lng,maps_items.title as hotelid_title,maps_items.geocoderAddress as hotelid_geocoderAddress,hotel.* from hotel inner join maps_items on hotel.id=maps_items.itemid where hotel.id=".$_GET['id']);
$hotel=execute_multi($sql);

$settings = array ();
$settings ['debug'] = 0;
$settings ['filters'] = array (
		'sort_field' => 'id',
		'perPage' => "1",
		'sort_direction' => 'DESC',
		'fields' => array (
				"geo_relations.itemid as hotelid_id",
				"geo_country.zoomLevel as countryid_zoomLevel",
				"geo_country.id as countryid_id",
				"geo_country.lat as countryid_lat",
				"geo_country.lng as countryid_lng",
				"geo_country.title as countryid_title",
				"geo_country.geocoderAddress as countryid_geocoderAddress",
				"geo_region.id as regionid_id",
				"geo_region.zoomLevel as regionid_zoomLevel",
				"geo_region.lat as regionid_lat",
				"geo_region.lng as regionid_lng",
				"geo_region.title as regionid_title",
				"geo_region.geocoderAddress as regionid_geocoderAddress",
				"geo_cityr.id as cityid_id",
				"geo_cityr.zoomLevel as cityid_zoomLevel",
				"geo_cityr.lat as cityid_lat",
				"geo_cityr.lng as cityid_lng",
				"geo_cityr.title as cityid_title",
				"geo_cityr.geocoderAddress as cityid_geocoderAddress"
		)
);

$settings['searchfilters'][] = array(
		'item'     => "geo_relations.itemid",
		'type'     => "eq",
		'val'      => $_GET['id'],
);

$tmpObj = new Location( array (
		"module" => ""
) );

$settings['filters']['jointables'] =array (
	array ('name' => "maps_items","idKey" => "maps_items.itemid","idRel" => "geo_relations.itemid","joinType" => " LEFT JOIN" ),
);

$tmpArr = $tmpObj->getHotelLocations($settings);

$tmpArr['results'][0]['hotelid_id']=$hotel[0]['id'];
$tmpArr['results'][0]['hotelid_zoomLevel']=$hotel[0]['hotelid_zoomLevel'];
$tmpArr['results'][0]['hotelid_lat']=$hotel[0]['hotelid_lat'];
$tmpArr['results'][0]['hotelid_lng']=$hotel[0]['hotelid_lng'];
$tmpArr['results'][0]['hotelid_geocoderAddress']=$hotel[0]['hotelid_geocoderAddress'];
$tmpArr['results'][0]['hotelid_title']=$hotel[0]['hotelid_title'];


//print_ar($tmpArr);

$smarty->assign('mapItem',json_encode($tmpArr));
$smarty->assign('item',$tmpArr['results'][0]);

$sql->q("select geo_country.id,geocoderAddress,title,lat,lng,zoomLevel,geo_relations.id as tid,count(geo_country.id) as total from geo_country left join geo_relations on geo_relations.countryid=geo_country.id group by geo_country.id");
$smarty->assign("countries",execute_multi($sql));

$sql->db_Select("geo_region left join geo_relations on geo_relations.regionid=geo_region.id  group by geo_region.id","geo_region.id,geocoderAddress,title,lat,lng,zoomLevel,geo_relations.id as tid,count(geo_region.id) as total");
$smarty->assign("regions",execute_multi($sql));
$sql->db_Select("geo_cityr left join geo_relations on geo_relations.cityid=geo_cityr.id  group by geo_cityr.id","geo_cityr.id,geocoderAddress,title,lat,lng,zoomLevel,geo_relations.id as tid,count(geo_cityr.id) as total");

$smarty->assign("cityr",execute_multi($sql));

$smarty->assign("id",$_GET['id']);

$smarty->assign("current_module",$current_module);
$template = $smarty->fetch($box['template']);
$output = array('template'=>$template,'box'=>$box,'a'=>$_GET);
if (!$buffer) {
	echo json_encode($output);
}






?>