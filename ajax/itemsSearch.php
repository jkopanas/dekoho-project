<?php
include_once('init.php');
if (!is_array($_GET['module']) && !is_array($_POST['module']) ) {
	$ArrModule[]= ($_GET['module']) ? $_GET['module'] : $_POST['module'];
} else {
	$ArrModule = ($_GET['module']) ? $_GET['module'] : $_POST['module'];
}
$numItems = count($ArrModule);
$i = 0;
foreach ($ArrModule as $key => $module) {
$current_module = $loaded_modules[$module];

$c = new Items(array('module'=>$current_module));
$posted_data = $_POST;
if ($_GET['action'] == "pagesFilter") {
	//$posted_data = array('categoryid'=>$cat,'availability'=>1,'thumb'=>1,'main'=>1);
	unset($current_module['active']);
	//$posted_data = array('availability'=>1);
	$posted_data['page'] = ($posted_data['page']) ? $posted_data['page'] : 1;
	$posted_data['cat_details'] = 1;
	$posted_data['sort'] = ($posted_data['sort']) ? $posted_data['sort'] : $current_module['settings']['default_sort'];
	$posted_data['sort_direction'] = ($posted_data['sort_direction']) ? $posted_data['sort_direction'] : $current_module['settings']['default_sort_direction'];
	$posted_data['results_per_page'] = ($posted_data['pageSize']) ? $posted_data['pageSize'] : $current_module['settings']['items_per_page'];	
	if ($_POST['sort']) {
		if (!strstr($_POST['sort'][0]['field'],'.')){
			$posted_data['sort'] = $_POST['sort'][0]['field'];
			$posted_data['sort_direction'] = $_POST['sort'][0]['dir'];
		}
		else {
			$posted_data['sort'] = '';
			$posted_data['sort_direction'] = '';
		}
	}

	
	if (is_array($_POST['filter']['filters'])) { //KENDO GRID FILTERS
		foreach ($_POST['filter']['filters'] as $key => $value) {
				switch ($value['operator']) {
					case 'eq': $op = 'EQ';
					break;
					case 'contains': $op = 'LIKE';
					break;
					case 'neq': $op = 'NE';
					break;
				}
				if ($value['field'] !='q') {//specific key
					$posted_data['searchFields'][$value['field']]= array('type'=>$op,'val'=>$value['value']);
				}
				else {//generic
					if (is_numeric($value['value'])) {
						$posted_data['searchFields']['id']= array('type'=>$op,'val'=>$value['value']);
					}
					else {
						$posted_data['searchFields']['title']= array('type'=>$op,'val'=>$value['value']);
					}
				}	
		}
	}

	
	if ($_GET['mode'] == 'FeaturedContent') {
		$featured_items = $c->FeaturedContent($_GET['category_id'],array('fields'=>'itemid','type'=>'itm'));

		if ($featured_items) {
			foreach ($featured_items as $v)
			{
			$tmp[] = $v['itemid'];
			}
			$posted_data['exclude'] = implode(',',$tmp);
		}//END ROWS
	}

	if (!empty($posted_data['data'])) {
		foreach ($posted_data['data'] as $key => $value  ) {
			$posted_data['searchFields'][$value['field']]= array('type'=>$value['searchType'],'val'=>$value['value']);
		}
	}
   
	$posted_data['SearchLogic'] = ($_POST['filter']['logic'])? $_POST['filter']['logic'] : "and";
	$posted_data['page'] = ($posted_data['page'])? $posted_data['page'] : 1;
	

	$res[$module] = $c->ItemSearch($posted_data,$current_module,$page,0);
	if ($res[$module]['results']) {
		foreach ($res[$module]['results'] as $key => $value ) {
			$res[$module]['results'][$key]['module']=$module;
		}
		$count = $smarty->getTemplateVars('list');
		$res[$module]['count'] = $count['total'];
	}
	else {
		$res[$module]['results'] = array();
	}

	if(++$i === $numItems) {
		if ($numItems > 1 ) {
			if ($_GET['smarty']) {
				$smarty->assign("items",json_encode($res)); 
			} else {
			echo json_encode($res);
			exit;
			}
		} else {
		echo json_encode($res[$module]);
		exit;
		}
	}
   
	
	
} else {
//$posted_data = array('categoryid'=>$cat,'availability'=>1,'thumb'=>1,'main'=>1);
$posted_data['cat_details'] = 1;
$posted_data['sort'] = ($posted_data['sort']) ? $posted_data['sort'] : $current_module['settings']['default_sort'];
$posted_data['sort_direction'] = ($posted_data['sort_direction']) ? $posted_data['sort_direction'] : $current_module['settings']['default_sort_direction'];
$posted_data['results_per_page'] = ($posted_data['results_per_page']) ? $posted_data['results_per_page'] : $current_module['settings']['items_per_page'];	

if ($posted_data['field'])
{
	$searchType = ($posted_data['searchType']) ? $posted_data['searchType'] : 'LIKE';
	$posted_data['searchFields'] = array($posted_data['field'] => array('type'=>$searchType,'val'=>$posted_data['val']));
}



$posted_data['page'] = ($posted_data['page'])? $posted_data['page'] : 1;
$res = $c->ItemSearch($posted_data,$current_module,$page,0);
$res = ($res['results']) ? $res['results'] : array();
echo json_encode(array('results'=>$res,'total'=>$smarty->getTemplateVars('list'),'pages'=>$smarty->getTemplateVars('num_links'),'page'=>$posted_data['page']));
}
}
?>