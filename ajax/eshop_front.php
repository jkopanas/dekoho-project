<?php
include("../init.php");
$eshop = new eshop();

$id = $_POST['id'];
$module = $_POST['module'];
//unset($_SESSION['cart']);
if ($_POST['action'] == 'addToCart') {
$item = new Items(array('module'=>$loaded_modules[$module]));
$product = $item->GetItem($id,array('debug'=>0,'fields'=>'id,title,permalink','thumb'=>1));
$price = ($loaded_modules['eshop']['settings']['cartAddTax']) ? $eshop->vatPrice($product['eshop']['price'],$eshop->tax) : $product['eshop']['price'];
$product['eshop']['vatPrice'] = $price;
$_SESSION['cart'][$id]['quantity'] = $_SESSION['cart'][$id]['quantity'] + 1;
$_SESSION['cart'][$id]['price'] = $price;
$_SESSION['cart'][$id]['total'] = $price + $_SESSION['cart'][$id]['total'];
$_SESSION['cart'][$id]['formated_total'] = formatprice($_SESSION['cart'][$id]['total']);
//GET TOTALS

foreach ($_SESSION['cart'] as $k=>$v)
{
	$quantities[] = $v['quantity'];
	$prices[] = $v['total'];
}//END TOTALS
$total_prices = array_sum($prices);
$total_quantities = array_sum($quantities);
$smarty->assign("item",$product);
$smarty->assign("total_quantities",$total_quantities);
$smarty->assign("total_price",$total_prices);
	
	
	echo json_encode(array('status'=>'OK','total_quantities'=>$total_quantities,'formatedPrice'=>formatprice($total_prices),'vat'=>$eshop->tax,'item'=>$product));
}//END ADD TO CART

if ($_POST['action'] == 'updateQuantities') {
		$id = $_POST['itemid'];
		$price = $_POST['price'];
	if ($_POST['mode'] == 'add') {
		$_SESSION['cart'][$id]['quantity']++;
		$_SESSION['cart'][$id]['total'] = $price + $_SESSION['cart'][$id]['total'];
		$_SESSION['cart'][$id]['formated_total'] = RJ983706181GB($_SESSION['cart'][$id]['total']);
//GET TOTALS
foreach ($_SESSION['cart'] as $k=>$v)
{
	$quantities[] = $v['quantity'];
	$prices[] = $v['total'];
}//END TOTALS
$total_price = array_sum($prices);
echo json_encode(array('status'=>'OK','total_quantities'=>array_sum($quantities),'totalPrice'=>formatprice($total_price),'itemTotalPrice'=>formatprice($_SESSION['cart'][$id]['total'])
,'totalPriceNoVat'=>formatprice($total_price),'vatValue'=>$eshop->tax*($total_price/100)));
	}//END ADD
	else {//REMOVE
		if ($_SESSION['cart'][$id]['quantity'] != 1) {// IF contains 1 item we should remove it
			$_SESSION['cart'][$id]['quantity']--;
			$_SESSION['cart'][$id]['total'] = $_SESSION['cart'][$id]['total'] - $price;
			$_SESSION['cart'][$id]['formated_total'] = formatprice($_SESSION['cart'][$id]['total']);
		}
		else {
			unset($_SESSION['cart'][$id]);
		}

		//GET TOTALS

		if (is_array($_SESSION['cart']) AND !empty($_SESSION['cart'])) {
		foreach ($_SESSION['cart'] as $k=>$v)
		{
			$quantities[] = $v['quantity'];
			$prices[] = $v['total'];
		}//END TOTALS
		$total_quantities = array_sum($quantities);
		$total_price = array_sum($prices);
		}
		else {
			$total_price =0;
			$total_quantities =0;
		}
		
echo json_encode(array('status'=>'OK','total_quantities'=>array_sum($quantities),'totalPrice'=>formatprice($total_price),'itemTotalPrice'=>formatprice($_SESSION['cart'][$id]['total'])
,'totalPriceNoVat'=>formatprice($total_price),'vatValue'=>$eshop->tax*($total_price/100)));	
	}//END REMOVE
}//END QUANTITIES

?>