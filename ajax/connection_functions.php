<?php
include("../manage/init.php");
if($_GET['action']=="add")
{
    $settings = $_POST['formData'];
    $connectionSettings = $_POST['connectionSettings'];
    //if(is_array($settings['Active']))
    $settings['connectionSettings']=json_encode($connectionSettings);
    $conn = new connections;
    $conn->newConnection($settings);
    if($settings['Active']!=1)$settings['Active']=0;
    if($settings['setDefault']!=1)$settings['setDefault']=0;
	else
	{
		global $sql;
        $sql->db_Update("smsConnections","setDefault=0");
        //execute_single($sql);
	}
	
    $data = $conn->getConnections("ORDER BY id DESC");
    $smarty->assign("items",$data);
    $smarty->display("modules/sms/admin/connectionResults.tpl");
}

if($_GET['action']=="delete")
{
    
    
    $conn = new connections;
    $routes = new routes;
    $id=$_GET['id'];
    $data = $routes->getRoutes("connectionID='$id'",1);
    if($data!=0)
    {
        $routes = "";
        foreach($data as $item)
        {
            $routes.=$item['title'].",";
        }
        $routes=trim($routes,",");
        $message = $lang['unable_to_delete_con']." ".$routes;
       $smarty->assign("warningMessage",$message);
    }
    else
    {
        $conn->deleteConnection($_GET['id']);    
    }
    
    
    
    $data = $conn->getConnections("ORDER BY id DESC");
    $smarty->assign("items",$data);
    $smarty->display("modules/sms/admin/connectionResults.tpl");
}

if($_GET['action']=="edit")
{
    $conns = new connections;
    $data = $conns->getConnectionDetails($_GET['id']);
    
    $data['settings'] = json_decode($data['connectionSettings'],true);
    
    $smarty->assign("item",$data);
    $smarty->display("modules/sms/admin/connectionForm.tpl");
}

if($_GET['action']=="update")
{
	 $settings = $_POST['formData'];
    $connectionSettings = $_POST['connectionSettings'];
    $settings['connectionSettings']=json_encode($connectionSettings);
    
    if($settings['Active']!=1)$settings['Active']=0;
    
    if($settings['setDefault']!=1)$settings['setDefault']=0;
    
	if($settings['setDefault']==1 OR $settings['wasDefault']==1)
	{
	    global $sql;
        $sql->db_Update("smsConnections","setDefault=0");
        $settings['setDefault']=1;
        
	}
    else if($settings['setDefault']==0 AND $settings['wasDefault']==0) 
    {
        
        $settings['setDefault']=0;
    }
            
    unset($settings['wasDefault']);
	
	$conn = new connections;
    $conn->updateConnection($settings['id'],$settings);
    
    $data = $conn->getConnections("ORDER BY id DESC");
    $smarty->assign("items",$data);
    $smarty->display("modules/sms/admin/connectionResults.tpl");
}
?>