<?php
global $s,$smarty,$loaded_modules;
$current_module = $loaded_modules[$_GET['module']];


if (!isset($boxIDs)) {

		foreach($_GET['boxID'] as $key => $value ) {
			if ($value != $box['id'] && !isset($tmp_id)) {
				$tmp_id=$value;
			}
		}
		
		$smarty->assign("settings",$box['settings']);
		$smarty->append("boxFilter",array("title" => $box['title'], "file" => $box['settings']['file']));
		$parentSet = $s->getBox($tmp_id);
		$smarty->assign("ParentSettings",$parentSet['name']);

		$c = new Items(array('module'=>$current_module));
		$allCategories = $c->AllItemsTreeCategories();		
		$smarty->assign("category",$allCategories);
		HookParent::getInstance()->doTriggerHookGlobally("BeforeLoadTemplate",$output);
		$template = $smarty->fetch($box['template']);
		$output = array('template'=> $template, 'box'=> $box, 'a'=>$_GET);
		HookParent::getInstance()->doTriggerHookGlobally("AfterLoadTemplate",$output);
		if (!$buffer) {
			echo json_encode(array('template'=> $template, 'box'=> $box, 'a'=>$_GET));
		}

		
}
?>
