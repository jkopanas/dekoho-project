<?php
include("../manage/init.php");

if($_GET['action']=="add")
{
    $settings = $_POST['secformData'];
    $searchSettings = $_POST;
    unset($searchSettings['secformData']);
    $route = new routes;
    
    $route->newRoute($settings);
    
    $data = $route->searchRoutes($route->getSearchData($searchSettings,$_GET['page']));
    
    $smarty->assign("items",$data);
    $smarty->display("modules/sms/admin/routeResults.tpl");
}

if($_GET['action']=="addCustomRoute")
{
    $settings = $_POST['secformData'];
    $searchSettings = $_POST;
    unset($searchSettings['secformData']);
    
    $route = new routes;
    
    $route->newCustomRoute($settings);
    
    $data = $route->searchRoutes($route->getSearchData($searchSettings,$_GET['page']));
    
    $smarty->assign("items",$data);
    $smarty->display("modules/sms/admin/routeResults.tpl");
}

if($_GET['action']=="deleteCustomRoute")
{
    //print_r($_POST);
    $route = new routes;
    $route->deleteCustomRoute($_GET['id']);
    $data = $route->searchRoutes($route->getSearchData($_POST,$_GET['page']));
    $smarty->assign("items",$data);
    $smarty->display("modules/sms/admin/routeResults.tpl");
}



if($_GET['action']=='getusers')
{
 $user = new user();
    $data = $_POST;
	$data['secondaryData']['debug'] = 0;
	$data['secondaryData']['return'] = 'multi';
	$data['secondaryData']['results_per_page'] = 10;
	$data['likeData']['uname'] = $_POST['uname'];
	$users = $user->searchUsers($user->setupSearchData($data));
	$i=0;
	
    foreach ($users as $v)
	{
		$tmp[$i]['id'] = $v['id'];
		$tmp[$i]['label'] = $v['uname'];
		$tmp[$i]['value'] = $v['uname'];
		$i++;
	}
    $tmp[$i]['id'] = 0;
	$tmp[$i]['label'] = $lang['all'];
	$tmp[$i]['value'] = $lang['all'];

	echo json_encode($tmp);
	exit();
}

if($_GET['action']=="update")
{
    $settings = $_POST['secformData'];
    $searchSettings = $_POST;
    unset($searchSettings['secformData']);

    $route = new routes;
    
    if($settings['Active']!=1)$settings['Active']=0;
    
    $route->updateRoute($settings['id'],$settings);
    
    $data = $route->searchRoutes($route->getSearchData($searchSettings,$_GET['Page']));
    $smarty->assign("items",$data);
    $smarty->display("modules/sms/admin/routeResults.tpl");
    
}

if($_GET['action']=="updateCustomRoute")
{
    
    $settings = $_POST['secformData'];
    $searchSettings = $_POST;
    unset($searchSettings['secformData']);
    
    if($settings['Active']!=1)$settings['Active']=0;
    
    $route = new routes;
    
    $route->updateCustomRoute($settings['id'],$settings);
    
    //print_r($_POST);
    //print_r($route->getSearchData($searchSettings,$_GET['Page']));
    
    $data = $route->searchRoutes($route->getSearchData($searchSettings,$_GET['Page']));
    
    //print_r($data);
    $smarty->assign("items",$data);
    $smarty->display("modules/sms/admin/routeResults.tpl");
    
}

if($_GET['action']=="delete")
{
    
    $route = new routes;
    $route->deleteRoute($_GET['id']);
    $data = $route->searchRoutes($route->getSearchData($_POST,$_GET['page']));
    $smarty->assign("items",$data);
    $smarty->display("modules/sms/admin/routeResults.tpl");
}

if($_GET['action']=="edit")
{
    $route = new routes;
    $conns = new connections;
    $data = $route->getRouteDetails($_GET['id']);
    $connections=$conns->getConnections();
    global $sql;
    $sql->db_Select("dialing_codes","id,Country,Code");
    $countries = execute_multi($sql);
    $smarty->assign("countries",$countries);
    $smarty->assign("connections",$connections);
    $smarty->assign("item",$data);
    
    $smarty->display("modules/sms/admin/routeForm.tpl");
}

if($_GET['action']=="editCustomRoute")
{
    $route = new routes;
    $conns = new connections;
    $data = $route->getCustomRouteDetails($_GET['id']);

    $smarty->assign("item",$data);
    
    $smarty->display("modules/sms/admin/customRouteForm.tpl");
}


if($_GET['action']=="search")
{
    $route = new routes;
    
    $data = $route->searchRoutes($route->getSearchData($_POST,$_GET['page']));
   	
    $smarty->assign("items",$data);
    $smarty->display("modules/sms/admin/routeResults.tpl");
}

if($_GET['action']=="getCountries")
{
    global $sql;
    $sql->db_Select("dialing_codes","id,Country","Country like '%".$_GET['term']."%'");
    //echo "SELECT Country from dialing_codes WHERE Country like '%".$_GET['term']."%'";
    $data = execute_multi($sql);
    
    foreach($data as $v)
    {
        $tmp[$i]['id']     = $v['id'];
		$tmp[$i]['label'] = $v['Country'];
		$tmp[$i]['value'] = $v['Country'];
		$i++;
    }
    
    echo json_encode($tmp);
    
}

if($_GET['action']=="getCustomRoutes")
{
    global $sql;
    global $smarty;
    $sql->db_Select("smsRoutingPricingPolicies","*","rid='".$_GET['id']."'");
    $routes = execute_multi($sql);
    
    $sql->db_Select("smsRoutingTable","cost","id='".$_GET['id']."'");
    $cost = execute_single($sql);
    $cost = $cost['cost'];
    $users = new user;
    for($i=0;$i<count($routes);$i++)
    {
        $routes[$i]["user"] = $users->userDetails($routes[$i]['uid'],array('fields'=>'id,uname'));
    }
    
    for($i=0;$i<count($routes);$i++)
    {
        $markup=$routes[$i]['markup'];
        $value =$routes[$i]['value'];
        if($markup=="%")
        {
            $routes[$i]['cost']=$cost+(($cost/100)*$value);
        }
        elseif($markup=="Fixed")
        {
            $routes[$i]['cost']=$value;
        }
        elseif($markup=="Plus")
        {
            $routes[$i]['cost']=$cost+$value;
        }
    }
    
    //print_r($routes);
    $smarty->assign('items',$routes);
    $smarty->display('modules/sms/admin/customRoutePolicies.tpl');
}

if($_GET['action']=="addFavoriteRoute")
{
    
    global $sql;
    global $smarty;
    $count = $sql->db_Count("routeFavorites","(id)","WHERE uid='".ID."' AND rid='".$_GET['id']."'");
    if($count==0)
    $sql->db_Insert("routeFavorites (rid,uid)","'".$_GET['id']."','".ID."'");

}

?>