<?php

include_once("./init.php");//load from ajax!!!!
$jsonOutput = array();

$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];

if ( $_GET['action'] == "GotoSupportMsg" ) {
	
	$sql->db_Select("users","id,uname,pass","id=".$_GET['uid']);
	$rowuser=execute_single($sql);
	setcookie("user_hotelier", base64_encode($rowuser['id'].":".$rowuser['uname'].":".$rowuser['pass'].":".$_GET['admin']), time()+3600, '/', '', 0);
	
	echo json_encode(array("results" => "/en/hotelier/index.html#supportNew/".$id."/view"),true);
exit;
}

$flag=false;
if(is_array($_REQUEST['filter']))
{
//	print_ar($_REQUEST['filter']);
		foreach ($_REQUEST['filter']['filters'] as $vArr)
		{
			if ($vArr['field'] != "new") {
			if ($vArr['field'] == "date-from" || $vArr['field'] == "date-to") {
				$vArr['field'] = "date_added";
			}
			
			$settings['searchfilters'][] = array(
					'item'     => $vArr['table'].".`".$vArr['field']."`",
					'type'     => strtoupper($vArr['operator']),
					'val'      => $vArr['value'],
			);
			} else {
				$flag=true;
				$settings['filters']['groupBy']="users_tickets.conversationid having counts=1";
				
			}
		}
//	print_ar($settings['searchfilters']);
}



$tmpObj = new Support();


$settings['searchfilters'][] = array(
		'item'     => "1",
		'type'     => "eq",
		'val'      => 1
);
$settings['debug']=0;
$settings['filters']['return']="total";




if (!$flag) {
	$settings['filters']['groupBy']="users_tickets.id";
	$settings['filters']['jointables'][]= array(
		'name'=>"(SELECT MAX(t1.date_add) as time, t1.id, t1.title, t1.description FROM users_tickets t1 GROUP BY conversationid ) recent",
		"idKey"=>"recent.time",
		"idRel"=>"users_tickets.date_add",
		"joinType"=>"INNER JOIN"
	);
}

$settings['filters']['jointables'][]= array(
		'name'=>"users",
		"idKey"=>"users.id",
		"idRel"=>"users_tickets.uid",
		"joinType"=>"INNER JOIN"
);

$settings['filters']['sort_field'] = ($_POST['sort'][0]['dir']) ? $_POST['sort'][0]['dir'] : "users_tickets.id";
$settings['filters']['page']=($_POST['page']) ? $_POST['page'] : 1;
$settings['filters']['perPage'] = ($_POST['pageSize'])?$_POST['pageSize']:10;


if (!$flag) {
$settings['filters']['fields'] = array("users_tickets.id");
} else {
	$settings['filters']['fields'] = array("users_tickets.id,count(conversationid) as counts");
}
$total=$tmpObj->getSupport($settings);

$settings['debug']=0;
$settings['filters']['return']="paginated";
if (!$flag) {
$settings['filters']['fields'] = array("users.uname,users_tickets.uid as userid,users_tickets.*");
} else {
	$settings['filters']['fields'] = array("count(conversationid) as counts,users.uname,users_tickets.uid as userid,users_tickets.*");
}
$res=$tmpObj->getSupport($settings);


if (is_array($res['results'][0])) {
	foreach($res['results'] as $key => $value) {
		$res['results'][$key]['date_add']= date('Y-m-d H:i:s',$value['date_add']);
	}
} else {
	$res['results']=array();
}

$jsonOutput["id"] = $id;
$jsonOutput["results"] = $res['results'];
$jsonOutput["data"]['total'] = count($res);
$jsonOutput["status"] = "success";

print(json_encode($jsonOutput,true));
exit();
?>