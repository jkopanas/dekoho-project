<?php
include_once("./init.php");//load from ajax!!!!
$jsonOutput = array();

$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];

if($_REQUEST['action'] == "delete")
{
	$tmpObj = new Notifier();
	$tmpArr = array();
	if($_REQUEST['id'])
	{
		$tmp = $tmpObj->getNotificationByKey($_REQUEST['id']);
		$tmpArr['id']		= $tmp['id'];
		$jsonOutput["id"] 	= array("id" => $tmpObj->deleteNotification($tmpArr));
	}
	else
	{
		foreach (explode(",", $_REQUEST['ids']) as $v)
		{
			$tmpArr['id'] 		= $v;
			$jsonOutput["id"][] = $tmpObj->deleteNotification($tmpArr);
		}
	}
}
elseif($_REQUEST['action'] == "addnew")
{
	$tmpObj = new Notifier();
	$tmpArr = array();

	$tmpArr['key'] 			= $_REQUEST['key'];
	$tmpArr['date_added'] 	= time();
	$tmpArr['module'] 		= $_REQUEST['module'];
	$tmpArr['description']	= $_REQUEST['description'];
	$tmpArr['status'] 		= $_REQUEST['status'];
	$tmpArr['permalink'] 	= "items.php?id=".$_REQUEST['key']."&module=".$_REQUEST['module'];
	$tmpArr['active'] 		= $_REQUEST['active'];
	$tmpArr['language'] 	= "en";
	$tmpArr['settings'] 	= "{}";

	$jsonOutput["id"] 	= array("id" => $tmpObj->insertNotification($tmpArr));
}
elseif($_REQUEST['action'] == "modify")
{
	$tmpObj = new Notifier();
	$tmpArr = array();

	$tmpArr['id'] 			= $_REQUEST['id'];
	$tmpArr['key'] 			= $_REQUEST['key'];
	$tmpArr['date_added'] 	= time();
	$tmpArr['module'] 		= $_REQUEST['module'];
	$tmpArr['description']	= $_REQUEST['description'];
	$tmpArr['status'] 		= $_REQUEST['status'];
	$tmpArr['permalink'] 	= "items.php?id=".$_REQUEST['key']."&module=".$_REQUEST['module'];
	$tmpArr['active'] 		= $_REQUEST['active'];
	$tmpArr['language'] 	= "en";
	$tmpArr['settings'] 	= "{}";

	$jsonOutput["id"] 	= array("id" => $tmpObj->updateNotification($tmpArr));
}
elseif($_REQUEST['action'] == "inactivate")
{
	$tmpObj = new Notifier();
	$tmpArr = array();
	if($_REQUEST['id'])
	{
		$tmp = $tmpObj->getNotificationByKey($_REQUEST['id']);
		$tmpArr['id']		= $tmp['id'];
		$tmpArr['active'] 	= 0;
		$jsonOutput["id"] 	= array("id" => $tmpObj->updateNotification($tmpArr));
	}
	
	else
	{
		foreach (explode(",", $_REQUEST['ids']) as $v)
		{
			$tmpArr['id'] 		= $v;
			$tmpArr['active'] 	= 0;
			$jsonOutput["id"][] = $tmpObj->updateNotification($tmpArr);
		}
	}
}
elseif($_REQUEST['action'] == "review")
{
	$tmpObj = new DBRevision();
	$tmpArr = array();
	$settings = array();
	$settings['filters'] = array();
	$settings['filters']['key'] = $id;
	$settings['filters']['module'] = $_REQUEST['module'];

	global $sql;
	$sql->db_Select($_REQUEST['module'],"*"," id = ".$id);
	$tmpArr = execute_single($sql);
	$jsonOutput["id"] 		= $id;
	$jsonOutput["revision"]	= $tmpObj->getRevisionByKey($settings);
	$jsonOutput["current"]	= $tmpArr;
	print(json_encode($jsonOutput));
	exit();
}
elseif($_REQUEST['action'] == "commit")
{
	$tmpObj = new DBRevision();
	$tmpArr = array();
	$settings = array();
	$settings['filters'] = array();
	$settings['filters']['key'] = $id;
	$settings['filters']['id'] = $id;
	$settings['filters']['module'] = $_REQUEST['module'];
	$settings['filters']['values'] = $_REQUEST['data'];

	$jsonOutput["id"] 		= $tmpObj->updateByRevision($settings);
	$jsonOutput["status"] 	= "success";
	print(json_encode($jsonOutput));
	exit();
}

$limit 					= ($_REQUEST['results_per_page'] AND $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 40;
$sort_field 			= ($_REQUEST['sort'][0]['field'] ? $_REQUEST['sort'][0]['field'] : 'id');
$sort_dir 				= ($_REQUEST['sort'][0]['dir'] ? $_REQUEST['sort'][0]['dir'] : 'DESC');
$settings				= array();
$settings['filters'] 	= array(
	'return' 		=> 'paginated',
	'limit' 		=> $limit,
	'page' 			=> ($_REQUEST['page'] ? $_REQUEST['page'] : 1 ),
	'sort_field' 	=> $sort_field,
	'sort_direction'=> $sort_dir,
	'searchfilters'	=> array(
		'active' 	=> (isset($_REQUEST['active'])? $_REQUEST['active'] : 1 )
	),
);

if(is_array($_REQUEST['filter']))
{
	foreach ($_REQUEST['filter']['filters'] as  $k => $v)
	{
		//$settings['filters']['searchfilters'][$k] = is_numeric($v)? $v : "%".$v."%";
		switch($k)
		{
			case 'id':
				$settings['filters']['searchfilters']['id'] = $v;
				break;
			case 'description':
				$settings['filters']['searchfilters']['description'] = "%".$v."%";
				break;
			case 'module':
				$settings['filters']['searchfilters']['module'] = $v;
				break;
			case 'date_added':
				list($day,$month,$year) = explode("/", $v);
				$settings['filters']['searchfilters']['date_added'] = mktime(0, 0, 0, $month, $day, $year);
				break;
			case 'status':
				$settings['filters']['searchfilters']['status'] = $v;
				break;
			default:
				break;
		}
	}
}

$tmpObj = new Notifier();
$tmpArr = $tmpObj->getNotifications($settings);
$jsonOutput["data"]		= $tmpArr;
$jsonOutput["count"]	= $tmpObj->getNotificationCount($settings);
$jsonOutput["status"] 	= "success";
print(json_encode($jsonOutput));
exit();
?>