<?php
include ABSPATH.'/plugins/facebook/fb.php';
$user = new fb();



if ($_GET['action'] == "removeApp" ) {

	list($encoded_sig, $payload) = explode('.',$_POST['signed_request'], 2); 

	//decode the data
  	$sig =  base64_decode(strtr($encoded_sig, '-_', '+/')); 
  	$data = json_decode(base64_decode(strtr($payload, '-_', '+/')) , true);

  	if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
    	return null;
  	}
  	
  	$users = $user->userProfile(0,array("getUser" => 1,"searchFields" => array( "facebook_uid" => $data['user_id']) ));

  	if (!empty($users)) {
		$user->bulkModifyUser( array($users['id']) ,array('action' => 'delete'));
  	}
	exit;
	
	
}

if ($_POST['action'] == "insert")  {
	
	$users = $user->userProfile(0,array("getUser" => 1,"searchFields" => array( "facebook_uid" => $data['id']) ));

	if (empty($users['id'])) {
		$data= $_POST['data'];
		$ar = array( "user_name" =>$data['first_name'], "user_surname" => $data['last_name'], "email" => $data['email'], "uname" => $data['username'] );
		$gender = ($data['gender'] == "male") ? 1 : 0;
		list($day, $month, $year) = explode('/',$data['birthday']);
		$ar_profile = array( 
										"birthday" => mktime(0, 0, 0, $month, $day, $year),
										"facebook_uid" => $data['id'],
									    "gender" => $gender,
									    "settings" => json_encode(array("hometown" => $data['hometown'],"timezone" => $data['timezone'], "location" => $data['location'], "locale" => $data['locale'], "link" => $data['link']))
									 );
		$user->addUser($ar, array( "use_profile" => $ar_profile, "action" => "addUser", "debug" => 0 ));
	}
	$user->GetRegisterUser($data['id']);
	echo json_encode($users);
	exit;
	
}

if ($_POST['action'] == "UpdateUser")  {

	$user->UpdateUser($_POST['data']);

}

/*
if ($_GET['action'] == "getTemplate")  {
		global $smarty;
		$t=$smarty->fetch($_POST['template']);
		echo json_encode(array('t' => $t));
		exit;
}
*/

if ($_POST['action'] == "get")  {
	
	$users = $user->userProfile(0,array("getUser" => 1,"searchFields" => array("facebook_uid" => $_POST['id']) ));
	
	$user->GetRegisterUser();
	
	echo json_encode($users);
	exit;
	
}



?>