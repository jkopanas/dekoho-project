<?php

include_once("./init.php");//load from ajax!!!!
$jsonOutput = array();

$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];

if ($_GET['action'] == "ShowHotelRequest" ) {
	
	global $sql;
	$ar=array();
	$tmpObj = new UpdateItem();
	$tmpArr=$tmpObj->GetSingleRequest($id);
	
	foreach($tmpArr as $key => $value) {
		if ($value['table'] == "extra_field_values") {
			$sql->db_Select("extra_fields_groups_items inner join extra_fields_groups on extra_fields_groups.id=extra_fields_groups_items.groupid","extra_fields_groups.title as title","itemid=".$value['itemid']);
			$row=execute_single($sql);
			$ar[$row['title']]++;
		} else if ( $value['table'] == "item_images" ) {
			$sql->db_Select("item_images","module,itemid","id=".$value['itemid']);
			$row=execute_single($sql);
			if ($row['module']) {
				$ar[$row['module']."_photos"]++;
			}
			if ($row['module'] == "rooms") {
				$roomid = $row['itemid'];
			}
		} else {
			if ( $value['table'] == "rooms" ) {
				$roomid = $value['itemid'];
			}
			$value['table'] = ($value['table'] == "locations") ? "Locations" : $value['table'];
			$ar[$value['table']]++;
		}
	}

	$sql->db_Select("users","id,uname,pass","id=".$_GET['uid']);
	$rowuser=execute_single($sql);
	setcookie("user_hotelier", base64_encode($rowuser['id'].":".$rowuser['uname'].":".$rowuser['pass'].":".$_GET['admin']), time()+3600, '/', '', 0);
	
	echo json_encode(array("results" => $ar,"hotelid" => $id, "roomid" => $roomid ,"uid" => $_GET['uid']),true);
	exit;
}


if(is_array($_REQUEST['filter']))
{
		foreach ($_REQUEST['filter']['filters'] as $vArr)
		{
			if ($vArr['field'] == "date-from" || $vArr['field'] == "date-to") {
				$vArr['field'] = "date_added";
			}
			if ($vArr['field'] == "status") {
				$posted_data['searchFields']['status'][0] = array(
						'type'     => strtoupper($vArr['operator']),
						'val'      => $vArr['value'],
						'table'  => $vArr['table']
				);
			} else {
				$posted_data['searchFields'][$vArr['field']][] = array(
					'type'     => strtoupper($vArr['operator']),
					'val'      => $vArr['value'],
					'table'  => $vArr['table']
				);
			}
		}
}

$posted_data['new'] = $_POST['new'];
$posted_data['SearchLogic']="and";
$posted_data['sort'] = $_POST['sort'][0]['field'];

$posted_data['sort_direction'] = $_POST['sort'][0]['dir'];
$posted_data['page']=($_POST['page']) ? $_POST['page'] : 1;
$posted_data['results_per_page'] = $_POST['pageSize'];
$posted_data['fields'] = "*,users_request.id as id,users_request.uid as userid,count(users_request.id) as changes,geo_relations.cityid as cityid,geo_relations.countryid  as countryid";

$tmpObj = new UpdateItem();
list($tmpArr,$total) = $tmpObj->searchRequests($posted_data,$settings,0);
$jsonOutput["id"] 		= $id;
$jsonOutput["results"]		= $tmpArr;
$jsonOutput["data"]['total']	= $total['data']['total'];
$jsonOutput["status"] 	= "success";

print(json_encode($jsonOutput));
exit();
?>