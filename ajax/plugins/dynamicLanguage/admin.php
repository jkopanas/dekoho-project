<?php
include_once("./init.php");//load from ajax!!!!
$jsonOutput = array();

$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];

if($_REQUEST['action'] == "delete")
{
	$tmpObj = new DynamicLanguage();
	$tmpArr = array();

	$tmpArr['id']			= $_REQUEST['id'];
	$jsonOutput["id"] 	= array("id" => $tmpObj->deleteMarketCountry($tmpArr));
}
elseif($_REQUEST['action'] == "addnew")
{
	$tmpObj = new DynamicLanguage();
	$tmpArr = array();
	
	$tmpArr['key'] 			= $_REQUEST['key'];
	$tmpArr['marketKey'] 	= $_REQUEST['marketKey'];
	$tmpArr['name'] 		= $_REQUEST['name'];
	$tmpArr['description']	= $_REQUEST['name']." ".$_REQUEST['currency'];
	$tmpArr['currency'] 	= $_REQUEST['currency'];
	$tmpArr['active'] 		= intval($_REQUEST['active']);
	$tmpArr['language'] 	= $_REQUEST['language'];
	
	$jsonOutput["id"] 	= array("id" => $tmpObj->insertMarketCountry($tmpArr));
}
elseif($_REQUEST['action'] == "modify")
{
	$tmpObj = new DynamicLanguage();
	$tmpArr = array();
	
	$tmpArr['id']			= $_REQUEST['id'];
	$tmpArr['key'] 			= $_REQUEST['key'];
	$tmpArr['marketID'] 	= intval($_REQUEST['marketID']);
	$tmpArr['marketKey'] 	= $_REQUEST['marketKey'];
	$tmpArr['name'] 		= $_REQUEST['name'];
	$tmpArr['currency'] 	= $_REQUEST['currency'];
	$tmpArr['active'] 		= intval($_REQUEST['active']);
	$tmpArr['language'] 	= $_REQUEST['language'];
	//$tmpObj->updateMarketCountry($tmpArr);
	$jsonOutput["id"] 	= array("id" => $tmpObj->updateMarketCountry($tmpArr));
}
elseif($_REQUEST['action'] == "getmarketslist")
{
	$tmpObj = new DynamicLanguage();
	$settings = array(
		'filters' => array(
			"active" => ($_REQUEST['active']? $_REQUEST['active'] : 1 )
		)
	);
	$tmpArr = $tmpObj->getMarkets($settings);
	$jsonOutput = array();
	
	if(is_array($tmpArr))
	{
		foreach ($tmpArr as $vArr)
		{
			$jsonOutput[] = array(
					"text" 		=> "[".$vArr['key']."] ".$vArr['name'],
					"value" 	=> $vArr['key']
			);
		}
	}
	
	print(json_encode($jsonOutput));
	exit();
}
elseif($_REQUEST['action'] == "getlanguagelist")
{
	$tmpObj = new Languages();
	$tmpArr = $tmpObj->AvailableLanguages();
	$jsonOutput = array();

	if(is_array($tmpArr))
	{
		foreach ($tmpArr as $vArr)
		{
			$jsonOutput[] = array(
					"text" 		=> "[".$vArr['code']."] ".$vArr['country'],
					"value" 	=> $vArr['code']
			);
		}
	}

	print(json_encode($jsonOutput));
	exit();
}

$limit = ($_REQUEST['results_per_page'] AND $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 40;
$sort_field = ($_REQUEST['sort'][0]['field'] ? $_REQUEST['sort'][0]['field'] : 'id');
$sort_dir = ($_REQUEST['sort'][0]['dir'] ? $_REQUEST['sort'][0]['dir'] : 'DESC');
$settings = array(
	'return' 		=> 'paginated',
	'limit' 		=> $limit,
	'page' 			=> ($_REQUEST['page'] ? $_REQUEST['page'] : 1 ),
	'sort_field' 	=> $sort_field,
	'sort_direction' => $sort_dir,
	'filters' 		=> array(
		'active' 	=> ($_REQUEST['active']? $_REQUEST['active'] : 1 )
	),
);

if(is_array($_REQUEST['filter']))
{
	foreach ($_REQUEST['filter']['filters'] as $vArr)
	{
		$settings['filters'][$vArr['field']] = $vArr['value'];
	}
}

$tmpObj = new DynamicLanguage();
$tmpArr = $tmpObj->getMarketCountries($settings);
//$jsonOutput[DATA_KEY] 	= array("id" => $_POST['id']);
$jsonOutput["data"]		= $tmpArr;
$jsonOutput["count"]	= $tmpObj->getMarketsCountriesCount($settings);
$jsonOutput["status"] 	= "success";
print(json_encode($jsonOutput));
exit();
?>