<?php
include_once("./init.php");//load from ajax!!!!
$jsonOutput = array();

$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];

$limit = ($_REQUEST['results_per_page'] AND $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 40;
$sort_field = ($_REQUEST['sort'][0]['field'] ? $_REQUEST['sort'][0]['field'] : 'id');
$sort_dir = ($_REQUEST['sort'][0]['dir'] ? $_REQUEST['sort'][0]['dir'] : 'DESC');
$settings = array(
	'return' 		=> 'paginated',
	'limit' 		=> $limit,
	'page' 			=> ($_REQUEST['page'] ? $_REQUEST['page'] : 1 ),
	'sort_field' 	=> $sort_field,
	'sort_direction' => $sort_dir,
	'filters' 		=> array(
		'active' 	=> ($_REQUEST['active']? $_REQUEST['active'] : 1 )
	),
);

if(is_array($_REQUEST['filter']))
{
	foreach ($_REQUEST['filter']['filters'] as $vArr)
	{
		$settings['filters'][$vArr['field']] = $vArr['value'];
	}
}

//$tmpObj = array();
$tmpArr = array();
//$jsonOutput[DATA_KEY] 	= array("id" => $_POST['id']);
$jsonOutput["data"]		= $tmpArr;
$jsonOutput["count"]	= 0;
$jsonOutput["status"] 	= "success";
print(json_encode($jsonOutput));
exit();
?>