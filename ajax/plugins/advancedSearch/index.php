<?php
//ajax/loader.php?file=plugins/advancedSearch/index.php
include_once("./init.php");//load from ajax!!!!
$jsonOutput = array();

$id = ($_GET['id']) ? $_GET['id'] : $_POST['id'];

$settings = array();
$settings['filters'] = array(
	'id'			=> $id,
	'return' 		=> 'paginated',
	'perPage' 		=> ($_REQUEST['results_per_page'] && $_REQUEST['results_per_page'] != '%') ? $_REQUEST['results_per_page']  : 40,
	'page' 			=> ($_REQUEST['page'] ? $_REQUEST['page'] : 1 ),
	'sort_field' 	=> ($_REQUEST['sort'][0]['field'] ? $_REQUEST['sort'][0]['field'] : 'id'),
	'sort_direction'=> ($_REQUEST['sort'][0]['dir'] ? $_REQUEST['sort'][0]['dir'] : 'DESC'),
	'active'		=> 1,
);
if(is_array($_REQUEST['filter']))
{
	foreach ($_REQUEST['filter']['filters'] as $vArr)
	{
		$settings['searchfilters'][] = array(
			'item'     => $vArr['field'],
			'type'     => $vArr['operator'],
			'val'      => $vArr['value'],
		);
	}
}
if(is_array($_REQUEST['categoryies']))
{
	foreach ($_REQUEST['categoryies'] as $k=>$v)
	{
		$settings['filters']['categoryies'][] = array(
			'categoryid'    => $v
		);
	}
}
$tmpObj = new Cruises();
$tmpArr = $tmpObj->getCruises($settings);
// $tmpObj = new Cruises();
// $tmpArr = $tmpObj->getCruisesInDiscount($settings);

$jsonOutput["id"] 		= $id;
$jsonOutput["data"]		= $tmpArr['results'];
$jsonOutput["count"]	= $tmpArr['total'];
$jsonOutput["status"] 	= "success";
print(json_encode($jsonOutput));
exit();
?>
