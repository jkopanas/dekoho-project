<?php
global $Content,$current_module;
$uri=$_GET['uri'];
$code =$_GET['uri'];
$tmpObj = new hotels();
$settings['searchfilters'][] = array(
	'item'     => "hotel.uid",
 	'type'     => "eq",
 	'val'      => 10,
);

$settings['filters']['fields'] = array (
		"hotel.id as id",
		"hotspots.id as idr",
		"hotspots.title",
		"hotspots.description",
		"maps_items.*"
		);
$settings['filters']['active'] = '1';
$hotel = $tmpObj->getHotelHotspots($settings);
$smarty->assign("hotel",$hotel);

$smarty->assign("include_file","hooks/hotelier/hotelManager.tpl");//assigned template variable include_file

?>