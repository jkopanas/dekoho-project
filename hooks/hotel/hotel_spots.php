<?php
global $sql,$loaded_modules;
   if(isset($_GET['uri']) && $_GET['uri']!="" ) {	
 			
	    $hotel_id = $_GET['uri'];
}


$Content = new Items(array('module' => $loaded_modules['hotel']));

$settings_item = array(
		'fields'=>'*',
		'thumb'=>1,
		'CatNav'=>1,
		'active'=>1,
		'cat_details'=>1,
		'main'=>1,
		'images'=>1,
		'available' => 1,
		'efields'=>1,
		"grouped"=>1
);

if ($_GET['preview']) {
	unset($settings_item['active']);
	unset($settings_item['available']);
	$smarty->assign("preview","/preview");
	$smarty->assign("hotelid",$hotel_id);
}


$item = $Content->GetItem($hotel_id,$settings_item);


			$current_module = $loaded_modules['hotel'];	
         	$map = new maps(array('module'=>$current_module));
         	$settings = array('fields'=>'id,lat,lng,geocoderAddress,zoomLevel,MapTypeId,itemid,title,content','debug'=>0);
         	$mapHotel = $map->getItems(array($hotel_id),$settings);



	$current_module = $loaded_modules['hotspots'];	
	$HotSpots = new Items(array('module' => $current_module));	
	$posted_data = array('searchFields'=>array('hotel_id'=>array("val"=>$hotel_id,'type'=>"EQ")));
	$posted_data['results_per_page']= 15;
	//$posted_data['page'] = 15;
	//$page=$posted_data['page'] ;
	$posted_data['sort_direction']="ASC";
	
	if ($_GET['preview']) {
		unset($posted_data['active']);
	}
	$spot_items = $HotSpots->ItemSearch($posted_data,$current_module,$page,0);

	$smarty->assign("spot_items",$spot_items['results']);

	if($spot_items['results']) {

		foreach($spot_items['results'] as $key=>$value) {
			$spot_item_id[] = $value['id'];
		}
	
         	$map = new maps(array('module'=>$current_module));
         	$settings = array('fields'=>'id,lat,lng,geocoderAddress,zoomLevel,MapTypeId,itemid,title,content','debug'=>0);
         	$mapItems = $map->getItems($spot_item_id,$settings);
			
			$i=0;
			foreach($mapItems as $key=>$value) {		
				foreach($spot_items['results'] as $k=>$v) {	
					if($value['itemid']==$v['id']) {
					    $i=$i+1;
						$arr[$v['id']]['title']=$v['title'];
						$arr[$v['id']]['description']=$v['description'];
						$arr[$v['id']]['lat'] = $value['lat'];
						$arr[$v['id']]['lng'] = $value['lng'];
						$arr[$v['id']]['zoomLevel']=20;
						$arr[$v['id']]['MapTypeId']='roadmap';
						$arr[$v['id']]['image'] = "/images/site/".$i.".png";
					}		
					
				}
			}
			
				
			foreach($arr as $key=>$value) {
				$ar['title'][]=$value['title'];
				$ar['description'][]=$value['description'];
				$ar['lat'][]=$value['lat'];
				$ar['lng'][]=$value['lng'];
				$ar['zoomLevel'][]=$value['zoomLevel'];
				$ar['MapTypeId'][]=$value['MapTypeId'];
				$ar['image'][]=$value['image'];
				
			}
			
			
			$title = implode("%",$ar['title']);
		    $description = implode("%",$ar['description']);
		    $lat = implode("%",$ar['lat']);
		    $lng = implode("%",$ar['lng']);
		    $zoomLevel = implode("%",$ar['zoomLevel']);
		    $MapTypeId = implode("%",$ar['MapTypeId']);
		  	$image = implode("%",$ar['image']);
	}	  	
	

			$smarty->assign("title",$title);
			$smarty->assign("description",$description);
			$smarty->assign("latt",$lat);
			$smarty->assign("lngg",$lng);
			$smarty->assign("image",$image);
			
include("main.php"); 
include("filter_rooms.php");


$smarty->assign("mapHotel",$mapHotel);
$smarty->assign("hotel",$item);

$smarty->assign("include_file","hooks/hotel/hotel_spots.tpl");

?>