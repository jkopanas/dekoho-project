<?php
global $sql;

$settings=array();

if ( is_numeric($_GET['uri'])) {
 			
	    $hotel_id = $_GET['uri'];
}

include("filter_rooms.php");

$Content = new Items(array('module' => $loaded_modules['hotel']));

$settings_item = array(
		'fields'=>'*',
		'thumb'=>1,
		'CatNav'=>1,
		'active'=>1,
		'cat_details'=>1,
		'main'=>1,
		'images'=>1,
		'available' => 1,
		'efields'=>1,
		"grouped"=>1
);


$item = $Content->GetItem($hotel_id,$settings_item);
$smarty->assign("hotel",$item);


 	  $tmpObj = new Rooms ();
 	  
 	  if ( isset($_GET['id']) ) { // refers to a single room from the list
 	 
		 	  	$room_id = $hotel_id;
		 	  
		 	  
		 	  		$settings['debug']=0;
		 	  	 $settings['filters'] = array(
		 	  	 						"sort_field" => "ORDER BY field (rooms.id,".$room_id.")",
		                                "sort_direction" => "asc",
		 	  	 						"execute" => "searchBySQL",
		                			//	"active"=>1,    
		                				"fields"=>array(
				                				"rooms.id",
 	  	 										"rooms.title",
		 	  	 								"rooms.description",
		 	  	 								"rooms.hotelKey",
				                				"rooms_hotel.price",
				                				"rooms_hotel.room_type"
		                                 )
		                );
		                
		                
		                $settings['searchfilters'][] = array(
		                                'item'     => "rooms_hotel.room_id",
		                                'type'     => "eq",
		                                'val'      => $room_id   
		                );
		                
		                /*
		                   $settings['searchfilters'][] = array(
	                		'item'     => "rooms_hotel.active",
	                		'type'     => "eq",
	                		'val'      => 1
	                );	                
	                */
 	  							
		               
		                
 	  	
 	  } else {   // main menu room clicked
 	  				$settings['debug']=0;
	                $settings['filters'] = array(
	                				"roomProperties"=>true,
	                                "sort_field" => "price",
	                                "sort_direction" => "asc",
	                				"execute" => "searchBySQL",
	                				"fields"=>array(
			                				"rooms.id",
	                						"rooms.title",
			                				"rooms_hotel.price",
			                				"rooms_hotel.room_type",
			                				"rooms_hotel.availability",
	                						"rooms.hotelKey",
			                				"rooms_hotel.discount",
	                						"rooms_hotel.end_discount_date"
	                                 )
	                );
	                
	                $settings['searchfilters'][] = array(
	                                'item'     => "hotelKey",
	                                'type'     => "eq",
	                                'val'      => $hotel_id
	                );
	                
	                /*
	                $settings['searchfilters'][] = array(
	                		'item'     => "rooms_hotel.active",
	                		'type'     => "eq",
	                		'val'      => 1
	                );	                
	                */
	                if ($_GET['preview']) {
	                	unset($settings['searchfilters']['1']);
	                	$smarty->assign("hotelid",$hotel_id);
	                }
 	  }     
 	  
       $hotel_rooms =   $tmpObj->getRoom($settings);


 	///first room with photos ///////
       $set['searchfilters'][] = array(
       		'item'     => "roomid",
       		'type'     => "eq",
       		'val'      => $hotel_rooms['results'][0]['id'],
       );
       
       $roomfacilities=$tmpObj->getPropertiesRoomsFront($set);
       $hotel_rooms['results'][0]['roomsfacilities']=$roomfacilities['roomsfacilities'];

		
       foreach ((array) $hotel_rooms['results'][0]['roomsfacilities'] as $key=>$value) {
       		$arr[] =  $value['title'];
       }

       $hotel_rooms['results'][0]['roomsfacilities'] = $arr;

       $smarty->assign("cheaper_room",$hotel_rooms['results'][0]);

       $Rooms = new Items(array('module' => $loaded_modules['rooms']));
       	
       $settings_room = array(
		        'fields'=>'*',
		        'thumb'=>1,
		        'CatNav'=>1,
              // 'active'=>1,
		        'cat_details'=>1,
		        'main'=>1,
		        'images'=>1,
              // 'available' => 1
       );
	   
	   if ($_GET['preview']) {
	   		unset($settings_room['active']);
	   		unset($settings_room['available']);
	   		$smarty->assign("preview","/preview");
	   }
			
	  $item_room = $Rooms->GetItem($hotel_rooms['results'][0]['id'],$settings_room);

       
       $smarty->assign("item_room",$item_room);
       
       
       $hotel_id = $hotel_rooms['results'][0]['hotelKey']; //prosoxiii me tis mlks m. Poutana ta xw kanei :P change the actual hotelid before i unset it .
			
       	////next rooms/////
       	if(!$the_persons){
       		unset($hotel_rooms['results'][0]);
      	}
      	
      	if(isset($_GET['id'])) {
      		unset($hotel_rooms['results'][0]);
      	}
      	
      	
       $smarty->assign("next_hotel_rooms",$hotel_rooms['results']);
		
       foreach($hotel_rooms['results'] as $key=>$value) 
       {
       		$next_rooms_details[]= $Rooms->GetItem($value['id'],$settings_room);
       }
       
       
     $smarty->assign("next_rooms_details",$next_rooms_details);


	 include("main.php");  
	

	if($the_persons) { 
	    if (!$_GET['preview']) {

	    	foreach($the_persons as $k=>$v) 
			 			{
			 				
			 				$new_persons[$v]++;
			 			}
					
			 			
			 		foreach($hotel_rooms['results'] as $key=>$value) {
			 			if($value['availability']>="1"){
			 				$hotel_rooms_available[]=$value;
			 			}
			 		}	
			 			
			 			
			 		foreach((array)$hotel_rooms_available as $key=>$value) 
			 		{		
				
			 			foreach($new_persons as $k=>$v) 
			 			{
			 			     if($value['room_type'] == $k) 
			 			     {
			 			     $user_rooms_simple[] = $value;	
			 			   	$user_rooms[$value['room_type']][] = $value;
			 			   
			 			     }		
			 			}
				
			 		}
			 	
			 	if($user_rooms) {
					foreach($user_rooms as $k=>$v) {	
						$max = 10000000000;	
						
						foreach($v as $key=>$value) {
						if($value['discount']!="0" && $value['end_discount_date']>=$the_timestamp) {
						$value['price']=$value['discount'];
					  }
			 			  if($max>$value['price']) {
	
			 			   		$lowest_prices[$value['room_type']]=array("price"=>$value['price'],'id'=>$value['id']);
			 			   		$max=$value['price'];
			 			   	}	
	    				}
					}
					
					    foreach($user_rooms_simple as $key=>$value) 
				       {
				       		$user_rooms_details[]= $Rooms->GetItem($value['id'],$settings_room);
				       }
	      }
			 		$smarty->assign("user_rooms_details",$user_rooms_details);
			 		$smarty->assign("new_persons",$new_persons);
			 		$smarty->assign("lowest_prices",$lowest_prices);
			 		$smarty->assign("user_rooms",$user_rooms);
			 	    $smarty->assign("user_rooms_details",$user_rooms_details);
			 		$smarty->assign("user_rooms_simple",$user_rooms_simple);

			 		
			 		$smarty->assign("include_file","hooks/hotel/filter_hotel_rooms.tpl");
			 			
      } 
 
	 } else {
 
     $smarty->assign("include_file","hooks/hotel/hotel_rooms.tpl");
 }
?>