<?php
global $sql;

if(is_numeric($_GET['uri']) ) {	
 			
	    $hotel_id = $_GET['uri'];
}


$Content = new Items(array('module' => $loaded_modules['hotel']));

$settings_item = array(
		'fields'=>'*',
		'thumb'=>1,
		'CatNav'=>1,
		'active'=>1,
		'cat_details'=>1,
		'main'=>1,
		'images'=>1,
		'available' => 1,
		'efields'=>1,
		"grouped"=>1
);

if ($_GET['preview']) {
	unset($settings_item['active']);
	unset($settings_item['available']);
	$smarty->assign("preview","/preview");
	$smarty->assign("hotelid",$hotel_id);
}


$item = $Content->GetItem($hotel_id,$settings_item);


include("main.php");
include("filter_rooms.php");


 		$current_module = $loaded_modules['hotel'];	
         	$map = new maps(array('module'=>$current_module));
         	$settings = array('fields'=>'id,lat,lng,geocoderAddress,zoomLevel,MapTypeId,itemid,title,content','debug'=>0);
         	$mapItems = $map->getItems(array($hotel_id),$settings);

	
         foreach((array) $item['efields']['81']['data'] as $key=>$value) {
         	if($value['type']=='area' && $value['value']!=' ' && $value['enable'] ) {		
         		   $places[]=$value;
    
         	}
         }	
         //	print_ar($places);
         	
	$smarty->assign("mapItems",$mapItems);
	$smarty->assign("hotel",$item);
	$smarty->assign("places",$places);
	//////////end map //////////////////


$smarty->assign("include_file","hooks/hotel/hotel_location.tpl");

?>