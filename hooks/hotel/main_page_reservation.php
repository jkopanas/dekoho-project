<?php



$pageURL = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

$moreurl = explode("?",$pageURL);  
	
			
			$the_url = explode("&",$moreurl[1]);
			
		
			foreach($the_url as $key=>$value) 
			{
				if (strstr($value, 'hotelid=')) 
				{
						$hotel[] = $value;
				}
			}

	$new_hotel = explode("=",$hotel[0]);

	$hotel_id = $new_hotel[1];
   $Content = new Items(array('module' => $loaded_modules['hotel']));
   	
   $settings_item = array(
   		'fields'=>'*',
   		'thumb'=>1,
   		'CatNav'=>1,
   	   // 'active'=>1,
   		'cat_details'=>1,
   		'main'=>1,
   		//'available' => 1,
   		"debug" =>0
   );
   
   $item = $Content->GetItem($hotel_id,$settings_item);
	
	$smarty->assign("hotel",$item);
   
	
	$sql->q("SELECT geocoderAddress FROM maps_items WHERE module='hotel' and itemid=".$hotel_id);
		 $hotel_location = execute_single($sql);
		 	
		 $location['hotel'] = explode(",",$hotel_location['geocoderAddress']);
		 		
		 $smarty->assign("location",$location);
		 
		 $sql->q("SELECT id,country_codes.code,countries_ph_codes.country,prefix from countries_ph_codes INNER JOIN  country_codes ON country_codes.code=countries_ph_codes.code");

       $countries = execute_multi($sql);
		 $smarty->assign('countries',$countries);
		 
		 
		 
		 ///////////////////////////////arrival			
					
					
			foreach($the_url as $key=>$value) 
			{
				if (strstr($value, 'arrival')) 
				{
						$p_arrival[] = $value;
				}
			if (strstr($value, 'departure')) 
				{
						$p_departure[] = $value;
				}
				
				
			}
			foreach($p_arrival as $key=>$value) 
					{
						$arrival = explode("=",$value);
					}
					
					
			foreach($p_departure as $key=>$value) 
					{
						$departure = explode("=",$value);
					}
				    
					$the_arrival = new DateTime($arrival[1]);
					$the_departure = new DateTime($departure[1]);
			    	$interval = $the_arrival->diff($the_departure);
				    $stays =  $interval->format('%a');
					
					 $the_timestamp = strtotime($arrival[1]);
					
					$smarty->assign("stays",$stays);
					
					$smarty->assign("arrival",$arrival[1]);
					$smarty->assign("departure",$departure[1]);
		
		 
		 
	/////////////////////persons-rooms////////////////////
$tmpObj = new Rooms ();
		
		 $settings['debug']=0;
	                $settings['filters'] = array(
	                                "sort_field" => "price",
	                                "sort_direction" => "asc",
	                				"fields"=>array(
			                				"rooms.*",
			                				"rooms_hotel.price",
			                				"rooms_hotel.room_type",
	                                        "rooms_hotel.availability",
			                				"rooms_hotel.discount",
	                						"rooms_hotel.end_discount_date"
	                                 )
	                );
	                
	                $settings['searchfilters'][] = array(
	                                'item'     => "hotelKey",
	                                'type'     => "eq",
	                                'val'      => $hotel_id
	                );
	                
	                $settings['searchfilters'][] = array(
	                		'item'     => "active",
	                		'type'     => "eq",
	                		'val'      => 1
	                );	                
		 
		 
		 $hotel_rooms =   $tmpObj->getRoom($settings);
	                
	                
		 foreach($the_url as $key=>$value) 
			{
				if (strstr($value, 'persons-')) 
				{
						$p_ar[] = $value;
				}
			}
		
	
					foreach($p_ar as $key=>$value) 
					{
						$person[] = explode("=",$value);
					}
					
					foreach($person as $key=>$value) 
					{
						foreach($value as $k=>$v) 
						{
							if (!strstr($v, 'persons-')) 
							{
								$the_persons[] = $v;
							}
					    }
					}
			
			foreach($the_persons as $k=>$v) 
			 			{
			 				
			 				$new_persons[$v]++;
			 			}
			 			
			 			
			 			
			 		foreach($hotel_rooms['results'] as $key=>$value) {
			 			if($value['availability']>="1"){
			 				$hotel_rooms_available[]=$value;
			 			}
			 		}	

			 		foreach($hotel_rooms_available as $key=>$value) 
			 		{			
			 			foreach($new_persons as $k=>$v) 
			 			{
			 			     if($value['room_type'] == $k) 
			 			     {
			 			     $user_rooms_simple[] = $value;	
			 			   	$user_rooms[$value['room_type']][] = $value;
			 			   
			 			     }		
			 			}
			 		}
			 		
			 		
			 		/**** keep room ids user selected ***////
			 		foreach ($user_rooms as $key=>$value) {
			 			foreach ($value as $k=>$v) {
					 		  for ($i=0; $i<$key; $i++) {
					            $rooms_ids[]=$v['id'];	
					 		  }
			 			 }
			 		 }
			 		
			 	   $all_rooms_ids = implode(",",$rooms_ids);
 
                  $smarty->assign("rooms_ids",$all_rooms_ids);
			 		 /****end keep room ids user selected ***////
			 		 
					foreach($user_rooms as $k=>$v) {	
						$max = 10000000000;	
						
						foreach($v as $key=>$value) {
					if($value['discount']!="0" && $value['end_discount_date']>=$the_timestamp) {
						$value['price']=$value['discount'];
					}	
			 			  if($max>$value['price']) {
								
			 			   		$lowest_prices[$value['room_type']]=array("price"=>$value['price'],'id'=>$value['id']);
								
			 			   		$max=$value['price'];
			 			   	}	
	    				}
					}
					
				
					$the_total = 0;
					$total_persons = 0;
					foreach($new_persons as $key=>$value) {
						foreach($lowest_prices as $k=>$v) {
							if($key ==$k) {
								$the_total = $the_total+$value*$v['price'];
								$total_persons = $total_persons+$key*$value;
								
							}
							
						}
					} 	
					
					
				 $total= $the_total*$stays;
				 
				$smarty->assign("total",$total);
					   $smarty->assign("total_persons",$total_persons);
					$smarty->assign("the_persons",$new_persons);
					
		
		 
	
   
?>