<?php
global $sql;
   if(isset($_GET['uri']) && $_GET['uri']!="" ) {	
 			
	    $hotel_id = $_GET['uri'];
}



$Content = new Items(array('module' => $loaded_modules['hotel']));

$settings_item = array(
		'fields'=>'*',
		'thumb'=>1,
		'CatNav'=>1,
		'active'=>1,
		'cat_details'=>1,
		'main'=>1,
		'images'=>1,
		'available' => 1,
		'efields'=>1,
		"grouped"=>1
);

if ($_GET['preview']) {
	unset($settings_item['active']);
	unset($settings_item['available']);
	$smarty->assign("preview","/preview");
	$smarty->assign("hotelid",$hotel_id);
}


$item = $Content->GetItem($hotel_id,$settings_item);

include("main.php");
include("filter_rooms.php");

$smarty->assign("hotel",$item);

$smarty->assign("include_file","hooks/hotel/hotel_policy.tpl");

?>