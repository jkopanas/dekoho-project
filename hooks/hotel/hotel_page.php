<?php
global $sql;


if ( is_numeric($_GET['uri'])) {
	
   $hotel_id = $_GET['uri'];
   $Content = new Items(array('module' => $loaded_modules['hotel']));
   	
   $settings_item = array(
   		'fields'=>'*',
   		'thumb'=>1,
   		'CatNav'=>1,
   	   // 'active'=>1,
   		'cat_details'=>1,
   		'main'=>1,
   		'images'=>1,
   		//'available' => 1,
   		'efields'=>1,
   		"grouped"=>1,
   		"debug" =>0
   );
   
   if ($_GET['preview']) {
   		unset($settings_item['active']);
   		unset($settings_item['available']);
   		$smarty->assign("preview","/preview");
   		$smarty->assign("hotelid",$hotel_id);
   }
   	
   $item = $Content->GetItem($hotel_id,$settings_item);
   
}
   


    	include("main.php");
		include("filter_rooms.php");
    
 	$Hotel = new Hotels();
 	$settings = array();
 	$settings['searchfilters'][] = array(
                                'item'     => "hotelid",
                                'type'     => "eq",
                                'val'      => $hotel_id
                );
 	
 	
 	if (!$_GET['preview']) {
 		$settings['searchfilters'][] = array(
 				'item'     => "enable",
 				'type'     => "eq",
 				'val'      => "1"
 		);
 	}
 	
 	$hotel_prop = $Hotel->getPropertiesHotelFront($settings);
 	

 	$smarty->assign("hotel_prop",$hotel_prop);
 	
 	$smarty->assign("hotel",$item);

	$smarty->assign("include_file","hooks/hotel/hotel_page.tpl");

?>