<?PHP
Class pagination
{

	var $getsVars;
	var $recordByPage;
	var $PagesFound;
	var $totalRecords;
	var $currentPage;
	
	function pagination($page,$recordByPage)
	{
		$this->_configCurrentPage($page);
		$this->_setRecordByPage($recordByPage);					
	}//END OF FUNCTION

   	/**
    * Setting current page
	* @access 	private
    */
	function _configCurrentPage($page)
	{
		if (empty($page)):
			$this->currentPage = 1; 
		else:    
			$this->currentPage = $page; 
		endif;	
	}	

   	/**
    * Setting the records listed in each page
	* @access 	private
    */
	function _setRecordByPage($recordByPage)
	{
		$this->recordByPage = $recordByPage;
	}		

   	/**
    * Setting the total number of records retrieved
	* @access 	public
    */
	function setTotalRecords($totalRecords)
	{
		$this->totalRecords = $totalRecords;
		$this->PagesFound = ceil($this->totalRecords / $this->recordByPage); 			
	}	

   	/**
    * Defining the additional parameters that may be necessary on the search and will be passed between pages with the GET method
	* @access 	public
    */
	function setGetVars($getsVars)
	{
		$this->getsVars = $getsVars;
	}


   	/**
    * Outputs the total number of records
	* @access 	public
    */
	function getTotalRecords()	
	{
		return $this->totalRecords;
	}	

   	/**
    * Outputs the total number of pages
	* @access 	public
    */
	function getPagesFound()
	{
		return $this->PagesFound;
	}

   	/**
    * Outputs the Navigation links
	* @access 	public
    */
	function getNavigation()
	{
		$result = "";
		$previous = $this->currentPage - 1; 
		$next     = $this->currentPage + 1; 
		$ar = array();
		if ($this->getsVars ==""):
			if ($this->PagesFound>1):
				if ($this->currentPage!=1): $ar['first'] = 1; endif;
				if ($this->currentPage>1): $ar['previous'] = $previous; 	endif;
				if (($this->currentPage < $this->PagesFound) && ($this->PagesFound >= 1)):	$ar['next'] = $next;		endif;
				if (($this->PagesFound>1)&&($this->currentPage!=$this->PagesFound)): $ar['final'] = $this->PagesFound;	endif;
			endif;			
		else:
			if ($this->currentPage!=1):$ar['first'] = 1; endif;
			if ($this->currentPage>1):  $ar['previous'] = $previous; endif;
			if (($this->currentPage<$this->PagesFound) && ($this->PagesFound>=1)):	$ar['next'] = $next; endif;
			if (($this->PagesFound>1)&&($this->currentPage!=$this->PagesFound)): $ar['final'] = $this->PagesFound;	endif;
		endif;	
		return $ar;
	}//END OF FUNCTION

   	/**
    * Outputs Navigation records list based in current page
	* @access 	public
    */
	function getCurrentPages()
	{
		$totalRecordsControl = $this->totalRecords;
		if (($totalRecordsControl%$this->recordByPage!=0)):
			while($totalRecordsControl%$this->recordByPage!=0):
				$totalRecordsControl++;
			endWhile;
		endif;
		$ultimo = substr($this->currentPage,-1);  
		if ($ultimo == 0):
			$begin = ($this->currentPage-9);
			$pageInicial = ($this->currentPage - $ultimo);
			$end = $this->currentPage;			
		else:
			$pageInicial = ($this->currentPage - $ultimo);			
			$begin = ($this->currentPage-$ultimo)+1;
			$end = $pageInicial+10;				
		endif;
		$num = $this->PagesFound;
		if ($end>$num):
		    $end = $num; 
		endif;
		for ($a = $begin; $a <= $end ; $a++):
		$ar[] = array('page' => $a);
		endfor;	
		return $ar;
	}//END OF FUNCTION

   	/**
    * Outputs the records list based in current page
	* @access 	public
    */
	function getListCurrentRecords()
	{
		$regFinal = $this->recordByPage * $this->currentPage;
		$regInicial = $regFinal - $this->recordByPage;
		if ($regInicial == 0): 
			$regInicial++; 
		endif;
		if ($this->currentPage == $this->PagesFound): 
			$regFinal = $this->totalRecords; 
		endif;	
		if ($this->currentPage > 1):  
			$regInicial++; 
		endif;
		$result = array('from' => $regInicial,'to' => $regFinal,'total' => $this->totalRecords);
		return $result;
	}//END OF FUNCTION

	function results($sql)
{
		$regFinal = $this->recordByPage * $this->currentPage;
		$regInicial = $regFinal - $this->recordByPage;
		if ($regInicial == 0): 
			$regInicial++; 
		endif;
		if ($this->currentPage == $this->PagesFound): 
			$regFinal = $this->totalRecords; 
		endif;	
		if ($this->currentPage > 1):  
			$regInicial++; 
		endif;

for ($i=0;$sql->db_Rows() > $i;$i++)
{
$rs[] = $sql->db_Fetch();
}

$ar = array_chunk($rs,$this->recordByPage);

return $ar[$this->currentPage - 1];
}//END OF FUNCTION
}//END OF class

?>