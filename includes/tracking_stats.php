<?php

xSESSION_register("stats_pageid");
xSESSION_register("stats_page_time");
xSESSION_register("stats_pages_string");
xSESSION_register("user_type");
$user_type = USER_TYPE;

function update_statistics()
{
    global $stats_pageid, $stats_page_time, $stats_pages_string;
	$sql = new db();
    $old_stats_pageid = $stats_pageid;
    $curtime = time();
    $REQUEST_URI = e_REQUEST_URI;
//    $stats_pageid = $_SESSION['page_stats_pageid'];
//    $stats_page_time = $_SESSION['page_time'];
//    $stats_pages_string = $_SESSION['stats_pages_string'];

# Update statistics of previous page views and average time user spent on that page
    if ($stats_pageid && $stats_page_time) 
    {
        $time_avg = $curtime - $stats_page_time;
        $sql->db_Update("stats_pages_views","time_avg='$time_avg' WHERE pageid='$stats_pageid'");
    }

    $stats_page_time = $curtime;

#
# Insert/update statistics of current page views
#
        $page = $REQUEST_URI;
        
    $page = ereg_replace("^(.+)([?&]+)(PHPSESSID=)([0-9a-hA-H]+)([&]*)(.*)$", "\\1\\5\\6", $page);
    $stats_pageid = "";

    $sql->db_Select("stats_pages","pageid","page = '$page'");
    $a = execute_single($sql,0);
    $stats_pageid = $a['pageid'];


    if (empty($stats_pageid)) {
		$sql->db_Insert("stats_pages","'','$page'");
	    $stats_pageid = mysql_insert_id();
	}
    $sql->db_Insert("stats_pages_views","'$stats_pageid','','$curtime'");

#
# Insert/update statistics of current pages path views
#

    if ($old_stats_pageid == $stats_pageid) return; # if reload - return
    
    if ($stats_pages_string)
        $stats_pages_string .= "-".$stats_pageid;
    else
        $stats_pages_string = $stats_pageid;

    $paths = split("-", $stats_pages_string);

	if (is_array($paths)) {
#
# Maximum length of the pages path
#
		$max_len_path = 5;
	
		$offset = (count($paths)<=$max_len_path)?0:count($paths)-$max_len_path;
	
		$paths = array_slice($paths, $offset);
	}
	
    $string = "";
    for($i = count($paths)-1; $i >= 0 ; $i--) {
        if ($string)
            $string = $paths[$i]."-".$string;
        else
            $string = $paths[$i];


		if (preg_match("\-", $string)) {

            $sql->db_Insert("stats_pages_paths","'$string','$curtime'");
		}

    }
} # function end

if(!defined('IS_ROBOT')) 
{
	update_statistics();
}
xSESSION_save();
?>