<?php

#
# Simple crypt function. Returns an encrypted version of argument.
# Does not matter what type of info you encrypt, the function will return
# a string of ASCII chars representing the encrypted version of argument.
# Note: text_crypt returns string, which length is 2 time larger
#
function text_crypt_symbol($c) {
# $c is ASCII code of symbol. returns 2-letter text-encoded version of symbol

        global $START_CHAR_CODE;

        return chr($START_CHAR_CODE + ($c & 240) / 16).chr($START_CHAR_CODE + ($c & 15));
}

function text_crypt($s, $is_blowfish = false) {
    global $START_CHAR_CODE, $CRYPT_SALT, $merchant_password, $current_area, $active_modules, $blowfish, $config;
   if ($s == "")
        return $s;
	if($is_blowfish && $merchant_password && ($current_area == 'A' || ($current_area == 'P' && $active_modules["Simple_Mode"])) && $blowfish && $config['Security']['blowfish_enabled'] == 'Y') {
		$result = addslashes("B".func_crc32($s).func_bf_crypt($s, $merchant_password));
	} else {
    	$enc = rand(1,255); # generate random salt.
	    $result = "S".text_crypt_symbol($enc); # include salt in the result;
    	$enc ^= $CRYPT_SALT;
	    for ($i = 0; $i < strlen($s); $i++) {
    	    $r = ord(substr($s, $i, 1)) ^ $enc++;
	        if ($enc > 255)
        	    $enc = 0;
    	    $result .= text_crypt_symbol($r);
	    }
	}
    return $result;
}

function text_decrypt_symbol($s, $i) {
# $s is a text-encoded string, $i is index of 2-char code. function returns number in range 0-255

        global $START_CHAR_CODE;

        return (ord(substr($s, $i, 1)) - $START_CHAR_CODE)*16 + ord(substr($s, $i+1, 1)) - $START_CHAR_CODE;
}

function text_decrypt($s) {
    global $START_CHAR_CODE, $CRYPT_SALT, $merchant_password, $current_area, $active_modules, $blowfish;
    if ($s == "")
        return $s;
	$crypt_method = substr($s, 0, 1);
	$s = substr($s, 1);
	if($crypt_method == 'B') {
		if($merchant_password && ($current_area == 'A' || ($current_area == 'P' && $active_modules["Simple_Mode"])) && $blowfish) {
			$crc32 = substr($s, 0, 4);
			$s = substr($s, 4);
			$result = func_bf_decrypt($s, $merchant_password);
			if(func_crc32($result) != $crc32) {
				$result = func_get_langvar_by_name('err_data_corrupted');
			}
		} else {
			return false;
		}
    } elseif($crypt_method != 'B') {
        if($crypt_method != 'S') {
            $s = $crypt_method.$s;
        }
    	$enc = $CRYPT_SALT ^ text_decrypt_symbol($s, 0);
		$result = "";
    	for ($i = 2; $i < strlen($s); $i+=2) { # $i=2 to skip salt
	        $result .= chr(text_decrypt_symbol($s, $i) ^ $enc++);
        	if ($enc > 255)
    	        $enc = 0;
	    }
	}
    return $result;
}


#
# This function recrypts data with the Blowfish method.
#

function func_data_recrypt() {
	global $sql_tbl, $merchant_password, $current_area, $active_modules, $config;

	if(!$merchant_password || !($current_area == 'A' || ($current_area == 'P' && $active_modules["Simple_Mode"])) || $config['Security']['blowfish_enabled'] != 'Y') {
		return false;
	}
	$orders = func_query("SELECT orderid, details FROM $sql_tbl[orders] WHERE details NOT LIKE 'B%'");
	if($orders) {
		$cnt = 0;
		set_time_limit(86400);
		foreach($orders as $order) {
            if(++$cnt / 100 == 0 && $cnt) {
                echo ".";
                if($cnt / 5000 == 0) {
                    echo "<br>\n";
                }
                func_flush(); 
            }
			$details = text_decrypt($order['details']);
			$details = text_crypt($details, true);
			db_query("UPDATE $sql_tbl[orders] SET details = '$details' WHERE orderid = '$order[orderid]'");
		}
	}
	return true;
}

#
# This function decrypts data Blowfish method -> Standart method.
#

function func_data_decrypt() {
    global $sql_tbl, $merchant_password, $current_area, $active_modules;

    if(!$merchant_password || !($current_area == 'A' || ($current_area == 'P' && $active_modules["Simple_Mode"]))) {
        return false;
    }
    $orders = func_query("SELECT orderid, details FROM $sql_tbl[orders] WHERE details LIKE 'B%'");
    if($orders) {
		$cnt = 0;
        set_time_limit(86400);
        foreach($orders as $order) {
			if(++$cnt / 100 == 0 && $cnt) {
				echo ".";
				if($cnt / 5000 == 0) {
					echo "<BR>\n";
				}
				func_flush();
			}
            $details = text_decrypt($order['details']);
            $details = text_crypt($details);
            db_query("UPDATE $sql_tbl[orders] SET details = '$details' WHERE orderid = '$order[orderid]'");
        }
    }
    return true;
}

#
# This function recrypts Blowfish-crypted data with new password
# where:
#	old_password - old Merchant password

function func_change_mpassword_recrypt($old_password) {
    global $sql_tbl, $merchant_password, $current_area, $active_modules, $blowfish;

    if(!$old_password || !$merchant_password || !($current_area == 'A' || ($current_area == 'P' && $active_modules["Simple_Mode"]))) {
        return false;
    }

    $orders = func_query("SELECT orderid, details FROM $sql_tbl[orders] WHERE details LIKE 'B%'");
    if($orders) {
		$_merchant_password = $merchant_password;
		$cnt = 0;
		set_time_limit(86400);
        foreach($orders as $order) {
            if(++$cnt / 100 == 0 && $cnt) {
                echo ".";
                if($cnt / 5000 == 0) {
                    echo "<BR>\n";
                }
                func_flush(); 
            }
			$merchant_password = $old_password;
            $details = text_decrypt($order['details']);
			$merchant_password = $_merchant_password;
            $details = text_crypt($details, true);
            db_query("UPDATE $sql_tbl[orders] SET details = '$details' WHERE orderid = '$order[orderid]'");
        }
		$merchant_password = $_merchant_password;
    }

    return true;
}



#
# Get CRC32 as 4-symbols
#

function func_crc32($str) {
	$int = crc32($str); 
	return chr($int >> 24).chr(($int >> 16) & 0xff). chr(($int >> 8) & 0xff).chr($int & 0xff);
}
?>
