<?php
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# DO NOT CHANGE ANYTHING BELOW THIS LINE UNLESS
# YOU REALLY KNOW WHAT ARE YOU DOING
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#

if ( !defined('SESSION_START') )
{

	#
	# PHP build-in sessions tuning (for type "1" & "2")
	#

	# PHP 4.3.0 and higher allow to turn off trans-sid using this command:
	ini_set("url_rewriter.tags","");
	# Let's garbage collection will occurs more frequently
	ini_set("session.gc_probability",90);
	ini_set("session.gc_divisor",100); # for PHP >= 4.3.0

	#
	# Anti cache block
	#
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

	if ($HTTP_SERVER_VARS["HTTPS"] || $HTTP_SERVER_VARS["SERVER_PORT"] == 443) 
		header("Cache-Control: private");
	else {
		header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
		header("Pragma: no-cache");
	}

    define("SESSION_START", 1);

    if (isset($HTTP_POST_VARS[$SESSION_NAME]))
        $SESSID = $HTTP_POST_VARS[$SESSION_NAME];
    elseif (isset($HTTP_GET_VARS[$SESSION_NAME]))
        $SESSID = $HTTP_GET_VARS[$SESSION_NAME];
	elseif (isset($HTTP_COOKIE_VARS[$SESSION_NAME]))
		$SESSID = $HTTP_COOKIE_VARS[$SESSION_NAME];
    else {
 		$SESSID = false;
    }
 	xSESSION_start($SESSID);
	register_shutdown_function("xSESSION_save");
 	setcookie($SESSION_NAME, $SESSID, 0, "/", "$https_host", 0);
 	setcookie($SESSION_NAME, $SESSID, 0, "/", "$http_host", 0);

//	$smarty->assign("SESSNAME", $SESSION_NAME);
//	$smarty->assign("SESSID", $SESSID);

}

####################################################################
#   FUNCTIONS
####################################################################

#
# Start session
#
function xSESSION_start($sessid) 
{
	$sql = new db();
	global $SESSION_VARS, $SESSION_NAME, $SESSID;
	global $config, $useSESSIONs_type;

	$SESSION_VARS = array();
	#
	# For new sessions always generate unique id
	#
	srand((double)microtime()*1000000);
	if (!$sessid) 
	{
					$sessid = md5(uniqid(rand()));
	}//END OF NO SESSION ID
	
	if ($useSESSIONs_type < 3) 
	{
		if( $useSESSIONs_type == 2 ) # restore handler for mysql sessions
			session_set_save_handler (
				'db_session_open',
				'db_session_close',
				'db_session_read',
				'db_session_write',
				'db_session_destroy',
				'db_session_gc'
			);
#
# Using standard PHP sessions
#
		session_cache_limiter('none');
		
		session_name($SESSION_NAME);
		if ($sessid) session_id($sessid);
		session_start();
		$SESSID = session_id();

		return;
	}//END OF DIFFERENT SESSION TYPE
	
	if (empty($config["Sessions"]["session_length"]))
		$config["Sessions"]["session_length"] = 30;

	$curtime = time();
	$expiry_time = $curtime + $config["Sessions"]["session_length"];

	$sql->db_Delete("sessions_data","expiry<'".time()."'");
	
	$sql->db_Select("sessions_data","*","sessid='$sessid'");
	$sess_data = execute_single($sql,0);

	if ($sess_data) {
		$SESSION_VARS = unserialize($sess_data["data"]);
		$sql->db_Update("sessions_data","expiry='$expiry_time' WHERE sessid='$sessid'");
	}
	else 
	{

		$sql->q("REPLACE INTO sessions_data (sessid, start, expiry) VALUES('$sessid', '$curtime', '$expiry_time')");
	}

	$SESSID = $sessid;
 	setcookie($SESSION_NAME, $SESSID, 0, "/", "", 0);
}//END OF FUNCTION

#
# Change current session to session with specified ID
#
function xSESSION_id($sessid="") 
{
	$sql = new db();
	global $useSESSIONs_type, $SESSION_VARS, $SESSID;

	$SESSION_VARS = array();
	if ($useSESSIONs_type < 3) {
#
# Using standard PHP sessions
#
		if ($sessid) {
			session_write_close();
			xSESSION_start($sessid);
			return;
		}
		$SESSID = session_id();
		return $SESSID;
	}

	if ($sessid) {
		$sql->q("SELECT * FROM sessions_data WHERE sessid='$sessid'");
		$sess_data = execute_single($sql,0);
		$SESSID = $sessid;
		if ($sess_data)
			$SESSION_VARS = unserialize($sess_data["data"]);
		else
			xSESSION_start($sessid);
	}
	else
		$sessid = $SESSID;
	return $sessid;
}

#
# Cut off variable if it is come from _GET, _POST or _COOKIES
#
function checkSESSION_var($varname) 
{
	global $HTTP_GET_VARS, $HTTP_POST_VARS, $HTTP_COOKIE_VARS;

	if (isset($HTTP_GET_VARS[$varname]) || isset($HTTP_POST_VARS[$varname]) || isset($HTTP_COOKIE_VARS[$varname]))
		return false;
	return true;
}

#
# Register variable SESSION_VARS array from the database
#
function xSESSION_register($varname, $default="") 
{
	global $SESSION_VARS, $SESSION_UNPACKED_VARS;
	global $useSESSIONs_type;
	global $HTTPSESSION_VARS;
	
	if (empty($varname))
		return false;

	if ($useSESSIONs_type < 3) {
#
# Using standard PHP sessions
#
		if (!session_is_registered($varname) && !checkSESSION_var($varname)) {
			$HTTPSESSION_VARS[$varname] = $default;
		}
		session_register($varname);
		#
		# Register global variable
		#
		$GLOBALS[$varname] =& $HTTPSESSION_VARS[$varname];
		return;
	}

#
# Register variable $varname in $SESSION_VARS array
#
	if (!isset($SESSION_VARS[$varname])) {
	
		if (isset($GLOBALS[$varname]) && checkSESSION_var($varname)) {
			$SESSION_VARS[$varname] = $GLOBALS[$varname];
		}
		else {
			$SESSION_VARS[$varname] = $default;
		}
	}
	else {
		if (isset($GLOBALS[$varname]) && checkSESSION_var($varname)) {
			$SESSION_VARS[$varname] = $GLOBALS[$varname];
		}
	}

#
# Unpack variable $varname from $SESSION_VARS array
#
	$SESSION_UNPACKED_VARS[$varname] = $SESSION_VARS[$varname];
	$GLOBALS[$varname] = $SESSION_VARS[$varname];

}


#
# Save the SESSION_VARS array in the database
#
function xSESSION_save() 
{
	$sql = new db();
	global $SESSID;
	global $SESSION_VARS, $SESSION_UNPACKED_VARS;
	global  $useSESSIONs_type;

	if ($useSESSIONs_type < 3) {
#
# Using standard PHP sessions
#
		return;
	}

    $varnames = func_get_args();
	if (!empty($varnames)) {
	    foreach($varnames as $varname) {
			if (isset($GLOBALS[$varname]))
				$SESSION_VARS[$varname] = $GLOBALS[$varname];
		}
    }
	elseif (is_array($SESSION_UNPACKED_VARS)) {
	    foreach($SESSION_UNPACKED_VARS as $varname=>$value) {
			if (isset($GLOBALS[$varname]))
				$SESSION_VARS[$varname] = $GLOBALS[$varname];
		}
	}


#
# Save session variables in the database
#
	$sql->db_Update("sessions_data","data='".addslashes(serialize($SESSION_VARS))."' WHERE sessid='$SESSID'");
}

#
# Unregister variable $varname from $SESSION_VARS array
#
function xSESSION_unregister($varname, $unset_global=false) 
{
	global $SESSION_VARS, $SESSION_UNPACKED_VARS;
	global $useSESSIONs_type;

	if (empty($varname))
		return false;

	if ($useSESSIONs_type < 3) {
#
# Using standard PHP sessions
#
		session_unregister($varname);
		return;
	}

	unset($SESSION_VARS[$varname]);
	unset($SESSION_UNPACKED_VARS[$varname]);
	if ($unset_global) {
		unset($GLOBALS[$varname]);
		unset($GLOBALS[$varname]);
	}

}

#
# Find out whether a global variable $varname is registered in 
# $SESSION_VARS array
#
function xSESSION_is_registered($varname) 
{
	global $SESSION_VARS;
	global $useSESSIONs_type;

	if (empty($varname))
		return false;

	if ($useSESSIONs_type < 3) {
#
# Using standard PHP sessions
#
		return session_is_registered($varname);
	}

	return isset($SESSION_VARS[$varname]);

}


?>
