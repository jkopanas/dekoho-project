<?php
include("init.php");
$current_module = $loaded_modules['products'];
$smarty->assign("current_module",$current_module);
$Content = new Items(array('module'=>$current_module));
if ($_POST['comment'] AND is_numeric(ID)) {
	$id = $_POST['id'];
	$t = new textparse();
	$comment = $t->formtpa($_POST['comment']);
	$time = time();
	$uid = ID;
	$status = ($current_module['settings']['moderate_comments'] == 1) ? 0 : 1; 
	$sql->db_Insert("users_comments","$id,'products','$comment',$uid,$time,'products',$status");
	if ($loaded_modules['users']['settings']['user_points']) {
		add_points($user_points_lists,array('uid' => ID,'module'=>'products','action'=>'add_comment','id'=>$id));
		$smarty->assign("USER_POINTS",get_users_points(array('uid'=>ID,'quick'=>1,'debug'=>0)));
	}
	header("Location: /products_$id.html#comments");
	exit();
}


if ($_GET['id']) {

if (!is_numeric($_GET['id'])) {
	$sql->db_Select("products","id","permalink = '".$_GET['id']."'");
	$tmp = execute_single($sql);
	$id = $tmp['id'];
}
else {
	$id = $_GET['id'];
}
	if ($loaded_modules['users']['settings']['user_points'] AND is_numeric(ID)) {
		add_points($user_points_lists,array('uid' => ID,'module'=>'products','action'=>'view_page','id'=>$id));
		$smarty->assign("USER_POINTS",get_users_points(array('uid'=>ID,'quick'=>1,'debug'=>0)));
	}


$item_settings = array('fields'=>'*','thumb'=>1,'CatNav'=>1,'debug'=>0,'active'=>1,'cat_details'=>1,'main'=>1,'images'=>1,'efields'=>1,'parse'=>1);
if (FRONT_LANG != DEFAULT_LANG) {
	$item_settings['translate'] = FRONT_LANG;
}
		if ($current_module['settings']['use_provider']) 
		{
			$smarty->assign("providers",get_providers(DEFAULT_LANG));
			$item_settings['get_provider'] = 1;
		}
		if ($current_module['settings']['use_content_tags']) 
		{
			$item_settings['tags'] = 1;
		}

	$item = $Content->GetItem($id,$item_settings);

	$item['related'] = $Content->RelatedContent($id,array('debug'=>0,'orderby'=>'orderby','get_results'=>1));
	$current_category = $item['category'];
    $ImagesStuff = new ItemImages(array('module'=>$current_module,'itemid'=>$id));
    $imageCategories = $ImagesStuff->ImageCategories();
    if ($imageCategories) {
	    foreach ($imageCategories as $v) {
	        $tmp[$v['title']] = $v;
	    }
    }
   
    $imageCategories = $tmp;
        $Docs = new ItemMedia(array('module' => $current_module,'itemid' => $id,'debug'=>0,'orderby' => 'orderby','available'=>1));
    $smarty->assign("docs",$Docs->ItemDocuments());
$smarty->assign('nav',$item['nav']);
$smarty->assign("cat",$current_category);
$smarty->assign("AllImageCategories",$imageCategories);
$smarty->assign("item",$item);
$ItemCat = ($item['main_category']['categoryid']) ? $item['main_category']['categoryid'] : $item['main_category']['catid'];
$smarty->assign("more_items",$Content->LatestItems(array('results_per_page'=>10,'active'=>1,'get_provider'=>0,'get_user'=>0,'debug'=>0,'just_results'=>1,'thumb'=>1
		,'categoryid'=>$ItemCat,'exclude'=>$item['id'],'JustItems'=>1,'efields'=>0)));
		
		############### HOOKS ##############
		if ($current_category['settings']['hook_module']) {
			$tmp = explode("|",$current_category['settings']['hook_module']);
			foreach ($tmp as $v)
			{
				if (include_once($_SERVER['DOCUMENT_ROOT']."/$v")) {
					
				}
			}
			$smarty->assign("hooks",$hooks );
		}
		
######################## LOAD THEME ###################
if (array_key_exists('themes',$loaded_modules)) 
{
	if (!$_GET['theme']) {
		$_GET['theme'] = $current_category['settings']['theme'];
	}
	$active_theme = load_theme_by_name($_GET['theme'],$theme_module['settings']);
	$theme_settings = json_decode($active_theme['settings'],true);
	if ($theme_settings['content_theme']) {
		$includeFile = $theme_settings['content_theme'];
		$content_theme = load_theme_by_name(str_replace(".tpl",'',$theme_settings['content_theme']),1);
		$smarty->assign("theme_settings",json_decode($content_theme['settings'],true));
	}
	else {
		$smarty->assign("theme_settings",$theme_settings);
	}

}//END OF content MODULE

$l = new siteModules();
$layout = $l->pageBoxes('all',e_FILE,'front',array('getBoxes'=>'full','fields'=>'boxes,areas,id,settings','boxFields'=>'id,title,name,settings,fetchType,file,required_modules,template','init'=>1,'boxFilters'=>array('active'=>1),'debug'=>0));
$smarty->assign("layout",$layout['boxes']);
}//END ID
else {
	header("Location: index.html");
	exit();
}


    
$smarty->assign("CURRENT_MODULE",$current_module);
$display = ($includeFile) ? $loaded_modules['themes']['folder']."/".$includeFile : $loaded_modules['themes']['folder']."/".$active_theme['file'];

$smarty->assign("include_file",$display);//assigned template variable include_file
$smarty->display("home.tpl",$url);//Display the home.tpl template
?>