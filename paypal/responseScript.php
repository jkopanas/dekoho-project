<?php
require_once('../init.php');
require_once('class/config.php');

global $sql,$smarty;

$post = $_POST['post'];

$action = ( $_POST['action'] == "hotel" ) ? "hotel" : "rooms";


$sql->db_Select("orders_types","settings","module='".$action."'");
$res = execute_single($sql);
$values = json_decode($res['settings'],true);

$sql->db_Select("orders","id,price,settings,itemid,module,uid,billing_profile_id","transaction_id=".$_GET['transactionid']);
$res_orders = execute_multi($sql);
$price=0;
foreach($res_orders as $key => $value) {
	$price=$price + $value['price'];
}

//Parameters returned from JCC
$jccMerID = $post['MerID'];
$jccAcqID = $post['AcqID'];
$jccOrderID = $post['OrderID'];
$jccResponseCode = intval($post['ResponseCode']);
$jccReasonCode = intval($post['ReasonCode']);
$jccReasonDescr = $post['ReasonCodeDesc'];
$jccRef = $post['ReferenceNo'];
$jccPaddedCardNo = $post['PaddedCardNo'];
$jccSignature = $post['Signature'];
//Authorization code is only returned in case of successful transaction, indicated with a value of 1
//for both response code and reason code

if ($jccResponseCode==1 && $jccReasonCode==1){
	$jccAuthNo = $post['AuthCode'];
}
//The parameters used for creating the JCC signature as stored on the merchant server
$password = "4iko87rC";

$merchantID = "0099581019";
$acquirerID = "402971";
$orderID = $_GET['transactionid'];

$toEncrypt = $password.$merchantID.$acquirerID.$orderID;
//Produce the hash using SHA1
//This will give fed389f2e634fa6b62bdfbfafd05be761176cee9
$sha1Signature = sha1($toEncrypt);

//Encode the signature using Base64
//This will give /tOJ8uY0+mtivfv6/QW+dhF2zuk=
$expectedBase64Sha1Signature = base64_encode(pack("H*",$sha1Signature));
//JCC signature verification is performed simply by comparing the signature we produced with the one sent from JCC
$verifyJCCSignature = ($expectedBase64Sha1Signature == $jccSignature);
?>

<?php
if ($jccResponseCode==1 && $jccReasonCode==1){


$sql->db_Update("orders", "txn_id='".$post['ReferenceNo']."', status=1, payer_id='".$jccAuthNo."' where transaction_id='".$_GET['transactionid']."'");
//echo "orders", "txn_id='".$post['ReferenceNo']."', status=1, payer_id='".$jccAuthNo."' where transaction_id='".$_GET['transactionid']."'";
foreach($res_orders as $key =>$value) {
	$set=json_decode($value['settings'],true);
	$sql->db_Update("rooms_hotel", "start_discount_date='".$set['start_date']."', end_discount_date='".$set['end_date']."', discount='".$set['discount']."' where room_id=".$value['itemid']);
	//echo "rooms_hotel", "start_discount_date='".$set['start_date']."', end_discount_date='".$set['end_date']."', discount='".$set['discount']."' where room_id=".$value['itemid'];
}



$tmpObj = new Hotelier();
$settings['search'][] = array( "users.id" => $res_orders[0]['uid'],"users_profile_billing.id" => $res_orders[0]['billing_profile_id'] );
$settings['fields']= array("users_profile_billing.*");
$settings['profile'] =0;
$settings['debug']=0;
$settings['JoinInner'][] = array("name"=>"users_profile_billing","idKey"=>"users_profile_billing.uid","idRel"=>"users.id","joinType"=>"INNER JOIN");
$tmpArr = $tmpObj->GetHotelierBillingProfile($settings);


$settings = array();
$settings['filters'] = array(
		'sort_field' 	=> 'id',
		'sort_direction'=> 'DESC'
);
$settings['overwriteJoin']=array();

foreach($res_orders as $key => $value) {

	$settings['debug']=0;
	$settings['searchfilters'][] = array(
			'item'     => "id",
			'type'     => "eq",
			'val'      => $value['itemid'],
	);

	if ($value['module'] == "rooms") {
		$tmpObj = new Rooms();
		$tmp=$tmpObj->getRoom($settings);
	} else {
		$tmpObj = new Hotels ();
		$tmp = $tmpObj->getHotel ( $settings );
	}
	$res_orders[$key]['item']=$tmp['results'][0];
	$total=$total+$value['price'];
}

$smarty->assign("item",$res_orders);
$smarty->assign("profile",$tmpArr[0]);
$smarty->assign("total",$total);

$content = $smarty->fetch("hooks/hotelier/tpls/print_order.tpl");

$file = ABSPATH."/orders/order-".$res_orders[0]['uid']."-".$_GET['transactionid'].".tpl";
file_put_contents($file, $content);

echo json_encode(array("msg" => "Your have one new Offer. Continue here" ),true);

} else {

echo json_encode(array("msg" => "Your payment for the Offer failed Open a ticket. Continue here" ),true);
}

exit;
?>