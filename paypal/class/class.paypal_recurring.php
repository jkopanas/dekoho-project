<?php

class paypal_recurring
{
	
	public $ReturnRequest = array();
	

function setExpressCheckout()
{

$environment=$this->environment;
$API_UserName=$this->API_UserName;
$API_Password=$this->API_Password;
$API_Signature=$this->API_Signature;
$API_Endpoint=$this->API_Endpoint;
$paymentAmount=$this->paymentAmount;
$currencyID=$this->currencyID;
$paymentType=$this->paymentType;
$returnURL=$this->returnURL;
$cancelURL=$this->cancelURL;
$startDate=$this->startDate;
$orderDetails=$this->orderDetails;
// Add request-specific fields to the request string.
//
//LANDINGPAGE=billing&SOLUTIONTYPE=Sole&

$nvpStr = "&AMT=$paymentAmount&ReturnUrl=$returnURL&CANCELURL=$cancelURL.&PAYMENTACTION=$paymentType&CURRENCYCODE=$currencyID".$orderDetails;

// Execute the API operation; see the PPHttpPost function above.
$httpParsedResponseAr = $this->fn_setExpressCheckout('SetExpressCheckout', $nvpStr);


//print_ar($httpParsedResponseAr);
if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
	// Redirect to paypal.com.
	$token = urldecode($httpParsedResponseAr["TOKEN"]);
	$payPalURL = "https://www.paypal.com/webscr&cmd=_express-checkout&token=$token";
	if("sandbox" === $environment || "beta-sandbox" === $environment) {
		$payPalURL = "https://www.$environment.paypal.com/webscr&cmd=_express-checkout&useraction=commit&token=$token";
	}
	
	echo json_encode($payPalURL,true);
	exit;
} else  {
	exit('SetExpressCheckout failed: ' . print_r($httpParsedResponseAr, true));
}



}


function getExpressCheckout()
{
$environment=$this->environment;
$API_UserName=$this->API_UserName;
$API_Password=$this->API_Password;
$API_Signature=$this->API_Signature;
$API_Endpoint=$this->API_Endpoint;
$paymentAmount=$this->paymentAmount;
$currencyID=$this->currencyID;
$paymentType=$this->paymentType;
$returnURL=$this->returnURL;
$cancelURL=$this->cancelURL;
$startDate=$this->startDate;
// Obtain the token from PayPal.
if(!array_key_exists('token', $_REQUEST)) {
	exit('Token is not received.');
}

// Set request-specific fields.
$token = urlencode(htmlspecialchars($_REQUEST['token']));

// Add request-specific fields to the request string.
$nvpStr = "&TOKEN=".$token;


// Execute the API operation; see the PPHttpPost function above.
$httpParsedResponseAr = $this->fn_getExpressCheckout('GetExpressCheckoutDetails', $nvpStr);

if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
	// Extract the response details.
	$payerID = $httpParsedResponseAr['PAYERID'];
	$street1 = $httpParsedResponseAr["SHIPTOSTREET"];
	if(array_key_exists("SHIPTOSTREET2", $httpParsedResponseAr)) {
		$street2 = $httpParsedResponseAr["SHIPTOSTREET2"];
	}
	$city_name = $httpParsedResponseAr["SHIPTOCITY"];
	$state_province = $httpParsedResponseAr["SHIPTOSTATE"];
	$postal_code = $httpParsedResponseAr["SHIPTOZIP"];
	$country_code = $httpParsedResponseAr["SHIPTOCOUNTRYCODE"];

  return $this->doExpressCheckout($payerID,$token);

//	exit('Get Express Checkout Details Completed Successfully: '.print_r($httpParsedResponseAr, true));
} else  {
	exit('GetExpressCheckoutDetails failed: ' . print_r($httpParsedResponseAr, true));
}


}





function doExpressCheckout($payerID,$token)
{
$environment=$this->environment;
$API_UserName=$this->API_UserName;
$API_Password=$this->API_Password;
$API_Signature=$this->API_Signature;
$API_Endpoint=$this->API_Endpoint;
$paymentAmount=$this->paymentAmount;
$currencyID=$this->currencyID;
$paymentType=$this->paymentType;
$returnURL=$this->returnURL;
$cancelURL=$this->cancelURL;
$notifyURL = $this->notifyURL;
$profileReference=$this->profileReference;

//&BILLINGAGREEMENTDESCRIPTION=paopao&BILLINGTYPE=RecurringPayments
// Add request-specific fields to the request string.
//PAYMENTREQUEST_0_AMT
$nvpStr = "&TOKEN=$token&PAYERID=$payerID&PROFILEREFERENCE=$profileReference&PAYMENTACTION=$paymentType&AMT=$paymentAmount&NOTIFYURL=$notifyURL&CURRENCYCODE=$currencyID";

// Execute the API operation; see the PPHttpPost function above.
$httpParsedResponseAr = $this->fn_doExpressCheckout('DoExpressCheckoutPayment', $nvpStr);
if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
	
	if ($this->RecurringPayment) {

		$this->ReturnRequest['doExpressCheckout'] = $httpParsedResponseAr;
		$this->ReturnRequest['doExpressCheckout']['PAYER_ID']=$payerID;
		$this->createRecurringPaymentsProfile($token);
		return $this->ReturnRequest['doExpressCheckout'];
	}
	//print_ar($httpParsedResponseAr);
	$httpParsedResponseAr['PAYER_ID']=$payerID;
	return $httpParsedResponseAr;
	exit('Express Checkout Payment Completed Successfully: '.print_r($httpParsedResponseAr, true));
} else  {
	exit('DoExpressCheckoutPayment failed: ' . print_r($httpParsedResponseAr, true));
}
}

function createRecurringPaymentsProfile($token)
{

$environment=$this->environment;
$API_UserName=$this->API_UserName;
$API_Password=$this->API_Password;
$API_Signature=$this->API_Signature;
$API_Endpoint=$this->API_Endpoint;
$paymentAmount=$this->paymentAmount;
$currencyID=$this->currencyID;
$paymentType=$this->paymentType;
$returnURL=$this->returnURL;
$cancelURL=$this->cancelURL;
$startDate=$this->startDate;
$billingPeriod=$this->billingPeriod;
$billingFreq=$this->billingFreq;
$profileReference=$this->profileReference;


$token = $_REQUEST['token'];


$nvpStr="&TOKEN=$token&AMT=$paymentAmount&PROFILEREFERENCE=$profileReference&DESC=".$this->desc."&MAXFAILEDPAYMENTS=3&AUTOBILLOUTAMT=AddToNextBilling&CURRENCYCODE=$currencyID&PROFILESTARTDATE=$startDate";
$nvpStr .= "&BILLINGPERIOD=$billingPeriod&BILLINGFREQUENCY=$billingFreq";

/*
&TOKEN=EC-6LN63512BP279294S&AMT=5&PROFILEREFERENCE=1376417688&DESC=HotelMembership&AUTOBILLOUTAMT=AddToNextBilling&CURRENCYCODE=USD&PROFILESTARTDATE=2013-08-13T21:15:16+03:00&BILLINGPERIOD=Year&BILLINGFREQUENCY=1
*/
//echo $nvpStr;

$httpParsedResponseAr = $this->fn_createRecurringPaymentsProfile('CreateRecurringPaymentsProfile', $nvpStr);

if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
	
	$this->ReturnRequest['createRecurring'] = $httpParsedResponseAr;
	
	//echo json_encode($this->ReturnRequest,true);
	return $this->ReturnRequest;
	exit();
} else  {
	echo json_encode($httpParsedResponseAr,true);
	exit();
}

}



function fn_createRecurringPaymentsProfile($methodName_, $nvpStr_)
{
	
$environment=$this->environment;
$API_UserName=$this->API_UserName;
$API_Password=$this->API_Password;
$API_Signature=$this->API_Signature;
//$API_Endpoint="https://api-3t.sandbox.paypal.com/nvp";
$API_Endpoint=$this->API_Endpoint;
$paymentAmount=$this->paymentAmount;
$currencyID=$this->currencyID;
$paymentType=$this->paymentType;
$returnURL=$this->returnURL;
$cancelURL=$this->cancelURL;
$startDate=$this->startDate;
$version = urlencode('95.0');

	if("sandbox" === $environment || "beta-sandbox" === $environment) {
		$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
	}

	// setting the curl parameters.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);

	// turning off the server and peer verification(TrustManager Concept).
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);

	// NVPRequest for submitting to server
	$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

	// setting the nvpreq as POST FIELD to curl
	curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

	// getting response from server
	$httpResponse = curl_exec($ch);

	if(!$httpResponse) {
		exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
	}

	// Extract the RefundTransaction response details
	$httpResponseAr = explode("&", $httpResponse);

	$httpParsedResponseAr = array();
	foreach ($httpResponseAr as $i => $value) {
		$tmpAr = explode("=", $value);
		if(sizeof($tmpAr) > 1) {
			$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
		}
	}

	if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
		exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
	}

	return $httpParsedResponseAr;
}




function fn_setExpressCheckout($methodName_, $nvpStr_) {

$environment=$this->environment;
$API_UserName=$this->API_UserName;
$API_Password=$this->API_Password;
$API_Signature=$this->API_Signature;
$API_Endpoint=$this->API_Endpoint;
$paymentAmount=$this->paymentAmount;
$currencyID=$this->currencyID;
$paymentType=$this->paymentType;
$returnURL=$this->returnURL;
$cancelURL=$this->cancelURL;
$startDate=$this->startDate;

	if("sandbox" === $environment || "beta-sandbox" === $environment) {
		$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
	}
	$version = urlencode('95.0');
	// Set the curl parameters.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);

	// Turn off the server and peer verification (TrustManager Concept).
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);

	// Set the API operation, version, and API signature in the request.
	$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
	// Set the request as a POST FIELD for curl.
	curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

	// Get response from the server.
	$httpResponse = curl_exec($ch);

	if(!$httpResponse) {
		exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
	}

	// Extract the response details.
	$httpResponseAr = explode("&", $httpResponse);

	$httpParsedResponseAr = array();
	foreach ($httpResponseAr as $i => $value) {
		$tmpAr = explode("=", $value);
		if(sizeof($tmpAr) > 1) {
			$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
		}
	}

	if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
		exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
	}

	return $httpParsedResponseAr;
}





function fn_getExpressCheckout($methodName_, $nvpStr_)
{
$environment=$this->environment;
$API_UserName=$this->API_UserName;
$API_Password=$this->API_Password;
$API_Signature=$this->API_Signature;
$API_Endpoint=$this->API_Endpoint;
$paymentAmount=$this->paymentAmount;
$currencyID=$this->currencyID;
$paymentType=$this->paymentType;
$returnURL=$this->returnURL;
$cancelURL=$this->cancelURL;
$startDate=$this->startDate;
	if("sandbox" === $environment || "beta-sandbox" === $environment) {
		$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
	}
	$version = urlencode('95.0');

	// Set the curl parameters.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);

	// Turn off the server and peer verification (TrustManager Concept).
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);

	// Set the API operation, version, and API signature in the request.
	$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

	// Set the request as a POST FIELD for curl.
	curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

	// Get response from the server.
	$httpResponse = curl_exec($ch);

	if(!$httpResponse) {
		exit('$methodName_ failed: '.curl_error($ch).'('.curl_errno($ch).')');
	}

	// Extract the response details.
	$httpResponseAr = explode("&", $httpResponse);

	$httpParsedResponseAr = array();
	foreach ($httpResponseAr as $i => $value) {
		$tmpAr = explode("=", $value);
		if(sizeof($tmpAr) > 1) {
			$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
		}
	}

	if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
		exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
	}

	return $httpParsedResponseAr;

}


function fn_doExpressCheckout($methodName_, $nvpStr_)
{
$environment=$this->environment;
$API_UserName=$this->API_UserName;
$API_Password=$this->API_Password;
$API_Signature=$this->API_Signature;
$API_Endpoint=$this->API_Endpoint;
$paymentAmount=$this->paymentAmount;
$currencyID=$this->currencyID;
$paymentType=$this->paymentType;
$returnURL=$this->returnURL;
$cancelURL=$this->cancelURL;
$startDate=$this->startDate;

	if("sandbox" === $environment || "beta-sandbox" === $environment) {
		$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
	}
	$version = urlencode('95.0');

	// setting the curl parameters.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);

	// Set the curl parameters.
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);

	// Set the API operation, version, and API signature in the request.
	$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

	// Set the request as a POST FIELD for curl.
	curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

	// Get response from the server.
	$httpResponse = curl_exec($ch);

	if(!$httpResponse) {
		exit('$methodName_ failed: '.curl_error($ch).'('.curl_errno($ch).')');
	}

	// Extract the response details.
	$httpResponseAr = explode("&", $httpResponse);

	$httpParsedResponseAr = array();
	foreach ($httpResponseAr as $i => $value) {
		$tmpAr = explode("=", $value);
		if(sizeof($tmpAr) > 1) {
			$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
		}
	}

	if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
		exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
	}

	return $httpParsedResponseAr;
}

}

?>