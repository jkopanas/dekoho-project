<?php
// Include required library files.
require_once('class/config.php');
require_once('class/paypal.class.php');

// Create PayPal object.
$PayPalConfig = array('Sandbox' => $sandbox, 'APIUsername' => $api_username, 'APIPassword' => $api_password, 'APISignature' => $api_signature);
$PayPal = new PayPal($PayPalConfig);

// Pass data into class for processing with PayPal and load the response array into $PayPalResult
//I-AFN3DLYTMBHD
//I-SEHR406J0KYS ---- 22 tou minos latest
//I-NSJTEFWU1UB6 --- 22 tou minos
//I-4YP71KEGEXVY 25 tou minos 
$PayPalResult = $PayPal->GetRecurringPaymentsProfileStatus($_GET['id']);

// Write the contents of the response array to the screen for demo purposes.
echo '<pre />';
print_r($PayPalResult);
?>