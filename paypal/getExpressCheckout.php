<?php
require_once('../init.php');
require_once('class/config.php');
include "class/class.paypal_recurring.php";

global $sql,$smarty;



$obj=new paypal_recurring;

$obj->environment =  $mode;  // or 'beta-sandbox' or 'live'
$obj->paymentType = urlencode(($action == "hotel" ) ? 'Authorization': 'Sale');		// or 'Sale' or 'Order'

//Get From DB settings
$action = ( $_POST['action'] == "hotel" ) ? "hotel" : "rooms";

$sql->db_Select("orders_types","settings","module='".$action."'");
$res = execute_single($sql);
$values = json_decode($res['settings'],true);

$sql->db_Select("orders","id,price,settings,itemid,module,uid,billing_profile_id","transaction_id=".$_GET['transactionid']);
$res_orders = execute_multi($sql);
$price=0;
foreach($res_orders as $key => $value) {
	$price=$price + $value['price']; 
}

$obj->startDate = date(DATE_ATOM);
$obj->billingPeriod = urlencode($values['period']);				// or "Day", "Week", "SemiMonth", "Year"
$obj->billingFreq = urlencode($values['interval']);				// combination of this and billingPeriod must be at most a year
$obj->paymentAmount = urlencode($price);
$obj->currencyID = urlencode($values['currency']);			// or other currency code ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
$obj->profileReference = $_GET['transactionid'];
/* PAYPAL API  DETAILS */
$obj->API_UserName = urlencode($api_username);
$obj->API_Password = urlencode($api_password);
$obj->API_Signature = urlencode($api_signature);
$obj->API_Endpoint= "https://api-3t.paypal.com/nvp";
$obj->RecurringPayment = ($action == "hotel") ? true : false;
if ($obj->RecurringPayment) { $obj->desc="HotelMembership"; }
/*SET SUCCESS AND FAIL URL*/
$obj->returnURL = urlencode($domain."en/hotelier/index.html#payment/Checkout/getExpressCheckout/".$action."/".$_GET['transactionid']);
$obj->cancelURL = urlencode($domain."en/hotelier/index.html#payment/error/".$action."/".$_GET['transactionid']);
$obj->notifyURL = urlencode($domain.'paypal/ipn.php');

$response=$obj->getExpressCheckout();

//$sql->db_Select("orders","settings,itemid,module"," transaction_id=".$_GET['transactionid']);
//$res=execute_multi($sql);

if ( $response['PAYMENTSTATUS'] == "Completed" ) {

	if ($action != "hotel" ) {
	$sql->db_Update("orders", "txn_id='".$response['TRANSACTIONID']."', status=1, payer_id='".$response['PAYER_ID']."' where and status=0 and uid=".ID." and transaction_id='".$_GET['transactionid']."'");
		
		foreach($res_orders as $key =>$value) {
			$set=json_decode($value['settings'],true);
			$sql->db_Update("rooms_hotel", "start_discount_date='".$set['start_date']."', end_discount_date='".$set['end_date']."', discount='".$set['discount']."' where room_id=".$value['itemid']);
		}
		echo json_encode(array("msg" => "Your have one new Offer. Continue here" ),true);
		
	} else {
		$sql->db_Update("orders", "txn_id='".$response['TRANSACTIONID']."',recurring_id='".urldecode($obj->ReturnRequest['createRecurring']['PROFILEID'])."', status=1, payer_id='".$response['PAYER_ID']."' where  status=0 and uid=".ID." and transaction_id='".$_GET['transactionid']."'");	
		echo json_encode(array("msg" => "Your have one new Hotel. Continue here" ),true);
		
	}
} else if ( $response['PAYMENTSTATUS'] == "Pending" ) {
	

	if ($action == "hotel" ) {
		$sql->db_Update("orders", "txn_id='".$response['TRANSACTIONID']."',recurring_id='".urldecode($obj->ReturnRequest['createRecurring']['PROFILEID'])."', status=2, payer_id='".$response['PAYER_ID']."' where status=0 and uid=".ID." and transaction_id='".$_GET['transactionid']."'");	
		echo json_encode(array("msg" => "Your payment status for the new hotel is <b>pending</b>. Until is completed go to" ),true);
	} else {
		$sql->db_Update("orders", "txn_id='".$response['TRANSACTIONID']."', status=2, payer_id='".$response['PAYER_ID']."' where transaction_id='".$_GET['transactionid']."'");
		echo json_encode(array("msg" => "Your payment status for the new offer is <b>pending</b>. Until is completed go to" ),true);
	}
}

if ( $response['PAYMENTSTATUS'] == "Complete" ||  $response['PAYMENTSTATUS'] == "Pending" ) {


$tmpObj = new Hotelier();
$settings['search'][] = array( "users.id" => $res_orders[0]['uid'],"users_profile_billing.id" => $res_orders[0]['billing_profile_id'] );
$settings['fields']= array("users_profile_billing.*");
$settings['profile'] =0;
$settings['debug']=0;
$settings['JoinInner'][] = array("name"=>"users_profile_billing","idKey"=>"users_profile_billing.uid","idRel"=>"users.id","joinType"=>"INNER JOIN");
$tmpArr = $tmpObj->GetHotelierBillingProfile($settings);


$settings = array();
$settings['filters'] = array(
		'sort_field' 	=> 'id',
		'sort_direction'=> 'DESC'
);
$settings['overwriteJoin']=array();

foreach($res_orders as $key => $value) {
		
	$settings['debug']=0;
	$settings['searchfilters'][] = array(
			'item'     => "id",
			'type'     => "eq",
			'val'      => $value['itemid'],
	);

	if ($value['module'] == "rooms") {
		$tmpObj = new Rooms();
		$tmp=$tmpObj->getRoom($settings);
	} else {
		$tmpObj = new Hotels ();
		$tmp = $tmpObj->getHotel ( $settings );
	}
	
	
	$res_orders[$key]['item']=$tmp['results'][0];
	$total=$total+$value['price'];
}

$smarty->assign("item",$res_orders);
$smarty->assign("profile",$tmpArr[0]);
$smarty->assign("total",$total);

$content = $smarty->fetch("hooks/hotelier/tpls/print_order.tpl");

$file = ABSPATH."/orders/order-".$res_orders[0]['uid']."-".$response['TRANSACTIONID'].".tpl";
file_put_contents($file, $content);
}
exit;
?>