<?php
/**
 *  PHP-PayPal-IPN Example
 *
 *  This shows a basic example of how to use the IpnListener() PHP class to 
 *  implement a PayPal Instant Payment Notification (IPN) listener script.
 *
 *  For a more in depth tutorial, see my blog post:
 *  http://www.micahcarrick.com/paypal-ipn-with-php.html
 *
 *  This code is available at github:
 *  https://github.com/Quixotix/PHP-PayPal-IPN
 *
 *  @package    PHP-PayPal-IPN
 *  @author     Micah Carrick
 *  @copyright  (c) 2011 - Micah Carrick
 *  @license    http://opensource.org/licenses/gpl-3.0.html
 */
 
 
/*
Since this script is executed on the back end between the PayPal server and this
script, you will want to log errors to a file or email. Do not try to use echo
or print--it will not work! 

Here I am turning on PHP error logging to a file called "ipn_errors.log". Make
sure your web server has permissions to write to that file. In a production 
environment it is better to have that log file outside of the web root.
*/
ini_set('log_errors', true);
ini_set('error_log', dirname(__FILE__).'/ipn_errors.log');

require_once('../init.php');
require_once('class/config.php');
// instantiate the IpnListener class

global $sql,$smarty;

include('class/ipnlistener.php');
$listener = new IpnListener();


/*
When you are testing your IPN script you should be using a PayPal "Sandbox"
account: https://developer.paypal.com
When you are ready to go live change use_sandbox to false.
*/
$listener->use_sandbox = false;

/*
By default the IpnListener object is going  going to post the data back to PayPal
using cURL over a secure SSL connection. This is the recommended way to post
the data back, however, some people may have connections problems using this
method. 

To post over standard HTTP connection, use:
$listener->use_ssl = false;

To post using the fsockopen() function rather than cURL, use:
$listener->use_curl = false;
*/

/*
The processIpn() method will encode the POST variables sent by PayPal and then
POST them back to the PayPal server. An exception will be thrown if there is 
a fatal error (cannot connect, your server is not configured properly, etc.).
Use a try/catch block to catch these fatal errors and log to the ipn_errors.log
file we setup at the top of this file.

The processIpn() method will send the raw data on 'php://input' to PayPal. You
can optionally pass the data to processIpn() yourself:
$verified = $listener->processIpn($my_post_data);
*/


try {
    $listener->requirePostMethod();
    $verified = $listener->processIpn();
} catch (Exception $e) {
	mail('gkopanas@hotmail.com', 'Invalid IPN1', $e->getMessage());
    //error_log($e->getMessage());
    exit(0);
}

/*
The processIpn() method returned true if the IPN was "VERIFIED" and false if it
was "INVALID".
*/
if ($verified) {
	
	$ar=array("nothing" => true);
    /*
    Once you have a verified IPN you need to do a few more checks on the POST
    fields--typically against data you stored in your database during when the
    end user made a purchase (such as in the "success" page on a web payments
    standard button). The fields PayPal recommends checking are:
    
        1. Check the $_POST['payment_status'] is "Completed"
	    2. Check that $_POST['txn_id'] has not been previously processed 
	    3. Check that $_POST['receiver_email'] is your Primary PayPal email 
	    4. Check that $_POST['payment_amount'] and $_POST['payment_currency'] 
	       are correct
    
    Since implementations on this varies, I will leave these checks out of this
    example and just send an email using the getTextReport() method to get all
    of the details about the IPN.  
    */
	
	$response=$listener->getDataArray();
	//$response=array("txn_type" =>"recurring_payment","recurring_payment_id" =>"I-E1SUKPPFKXH2","payment_status" =>"Completed","txn_id" => "4254543562");
	
	if ( $response['txn_type'] == "express_checkout" ) {
	
	    $sql->db_Select("orders","settings,itemid,module","txn_id='".$response['txn_id']."' and status=2 and payer_id='".$response['payer_id']."'");
	    $res=execute_multi($sql);
	
	    if ($res[0] && $response['payment_status'] == "Completed" ) {
	    	
	    	$ar=array("completed" => 1);
		    $sql->db_Update("orders", " status=1", "where status=2 and txn_id ='".$_GET['txn_id']."'");
		    
		    if ( $res[0]['module'] != "hotel" ) {
		    	foreach($res as $key =>$value) {
		    		$set=json_decode($value['settings'],true);
		    		$sql->db_Update("rooms_hotel", "start_discount_date='".$set['start_date']."', end_discount_date='".$set['end_date']."', discount='".$set['discount']."' where room_id=".$value['itemid']);
		    	}
		    }
	    } elseif ( $response['payment_status'] == "Refunded" ) {
	    	$ar=array("Refunded" => 1);
	    	$sql->db_Select("orders","itemid,module","txn_id='".$response['txn_id']."'");
	    	$room=execute_multi($sql);
	    
	    	$sql->db_Update("orders", " status=4", "where (status=2 or status=1) and txn_id ='".$response['txn_id']."'"); 	
	    	$sql->db_Update("rooms_hotel", " discount=0,start_discount_date=0,end_discount_date=0 where room_id=".$room['itemid']);
	    }
    
	} elseif ( $response['txn_type'] == "recurring_payment" ) {
		$curtime = explode(".",microtime(true));
		
		if ($response['payment_status'] == "Completed" ) {
			$sql->q("INSERT INTO orders (transaction_id,recurring_id,txn_id,payer_id,uid,itemid,typeid,module,billing_profile_id,payment_method,status,date_added,price,archive,autorenew,settings) SELECT transaction_id,recurring_id,'".$response['txn_id']."',payer_id,uid,itemid,typeid,module,billing_profile_id,payment_method,status,".$curtime[0].",price,archive,autorenew,settings  FROM orders WHERE recurring_id = '".$response['recurring_payment_id']."' limit 1");
			$debugstr = "INSERT INTO orders (transaction_id,recurring_id,txn_id,payer_id,uid,itemid,typeid,module,billing_profile_id,payment_method,status,date_added,price,archive,autorenew,settings) SELECT transaction_id,recurring_id,'".$response['txn_id']."',payer_id,uid,itemid,typeid,module,billing_profile_id,payment_method,2,".$curtime[0].",price,archive,autorenew,settings  FROM orders WHERE recurring_id = '".$response['recurring_payment_id']."' limit 1";
		} else {
			$sql->q("INSERT INTO orders (transaction_id,recurring_id,txn_id,payer_id,uid,itemid,typeid,module,billing_profile_id,payment_method,status,date_added,price,archive,autorenew,settings) SELECT transaction_id,recurring_id,'".$response['txn_id']."',payer_id,uid,itemid,typeid,module,billing_profile_id,payment_method,2,".$curtime[0].",price,archive,autorenew,settings  FROM orders WHERE recurring_id = '".$response['recurring_payment_id']."' limit 1");
			$debugstr = "INSERT INTO orders (transaction_id,recurring_id,txn_id,payer_id,uid,itemid,typeid,module,billing_profile_id,payment_method,status,date_added,price,archive,autorenew,settings) SELECT transaction_id,recurring_id,'".$response['txn_id']."',payer_id,uid,itemid,typeid,module,billing_profile_id,payment_method,2,".$curtime[0].",price,archive,autorenew,settings  FROM orders WHERE recurring_id = '".$response['recurring_payment_id']."' limit 1";
		}

		$sql->db_Select("orders","id,price,settings,itemid,module,uid,billing_profile_id,transaction_id,txn_id","recurring_id='".$response['recurring_payment_id']."'");
		$res_orders = execute_single($sql);
		$tmpObj = new Hotelier();
		$settings['search'][] = array( "users.id" => $res_orders['uid'],"users_profile_billing.id" => $res_orders['billing_profile_id'] );
		$settings['fields']= array("users_profile_billing.*");
		$settings['profile'] =0;
		$settings['debug']=0;
		$settings['JoinInner'][] = array("name"=>"users_profile_billing","idKey"=>"users_profile_billing.uid","idRel"=>"users.id","joinType"=>"INNER JOIN");
		$tmpArr = $tmpObj->GetHotelierBillingProfile($settings);
		
		
		$settings = array();
		$settings['filters'] = array(
				'sort_field' 	=> 'id',
				'sort_direction'=> 'DESC'
		);
		$settings['overwriteJoin']=array();

		
			$settings['debug']=0;
			$settings['searchfilters'][] = array(
					'item'     => "id",
					'type'     => "eq",
					'val'      => $res_orders['itemid'],
			);
	
			$tmpObj = new Hotels ();
			$tmp = $tmpObj->getHotel ( $settings );
			$res_orders['item']=$tmp['results'][0];

		
		$smarty->assign("item",array($res_orders));
		$smarty->assign("profile",$tmpArr[0]);
		$smarty->assign("total",$res_orders['price']);
		
		$smarty->assign("debug",$debugstr);
		
		
		$content = $smarty->fetch("hooks/hotelier/tpls/print_order.tpl");
		$file = ABSPATH."/orders/order-".$res_orders['uid']."-".$res_orders['txn_id'].".tpl";
		file_put_contents($file, $content);
		
		
		
	}
    
    mail('gkopanas@hotmail.com', 'Verified IPN', $listener->getTextReport($ar));

} else {
	
    /*
    An Invalid IPN *may* be caused by a fraudulent transaction attempt. It's
    a good idea to have a developer or sys admin manually investigate any 
    invalid IPN.
    */
	
    mail('gkopanas@hotmail.com', 'Invalid IPN', $listener->getTextReport());
    
}


exit;
//recurring_payment_suspended
//recurring_payment_profile_cancel

?>
