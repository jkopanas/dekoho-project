<?php
include("../init.php");

$file = $_GET['file'];
if( file_exists( $file.".php") ) {
	include $file.".php";
} else {
	json_encode(array("false"),true);
	exit;
}