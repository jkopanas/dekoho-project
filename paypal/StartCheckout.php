<?php
require_once('../init.php');
require_once('class/config.php');
include "class/class.paypal_recurring.php";

global $sql;


//print_ar($_POST);
//Get From DB settings
$action = ( $_POST['action'] == "hotel" ) ? "hotel" : "rooms";

$sql->db_Select("orders_types","id,title,settings","module='".$action."'");
$orderTypes = execute_single($sql);
$values = json_decode($orderTypes['settings'],true);


$ar = array();
$cur = explode(".",microtime(true));
$p = 0;
$price=0;
foreach((array)$_POST['data']['id'] as $key => $value) {
	
	if (!isset($_POST['data']['coupons-'.$value])) {
		$ar[$_POST['data']['coupons-'.$value]][]=$values['price'];
		$p = array_sum($ar[$_POST['data']['coupons-'.$value]]);
		$sql->db_Select("coupons","id","uid=".ID." and code in ('".$_POST['data']['coupons-'.$value]."') AND start_date < ".$cur[0]." and end_date > ".$cur[0]." and price >= ".$p);
		$res=execute_single($sql);
	}
	
	if ( !$res['id'] ) {
		$price=$price + $values['price'];
		$single_price = $values['price'];
	} else {
		$coupon[$_POST['data']['coupons-'.$value]][]=$values['price'];
		$single_price = 0;
	}
	
	$curtime = explode(".",microtime(true));
	$settings = array();
	$settings['start_date']= $_POST['data']['start_date-'.$value];
	$settings['end_date']= $_POST['data']['end_date-'.$value];
	$settings['discount'] = $_POST['data']['discount-'.$value];
	$settings['coupon'] = $_POST['data']['coupons-'.$value];
	$settings = json_encode($settings,true);
	
	
	///// Check end date before pass it to archive
	$sql->db_Select("orders","id","uid=".ID." and  module='".$action."' and status != 3 and itemid=".$value);
	$re=execute_single($sql);
	if ($re['id'] && $action != "hotel") {
		$sql->db_Update("orders","transaction_id=".$curtime[0].", settings='".$settings."', payment_method='".$_POST['data']['payment']."', price=".$single_price.",date_added='".$cur[0]."', billing_profile_id=".$_POST['data']['billing']." where id=".$re['id']);
	} elseif ($action == "rooms") {
		$sql->db_Insert("orders (id,transaction_id,uid,itemid,typeid,module,billing_profile_id,payment_method,status,date_added,price,archive,autorenew,settings)","'',".$curtime[0].",".ID.",".$value.",".$orderTypes['id'].",'".$action."',".$_POST['data']['billing'].",'".$_POST['data']['payment']."',0,'".$cur[0]."','".$single_price."',0,0,'".$settings."'");
	} elseif ( $action == "hotel" ) {
		$sql->db_Insert("orders (id,transaction_id,uid,itemid,typeid,module,billing_profile_id,payment_method,status,date_added,price,archive,autorenew,settings)","'',".$curtime[0].",".ID.",".$value.",".$orderTypes['id'].",'".$action."',".$_POST['data']['billing'].",'".$_POST['data']['payment']."',0,'".$cur[0]."','".$single_price."',0,1,'".$settings."'");		
	}

	$desc .= $_POST['data']['title-'.$value].", ";
	//	echo "orders (id,transaction_id,uid,itemid,typeid,module,billing_profile_id,payment_method,status,date_added,price,archive,autorenew,settings)","'',".$curtime[0].",".ID.",".$value.",".$orderTypes['id'].",'".$action."',".$_POST['data']['billing'].",'".$_POST['data']['payment']."',0,'".$cur[0]."','".$single_price."',0,0,'".$settings."'";

}
$obj=new paypal_recurring;

$obj->RecurringPayment = ($action == "hotel") ? true : false;

if ($obj->RecurringPayment) {
	$obj->orderDetails ="&L_BILLINGTYPE0=RecurringPayments&L_BILLINGAGREEMENTDESCRIPTION0=HotelMembership&L_PAYMENTREQUEST_0_NAME0=".urlencode($orderTypes['title'])."&L_PAYMENTREQUEST_0_NUMBER0=".$curtime[0]."&L_PAYMENTREQUEST_0_DESC0=".urlencode($desc)."&L_PAYMENTREQUEST_0_AMT0=".$price."&L_PAYMENTREQUEST_0_QTY0=1&PAYMENTREQUEST_0_ITEMAMT=".$price."&PAYMENTREQUEST_0_AMT=".$price;
} else {
	$obj->orderDetails ="&L_PAYMENTREQUEST_0_NAME0=".urlencode($orderTypes['title'])."&L_PAYMENTREQUEST_0_NUMBER0=".$curtime[0]."&L_PAYMENTREQUEST_0_DESC0=".urlencode($desc)."&L_PAYMENTREQUEST_0_AMT0=".$price."&L_PAYMENTREQUEST_0_QTY0=1&PAYMENTREQUEST_0_ITEMAMT=".$price."&PAYMENTREQUEST_0_AMT=".$price;
}
$obj->types = $orderTypes['id'];
$obj->API_Endpoint= "https://api-3t.paypal.com/nvp";
$obj->environment =  $mode;  // or 'beta-sandbox' or 'live'
$obj->paymentType = urlencode(($action == "hotel" ) ? 'Authorization': 'Sale');		// or 'Sale' or 'Order'
$obj->startDate = date(DATE_ATOM);
$obj->billingPeriod = urlencode($values['period']);				// or "Day", "Week", "SemiMonth", "Year"
$obj->billingFreq = urlencode($values['interval']);				// combination of this and billingPeriod must be at most a year
$obj->paymentAmount = $price; // urlencode($values['price']);
$obj->currencyID = urlencode($values['currency']);			// or other currency code ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
$obj->profileReference = $curtime[0];
/* PAYPAL API  DETAILS */
$obj->API_UserName = urlencode($api_username);
$obj->API_Password = urlencode($api_password);
$obj->API_Signature = urlencode($api_signature);
/*SET SUCCESS AND FAIL URL*/
$obj->returnURL = urlencode($domain."en/hotelier/payment.html#payment/Checkout/getExpressCheckout/".$action."/".$curtime[0]);
$obj->cancelURL = urlencode($domain."en/hotelier/payment.html#payment/error/false/".$action."/".$curtime[0]);
$obj->notifyURL = urlencode($domain.'paypal/ipn.php');

$obj->setExpressCheckout();

exit;

?>