<?php
require_once('../init.php');
require_once('class/config.php');

global $sql;

//print_ar($_POST);
//Get From DB settings
$action = ( $_POST['action'] == "hotel" ) ? "hotel" : "rooms";

$sql->db_Select("orders_types","id,title,settings","module='".$action."'");
$orderTypes = execute_single($sql);
$values = json_decode($orderTypes['settings'],true);


$ar = array();
$cur = explode(".",microtime(true));
$p = 0;
$price=0;

foreach($_POST['data']['id'] as $key => $value) {

	if (!isset($_POST['data']['coupons-'.$value])) {
		$ar[$_POST['data']['coupons-'.$value]][]=$values['price'];
		$p = array_sum($ar[$_POST['data']['coupons-'.$value]]);
		$sql->db_Select("coupons","id","uid=".ID." and code in ('".$_POST['data']['coupons-'.$value]."') AND start_date < ".$cur[0]." and end_date > ".$cur[0]." and price >= ".$p);
		$res=execute_single($sql);
	}

	if (!$res['id'] ) {
		$price=$price+$values['price'];
		$single_price = $values['price'];
	} else {
		$coupon[$_POST['data']['coupons-'.$value]][]=$values['price'];
		$single_price = 0;
	}

	$curtime = explode(".",microtime(true));
	$settings = array();
	$settings['start_date']= $_POST['data']['start_date-'.$value];
	$settings['end_date']= $_POST['data']['end_date-'.$value];
	$settings['discount'] = $_POST['data']['discount-'.$value];
	$settings['coupon'] = $_POST['data']['coupons-'.$value];
	$settings = json_encode($settings,true);


	///// Check end date before pass it to archive
	$sql->db_Select("orders","id","uid=".ID." and  module='".$action."' and status != 3 and itemid=".$value);
	$re=execute_single($sql);
	if ($re['id']) {
		$sql->db_Update("orders","transaction_id=".$curtime[0].", settings='".$settings."', payment_method='".$_POST['data']['payment']."', price=".$single_price.",date_added='".$cur[0]."', billing_profile_id=".$_POST['data']['billing']." where id=".$re['id']);
	} else {
		$sql->db_Insert("orders (id,transaction_id,uid,itemid,typeid,module,billing_profile_id,payment_method,status,date_added,price,archive,autorenew,settings)","'',".$curtime[0].",".ID.",".$value.",".$orderTypes['id'].",'".$action."',".$_POST['data']['billing'].",'".$_POST['data']['payment']."',0,'".$cur[0]."','".$single_price."',0,0,'".$settings."'");
	}

	$desc .= $_POST['data']['title-'.$value].", ";
	//	echo "orders (id,transaction_id,uid,itemid,typeid,module,billing_profile_id,payment_method,status,date_added,price,archive,autorenew,settings)","'',".$curtime[0].",".ID.",".$value.",".$orderTypes['id'].",'".$action."',".$_POST['data']['billing'].",'".$_POST['data']['payment']."',0,'".$cur[0]."','".$single_price."',0,0,'".$settings."'";

}
//Version
$tmpArr['PaymentData']['gcc']['version'] = $version;
//Merchant ID
$tmpArr['PaymentData']['gcc']['merchantID'] = $merchantID;
//Acquirer ID
$tmpArr['PaymentData']['gcc']['acquirerID'] = $acquirerID;
//The SSL secured URL of the merchant to which JCC will send the transaction result
//This should be SSL enabled – note https:// NOT http://
$tmpArr['PaymentData']['gcc']['responseURL'] = $domain."en/hotelier/payment.html#payment/Checkout/responseScript/rooms/".$curtime[0];
//Purchase Amount
$purchaseAmt = $price;

//Pad the purchase amount with 0's so that the total length is 13 characters, i.e. 20.50 will become 0000000020.50
$purchaseAmt = str_pad($purchaseAmt, 13, "0", STR_PAD_LEFT);
//Remove the dot (.) from the padded purchase amount(JCC will know from currency how many digits to consider as decimal)
//0000000020.50 will become 000000002050 (notice there is no dot)
$tmpArr['PaymentData']['gcc']['formattedPurchaseAmt'] = substr($purchaseAmt,0,10).substr($purchaseAmt,11);
//Euro currency ISO Code; see relevant appendix for ISO codes of other currencies
$tmpArr['PaymentData']['gcc']['currency'] = $currency;
//The number of decimal points for transaction currency, i.e. in this example we indicate that Euro has 2 decimal points
$tmpArr['PaymentData']['gcc']['currencyExp'] = 2;
//Order number
$tmpArr['PaymentData']['gcc']['orderID'] = $curtime[0];
//Specify we want not only to authorize the amount but also capture at the same time. could be M (for capturing later)
//Password
//$password = "1234abcd";
$password=$gccpassword;
//Form the plaintext string to encrypt by concatenating Password, Merchant ID, Acquirer ID, Order ID, Formatter Purchase Amount and Currency
//This will give 1234abcd | 0011223344 | 402971 | TestOrder12345 | 000000002050 | 978 (spaces and | introduced here for clarity)
$toEncrypt= $password.$tmpArr['PaymentData']['gcc']['merchantID'].$tmpArr['PaymentData']['gcc']['acquirerID'].$tmpArr['PaymentData']['gcc']['orderID'].$tmpArr['PaymentData']['gcc']['formattedPurchaseAmt'].$tmpArr['PaymentData']['gcc']['currency'];
//Produce the hash using SHA1
//This will give b14dcc7842a53f1ec7a621e77c106dfbe8283779
$sha1Signature = sha1($toEncrypt);
//Encode the signature using Base64 before transmitting to JCC
//This will give sU3MeEKlPx7HpiHnfBBt++goN3k=
$tmpArr['PaymentData']['gcc']['base64Sha1Signature'] = base64_encode(pack("H*",$sha1Signature));

$tmpArr['PaymentData']['gcc']['captureFlag'] = $captureFlag;
//The name of the hash algorithm use to create the signature; can be MD5 or SHA1; the latter is preffered and is what we used in this example
$tmpArr['PaymentData']['gcc']['signatureMethod'] = $signatureMethod;

echo json_encode($tmpArr,true);
exit;

?>