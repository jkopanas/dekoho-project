<?php

$_COOKIE['boxes'] = '';

include("init.php");



$current_module = ($_GET['module']? $loaded_modules[$_GET['module']]: $loaded_modules['content']);
$smarty->assign("current_module",$current_module);

define("PREVIEW", "false");

if ($_POST['var']) {

	foreach ($_POST['var'] as $k=>$v) {
		$posted_data[] = "$k : $v";
	}

	$mailer = new PHPMailer();
	$mailbody = implode("\n\r\<br>",$posted_data);


	$mailer->IsHTML(true);
	$mailer->From = $_POST['var']['email'];  // This HAS TO be your gmail address or it won't work!
	$mailer->FromName = COMPANY_NAME." ".$lang['user']; // This is the from name in the email, you can put anything you like here
	$mailer->Subject = 'New Question From '.SITE_NAME;
	$mailer->Body = $mailbody;
	$mailer->CharSet ="utf-8";
	$mailer->AddAddress(COMPANY_EMAIL);
	$mailer->Send();
	$harvester = new userHarvester();
	
	echo $harvester->writeUserToDB($_POST['var'],array('fields'=>'id','check'=>1));
	exit();

}

/*
$mails=array(0=>"achillion@rphotels.gr",1=>"dimgekas@gmail.com",2=>"efharishotel@yahoo.gr",3=>"elenamet@otenet.gr",4=>"france_h@otenet.gr",5=>"giamandes@giamandes.com",6=>"Guesthouse.Papastathi@gmail.com",7=>"guesthouseiridanos@hotmail.com",8=>"hotel@oreadeselati.gr",9=>"hotel-famisi@kmp.forthnet.gr",10=>"hotelgalaxyego@yahoo.com",11=>"hotelkostafamissi@yahoo.gr",12=>"hotelpanellinion@yahoo.gr",13=>"idalamagas@hotmail.com",14=>"info@alsoshouse.gr",15=>"info@arhontiko-tsimponi.gr",16=>"info@arsenis-meteora.gr",17=>"info@astrovolia.com",18=>"info@brizi.gr",19=>"info@camping-boufidis-meteora.com",20=>"info@dellasboutiquehotel.com",21=>"info@doupianihouse.com",22=>"info@fretzato.gr",23=>"info@galleryarthotel.gr",24=>"info@guesthouse-sotiriou.gr",25=>"info@hotel-adrachti.gr",26=>"info@hotelalexiou.com",27=>"info@hotel-edelweiss.gr",28=>"info@hotelgogos.com",29=>"info@hotelkastraki.gr",30=>"info@hotel-orfeas.gr",31=>"info@hotelpapanastasiou.gr",32=>"info@hotelrex.gr",33=>"info@kokkinosvrachos.gr",34=>"info@koziakashotel.gr",35=>"info@kripti.gr",36=>"info@ligerihotel.gr",37=>"info@magema.gr",38=>"info@meteoritis.gr",39=>"info@mikriarktos.com.gr",40=>"info@monastiri-guesthouse.gr",41=>"info@mythos-guesthouse.com",42=>"info@sangiorgio-hotel-meteora.com",43=>"info@stgeorgechrisomilia.gr",44=>"info@velousi.gr",45=>"info@viglapiras.gr",46=>"info@vilafoti.gr",47=>"info@xenonaslefteriloukias.gr",48=>"info@xenonaspyrras.gr",49=>"info@xilinohorio.gr",50=>"litheonhotel@gmail.com",51=>"magiossi@yahoo.com",52=>"mantania@otenet.gr",53=>"mithos@mithos-hotel.gr",54=>"mpalatsos@kmp.forthnet.gr",55=>"papagiannishotel@yahoo.gr",56=>"periklis.lithos@yahoo.gr",57=>"pertouli@pertoulihotel.gr",58=>"pertouli@trk.forthnet.gr",59=>"plakiasn@gmail.com",60=>"reservations@totiboutiquehotel.com",61=>"spnikolo@ath.forthnet.gr",62=>"tapetrina@tapetrina.gr",63=>"tkai49@otenet.gr",64=>"tomis6369@gmail.com",65=>"trigona@otenet.gr",66=>"tsikelihotelf@gmail.com",67=>"xenonas@kroupi.gr",68=>"ziogasrooms@gmail.com",69=>"aiolides@yahoo.gr",70=>"antigoni@antigoni-hotel.gr",71=>"chr_sidas@yahoo.gr",72=>"info@adamoma.gr",73=>"info@aenao-hotel.gr",74=>"info@akrolimnia.gr",75=>"info@anerades.gr",76=>"info@apolisclub.gr",77=>"info@drimos.com.gr",78=>"info@esperiahotel-plastira.gr",79=>"info@evilion-lakeplastira.gr",80=>"info@hotelkoutsikouris.gr",81=>"info@kinthia.gr",82=>"info@ktimaalonaki.com",83=>"info@loutrasmokovou.gr",84=>"info@megdovas.gr",85=>"info@myavra.gr",86=>"info@oramahotel.gr",87=>"info@oreiades-resort.gr",88=>"info@plastirashotel.gr",89=>"info@villamontania.gr",90=>"info@xenonas-archontikon.gr",91=>"info@zakoni.gr",92=>"ipsivaton@yahoo.gr",93=>"kierion1@otenet.gr",94=>"mail@dolopia.gr",95=>"mail@tasprolithia.gr",96=>"musesrooms@yahoo.gr",97=>"pezoula@hol.gr",98=>"reservations@limni-plastira-suites.gr",99=>"rooms@artemishostel.gr",100=>"titagionhotel@windowslife.com",101=>"welcome@katsaros-suites.gr",102=>"ghlar79@otenet.gr",103=>"hotel@aigligroup.gr",104=>"hotelkouria@otenet.gr",105=>"hotelvlassis@gmail.com",106=>"info@achilliohotel.gr",107=>"info@asterashotel.gr",108=>"info@avli-olympou.gr",109=>"info@christina-hotel.gr",110=>"info@dionissoshotel.gr",111=>"info@dohos.gr",112=>"info@gbh.gr",113=>"info@giouli-hotel.gr",114=>"info@goldensunhotel.eu",115=>"info@helena.gr",116=>"info@hotel-alexiou.gr",117=>"info@hotelmetoxi.gr",118=>"info@hotelmetropol.gr",119=>"info@hotel-stomio-beach.gr",120=>"info@kastrihotel.gr",121=>"info@melastron.com",122=>"info@panoramastomio.gr",123=>"info@parkhotellarisa.gr",124=>"info@poiantas.gr",125=>"info@villa-yianna.gr",126=>"info@xenonas-ios.com",127=>"isvoros@gmail.com",128=>"lanari@lanari.gr",129=>"villa.kornilia@gmail.com",130=>"voulioti@nostosapartments.gr");


foreach($mails as $key=>$value) {

        sleep(1);
        $date = new DateTime();
        $uname =substr($date->getTimestamp(),3);
        $date= $date->getTimestamp();

        echo $uname."<br/>";
        //echo "INSERT INTO users (`uname`,`pass`,`email`,`user_folder`,`user_location`,`user_join`,`user_login`,user_class) VALUES ('GR$uname','Smpnqnsnjngnmnonhosomoq','$key','/var/www/vhosts/dekoho.com/httpdocs/media_files/users/$folder','gr',$date,1,'U')";
        $sql->db_Insert("users (`uname`,`pass`,`email`,`user_location`,`user_join`,`user_login`,user_class)", "'GR$uname','Smpnqnsnjngnmnonhosomoq','$value','gr',$date,1,'U'" );
        $id=$sql->last_insert_id;
        //echo "users (`uname`,`pass`,`email`,`user_location`,`user_join`,`user_login`,user_class)", "'GR$uname','Smpnqnsnjngnmnonhosomoq','$value','gr',$date,1,'U'";
        $sql->db_Update("users","user_folder='/var/www/vhosts/dekoho.com/httpdocs/media_files/users/".$id."' WHERE id=".$id);

}
*/



if (!$smarty->isCached('home.tpl',$url)) {
	
	$l = new siteModules();
	$layout = $l->pageBoxes(($_GET['module']? $_GET['module']: 'all'),e_FILE,'front',array('getBoxes'=>'full','fields'=>'boxes,areas,id,settings','boxFields'=>'id,title,name,settings,fetchType,file,required_modules,template','init'=>1,'boxFilters'=>array('active'=>1),'debug'=>0));
	
	$smarty->assign("layout",$layout['boxes']);
	$smarty->assign("layout_settings",$layout['settings']);
}//END BOXES

$smarty->assign("nav_area","home");//assigned template variable include_file
//assigned template variable include_file


$sql->q("select * from exchange group by currency;");
$currencies=execute_multi($sql);
$currencies[]= array("currency"=>"EUR","rate" => "1","symbol"=> "&euro;");

$smarty->assign("currencies",$currencies);

$_SESSION['currency'] = (isset($_POST['currency'])) ? $_POST['currency']  : ( ( !isset($_SESSION['currency']) ) ? $_SESSION['plugins']['dynamicLanguage']['currency'] : $_SESSION['currency']);


foreach($currencies as $key => $value) {
	
	if  ( $value['currency'] == $_SESSION['currency'] ) {
		$rate = $value['rate'];
		$smarty->assign("current_currency",$currencies[$key]);
		unset($currencies[$key]);
	} 
}
if (!isset($_SESSION['popuponce']) || $_SESSION['popuponce']=false ) {
	$smarty->assign("popuponce",0);	
	$_SESSION['popuponce']=true;
} else {
	$smarty->assign("popuponce",1);
}

$smarty->assign("currencies",$currencies);
$_SESSION['exchange']=$rate;
include(ABSPATH."/memcache.php");
$smarty->assign("exchange",$rate);
$smarty->caching = USE_SMARTY_CAHCHING;
HookParent::getInstance()->doTriggerHookGlobally("HomePreFetch");
if ($_GET['preview']) {	
	$smarty->display("homepreview.tpl",$url);//Display the home.tpl template
} else {	
	$smarty->display("home.tpl",$url);//Display the home.tpl template
}
//$smarty->display("search_results.tpl",$url);

?>
