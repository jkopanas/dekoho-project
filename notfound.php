<?php

exit;

	include ("init.php");
	require ('Logging.class.php');

	$_SESSION['plugins']['dynamicLanguage']['countryName'] = ($lang[$_SESSION['plugins']['dynamicLanguage']['countryName']]) ? $lang[$_SESSION['plugins']['dynamicLanguage']['countryName']] : $_SESSION['plugins']['dynamicLanguage']['countryName'];
	$smarty -> assign("Cultures", $_SESSION['plugins']['dynamicLanguage']['marketSettings']);
	$smarty -> assign("TheCountry", $_SESSION['plugins']['dynamicLanguage']['countryName']);
	$smarty -> assign("CulturesTpl", json_decode($_SESSION['plugins']['dynamicLanguage']['marketSettings'], true));
	$smarty -> assign("MarketKey", $_SESSION['plugins']['dynamicLanguage']['marketKey']);
	$smarty -> assign("currency", $_SESSION['plugins']['dynamicLanguage']['currency']);

	$pageURL = 'http';

	$txt_path = "tmp/notfound.txt";

	if ($_SERVER["HTTPS"] == "on")
	{
		$pageURL .= "s";
	}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80")
	{
		$pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
	}
	else
	{
		$pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
	}

	$log = new Logging();

	$txt_file = file_get_contents($txt_path);
	$rows = explode("\n", $txt_file);
	array_shift($rows);

	$i = 0;
	$data = array();

	foreach ($rows as $row)
	{
		$i++;
		$row = end(explode("/", $row));

		if ($i % 2 == 1)
		{
			$txt_data[] = $row;
		}
	}

	// set path and name of log file (optional)
	$log -> lfile($txt_path);

	$request = explode("/", $_SERVER["REQUEST_URI"]);

	if (!in_array($request[1], $txt_data))
	{
		//echo 'write';
	}
	else
	{
		//echo 'not';
	}

	// write message to the log file
	$log -> lwrite($pageURL);

	// close log file
	$log -> lclose();

	$extrafield = array('Meta_tags' => array('meta_title' => array('value' => '404 Status Code (Page Not Found)')));

	$smarty -> assign("extraField", $extrafield);
	$smarty -> assign("include_file", "notfound.tpl");
	$smarty -> display("home.tpl", $url);
?>