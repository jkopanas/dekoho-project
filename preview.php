<?php
include ("init.php");


   
$current_module = ($_GET['module']? $loaded_modules[$_GET['module']]: $loaded_modules['content']);
$smarty->assign("current_module",$current_module);

define("PREVIEW", "true");


if (!$smarty->isCached('home.tpl',$url)) {
	
	$l = new siteModules();
	$layout = $l->pageBoxes(($_GET['module']? $_GET['module']: 'all'),e_FILE,'front',array('getBoxes'=>'full','fields'=>'boxes,areas,id,settings','boxFields'=>'id,title,name,settings,fetchType,file,required_modules,template','init'=>1,'boxFilters'=>array('active'=>1),'debug'=>0));
	print_ar($layout);
	$smarty->assign("layout",$layout['boxes']);
	$smarty->assign("layout_settings",$layout['settings']);
}//END BOXES



include(ABSPATH."/memcache.php");
$smarty->caching = USE_SMARTY_CAHCHING;
HookParent::getInstance()->doTriggerHookGlobally("HomePreFetch");
$smarty->display("homepreview.tpl",$url);//Display the home.tpl template
//$smarty->display("search_results.tpl",$url);

?>
